#!/usr/bin/octave -q
# -*- octave -*-

clear
close all

# Wall clock times for the document/literal loan example.
# 
# Software specs:
#
# `/usr/bin/time -V`:
# GNU time 1.7.
#
# `lsb_release -a`:
# No LSB modules are available.
# Distributor ID:	Ubuntu
# Description:	Ubuntu 8.04
# Release:	8.04
# Codename:	hardy
#
# `uname -rvso`:
# Linux 2.6.24-18-generic #1 SMP Wed May 28 20:27:26 UTC 2008 GNU/Linux
#
# `java -version`:
# java version "1.6.0_06"
# Java(TM) SE Runtime Environment (build 1.6.0_06-b02)
# Java HotSpot(TM) Client VM (build 10.0-b22, mixed mode, sharing)
#
# `ant -version`:
# Apache Ant version 1.7.0 compiled on August 29 2007
#
# `perl -v`:
# This is perl, v5.8.8 built for i486-linux-gnu-thread-multi
#
# Using BPELUnit 1.0, ActiveBPEL 4.1 and Daikon 4.3.1. 
#
# Hardware specs (from /proc/{cpuinfo,meminfo}):
# CPU: 2 x Core Duo T2250 @ 1.73Ghz, 2048KiB cache, 3460 bogomips
# Memory: 1 GiB RAM DDR2 533MHz
# HDD: 80GB 5400rpm SATA (8 MB Buffer)

### Measurements

instr = [2.1, 2.19, 2.19, 2.39, 2.24, 2.19, 2.1, 2.15, 2.09, 2.09];

exec_5   = [2.28, 2.26, 2.26, 2.3, 2.5, 2.26, 2.35, 2.25, 2.27, 2.27];
exec_10  = [2.7, 2.86, 2.66, 2.66, 2.62, 2.66, 2.66, 2.91, 2.67, 2.66];
exec_20  = [3.58, 3.74, 3.51, 3.48, 3.59, 3.66, 3.46, 3.43, 3.68, 3.46];
exec_50  = [5.91, 5.68, 6.01, 5.79, 6.03, 5.68, 5.77, 6, 5.74, 5.98];

analysis_5  = [12.55, 14.15, 13.49, 14.02, 12.64, 13.59, 13.31, 13.77, 13.05, 13.71];
analysis_10 = [13.6, 13.13, 13.42, 12.61, 14.96, 14.26, 13.61, 13.46, 12.98, 13.44];
analysis_20 = [13.91, 14.76, 13.36, 15.28, 13.94, 13.73, 14.87, 14.86, 13.87, 13.39];
analysis_50 = [15.33, 14.96, 15.23, 15.37, 16.3, 15.97, 16.41, 15.3, 17.04, 14.69];

### Averages for 5, 10, 20 and 50 cases

avg_instr      = [5 10 20 50; 1  1  1  1];
avg_instr(2,:) = avg_instr(2,:) * mean(instr);

avg_exec = [5 10 20 50;...
	    mean(exec_5) mean(exec_10) mean(exec_20) mean(exec_50)];

avg_analysis = [5 10 20 50;...
	       mean(analysis_5) mean(analysis_10) mean(analysis_20) mean(analysis_50)];

### Plots
# http://www.nabble.com/setting-font-size-for-axes-names-td17537306.html
set(0,"Defaulttextfontsize",18);

f1 = figure;
plot(avg_instr(1,:), avg_instr(2,:), '-ro;Instrumentation;', "markersize", 2,...
     avg_exec(1,:), avg_exec(2,:), '-b*;Execution;', "markersize", 2,...
     avg_analysis(1,:), avg_analysis(2,:), '-m+;Analysis;', "markersize", 2);
%title 'Time per processing step';
xlabel 'Number of test cases';
ylabel 'Seconds';
axis([0 1 0 max([avg_analysis(2,:) avg_exec(2,:) avg_instr(2,:)](:))+2]);
axis "autox";
print('times.eps', '-deps', '-solid');

f2 = figure;
plot(avg_instr(1,:), avg_instr(2,:), '-ro;Instrumentation;',"markersize", 2,...
     avg_exec(1,:), avg_exec(2,:)+avg_instr(2,:), '-b*;Instrumentation + Execution;',"markersize", 2,...
     avg_analysis(1,:), avg_analysis(2,:)+avg_exec(2,:)+avg_instr(2,:), '-m+;All;',"markersize", 2);
%title 'Accumulated time per processing step';
xlabel 'Number of test cases';
ylabel 'Seconds';
axis([0 1 0 max([avg_analysis(2,:)+avg_exec(2,:)+avg_instr(2,:)](:))+2]);
axis "autox";
print('acumTimes.eps', '-deps', '-solid');
