<?xml version="1.0" encoding="UTF-8"?>
<tes:testSuite 
    xmlns:ap="http://j2ee.netbeans.org/wsdl/ApprovalService"
    xmlns:as="http://j2ee.netbeans.org/wsdl/AssessorService"
    xmlns:sp="http://j2ee.netbeans.org/wsdl/LoanService"
    xmlns:gen="http://xml.netbeans.org/schema/Loans"
    xmlns:tes="http://www.bpelunit.org/schema/testSuite">

  <tes:name>loanApprovalProcess</tes:name>
  <tes:baseURL>http://localhost:7777/ws</tes:baseURL>

  <tes:deployment>
    <tes:put name="loanApprovalProcess" type="activebpel">
      <tes:wsdl>LoanService.wsdl</tes:wsdl>
      <tes:property name="BPRFile">LoanApprovalDoc.bpr</tes:property>
    </tes:put>
    <tes:partner name="assessor" wsdl="AssessorService.wsdl"/>
    <tes:partner name="approver" wsdl="ApprovalService.wsdl"/>
  </tes:deployment>

  <tes:testCases>
    <tes:testCase name="SmallAmountLowRisk" basedOn="" abstract="false" vary="false">
      <tes:clientTrack>
	<tes:sendReceive 
	    service="sp:LoanServiceService" 
	    port="LoanServicePort"
	    operation="grantLoan">

	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalRequest>
		<gen:amount>9999</gen:amount>
	      </gen:ApprovalRequest>
	    </tes:data>
	  </tes:send>

	  <tes:receive fault="false">
	    <tes:condition>
	      <tes:expression>//gen:accept</tes:expression>
	      <tes:value>'true'</tes:value>
	    </tes:condition>
	  </tes:receive>
	</tes:sendReceive>
      </tes:clientTrack>

      <tes:partnerTrack name="assessor">
	<tes:receiveSend
	    service="as:AssessorServiceService"
	    port="AssessorServicePort"
	    operation="assessLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:AssessorResponse>
		<gen:risk>low</gen:risk>
	      </gen:AssessorResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>

      <tes:partnerTrack name="approver"/>
    </tes:testCase>

    <tes:testCase name="SmallAmountHighRiskOK" basedOn="" abstract="false" vary="false">
      <tes:clientTrack>
	<tes:sendReceive 
	    service="sp:LoanServiceService" 
	    port="LoanServicePort"
	    operation="grantLoan">

	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalRequest>
		<gen:amount>9999</gen:amount>
	      </gen:ApprovalRequest>
	    </tes:data>
	  </tes:send>

	  <tes:receive fault="false">
	    <tes:condition>
	      <tes:expression>//gen:accept</tes:expression>
	      <tes:value>'true'</tes:value>
	    </tes:condition>
	  </tes:receive>
	</tes:sendReceive>
      </tes:clientTrack>

      <tes:partnerTrack name="assessor">
	<tes:receiveSend
	    service="as:AssessorServiceService"
	    port="AssessorServicePort"
	    operation="assessLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:AssessorResponse>
		<gen:risk>high</gen:risk>
	      </gen:AssessorResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>

      <tes:partnerTrack name="approver">
	<tes:receiveSend 
	    service="ap:ApprovalServiceService"
	    port="ApprovalServicePort"
	    operation="approveLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalResponse>
		<gen:accept>true</gen:accept>
	      </gen:ApprovalResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>

    </tes:testCase>

    <tes:testCase name="SmallAmountHighRiskRejected" basedOn="" abstract="false" vary="false">
      <tes:clientTrack>
	<tes:sendReceive 
	    service="sp:LoanServiceService" 
	    port="LoanServicePort"
	    operation="grantLoan">

	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalRequest>
		<gen:amount>9999</gen:amount>
	      </gen:ApprovalRequest>
	    </tes:data>
	  </tes:send>

	  <tes:receive fault="false">
	    <tes:condition>
	      <tes:expression>//gen:accept</tes:expression>
	      <tes:value>'false'</tes:value>
	    </tes:condition>
	  </tes:receive>
	</tes:sendReceive>
      </tes:clientTrack>

      <tes:partnerTrack name="assessor">
	<tes:receiveSend
	    service="as:AssessorServiceService"
	    port="AssessorServicePort"
	    operation="assessLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:AssessorResponse>
		<gen:risk>high</gen:risk>
	      </gen:AssessorResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>

      <tes:partnerTrack name="approver">
	<tes:receiveSend 
	    service="ap:ApprovalServiceService"
	    port="ApprovalServicePort"
	    operation="approveLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalResponse>
		<gen:accept>false</gen:accept>
	      </gen:ApprovalResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>

    </tes:testCase>

    <tes:testCase name="LargeAmountOK" basedOn="" abstract="false" vary="false">
      <tes:clientTrack>
	<tes:sendReceive 
	    service="sp:LoanServiceService" 
	    port="LoanServicePort"
	    operation="grantLoan">

	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalRequest>
		<gen:amount>10001</gen:amount>
	      </gen:ApprovalRequest>
	    </tes:data>
	  </tes:send>

	  <tes:receive fault="false">
	    <tes:condition>
	      <tes:expression>//gen:accept</tes:expression>
	      <tes:value>'true'</tes:value>
	    </tes:condition>
	  </tes:receive>
	</tes:sendReceive>
      </tes:clientTrack>

      <tes:partnerTrack name="assessor"/>

      <tes:partnerTrack name="approver">
	<tes:receiveSend 
	    service="ap:ApprovalServiceService"
	    port="ApprovalServicePort"
	    operation="approveLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalResponse>
		<gen:accept>true</gen:accept>
	      </gen:ApprovalResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>
    </tes:testCase>

    <tes:testCase name="LargeAmountRejected" basedOn="" abstract="false" vary="false">
      <tes:clientTrack>
	<tes:sendReceive 
	    service="sp:LoanServiceService" 
	    port="LoanServicePort"
	    operation="grantLoan">

	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalRequest>
		<gen:amount>10001</gen:amount>
	      </gen:ApprovalRequest>
	    </tes:data>
	  </tes:send>

	  <tes:receive fault="false">
	    <tes:condition>
	      <tes:expression>//gen:accept</tes:expression>
	      <tes:value>'false'</tes:value>
	    </tes:condition>
	  </tes:receive>
	</tes:sendReceive>
      </tes:clientTrack>

      <tes:partnerTrack name="assessor"/>

      <tes:partnerTrack name="approver">
	<tes:receiveSend 
	    service="ap:ApprovalServiceService"
	    port="ApprovalServicePort"
	    operation="approveLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalResponse>
		<gen:accept>false</gen:accept>
	      </gen:ApprovalResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>

    </tes:testCase>


    <tes:testCase name="Limite3HighRiskRejected" basedOn="" abstract="false" vary="false">
      <tes:clientTrack>
	<tes:sendReceive 
	    service="sp:LoanServiceService" 
	    port="LoanServicePort"
	    operation="grantLoan">

	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalRequest>
		<gen:amount>10000</gen:amount>
	      </gen:ApprovalRequest>
	    </tes:data>
	  </tes:send>

	  <tes:receive fault="false">
	    <tes:condition>
	      <tes:expression>//gen:accept</tes:expression>
	      <tes:value>'false'</tes:value>
	    </tes:condition>
	  </tes:receive>
	</tes:sendReceive>
      </tes:clientTrack>

      <tes:partnerTrack name="assessor">
	<tes:receiveSend
	    service="as:AssessorServiceService"
	    port="AssessorServicePort"
	    operation="assessLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:AssessorResponse>
		<gen:risk>high</gen:risk>
	      </gen:AssessorResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>

      <tes:partnerTrack name="approver">
	<tes:receiveSend 
	    service="ap:ApprovalServiceService"
	    port="ApprovalServicePort"
	    operation="approveLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalResponse>
		<gen:accept>false</gen:accept>
	      </gen:ApprovalResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>

    </tes:testCase>

    <tes:testCase name="LimiteHighRiskOK" basedOn="" abstract="false" vary="false">
      <tes:clientTrack>
	<tes:sendReceive 
	    service="sp:LoanServiceService" 
	    port="LoanServicePort"
	    operation="grantLoan">

	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalRequest>
		<gen:amount>10000</gen:amount>
	      </gen:ApprovalRequest>
	    </tes:data>
	  </tes:send>

	  <tes:receive fault="false">
	    <tes:condition>
	      <tes:expression>//gen:accept</tes:expression>
	      <tes:value>'true'</tes:value>
	    </tes:condition>
	  </tes:receive>
	</tes:sendReceive>
      </tes:clientTrack>

      <tes:partnerTrack name="assessor">
	<tes:receiveSend
	    service="as:AssessorServiceService"
	    port="AssessorServicePort"
	    operation="assessLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:AssessorResponse>
		<gen:risk>high</gen:risk>
	      </gen:AssessorResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>

      <tes:partnerTrack name="approver">
	<tes:receiveSend 
	    service="ap:ApprovalServiceService"
	    port="ApprovalServicePort"
	    operation="approveLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalResponse>
		<gen:accept>true</gen:accept>
	      </gen:ApprovalResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>

    </tes:testCase>

    <tes:testCase name="LimiteLowRisk" basedOn="" abstract="false" vary="false">
      <tes:clientTrack>
	<tes:sendReceive 
	    service="sp:LoanServiceService" 
	    port="LoanServicePort"
	    operation="grantLoan">

	  <tes:send fault="false">
	    <tes:data>
	      <gen:ApprovalRequest>
		<gen:amount>10000</gen:amount>
	      </gen:ApprovalRequest>
	    </tes:data>
	  </tes:send>

	  <tes:receive fault="false">
	    <tes:condition>
	      <tes:expression>//gen:accept</tes:expression>
	      <tes:value>'true'</tes:value>
	    </tes:condition>
	  </tes:receive>
	</tes:sendReceive>
      </tes:clientTrack>

      <tes:partnerTrack name="assessor">
	<tes:receiveSend
	    service="as:AssessorServiceService"
	    port="AssessorServicePort"
	    operation="assessLoan">
	  <tes:receive fault="false"/>
	  <tes:send fault="false">
	    <tes:data>
	      <gen:AssessorResponse>
		<gen:risk>low</gen:risk>
	      </gen:AssessorResponse>
	    </tes:data>
	  </tes:send>
	</tes:receiveSend>
      </tes:partnerTrack>

      <tes:partnerTrack name="approver"/>
    </tes:testCase>
  </tes:testCases>
</tes:testSuite>

