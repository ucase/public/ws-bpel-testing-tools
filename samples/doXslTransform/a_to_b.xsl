<stylesheet xmlns="http://www.w3.org/1999/XSL/Transform" xmlns:p="http://www.uca.es/bpel">

	<template match="p:ReplaceAWithBProcessRequest">
		<p:ReplaceAWithBProcessResponse>
			<apply-templates />
		</p:ReplaceAWithBProcessResponse>
	</template>

	<template match="p:a">
		<p:b><value-of select="." /></p:b>
	</template>

</stylesheet>