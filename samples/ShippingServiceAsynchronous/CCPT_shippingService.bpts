<?xml version="1.0" encoding="utf-8"?>
<tes:testSuite xmlns:tns="http://example.com/shipping/interfaces/"
               xmlns:tes="http://www.bpelunit.org/schema/testSuite"
               xmlns:sif="http://example.com/shipping/partnerLinkTypes/"
               xmlns:ship="http://example.com/shipping/ship.xsd">

  <tes:name>ShippingServiceTest</tes:name>
  <tes:baseURL>http://localhost:7777/ws</tes:baseURL>

  <tes:deployment>
    <tes:put name="shippingService" type="activebpel">
      <tes:wsdl>shippingLT.wsdl</tes:wsdl>
      <tes:property name="BPRFile">shipping.bpr</tes:property>
    </tes:put>
    <tes:partner name="Intermediary" wsdl="shippingLT.wsdl"/>
  </tes:deployment>

  <tes:testCases>

    <!-- El cliente hace una peticion de un envio completo de 1 elemento -->
    <!-- Se envia una notificacion de envio de 1 elemento -->
    <tes:testCase name="SS_P1" basedOn="" abstract="false" vary="false">
      <tes:clientTrack>
        <tes:sendReceiveAsynchronous>
          <tes:send fault="false" service="tns:shippingService"
                    port="shippingService"
                    operation="shippingRequest">
            <tes:data>
              <ship:shipOrder>
                <ship:ShipOrderRequestHeader>
                  <ship:shipOrderID>555</ship:shipOrderID>
                  <ship:itemsTotal>1</ship:itemsTotal>
		  <ship:shipComplete>true</ship:shipComplete>
                </ship:ShipOrderRequestHeader>
              </ship:shipOrder>
            </tes:data>
          </tes:send>

          <tes:receive fault="false" service="tns:shippingServiceCallback"
                       port="shippingServiceCustomer"
                       operation="shippingNotice">
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:shipOrderID</tes:expression>
              <tes:value>555</tes:value>
            </tes:condition>
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:itemsCount</tes:expression>
              <tes:value>1</tes:value>
            </tes:condition>
	  </tes:receive>
        </tes:sendReceiveAsynchronous>
      </tes:clientTrack>

      <tes:partnerTrack name="Intermediary"/>
    </tes:testCase>

    <!-- El cliente hace una peticion de un envio no completo de 1 elemento -->
    <!-- Se envia una notificacion de envio de 1 elemento -->
    <tes:testCase name="SS_P2" basedOn="" abstract="false" vary="false">
      <tes:clientTrack>
        <tes:sendReceiveAsynchronous>
          <tes:send fault="false" service="tns:shippingService"
                    port="shippingService"
                    operation="shippingRequest">
            <tes:data>
              <ship:shipOrder>
                <ship:ShipOrderRequestHeader>
                  <ship:shipOrderID>555</ship:shipOrderID>
                  <ship:itemsTotal>1</ship:itemsTotal>
                  <ship:shipComplete>false</ship:shipComplete> 
                </ship:ShipOrderRequestHeader>
              </ship:shipOrder>
            </tes:data>
          </tes:send>

          <tes:receive fault="false" service="tns:shippingServiceCallback"
                       port="shippingServiceCustomer"
                       operation="shippingNotice">
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:shipOrderID</tes:expression>
              <tes:value>555</tes:value>
            </tes:condition>
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:itemsCount</tes:expression>
              <tes:value>1</tes:value>
            </tes:condition>
	  </tes:receive>
        </tes:sendReceiveAsynchronous> 
      </tes:clientTrack>

      <tes:partnerTrack name="Intermediary">
        <tes:receiveSendAsynchronous>
          <tes:receive service="tns:IntermediarySoapService"
                       port="IntermediarySoapHttpPort"
                       operation="requestItems"
                       fault="false">
	    <tes:condition>
              <tes:expression>ship:shipOrder/ship:ShipOrderRequestHeader/ship:shipOrderID</tes:expression>
              <tes:value>555</tes:value>
            </tes:condition>
	    <tes:condition>
              <tes:expression>ship:shipOrder/ship:ShipOrderRequestHeader/ship:itemsTotal</tes:expression>
              <tes:value>1</tes:value>
            </tes:condition>
	  </tes:receive>

          <tes:send service="tns:IntermediaryCallbackService"
                    port="IntermediaryCallbackSoapHttpPort"
                    operation="itemsArrived"
                    fault="false">
            <tes:data>
	      <ship:shipNotice>
                <ship:ShipNoticeHeader>
                  <ship:shipOrderID>555</ship:shipOrderID>
                  <ship:itemsCount>1</ship:itemsCount>
                </ship:ShipNoticeHeader>
              </ship:shipNotice>
	    </tes:data>
          </tes:send>
        </tes:receiveSendAsynchronous>
      </tes:partnerTrack>
    </tes:testCase>

    <!-- El cliente hace una peticion de un envio no completo de 4 elementos -->
    <!-- Se envian 4 notificaciones de 1 elemento cada una -->
    <tes:testCase name="SS_P3" basedOn="" abstract="false" vary="false">
      <tes:clientTrack>
          <tes:sendOnly fault="false" service="tns:shippingService"
                    port="shippingService"
                    operation="shippingRequest">
            <tes:data>
              <ship:shipOrder>
                <ship:ShipOrderRequestHeader>
                  <ship:shipOrderID>555</ship:shipOrderID>
                  <ship:itemsTotal>4</ship:itemsTotal>
                  <ship:shipComplete>false</ship:shipComplete> 
                </ship:ShipOrderRequestHeader>
              </ship:shipOrder>
            </tes:data>
          </tes:sendOnly>

          <tes:receiveOnly fault="false" service="tns:shippingServiceCallback"
                       port="shippingServiceCustomer"
                       operation="shippingNotice">
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:shipOrderID</tes:expression>
              <tes:value>555</tes:value>
            </tes:condition>
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:itemsCount</tes:expression>
              <tes:value>1</tes:value>
            </tes:condition>
	  </tes:receiveOnly>

          <tes:receiveOnly fault="false" service="tns:shippingServiceCallback"
                       port="shippingServiceCustomer"
                       operation="shippingNotice">
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:shipOrderID</tes:expression>
              <tes:value>555</tes:value>
            </tes:condition>
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:itemsCount</tes:expression>
              <tes:value>1</tes:value>
            </tes:condition>
	  </tes:receiveOnly>

          <tes:receiveOnly fault="false" service="tns:shippingServiceCallback"
                       port="shippingServiceCustomer"
                       operation="shippingNotice">
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:shipOrderID</tes:expression>
              <tes:value>555</tes:value>
            </tes:condition>
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:itemsCount</tes:expression>
              <tes:value>1</tes:value>
            </tes:condition>
	  </tes:receiveOnly>

	  <tes:receiveOnly fault="false" service="tns:shippingServiceCallback"
                       port="shippingServiceCustomer"
                       operation="shippingNotice">
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:shipOrderID</tes:expression>
              <tes:value>555</tes:value>
            </tes:condition>
	    <tes:condition>
              <tes:expression>ship:shipNotice/ship:ShipNoticeHeader/ship:itemsCount</tes:expression>
              <tes:value>1</tes:value>
            </tes:condition>
	  </tes:receiveOnly>
      </tes:clientTrack>

      <tes:partnerTrack name="Intermediary">
          <tes:receiveOnly service="tns:IntermediarySoapService"
                       port="IntermediarySoapHttpPort"
                       operation="requestItems"
                       fault="false">
	    <tes:condition>
              <tes:expression>ship:shipOrder/ship:ShipOrderRequestHeader/ship:shipOrderID</tes:expression>
              <tes:value>555</tes:value>
            </tes:condition>
	    <tes:condition>
              <tes:expression>ship:shipOrder/ship:ShipOrderRequestHeader/ship:itemsTotal</tes:expression>
              <tes:value>4</tes:value>
            </tes:condition>
	   </tes:receiveOnly>

	  <tes:sendOnly service="tns:IntermediaryCallbackService"
                    port="IntermediaryCallbackSoapHttpPort"
                    operation="itemsArrived"
                    fault="false">
            <tes:data>
	      <ship:shipNotice>
                <ship:ShipNoticeHeader>
                  <ship:shipOrderID>555</ship:shipOrderID>
                  <ship:itemsCount>1</ship:itemsCount>
                </ship:ShipNoticeHeader>
              </ship:shipNotice>
	    </tes:data>
          </tes:sendOnly>

          <tes:sendOnly service="tns:IntermediaryCallbackService"
			port="IntermediaryCallbackSoapHttpPort"
			operation="itemsArrived"
			fault="false">
            <tes:data>
	      <ship:shipNotice>
                <ship:ShipNoticeHeader>
                  <ship:shipOrderID>555</ship:shipOrderID>
                  <ship:itemsCount>1</ship:itemsCount>
                </ship:ShipNoticeHeader>
              </ship:shipNotice>
	    </tes:data>
          </tes:sendOnly>
	  
          <tes:sendOnly service="tns:IntermediaryCallbackService"
			port="IntermediaryCallbackSoapHttpPort"
			operation="itemsArrived"
			fault="false">
            <tes:data>
	      <ship:shipNotice>
                <ship:ShipNoticeHeader>
                  <ship:shipOrderID>555</ship:shipOrderID>
                  <ship:itemsCount>1</ship:itemsCount>
                </ship:ShipNoticeHeader>
              </ship:shipNotice>
	    </tes:data>
          </tes:sendOnly>
	  
          <tes:sendOnly service="tns:IntermediaryCallbackService"
			port="IntermediaryCallbackSoapHttpPort"
			operation="itemsArrived"
			fault="false">
            <tes:data>
	      <ship:shipNotice>
                <ship:ShipNoticeHeader>
                  <ship:shipOrderID>555</ship:shipOrderID>
                  <ship:itemsCount>1</ship:itemsCount>
                </ship:ShipNoticeHeader>
              </ship:shipNotice>
	    </tes:data>
          </tes:sendOnly>
      </tes:partnerTrack>
    </tes:testCase>
    
  </tes:testCases>
  
</tes:testSuite>
