Problema de los mutantes repetidos
==================================

Detección del problema
----------------------

El analizador del código  WS-BPEL (`mubpel analyze`) produce sus
resultados a través de una serie de líneas con el formato siguiente:

    nombre num_operandos max_atributo

Donde:

* nombre: nombre del operador

* num_operandos: número de operandos

* max_atributo:  número  máximo de  valores  distintos para  cualquier
  operando

Utilizando esta  salida, se pueden  generar los mutantes de  todos los
posibles individuos que pueden  aparecer en GAmera, simplemente usando
tres  bucles  anidados:  uno  sobre  los operadores,  otro  sobre  los
operandos  y  un último  bucle  sobre  los  atributos. Es  el  proceso
seguido, por ejemplo, por `mubpel applyall`.

Aplicando este  procedimiento sobre LoanApprovalExtended,  se encontró
que sus  5711 individuos sólo generaban 3477  mutantes distintos. Para
hallar  este valor,  se calcularon  los  resúmenes SHA1  de todos  los
mutantes  generados,  se  ordenaron   y  se  retiraron  los  elementos
repetidos:

    cd samples/LoanApprovalExtended
    mubpel applyall LoanApprovalExtended.bpel
    sha1sum m*.bpel > sha1sums
    ../../scripts/sameFirstField.py sha1sums

_NOTA_:  el guión  Python `classify-mutants`  que simula  unas pruebas
tradicionales de mutaciones no utiliza esta orden, sino que los genera
uno  a uno  a partir  del resultado  del analizador  para  obtener los
tiempos de generación de cada mutante.

Planteamiento de la solución
----------------------------

Como  prueba  de  concepto,  `classify-mutants` se  optimizó  para  no
ejecutar de  nuevo un mutante que  tuviera un SHA1 ya  conocido y para
que registrara los mutantes repetidos en su fichero de salida. Se pudo
ahorrar  1 minuto  en el  ejemplo de  LoanApprovalRPC, por  lo  que se
propuso realizar esta mejora también en GAmera.

Sin  embargo, se  concluyó que  usar los  SHA1 en  GAmera  no evitaría
generar  esos mutantes  repetidos, por  lo  que sería  sólo un  parche
temporal. Para  retirar este  problema de raíz,  había que  revisar la
definición de los operadores.

De todas formas,  podría ser útil en un futuro usar  SHA1 en GAmera si
se  añadieran  nuevos  operadores   cuyos  conjuntos  de  mutantes  se
solaparan  con los  de los  operadores  anteriores. `classify-mutants`
conserva  la generación  y uso  de los  SHA1, para  ayudar  a detectar
problemas  como éste  más rápidamente  en el  futuro si  ocurrieran de
nuevo.

Operadores afectados
--------------------

Tras  revisar   los  operadores  de  mutación,   se  detectaron  estos
problemas:

* ASI, ISV y XTF tenían rangos variables en el atributo.

  Este aspecto  era el  que producía la  gran mayoría de  los mutantes
  repetidos.  El uso  de  rangos variables  de  atributos conlleva  un
  problema importante:  el rango efectivo  de valores distintos  en un
  operando determinado puede ser mucho más pequeño que el máximo rango
  del que informa el analizador.

  Por ejemplo, si el operador  ISV tiene 2 posibles reemplazos para un
  uso  de  una variable  y  10  para otro  uso  de  otra variable,  el
  analizador  indicaría que  el rango  máximo para  el  atributo sería
  [1,10]. Es decir, a partir  de un operando (aquí el segundo) podemos
  obtener  hasta 10 mutantes  distintos. Sin  embargo, para  el primer
  operando, sólo  existen realmente 2 mutantes distintos:  cada uno de
  ellos se repetirá 5 veces, para cubrir los 10 valores del atributo.

  En el caso de LoanApprovalExtended,  había 225 operandos para ISV, y
  el rango máximo era 10. ISV, por lo tanto, producía 2250 individuos,
  pero buena  parte de ellos  generaban mutantes repetidos. XTF  y ASI
  sufrían del mismo problema.

  La  solución a  este problema  es más  compleja que  las de  los dos
  problemas siguientes, por lo que  se le dedica su propia sección más
  abajo.

* ASI intercambiaba tanto hacia atrás como hacia delante.

  ASI es el operador  ocupado de intercambiar actividades. Juan revisó
  ASI   para  que   pudiera  intercambiar   actividades   hermanas  no
  adyacentes,  y  funcionaba  bien.  Sin  embargo,  se  listaban  como
  operandos distintos el  intercambio de la actividad A con  la B y el
  intercambio de la B con la A. Esto hacía que el número de individuos
  detectados  de ASI  fuera efectivamente  el doble  que el  número de
  mutantes distintos a producir.

  Sólo fue cuestión de revisar la  hoja XSLT del operador ASI para que
  usara  el eje  XPath `following-sibling::*`  para  únicamente buscar
  candidatos entre los hermanos que vienen a continuación del operando
  seleccionado.

* ECN producía mutantes repetidos cuando la constante era 0 o 1.

  ECN es el operador ocupado  de mutar las constantes numéricas en las
  expresiones XPath. Tiene un rango  fijo de 4 valores distintos en el
  atributo. Según  el valor del  atributo, puede generar a  partir del
  número `n` 4 mutantes que reemplazan dicho valor por otro resultante
  de restar  1, sumar 1, repetir  la última cifra o  retirar la última
  cifra. Es decir, a partir de `n` generamos  `(n - 1, n + 1, n * 10 +
  n % 10, n / 10)`, donde `/` es división entera.

  Para evitar que produjera  mutantes idénticos al original, se refinó
  ECN en su momento para que `n = 0` produjera `(-1, 1, -1, 1)` en vez
  de `(-1, 1, 0, 0)`. Sin embargo, esto generaba 40 mutantes repetidos
  en LoanApprovalExtended,  por lo que  se cambió dicho caso  para que
  produjera `(-1, 1, -10, 10)`. Se detectó un problema parecido cuando
  `n = 1`,  que producía `(0, 2, 11, 0)`,  generando otros 40 mutantes
  repetidos en  LoanApprovalExtended. Este caso  se resolvió cambiando
  ECN para que cuando `n = 1` produjera los mutantes `(0, 2, 11, -1)`.

Todos estos problemas se  han resuelto, consiguiendo no generar ningún
mutante    repetido     en    LoanApprovalExtended,    MetaSearch    y
TravelReservationService.

Retirada de rangos variables de atributos en ASI, ISV y XTF
-----------------------------------------------------------

Resolver el primer problema antes mencionado exigiría en principio que
GAmera sólo le diera a los atributos de rango variable los valores del
rango efectivo del operando en  cuestión. Es decir, si la variable del
primer operando de ISV tiene tres candidatos, GAmera sólo debería usar
los valores 1, 2  y 3 en el atributo, aunque el  rango máximo fuera de
10.

Sin embargo,  esto requeriría  cambiar todo el  formato de  salida del
analizador y GAmera  en sí, por lo que no era  viable. La mejor opción
era, por  lo tanto, no usar  el campo atributo en  los operadores ASI,
ISV y XTF. Para ello, había que redefinirlos de forma que cada posible
intercambio de actividades  (ASI), de variables (ISV) o  de nombres de
fallos (XTF)  fuera un *operando*,  en vez de una  *localización* como
hasta  ahora se  consideraba. El  problema  era cómo  guardar en  cada
operando la información necesaria para hacer la mutación.

Para entender  esta parte  de la solución,  es mejor hacer  un conciso
sobre la implementación hasta entonces. Las hojas XSLT que se ocupaban
de implementar cada  uno de los operadores ya  disponían de plantillas
llamadas `generate-operands` que  generaban secuencias planas de nodos
con  la información  necesaria  para  cada uno  de  sus operandos.  La
secuencia  se dividía  de  forma regular  en  subsecuencias con  estos
componentes:

* `bpelNode` era una referencia al nodo del árbol de la definición del
  proceso WS-BPEL que  se correspondía con el operando.  Tenía que ser
  un elemento que perteneciera al espacio de nombres de WS-BPEL 2.0.

* `ast` (opcional) era una referencia al nodo raíz del árbol abstracto
  de sintaxis de  la expresión XPath a mutar  que se encontraba dentro
  de `bpelNode`. Se trataba  del primer elemento `uca:expression` tras
  `bpelNode` en esta subsecuencia,  anterior al siguiente elemento que
  perteneciera al espacio de nombres de WS-BPEL.

* `astNode` (opcional)  era una referencia al nodo  concreto del árbol
  abstracto de sintaxis XPath con raíz en `ast` que se iba a mutar. Se
  trataba  del  primer elemento  *distinto*  de `uca:expression`  tras
  `bpelNode` en esta subsecuencia,  anterior al siguiente elemento que
  perteneciera al espacio de nombres de WS-BPEL.

Como puede  verse, `astNode`  tenía una definición  bastante relajada:
podía  meterse  cualquier elemento,  siempre  que  no perteneciera  al
espacio    de     nombres    de    WS-BPEL    y     no    fuera    una
`uca:expression`. Aprovechando  este componente, se  podría guardar la
información extra necesaria para ASI (el índice del hermano con el que
intercambiarse), ISV (el nombre de la nueva variable) y XTF (el nombre
del nuevo  fallo). Por  esta razón, `astNode`  se renombró  a `extra`,
para hacer ver que no sólo se usaba para referenciar a nodos del árbol
abstracto de  sintaxis XPath, sino para  guardar cualquier información
extra requerida por el operador.

Hubo  un   caso  que  planteó   algunas  dificultades:  ISV   no  sólo
intercambiaba referencias  a variables dentro  de actividades WS-BPEL,
sino  también  dentro de  expresiones  XPath.  En  caso de  mutar  una
referencia a  variable dentro de  un árbol XPath ya  necesitábamos los
tres  componentes  anteriores:   `bpelNode`,  `ast`  y  `extra`.  Para
resolver este  problema, se relajó  la definición de `extra`  para que
fuera   la   subsecuencia  de   todos   los   elementos  distintos   a
`uca:expression`  posteriores a `bpelNode`  y anteriores  al siguiente
elemento que perteneciera al espacio de nombres de WS-BPEL.

De  esta   forma,  se  podían  colocar  varios   valores  en  `extra`,
resolviendo el problema que planteaba el caso anterior.
