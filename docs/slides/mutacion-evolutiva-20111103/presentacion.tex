\pdfminorversion=5
\pdfobjcompresslevel=2
\documentclass[svgnames,ignoreonframetext,10pt]{beamer}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}

\mode<article>{\usepackage{fullpage}
  \setjobnamebeamerversion{mistranspa.tex}}

\mode<presentation>
{
  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
  \usetheme{Warsaw}
  \setbeamercovered{transparent}
}

\usepackage{amsmath,amssymb}
\usepackage[utf8]{inputenc}
\usepackage{colortbl}
\usepackage[spanish]{babel}
\usepackage{alltt,multicol}
\usepackage[scaled]{helvet}
\usepackage{listings}
\usepackage{array}
\usepackage{xspace}
\usepackage{graphicx}
\usepackage{rotating,multirow,fancyvrb}
\usepackage{alltt,multicol,pifont}
\usepackage{booktabs}
\usepackage{fancyvrb}
\usepackage{csquotes}
\usepackage{tikz}

\usetikzlibrary{trees,shapes.geometric}

\newcommand{\fallo}{\ding{52}}
\newcommand{\anotacion}[1]{\(\leftarrow\) \textnormal{\emph{#1}}}
\newcommand{\reduce}{\fontsize{8}{9}\selectfont}
\newcommand{\insertaimagen}{\includegraphics[width=2cm]{logo-spifm.pdf}}

\newcommand*{\proceso}[1]{\textsc{#1}}
\newcommand*{\fichero}[1]{\texttt{#1}}
\newcommand*{\elemento}[1]{\emph{#1}}
\newcommand*{\atributo}[1]{\emph{#1}}
\newcommand*{\opcion}[1]{\texttt{--#1}}
\newcommand*{\sub}[1]{\ensuremath{_\mathrm{#1}}}

\newcommand*{\biblioteca}[1]{\texttt{#1}}
\newcommand*{\programa}[1]{\textit{#1}}
\newcommand*{\clase}[1]{\textit{#1}}

\newcommand*{\CPP}{\mbox{C\hspace{-.1em}\raise.2ex\hbox{+\hspace{-.1em}+}}\xspace}
\input{prologo-dibujos}

% Prólogo de Pygments
\immediate\write18{pygmentize -f latex -O full=True all-ok-abridged.yaml | sed -n '/makeatletter/,/makeatother/p' > \jobname.out}
\input{\jobname.out}

\title[Cádiz, 3 de noviembre 2011]{Mutación evolutiva}
\author[Mutación evolutiva]{Juan José Domínguez Jiménez \\ Antonio García Domínguez}
\institute[Universidad de Cádiz]{
  Grupo de Investigación UCASE \\
  Dpto. Lenguaje y Sistemas Informáticos\\
  Universidad de Cádiz\\
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Mutación evolutiva}
\label{sec:mutacion-evolutiva}

\subsection{Motivación}
\label{sec:motivacion}

\begin{frame}
  \frametitle{Prueba de mutaciones}

  \begin{block}{Inconveniente}
    \begin{itemize}
    \item El coste computacional debido al elevado número de mutantes.
    \end{itemize}
  \end{block}
  \begin{block}{Reducción del número de mutantes}
    \begin{itemize}
    \item \emph{Mutant sampling}
    \item \emph{Selective mutation}
    \item \emph{Mutant clustering}
    \item \emph{High Order Mutation}
    \item \textbf{\emph{Evolutionary Mutation Testing}}
    \end{itemize}
  \end{block}
\end{frame}

\subsection{Descripción de la técnica}
\label{sec:descripcion-de-la}

\begin{frame}
  \frametitle{Mutación evolutiva}

  \begin{block}{Características}
    \begin{itemize}
    \item Genera un subconjunto de todos los posibles mutantes.
    \item Usa \structure{todos} los operadores disponibles para el lenguaje.
    \item Emplea un algoritmo evolutivo para la generación de mutantes que se
      encarga de generar el subconjunto de mutantes más idóneo.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Mutación evolutiva (cont.)}
  \begin{block}{Tipos de mutantes}
    \begin{description}[Mutante muerto]
    \item[Mutante muerto] Su salida es diferente respecto al programa
      original en al menos un caso de prueba.
      \begin{description}[Pot.\ equivalente]
      \item[Difícil de matar] Muere con un caso de prueba que sólo lo
        mata a él.
      \end{description}
    \item[Mutante vivo] ningún caso de prueba es capaz de
      diferenciarlo del programa original.
      \begin{description}[Pot.\ equivalente]
      \item[Equivalente] la salida del mutante y la del programa
        original es siempre la misma.
      \item[Pot.\ equivalente] el conjunto de casos de prueba no es
        suficiente para detectarlo.
      \end{description}
    \item[Mutante fuerte] Difícil de matar o potencialmente
      equivalente. Interesantes para generar pruebas posteriormente.
    \end{description}
  \end{block}
\end{frame}

\subsection{Aplicación: algoritmos genéticos y WS-BPEL}

\begin{frame}
  \frametitle{GAmera: generador de mutantes para WS-BPEL}
  \begin{center}
    \includegraphics[width=\textwidth]{esquema-bloques-sistema-v2.pdf}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Algoritmos genéticos}
  \let\emph=\structure

  \begin{block}{Nociones básicas}
    \begin{itemize}\small
    \item Los AG trabajan con un conjunto de soluciones, que se
      denominan (\emph{individuos}), y al conjunto se llama
      \emph{población}.
    \item Cada individuo tiene una \emph{aptitud} que representa la
      calidad de esa solución.
    \item Los AG realizan un proceso iterativo: cada iteración es una \emph{generación}.
    \item Usan dos tipos de operadores: selección y reproducción.
    \item \emph{Operadores de selección} seleccionan individuos en la
      población para la reproducción, de forma proporcional o no a la
      aptitud.
    \item \emph{Operadores de reproducción} generan nuevos individuos
      mediante cruces y mutaciones.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Representación de un individuo}
  \framesubtitle{Cada individuo codifica una mutación del programa original}

  \begin{center}
    \begin{tikzpicture}[every node/.style={draw=black,thick,text height=1em,text depth=.4em}]
      \node[draw] (op) {Operador};
      \node[anchor=west] (ins) at (op.east) {Instrucción};
      \node[anchor=west] at (ins.east) {Atributo};
    \end{tikzpicture}
  \end{center}

  \begin{block}{Descripción de cada parte}
    \begin{description}
    \item[Operador] identifica el operador de mutación a aplicar.
    \item[Localidad] representa la localidad donde aplicar el
      operador.
    \item[Atributo] representa el nuevo valor que resultará al aplicar
      el operador de mutación.
    \end{description}
  \end{block}
\end{frame}

\begin{frame}[containsverbatim]{Conversión de un individuo a un mutante}

  \begin{exampleblock}{Individuo (ERR, 2, 3)}
    \begin{multicols}{2}
      \reduce
      \noindent \textbf{Programa Original}
      \begin{alltt}\reduce
        <if name="discount">
        <condition>
        $buy \&gt; 5000
        </condition>
        <invoke name="10off" \textrm{\ldots} />
        <elseif>
        <condition>
        $buy \textcolor{blue}{\textbf{\&gt;}} 2500
        </condition>
        <invoke name="5off" \textrm{\ldots} />
        </elseif>
        <else>
        <reply name="nooff" \textrm{\ldots} />
        </else>
        </if>
      \end{alltt}

      \noindent \textbf{Mutante}
      \begin{alltt}\reduce
        <if name="discount">
        <condition>
        \$buy \&gt; 5000
        </condition>
        <invoke name="10off" \textrm{\ldots} />
        <elseif>
        <condition>
        \$buy \textcolor{blue}{\textbf{=}} 2500
        </condition>
        <invoke name="5off" \textrm{\ldots} />
        </elseif>
        <else>
        <reply name="nooff" \textrm{\ldots} />
        </else>
        </if>
      \end{alltt}
    \end{multicols}
  \end{exampleblock}
\end{frame}

\begin{frame}[containsverbatim]{Operadores de mutación y atributos}

  \begin{center}
    \begin{tabular}{|c|c|p{1cm}r|}
      \hline
      \textbf{Operador} & \textbf{Valor} & \multicolumn{2}{c|}{\textbf{Max. Valor de
          Atributo}} \\
      \hline
      ISV & 1 & \multicolumn{2}{l|}{ \hspace{0,25cm}N} \\
      EAA & 2 & \hspace{0,25cm}5  & \verb=+=, \verb=-=, \verb=*=, \verb=div=, \verb=mod= \\
      EEU & 3 & \multicolumn{2}{l|}{ \hspace{0,25cm}1} \\
      ERR & 4 &  \hspace{0,25cm}6 & \verb=<=, \verb=>=, \verb|>=|, \verb|<=|, \verb|=|, \verb|!=| \\
      ELL & 5 &  \hspace{0,25cm}2 & \verb|and|, \verb|or| \\
      ECC & 6 &  \hspace{0,25cm}2 & \verb|/|, \verb|//| \\
      ECN & 7 &  \hspace{0,25cm}4 & +1, -1, añadir, quitar \\
      EMD & 8 &  \hspace{0,25cm}2 &0, mitad\\
      EMF & 9 &  \hspace{0,25cm}2 &0, mitad \\
      ACI & 10 & \multicolumn{2}{l|}{ \hspace{0,25cm}1} \\
      AFP & 11 & \multicolumn{2}{l|}{ \hspace{0,25cm}1} \\
      ASF & 12 & \multicolumn{2}{l|}{ \hspace{0,25cm}1} \\
      AIS & 13 & \multicolumn{2}{l|}{ \hspace{0,25cm}1} \\
      \ldots &\ldots &\ldots &\ldots \\
      \hline
    \end{tabular}
  \end{center}

\end{frame}

\begin{frame}{Ejecución de los mutantes}

  \begin{block}{}
    \begin{itemize}
    \item Para evaluar la aptitud de cada individuo, éste es ejecutado frente a un
      conjunto de casos de prueba.
    \item La aptitud no se evalúa individualmente, sino que se evalúa teniendo en
      cuenta a la población.
    \end{itemize}
  \end{block}

  \begin{block}{Matriz de ejecución}
    \begin{equation*}
      ( m_{ij} )_{M \times T} =
      \begin{pmatrix}
        0  &  0  &  1  & \dots & 0   \\
        1  &  0  &  0  & \dots & 0   \\
        \hdotsfor{5} \\
        0  &  0  &  0  & \dots & 0 \\
        2  &  2  &  2  & \dots & 2 \\
      \end{pmatrix}
    \end{equation*}
  \end{block}
\end{frame}

\begin{frame}{Aptitud del individuo}
  \begin{block}{Aptitud}
    \begin{itemize}
    \item Tendrá en cuenta si el mutante está vivo o muerto, y cuántos mutantes
      mueren con el mismo caso de prueba que lo mata:
      \begin{equation*}
        \text{Aptitud}(I) = M \times T - \overset{T}{\underset{j=1}{\sum}} \left( m_{Ij}
          \times \overset{M}{\underset{i=1}{\sum}} m_{ij} \right)
      \end{equation*}

    \item Intenta penalizar a grupos de mutantes que son muertos por el mismo caso
      de prueba.

    \item La función favorece los mutantes fuertes: los mutantes potencialmente
      equivalentes y los mutantes difíciles de matar.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Aptitud del individuo (cont.)}
  \begin{block}{Un ejemplo}
    \begin{equation*}
      \begin{pmatrix}
        & T1 & T2  & T3 & T4 \\
        M1 & 0  &  1  &  1  & 0   \\
        M2 & 1  &  1  &  0  & 0   \\
        M3 & 0  &  0  &  0  & 1 \\
        M4 & 1  &  1  &  1  & 0 \\
      \end{pmatrix}
    \end{equation*}

    M1 = 4*4 - (0+3+2+0) = 11 \\
    M2 = 4*4 - (2+3+0+0) = 11 \\
    M3 = 4*4 - (0+0+0+1) = 13 \\
    M4 = 4*4 - (2+3+2+0) = 9
  \end{block}
\end{frame}

\section{Herramientas}
\label{sec:herramientas}

\begin{frame}{Divisiones a nivel de código}

  \vspace{1em}
  \begin{center}
    \includegraphics[width=\textwidth]{esquema-bloques-sistema-v2.pdf}
  \end{center}
  \begin{tikzpicture}[overlay,mubpel/.style={rounded corners,draw=DarkGreen,fill=green!20,fill opacity=0.4},gamera/.style={rounded corners,draw=red,fill=red!20,fill opacity=0.4}]
    \draw[mubpel]
      (-.5em,19em)
      -- node[above=-.5em,color=DarkGreen,draw=DarkGreen,fill=green!20!white,opacity=1,text depth=.1em] {MuBPEL (todo lo específico a WS-BPEL)} ++(33.5em,0em)
      -- ++(0em,-18em)
      -- ++(-16em,0em)
      -- ++(0em,9.5em)
      -- ++(7em,0em)
      -- ++(0em,5.5em)
      -- ++(-24.5em,0)
      -- cycle;

    \draw[gamera] (-.5em,16em) rectangle node[below=2.5em,color=DarkRed,draw=DarkRed,fill=red!20!white,opacity=1,text depth=.1em] {GAmera (algoritmo genético)} +(24.5em,-5.5em);
  \end{tikzpicture}

\end{frame}

\subsection{GAmera 2.0}
\label{sec:gamera-2.0}

\begin{frame}{GAmera 2.0}
  \begin{block}{Funcionalidades}
    \begin{itemize}
    \item Implementa el algoritmo genético (AG) de forma genérica
    \item El soporte del lenguaje de programación usado está por
      separado
    \item Muchos componentes del AG son intercambiables y
      configurables
    \item A fecha de hoy, se centra en la generación de mutantes
      \enquote{fuertes} (vivos no equivalentes + muertos por casos muy
      específicos)
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{GAmera 2.0: diseño}

  \begin{block}{Algunos detalles}
    \begin{itemize}
    \item Escrito en Java SE 6, usando su aritmética de precisión arbitraria
    \item Núcleo con algoritmo genético + componentes intercambiables
    \item Un fichero YAML selecciona y configura los componentes
    \item Licencia libre: Apache Software License 2.0
    \end{itemize}
  \end{block}

  \begin{block}{Reparto del código en módulos}
    \begin{description}[gamera2-core]
    \item[gamera2-api]  API estándares para los componentes de GAmera
    \item[gamera2-core] Núcleo que implementa el algoritmo genético
    \item[gamera2-bpel] Integra MuBPEL en GAmera
    \item[gamera2-gui]  Futura interfaz gráfica para GAmera (limitada a mut.\ tradicional actualmente)
    \end{description}
  \end{block}

  % diagrama

\end{frame}

\begin{frame}[plain]{Ficheros de configuración}
  \framesubtitle{Formato definido a partir de las clases Java, usando introspección}
  \immediate\write18{pygmentize -f latex all-ok-abridged.yaml > \jobname.pygout}
  \small\input{\jobname.pygout}
\end{frame}

\subsection{MuBPEL}
\label{sec:mubpel}

\begin{frame}{MuBPEL}
  \begin{block}{Funcionalidades}
    \begin{itemize}
    \item Realiza el análisis y la conversión individuo $\rightarrow$
      mutante
    \item Ejecuta los mutantes y los compara con el programa original,
      repartiendo el trabajo entre varias CPU
    \item Normaliza programas para facilitar comparaciones con \texttt{*diff}
    \end{itemize}
  \end{block}

  \begin{block}{Diseño}
    \begin{itemize}
    \item Núcleo en Java SE 6 y operadores de mutación en XSLT 2.0
    \item Usable desde terminal o mediante interfaz gráfica
    \item Licencia libre: Apache Software License 2.0
    \item Se distribuye junto con GAmera
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Jerarquía de operadores}

  \begin{center}
    \begin{tikzpicture}[
      grow cyclic,
      level 1/.style={level distance=6em,sibling angle=32.72},
      level 2/.style={level distance=8em,sibling angle=16},
      op/.style={shape=ellipse,inner sep=.1em},
    ]
      \node[color=DarkRed,draw,fill=red!20] {op}
        child[color=DarkGreen] {node[draw,fill=DarkGreen!20] {delete\_op}
          child {node[op] {AEL}}
          child {node[op] {AIE}}
          child {node[op] {AJC}}
          child {node[op] {APA}}
          child {node[op] {APM}}
          child {node[op] {XEE}}
          child {node[op]{XMC}}
          child {node[op]{XMF}}
          child {node[op]{XMT}}
        }
        child {node[op]{ACI}}
        child {node[op]{AFP}}
        child {node[op]{AIS}}
        child {node[op]{ASF}}
        child[xshift=4em,color=structure] {node[draw,fill=structure!20] {xpath\_op}
          child {node[op]{CCO}}
          child {node[op]{CDC}}
          child {node[op]{CDE}}
          child {node[op]{EAA}}
          child {node[op]{EAN}}
          child {node[op]{EAP}}
          child {node[op]{ECC}}
          child {node[op]{ECN}}
          child {node[op]{EEU}}
          child {node[op]{EIN}}
          child {node[op]{EIU}}
          child {node[op]{ELL}}
          child {node[op]{EMD}}
          child {node[op]{EMF}}
          child {node[op]{ERR}}
          child {node[op]{ISV}}
        }
        child {node[op]{ASI}}
        child {node[op]{AWR}}
        child {node[op]{CFA}}
        child {node[op]{XER}}
        child {node[op]{XTF}}
        ;
    \end{tikzpicture}
  \end{center}

\end{frame}

\subsection{Soporte para otros lenguajes}
\label{sec:extension-de-gamera2}

\begin{frame}{¿Cómo extender GAmera a otros lenguajes?}
  \begin{block}{Funcionalidad a implementar}
    \begin{description}
    \item[Analizador] Indica operadores aplicables, en cuántas
      localidades se puede aplicar cada uno y el valor máximo del atributo
    \item[Conversor] A partir de un individuo, transforma el programa
      original al mutante correspondiente
    \item[Ejecutor] Ejecuta el mutante, compara sus salidas con las
      del programa original y da la fila de la matriz de ejecución
    \end{description}  
  \end{block}

  \begin{block}{A nivel de código}
    \begin{enumerate}
    \item Implementar la interfaz \emph{GAExecutor} definida en \emph{gamera2-api}
    \item Añadir el \texttt{.jar} al \emph{classpath} de GAmera
    \item Escribir el fichero YAML para que utilice nuestra implementación
    \end{enumerate}
  \end{block}

\end{frame}

\end{document}
