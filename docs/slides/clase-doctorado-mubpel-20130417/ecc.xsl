<?xml version="1.0" encoding="UTF-8" ?>
<!--
   ECC WS-BPEL mutation operator

   Switches between "/" (direct children) and "//" (descendants)
   XPath path operators.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/xpath-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <xsl:call-template name="generate-xpath-operands">
      <xsl:with-param name="selectedNodes" select="'uca:children | uca:alldesc'"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="uca:children" mode="mutate-ast-node">
    <uca:alldesc>
      <xsl:copy-of select="@*|node()"/>
    </uca:alldesc>
  </xsl:template>

  <xsl:template match="uca:alldesc" mode="mutate-ast-node">
    <uca:children>
      <xsl:copy-of select="@*|node()"/>
    </uca:children>
  </xsl:template>

</xsl:stylesheet>
