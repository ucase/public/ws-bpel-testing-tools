<?xml version="1.0" encoding="UTF-8"?>
<!--
   ACI (Activities: Create Instance) WS-BPEL mutation operator

   Changes the createInstance attribute in the <receive> activities from
   "yes" to "no". This operator will not report any operands unless there is
   more than one receive with its createInstance attribute set to "yes".

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable">

  <xsl:import href="es/uca/webservices/mutants/operators/op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <xsl:variable name="candidates" as="node()*">
      <xsl:sequence select="//(bpel:receive | bpel:pick)[@createInstance='yes']
      [ancestor-or-self::*[parent::bpel:sequence][position() > 1]]"/>
    </xsl:variable>
    <xsl:if test="count($candidates) > 1">
      <xsl:sequence select="$candidates"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="bpel:receive" mode="mutate-node">
    <xsl:element name="receive" namespace="http://docs.oasis-open.org/wsbpel/2.0/process/executable">
      <xsl:copy-of select="@*[local-name(.) != 'createInstance']"/>
      <xsl:attribute name="createInstance">no</xsl:attribute>
      <xsl:copy-of select="node()"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="bpel:pick" mode="mutate-node">
    <xsl:element name="pick" namespace="http://docs.oasis-open.org/wsbpel/2.0/process/executable">
      <xsl:copy-of select="@*[local-name(.) != 'createInstance']"/>
      <xsl:attribute name="createInstance">no</xsl:attribute>
      <xsl:copy-of select="node()"/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
