Takuan changelog
================

Staged for next version
------------------------

* _Changed_: do not reuse types.xml.

Version 1.0 (2010/04/12)
------------------------

* First stable release.
