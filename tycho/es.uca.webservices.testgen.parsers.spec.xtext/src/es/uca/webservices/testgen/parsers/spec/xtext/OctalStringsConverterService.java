package es.uca.webservices.testgen.parsers.spec.xtext;

import org.eclipse.xtext.common.services.DefaultTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.STRINGValueConverter;
import org.eclipse.xtext.nodemodel.INode;

import com.google.inject.Singleton;

@Singleton
public class OctalStringsConverterService extends DefaultTerminalConverters {

	public static class OctalStringsConverter extends STRINGValueConverter {
		@Override
		public String toValue(String string, INode node) throws ValueConverterException {
			if (string.contains("\\0")) {
				string = string.replace("\"", "");
				string = string.replace("\\0", "ñ");
				String[] strings = string.split("ñ");
				String octalString = "";
				for (String s : strings) {
					if (!s.isEmpty()) {
						octalString = octalString
								+ (char) Integer.parseInt(s, 8);
					}
				}
				string = "\"" + octalString + "\"";
			}
			return super.toValue(string, node);
		}
	}

	@Override
	@ValueConverter(rule = "STRING")
	public IValueConverter<String> STRING() {
		return new OctalStringsConverter();
	}
}
