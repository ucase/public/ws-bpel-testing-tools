
package es.uca.webservices.testgen.parsers.spec.xtext;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class TestSpecStandaloneSetup extends TestSpecStandaloneSetupGenerated{

	public static void doSetup() {
		new TestSpecStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

