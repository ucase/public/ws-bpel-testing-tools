#!/usr/bin/env python
"""Naive invariant comparator.

Takes two .out output files from Daikon and compares the sets of
invariants produced for each program point, using simply their text
representations. It doesn't make the least effort to try and parse
them: careful with the results reported."""

import re


class DiffLine(object):
    """Represents a line from a diff output.

    In addition to being usable as a normal string, we can ask
    for what file it came from using its origin attribute."""

    LEFT, RIGHT = range(2)

    def __init__(self, value, origin):
        self.value = value
        self.origin = origin

    def __str__(self):
        return "%s %s" % ("<" if self.origin == self.LEFT else ">",
                          self.value)


def load_daikon_out(f):
    """Loads a Daikon .out invariant list.

    The data structure used is a hash on the program point name,
    followed by a list of all the lines with the invariants reported.
    Initial and final messages are stripped."""

    contents = f.read()
    splitter = re.compile("\s*^=+$\s*", re.M)
    sections = splitter.split(contents)
    sections = [s.split("\n") for s in sections]

    # Strip the prologue
    del sections[0]
    # Strip the exit lines in the last section
    sections[-1] = [l for l in sections[-1] if l != 'Exiting Daikon.']

    results = {}
    for section in sections:
        point, invariants = section[0], section[1:]
        results[point] = invariants

    return results


def compare(f_inv1, f_inv2, combine_points=(lambda l, r: l | r)):
    """Compares two Daikon .out invariant files and reports differences.

    These differences simply compare the sets of lines in each version
    of the same program point, if it exists in both places. Otherwise,
    the program point that exists is used as-is.

    By default, points which are in the first invariant list but not
    in the second or viceversa are considered as points in which all
    lines belong to the left part or to the right part. This can be
    modified by passing a different point combination function through
    the combine_points argument."""

    d_inv1 = load_daikon_out(f_inv1)
    d_inv2 = load_daikon_out(f_inv2)
    results = {}

    points_i1 = set(d_inv1.keys())
    points_i2 = set(d_inv2.keys())

    for point, lines_i1, lines_i2 in [(k,
                                       set(d_inv1.get(k, [])),
                                       set(d_inv2.get(k, [])))
                                      for k in combine_points(points_i1, points_i2)]:

        results[point] = \
            ([DiffLine(l, DiffLine.LEFT) for l in lines_i1 - lines_i2] +
             [DiffLine(l, DiffLine.RIGHT) for l in lines_i2 - lines_i1])

    return results


def print_diff_results(results):
    for point, diffs in results.iteritems():
        if len(diffs) == 0:
            continue

        print "=============================="
        print point
        for diff in diffs:
            print diff

if __name__ == "__main__":
    import sys

    if len(sys.argv) < 2:

        print "Usage:", sys.argv[0], "[--only-left-points|--only-right-points] (inv1) (inv2)"
        sys.exit(1)

    else:

        # Parse additional arguments
        extra_compare_args = {}
        if '--only-left-points' in sys.argv:
            sys.argv.remove('--only-left-points')
            extra_compare_args = {'combine_points': (lambda a, b: a)}
        if '--only-right-points' in sys.argv:
            sys.argv.remove('--only-right-points')
            extra_compare_args = {'combine_points': (lambda a, b: b)}

        # Open both invariant lists
        f_inv1 = open(sys.argv[1])
        f_inv2 = open(sys.argv[2])

        results = compare(f_inv1, f_inv2, **extra_compare_args)
        print "<<< %s" % f_inv1.name
        print ">>> %s" % f_inv2.name
        print_diff_results(results)
