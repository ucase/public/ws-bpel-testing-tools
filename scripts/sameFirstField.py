#!/usr/bin/python3.1

# Script for listing together all lines whose first whitespace-separated field
# is the same. This script is useful, for instance, for listing files with the
# same SHA1 checksums:
#
# sha1sum files > sha1sums
# sameFirstField.py sha1sums
#
# Copyright (C) 2010 Antonio García-Domínguez
# Available under the terms of the GPLv3 license

def print_repeated_lines(fich):
  lineas = None
  with open(fich) as f:
    lineas = f.readlines()
  lineas.sort()

  suma_ant, nom_ant = '', ''
  for l in lineas:
    suma, nombre = l.split(None, 1)
    nombre = nombre.strip()
    if suma == suma_ant:
      print('Repeated {0}: {1} (was {2})'.format(suma, nombre, nombre_ant))
    else:
      suma_ant, nombre_ant = suma, nombre

if __name__ == "__main__":
  import sys
  if len(sys.argv) != 2:
    print("Usage: {0} (file)".format(sys.argv[0]), file=sys.stderr)
    sys.exit(1)
  print_repeated_lines(sys.argv[1])
