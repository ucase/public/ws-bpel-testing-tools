#!/bin/sh

DAIKON=/opt/daikon/java
DAIKON_CLASS=daikon.Daikon
DAIKON_OPTS_FILE=/opt/daikon/daikon.options

export CLASSPATH=${CLASSPATH}:${DAIKON}

java -Xmx512m -XX:MaxPermSize=256m -Xss1m -server -cp $DAIKON $DAIKON_CLASS --config "$DAIKON_OPTS_FILE" $*
