#!/bin/sh

egrep '[^/]condition|[^/]from' $* | perl -pe 's/.*>(.*?)<\/.*/\1/' | sort | uniq

