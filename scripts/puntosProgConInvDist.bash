#!/bin/bash

# Script que cuenta cuantas líneas no seguidas tienen los caracteres :::

if [ ! -f $1 ]
then
 echo "El primer y unico parametro debe ser un fichero diff.txt o diffU.txt"
 exit
fi

if [ $# -ne 1 ]
then
 echo "El primer y unico parametro debe ser un fichero diff.txt o diffU.txt"
 exit
fi

TMP=/tmp/$(basename $0)-$$

cat $1 | grep -v SUU | grep -v SJJ | grep -v SJU | grep -v SUJ > $TMP

ACUM=0
CONT=0
TPP=0
VOLCAR=0

while read L
do
 
 echo "$L" | grep ::: > /dev/null && CONT=$((CONT + 1)) && WC=$((WC+1))


 echo "$L" | grep -v :::  > /dev/null && VOLCAR=1

 if [ $VOLCAR -eq 1 ]
 then
  if [ $CONT -ne 0 ]
  then
   ACUM=$((ACUM+CONT-1))
   CONT=0
  fi
 VOLCAR=0
 fi

done < $TMP

if [ $CONT -ne 0 ]
then
 ACUM=$((ACUM+CONT))
fi

rm $TMP

WC=$((WC-ACUM))

echo "$1;$WC"

