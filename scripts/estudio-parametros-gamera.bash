#!/bin/bash

declare fichero="gamera-"
declare extension=".conf"
declare CONF=""

# Template variables
declare -i size=0
declare -i gval=0
declare pcval=0
declare pmval=0
declare pnval=0
declare -i seed=0
declare -i generations=0

# Constants which configure the value ranges to be tested
declare -i MIN_SEED=1
declare -i MAX_SEED=30
declare -ia G=(1)
declare -ia PC=(0.9 0.8 0.7 0.6)
declare -ia PM=(0.1 0.2 0.3 0.4)
declare -ia PN=(0.1 0.2 0.3)

#
# Generates the next config file to be used, placing its name in the
# CONF variable.
#
generate_conf() {
    generations=$size*$gval

    params="$size-$generations-$pcval-$pnval-0.9-seed$seed"
    CONF="${fichero}${params}${extension}"

    cat >"$CONF" <<EOF
TAMPOBLACION $size
NUMGENERACIONES $generations
PCRUCE $pcval
PMUTACION $pmval
PNUEVO $pnval
PSUSTITUIR 1
NUMTEST ${CASES}
DIFERENTES 0
PMUTANTES 0.9
DIRECTORIOSALIDA 1 SALIDA-${params}
ANALIZADOR 0 analisis.txt
BPELORIGINAL 1 ${BPEL}
BPELOUT 1 ${BPEL}.out
TESTSUITE 1 ${BPTS}
SALIDAGAMERA 1 mutantes-${params}.out
ESTADISTICAS 1 estadisticas-${params}.dat
BDMUTANTES 1 mutantes-generados-${params}.dat
MUTANTESCALIDAD 1 mutantes-calidad.txt
EOF

    log "Generado ${CONF}"
}

log() {
    echo "$(date) -- $*"
}

run_conf() {
    cp "$CONF" gamera.conf


    # time(1) puede sacar muchos tiempos, pero se mezclan con el
    # stdout de GAmera, así que también lo calculamos con una
    # diferencia de tiempos
    PREV_TIME=$(date +'%s')
    /usr/bin/time gamera seed "$seed"
    log "Tiempo de $CONF: $(($(date +'%s') - $PREV_TIME))" | tee -a tiempos.log
}

## CUERPO PRINCIPAL

if [ "$#" -ne 4 ]; then
    echo "Uso: $0 (.bpts) (.bpel) (tamaño población) (casos en .bpts)"
    exit 1
fi
BPTS=$1
BPEL=$2
declare -ia TP=($3 $(($3 * 2)) $(($3 * 4)))
CASES=$4

log "Reiniciando ActiveBPEL"
ActiveBPEL.sh restart

log "Ejecutando GAmera"
mubpel run "$BPTS" "$BPEL" > "$BPEL".out

for size in "${TP[@]}"; do
    for gval in "${G[@]}"; do
        for (( iProb=0; iProb < ${#PM[@]}; iProb++ )); do
            pcval="${PC[$iProb]}"
            pmval="${PM[$iProb]}"
            for pnval in "${PN[@]}"; do
                for seed in $(seq $MIN_SEED $MAX_SEED); do
                    generate_conf

                    # Only run tests which have not been run before
                    if ! fgrep -q "$CONF" tiempos.log; then
                        run_conf \
                            > >(tee "$CONF-stdout.log") \
                           2> >(tee "$CONF-stderr.log")
                    fi
                done
            done
        done
    done
done
