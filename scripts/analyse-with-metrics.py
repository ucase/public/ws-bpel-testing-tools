#!/usr/bin/env python

from __future__ import with_statement
import subprocess
import time
import threading


class CommandWatcher(threading.Thread):
    """Reruns a command every certain time and collects its outputs.

    All output lines are sent to the processOutput method, which isn't
    defined by default: you'll have to inherit from this class to make
    it actually do something with the output."""

    def __init__(self, command, interval=1, *args, **kwargs):
        super(CommandWatcher, self).__init__(*args, **kwargs)
        self.command = command
        self.interval = interval

    def run(self):
        self.running = True
        while self.running:
            watched_process = subprocess.Popen(self.command,
                                               shell=True,
                                               stdout=subprocess.PIPE)
            self.processOutput(time.time(),
                               watched_process.stdout.readlines())
            time.sleep(self.interval)


class PerlScriptWatcher(CommandWatcher):

    def __init__(self, *args, **kwargs):
        super(PerlScriptWatcher, self).__init__(
            command="ps -C perl --no-headers -o rss,vsize",
            *args, **kwargs)
        self.v_rss = []
        self.v_vsz = []

    def processOutput(self, time, lines):
        split_lines = [l.split() for l in lines]
        total_rss = sum([int(l[0]) for l in split_lines])
        total_vsz = sum([int(l[1]) for l in split_lines])

        def add_if_not_same_as_last(v, x):
            if len(v) > 0 and v[-1] != x or len(v) == 0:
                v.append((time, x))

        add_if_not_same_as_last(self.v_rss, total_rss)
        add_if_not_same_as_last(self.v_vsz, total_vsz)


class JStatTotalUsageWatcher(CommandWatcher):
    def __init__(self, lvmid, *args, **kwargs):
        super(JStatTotalUsageWatcher, self).__init__(
            command="jstat -gc %d" % lvmid,
            *args, **kwargs)

        self.lvmid = lvmid
        self.v_totalu = []

    def processOutput(self, time, lines):
        if len(lines) < 2: return

        fields = [float(x.replace(',', '.')) for x in lines[1].split()]
        s0u, s1u, eu, ou, pu = \
            fields[2], fields[3], fields[5], fields[7], fields[9]

        total_utilization = s0u + s1u + eu + ou + pu
        self.v_totalu.append((time, total_utilization))


class JavaMemoryWatcher(threading.Thread):

    class InterruptedException(Exception):
        pass

    def __init__(self, java_program, interval=0.25, *args, **kwargs):
        super(JavaMemoryWatcher, self).__init__(*args, **kwargs)
        self.java_program = java_program
        self.interval = interval

    def _set_running(self, value):
        self._running = value
        if getattr(self, 'jstat_watcher', None):
            self.jstat_watcher.running = value

    def _get_running(self):
        return self._running

    running = property(_get_running, _set_running)

    def waitForLVMID(self, program):
        #print "Waiting to get LVMID of '%s'" % program
        while self.running:
            jps_output = subprocess.Popen("jps",
                                          shell=True,
                                          stdout=subprocess.PIPE)\
                                          .communicate()[0].split("\n")

            for lvmid in [l.split()[0]
                          for l in jps_output
                          if l.count(program) > 0]:
                try:
                    return int(lvmid)
                except:
                    pass

            time.sleep(0.25)

        raise JavaMemoryWatcher.InterruptedException

    def run(self):
        self.running = True

        try:
            self.lvmid = self.waitForLVMID(self.java_program)
            #print "Got LVMID #%d" % self.lvmid

            self.jstat_watcher = JStatTotalUsageWatcher(self.lvmid, interval=self.interval)
            self.jstat_watcher.start()
            self.jstat_watcher.join()

            self.v_totalu = self.jstat_watcher.v_totalu

        except JavaMemoryWatcher.InterruptedException:
            print "Execution was interrupted"
            self.v_totalu = [(0,0)]

        finally:
            self.running = False


def main():
    """Main execution function.

    It follows these steps:
    * Starts the watcher for the Perl scripts
    * Runs the analysis step using the existing Ant buildfile
    * Stops the watcher and presents the results"""

    # Run the analysis step while monitoring
    watchThread = PerlScriptWatcher(interval=0.25)
    jwatchThread = JavaMemoryWatcher("Daikon")
    watchThread.start()
    jwatchThread.start()
    subprocess.call(["ant", "analyze"])
    watchThread.running = False
    jwatchThread.running = False
    watchThread.join()
    jwatchThread.join()

    # Print results
    print "Elapsed time:   %4.2f seconds" % (watchThread.v_rss[-1][0] - watchThread.v_rss[0][0])
    print "Maximum Perl RSS:     %6d KiB" % max(x[1] for x in watchThread.v_rss)
    print "Maximum Perl VSZ:     %6d KiB" % max(x[1] for x in watchThread.v_vsz)
    print "Maximum Daikon usage: %6d KiB" % max(x[1] for x in jwatchThread.v_totalu)

    for fname, values in [('rss.csv', watchThread.v_rss),
                          ('vsz.csv', watchThread.v_vsz),
                          ('jstat.csv', jwatchThread.v_totalu)]:
        with open(fname, "w") as f:
            f.writelines(["%f,%d\n" % (time, val)
                          for time, val in values])

if __name__ == "__main__":
    main()
