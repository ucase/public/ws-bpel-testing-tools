#!/bin/bash

if [ "$#" -lt 2 -o "$#" -gt 3 ]; then
    echo "Usage: $0 bpts bpel [none|full]"
    exit 1
fi

### FUNCTIONS

function keep_removing_old_logs() {
    while true; do
        find /tmp -name "*.log" -mmin +2 -execdir rm '{}' \; &>/dev/null
        sleep 1s
    done
}

###

# Parse the arguments
BPTS=$1
OBPEL=$2
LOGLEVEL=full
if [ "$#" == 3 ]; then
    LOGLEVEL=$3
fi

# Basic configuration
TIEMPOS=times.txt
OUT=salida.xml

# Start background task for cleaning up old big files
keep_removing_old_logs &

# Clean up old mutants and times
rm -f "$TIEMPOS"
rm -f m*.bpel
mubpel applyall "$OBPEL"
mubpel run -l none "$BPTS" "$OBPEL" > "$OUT"

# Try comparing all mutants with various combinations of parameters
PREFIX="/usr/bin/time -ao $TIEMPOS mubpel compare -l $LOGLEVEL"
SUFFIX="$BPTS $OBPEL $OUT m*.bpel"

declare -ia KEEPGOING=(0 1)
declare -ia ENGINES=(2 3)
declare -ia DECKSIZES=(128 32 4 0)

# Run the tests while logging the execution times
for kg in "${KEEPGOING[@]}"; do
    FLAGS=""
    if [ "$kg" == 1 ]; then
        FLAGS="-k"
    fi
    NAME="k$kg-sequential.txt"
    CMD="$PREFIX $FLAGS $SUFFIX"
    echo -e "---\n$NAME : '$CMD'" >> "$TIEMPOS"
    $CMD | tee "$NAME"

    for n in "${ENGINES[@]}"; do
        for d in "${DECKSIZES[@]}"; do
            NAME="k$kg-p$n-d$d.txt"
            CMD="$PREFIX $FLAGS -p "$n" -d "$d" $SUFFIX"
            echo -e "---\n$NAME : '$CMD'" >> "$TIEMPOS"
            $CMD | tee "$NAME"
        done
    done
done

# Kill the background task for cleaning up old logs
kill %1
