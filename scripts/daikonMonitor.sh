#!/bin/sh

DAIKON=${HOME}/src/daikon/java
DAIKON_CLASS=daikon.Daikon
DAIKON_OPTS_FILE=${HOME}/bin/daikon.options
JCONSOLE=$(which jconsole)

export CLASSPATH=${CLASSPATH}:${DAIKON}

# Por lo general
# java -server -Xms512m -Xmx512m -Xss2m -cp $DAIKON $DAIKON_CLASS --config "$DAIKON_OPTS_FILE" $*

# Para el ejemplo con index-flattening y sin ninguna optimización
# java -server -Xms800m -Xmx800m -verbose:gc -cp $DAIKON $DAIKON_CLASS --config "$DAIKON_OPTS_FILE" $*

# Para monitorización
java -server -Xmx800m -Xss2m -cp $DAIKON $DAIKON_CLASS --config "$DAIKON_OPTS_FILE" $* &

# Esperamos a que la JVM de Daikon aparezca en jps
# y sacamos su LVMID para jconsole
while ! (jps | grep Daikon); do sleep 0.05s; done
DAIKON_LVMID=$(jps | grep Daikon | cut -f1 --delim=' ' | perl -pe 's/\s+/ /g;')

# Ejecutamos jconsole
echo "export DISPLAY=${DISPLAY}
${JCONSOLE} -interval=1 ${DAIKON_LVMID}" | at now

echo "Waiting for Daikon on LVMID ${DAIKON_LVMID} to complete..." >&2
