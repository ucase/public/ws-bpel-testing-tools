#!/bin/bash

# Exit upon error
set -e

if [ "$#" -lt 2 ]; then
    echo "Usage: $0 (original .bpel) (mutants...)"
    exit 1
elif [ ! -f build.xml ]; then
    echo "This script must be run from the main directory of the original"
    echo "WS-BPEL composition. The build.xml Ant script must be there"
    echo "as well."
    exit 1
fi

ORIGINAL_BPEL=$1
shift

# Set up path for helper script (should be in the same directory, or
# in a directory in the PATH)
export PATH=$PATH:$(readlink -f "$0"/..)

## CORE COUNT DETECTION ########################################################

# 'siblings' is more accurate than 'cpu cores' for Intel CPUs with
# hyperthreading
CORES=`sed -rn '/siblings/{s/.+: //p; q}' /proc/cpuinfo`

## ANALYSIS DIRECTORIES AND FLAGS ##############################################

declare -a ANALYSIS_RESULT_DIRS
declare -a ANALYSIS_FLAGS

# Matrix slicing, nothing else. It's best to run this first, as it's
# the setting which uses most memory, by far.
#
# NOTE: "index flattening" is the old name (pre-SOSE) for "matrix
# slicing". The old name for "matrix flattening" is "nodeset
# flattening".

# Matrix slicing, everything else turned on
ANALYSIS_RESULT_DIRS[1]="results-slicing-comp-filtunused-xsd-simp"
ANALYSIS_FLAGS[1]="--index-flattening --simplify"

# Matrix flattening, everything else turned on
ANALYSIS_RESULT_DIRS[2]="results-flattening-comp-filtunused-xsd-simp"
ANALYSIS_FLAGS[2]="--simplify"

## HELPER FUNCTIONS ############################################################

function run_bpel() {
    BPEL=$1
    DAIKON_OUTPUT_DIR=$2

    for i in `seq 1 ${#ANALYSIS_RESULT_DIRS[@]}`; do
        ANALYSIS_RESULT_PATH=$DAIKON_OUTPUT_DIR/${ANALYSIS_RESULT_DIRS[$i]}
        ANT_LOG=$ANALYSIS_RESULT_PATH/ant.log
	SHA1_LOG=$DAIKON_OUTPUT_DIR/sha1.log
        mkdir -p "$ANALYSIS_RESULT_PATH"

        # Same Ant properties, but different targets
        if [ "$i" == 1 ]; then
            # First analysis: run the BPTS suite and analyze
            TARGET="test-and-analyze"
        else
            # Rest: only analyze
            TARGET="analyze"
        fi

        ant -Dmain.bpel="$BPEL" \
            -Ddaikonout.dir="$DAIKON_OUTPUT_DIR" \
            -Ddaikonout.analysis.dirname="${ANALYSIS_RESULT_DIRS[$i]}" \
            -Danalyzer.flags="${ANALYSIS_FLAGS[$i]} --ncpu=$CORES" \
            "$TARGET"

	# First analysis: log SHA1 of auxiliary files
	if [ "$i" == 1 ]; then
	    sha1sum {catalog,tiposReunidos,types}.xml *.pdd *.decls velocity.log > "$SHA1_LOG"
	fi
    done

    # Wait for all processing to finish
    wait
}

# Compares the invariants from the original program with those from
# the mutant, for every option combination used.
function compare_with_original() {
    ORIGINAL_DIR=$1
    MUTANT_DIR=$2

    # Match each analysis result directory in the original composition
    # with the equivalent in the mutant composition. For instance,
    # original/a/a.inv.gz should be compared with mutant/a/a.inv.gz.
    #
    # NOTE: these comparisons will be done in parallel, so you better
    # have a few cores and quite a bit (several GiBs) of RAM!
    for RESDIR in `find "$ORIGINAL_DIR" -mindepth 1 -maxdepth 1 -type d`; do
        RESDIR_NAME=`basename "$RESDIR"`
        if [ -r "$RESDIR"/*.inv.gz ]; then
            compara-invariantes.bash -v \
                -c "$ORIGINAL_DIR/$RESDIR_NAME.csv" \
                -r "$RESDIR"/*.inv.gz \
                "$MUTANT_DIR/$RESDIR_NAME"/*.inv.gz &
        fi
    done

    # Wait for all subprocesses to be finished
    wait
}

## SCRIPT BODY #################################################################

# Restart ActiveBPEL
~/bin/ActiveBPEL.sh restart

# Run the original process and compare its invariants to themselves
ORIGINAL_OUTPUT_DIR=takuan-original-`date +"%Y%m%d-%H%M%S"`
run_bpel "$ORIGINAL_BPEL" "$ORIGINAL_OUTPUT_DIR"
for RESDIR_NAME in ${ANALYSIS_RESULT_DIRS[@]}; do
    compara-invariantes.bash \
        -c "$ORIGINAL_OUTPUT_DIR/$RESDIR_NAME.csv" \
        -vr "$ORIGINAL_OUTPUT_DIR/$RESDIR_NAME"/*.inv.gz &
done
wait

# Run all mutants and compare them against the original process
for BPEL; do
    BPEL_FNAME=`basename "$BPEL"`
    BPEL_PATH=`dirname "$BPEL"`
    DAIKON_OUTPUT_DIR=$BPEL_PATH/takuan-$BPEL_FNAME-`date +"%Y%m%d-%H%M%S"`

    # Try to run the BPEL and compare it with the original one, but do not fail the whole process if it is invalid
    run_bpel "$BPEL" "$DAIKON_OUTPUT_DIR" && compare_with_original "$ORIGINAL_OUTPUT_DIR" "$DAIKON_OUTPUT_DIR" || true

    # Compress results and delete the old directory
    tar czf "$DAIKON_OUTPUT_DIR.tar.gz" "$DAIKON_OUTPUT_DIR"
    rm -r "$DAIKON_OUTPUT_DIR"
done
