#!/bin/bash

if [ "$#" -lt 2 ]; then
    echo "Usage: $0 bpts bpel"
    exit 1
fi

BPTS=$1; shift
BPEL=$1; shift
SALIDA_ORIG=salida-original.xml
LISTA_MUTANTES=listado.xml

# List all mutants
rm -f m*.bpel
mubpel applyall "$BPEL"
ls m*.bpel > "$LISTA_MUTANTES"

# Run the BPTS against the original program
rm -f "$SALIDA_ORIG"
mubpel run "$BPTS" "$BPEL" > "$SALIDA_ORIG"

# Perform the comparison
cat "$LISTA_MUTANTES" | \
    xargs mubpel compare --parallel 4 --bpel-loglevel none "$BPTS" "$BPEL" "$SALIDA_ORIG" | \
    paste "$LISTA_MUTANTES" - | \
    tee "$BPTS.out.txt"

# Mail Lorena and I
mail -s "Terminado $BPTS" \
    antonio.garciadominguez@uca.es \
    lorena.gutierrez@uca.es \
    <<<"Terminado $BPTS con codigo $?"
