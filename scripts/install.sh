#!/bin/bash
# -*- bash -*-

# Takuan/GAmera installation and update script for Ubuntu/Debian,
# openSUSE 11.0 and Arch Linux environments
# Copyright (C) 2009-2014 Antonio García Domínguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This script simply automates all the steps included in the Takuan/GAmera
# installation instructions. It is a poor man's substitute to a properly
# written set of Ubuntu/Debian/openSUSE/Arch packages.
#
# It accesses the Neptuno Nexus instance to download the latest version of
# each required artifact. When creating a tag in SVN, please don't forget
# to create releases for each component installed here and customize the
# _NEXUS variables with the proper versions and repositories!

# Exit on error
set -e
# Use error code from last command in a pipe chain
set -o pipefail
# export all variable changes
set -o allexport

### USER CONFIGURATION ########################################################

# Path under which all downloads should be cached to save bandwidth
DL_CACHE="${HOME}/Downloads"

# Path under which all binaries will be installed
BIN_PATH="${HOME}/bin"

# Path under which all the source code will be unpacked
SRC_PATH="${HOME}/src"

# Path to symlink under which Tomcat will be accesible (keep the BIN_PATH part)
CATALINA_HOME="${BIN_PATH}/tomcat5"

# Path under which Maven will be available
M2_HOME=${BIN_PATH}/maven

# Path under which BPELUnit will be available
BPELUNIT_HOME=${BIN_PATH}/bpelunit

# Host which serves Nexus and SVN
SERVER_HOST=neptuno.uca.es

# URL to the prefix of the Nexus REST service API for
# resolving/downloading artifacts by GAV coordinates
NEXUS_ARTIFACT_PREFIX="https://${SERVER_HOST}/nexus/service/local/artifact/maven"

### SCRIPT CONFIGURATION ######################################################

# Base URL to use as prefix for all downloads
#
# Note to developers: be sure to customize it when you copy this script to
# a tag or a branch!
BASE_SVN_URL="https://${SERVER_HOST}/svn/sources-fm/trunk/"

# Default installation target (if any). Leave empty if you don't want
# to use any.
#
# Same note as above.
TARGET=""

### you shouldn't need to change much below here, unless you've changed ###
### the layout of the Subversion repository or its dependencies.        ###

# Tomcat GAV coordinates
TOMCAT_BIN_NEXUS="r=thirdparty&g=org.apache&a=tomcat&v=5.5.26&c=dist&p=zip"

# Maven GAV coordinates
M2_BIN_NEXUS="r=thirdparty&g=org.apache&a=maven&v=3.0.5&c=dist&e=tar.gz"

# ActiveBPEL 4.1, UCA distribution
ACTIVEBPEL_NEXUS="r=thirdparty&g=org.activebpel&a=rt.dist&v=4.1-uca15&e=zip&c=tomcat"

# ActiveBPEL 4.1 launcher basename
ACTIVEBPEL_LAUNCHER_BASENAME=ActiveBPEL.sh

# Deployment directory for ActiveBPEL
ACTIVEBPEL_DEPLOY_DIR="${CATALINA_HOME}/bpr"

# Path to the current BPELUnit binary distribution
BPELUNIT_BIN_NEXUS="r=snapshot-repos&g=net.bpelunit&a=dist&v=1.6.2-SNAPSHOT&p=tar.gz&c=standalone"

# Path to configuration.xml from BPELUnit home dir
BPELUNIT_CONF="conf/configuration.xml"

# Takuan servlet GAV coordinates
TAKUAN_SERVLET_NEXUS="r=snapshots&g=es.uca.webservices&a=takuan-servlet&v=2.0.2-SNAPSHOT&p=war"

# Path to the Takuan servlet WAR
TAKUAN_SERVLET_WAR="${CATALINA_HOME}/webapps/takuan.war"

# Takuan binary distribution for 32-bit Linux
TAKUAN_DIST_I386_NEXUS="r=snapshots&g=es.uca.webservices&a=takuan-launcher&v=2.0.2-SNAPSHOT&p=zip&c=dist-Linux-i386"

# Takuan binary distribution for 64-bit Linux
TAKUAN_DIST_AMD64_NEXUS="r=snapshots&g=es.uca.webservices&a=takuan-launcher&v=2.0.2-SNAPSHOT&p=zip&c=dist-Linux-amd64"

# mubpel GAV coordinates
MUBPEL_DIST_NEXUS="r=snapshots&g=es.uca.webservices&a=mubpel&v=1.3.1-SNAPSHOT&p=zip&c=uberjar"

# Basename of the mubpel launcher
MUBPEL_LAUNCHER_BASENAME="mubpel"

# Basename of the mubpel über-JAR
MUBPEL_UBERJAR_BASENAME="mutation_analysis.jar"

# GAV coordinates for GAmeraHOM
GAMERAHOM_DIST_NEXUS="r=snapshots&g=es.uca.webservices&a=gamerahom-core&v=2.0.0-SNAPSHOT&e=zip&c=dist"

# Basename of the GAmeraHOM launcher
GAMERAHOM_LAUNCHER_BASENAME="gamerahom"

# GAV coordinates for the GAmeraHOM Bacterio converter
GAMERAHOMBACTERIO_DIST_NEXUS="r=snapshots&g=es.uca.webservices&a=gamerahom-bacterio-converter&v=2.0.0-SNAPSHOT&e=zip&c=uberjar"

# Basename of the GAmeraHOM Bacterio converter launcher
GAMERAHOMBACTERIO_LAUNCHER_BASENAME="gamerahom-bacterio-converter"

# Basename of the GAmeraHOM Bacterio converter überjar
GAMERAHOMBACTERIO_UBERJAR_BASENAME="gamerahom-bacterio-converter.jar"

# ServiceAnalyzer GAV coordinates
SA_DIST_NEXUS="r=snapshots&g=es.uca.webservices&a=service-analyzer&v=1.1.3-SNAPSHOT&p=zip&c=dist"

# Basename of the ServiceAnalyzer launcher
SA_LAUNCHER_BASENAME="service-analyzer"

# GAV coordinates for GAmeraHOM-ggen
GAMERAHOMGGEN_DIST_NEXUS="r=snapshots&g=es.uca.webservices&a=gamerahom-ggen&v=2.0.0-SNAPSHOT&p=zip&c=dist"

# Basename of the GAmeraHOM launcher
GAMERAHOMGGEN_LAUNCHER_BASENAME="rodan"

# TestGenerator GAV coordinates
TG_DIST_NEXUS="r=snapshots&g=es.uca.webservices&a=test-generator&v=1.1.0-SNAPSHOT&p=zip&c=dist"

# Basename of the TestGenerator launcher
TG_LAUNCHER_BASENAME="testgenerator"

# test-generator-autoseed GAV coordinates
TGAUTOSEED_DIST_NEXUS="r=snapshots&g=es.uca.webservices&a=test-generator-autoseed&v=1.1.0-SNAPSHOT&p=zip&c=uberjar"

# Basename of the TestGenerator launcher
TGAUTOSEED_LAUNCHER_BASENAME="test-generator-autoseed"

# Basename of the GAmeraHOM uberjar
TGAUTOSEED_UBERJAR_BASENAME="test-generator-autoseed.jar"

# bpelstats GAV coordinates
BPELSTATS_DIST_NEXUS="r=snapshots&g=net.bpelunit&a=bpelstats&v=1.6.2-SNAPSHOT&p=zip&c=dist"

# Basename of the BPEL Stats launcher
BPELSTATS_LAUNCHER_BASENAME="bpelstats"

# Basename of the BPEL Stats uberjar
BPELSTATS_UBERJAR_BASENAME="bpelstats.jar"

# bpelstats GAV coordinates
BPTSGEN_DIST_NEXUS="r=snapshots&g=es.uca.webservices&a=bpts-generator&v=1.0.0-SNAPSHOT&e=zip&c=uberjar"

# Basename of the BPEL Stats launcher
BPTSGEN_LAUNCHER_BASENAME="bpts-generator"

# Basename of the BPEL Stats uberjar
BPTSGEN_UBERJAR_BASENAME="bpts-generator.jar"

### AUXILIARY FUNCTIONS #######################################################

function echo_err() {
  echo "$@" >&2
}

function log() {
  if [[ "$#" < 2 ]]; then
    error "Wrong parameters: expected level and rest of message, got $@"
    return 1
  fi
  LEVEL="$1"; shift
  echo_err "`date -R`: ($LEVEL) $@"
}

function status() {
  log INFO "$@"
}

function error() {
  log ERROR "$@"
}

function warn() {
  log WARN "$@"
}

function push_d() {
  # Create if it doesn't exist
  if test -f "$1"; then
    error "$1 is a file"
    return 1
  fi
  mkdir -p "$1"
  pushd "$1" > /dev/null
}

function pop_d() {
  popd > /dev/null
}

# Installs several Debian packages, without asking for confirmation.  Relies on
# several utilities found in recent Debian-based distros.  Produces an error if
# they are not present.
#
# Accepts a mix of package names and filenames, like this:
#
# install_deb_packages emacs my-other-local-file.deb
#
# Every argument which matches a file in the local filesystem will be passed to
# gdebi, and the rest will be passed to apt-get.
function install_deb_packages() {
  declare -a FILES
  declare -a NAMES
  for f in $@; do
    if test -f "$f"; then
      FILES[${#FILES[*]}]=$f
    else
      NAMES[${#NAMES[*]}]=$f
    fi
  done
  if test "${NAMES[*]}" != ""; then
    sudo apt-get install -yq2 ${NAMES[@]}
  fi
  if test "${FILES[*]}" != ""; then
    sudo gdebi -n ${FILES[@]}
  fi
}

# Installs several RPM packages, without asking for confirmation.  Relies on the
# Zypper tool used by recent openSUSE releases. Produces an error if it is not
# present.
#
# Accepts anything Zypper accepts, which is a mix of package names and
# filenames:
#
# install_rpm_packages emacs my-local-package.rpm
function install_rpm_packages() {
  sudo zypper -n install -l $@
}

# Installs several Perl modules from CPAN, without asking for confirmation.
function install_from_cpan() {
  yes | sudo cpan $@
}

function has_perl_module() {
  if [ "$#" != 1 ]; then
     error "Wrong parameters: expected Perl module name (such as Module::Package), got $@."
     return 1
  fi
  perl -M"$1" -e 1 >/dev/null
}

# Performs an XPath query on a file, expecting a scalar result. Both
# openSUSE 11.0 and Ubuntu 9.10 have the XML::XPath 1.13 Perl module,
# but their launchers are different.
function xpath_query() {
  if [ "$#" != 2 ]; then
      error "Wrong parameters: expected file and XPath query, got $@"
      return 1
  fi

  FILE=$1
  QUERY=$2
  perl -MXML::XPath \
    -e "print XML::XPath->new(filename=>'$FILE')->find(<>);" <<<"$QUERY"
}

function download_from_svn() {
  if [ "$#" != 1 ]; then
    error "Wrong parameters: downloadFromSVN expects relative url from $BASE_SVN_URL, got $@."
    return 1
  fi
  URL="$BASE_SVN_URL/$1"
  BASENAME=`basename "$URL"`
  CACHE_PATH=$DL_CACHE/$BASENAME

  if ! has_perl_module "Date::Parse"; then
      if can_run apt-get; then
          install_deb_package libtimedate-perl
      elif can_run pacman; then
          sudo pacman -S perl-timedate
      fi
  fi

  if test -e "$CACHE_PATH"; then
    # Floating-point math doesn't work well from Bash scripts, and stat(1) returns
    # an integer anyway, so we truncate the SVN timestamp to seconds. We do lose
    # some accuracy, but it'd only be a problem if we did several commits in less
    # than a second, and we somehow exported the file in one of the revisions
    # in-between, which isn't all too likely.
    CACHED_DATE_SECS=`stat -c "%Y" "$CACHE_PATH"`
    SVN_DATE=`svn info --xml "$URL" | sed -nre 's#<date>(.*)</date>#\1#p'`
    SVN_DATE_SECS=`perl -MDate::Parse -e "print int(str2time('$SVN_DATE'));"`
    if (( $SVN_DATE_SECS <= $CACHED_DATE_SECS )); then
      status "Cached copy of $BASENAME in $DL_CACHE is up to date."
    else
      status "Cached copy of $BASENAME in $DL_CACHE is outdated (SVN: $SVN_DATE_SECS, cached: $CACHED_DATE_SECS)."
      rm -rf "$CACHE_PATH"
    fi
  fi

  if test ! -e "$CACHE_PATH"; then
    status "Downloading $URL into $CACHE_PATH..."
    push_d "$DL_CACHE"
    svn export -q "$URL"
    pop_d
  fi

  echo $CACHE_PATH
}

function download_from_nexus() {
    if [ "$#" != 1 ]; then
        error "Usage: download_from_nexus (REST API query string)"
        error "See Nexus documentation for details (http://j.mp/dCuZxL) for:"
        error "  - /artifact/maven/content"
        error "  - /artifact/maven/resolve"
        return 1
    fi
    QUERY="$1"
    status "Fetching Nexus artfact matching $QUERY..."
    push_d "$DL_CACHE"

    URL="$NEXUS_ARTIFACT_PREFIX/redirect?$QUERY"
    status "Visiting URL $URL..."

    # Download the artifact into the cache, using Nexus' redirects and
    # wget's timestamping to get the name of the file and avoid
    # downloading the same thing over and over. Notes:
    #
    #                 LC_ALL: we need C locale to parse wget's output reliably.
    # --no-check-certificate: TERENA SSL certificate is not trusted in openSUSE.
    #  --content-disposition: use the filename proposed by Nexus.
    #         --timestamping: do not download if the existing file is newer
    BASENAME=$(LC_ALL="C" \
        wget --no-check-certificate --content-disposition --timestamping \
        "$URL" 2>&1 \
        | tee | grep '^Location' | awk '{print $2}' | xargs basename)
    if test "$?" != 0; then
        error "Could not download artifact matching $QUERY"
        return 1
    fi

    DEST_PATH="$DL_CACHE/$BASENAME"
    status "Nexus artifact downloaded to $DEST_PATH"
    pop_d
    echo "$DEST_PATH"
}

function download_from_http() {
    if [ "$#" != 1 ]; then
        error "Usage: download_http (URL)"
        return 1
    fi

    URL=$1
    push_d "$DL_CACHE"

    status "Downloading $URL..."
    if ! wget --no-check-certificate --no-http-keep-alive --timestamping "$URL"; then
        error "Failed to download $URL"
        return 2
    fi

    DEST_PATH=$DL_CACHE/$(basename "$URL")
    status "File downloaded to $DEST_PATH"
    pop_d
    echo $DEST_PATH
}

function checkout_from_svn() {
  if [ "$#" != 1 -a "$#" != 2 ]; then
    error "Wrong parameters: checkout_from_svn expected relative path to folder from $BASE_SVN_URL, and optionally the checkout path, got $@."
    return 1
  fi

  SVNPATH="$1"
  URL="$BASE_SVN_URL/$SVNPATH"
  FLAGS=""
  if [ "$#" == 2 ]; then
    DEST="$2"
  else
    DEST=`pwd`/`basename "$URL"`
  fi

  if test -e "$DEST"; then
    status "The SVN checkout path $DEST already exists. I need to remove it to continue."
    if ! confirm_rm "$DEST"; then
      return 1
    fi
  fi

  if ! svn info "$URL" &>/dev/null; then
      # If it doesn't exist as a regular path in the SVN repository
      # (svn info doesn't produce anything on stdout), then it might
      # be an svn:external reference. Parse the URL from the
      # svn:externals property and check that out.

      # Escape slashes in the path
      ESC_SVNPATH=`sed 's#/#\\\\/#g' <<<"$SVNPATH"`

      # Do the parsing. NOTE: this will not work with the new formats
      # in SVN 1.5 and 1.6!
      SVN_EXT_LINE=`svn pg svn:externals "$BASE_SVN_URL" |\
        gawk -F'[ \t]+' "/^$ESC_SVNPATH[[:space:]]+/ {print \\$2 \",\" \\$3}"`

      if [ -z "${SVN_EXT_LINE%,*}" ]; then
          echo "$SVNPATH isn't external and doesn't exist at $URL."
          return 2
      elif [ -n "${SVN_EXT_LINE#*,}" ]; then
          FLAGS=${SVN_EXT_LINE%,*}
          URL=${SVN_EXT_LINE#*,}
      else
          URL=${SVN_EXT_LINE%,*}
      fi
  fi

  svn checkout $FLAGS "$URL" "$DEST"
  echo "$DEST"
}

function confirm() {
  RESULT=""
  while test "$RESULT" != "y" -a "$RESULT" != "n" ; do
    echo_err -n "$1 [yn] "
    read RESULT
    RESULT=`tr [:upper:] [:lower:] <<<"$RESULT"`
  done
  test "$RESULT" = "y"
}

# Checks whether an environment variable has been set somewhere in the
# user's profile file.
function has_env_var() {
  if test "$#" != 1; then
    error "Wrong parameters: expected variable name, got $@"
    return 1
  fi

  VAR="$1"
  egrep -q "^export +$VAR=" ~/.bashrc
}

# Adds a specific line to the user's .bashrc file
function add_profile_line() {
  if test "$#" != 1; then
    error "Wrong parameters: expected line to be added to ~/.bashrc, got $@"
    return 1
  fi

  # We need to stay in sync with the .bashrc file
  LINE="$1"
  eval $LINE
  if ! grep -q "^$LINE" ~/.bashrc; then
    echo "$LINE" >>~/.bashrc
    status "Added line '$LINE' to ~/.bashrc"
  else
    warn "Line '$LINE' was already in ~/.bashrc. Will not do anything."
  fi
}

# Saves an environment variable to the user's .bashrc. Doesn't modify any
# prior bindings to the same environment variable in the file.
function save_env_var() {
  if test "$#" != 2; then
    error "Wrong parameters: expected variable name and value, got $@"
    return 1
  fi

  VAR="$1"
  VALUE="$2"
  LINE="export $VAR=$VALUE"

  add_profile_line "$LINE"
}

# Removes _all_ assignments to an environment variable from the user's .bashrc.
# The old .bashrc is saved in .bashrc.bak.
function clear_env_var() {
  if test "$#" != 1; then
    error "Wrong parameters: expected variable name, got $@"
    return 1
  fi

  VAR="$1"
  sed -i.bak -e "/^export $VAR=/d" ~/.bashrc

  # Check that the variable is not bound anymore
  if ! has_env_var "$VAR"; then
    status "Removed all bindings for $VAR from ~/.bashrc"
  else
    error "Could not remove bindings for $VAR from ~/.bashrc"
    return 1
  fi
}

function has_java_version() {
  if test "$#" != 1; then
    error "Wrong arguments: expected Java version (5, 6, etc.)."
    return 1
  fi
  if ! can_run javac; then
    return 2
  fi

  EXPECTED="$1"
  OBTAINED=$(javac -version 2>&1 | sed -nre 's/.* (.*)_.*/\1/p')
  if test -n "$OBTAINED"; then
    status "Looking for Java $EXPECTED, found Java $OBTAINED set as default"
  else
    status "Looking for Java $EXPECTED, didn't find any JRE set up"
  fi
  test "$EXPECTED" = "$OBTAINED"
}

function can_run() {
  if test "$#" != 1; then
    error "Wrong arguments: expected command name."
    exit 1
  fi

  CMD="$1"
  type -P "$CMD" >/dev/null
}

function ensure_can_run() {
    if test "$#" != 1; then
        error "Wrong arguments: expected basename of the program to be run from PATH."
        return 1
    fi

    BASENAME=$1
    if ! can_run "$BASENAME"; then
        # Need to add $BIN_PATH to PATH
        export PATH="$PATH":"$BIN_PATH"
        save_env_var PATH "\$PATH:$BIN_PATH"
        status "Added $BIN_PATH to the PATH environment variable"
    fi
    if ! can_run "$BASENAME"; then
        error "Could not ensure $BASENAME runs from the PATH"
        return 1
    fi
}

function install_symlink() {
  if test "$#" != 1; then
    error "Wrong arguments: expected path to script."
    return 1
  fi

  SCRIPT=$1
  BASENAME=`basename "$SCRIPT"`
  DEST=$BIN_PATH/$BASENAME
  if [ "$SCRIPT" != "$DEST" ]; then
      chmod +x "$SCRIPT"
      ln -sf "$SCRIPT" "$DEST"
  fi
  ensure_can_run "$BASENAME"
  status "Installed $SCRIPT in $BIN_PATH"
}

function install_script() {
  if test "$#" != 1; then
    error "Wrong arguments: expected path to script."
    return 1
  fi

  SCRIPT=$1
  BASENAME=`basename "$SCRIPT"`
  DEST=$BIN_PATH/$BASENAME
  if [ "$SCRIPT" != "$DEST" ]; then
      cp "$SCRIPT" "$DEST"
  fi
  chmod +x "$DEST"
  ensure_can_run "$BASENAME"

  status "Installed $SCRIPT in $BIN_PATH"
}

function unpack() {
  if test "$#" == 0 || test "$#" -gt 2; then
    error "Usage: unpack [-np] (archive)"
    error "   -n: do not recreate directories (only for .zip/.jar)"
    error "   -p: print main directory basename after unpacking (conflicts with -n)"
    return 1
  fi

  status "Unpacking with $* at $(pwd)..."
  unset JUNK_PATHS
  unset PRINT_MAIN_DIR
  OPTIND=1
  while getopts :np OPTION; do
      case "$OPTION" in
          n) JUNK_PATHS="yes"; status "Will not recreate paths";;
          p) PRINT_MAIN_DIR="yes"; status "Will print the main directory";;
      esac
  done
  shift $(( $OPTIND - 1 ))
  ARCHIVE="$1"

  if [ -n "$JUNK_PATHS" ]; then
      case "$ARCHIVE" in
          *.tar.*|*.t*)
              error "Cannot discard paths in the $ARCHIVE tar file! Bailing out..."
              return 3
              ;;
      esac
  elif ! test -e "$ARCHIVE"; then
      error "Cannot unpack $ARCHIVE: does not exist"
      return 4
  elif ! test -f "$ARCHIVE"; then
      error "Cannot unpack $ARCHIVE: it's not a file"
      return 5
  fi

  case "$ARCHIVE" in
    *.tar.gz|*.tgz)
          MAIN_DIR=$(tar xvzf "$ARCHIVE" | tail -1 | awk -F '/' '{print $1}')
          ;;
    *.tar.bz2)
          MAIN_DIR=$(tar xvjf "$ARCHIVE" | tail -1 | awk -F '/' '{print $1}')
          ;;
    *.zip|*.jar)
          unset FLAGS
          if [ -n "$JUNK_PATHS" ]; then
              FLAGS="-j"
          fi
          MAIN_DIR=$(unzip -v "$ARCHIVE" | grep -w 00000000 | tail -1 | awk '{print $8}' | awk -F/ '{print $1}')
          unzip -qq $FLAGS "$ARCHIVE"
          ;;
    *)
      error "Unknown file extension in $ARCHIVE"
      return 6
      ;;
  esac

  if [ -n "$PRINT_MAIN_DIR" ]; then
      status "Main directory of $ARCHIVE: $MAIN_DIR"
      echo "$MAIN_DIR"
  fi
}

function host_is_amd64() {
  ARCH=`uname -m`
  if [ "$ARCH" = 'x86_64' ]; then
    status "$ARCH: 64 bits, x86"
    return 0
  else
    status "$ARCH: unknown architecture, assuming 32 bits x86"
    return 1
  fi
}

# Substitute for "rm -I" (coreutils 7.4). "rm -I" doesn't tell the user what
# files are going to be removed before asking for confirmation. It just
# says something like "remove all arguments?", which is useless if we're
# calling it from a shell script, like in our case.
#
# This function tells the user what is going to be removed and asks for
# confirmation. If confirmed, the files are removed and a zero status code
# is returned. Otherwise, a non-zero status code is returned. Only existing
# paths are reported to the user: if none of them exist, this function will
# simply do nothing.
#
# This function accepts a sequence of paths to be removed. Optionally, the
# -s may be used before any path, to indicate sudo should be used to remove
# the files with root privileges.
#
# Suggested tests:
# - args = []                         -> error about usage
# - args = [a], !exist(a)             -> nothing
# - args = [a], exist(a)              -> prompt([a]),   confirm -> !exist(a)
# - args = [a], exist(a)              -> prompt([a]),   cancel  -> status(1), exist(a)
# - args = [a,b], !exist(a), !exist(b)-> nothing
# - args = [a,b], !exist(a), exist(b) -> prompt([b]),   confirm -> !exist(a),!exist(b)
# - args = [a,b], exist(a), !exist(b) -> prompt([a]),   confirm -> !exist(a),!exist(b)
# - args = [a,b], exist(a), exist(b)  -> prompt([a,b]), confirm -> !exist(a),!exist(b)
# - args = [a,b], exist(a), exist(b)  -> prompt([a,b]), confirm -> !exist(a),!exist(b)
# - args = [a,b], exist(a), exist(b)  -> prompt([a,b]), cancel  -> status(1), exist(a), exist(b)
function confirm_rm() {
  if test "$#" == 0; then
    error "Usage: [-s] path(s)..."
    error "   -s: use root privileges (sudo) for removing the paths"
    exit 1
  fi

  # -s: use sudo to run rm
  OPTIND=1
  MSG_TEXT="All files under the following paths will be removed with the rights of user"
  if getopts :s OPCIONES; then
      MSG_TEXT="$MSG_TEXT 'root':"
      CMD_PREFIX="sudo "
  else
      MSG_TEXT="$MSG_TEXT '`whoami`':"
      CMD_PREFIX=""
  fi

  SOME_FILE_EXISTS=0
  for f in $@; do
    if test -e "$f"; then
      if test "$SOME_FILE_EXISTS" == 0; then
        echo "$MSG_TEXT"
        SOME_FILE_EXISTS=1
      fi
      echo "  - $f"
    fi
  done
  if test "$SOME_FILE_EXISTS" == 1; then
    if confirm "Are you sure?"; then
      $CMD_PREFIX rm -rf -- $@
    else
      return 1
    fi
  fi
}

#
# Installs a program included in a binary distribution with a single
# uberjar containing all dependencies and a Bash script which runs
# that uberjar. Both files should be at the same nesting level.
#
function install_dist_with_uberjar() {
    if [[ "$#" != 4 ]]; then
        error "Wrong parameters: expected name, launcher basename, uberjar basename and GAV coords, got $@"
        return 1
    fi

    PROGRAM=$1
    LAUNCHER_BASENAME=$2
    UBERJAR_BASENAME=$3
    DIST_NEXUS=$4

    LAUNCHER=$BIN_PATH/$LAUNCHER_BASENAME
    UBERJAR=$BIN_PATH/$UBERJAR_BASENAME

    if can_run "$LAUNCHER_BASENAME"; then
        if ! (confirm "$PROGRAM seems to be already installed. Reinstall?" &&\
              confirm_rm "$LAUNCHER" "$UBERJAR")
        then
            return 0
        fi
    fi

    DIST=$(download_from_nexus "$DIST_NEXUS")
    push_d "$BIN_PATH"
    unpack -n "$DIST"
    pop_d
    install_script "$LAUNCHER"

    if can_run "$LAUNCHER"; then
        status "Successfully installed $PROGRAM in $BIN_PATH"
    else
        error "Failed to install $PROGRAM in $BIN_PATH"
        return 1
    fi
}

#
# Installs a binary distribution formed by a single directory with
# several launch scripts (with the +x bit set) and a lib/
# subfolder. The launch scripts should put all the entries into the
# classpath (and nothing else) before invoking the program.
#
function install_dist_with_lib_folder() {
    if [[ "$#" != 3 ]]; then
        error "Wrong parameters: expected name, main launcher basename and GAV coordinates, got $@"
        return 1
    fi

    PROGRAM=$1
    LAUNCHER_BASENAME=$2
    DIST_NEXUS=$3

    if can_run "$LAUNCHER_BASENAME"; then
        LAUNCHER_SYMLINK="$BIN_PATH/$LAUNCHER_BASENAME"
        if ! (confirm "$PROGRAM seems to be already installed. Reinstall?" && \
              confirm_rm "$LAUNCHER_SYMLINK" \
                  "$(dirname $(readlink -f "$LAUNCHER_SYMLINK"))")
        then
            return 0
        fi
    fi

    DIST=$(download_from_nexus "$DIST_NEXUS")
    push_d "$BIN_PATH"
    DIRNAME=$(unpack -p "$DIST")
    find $DIRNAME -mindepth 1 -maxdepth 1 -type f -executable | (
        while read f; do
            install_symlink "$f"
        done
    )
    pop_d

    if can_run "$LAUNCHER_BASENAME"; then
        status "Successfully installed $PROGRAM."
    else
        error "Could not run $PROGRAM's main launcher: installation failed."
        return 1
    fi
}

### COMMON DEPENDENCIES ########################################################

function prepare_repositories() {
  if can_run apt-get; then
    sudo apt-get update
  elif can_run zypper; then
    # Running openSUSE: set DVD repos to a *low* priority
    zypper lr | grep DVD | awk -F ' *\\| *' '{print $2}' | \
        (while read repo; do sudo zypper mr -p 200 "$repo"; done)
    sudo zypper refresh
  elif can_run pacman; then
    sudo pacman -Sy
  fi
}

function install_base_deps() {
  if can_run apt-get; then
    install_deb_packages subversion unzip curl ant ant-optional patch gdebi \
        sed gawk libxml-xpath-perl
  elif can_run zypper; then
    install_rpm_packages subversion unzip curl ant ant-junit ant-trax patch \
        sed gawk perl-XML-XPath openssl openssl-certs
  elif can_run pacman; then
    sudo pacman -S subversion unzip apache-ant junit perl-xml-xpath wget
    save_env_var PATH "\$PATH:/usr/share/java/apache-ant/bin"
  fi
}

function install_java_jdk() {
  if can_run javac && has_env_var JAVA_HOME; then
    if ! confirm "A Java SE JDK seems to be already installed and set up. Reinstall?"; then
      return 0
    fi
    clear_env_var JAVA_HOME
    clear_env_var JDK_HOME
    clear_env_var JDKDIR
  fi

  status "Installing OpenJDK 6"
  if can_run apt-get; then
    install_deb_packages openjdk-6-jdk
    # Note: users may have *both* the 32-bit and 64-bit JDKs
    # installed. Try first with the 64-bit one, and if it is not
    # available, go for the 32-bit one.
    save_env_var JAVA_HOME $(ls -d1 /usr/lib{64,}/jvm/java-6-openjdk{-i386,-amd64,} 2>/dev/null | head -1)
    if host_is_amd64; then
        sudo update-java-alternatives --set java-1.6.0-openjdk-amd64
    else
        sudo update-java-alternatives --set java-1.6.0-openjdk-i386
    fi
  elif can_run zypper; then
    install_rpm_packages java-1_6_0-openjdk-devel
    # Users may have *both* the 32-bit and 64-bit JDKs
    # installed. Try first with the 64-bit one, and if it is not
    # available, go for the 32-bit one.
    save_env_var JAVA_HOME $(ls -d1 /usr/lib{64,}/jvm/java-1.6.0-openjdk 2>/dev/null | head -1)

    # openSUSE 11.0: OpenJDK 6 might already be installed, but some other JDK
    # might be set as the default. We will take advantage of the fact that
    # --auto sets the alternative to that with the highest priority (OpenJDK 6
    # has higher priority than Sun JDK 6 and IcedTea 7), seeing that the
    # version of update-alternatives shipped in openSUSE 11.0 does not have
    # the --set option.
    sudo /usr/sbin/update-alternatives --auto java
    sudo /usr/sbin/update-alternatives --auto javac
  elif can_run pacman; then
      sudo pacman -S jdk7-openjdk
      save_env_var JAVA_HOME /usr/lib/jvm/java-7-openjdk/
  fi
  save_env_var JDK_HOME  \$JAVA_HOME
  save_env_var JDKDIR    \$JDK_HOME
  save_env_var CLASSPATH \$CLASSPATH:\$JDKDIR/lib/tools.jar
  save_env_var CLASSPATH \$CLASSPATH:.

  if can_run javac && has_env_var JAVA_HOME; then
    status "Successfully installed and set up OpenJDK."
  else
    error "Failed to install and set up OpenJDK."
    return 1
  fi
}

function install_maven() {
    if can_run mvn; then
        # Use the double dirname invocation to get the directory where
        # the existing Maven binaries are located, regardless of the
        # value of the M2_HOME variable. This should help transition
        # from ~/bin/maven2 to ~/bin/maven.
        CURR_M2_HOME=$(dirname $(dirname $(type -P mvn)))
        if ! (confirm "Maven seems to be already installed. Reinstall?" &&\
                confirm_rm "$CURR_M2_HOME" "$(readlink -f "$CURR_M2_HOME")"); then
            return 0
        fi
    fi

    # Download binaries from SVN
    M2_BIN=$(download_from_nexus "$M2_BIN_NEXUS")
    push_d "$BIN_PATH"
    M2_MAIN_DIR_UNPACKED=$(unpack -p "$M2_BIN")
    ln -sf "$(pwd)/$M2_MAIN_DIR_UNPACKED" "$M2_HOME"
    pop_d

    # Add to path
    export PATH=$PATH:$M2_HOME/bin
    save_env_var PATH "\$PATH:$M2_HOME/bin"

    # Add settings.xml file with default contents, if it does not
    # exist already. This allows users to simply check out
    # gamerahom-core or any other Maven-based project and work on it
    # without touching the rest. It will also allow for smoothly
    # removing the <repositories> element in our parent POM.
    M2_SETTINGS=$HOME/.m2/settings.xml
    if ! test -f "$M2_SETTINGS"; then
        status "Installed default settings in $M2_SETTINGS"
        mkdir -p "$(dirname "$M2_SETTINGS")"
        cat > "$M2_SETTINGS" <<EOF
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                              http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <profiles>
    <profile>
      <id>default</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>
      <repositories>
        <repository>
          <id>neptuno-releases</id>
          <url>https://$SERVER_HOST/nexus/content/groups/release-repos/</url>
          <releases><enabled>true</enabled></releases>
        </repository>
        <repository>
          <id>neptuno-snapshots</id>
          <url>https://$SERVER_HOST/nexus/content/groups/snapshot-repos/</url>
          <snapshots><enabled>true</enabled></snapshots>
        </repository>
      </repositories>
      <pluginRepositories>
        <pluginRepository>
          <id>neptuno-releases</id>
          <url>https://$SERVER_HOST/nexus/content/groups/release-repos/</url>
          <releases><enabled>true</enabled></releases>
        </pluginRepository>
        <pluginRepository>
          <id>neptuno-snapshots</id>
          <url>https://$SERVER_HOST/nexus/content/groups/snapshot-repos/</url>
          <snapshots><enabled>true</enabled></snapshots>
        </pluginRepository>
      </pluginRepositories>
    </profile>
  </profiles>
</settings>
EOF
    else
        status "$M2_SETTINGS already exists, leaving as is"
    fi

    # Check if it was correctly installed
    if mvn -h >/dev/null; then
        status "Successfully installed Maven in $M2_HOME"
    else
        error "Failed to install Maven in $M2_HOME"
        return 1
    fi
}

function install_tomcat5() {
  if test -d "$CATALINA_HOME"; then
    if ! (confirm "Tomcat 5.5 seems to be already installed at $CATALINA_HOME. Reinstall?" &&\
          confirm_rm "$CATALINA_HOME" "$(readlink -f "$CATALINA_HOME")"); then
      return 0
    fi
    clear_env_var CATALINA_HOME
  fi

  status "Installing Tomcat 5.5..."
  TOMCAT_DIST=$(download_from_nexus "$TOMCAT_BIN_NEXUS")
  push_d "$BIN_PATH"
  TOMCAT_MAINDIR=$(pwd)/$(unpack -p "$TOMCAT_DIST")
  ln -s "$TOMCAT_MAINDIR" "$CATALINA_HOME"
  status "Unpacked and linked Tomcat directory to $CATALINA_HOME"
  pop_d
  save_env_var CATALINA_HOME "$CATALINA_HOME"

  status "Successfully installed Tomcat 5.5."
}

function install_activebpel() {
  if test -d "$ACTIVEBPEL_DEPLOY_DIR"; then
    if ! (confirm "ActiveBPEL seems to be already installed. Reinstall?" &&\
          confirm_rm "$ACTIVEBPEL_DEPLOY_DIR"\
            "$BIN_PATH/$ACTIVEBPEL_LAUNCHER_BASENAME"); then
      return 0
    fi
    "$ACTIVEBPEL_LAUNCHER_BASENAME" stop || true
  fi

  # openSUSE 11.0: the Xalan serializer is split apart from the
  # main Xalan JAR and needs to be added manually to the CLASSPATH
  if can_run zypper; then
    save_env_var CLASSPATH \$CLASSPATH:/usr/share/java/xalan-j2-serializer.jar
  fi

  # Grab the distribution from SVN, unpack it and install the launcher
  ACTIVEBPEL_DIST=$(download_from_nexus "$ACTIVEBPEL_NEXUS")
  push_d "$CATALINA_HOME"
  unpack "$ACTIVEBPEL_DIST"
  install_script "bin/$ACTIVEBPEL_LAUNCHER_BASENAME"
  pop_d

  if "$ACTIVEBPEL_LAUNCHER_BASENAME" start; then
    "$ACTIVEBPEL_LAUNCHER_BASENAME" stop
    status "ActiveBPEL 4.1 has been succesfully patched, compiled and installed."
    status "ActiveBPEL has been configured with full logging enabled."
  else
    error "Failed to install ActiveBPEL."
    return 1
  fi
}

function install_xpath_monitoring() {
    if test -e "$XPATHEXT_JAR_PATH"; then
        if ! (confirm \
            "The UCA XPath monitoring extensions seem to be already installed. Reinstall?" && \
            confirm_rm "$XPATHEXT_JAR_PATH"); then
            return 0
        fi
    fi

    # Download the .jar and put it in the right place
    XPATH_JAR=$(download_from_nexus "$XPATHEXT_BIN_NEXUS")
    cp "$XPATH_JAR" "$XPATHEXT_JAR_PATH"

    # Make sure the .jar was put in the right place, and that it contains Java code
    if javap -classpath "$XPATHEXT_JAR_PATH" "$XPATHEXT_TEST_CLASS"; then
        status "Successfully installed the UCA XPath monitoring extensions."
    else
        error "Failed to install the UCA XPath monitoring extensions."
        return 1
    fi
}

function install_bpelunit() {
  if test -d "$BPELUNIT_HOME"; then
    if ! (confirm "BPELUnit seems to be already installed. Reinstall?" &&\
          confirm_rm "$BPELUNIT_HOME" "$(readlink -f "$BPELUNIT_HOME")"); then
      return 0
    fi
    clear_env_var BPELUNIT_HOME
  fi

  # Unpack the binary distribution and create the symlink
  push_d "$BIN_PATH"
  BPELUNIT_BIN=$(download_from_nexus "$BPELUNIT_BIN_NEXUS")
  ln -s "$(pwd)/$(unpack -p "$BPELUNIT_BIN")" "$BPELUNIT_HOME"
  pop_d

  # Use our own configuration.xml
  cat > "$BPELUNIT_HOME/$BPELUNIT_CONF" <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<testConfiguration xmlns="http://www.bpelunit.org/schema/testConfiguration"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.bpelunit.org/schema/testConfiguration">
  <configuration deployer="activebpel">
    <property name="DeploymentDirectory">$ACTIVEBPEL_DEPLOY_DIR</property>
  </configuration>
</testConfiguration>
EOF

  # Install the launch scripts
  install_script "$BPELUNIT_HOME/bpelunit.sh"
  install_script "$BPELUNIT_HOME/dsexpand.sh"

  # Save the BPELUNIT_HOME environment variable
  save_env_var BPELUNIT_HOME "$BPELUNIT_HOME"

  if can_run bpelunit.sh; then
    status "Successfully installed BPELUnit."
  else
    error "Failed to install BPELUnit."
    return 1
  fi
}

### TAKUAN DEPENDENCIES ########################################################

function install_takuan() {
    if can_run takuan; then
        if ! (confirm "Takuan seems to be already installed. Reinstall?" && \
              confirm_rm "$(dirname $(readlink -f $(which takuan)))" "$(which takuan)")
        then
            return 0
        fi
    fi

    # Download the .zip
    if host_is_amd64; then
        TAKUAN_ZIP=$(download_from_nexus "$TAKUAN_DIST_AMD64_NEXUS")
    else
        TAKUAN_ZIP=$(download_from_nexus "$TAKUAN_DIST_I386_NEXUS")
    fi

    # Unpack and install the script
    push_d "$BIN_PATH"
    MAINDIR=$(unpack -p "$TAKUAN_ZIP")
    install_symlink "$MAINDIR/takuan"
    pop_d

    if can_run takuan; then
        status "Successfully installed Takuan"
    else
        error "Failed to install Takuan"
        return 1
    fi
}

function install_takuan_servlet() {
    if test -s "$TAKUAN_SERVLET_WAR"; then
        if ! (confirm "The Takuan servlet seems to be already installed. Reinstall?" &&\
              confirm_rm "$TAKUAN_SERVLET_WAR"); then
            return 0
        fi
    fi

    # Download the .war and deploy it in Tomcat
    SERVLET_WAR=$(download_from_nexus "$TAKUAN_SERVLET_NEXUS")
    cp "$SERVLET_WAR" "$TAKUAN_SERVLET_WAR"

    # Restart ActiveBPEL
    "$ACTIVEBPEL_LAUNCHER_BASENAME" restart
    status "Successfully installed the Takuan servlet in $TAKUAN_SERVLET_WAR."
}

### GAMERA DEPENDENCIES ########################################################

function install_mubpel() {
    install_dist_with_uberjar mubpel \
        "$MUBPEL_LAUNCHER_BASENAME" "$MUBPEL_UBERJAR_BASENAME" \
        "$MUBPEL_DIST_NEXUS"
}

function install_gamerahom() {
    if can_run "$GAMERAHOM_LAUNCHER_BASENAME"; then
        if ! (confirm "GAmeraHOM seems to be already installed. Reinstall?" && \
              confirm_rm "$BIN_PATH/$GAMERAHOM_LAUNCHER_BASENAME" \
                         "$(dirname $(readlink -f "$BIN_PATH/$GAMERAHOM_LAUNCHER_BASENAME"))")
        then
            return 0
        fi
    fi

    DIST=$(download_from_nexus "$GAMERAHOM_DIST_NEXUS")
    push_d "$BIN_PATH"
    DIRNAME=$(unpack -p "$DIST")
    LAUNCHER_PATH="$BIN_PATH/$DIRNAME/$GAMERAHOM_LAUNCHER_BASENAME"
    install_symlink "$LAUNCHER_PATH"
    pop_d

    if can_run "$GAMERAHOM_LAUNCHER_BASENAME"; then
        status "Successfully installed GAmeraHOM."
    else
        error "Could not run GAmeraHOM's launcher: installation failed."
        return 1
    fi
}

function install_gamerahom_bacterio_converter() {
    install_dist_with_uberjar "$GAMERAHOMBACTERIO_LAUNCHER_BASENAME" \
        "$GAMERAHOMBACTERIO_LAUNCHER_BASENAME" "$GAMERAHOMBACTERIO_UBERJAR_BASENAME" \
        "$GAMERAHOMBACTERIO_DIST_NEXUS"
}

function install_serviceanalyzer() {
    install_dist_with_lib_folder ServiceAnalyzer \
        "$SA_LAUNCHER_BASENAME" "$SA_DIST_NEXUS"
}

function install_rodan() {
    install_dist_with_lib_folder Rodan \
        "$GAMERAHOMGGEN_LAUNCHER_BASENAME" "$GAMERAHOMGGEN_DIST_NEXUS"
}

function install_testgenerator() {
    install_dist_with_lib_folder \
        TestGenerator "$TG_LAUNCHER_BASENAME" "$TG_DIST_NEXUS"
}

function install_testgenerator_autoseed() {
    install_dist_with_uberjar TestGenerator-autoseed \
       "$TGAUTOSEED_LAUNCHER_BASENAME" "$TGAUTOSEED_UBERJAR_BASENAME" \
       "$TGAUTOSEED_DIST_NEXUS"
}

function install_bpelstats() {
    install_dist_with_uberjar bpelstats \
        "$BPELSTATS_LAUNCHER_BASENAME" "$BPELSTATS_UBERJAR_BASENAME" \
        "$BPELSTATS_DIST_NEXUS"
}

function install_bptsgenerator() {
    install_dist_with_uberjar BPTS-Generator \
        "$BPTSGEN_LAUNCHER_BASENAME" "$BPTSGEN_UBERJAR_BASENAME" \
        "$BPTSGEN_DIST_NEXUS"
}

### MAIN SCRIPT BODY ##########################################################

if [ `whoami` = "root" ]; then
  echo "This script should not be run as root."
  exit 1
fi

if [[ "$#" != 1 ||\
      ( "$1" != "both" && "$1" != "takuan" && "$1" != "gamera" && "$1" != "mubpel"  && "$1" != "rodan") ]]; then
  if [ -z "$TARGET" ]; then
    # No default target was defined, and the user did not provide any,
    # or provided a wrong one: print usage and exit.
    echo "Usage: $0 (both|takuan|gamera|mubpel|rodan)"
    exit 1
  fi
else
  TARGET="$1"
fi

# Load the latest .bashrc, but careful: some of its commands may
# fail. For instance, the default openSUSE 11.1 per-user profile file
# checks whether $PROFILEREAD is unset. If it is set, the check will
# fail and with "set -e" still in effect, we would bail out of the
# whole install process. It's better to switch to "set +e" temporarily
# while loading the .bashrc file.
set +e
source ~/.bashrc
set -e

if [[ "$TARGET" != "mubpel" && "$TARGET" != "rodan" ]]; then
    status "Installing common requirements..."
    prepare_repositories
    install_base_deps
    install_maven
    install_java_jdk
    install_tomcat5
    install_activebpel
    install_bpelunit
    install_bpelstats
fi

if [[ "$TARGET" == "takuan" || "$TARGET" == "both" ]]; then
    status "Installing Takuan..."
    install_takuan
    install_takuan_servlet
fi

if [[ "$TARGET" == "gamera" || "$TARGET" == "both" ]]; then
    status "Installing GAmera..."
    install_mubpel
    install_gamerahom
    install_gamerahom_bacterio_converter
    install_serviceanalyzer
    install_testgenerator
    install_testgenerator_autoseed
    install_bptsgenerator
    install_rodan
fi

if [[ "$TARGET" == "mubpel" ]]; then
    status "Installing MuBPEL..."
    install_mubpel
fi

if [[ "$TARGET" == "rodan" ]]; then
    status "Installing Rodan..."
    install_rodan
fi

status "Done. Please run '. ~/.bashrc' or log out and back in."
