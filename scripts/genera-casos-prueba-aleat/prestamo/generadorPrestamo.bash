#!/bin/bash

if [ $# -lt 1 ]
then
 echo "Uso: $0 <n> <fich>"
 echo "  n es el número de casos de prueba aleatorios a generar"
 echo "  fich es el fichero de salida (por defecto f.bpts)"
 exit
fi

declare -i I=0

if [ $# -gt 1 ]
then
 FICH=$2
else
 FICH="f.bpts"
fi

echo '<?xml version="1.0" encoding="UTF-8"?> <tes:testSuite xmlns:esq="http://xml.netbeans.org/schema/Creditos" xmlns:ap="http://j2ee.netbeans.org/wsdl/ServicioAprobador" xmlns:as="http://j2ee.netbeans.org/wsdl/ServicioAsesor" xmlns:sp="http://j2ee.netbeans.org/wsdl/ServicioPrestamos" xmlns:pr="http://enterprise.netbeans.org/bpel/N6_ServicioPrestamo/ProcesoAprobadorPrestamo" xmlns:tes="http://www.bpelunit.org/schema/testSuite"> <tes:name>ServicioPrestamosTest</tes:name> <tes:baseURL>http://localhost:7777/ws</tes:baseURL> <tes:deployment> <tes:put name="ProcesoAprobadorPrestamo" type="activebpel"> <tes:wsdl>ServicioPrestamos.wsdl</tes:wsdl> <tes:property name="BPRFile">ServicioPrestamos.bpr</tes:property> </tes:put> <tes:partner name="assessor" wsdl="ServicioAsesor.wsdl"/> <tes:partner name="approver" wsdl="ServicioAprobador.wsdl"/> </tes:deployment>

  <tes:testCases>' > $FICH;
while [ $I -lt $1 ]
do
I+=1; R=$RANDOM; let "R %= 500";R=$R"00";echo '    <!-- Caso : cantidad , riesgo , aceptado  -->
    <tes:testCase name="caso'$I'" basedOn="" abstract="false" vary="false">

      <tes:clientTrack> <tes:sendReceive service="sp:ServicioPrestamosService" port="ServicioPrestamosPort" operation="concederCredito"> <tes:send fault="false"> <tes:data> <esq:ApprovalRequest>

    <!-- Cantidad baja -->
		<esq:amount>'$R'</esq:amount>

	      </esq:ApprovalRequest> </tes:data> </tes:send> <tes:receive fault="false">  </tes:receive> </tes:sendReceive> </tes:clientTrack>' >> $FICH;
R1=$RANDOM; let "R1 %= 2";R2=$RANDOM; let "R2 %= 2";
if [ $R1 -eq 0 ]
then
 R1=high
else
 R1=low
fi
if [ $R2 -eq 0 ]
then
 R2=true
else
 R2=false
fi;
if [ $R -gt 10000 ]
then
echo ' <tes:partnerTrack name="approver"> <tes:receiveSend service="ap:ServicioAprobadorService" port="ServicioAprobadorPort" operation="aprobarCredito"> <tes:send fault="false"> <tes:data> <esq:ApprovalResponse>

    <!-- Aceptado true -->
                <esq:accept>'$R2'</esq:accept>

              </esq:ApprovalResponse> </tes:data> </tes:send> <tes:receive fault="false"/> </tes:receiveSend> </tes:partnerTrack>  ' >> $FICH;
else

echo ' <tes:partnerTrack name="assessor"> <tes:receiveSend service="as:ServicioAsesorService" port="ServicioAsesorPort" operation="asesorarCredito"> <tes:receive fault="false"/> <tes:send fault="false"> <tes:data>  <esq:AssessorResponse>

    <!-- Riesgo alto -->
		<esq:risk>'$R1'</esq:risk>

	      </esq:AssessorResponse> </tes:data> </tes:send> </tes:receiveSend> </tes:partnerTrack>'>>$FICH;
if [ $R1 = "high" ]
then
echo ' <tes:partnerTrack name="approver"> <tes:receiveSend service="ap:ServicioAprobadorService" port="ServicioAprobadorPort" operation="aprobarCredito"> <tes:send fault="false"> <tes:data> <esq:ApprovalResponse>

    <!-- Aceptado true -->
                <esq:accept>'$R2'</esq:accept>

              </esq:ApprovalResponse> </tes:data> </tes:send> <tes:receive fault="false"/> </tes:receiveSend> </tes:partnerTrack>  ' >> $FICH;
fi;
fi;
echo "</tes:testCase>" >> $FICH;
done
echo '  </tes:testCases>
</tes:testSuite> ' >> $FICH;







