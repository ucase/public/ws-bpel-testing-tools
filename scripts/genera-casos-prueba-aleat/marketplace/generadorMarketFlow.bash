#!/bin/bash

if [ $# -lt 1 ]
then
 echo "Uso: $0 <n> <fich>"
 echo "  n es el número de casos de prueba aleatorios a generar. Ojo, como se generan a pares habrá n/2 casos cada uno con 2 retrasos en ejecucion (el retraso de cada mensaje es un número aleatorio de 0 a 2)"
 echo "  fich es el fichero de salida (por defecto f.bpts)"
 exit
fi

declare -i I=0

if [ $# -gt 1 ]
then
 FICH=$2
else
 FICH="f.bpts"
fi

echo '<?xml version="1.0" encoding="UTF-8"?>
<tes:testSuite
   xmlns:tns="http://docs.active-endpoints.com/activebpel/sample/wsdl/marketplace/2006/09/marketplace.wsdl"
   xmlns:tes="http://www.bpelunit.org/schema/testSuite">

  <tes:name>MarketPlace</tes:name>
  <tes:baseURL>http://localhost:7777/ws</tes:baseURL>

  <tes:deployment>
    <tes:put name="marketplace" type="activebpel">
      <tes:wsdl>marketplace.wsdl</tes:wsdl>
      <tes:property name="BPRFile">marketplace.bpr</tes:property>
    </tes:put>
    <tes:partner name="Buyer" wsdl="marketplace.wsdl"/>
  </tes:deployment>

  <tes:testCases>' > $FICH;

let "CASOS = $1 / 2"

while [ $I -lt $CASOS ]
do
I+=1;R=$RANDOM; let "R %= 1000";
T1=$RANDOM; let "T1 %= 3";
T2=$RANDOM; let "T2 %= 3";
#T1=0
#T2=2
echo '    <!-- Caso '$I'  -->
   <tes:testCase name="caso'$I'" basedOn="" abstract="false" vary="true">
      <tes:clientTrack>
        <tes:sendReceive
           service="tns:marketplaceSeller"
           port="seller"
           operation="submit">

          <tes:send fault="false" delaySequence="0,2">
            <tes:data>
              <tns:inventoryItem>Takuan</tns:inventoryItem>
              <tns:askingPrice>'$R'</tns:askingPrice>
            </tes:data>
          </tes:send>

          <tes:receive fault="false">
          </tes:receive>
        </tes:sendReceive>
      </tes:clientTrack>

' >> $FICH;
R1=$RANDOM; let "R1 %= 1000";
T3=$RANDOM; let "T3 %= 3";
T4=$RANDOM; let "T4 %= 3";
#T3=2
#T4=0
echo ' 
      <tes:partnerTrack name="Buyer">
        <tes:sendReceive
           service="tns:marketplaceBuyer"
           port="buyer"
           operation="submit">

          <tes:send fault="false" delaySequence="2,0">
            <tes:data>
              <tns:inventoryItem>Takuan</tns:inventoryItem>
              <tns:askingPrice>'$R1'</tns:askingPrice>
            </tes:data>
          </tes:send>

          <tes:receive fault="false">
          </tes:receive>
        </tes:sendReceive>
      </tes:partnerTrack>' >> $FICH;

echo "</tes:testCase>" >> $FICH;
done
echo '  </tes:testCases>
</tes:testSuite> ' >> $FICH;









