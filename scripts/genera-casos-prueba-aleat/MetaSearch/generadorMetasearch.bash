#!/bin/bash

declare -i J
DICC="/usr/share/dict/american-english"
FICH="f.bpts"

if [ $# -lt 1 ]
then
 echo "Uso: $0 <n> [fich] [-d diccionario]"
 echo "  n es el número de casos de prueba aleatorios a generar (si no se indica diccionario se usa $DICC )"
 echo "  fich es el fichero de salida (por defecto f.bpts)"
 exit
fi

declare -i I=0

if [ $# -eq 2 ]
then
 FICH=$2
fi

if [ $# -eq 3 ]
then
 DICC=$3
fi

if [ $# -eq 4 ]
then
 FICH=$2
 DICC=$4
fi

if [ ! -r $DICC ]
then
 echo "No se puede leer el diccionario $DICC"
 exit
fi

echo '<?xml version="1.0" encoding="UTF-8"?>
<!--
  Original version: copyright (C) 2006 Philip Meyer
  InvalidFromGoogle, InvalidFromMSN: copyright (C) 2008-2009 Antonio García Domínguez
-->
<tes:testSuite xmlns:msn="http://schemas.microsoft.com/MSNSearch/2005/09/fex" xmlns:met="http://examples.bpelunit.org/MetaSearch" xmlns:tes="http://www.bpelunit.org/schema/testSuite" xmlns:goog="http://examples.bpelunit.org/GoogleBridge">
  <tes:name>MetaSearchTest</tes:name>
  <tes:baseURL>http://localhost:7777/ws</tes:baseURL>
  <tes:deployment>
    <tes:put name="MetaSearch" type="activebpel">
      <tes:wsdl>MetaSearch.wsdl</tes:wsdl>
      <tes:property name="BPRFile">MetaSearch.bpr</tes:property>
    </tes:put>
    <tes:partner name="Google" wsdl="GoogleBridge.wsdl"/>
    <tes:partner name="MSN" wsdl="msnsearch.wsdl"/>
  </tes:deployment>
  <tes:testCases>' > $FICH;

# Se guarda el total de palabras
NUM_WORDS=$(cat $DICC | wc -l)

while [ $I -lt $1 ]
do
I+=1; R=$RANDOM; let "R %= 10"; let "R += 1";
R1=$RANDOM; let "R1 %= NUM_WORDS";
WORD=$(head -n $R1 $DICC | tail -n 1)

echo '       <tes:testCase name="caso'$I'" basedOn="SendPhilippLahmData" abstract="false" vary="false">
      <tes:clientTrack>
	<tes:sendReceive service="met:MetaSearch" port="MetaSearchPort" operation="process" xmlns:fex="http://schemas.microsoft.com/MSNSearch/2005/09/fex">
	  <tes:send fault="false">
	    <tes:data>
	      <met:MetaSearchProcessRequest>
		<met:query>'$WORD'</met:query>
		<met:language>en</met:language>
		<met:country>en_US</met:country>
		<met:maxResults>'$R'</met:maxResults>
	      </met:MetaSearchProcessRequest>
	    </tes:data>
	  </tes:send>
	  <tes:receive fault="false">
	  </tes:receive>
	</tes:sendReceive>
      </tes:clientTrack>' >> $FICH;

RG=$RANDOM; let "RG %= R"; J=0
 if [ $RG -eq 0 ]
 then
   echo '      <tes:partnerTrack name="Google"/>' >> $FICH
  else
   echo '<tes:partnerTrack name="Google">
	  <tes:receiveSend service="goog:GoogleSearchService" port="GoogleSearchPort" operation="doGoogleSearch">
	   <tes:send fault="false">
	     <tes:data>
	       <goog:SearchEngineResultList>' >> $FICH

   while [ $J -lt $RG ]
   do
    let "J += 1"
       R1=$RANDOM; let "R1 %= NUM_WORDS";
       WORD1=$(head -n $R1 $DICC | tail -n 1)

       RTITLE=$RANDOM; let "RTITLE %= 4"; let "RTITLE += 1"; K=0 ; TITLE=""
       while [ $K -lt $RTITLE ]
       do
        let "K += 1"

        R3=$RANDOM; let "R3 %= NUM_WORDS";
        WORD2=$(head -n $R3 $DICC | tail -n 1)

        TITLE="$TITLE $WORD2"
       done

       RSNIP=$RANDOM; let "RSNIP %= 10"; let "RSNIP += 1"; K=0 ; SNIP=""
       while [ $K -lt $RSNIP ]
       do
        let "K += 1"

        R3=$RANDOM; let "R3 %= NUM_WORDS";
        WORD2=$(head -n $R3 $DICC | tail -n 1)

        SNIP="$SNIP $WORD2"
       done

  	echo '<result>
		  <url>http://'$WORD1'</url>
		  <title>'$TITLE'</title>
		  <snippet>'$SNIP'</snippet>
		</result>' >> $FICH
   done
   echo '	      </goog:SearchEngineResultList>
	    </tes:data>
	  </tes:send>
	  <tes:receive fault="false"/>
	</tes:receiveSend>
      </tes:partnerTrack>' >> $FICH
 fi

RM=$RANDOM; let "RM %= R"; J=0; 
 if [ $RM -eq 0 ]
 then
   echo '      <tes:partnerTrack name="MSN"/>' >> $FICH
 else
   echo '      <tes:partnerTrack name="MSN">
	<tes:receiveSend service="fex:MSNSearchService" port="MSNSearchPort" operation="Search" xmlns:fex="http://schemas.microsoft.com/MSNSearch/2005/09/fex">
	   <tes:send fault="false">
	     <tes:data>
	      <msn:SearchResponse>
		<msn:Response>
		  <msn:Responses>
		    <msn:SourceResponse>
		      <msn:Source>Web</msn:Source>
		      <msn:Offset>0</msn:Offset>
		      <msn:Total>'$RM'</msn:Total>
		      <msn:Results>' >> $FICH

   while [ $J -lt $RM ]
   do
    let "J += 1";
       R1=$RANDOM; let "R1 %= NUM_WORDS";
       WORD1=$(head -n $R1 $DICC | tail -n 1)
 
       RTITLE=$RANDOM; let "RTITLE %= 4"; let "RTITLE += 1"; K=0 ; TITLE=""
       while [ $K -lt $RTITLE ]
       do
        let "K += 1"

        R3=$RANDOM; let "R3 %= NUM_WORDS";
        WORD2=$(head -n $R3 $DICC | tail -n 1)

        TITLE="$TITLE $WORD2"
       done

       RDESC=$RANDOM; let "RDESC %= 10"; let "RDESC += 1"; K=0 ; DESC=""
       while [ $K -lt $RDESC ]
       do
        let "K += 1"

        R3=$RANDOM; let "R3 %= NUM_WORDS";
        WORD2=$(head -n $R3 $DICC | tail -n 1)

        DESC="$DESC $WORD2"
       done


  	echo '			<msn:Result>
			  <msn:Title>'$TITLE'</msn:Title>
			  <msn:Description>'$DESC'</msn:Description>
			  <msn:Url>http://'$WORD1'</msn:Url>
			</msn:Result>' >> $FICH
   done
   echo '	      		      </msn:Results>
		    </msn:SourceResponse>
		  </msn:Responses>
		</msn:Response>
	      </msn:SearchResponse>
	    </tes:data>
	  </tes:send>
	  <tes:receive fault="false"/>
	</tes:receiveSend>
      </tes:partnerTrack>' >> $FICH
 fi





echo "</tes:testCase>" >> $FICH;
done
echo '  </tes:testCases>
</tes:testSuite> ' >> $FICH;







