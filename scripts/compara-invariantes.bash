#!/bin/bash

# Script para comparar los resultados de diversas ejecuciones de Takuan
#
# Copyright Manuel Palomo Duarte, Antonio García Domínguez 2009, 2010
# Disponible bajo los términos de la licencia GPL v3 o superior
#
# Ojo, si se compara un resultado consigo mismo es probable que salgan
# diferencias en los invariantes no interesantes.  Esto es porque el
# Diff de Daikon pone invariantes iguales como distintos si son SJJ o
# SUU (tambien SJU y SUJ)

# Salir al encontrar un error
set -e

## AYUDA Y PROCESAMIENTO DE PARÁMETROS #########################################

function ayuda()
{
    echo "
Uso: $0 [-hvp] [-c <ruta>] -r <referencia> [<conj...>]

Donde referencia es el conjunto de referencia y los demás son los que
se comparan con él. Se asume que los inv.gz están en directorios
distintos.

Si no se pasa ningún otro conjunto que el de referencia (el dado con
-r), se comparará el conjunto de referencia consigo mismo. De lo
contrario, el conjunto de referencia se comparará con los
proporcionados, pero *NO* consigo mismo. Esto nos permitirá ir
haciendo las comparaciones mutante a mutante a la vez que vamos
comprimiendo y borrando los directorios de los mutantes.

Ejemplos:

  * $0 -r a/a.inv.gz
  * $0 -r a/a.inv.gz b/b.inv.gz

Opciones:

  -v habilita el modo detallado (verbose)

  -h da un mensaje detallado de ayuda y termina

  -c <ruta> cambia el nombre del fichero CSV a generar. El valor por
   omisión es 'compara-invariantes.csv'.

  -r <referencia> indica el inv.gz con el conjunto de referencia.

Si el script falla indicando que no encuentra una clase, revise
DAIKON_JAR más abajo en este guión.

Las comparaciones que incluye este guión son:

* diff de Daikon sin opciones (sólo invariantes interesantes). Se
  almacena en el fichero diff.txt del directorio del inv.gz del
  mutante.

* diff de Daikon con -u (incluye también invariantes no
  interesantes). Se almacena en el fichero diffU.txt del directorio
  del inv.gz del mutante.

* diferencias obtenidas con diff del sistema. Se muestran por pantalla
  y se almacenan en el fichero compara-invariantes.csv del directorio
  actual. Se comparan los conjuntos con el de referencia y las
  estadísticas del de referencia: Total inv , Inv dist int , Inv dist
  (+noint) , TPP , TPP inv dist , TPP inv dist (+noint). Cada fila
  viene identificada con el inv.gz del mutante (o del conjunto de
  referencia si se ha comparado consigo mismo).

* Estadísticas de daikon (diff -s): las estadísticas de cada
  comparación se almacenan en el fichero stats.txt en el directorio
  del inv.gz del mutante, y las del conjunto de referencia en el
  fichero stats.txt de su inv.gz.

* Además, se crea un fichero CSV con las estadísticas de las
 comparaciones anteriores y de las trazas

-----------------------------------
LEYENDA DE java daikon.diff.Diff -s
-----------------------------------

Cogida de http://groups.csail.mit.edu/pag/daikon/download/jdoc/src-html/daikon/diff/DetailedStatisticsVisitor.html#line.20

Columnas:
SJJ	Both present, same invariant, justified in file1, justified in file2
SJU	Both present, same invariant, justified in file1, unjustified in file2
SUJ	Both present, same invariant, unjustified in file1, justified in file2
SUU	Both present, same invariant, unjustified in file1, unjustified in file2
DJJ	Both present, diff invariant, justification in file1, justified in file2
DJU	Both present, different invariant, justified in file1, unjustified in
	file2
DUJ	Both present, different invariant, unjustified in file1, justified in
	file2
DUU	Both present, different invariant, unjustified in file1, unjustified in
	file2
JM	Present in file1, justified in file1, not present in file2
UM	Present in file1, unjustified in file1, not present in file2
MJ	Not present in file1, present in file2, justified in file2
MU	Not present in file1, present in file2, unjustified in file2

Filas:
Nint	Nula interesante
N!Int	Nula no interesante
Uint	Unaria interesante
U!Int	Unaria no interesante
Bin	Binaria
Ter	Ternaria
"
}

# Por omisión, el modo detallado está desactivado
VERBOSE=0
CMD_LINE="$0 $*"
FICHERO_CSV=compara-invariantes.csv
while getopts ":hvr:c:" FLAG; do
    case "$FLAG" in
        h) ayuda; exit;;
        v) VERBOSE=1;;
        r) INV_REFERENCIA="$OPTARG";;
        c) FICHERO_CSV="$OPTARG";;
        *) echo "Opción desconocida: $OPTARG"; ayuda; exit 1;;
    esac
done
# Retira todos los argumentos utilizados como opciones
shift $(( OPTIND - 1 ))

# Comprobamos que podemos leer el conjunto de referencia
if [ -z "$INV_REFERENCIA" ]; then
    echo "No ha especificado el conjunto de referencia. Mostrando ayuda y saliendo..."
    ayuda
    exit 2
elif [ ! -r "$INV_REFERENCIA" ]; then
    echo "No puede leerse el conjunto de referencia $INV_REFERENCIA"
    echo "(puede que no exista). Mostrando ayuda y saliendo..."
    ayuda
    exit 3
fi

## CONFIGURACIÓN ###############################################################

FICHERO_LOG=compara-invariantes.log
DAIKON_JAR=~/src/daikon/daikon.jar
JAVA_FLAGS="-Xmx1500m -XX:MaxPermSize=800m -Xss1m -server"

## FUNCIONES AUXILIARES ########################################################

function log() {
    echo -e $@ | tee -a "$FICHERO_LOG"
}

function info() {
    log "[INFO]\t$*"
}

function verbose() {
    [ $VERBOSE -eq 1 ] && log "[DEBUG]\t$*" || true
}

function daikon_tpp() {
    if [ ! -f $1 ]; then
        echo "El primer y unico parametro debe ser un fichero diff.txt o diffU.txt"
        exit 1
    elif [ $# -ne 1 ]; then
        echo "El primer y unico parametro debe ser un fichero diff.txt o diffU.txt"
        exit 2
    fi

    TMP=/tmp/$(basename $0)-$$
    cat $1 | grep -v SUU | grep -v SJJ | grep -v SJU | grep -v SUJ > $TMP

    ACUM=0
    CONT=0
    TPP=0
    VOLCAR=0

    while read L; do
        echo "$L" | grep ::: > /dev/null && CONT=$((CONT + 1)) && WC=$((WC+1))
        echo "$L" | grep -v :::  > /dev/null && VOLCAR=1
        if [ $VOLCAR -eq 1 ]; then
            if [ $CONT -ne 0 ]; then
                ACUM=$((ACUM+CONT-1))
                CONT=0
            fi
            VOLCAR=0
        fi
    done < $TMP

    if [ $CONT -ne 0 ]; then
        ACUM=$((ACUM+CONT))
    fi

    rm $TMP

    WC=$((WC-ACUM))
    echo "$WC"
}

function daikon_diff() {
    java $JAVA_FLAGS -cp "$DAIKON_JAR" daikon.diff.Diff $@
}

function daikon_compare() {
    DIR=`dirname "$2"`
    verbose "Comparando '$1' y '$2' con diff Daikon..."
    daikon_diff    "$1" "$2" > "$DIR/diff.txt"
    daikon_diff -u "$1" "$2" > "$DIR/diffU.txt"
}

function daikon_stats() {
    DIR=`dirname "$1"`
    if [ "$#" = 1 ]; then
        verbose "Sacando estadísticas de '$1' con diff -s Daikon..."
        daikon_diff -s "$1" > "$DIR/stats.txt"
    else
        verbose "Sacando estadísticas de '$1' contra '$2' con diff -s Daikon..."
        daikon_diff -s "$1" "$2" > "$DIR/stats.txt"
    fi
}

function system_diff() {
    OUT_REF=`dirname "$1"`/procesoInspeccionado.out
    OUT_OTRO=`dirname "$2"`/procesoInspeccionado.out

    DREF=`dirname "$1"`
    DOTRO=`dirname "$2"`

    # ¡Mantener la consistencia con daikon_compare!
    DIR=`dirname "$2"`
    DDIFF=$DIR/diff.txt
    DDIFFU=$DIR/diffU.txt

    verbose "Comparando '$1' y '$2' con diff sistema..."

    if [ ! -f "$FICHERO_CSV" ]; then
        HEADER_LINE="Ruta inv mutante ; Num Traz ; Num TrazAp ; Traz iden ; TrazAp iden ; Total inv ; Inv dist int ; Inv dist (+noint) ; TPP ; TPP inv dist ; TPP inv dist (+noint)"
        verbose "$HEADER_LINE"
        echo "$HEADER_LINE" >> "$FICHERO_CSV"
    fi

    TINV=$(grep -v ::: "$OUT_OTRO"  | grep -v ================= | wc -l)
    let "TINV -= 5"
    IDI=$(grep -v  ::: "$DDIFF"     | grep -v SUU | grep -v SJJ | grep -v SJU | grep -v SUJ | wc -l)
    IDNI=$(grep -v ::: "$DDIFFU"    | grep -v SUU | grep -v SJJ | grep -v SJU | grep -v SUJ | wc -l)
    TPP=$(grep     ::: "$OUT_OTRO"  | wc -l)
    TPPI=$(daikon_tpp "$DDIFF")
    TPPNI=$(daikon_tpp "$DDIFFU")

    NT=$(ls "$DOTRO"/*[0-9].dtrace|wc -l)
    NTA=$(ls "$DOTRO"/*[0-9].flat.dtrace|wc -l)


    # Comparación de trazas
    # Mientras no se demuestre lo contrario, las trazas son iguales 

    TI=1

	# Guardamos los nombres de ficheros de trazas no aplanadas de referencia en un vector
	CONT=0
	cd "$DREF"
	for F in $(ls *[0-9].dtrace | sort -n)
	do
		VREF[$CONT]="$DREF/$F"
		CONT=$((CONT + 1))
	done
	cd -

	CONT=0
	cd "$DOTRO"
	for F in $(ls *[0-9].dtrace | sort -n)
	do
		VOTRO[$CONT]="$DOTRO/$F"
		CONT=$((CONT + 1))
	done
	cd -
	TOPE=$CONT

	CONT=0
	while [ $CONT -lt $TOPE ]
	do
		cmp -s "${VOTRO[$CONT]}" "${VREF[$CONT]}" || TI=0
		if [ $TI -ne 1 ]
		then
			break
		fi
		CONT=$((CONT + 1))
	done

    # Mientras no se demuestre lo contrario, las trazas aplanadas son iguales 
	TAI=1

	# Guardamos los nombres de ficheros de trazas aplanadas de referencia en un vector
	CONT=0
	cd "$DREF"
	for F in $(ls *[0-9].flat.dtrace | sort -n)
	do
		VREF[$CONT]="$DREF/$F"
		CONT=$((CONT + 1))
	done
	cd -

	CONT=0
	cd "$DOTRO"
	for F in $(ls *[0-9].flat.dtrace | sort -n)
	do
		VOTRO[$CONT]="$DOTRO/$F"
		CONT=$((CONT + 1))
	done
	cd -
	TOPE=$CONT

	CONT=0
	while [ $CONT -lt $TOPE ]
	do
		cmp -s "${VOTRO[$CONT]}" "${VREF[$CONT]}" || TAI=0
		if [ $TAI -ne 1 ]
		then
			break
		fi
		CONT=$((CONT + 1))
	done

    NEW_LINE="$2;$NT;$NTA;$TI;$TAI;$TINV;$IDI;$IDNI;$TPP;$TPPI;$TPPNI"
    verbose "$NEW_LINE"
    echo "$NEW_LINE" >> "$FICHERO_CSV"
}

## CUERPO DEL GUIÓN ############################################################

info "Resultado de ejecutar:\n $CMD_LINE"

# Si sacamos la información del conjunto de referencia no sacamos la
# del resto, y viceversa. Esto nos permitirá ir haciendo las
# comprobaciones uno a uno en vez de dejarlas todas para el final, y
# así podremos ir comprimiendo los directorios de los mutantes sobre
# la marcha.
if [ "$#" -gt 0 ]; then
    for INV_OTRO; do
        if [ ! -r "$INV_OTRO" ]; then continue; fi
        daikon_compare "$INV_REFERENCIA" "$INV_OTRO"
        system_diff  "$INV_REFERENCIA" "$INV_OTRO"
        daikon_stats "$INV_OTRO" "$INV_REFERENCIA"
    done
else
    daikon_compare "$INV_REFERENCIA" "$INV_REFERENCIA"
    system_diff  "$INV_REFERENCIA" "$INV_REFERENCIA"
    daikon_stats "$INV_REFERENCIA"
fi

verbose "Proceso terminado"
