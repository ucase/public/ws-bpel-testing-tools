#!/bin/bash

#
# Simple script for comparing a WS-BPEL compositions against all its mutants
# Copyright (C) 2010 Antonio García-Domínguez
# Licensed under the terms of the GPLv3
#

if [ "$#" != 2 ]; then
  echo "Applies all the mutation operators and generates a file with"
  echo "the name of the mutant and the row resulting from comparing it"
  echo "against the original program with mubpel."
  echo ""
  echo "Usage: $0 (.bpts) (.bpel)"
  exit 1
fi

BPTS=$1
BPEL=$2
ORIGINAL=original.out
LIST_FILES=ficheros.txt
COMPARISONS=comparisons.txt

ActiveBPEL.sh restart

# Apply all the mutation operators everywhere
rm m*.bpel
mubpel applyall "$BPEL"
ls m*.bpel > "$LIST_FILES"

# Run the original and compare all mutants with it
mubpel run "$BPTS" "$BPEL" > "$ORIGINAL"
cat "$LIST_FILES" \
  | xargs mubpel compare "$BPTS" "$BPEL" "$ORIGINAL" \
  | paste "$LIST_FILES" - > "$COMPARISONS"
