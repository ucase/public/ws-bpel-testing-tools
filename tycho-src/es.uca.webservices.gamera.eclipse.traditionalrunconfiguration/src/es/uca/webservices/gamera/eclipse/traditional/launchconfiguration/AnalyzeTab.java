package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration;


import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners.MoveAllOperatorsListener;
import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners.MoveSingleOperatorListener;
import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners.OperatorSelectedListener;
import es.uca.webservices.gamera.eclipse.utilities.PathLocator;

/**
 * This class generates the interface of operator selection of MuBPEL
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class AnalyzeTab extends AbstractLaunchConfigurationTab{
	private List lstAvailable;
	private List lstSelected;
	private Button btnAddAll, btnEraseAll, btnAdd, btnErase;
	private ILaunchConfigurationDialog dialog;
	private ILaunchConfigurationWorkingCopy configuration;
	
	public AnalyzeTab(ILaunchConfigurationDialog dialog){
		this.dialog=dialog;
	}

	@Override
	public void createControl(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		setControl(comp);
		comp.setLayout(new GridLayout(3, false));
		comp.setFont(parent.getFont());
		
		final Label lblAvailable = new Label(comp, SWT.NONE);
		lblAvailable.setText(Messages.AnalyzeTab_1);
		lblAvailable.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, false, false, 1, 1));
		new Label(comp, SWT.NONE);

		final Label lblSelected = new Label(comp, SWT.NONE);
		lblSelected.setText(Messages.AnalyzeTab_2);
		lblSelected.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, false, false));

		//List with the available operators
		lstAvailable = new List(comp, SWT.BORDER | SWT.V_SCROLL);
		lstAvailable.setItems(new String[] {});
		lstAvailable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Composite cmpButtons = new Composite(comp, SWT.NONE);
		cmpButtons.setLayout(new FillLayout(SWT.VERTICAL));
		cmpButtons.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));

		//Button to add all the operators
		btnAddAll= new Button(cmpButtons, SWT.NONE);
		ImageDescriptor addAll;
		try {
			addAll = PathLocator.getFile("/icons/addall.svg"); //$NON-NLS-1$ //$NON-NLS-2$
			btnAddAll.setImage(addAll.createImage());
		} catch (Exception e) {
			btnAddAll.setText(Messages.AnalyzeTab_3);
		}
		btnAddAll.setEnabled(false);
		
		//Button to add an operator
		btnAdd = new Button(cmpButtons, SWT.NONE);

		ImageDescriptor rightArrow;
		try {
			rightArrow = PathLocator.getFile("/icons/rightarrow.svg"); //$NON-NLS-1$ //$NON-NLS-2$
			
			btnAdd.setImage(rightArrow.createImage());
		} catch (Exception e) {
			btnAdd.setText("     ->      "); //$NON-NLS-1$
		}
		btnAdd.setEnabled(false);
		
		//Button to quit an operator
		btnErase = new Button(cmpButtons, SWT.NONE);
		ImageDescriptor leftArrow;
		try {
			leftArrow = PathLocator.getFile("/icons/leftarrow.svg"); //$NON-NLS-1$ //$NON-NLS-2$
			btnErase.setImage(leftArrow.createImage());
		} catch (Exception e) {
			btnErase.setText("     <-      "); //$NON-NLS-1$
		}
		
		btnErase.setEnabled(false);
		
		//Button to quit all operators
		btnEraseAll= new Button(cmpButtons, SWT.NONE);
		ImageDescriptor eraseAll;
		try {
			eraseAll= PathLocator.getFile("/icons/eraseall.svg"); //$NON-NLS-1$ //$NON-NLS-2$
			btnEraseAll.setImage(eraseAll.createImage());
		} catch (Exception e) {
			btnEraseAll.setText(Messages.AnalyzeTab_4);
		}
		btnEraseAll.setEnabled(false);

		//List with the selected operators
		lstSelected = new List(comp, SWT.BORDER | SWT.V_SCROLL);
		lstSelected.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		
		//Add event handlers

		btnAdd.addSelectionListener(new MoveSingleOperatorListener(lstAvailable,lstSelected, new Button[]{btnEraseAll}, new Button[]{btnAdd, btnAddAll}, this, true));
		btnErase.addSelectionListener(new MoveSingleOperatorListener(lstSelected, lstAvailable, new Button[]{btnAddAll}, new Button[]{btnErase, btnEraseAll}, this, false));
		btnAddAll.addSelectionListener(new MoveAllOperatorsListener(lstAvailable,lstSelected, new Button[]{btnEraseAll}, new Button[]{btnAddAll, btnAdd}, this, true));
		btnEraseAll.addSelectionListener(new MoveAllOperatorsListener(lstSelected,lstAvailable, new Button[]{btnAddAll}, new Button[]{btnEraseAll, btnErase}, this, false));
		
		lstAvailable.addSelectionListener(new OperatorSelectedListener(lstSelected, btnAdd, btnErase));
		lstSelected.addSelectionListener(new OperatorSelectedListener(lstAvailable, btnErase, btnAdd));
		
	}
	
	public void setAnalysisResults(AnalysisResults results){
		lstAvailable.removeAll();
		lstSelected.removeAll();
		
		configuration.removeAttribute("lstAvailable"); //$NON-NLS-1$
		configuration.removeAttribute("lstSelected"); //$NON-NLS-1$

		// Vector with the number of lines in which each operator can be applied
		final BigInteger[] biListOperators = results.getLocationCounts();

		// Put the available operators in the ArrayList
		for(int i=0;i<results.getFieldRanges().length;i++){
			if(!biListOperators[i].toString().equals("0")){ //$NON-NLS-1$
				lstAvailable.add(results.getOperatorNames()[i]);
				configuration.setAttribute("lstAvailable", results.getOperatorNames()[i]); //$NON-NLS-1$
				btnAddAll.setEnabled(true);
				btnErase.setEnabled(false);
				btnEraseAll.setEnabled(false);
			}
		}
	}
	
	public ArrayList<String> getSelectedOperators(){
		ArrayList<String> l=new ArrayList<String>();
		l.addAll(Arrays.asList(lstSelected.getItems()));
		return l;
	}

	public ILaunchConfigurationWorkingCopy getLaunchConfiguration(){
		return configuration;
	}
	
	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * This methods loads the last values.
	 */
	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		try {
			if(lstAvailable.getItems().length==0 && lstSelected.getItems().length==0){
				for(Object s:configuration.getAttribute("lstAvailable", new ArrayList<String>())){ //$NON-NLS-1$
					lstAvailable.add((String)s);
					btnAddAll.setEnabled(true);
				}
				for(Object s:configuration.getAttribute("lstSelected", new ArrayList<String>())){ //$NON-NLS-1$
					lstSelected.add((String)s);
					btnEraseAll.setEnabled(true);
				}
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * This method saves the current values.
	 */
	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
			this.configuration=configuration;
			ArrayList<String> selected=new ArrayList<String>();
			ArrayList<String> available=new ArrayList<String>();
			for(String s:lstSelected.getItems()){
				selected.add(s);
			}
			for(String s: lstAvailable.getItems()){
				available.add(s);
			}
			configuration.setAttribute("lstSelected", selected); //$NON-NLS-1$
			configuration.setAttribute("lstAvailable", available); //$NON-NLS-1$
	}
	
	/**
	 * This method resets the interface.
	 */
	public void clear(){
		lstSelected.removeAll();
		lstAvailable.removeAll();
		btnAddAll.setEnabled(false);
		btnEraseAll.setEnabled(false);
		btnErase.setEnabled(false);
		btnAdd.setEnabled(false);
		
	}

	@Override
	public String getName() {
		return Messages.AnalyzeTab_5;
	}
	
	public void activateTab(){
		dialog.setActiveTab(this);
	}
	
	/**
	 * This method checks if the configuration is correct.
	 */
	@Override
	public boolean isValid(ILaunchConfiguration launchConfig) {
		if(lstSelected.getItems().length==0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public void update(){
		this.updateLaunchConfigurationDialog();
	}

}
