package es.uca.webservices.gamera.eclipse.utilities;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.jface.resource.ImageDescriptor;




/**
 * This class find the file using an Eclipse relative path.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class PathLocator {
	public static ImageDescriptor getFile(String relativePath) throws URISyntaxException, IOException{
		URL url = PathLocator.class.getResource(relativePath);
		return ImageDescriptor.createFromURL(url);
	}
}
