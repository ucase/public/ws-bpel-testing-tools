package es.uca.webservices.gamera.eclipse.utilities;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.uca.webservices.gamera.api.GAIndividual;

/**
 * Class with utilities to works with individuals.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class IndividualUtils {
	
	/**
	 * This method returns the index of several operators.
	 */
	private static Map<String, Integer> operatorsToIndex(String[] operators){
		Map<String, Integer> opName2Index = new HashMap<String, Integer>();
		for (int i = 0; i < operators.length; ++i) {
			opName2Index.put(operators[i], i);
		}
		return opName2Index;
	}
	
	/**
	 * This methods constructs the list of GAIndividual generated by the selected operators. 
	 */
	public static List<GAIndividual> getSelectedIndividuals(BigInteger[] biOperatorLocations, BigInteger[] biOperatorRanges,
			String[] operators, Map<String, Integer> mutantsForOperator){
		
		final List<GAIndividual> individuals = new ArrayList<GAIndividual>();
		Map<String, Integer> opName2Index=operatorsToIndex(operators);
		for (final String sOp : operators) {
			final int biIndex = opName2Index.get(sOp);
			final BigInteger biOperatorLocation = biOperatorLocations[biIndex];
			final BigInteger biOperatorRange = biOperatorRanges[biIndex];

			for (BigInteger biLoc = BigInteger.ONE; biLoc.compareTo(biOperatorLocation) <= 0; biLoc = biLoc.add(BigInteger.ONE)) {
				for (BigInteger biAttr = BigInteger.ONE; biAttr.compareTo(biOperatorRange) <= 0; biAttr = biAttr.add(BigInteger.ONE)) {
					final GAIndividual individual = new GAIndividual(BigInteger.valueOf(biIndex + 1), biLoc, biAttr, true);
					individuals.add(individual);

					final int nMutantsForThisOp = mutantsForOperator.containsKey(sOp) ? mutantsForOperator .get(sOp) : 0;
					mutantsForOperator.put(sOp, 1 + nMutantsForThisOp);

				}
			}
		}
		return individuals;
	}

}
