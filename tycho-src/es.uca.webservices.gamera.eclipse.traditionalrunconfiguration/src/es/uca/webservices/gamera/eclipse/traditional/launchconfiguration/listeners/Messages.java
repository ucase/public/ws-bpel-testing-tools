package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners.messages"; //$NON-NLS-1$
	public static String AnalyzeListener_1;
	public static String AnalyzeListener_2;
	public static String AnalyzeListener_3;
	public static String AnalyzeListener_4;
	public static String AnalyzeListener_5;
	public static String AnalyzeListener_6;
	public static String AnalyzeListener_7;
	public static String AnalyzeListener_8;
	public static String TableItemDoubleSelectorListener_1;
	public static String TableItemDoubleSelectorListener_2;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
