package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.List;

/**
 * This listener updates the buttons of the interface when an operator is selected.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class OperatorSelectedListener extends SelectionAdapter{
	
	private List lstToDeselect;
	private Button btnToEnable, btnToDisable;
	
	public OperatorSelectedListener(List lstToDeselect, Button btnToEnable, Button btnToDisable){
		this.lstToDeselect=lstToDeselect;
		this.btnToEnable=btnToEnable;
		this.btnToDisable=btnToDisable;
		
	}
	
	@Override
	public void widgetSelected(SelectionEvent e) {
		btnToEnable.setEnabled(true);
		btnToDisable.setEnabled(false);
		lstToDeselect.deselectAll();
	}
}
