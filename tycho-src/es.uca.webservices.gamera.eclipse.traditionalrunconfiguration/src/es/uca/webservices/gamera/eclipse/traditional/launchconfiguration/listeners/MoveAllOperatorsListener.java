package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners;

import java.util.Arrays;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.List;

import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.AnalyzeTab;

/**
 * This listeners move all the operators to the other category.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class MoveAllOperatorsListener extends SelectionAdapter{
	
	List lstSource, lstDestination;
	Button[] btnToEnable, btnToDisable;
	AnalyzeTab analyze;
	boolean sense;
	
	public MoveAllOperatorsListener(List lstSource, List lstDestination, Button[] btnToEnable, Button[] btnToDisable,
			AnalyzeTab analyze, boolean sense){
		this.lstSource=lstSource;
		this.lstDestination=lstDestination;
		this.btnToEnable=btnToEnable;
		this.btnToDisable=btnToDisable;
		this.analyze=analyze;
		this.sense=sense;
	}
	
	@Override
	public void widgetSelected(SelectionEvent e) {
		//Put all the operator in the selected list
		for(String s:lstSource.getItems())
		{
			lstDestination.add(s);
		}
		//Clear the available list
		lstSource.removeAll();
		
		java.util.List<String> destination=null;
		java.util.List<String> origin=null;
		if(sense){
			destination=Arrays.asList(lstDestination.getItems());
			origin=Arrays.asList(lstSource.getItems());

		}else{
			destination=Arrays.asList(lstSource.getItems());
			origin=Arrays.asList(lstDestination.getItems());
		}
		
		analyze.getLaunchConfiguration().setAttribute("lstSelected", destination);
		analyze.getLaunchConfiguration().setAttribute("lstAvailable", origin);

		
		for(Button b:btnToEnable){
			b.setEnabled(true);
		}
		
		for(Button b:btnToDisable){
			b.setEnabled(false);
		}
		
		analyze.update();
	}
}
