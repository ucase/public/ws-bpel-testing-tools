package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionProcessor;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.util.Pair;
import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.AnalyzeTab;
import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.ConfigureTab;
import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.ProgressMonitor;
import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.TraditionalLaunchConfigurationDelegate;

/**
 * Class which controls the analyze process
 *
 */
public final class AnalyzeListener extends SelectionAdapter {
	
	private GAExecutor inst;
	private AnalyzeTab analyze;
	
	private ConfigureTab configure;

	
	public AnalyzeListener(ConfigureTab configure, AnalyzeTab analyze){
		this.configure=configure;
		this.analyze=analyze;
	}

	@Override
	/**
	 * Analyze button event handler
	 */
	public void widgetSelected(SelectionEvent e) {
		Table tbConfigure=configure.getConfigurationTable();
		final Combo cbImplementations=configure.getImplementations();
		ArrayList<Object> configurationClass=configure.getConfigurationClass();

		final int index = cbImplementations.getSelectionIndex();
		final Object classInstance = configurationClass.get(index);
		try {
			configureGAExecutor(tbConfigure, classInstance);


			Job job = new Job(Messages.AnalyzeListener_2) {
				@Override
				protected IStatus run(final IProgressMonitor monitor) {

					try {
						Display.getDefault().syncExec(new Runnable() {
							@Override
							public void run() {
								monitor.subTask(Messages.AnalyzeListener_4);

							}
						});

						inst.validate();

						ProgressMonitor progressMonitor = new ProgressMonitor(
								monitor, Messages.AnalyzeListener_3, Integer.MAX_VALUE);
						final AnalysisResults results=inst.analyze(progressMonitor);
						Display.getDefault().syncExec(new Runnable() {
							@Override
							public void run() {
								analyze.setAnalysisResults(results);
								monitor.done();
								analyze.activateTab();

							}
						});


						Display.getDefault().syncExec(new Runnable() {
							@Override
							public void run() {
								try {
									PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
									.showView("es.uca.webservices.gamera.eclipse.mutantcomparison.views.MutantComparisonView");

								} catch (PartInitException e) {

									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						});

						//Send the analysis results to other plugins
						BundleContext ctx = FrameworkUtil.
								getBundle(TraditionalLaunchConfigurationDelegate.class).getBundleContext();
						ServiceReference<EventAdmin> ref = ctx.getServiceReference(EventAdmin.class);
						EventAdmin eventAdmin = ctx.getService(ref);
						Map<String, Pair<AnalysisResults, String[]>> properties = 
								new HashMap<String, Pair<AnalysisResults, String[]>>();

						properties.put("ANALYSISRESULTS", new Pair<AnalysisResults, String[]>(results, //$NON-NLS-1$
								new String[]{inst.getOriginalProgramFile().getAbsolutePath(), classInstance.getClass().getName()}));


						Event event = new Event("analysiscommunication/asyncEvent", properties); //$NON-NLS-1$
						eventAdmin.postEvent(event);

					} catch (Exception e) {
						final Status status = 
								new Status(IStatus.ERROR, "SCS Admin", e.getLocalizedMessage(), e); //$NON-NLS-1$

						Display.getDefault().syncExec(new Runnable() {
							@Override
							public void run() {
								ErrorDialog errorDialog=new ErrorDialog(analyze.getControl().getShell(), 
										Messages.AnalyzeListener_5, Messages.AnalyzeListener_6, status, Status.ERROR);
								errorDialog.open();
							}
						});
					} 

					return Status.OK_STATUS;
				}
			};

			// Start the Job
			job.schedule(); 
		} catch (Exception e1) {
			//Create the error dialog
			final Status status = 
					new Status(IStatus.ERROR, "SCS Admin", e1.getLocalizedMessage(), e1); //$NON-NLS-1$

			Display.getDefault().syncExec(new Runnable() {
				@Override
				public void run() {
					configure.getLaunchConfiguration().removeAttribute("lstAvailable");
					configure.getLaunchConfiguration().removeAttribute("lstSelected");
					configure.getAnalyzeTab().clear();
					ErrorDialog errorDialog=new ErrorDialog(analyze.getControl().getShell(), 
							Messages.AnalyzeListener_7, Messages.AnalyzeListener_8, status, Status.ERROR);
					errorDialog.open();

				}
			});
		} 
	}

	/**
	 * Method that configure the GAExecutor with the selected files
	 * @param tableItems Parameters of the table
	 * @param className String with the name of selected class
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	private void configureGAExecutor(Table table, Object classInstance) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		Class<?> selectedClass = classInstance.getClass();

		//Create an instance of the selected class
		inst = (GAExecutor) classInstance;

		final Map<Method, Option> options = OptionProcessor.listOptions(selectedClass);

		//Find the configure methods and invoke them with the selected files
		for (final Method m : selectedClass.getMethods()) {

			if (options.containsKey(m)) {
				String type=options.get(m).type().toString();
				if(type.equals("PATH_LOAD") || type.equals("PATH_SAVE")){ //$NON-NLS-1$ //$NON-NLS-2$
					String sPath=null;
					for(int i=0;i<table.getItemCount();i++){
						if(table.getItem(i).getText(1).equals(m.getName())){
							sPath = table.getItem(i).getText(2);
						}
					}


					if(!sPath.equals("")){ //$NON-NLS-1$
						IPath path=new Path(sPath);
						IFile file= ResourcesPlugin.getWorkspace().getRoot().getFile(path);	
						m.invoke(inst, new Object[] { file.getRawLocation().makeAbsolute().toString()});
					}
				}
			}
		}


	}
}