package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.ConfigureTab;

/**
 * Class which controls the selection of items
 *
 */
public final class TableItemSelectionListener extends SelectionAdapter {
	private final TableEditor editor;
	private ConfigureTab configure;
	
	/**
	 * Constructor of the class
	 * @param btnAnalyze Button of analysis
	 * @param editor TableEditor to write in the table
	 */
	public TableItemSelectionListener(ConfigureTab configure, TableEditor editor) {
		this.configure=configure;
		this.editor=editor;
	}
	
	/**
	 * Write into the table event handler
	 */
	@Override
	public void widgetSelected(SelectionEvent e) {
		final Button btnAnalyze=configure.getAnalyzeButton();
		final Table tbConfigure=configure.getConfigurationTable();
		TableItem item = (TableItem) e.item;
        Text newEditor = new Text(tbConfigure, SWT.NONE);
        if(item!=null){
	        newEditor.setText(item.getText(2));
	        newEditor.addModifyListener(new ModifyListener() {
	          public void modifyText(ModifyEvent me) {
	            Text text = (Text) editor.getEditor();
	            editor.getItem().setText(2, text.getText());
	            configure.getLaunchConfiguration().setAttribute(editor.getItem().getText(1), text.getText());
	            configure.getLaunchConfiguration().removeAttribute("lstAvailable");
				configure.getLaunchConfiguration().removeAttribute("lstSelected");
				configure.getAnalyzeTab().clear();
	            
		        //Activate the analysis button
				if(configure.isValidTable()){
					btnAnalyze.setEnabled(true);
				}
				else{
					btnAnalyze.setEnabled(false);
				}
				configure.update();
	          }
	        });
	        newEditor.selectAll();
	        newEditor.setFocus();
	        editor.setEditor(newEditor, item, 2);
        }
	}
}