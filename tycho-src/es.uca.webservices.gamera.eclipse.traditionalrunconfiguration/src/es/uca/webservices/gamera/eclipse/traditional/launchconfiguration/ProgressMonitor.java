package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration;

import org.eclipse.core.runtime.IProgressMonitor;

import es.uca.webservices.gamera.api.exec.GAProgressMonitor;

/**
 * Class which implements the GAExecutor; it is called after a big operation
 *
 */
public class ProgressMonitor implements GAProgressMonitor{
	
	private IProgressMonitor progressMonitor;
	private int worked=0;
	private int maxValue;
	private double increment=0;
	
	/**
	 * Constructor of the class
	 * @param progress Dialog which show the process of the operation
	 */
	public ProgressMonitor(IProgressMonitor progressMonitor, String taskName, int maxValue){
		this.progressMonitor=progressMonitor;
		this.maxValue=maxValue;
		this.progressMonitor.beginTask(taskName, maxValue);
	}

	@Override
	/**
	 * Method that update the progress bar with the actual progress
	 */
	public void done(final int value) {
		worked+=(int)(increment*value);
		progressMonitor.worked(worked);
	}

	@Override
	/**
	 * Method which return true if the process has been cancelled
	 */
	public boolean isCancelled() {
		return progressMonitor.isCanceled();
	}

	@Override
	/**
	 * Method which set the max value of the progress bar
	 */
	public void start(final int maxValue) {
		increment=((double)(this.maxValue - worked))/maxValue;
		
	}

	@Override
	/**
	 * Method which update the message that the dialog shows
	 */
	public void status(final String message) {
		progressMonitor.subTask(message);

		
	}
	
	public void setWorked(int worked){
		this.worked=worked;
		progressMonitor.worked(worked);
	}
}
