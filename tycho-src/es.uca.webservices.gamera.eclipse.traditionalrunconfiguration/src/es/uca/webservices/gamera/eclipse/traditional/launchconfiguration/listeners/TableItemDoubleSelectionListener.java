package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;


import es.uca.webservices.gamera.eclipse.dialog.ExtensionValidator;
import es.uca.webservices.gamera.eclipse.dialog.TypeViewerFilter;
import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.ConfigureTab;

/**
 * Class which controls the selection of files into the  items
 *
 */
public final class TableItemDoubleSelectionListener implements Listener {
	private final ArrayList<List<String>>alExtensions;
	private final ConfigureTab configure;

	
	/**
	 * Constructor of the class
	 * @param btnAnalyze Button of analysis
	 */
	public TableItemDoubleSelectionListener(ConfigureTab configure, ArrayList<List<String>>alExtensions) {
		this.alExtensions=alExtensions;
		this.configure=configure;
	}

	/**
	 * Load file event handler
	 */
	@Override
	public void handleEvent(Event arg0) {
		Button btnAnalyze=configure.getAnalyzeButton();
		Table tbConfigure=configure.getConfigurationTable();
		
		ElementTreeSelectionDialog select=new ElementTreeSelectionDialog(new Shell(), new WorkbenchLabelProvider(),
			    new BaseWorkbenchContentProvider());
		select.setTitle(Messages.TableItemDoubleSelectorListener_1);
		select.setMessage(Messages.TableItemDoubleSelectorListener_2);
		select.setInput(ResourcesPlugin.getWorkspace().getRoot());
		int index = tbConfigure.getSelectionIndex();
		select.addFilter(new TypeViewerFilter(alExtensions.get(index)));
		select.setAllowMultiple(false);
		select.setValidator(new ExtensionValidator());
		select.open();
		if(select.getResult()!=null){
			String path=((IFile)select.getResult()[0]).getFullPath().toString();
			tbConfigure.getItem(index).setText(2, path);
			configure.getLaunchConfiguration().setAttribute(tbConfigure.getItem(index).getText(1), path);
			configure.getLaunchConfiguration().removeAttribute("lstAvailable");
			configure.getLaunchConfiguration().removeAttribute("lstSelected");
			configure.getAnalyzeTab().clear();
		}

		//Activate the analysis button
		if(configure.isValidTable()){
			btnAnalyze.setEnabled(true);
			configure.update();
		}
		
		
		
	}
}