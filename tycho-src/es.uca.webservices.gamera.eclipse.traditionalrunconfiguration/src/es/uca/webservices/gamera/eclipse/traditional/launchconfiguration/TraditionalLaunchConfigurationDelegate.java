package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionProcessor;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.api.util.Pair;
import es.uca.webservices.gamera.eclipse.utilities.IndividualUtils;

/**
 * This class runs the launch configurations which configures MuBPEL.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class TraditionalLaunchConfigurationDelegate implements ILaunchConfigurationDelegate {
	private GAExecutor inst=null;

	@Override
	public void launch(ILaunchConfiguration configuration, String mode,
			ILaunch launch, final IProgressMonitor monitor) throws CoreException {
		try{

			final ProgressMonitor progress= new ProgressMonitor(monitor, Messages.TraditionalLaunchConfigurationDelegate_1,
						Integer.MAX_VALUE);
	

			monitor.subTask(Messages.TraditionalLaunchConfigurationDelegate_2);	
			
			if(configuration.getAttribute("lstSelected", new ArrayList<String>()).isEmpty()){ //$NON-NLS-1$
				throw new Exception();
			}
			
			ExtensionPointExplorer extensionPointExplorer=new ExtensionPointExplorer("es.uca.webservices.gamera.eclipse.executor"); //$NON-NLS-1$
			

			for(Object o:extensionPointExplorer.explore()){
				if(o.getClass().getName().equals(configuration.getAttribute("cbImplementations", ""))){ //$NON-NLS-1$ //$NON-NLS-2$
					inst = (GAExecutor) o;
				}
			}
			
			
			if(inst!=null){
				
				
				final Map<Method, Option> options = OptionProcessor.listOptions(inst.getClass());
				
				//Find the configure methods and invoke them with the selected files
				for (final Method m : inst.getClass().getMethods()) {
					if (options.containsKey(m)) {
						String type=options.get(m).type().toString();
						if(type.equals("PATH_LOAD") || type.equals("PATH_SAVE")){ //$NON-NLS-1$ //$NON-NLS-2$
							IPath path=new Path(configuration.getAttribute(m.getName(), "")); //$NON-NLS-1$
							IFile file= ResourcesPlugin.getWorkspace().getRoot().getFile(path);	
							m.invoke(inst, new Object[] { file.getRawLocation().makeAbsolute().toString()});
						}
					}
				}
				inst.prepare();
				AnalysisResults results=inst.analyze(null);

				final BigInteger[] biOperatorLocations = results.getLocationCounts();
				final BigInteger[] biOperatorRanges = results.getFieldRanges();
						
				
				final Map<String, Integer> mutantsForOperator = new HashMap<String, Integer>();
				final List<GAIndividual> individuals = IndividualUtils.getSelectedIndividuals(biOperatorLocations, biOperatorRanges,
						results.getOperatorNames(), mutantsForOperator);
				
				final ArrayList<GAIndividual> selectedIndividuals=new ArrayList<GAIndividual>();
				Map<Integer, String> operatorsDictionary=new HashMap<Integer, String>();
				for(Object op1:configuration.getAttribute("lstSelected",new ArrayList<String>())){ //$NON-NLS-1$
					int cont=1;
					for(int i=0; i<results.getOperatorNames().length && !op1.toString().equals(results.getOperatorNames()[i]);i++){
						cont++;
					}
					operatorsDictionary.put(cont, op1.toString());
					for(GAIndividual ind:individuals){
						if(ind.getOperator().get(0).intValue()==cont){
							selectedIndividuals.add(ind);
						}
					}
				}

				progress.setWorked(Integer.MAX_VALUE/3);

				
				final ComparisonResults[] allCompResults = inst.compare(progress,
						selectedIndividuals.toArray(new GAIndividual[selectedIndividuals.size()]));

				
				Display.getDefault().syncExec(new Runnable() {
					@Override
					public void run() {
						try {
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
								.showView("es.uca.webservices.gamera.eclipse.results.views.ResultsView");
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
								.showView("es.uca.webservices.gamera.eclipse.mutationstatistics.views.StatisticsView");

						} catch (PartInitException e) {
						
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
					}
				});
				
				BundleContext ctx = FrameworkUtil.
						getBundle(TraditionalLaunchConfigurationDelegate.class).getBundleContext();
		        ServiceReference<EventAdmin> ref = ctx.getServiceReference(EventAdmin.class);
		        EventAdmin eventAdmin = ctx.getService(ref);
		        List<ComparisonResults[]> generation=new ArrayList<ComparisonResults[]>();
		        generation.add(allCompResults);
		        Map<String,Pair <List<ComparisonResults[]>, Map<Integer, String>>> properties = 
		        		new HashMap<String, Pair <List<ComparisonResults[]>,
		        		Map<Integer, String>>>();
		        properties.put("MUTATIONRESULTS",  //$NON-NLS-1$
		        		new Pair<List<ComparisonResults[]>, Map<Integer, String>>( generation, operatorsDictionary));

		     
		        Event event = new Event("mutationcommunication/asyncEvent", properties); //$NON-NLS-1$

		        eventAdmin.postEvent(event);
				inst.cleanup();
				Display.getDefault().syncExec(new Runnable() {
					@Override
					public void run() {
						//eclipseProgress.close();
					}
				});
				
			}
		}catch (Exception e) {
			final Status status = 
			        new Status(IStatus.ERROR, "SCS Admin", e.getMessage(), e); //$NON-NLS-1$
			Display.getDefault().syncExec(new Runnable() {
				@Override
				public void run() {
					ErrorDialog errorDialog=new ErrorDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(), 
							Messages.TraditionalLaunchConfigurationDelegate_3, Messages.TraditionalLaunchConfigurationDelegate_4, status, Status.ERROR);
					//eclipseProgress.close();
					try {
						inst.cleanup();
					} catch (PreparationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					errorDialog.open();
				}
			});
			//e.printStackTrace();
		}
		
	}

}
