package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionProcessor;
import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.ConfigureTab;

/**
 * This listener updates the interface when an implementation is selected.
 */
public class PluginSelectedAdapter extends SelectionAdapter {
	private final TableEditor editor;
	private final List<Object> configurationClass;
	private ConfigureTab configure;

	/**
	 * Constructor of the class
	 * @param btnAnalyze Button of analysis
	 * @param btnLoadConfiguration Button of load configuration
	 * @param editor TableEditor to the event handler
	 */
	public PluginSelectedAdapter(ConfigureTab configure, List<Object> configurationClass, TableEditor editor) {
		this.editor=editor;
		this.configurationClass=configurationClass;
		this.configure=configure;
	}

	/**
	 * Create an instance of the selected class and Update the table
	 */
	@Override
	public void widgetSelected(SelectionEvent e) {
		
		Button btnAnalyze=configure.getAnalyzeButton();
		Table tbConfigure=configure.getConfigurationTable();
		Combo availableClass=configure.getImplementations();

		//Clear the table from previous runs
		if(tbConfigure.getListeners(SWT.Selection).length>0){
			tbConfigure.removeListener(SWT.Selection, tbConfigure.getListeners(SWT.Selection)[0]);
		}
		if(tbConfigure.getListeners(SWT.MouseDoubleClick).length>0){
			tbConfigure.removeListener(SWT.MouseDoubleClick, tbConfigure.getListeners(SWT.MouseDoubleClick)[0]);
		}
		tbConfigure.removeAll();
		
		configure.getAnalyzeTab().clear();
		if(configure.getLaunchConfiguration()!=null && configure.isReadyToClean()){
			configure.getLaunchConfiguration().removeAttribute("lstSelected");
			configure.getLaunchConfiguration().removeAttribute("lstAvailable");
		}
		
		btnAnalyze.setEnabled(false);
		final Class<?> selectedClass = configurationClass.get(availableClass.getSelectionIndex()).getClass();
		//Update the table with the configure methods of selected class
		addConfigureItems(tbConfigure, selectedClass, btnAnalyze, editor);
		if(configure.getLaunchConfiguration()!=null){
			configure.getLaunchConfiguration().setAttribute("cbImplementations", availableClass.getText());
		}
	}

	/**
	 * Method that update the table with the configure methods of selected class
	 * @param tbConfigure Table to be update
	 * @param selectedClass Name of the selected class
	 * @param btnAnalyze Button of analysis
	 * @param editor TableEditor to the event handler
	 */
	private void addConfigureItems(final Table tbConfigure,	
			final Class<?> selectedClass, final Button btnAnalyze, final TableEditor editor) {
		ArrayList<List<String>>alExtensions = new ArrayList<List<String>>();
		final Map<Method, Option> options = OptionProcessor.listOptions(selectedClass);
		tbConfigure.setItemCount(0);
		
		// Find the configured methdos
		
		
		List<Method> methods=Arrays.asList(selectedClass.getMethods());
		Collections.sort(methods, new Comparator<Method>(){
		     public int compare(Method m1, Method m2){
		    	 return m1.getName().compareTo(m2.getName());
		     }
		});
		for (final Method m : methods) {
			if (options.containsKey(m)) {
				

				Option methodOptions = options.get(m);
				String type=methodOptions.type().toString();
				//　If it is a configure method, add to the table
				if(type.equals("PATH_LOAD") || type.equals("PATH_SAVE")){
					//alMethodNames.add(m.getName());
					final int newItemCount = tbConfigure.getItemCount()+1;
					tbConfigure.setItemCount(newItemCount);
					TableItem tableItem = tbConfigure.getItem(newItemCount - 1);
					
					if(methodOptions.optional()){
						tableItem.setText(0,"*");
					}
					try{
						tableItem.setText(1, OptionProcessor.getShortDescription(m));
					}catch(MissingResourceException e){
						tableItem.setText(1, m.getName());
					}
					
					tableItem.setText(2, "");

					alExtensions.add(Arrays.asList(methodOptions.fileExtensions()));
					
				}
			}
		}
		for (TableColumn col : tbConfigure.getColumns()) {
			col.pack();
		}

		// Add the event handlers
		tbConfigure.addListener(SWT.MouseDoubleClick, new TableItemDoubleSelectionListener(configure, alExtensions));
		tbConfigure.addSelectionListener(new TableItemSelectionListener(configure, editor));
	}
}