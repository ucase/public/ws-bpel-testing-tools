package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration;



import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;

/**
 * This class creates the tabs of the launch configuration of MuBPEL.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class TraditionalMutationTabGroup extends AbstractLaunchConfigurationTabGroup{
	

	@Override
	public void createTabs(ILaunchConfigurationDialog dialog, String mode) {  
		AnalyzeTab analyze=new AnalyzeTab(dialog);
		ILaunchConfigurationTab[] tabs = new ILaunchConfigurationTab[] {
				new ConfigureTab(analyze),
				analyze,
				new CommonTab()
		};
		setTabs(tabs);
		
	}

}
