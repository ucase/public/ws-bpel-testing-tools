package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionProcessor;
import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners.AnalyzeListener;
import es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.listeners.PluginSelectedAdapter;
import es.uca.webservices.gamera.eclipse.utilities.PathLocator;

/**
 * This class generates the interface of configuration of MuBPEL
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class ConfigureTab extends AbstractLaunchConfigurationTab{
	private Combo cbImplementations;
	private Table tbConfigure;
	private ArrayList<Object> configurationClass;
	private Button btnAnalyze;
	private AnalyzeTab analyze;
	private ILaunchConfigurationWorkingCopy configuration;
	private boolean readyToClean=true;
	
	public ConfigureTab(AnalyzeTab analyze){
		this.analyze=analyze;
	}

	@Override
	public void createControl(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		setControl(comp);
		comp.setLayout(new GridLayout(2, true));
		comp.setFont(parent.getFont());
		
		
		final Label lblUploadFiles = new Label(comp, SWT.NONE);
		lblUploadFiles.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		lblUploadFiles.setText(Messages.ConfigureTab_0);

		//Combo to select the implementation
		cbImplementations = new Combo(comp, SWT.NONE);
		cbImplementations.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		ExtensionPointExplorer extensionPointExplorer=new ExtensionPointExplorer("es.uca.webservices.gamera.eclipse.executor"); //$NON-NLS-1$
		configurationClass=extensionPointExplorer.explore();
		for(Object conf:configurationClass){
			cbImplementations.add(conf.getClass().getName());
		}

		//Table to configure the GAExecutor
		tbConfigure = new Table(comp, SWT.BORDER | SWT.VIRTUAL);
		tbConfigure.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		tbConfigure.setHeaderVisible(true);
		tbConfigure.setToolTipText(""); //$NON-NLS-1$
		tbConfigure.setLinesVisible(true);
		tbConfigure.setItemCount(0);
		
		//TableEditor to write into the table
		final TableEditor editor = new TableEditor(tbConfigure);
		
		//Column with the name of the method
		final TableColumn tbcolOptional = new TableColumn(tbConfigure, SWT.NONE);
		tbcolOptional.setResizable(false);
		tbcolOptional.setText("   "); //$NON-NLS-1$
		tbcolOptional.pack();

		//Column with the name of the method
		final TableColumn tbcolFile = new TableColumn(tbConfigure, SWT.NONE);
		tbcolFile.setResizable(false);
		tbcolFile.setText(Messages.ConfigureTab_4);
		tbcolFile.pack();

		//Column with the paths
		final TableColumn tbcolPath = new TableColumn(tbConfigure, SWT.NONE);
		tbcolPath.setResizable(false);
		tbcolPath.setText(Messages.ConfigureTab_5);
		tbcolPath.pack();

		//Button to analyze
		btnAnalyze = new Button(comp, SWT.NONE);
		btnAnalyze.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		btnAnalyze.setEnabled(true);
		ImageDescriptor fAnalyze;
		try {
			fAnalyze = PathLocator.getFile("/icons/analysis.svg"); //$NON-NLS-1$ //$NON-NLS-2$
			btnAnalyze.setImage(fAnalyze.createImage());
		} catch (Exception e) {
			
		}
		btnAnalyze.setText(Messages.ConfigureTab_8);
		btnAnalyze.addSelectionListener(new AnalyzeListener(this, analyze));		
		cbImplementations.addSelectionListener(new PluginSelectedAdapter(this,configurationClass, editor));
		
		/*try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView("es.uca.webservices.gamera.eclipse.results.views.ResultsView");
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */
		
	}
	
	public Combo getImplementations(){
		return cbImplementations;
	}
	
	public ArrayList<Object> getConfigurationClass(){
		return configurationClass;
	}
	
	public Table getConfigurationTable(){
		return tbConfigure;
	}
	
	public Button getAnalyzeButton(){
		return btnAnalyze;
	}
	
	public ILaunchConfigurationWorkingCopy getLaunchConfiguration(){
		return configuration;
	}
	
	public AnalyzeTab getAnalyzeTab(){
		return analyze;
	}
	
	public boolean isReadyToClean(){
		return readyToClean;
	}
	
	public void setReadyToClean(boolean state){
		readyToClean=state;
	}
	

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
	}

	/**
	 * This method loads the last values.
	 */
	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		try {
			
			analyze.performApply(configuration.getWorkingCopy());
			
			if(tbConfigure.getListeners(SWT.Selection).length>0){
				tbConfigure.removeListener(SWT.Selection, tbConfigure.getListeners(SWT.Selection)[0]);
			}
			if(tbConfigure.getListeners(SWT.MouseDoubleClick).length>0){
				tbConfigure.removeListener(SWT.MouseDoubleClick, tbConfigure.getListeners(SWT.MouseDoubleClick)[0]);
			}
			tbConfigure.removeAll();
			
			
			String selected=configuration.getAttribute("cbImplementations", ""); //$NON-NLS-1$ //$NON-NLS-2$
			if(selected!=""){ //$NON-NLS-1$
				int count=0;
				for(String s:cbImplementations.getItems()){
					if(s.equals(selected)){
						cbImplementations.select(count);
						this.setReadyToClean(false);
						cbImplementations.notifyListeners(SWT.Selection, new Event());
						this.setReadyToClean(true);
						Object conf=configurationClass.get(count);
						boolean complete=true;
						for (final Method m : conf.getClass().getMethods()) {
							final Map<Method, Option> options = OptionProcessor.listOptions(conf.getClass());
						
							if (options.containsKey(m)) {

								Option methodOptions = options.get(m);
								String type=methodOptions.type().toString();
								//　If it is a configure method, add to the table
								if(type.equals("PATH_LOAD") || type.equals("PATH_SAVE")){ //$NON-NLS-1$ //$NON-NLS-2$
									String path=configuration.getAttribute(m.getName(), ""); //$NON-NLS-1$
									if(path=="" && !methodOptions.optional()){ //$NON-NLS-1$ //$NON-NLS-2$
										complete=false;
									}
									
									for(TableItem item:tbConfigure.getItems()){
										if(item.getText(1).equals(m.getName())){
											item.setText(2, path);
											if(methodOptions.optional()){
												item.setText(0,"*"); //$NON-NLS-1$
											}
										}
									}
									
									
								}
							}
						}
						if(complete){
							btnAnalyze.setEnabled(true);
						}
					}
					else{
						count++;
					}
				}
			}
			else{
				//Default values
				cbImplementations.setText(""); //$NON-NLS-1$
			}
			
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * This method saves the current values.
	 */
	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		this.configuration=configuration;
		configuration.setAttribute("cbImplementations", cbImplementations.getText()); //$NON-NLS-1$
		for(Object conf:configurationClass){
			if(conf.getClass().getName().equals(cbImplementations.getText())){
				for (final Method m : conf.getClass().getMethods()) {
					final Map<Method, Option> options = OptionProcessor.listOptions(conf.getClass());
				
					if (options.containsKey(m)) {

						Option methodOptions = options.get(m);
						String type=methodOptions.type().toString();
						//　If it is a configure method, add to the table
						if(type.equals("PATH_LOAD") || type.equals("PATH_SAVE")){ //$NON-NLS-1$ //$NON-NLS-2$
							for(TableItem item:tbConfigure.getItems()){
								if(item.getText(1).equals(m.getName())){
									configuration.setAttribute(m.getName(), item.getText(2));

								}
							}
						}
					}
				}
			}
		}
	}

	
	@Override
	public String getName() {
		return Messages.ConfigureTab_22;
	}
	
	public boolean isValidTable(){
		for(int i=0;i<tbConfigure.getItemCount();i++){
			if(!tbConfigure.getItem(i).getText(0).equals("*") &&
					tbConfigure.getItem(i).getText(2).equals("")){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * This methods checks if the currenct configuration is valid.
	 */
	@Override
	public boolean isValid(ILaunchConfiguration launchConfig) {
		
		return isValidTable() && analyze.isValid(analyze.getLaunchConfiguration());
	}
	
	public void update(){
		this.updateLaunchConfigurationDialog();
	}

}
