package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration;

import java.util.ArrayList;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.RegistryFactory;

/**
 * This class returns all the class which are accessible by the extension points.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class ExtensionPointExplorer {
	private String extensionPointID;
	
	public ExtensionPointExplorer(String extensionPointID){
		this.extensionPointID=extensionPointID;
	}
	
	public ArrayList<Object> explore(){
		IExtensionRegistry registry = RegistryFactory.getRegistry();

		IConfigurationElement[] config =
		        registry.getConfigurationElementsFor(extensionPointID);
		ArrayList<Object> configurationClass=new ArrayList<Object>();
		for(IConfigurationElement c: config){
			try {
				final Object o =c.createExecutableExtension("class");

				configurationClass.add(o);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return configurationClass;
		   
	}

}


