package es.uca.webservices.gamera.eclipse.traditional.launchconfiguration;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "es.uca.webservices.gamera.eclipse.traditional.launchconfiguration.messages"; //$NON-NLS-1$
	public static String AnalyzeTab_1;
	public static String AnalyzeTab_2;
	public static String AnalyzeTab_3;
	public static String AnalyzeTab_4;
	public static String AnalyzeTab_5;
	public static String ConfigureTab_0;
	public static String ConfigureTab_22;
	public static String ConfigureTab_4;
	public static String ConfigureTab_5;
	public static String ConfigureTab_8;
	public static String EclipseDialogProgressMonitor_1;
	public static String TraditionalLaunchConfigurationDelegate_0;
	public static String TraditionalLaunchConfigurationDelegate_1;
	public static String TraditionalLaunchConfigurationDelegate_2;
	public static String TraditionalLaunchConfigurationDelegate_3;
	public static String TraditionalLaunchConfigurationDelegate_4;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
