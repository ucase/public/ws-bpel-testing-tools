package es.uca.webservices.gamera.eclipse.dialog;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

/**
 * This class filters the files that are showed in the dialog.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class TypeViewerFilter extends ViewerFilter{
	
	private List<String> extensions;
	
	public TypeViewerFilter(List<String> extensions){
		this.extensions=extensions;
	}

	/**
	 * This method determines if a item must appears in the dialog
	 */
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		//If the element is a file, we check if the extension is one of the required extensions.
		if(element instanceof IFile){
			for(String extension:extensions){
				if(((IFile)element).getFileExtension().equals(extension)){
					return true;
				}
			}
			return false;
			
		}
		// If the element isn't a file, we use the recursive method explore to determine
		// if the element contains file with the required extension.
		else {
			try {
				return explore(element);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return true;
	}
	
	private boolean explore(Object element) throws CoreException{
		// If the element is a project or a folder, we explore recursively his
		// childs and if contents a file with the required extension, we show the
		// project or folder in the dialog.
		if(element instanceof IProject){
			for(IResource r:((IProject)element).members()){
				for(String extension:extensions){
					if(r.getFileExtension()!=null && 
							r.getFileExtension().equals(extension)){
						return true;
					}
				}
				if(!(r instanceof IFile)){
					return explore(r);
				}
			}
		}
		if(element instanceof IFolder){
			
			for(IResource r:((IFolder)element).members()){
				for(String extension:extensions){
					if(r.getFileExtension()!=null && 
							r.getFileExtension().equals(extension)){
						return true;
					}
				}
				
				if(!(r instanceof IFile)){
					return explore(r);
				}
			}
		}
		return false;

	}

}
