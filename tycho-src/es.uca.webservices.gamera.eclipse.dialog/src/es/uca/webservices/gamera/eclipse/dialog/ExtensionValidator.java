package es.uca.webservices.gamera.eclipse.dialog;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

/**
 * This class check if the selected item in the dialog is a file.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class ExtensionValidator implements ISelectionStatusValidator{

	@Override
	public IStatus validate(Object[] selection) {
		
		for(Object o:selection){
			if(!(o instanceof IFile)){
				Status status=new Status(IStatus.ERROR, "es.uca.webservices.gamera.eclipse.dialog",  //$NON-NLS-1$
						Messages.ExtensionValidator_1);
				return status;
			}
		}
		Status status=new Status(IStatus.OK, "es.uca.webservices.gamera.eclipse.dialog", ""); //$NON-NLS-1$ //$NON-NLS-2$
		return status;
	}

}
