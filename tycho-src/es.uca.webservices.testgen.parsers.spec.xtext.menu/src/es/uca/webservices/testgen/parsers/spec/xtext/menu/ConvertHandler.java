package es.uca.webservices.testgen.parsers.spec.xtext.menu;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Shell;

public class ConvertHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Shell shell = new Shell();
		WindowSpec spec = new WindowSpec(shell);
		spec.create();
		spec.open();
		return null;
	}
}