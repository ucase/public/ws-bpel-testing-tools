package es.uca.webservices.testgen.parsers.spec.xtext.menu;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;

import es.uca.webservices.testgen.TestGeneratorRun;

public class WindowSpec extends TitleAreaDialog{

	public WindowSpec(Shell parentShell) {
		super(parentShell);
	}

	protected Shell shlGenerateTestcase;
	private Text txtFileName;
	private Text txtStrategyName;

	/**
	 * Open the window.
	 * @return 
	 */
	public int open() {
		Display display = Display.getDefault();
		createContents();
		shlGenerateTestcase.open();
		shlGenerateTestcase.layout();
		while (!shlGenerateTestcase.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return 0;
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlGenerateTestcase = new Shell();
		shlGenerateTestcase.setSize(450, 259);
		shlGenerateTestcase.setText("Generate Test-Case");
		shlGenerateTestcase.setLayout(new FormLayout());

		Button btnAccept = new Button(shlGenerateTestcase, SWT.NONE);

		FormData fd_btnAccept = new FormData();
		fd_btnAccept.bottom = new FormAttachment(100, -34);
		fd_btnAccept.left = new FormAttachment(0, 192);
		btnAccept.setLayoutData(fd_btnAccept);
		btnAccept.setText("Accept");

		CLabel lblNumCase = new CLabel(shlGenerateTestcase, SWT.NONE);
		FormData fd_lblNumCase = new FormData();
		fd_lblNumCase.top = new FormAttachment(0, 49);
		fd_lblNumCase.left = new FormAttachment(0, 46);
		lblNumCase.setLayoutData(fd_lblNumCase);
		lblNumCase.setText("#Cases");

		CLabel lblFormatter = new CLabel(shlGenerateTestcase, SWT.NONE);
		FormData fd_lblFormatter = new FormData();
		fd_lblFormatter.top = new FormAttachment(lblNumCase, 6);
		fd_lblFormatter.left = new FormAttachment(lblNumCase, 0, SWT.LEFT);
		lblFormatter.setLayoutData(fd_lblFormatter);
		lblFormatter.setText("Formatter");

		final Combo cmbFormater = new Combo(shlGenerateTestcase, SWT.READ_ONLY);
		cmbFormater.setItems(new String[] { "Apache Velocity", "CSV" });
		FormData fd_cmbFormater = new FormData();
		fd_cmbFormater.top = new FormAttachment(0, 78);
		fd_cmbFormater.left = new FormAttachment(lblFormatter, 114);
		cmbFormater.setLayoutData(fd_cmbFormater);
		cmbFormater.select(1);

		CLabel lblFileName = new CLabel(shlGenerateTestcase, SWT.NONE);
		lblFileName.setText("Filename");
		FormData fd_lblFileName = new FormData();
		fd_lblFileName.top = new FormAttachment(lblFormatter, 12);
		fd_lblFileName.left = new FormAttachment(lblNumCase, 0, SWT.LEFT);
		lblFileName.setLayoutData(fd_lblFileName);

		txtFileName = new Text(shlGenerateTestcase, SWT.BORDER | SWT.RIGHT);
		fd_cmbFormater.right = new FormAttachment(txtFileName, 0, SWT.RIGHT);
		txtFileName.setToolTipText("name of the file");
		txtFileName.setText("TestCase");
		FormData fd_txtFileName = new FormData();
		fd_txtFileName.top = new FormAttachment(cmbFormater, 6);
		fd_txtFileName.left = new FormAttachment(lblFileName, 123);
		fd_txtFileName.right = new FormAttachment(100, -64);
		txtFileName.setLayoutData(fd_txtFileName);
		
		CLabel lblStrategyName = new CLabel(shlGenerateTestcase, SWT.NONE);
		lblStrategyName.setText("Strategy");
		FormData fd_lblStrategyName = new FormData();
		fd_lblStrategyName.top = new FormAttachment(lblFormatter, 32);
		fd_lblStrategyName.left = new FormAttachment(lblNumCase, 0, SWT.LEFT);
		lblStrategyName.setLayoutData(fd_lblStrategyName);
		
		txtStrategyName = new Text(shlGenerateTestcase, SWT.BORDER | SWT.RIGHT);
		fd_cmbFormater.right = new FormAttachment(txtStrategyName, 0, SWT.RIGHT);
		txtStrategyName.setToolTipText("name of the strategy");
		txtStrategyName.setText("");
		FormData fd_txtStrategyName = new FormData();
		fd_txtStrategyName.top = new FormAttachment(cmbFormater, 32);
		fd_txtStrategyName.left = new FormAttachment(lblStrategyName, 123);
		fd_txtStrategyName.right = new FormAttachment(100, -64);
		txtStrategyName.setLayoutData(fd_txtStrategyName);

		final Spinner spinner = new Spinner(shlGenerateTestcase, SWT.BORDER);
		spinner.setMaximum(500);
		spinner.setMinimum(1);
		FormData fd_spinner = new FormData();
		fd_spinner.bottom = new FormAttachment(lblNumCase, 0, SWT.BOTTOM);
		fd_spinner.right = new FormAttachment(lblNumCase, 199, SWT.RIGHT);
		fd_spinner.left = new FormAttachment(cmbFormater, 0, SWT.LEFT);
		spinner.setLayoutData(fd_spinner);

		btnAccept.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (txtFileName.getText().isEmpty()) {
					MessageBox messageDialog = new MessageBox(
							shlGenerateTestcase, SWT.ERROR);
					messageDialog.setText("Error");
					messageDialog.setMessage("Filename is empty");
					messageDialog.open();
				} else {
					try{
						final IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
						final IEditorPart editor = getActiveEditor();
						final String input = editor.getEditorInput().toString().split("\\(")[1].split("\\)")[0];
						final String[] pathInput = input.split("/");
						final IProject myProject = myWorkspaceRoot.getProject(pathInput[1]);
						if (myProject.exists() && !myProject.isOpen())
							myProject.open(null);
						
						String pathWork=myProject.getLocation().toString();
						pathWork= " file:///" + pathWork + '/' +pathInput[pathInput.length-1];
						//final String fileSpec = pathWork+input;
						//File(System.getProperty("user.dir")+"/es.uca.webservices.testgen.parsers.spec.xtext.menu/src-gen/");
						final int numRandTest = Integer.parseInt(spinner.getText());
						final int indexFormater = cmbFormater.getSelectionIndex();
						String formatter;
						if (indexFormater == 1) {
							formatter = "csv";
						} else {
							formatter = "velocity";
						}
						final IFolder imagesFolder = myProject.getFolder("gen-src");
						if (!imagesFolder.exists()) {
							imagesFolder.create(true, true, null);
						}
						final String path = new String(myProject.getLocation().toString() + "/gen-src/" + txtFileName.getText().trim() + "." + formatter);
						TestGeneratorRun testRun = new TestGeneratorRun (pathWork,formatter,numRandTest,txtStrategyName.getText().trim());

						final File file = new File(path);
						file.createNewFile();
						OutputStream os = new FileOutputStream(file);
						testRun.run(os);
						final MessageBox messageDialog = new MessageBox(
							shlGenerateTestcase, SWT.OK);
						
						messageDialog.setText("Generate Test-Case");
						messageDialog.setMessage("Generate Test-Case");
						messageDialog.open();
						} catch (Exception e1) {
						final MessageBox messageDialog = new MessageBox(
								shlGenerateTestcase, SWT.ERROR);
						
						messageDialog.setText("Generate Test-Case");
						messageDialog.setMessage(e1.getMessage());
						messageDialog.open();
						e1.printStackTrace();
					}					
				}
			}

		});

	}
	
	private IEditorPart getActiveEditor() {  
	    return Activator.getDefault().getWorkbench().getActiveWorkbenchWindow()  
	                .getActivePage().getActiveEditor();  
	}
}
