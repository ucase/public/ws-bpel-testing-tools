package es.uca.webservices.gamera.eclipse.mutantcomparison.views;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.*;
import org.eclipse.compare.CompareUI;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.RegistryFactory;
import org.eclipse.jface.viewers.*;
import org.eclipse.jface.action.*;
import org.eclipse.swt.SWT;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionProcessor;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.util.Pair;

/**
 * This class creates the view to select the mutants which will be compared.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class MutantComparisonView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "es.uca.webservices.gamera.eclipse.mutantcomparison.views.MutantComparisonView";

	private TreeViewer treeViewer;
	private Action doubleClickAction;
	private String originalFile;
	private String executorClass;
	private Map<String, GAIndividual> labelToIndividual;
	private Map<String, CompareInput> generatedComparisons;

	/**
	 * The constructor.
	 */
	public MutantComparisonView() {
	}

	
	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(final Composite parent) {
		labelToIndividual = new HashMap<String, GAIndividual>();
		treeViewer = new TreeViewer(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		getSite().setSelectionProvider(treeViewer);
		treeViewer.setLabelProvider(new MutantLabelProvider());
		treeViewer.setContentProvider(new MutantContentProvider());
		
		generatedComparisons = new HashMap<String, CompareInput>();
		
		// Create the help context id for the viewer's control
		PlatformUI.getWorkbench().getHelpSystem().setHelp(treeViewer.getControl(), "es.uca.webservices.eclipse.mutantcomparison.viewer");
		makeActions();
		hookDoubleClickAction();	
		
		// Receive the OSGi event with the results of the mutation
		BundleContext ctx = FrameworkUtil.getBundle(MutantComparisonView.class).getBundleContext();
	    EventHandler handler = new EventHandler() {
	      public void handleEvent(final Event event) {
	    	  if(!parent.isDisposed()){
		    	  parent.getDisplay().syncExec(new Runnable() {
		    		  public void run() {
		    			  generatedComparisons.clear();
		    			  @SuppressWarnings("unchecked")
		    			  Pair<AnalysisResults, String[]> request=(Pair<AnalysisResults, String[]>) event.getProperty("ANALYSISRESULTS");
		    			  AnalysisResults results=request.getLeft();
		    			  originalFile=request.getRight()[0];
		    			  executorClass=request.getRight()[1];
	
		    	
		    			  final BigInteger[] biListLocation = results.getLocationCounts();
		    			  final BigInteger[] biListAttribute= results.getFieldRanges();
		    			  // Put the available operators in the ArrayList
		    			  final List<Pair<String, List<String>> > lOperators= new ArrayList<Pair<String, List<String>>>();
		    			  for(int i=0;i<biListAttribute.length;i++){
		    				  if(!biListLocation[i].toString().equals("0")){
		    					  List<String> individuals= new ArrayList<String>();
		    					  for(int location=1;location<=biListLocation[i].intValue();location++){
		    						  for(int attribute=1; attribute<=biListAttribute[i].intValue();attribute++){
		    							  String text=results.getOperatorNames()[i]+" "+location+" "+attribute;
		    							  individuals.add(text);
		    							  labelToIndividual.put(text, new GAIndividual(i+1, location, attribute, true));
		    						  }
		    					  }
		    					  Pair<String, List<String>> individualsByOperator=new Pair<String, List<String>>(
		    							  results.getOperatorNames()[i],individuals);
		    					  lOperators.add(individualsByOperator);
		    				  }
		    			  }
		    			  treeViewer.setInput(lOperators);
		    			  treeViewer.collapseAll();
	
		    		  }
		    	  });
	    	  }
	        }
	    };
	    	    
	    Dictionary<String,String> properties = new Hashtable<String, String>();
	    properties.put(EventConstants.EVENT_TOPIC, "analysiscommunication/*");
	    ctx.registerService(EventHandler.class, handler, properties);
	}

	private void makeActions() {		
		doubleClickAction = new Action() {
			public void run() {
				ISelection selection = treeViewer.getSelection();
				Object obj = ((IStructuredSelection)selection).getFirstElement();
				if(obj instanceof String){
					
					IExtensionRegistry registry = RegistryFactory.getRegistry();

    				IConfigurationElement[] config =
    				        registry.getConfigurationElementsFor("es.uca.webservices.gamera.eclipse.executor");
    				for(IConfigurationElement c: config){

    					try {
    						if(c.createExecutableExtension("class").getClass().getName().equals(executorClass)){

    							Object object = c.createExecutableExtension("class");
    							Class<?> selectedClass = object.getClass();
    							GAExecutor executor = (GAExecutor) object;
    							final Map<Method, Option> options = OptionProcessor.listOptions(selectedClass);
    							
    							//Find the configure methods and invoke them with the selected files
    							for (final Method m : selectedClass.getMethods()) { 								
    								if (options.containsKey(m)) {
    									for(String s:options.get(m).fileExtensions()){
    										if(s.equals(FilenameUtils.getExtension(originalFile))){
    											m.invoke(executor, new Object[] { originalFile });
    										}
    									}
    									
    								}
    							}
    							
    							String selectionItem=((IStructuredSelection)selection).getFirstElement().toString();
    							CompareInput comp;
    							if(generatedComparisons.containsKey(selectionItem)){
    								comp=generatedComparisons.get(selectionItem);
    							}
    							else{
	    							final GAIndividual individual = labelToIndividual.get(selectionItem);
									File f= executor.getMutantFile(individual);
									
									comp=new CompareInput(originalFile, f.getAbsolutePath());
									generatedComparisons.put(selectionItem, comp);
    							}
								CompareUI.openCompareEditor(comp);

    						}
    					} catch (CoreException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (UnsupportedOperationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (GenerationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 

    				}
				}
			}
		};
	}

	private void hookDoubleClickAction() {
		treeViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}
	

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		treeViewer.getControl().setFocus();
	}
}