package es.uca.webservices.gamera.eclipse.mutantcomparison.views;

import java.util.List;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;

import es.uca.webservices.gamera.api.util.Pair;

public class MutantLabelProvider implements ILabelProvider{

	@Override
	public void addListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Image getImage(Object element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getText(Object element) {
		if (element instanceof String) {
			String str = (String) element;
			return str;
		}
		if (element instanceof Pair) {
			@SuppressWarnings("unchecked")
			Pair<String, List<String>> pair = (Pair<String, List<String>>) element;
			return pair.getLeft();
		}
		return "?";
	}

}
