package es.uca.webservices.gamera.eclipse.mutantcomparison.views;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.CompareEditorInput;
import org.eclipse.compare.structuremergeviewer.DiffNode;
import org.eclipse.compare.structuremergeviewer.Differencer;
import org.eclipse.core.runtime.IProgressMonitor;


/**
 * This class creates the comparison editor which shows the differences
 * between a mutant and the original code.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class CompareInput extends CompareEditorInput {
	private String fOld, fNew;
	
    public CompareInput(String fOld, String fNew) {
       super(new CompareConfiguration());
       this.fOld=fOld;
       this.fNew=fNew;

    }
    protected Object prepareInput(IProgressMonitor pm) {
       CompareItem ancestor = 
          new CompareItem("Common", "contents", 0);
	try {
		FileInputStream isOld = new FileInputStream(fOld);
		FileInputStream isNew = new FileInputStream(fNew);
		CompareItem left = 
		          new CompareItem("Left", IOUtils.toString(isOld), 1);
		       CompareItem right = 
		          new CompareItem("Right", IOUtils.toString(isNew),2);
		       return new DiffNode(null, Differencer.CHANGE, 
		          ancestor, left, right);
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return null;
       
       
    }
    
    
 }