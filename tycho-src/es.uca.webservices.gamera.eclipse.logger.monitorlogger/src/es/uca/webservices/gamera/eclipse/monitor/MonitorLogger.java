package es.uca.webservices.gamera.eclipse.monitor;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;


import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.log.GALogger;

/**
 * This logger monitors the progress of the evolutionary algorithm.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
@Option(hidden=true)
public class MonitorLogger implements GALogger{
	
	private IProgressMonitor monitor=null;

	//Necesary to SnakeYAML
	public MonitorLogger() {
		// TODO Auto-generated constructor stub
	}
	
	public MonitorLogger(IProgressMonitor monitor) {
		this.monitor=monitor;
	}

	@Override
	public void finished(GAState arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finishedAnalysis(GAState arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finishedComparison(GAState arg0, GAPopulation arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finishedComparison(GAState arg0, GAPopulation arg1,
			GAIndividual arg2, ComparisonResults arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finishedEvaluation(GAState arg0, GAPopulation arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void newGeneration(GAState arg0, GAPopulation arg1) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * This method starts the monitor.
	 */
	@Override
	public void started(GAState arg0) {
		monitor.beginTask("Performing read. Please wait...", IProgressMonitor.UNKNOWN);
		monitor.worked(IProgressMonitor.UNKNOWN);
		monitor.subTask("");
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startedAnalysis(GAState arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startedComparison(GAState arg0, GAPopulation arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startedComparison(GAState arg0, GAPopulation arg1,
			GAIndividual arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startedEvaluation(GAState arg0, GAPopulation arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void appliedGeneticOperator(GAState arg0, String arg1,
			List<GAIndividual> arg2, List<GAIndividual> arg3) {
		// TODO Auto-generated method stub
		
	}

}
