package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.widgets.TreeItem;

/**
 * This class constructs a Configuration object using the tree.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class ConfiguratorConstructor {
	
	private TreeItem treeItem;
	private static ArrayList<Object> bundleObjects=null;
	
	public ConfiguratorConstructor(TreeItem treeItem){
		this.treeItem=treeItem;
		if(bundleObjects==null){
			bundleObjects=new ExtensionPointsExplorer("es.uca.webservices.gamera.eclipse").explore();
		}
	}
	
	/**
	 * This method generates the Configuration object.
	 */
	public Object generate() throws InstantiationException, IllegalAccessException, IllegalArgumentException,
	InvocationTargetException, SecurityException{
		Object node=null;

		//If the node represents a object, we create a instance of it and configure his methods.
		if(((Set<NodeType>)treeItem.getData("types")).contains(NodeType.CLASS)){
			Class<?> classNode=null;
			try{
				classNode= Class.forName(treeItem.getText());
			}catch (ClassNotFoundException e) {
				for(Object o:bundleObjects){
					if(o.getClass().getName().equals(treeItem.getText())){
						classNode=o.getClass();
					}
				}
			}
			node=classNode.newInstance();
			for(TreeItem methodItem:treeItem.getItems()){
				Method method=(Method)methodItem.getData("method");
				method=new ExploreClass(classNode).getCouple(method);
				if(methodItem.getItems().length>0){
					//If the param of a method is a list, we iterate by each child node
					if(((Set<NodeType>)methodItem.getData("types")).contains(NodeType.LIST)){
						List<Object> list=new ArrayList<Object>();
						for(TreeItem item:methodItem.getItems()){
							ConfiguratorConstructor constructor= new ConfiguratorConstructor(item);
							list.add(constructor.generate());

						}
						method.invoke(node, list);
					}
					//If the param of a method is a map, we iterate by each pair
					else if(((Set<NodeType>)methodItem.getData("types")).contains(NodeType.MAP)){
						Map<Object, Object> map = new HashMap<Object, Object>();
						for(TreeItem item:methodItem.getItems()){
							TreeItem itemKey=item.getItem(0);
							TreeItem itemValue=item.getItem(1);
							ConfiguratorConstructor constructorKey= new ConfiguratorConstructor(itemKey);
							ConfiguratorConstructor constructorValue= new ConfiguratorConstructor(itemValue);

							map.put(constructorKey.generate(), constructorValue.generate());
							method.invoke(node, map);

						}
					}
					//If the param of a method is a object, we create a instance an set it in the method. We call
					//recursively to configure this object.
					else{
						
						TreeItem subClassItem=methodItem.getItems()[0];
						Object subClass= new ConfiguratorConstructor(subClassItem).generate();
						Class<?> type=method.getParameterTypes()[0];
						try {
							subClass=type.getConstructor(String.class).newInstance(subClass);
							method.invoke(node, subClass);
						} catch (NoSuchMethodException e) {
							if(type.equals(Integer.TYPE) || type.isAssignableFrom(Integer.class)){
								subClass=Integer.valueOf((String) subClass);
								method.invoke(node, subClass);
							}
							else if(type.equals(Double.TYPE) || type.isAssignableFrom(Double.class)){
								subClass=Double.valueOf((String) subClass);
								method.invoke(node, subClass);
							}
							else if(type.equals(Float.TYPE) || type.isAssignableFrom(Double.class)){
								subClass=Float.valueOf((String) subClass);
								method.invoke(node, subClass);
							}
							else if(type.equals(Boolean.TYPE) || type.isAssignableFrom(Boolean.class)){
								subClass=Boolean.valueOf((Boolean) subClass);
								method.invoke(node, subClass);
							}
							else{
								method.invoke(node, type.cast(subClass));
							}
						}	
					}
				}
			}


		}
		//If the node represents a value, we get the value from the node.
		if(((Set<NodeType>)treeItem.getData("types")).contains(NodeType.VALUE) || 
				((Set<NodeType>)treeItem.getData("types")).contains(NodeType.EDITABLE) ||
				((Set<NodeType>)treeItem.getData("types")).isEmpty()){
			node= treeItem.getText();
		}
		return node;
	}

}
