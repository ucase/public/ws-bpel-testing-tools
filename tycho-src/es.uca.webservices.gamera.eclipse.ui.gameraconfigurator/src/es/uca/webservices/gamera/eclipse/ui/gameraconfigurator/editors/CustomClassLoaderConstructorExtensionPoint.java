package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors;

import org.yaml.snakeyaml.constructor.CustomClassLoaderConstructor;

/**
 * SnakeYAML need a classloader to determine the class available to be constructed.
 * In this project, we have class distributed by different plug-ins relationated which
 * extension points. This classloader explores all the classpath of the different plug-ins.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class CustomClassLoaderConstructorExtensionPoint extends CustomClassLoaderConstructor{

	public CustomClassLoaderConstructorExtensionPoint(ClassLoader cLoader) {
		super(cLoader);
		
	}
	
	@Override
	protected Class<?> getClassForName(String name) throws ClassNotFoundException {
			try{
				return super.getClassForName(name);
			}catch(ClassNotFoundException e){
				Class<?> foundClass=null;
				ExtensionPointsExplorer extensionPointExplorer=new ExtensionPointsExplorer("es.uca.webservices.gamera.eclipse"); //$NON-NLS-1$
				for(Object o:extensionPointExplorer.explore()){
					if(o.getClass().getName().equals(name)){
						foundClass=o.getClass();
					}
				}
				if(foundClass!=null){
					return foundClass;
				}
				else{
					throw new ClassNotFoundException();
				}
			}
	}

}
