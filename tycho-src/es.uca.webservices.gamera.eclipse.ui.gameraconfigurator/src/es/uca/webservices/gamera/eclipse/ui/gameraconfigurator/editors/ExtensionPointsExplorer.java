package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors;

import java.util.ArrayList;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.RegistryFactory;

/**
 * This class explore all the class accessible from a determined plug-in.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class ExtensionPointsExplorer {
	private String id;
	
	public ExtensionPointsExplorer(String id){
		this.id=id;
	}
	
	public ArrayList<Object> explore(){
		IExtensionRegistry registry = RegistryFactory.getRegistry();
		ArrayList<Object> configurationClass=new ArrayList<Object>();
		for(IExtensionPoint extensionPoint:registry.getExtensionPoints(id)){
			IConfigurationElement[] config =extensionPoint.getConfigurationElements();
			for(IConfigurationElement c: config){
				try {
					final Object o =c.createExecutableExtension("class"); //$NON-NLS-1$

					configurationClass.add(o);
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return configurationClass;   
	}
}


