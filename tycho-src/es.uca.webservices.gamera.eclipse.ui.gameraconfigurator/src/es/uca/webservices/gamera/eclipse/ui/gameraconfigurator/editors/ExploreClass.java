package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class ExploreClass {
	Class<?> node;
	
	public ExploreClass(Class<?> node){
		this.node=node;
	}

	/**
	 * This method returns the configuration methods of a class.
	 */
	public List<Method> getMethods(){
		Method[] allMethods=node.getMethods();
		List<Method> configurationMethods=new ArrayList<Method>();
		for(int i=0;i<allMethods.length;i++){
			if(allMethods[i].getName().startsWith("set")){
				String value=allMethods[i].getName().substring(3, allMethods[i].getName().length());
				if(!value.equals("")){
					for(int j=0;j<allMethods.length;j++){
						if(allMethods[j].getName().equals("get"+value)){
							configurationMethods.add(allMethods[i]);
						}
					}
				}
			}
		}
		return configurationMethods;
	}
	
	/**
	 * This method returns the couple of a determined method.
	 */
	public Method getCouple(Method method){
		for(Method m: node.getMethods()){
			String split1=m.getName().substring(3, m.getName().length());
			String split2=method.getName().substring(3, method.getName().length());
			if(!m.getName().equals(method.getName()) && split1.equals(split2) && !split1.equals("")){
				return m;
			}
		}
		return null;
	}

}
