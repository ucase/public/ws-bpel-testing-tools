package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.listener;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.listener.messages"; //$NON-NLS-1$
	public static String AddMenuListener_10;
	public static String AddMenuListener_12;
	public static String AddMenuListener_16;
	public static String AddMenuListener_17;
	public static String AddMenuListener_4;
	public static String AddMenuListener_1;
	public static String AddMenuListener_2;
	public static String AddMenuListener_9;
	public static String EditMenuListener_0;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
