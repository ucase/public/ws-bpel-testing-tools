package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionProcessor;

/**
 * This class generates the node which represent a object and all the subnodes with his configuration methods and 
 * sub-objects.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class GenerateNodes {
	private TreeItem parent;
	private Object node=null;
	private Tree tree=null;

	/**
	 * This method generates the nodes which represent an object and that are child of a parent node.
	 */
	public GenerateNodes(TreeItem parent, Object node){
		this.parent=parent;
		this.node=node;
	}
	/**
	 * This method generates the node which represent an object and that is the first node in the tree.
	 */
	public GenerateNodes(Tree tree, Object node){
		this.tree=tree;
		this.node=node;
	}
	
	/**
	 * This method removes the "set" or "get" preposition of a configuration method.
	 * @param m
	 * @return
	 */
	public String processMethodName(Method m){
		String name=m.getName();
		name=name.substring(3);
		String[] r = name.split("(?=\\p{Lu})"); //$NON-NLS-1$
		String processedName=""; //$NON-NLS-1$
		for(String s:r){
			if(!s.equals("")){ //$NON-NLS-1$
				processedName+=s+" "; //$NON-NLS-1$
			}
		}
		
		return processedName.substring(0, processedName.length()-1);
	}
	
	/**
	 * This method generates the nodes.
	 */
	public void generate(){
        if(node!=null){
        	// If the parent node is a list, we generate the nodes of each child.
        	if(node instanceof List){
        		((Set<NodeType>)parent.getData("types")).add(NodeType.LIST); //$NON-NLS-1$
        		List<?> list= (List<?>) node;
        		for(Object o:list){
        			new GenerateNodes(parent, o).generate();
        		}
        	}
        	// If the parent node is a map, we generate two node per each pair.
        	else if(node instanceof Map){
        		((Set<NodeType>)parent.getData("types")).add(NodeType.MAP); //$NON-NLS-1$
        		Map<?, ?> map= (Map<?, ?>) node;
        		for(Entry<?, ?> entry:map.entrySet()){
        			TreeItem pairItem;
        			if(tree==null){
	        			pairItem = new TreeItem(parent, SWT.FILL);
	                	
        			}else{
        				pairItem = new TreeItem(tree, SWT.FILL);
        			}
        			Set<NodeType> types=new HashSet<NodeType>();
    	            types.add(NodeType.PAIR);
        			pairItem.setText(Messages.GenerateNodes_6);
        			pairItem.setData("types", types); //$NON-NLS-1$
        			new GenerateNodes(pairItem, entry.getKey()).generate();
        			new GenerateNodes(pairItem, entry.getValue()).generate();
        		}
        	}
        	// If the parent node needs a number or a string, we generate this node.
        	else if(node instanceof Number | node instanceof String){
        		TreeItem valueItem;
        		if(tree==null){
        			valueItem= new TreeItem(parent, SWT.FILL);
        		}else{
        			valueItem= new TreeItem(tree, SWT.FILL);
        		}
        		
            	valueItem.setText(node.toString());
            	Set<NodeType> types=new HashSet<NodeType>();
            	if(((Set<NodeType>)parent.getData("types")).contains(NodeType.OPERATORS)){ //$NON-NLS-1$
            		types.add(NodeType.EDITABLE);
            	}
            	else if(!((Set<NodeType>)parent.getData("types")).contains(NodeType.FILE)){
            		types.add(NodeType.VALUE);
            	}
            	valueItem.setData("types",types); //$NON-NLS-1$
        	}
        	// If the parent node need a object, we generate the node that represent this objects, with his configuration
        	// methods. We call recursively with each configuration method.
        	else{
        		TreeItem classItem;
        		Set<NodeType> classTypes=new HashSet<NodeType>();
	            classTypes.add(NodeType.CLASS);
        		if(tree==null){
        			classItem= new TreeItem(parent, SWT.FILL);
                	classItem.setData("types",classTypes); //$NON-NLS-1$
        		}else{
        			classItem= new TreeItem(tree, SWT.FILL);
        			classTypes.add(NodeType.UNERASABLE);
                	classItem.setData("types",classTypes); //$NON-NLS-1$

        		}
        		
            	classItem.setText(node.getClass().getName());
				try {
					ImageDescriptor image  = PathLocator.getFile("/icons/class.gif"); //$NON-NLS-1$ //$NON-NLS-2$
					classItem.setImage(image.createImage());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
	        	ExploreClass explorer=new ExploreClass(node.getClass());
	        	ImageDescriptor image=null;
				try {
					image  = PathLocator.getFile("/icons/methods_co.gif"); //$NON-NLS-1$ //$NON-NLS-2$
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				final Map<Method, Option> options = OptionProcessor.listOptions(node.getClass());
				List<Method> methods=explorer.getMethods();
				Collections.sort(methods, new Comparator<Method>(){
				     public int compare(Method m1, Method m2){
				    	 return m1.getName().compareTo(m2.getName());
				     }
				});
				for(Method m: methods){
					
					Option methodOptions = options.get(m);
					if(methodOptions==null || !methodOptions.hidden()){
						TreeItem attributeItem = new TreeItem(classItem, SWT.FILL);
			            attributeItem.setText(processMethodName(m));
			            attributeItem.setImage(image.createImage());
			            Set<NodeType> types=new HashSet<NodeType>();
			            types.add(NodeType.METHOD);
			            if(methodOptions!=null && methodOptions.operators()){
			            	types.add(NodeType.OPERATORS);
			            }
			            if(methodOptions!=null){
			            	String type=methodOptions.type().toString();
							if(type.equals("PATH_LOAD") || type.equals("PATH_SAVE")){
								types.add(NodeType.FILE);
								attributeItem.setData("extensions", 
										Arrays.asList(methodOptions.fileExtensions()));
							}
			            }
			            attributeItem.setData("types", types); //$NON-NLS-1$
			            attributeItem.setData("method", explorer.getCouple(m)); //$NON-NLS-1$
			            try {
							new GenerateNodes(attributeItem, explorer.getCouple(m).invoke(node)).generate();
	
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
        	}
        }
	}

}
