package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.listener;


import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.dialogs.ListSelectionDialog;

import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors.GameraMultiPageEditor;
import es.uca.webservices.mutants.operators.OperatorConstants;


/**
 * This method edits a node.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class EditMenuListener  extends SelectionAdapter{
	
	private TreeItem treeItem;
	private GameraMultiPageEditor editor;
	
	public EditMenuListener(TreeItem treeItem, GameraMultiPageEditor editor){
		this.treeItem=treeItem;
		this.editor=editor;
	}
	
	@Override
	public void widgetSelected(SelectionEvent e) {
		ListSelectionDialog operatorsDialog=new ListSelectionDialog(
				new Shell(),
				OperatorConstants.OPERATOR_NAMES,
				new ArrayContentProvider(),
				new LabelProvider(),
				Messages.EditMenuListener_0); 
		
		operatorsDialog.setInitialSelections(treeItem.getText().split(", ")); //$NON-NLS-1$

		operatorsDialog.open();
		if(operatorsDialog.getReturnCode()==Window.OK){
			String selectedOperators=""; //$NON-NLS-1$
			for(Object operator: operatorsDialog.getResult()){
				selectedOperators+=(String)operator+", "; //$NON-NLS-1$
			}
	
			if(!selectedOperators.isEmpty()){
				String operators=selectedOperators.substring(0, selectedOperators.length()-2);
		        treeItem.setText(operators);

			}
			else{
				treeItem.dispose();
			}
			editor.updateTextEditor();
		}
	}
}
