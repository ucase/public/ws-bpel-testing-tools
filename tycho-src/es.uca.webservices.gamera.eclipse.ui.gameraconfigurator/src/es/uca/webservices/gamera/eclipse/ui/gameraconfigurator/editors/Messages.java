package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors.messages"; //$NON-NLS-1$
	public static String DescriptionListDialog_0;
	public static String GameraMultiPageEditor_0;
	public static String GameraMultiPageEditor_1;
	public static String GameraMultiPageEditor_10;
	public static String GameraMultiPageEditor_11;
	public static String GameraMultiPageEditor_12;
	public static String GameraMultiPageEditor_13;
	public static String GameraMultiPageEditor_14;
	public static String GameraMultiPageEditor_2;
	public static String GameraMultiPageEditor_4;
	public static String GameraMultiPageEditor_5;
	public static String GameraMultiPageEditor_7;
	public static String GenerateNodes_6;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
