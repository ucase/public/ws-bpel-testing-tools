package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors;

/**
 * This enumerate defines the different types of nodes in our configuration tree. 
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public enum NodeType {
	CLASS, VALUE, METHOD, LIST, MAP, UNERASABLE, PAIR, OPERATORS, EDITABLE, FILE
}
