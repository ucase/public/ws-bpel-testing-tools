package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.listener;

import java.beans.Introspector;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.eclipse.dialog.ExtensionValidator;
import es.uca.webservices.gamera.eclipse.dialog.TypeViewerFilter;
import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors.DescriptionListDialog;
import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors.ExtensionPointsExplorer;
import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors.GameraConstructor;
import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors.GameraMultiPageEditor;
import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors.GenerateNodes;
import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors.NodeType;
import es.uca.webservices.mutants.operators.OperatorConstants;

/**
 * This class adds a new node to the tree.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class AddMenuListener extends SelectionAdapter{
	
	private TreeItem treeItem;
	private GameraMultiPageEditor editor;
	
	public AddMenuListener(TreeItem treeItem, GameraMultiPageEditor editor){
		this.treeItem=treeItem;
		this.editor=editor;
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		//When the parent node is a list...
		if(((Set<NodeType>)treeItem.getData("types")).contains(NodeType.LIST)){ //$NON-NLS-1$
			GameraConstructor c=new GameraConstructor();
			String methodName=((Method)treeItem.getData("method")).getName(); //$NON-NLS-1$
			String property= methodName.substring(3);
			try {
				Class<?> superClass=c.getConstructor().getListPropertyType(Introspector.decapitalize(property));
				generateDialog(superClass, treeItem);
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		//When the parent node is a map...
		else if(((Set<NodeType>)treeItem.getData("types")).contains(NodeType.MAP)){ //$NON-NLS-1$
			GameraConstructor c=new GameraConstructor();
			String methodName=((Method)treeItem.getData("method")).getName(); //$NON-NLS-1$
			String property= methodName.substring(3);
			TreeItem pairItem = new TreeItem(treeItem, SWT.FILL);
        	pairItem.setText(Messages.AddMenuListener_4);
        	Set<NodeType> types=new HashSet<NodeType>();
            types.add(NodeType.PAIR);
            pairItem.setData("types", types); //$NON-NLS-1$
			try {
				Class<?> keyClass=c.getConstructor().getMapKeyType(Introspector.decapitalize(property));
				generateDialog(keyClass, pairItem);
				Class<?> valueClass=c.getConstructor().getMapValueType(Introspector.decapitalize(property));
				generateDialog(valueClass, pairItem);
				if(pairItem.getItems().length<2){
					pairItem.dispose();
				}
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		//The parent node is a atomic element.
		else{
		
			Class<?> superClass=((Method)treeItem.getData("method")).getReturnType(); //$NON-NLS-1$
			generateDialog(superClass, treeItem);
		}
		editor.updateTextEditor();
		
	}
	
	/**
	 * This method generates a different type of dialog depending of the type of node that will be generated.
	 */
	private void generateDialog(Class<?> superClass, TreeItem treeItem){
		//Check if there are class which should be added as childs of the selected node.
		ExtensionPointsExplorer extensionPointExplorer=new ExtensionPointsExplorer("es.uca.webservices.gamera.eclipse"); //$NON-NLS-1$
		Map<String, String> availableObjects=new HashMap<String, String>();
		for(Object o:extensionPointExplorer.explore()){
			if(o.getClass().getAnnotation(Option.class)==null ||
					!o.getClass().getAnnotation(Option.class).hidden()){
				if(superClass.isAssignableFrom(o.getClass())){
					String description=""; //$NON-NLS-1$
					if(o.getClass().getAnnotation(Option.class)!=null){
						description=o.getClass().getAnnotation(Option.class).description();
					}
					
					availableObjects.put(o.getClass().getSimpleName(), description);
				}
			}
		}
		//If there are class to add, we show they in a dialog.
		if(!availableObjects.isEmpty()){
			DescriptionListDialog listDialog=new DescriptionListDialog(new Shell());
			listDialog.setContentProvider(new ArrayContentProvider());
			listDialog.setLabelProvider(new LabelProvider());
			listDialog.setTitle(Messages.AddMenuListener_9);
			listDialog.setMessage(Messages.AddMenuListener_10); 
			
			listDialog.setInputWithMessage(availableObjects);
			listDialog.open();
			for(Object o:extensionPointExplorer.explore()){
	
				if(listDialog.getReturnCode()==Window.OK 
						&& listDialog.getResult().length>0 
						&& o.getClass().getSimpleName().equals(listDialog.getResult()[0])){
					new GenerateNodes(treeItem, o).generate();
				}
			}
		}
		//If the selected node need mutation operators, we show a dialog with the available operators.
		else if(((Set<NodeType>)treeItem.getData("types")).contains(NodeType.OPERATORS)){ //$NON-NLS-1$
			ListSelectionDialog operatorsDialog=new ListSelectionDialog(
					new Shell(),
					OperatorConstants.OPERATOR_NAMES,
					new ArrayContentProvider(),
					new LabelProvider(),
					Messages.AddMenuListener_12); 

			operatorsDialog.open();
			String selectedOperators=""; //$NON-NLS-1$
			if(operatorsDialog.getResult()!=null){
				for(Object operator: operatorsDialog.getResult()){
					selectedOperators+=(String)operator+", "; //$NON-NLS-1$
				}
			}

			if(!selectedOperators.isEmpty()){
				String operators=selectedOperators.substring(0, selectedOperators.length()-2);
				TreeItem valueItem = new TreeItem(treeItem, SWT.FILL);
		        valueItem.setText(operators);
		        Set<NodeType> types=new HashSet<NodeType>();
		        types.add(NodeType.EDITABLE);
	            valueItem.setData("types",types); //$NON-NLS-1$
	            //editor.setLastEditedItem(valueItem);
			}
		}
		
		//If the selected node need a file, we show a dialog with the available files.
		else if(((Set<NodeType>)treeItem.getData("types")).contains(NodeType.FILE)){ //$NON-NLS-1$
			
			
			ElementTreeSelectionDialog select=new ElementTreeSelectionDialog(new Shell(), new WorkbenchLabelProvider(),
				    new BaseWorkbenchContentProvider());
			select.setTitle(Messages.AddMenuListener_1);
			select.setMessage(Messages.AddMenuListener_2);
			select.setInput(ResourcesPlugin.getWorkspace().getRoot());

			select.addFilter(new TypeViewerFilter((List<String>)treeItem.getData("extensions")));
			select.setAllowMultiple(false);
			select.setValidator(new ExtensionValidator());
			select.open();
			if(select.getResult()!=null){
				//String path=((IFile)select.getResult()[0]).getFullPath().toString();
				IFile file=(IFile)select.getResult()[0];
				String path=file.getRawLocation().makeAbsolute().toFile().getAbsolutePath();
				TreeItem valueItem = new TreeItem(treeItem, SWT.FILL);
		        valueItem.setText(path);
		        Set<NodeType> types=new HashSet<NodeType>();
	            valueItem.setData("types",types); //$NON-NLS-1$
			}
			
			
		
		}
		
		//If there isn't class to add, we create a dialog to introduce a value by keyboard.
		else{
			
			InputDialog inputDialog = new InputDialog(new Shell(),
		            Messages.AddMenuListener_16, Messages.AddMenuListener_17, null, new LengthValidator());
			inputDialog.open();
			if(inputDialog.getReturnCode()==Window.OK){
				TreeItem valueItem = new TreeItem(treeItem, SWT.FILL);
	        	valueItem.setText(inputDialog.getValue());
	        	Set<NodeType> types=new HashSet<NodeType>();
	            types.add(NodeType.VALUE);
            	valueItem.setData("types",types); //$NON-NLS-1$
            	editor.setLastEditedItem(valueItem);
			}
		}
	
		
	}
	
	private class LengthValidator implements IInputValidator {
		  /**
		   * Validates the String. Returns null for no error, or an error message
		   * 
		   * @param newText the String to validate
		   * @return String
		   */
		  public String isValid(String newText) {
		    int len = newText.length();

		    // Determine if input is too short or too long
		    if (len < 1) return ""; //$NON-NLS-1$

		    // Input must be OK
		    return null;
		  }
		}

}
