package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors;

import org.yaml.snakeyaml.TypeDescription;

import es.uca.webservices.gamera.api.generate.GAIndividualGenerator;
import es.uca.webservices.gamera.api.generate.GAIndividualGeneratorOptions;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperator;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.api.select.GASelectionOperator;
import es.uca.webservices.gamera.api.select.GASelectionOperatorOptions;
import es.uca.webservices.gamera.api.term.GATerminationCondition;
import es.uca.webservices.gamera.conf.Configuration;

/**
 * This class provides information about the types of the methods of the evolutionary algorithm. SnakeYAML
 * needs know which types need every method to be constructed.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class GameraConstructor{

    public TypeDescription getConstructor() throws ClassNotFoundException {

        TypeDescription typeDesc = new TypeDescription(Configuration.class);
        typeDesc.putMapPropertyType("geneticOperators", GAGeneticOperator.class, 
        		GAGeneticOperatorOptions.class);
        typeDesc.putMapPropertyType("individualGenerators", 
        		GAIndividualGenerator.class, GAIndividualGeneratorOptions.class);

        typeDesc.putMapPropertyType("selectionOperators",
            GASelectionOperator.class, GASelectionOperatorOptions.class);
        typeDesc.putListPropertyType("terminationConditions", GATerminationCondition.class);
        typeDesc.putListPropertyType("loggers", GALogger.class);
        return typeDesc;
    }
}
