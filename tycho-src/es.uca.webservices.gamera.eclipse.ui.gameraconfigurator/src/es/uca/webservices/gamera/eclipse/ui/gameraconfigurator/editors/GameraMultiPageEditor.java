package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.text.IDocument;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.*;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.ide.IDE;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.CustomClassLoaderConstructor;

import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.listener.AddMenuListener;
import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.listener.EditMenuListener;
import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.listener.EraseMenuListener;

import es.uca.webservices.gamera.conf.Configuration;

/**
 * This class generates the tree/text editor.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class GameraMultiPageEditor extends MultiPageEditorPart implements IResourceChangeListener{

	/** The text editor used in page 0. */
	private TextEditor editor;
	
	private Configuration configuration=new Configuration();
	
	private Tree tree;
	
	private List<Text> textList;
	
	private boolean modified=false;
	
	private TreeItem lastEditedItem;


	/**
	 * Creates a multi-page editor example.
	 */
	public GameraMultiPageEditor() {
		super();
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
		
	}
	/**
	 * Creates page 0 of the multi-page editor,
	 * which contains a text editor.
	 */
	void createSourcePage() {
		try {
			editor = new TextEditor();
			int index = addPage(editor, getEditorInput());
			setPageText(index, Messages.GameraMultiPageEditor_0);
		} catch (PartInitException e) {
			ErrorDialog.openError(
				getSite().getShell(),
				Messages.GameraMultiPageEditor_1,
				null,
				e.getStatus());
		}
	}
	
	/**
	 * This method creates the page to show the tree.
	 */
	void createDesignPage() {
		Composite composite = new Composite(getContainer(), SWT.NONE);
		composite.setLayout(new GridLayout(1, true));
		
		tree = new Tree(composite, SWT.FILL);
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		updateTree();
		int index = addPage(composite);
        setPageText(index, Messages.GameraMultiPageEditor_2);
        textList=new ArrayList<Text>();
	}
	
	/**
	 * This method updates the tree when the code changes.
	 */
	void updateTree(){
		GenerateNodes generator=new GenerateNodes(tree, configuration);
		
		
		final Menu menu = new Menu(tree);
		tree.setMenu(menu);
		final GameraMultiPageEditor multiPage=this;
		menu.addMenuListener(new MenuAdapter()
		{
			public void menuShown(MenuEvent e)
			{
				MenuItem[] items = menu.getItems();
				for (int i = 0; i < items.length; i++)
				{
					items[i].dispose();
				}
				Set<NodeType> nodes=((Set<NodeType>)tree.getSelection()[0].getData("types")); //$NON-NLS-1$
				
				if(nodes!=null && nodes.contains(NodeType.METHOD)){
					if(nodes.contains(NodeType.MAP) || nodes.contains(NodeType.LIST) ||(!nodes.contains(NodeType.LIST)) &&
							tree.getSelection()[0].getItems().length==0){

						MenuItem addItem = new MenuItem(menu, SWT.NONE);
						addItem.setText(Messages.GameraMultiPageEditor_4);
						addItem.addSelectionListener(new AddMenuListener(tree.getSelection()[0], multiPage));
					}
				}
				
				if(nodes!=null && nodes.contains(NodeType.EDITABLE)){
					MenuItem addItem = new MenuItem(menu, SWT.NONE);
					addItem.setText(Messages.GameraMultiPageEditor_5);
					addItem.addSelectionListener(new EditMenuListener(tree.getSelection()[0], multiPage));
				}
				
				if(nodes!=null && tree.getSelection()[0].getParentItem()!=null
						&& !((Set<NodeType>)tree.getSelection()[0].getParentItem().getData("types")).contains(NodeType.PAIR) //$NON-NLS-1$
						&& (nodes.contains(NodeType.VALUE) ||
						(nodes.contains(NodeType.CLASS) && !nodes.contains(NodeType.UNERASABLE)) ||
						(nodes.contains(NodeType.PAIR)) ||
						(nodes.contains(NodeType.EDITABLE)) ||
						((Set<NodeType>)tree.getSelection()[0].getParentItem().getData("types")).contains(NodeType.FILE))){
					MenuItem eraseItem = new MenuItem(menu, SWT.NONE);
					eraseItem.setText(Messages.GameraMultiPageEditor_7);
					eraseItem.addSelectionListener(new EraseMenuListener(tree.getSelection()[0], multiPage));
				}

			}
		});

		generator.generate();
        
		final TreeEditor editor = new TreeEditor(tree);
        //The editor must have the same size as the cell and must
        //not be any smaller than 50 pixels.
        editor.horizontalAlignment = SWT.LEFT;
        editor.grabHorizontal = true;
        editor.minimumWidth = 50;
        
        
        tree.addSelectionListener(new SelectionAdapter() {
        	public void widgetSelected(SelectionEvent e) {
        		
        		if(modified){
        			updateTextEditor();
        			modified=false;
        		}
        		
        		// Clean up any previous editor control
        		Control oldEditor = editor.getEditor();
        		if (oldEditor != null) oldEditor.dispose();

        		// Identify the selected row
        		final TreeItem item = (TreeItem)e.item;
        		if (item == null) return;

        		if(item.getData("types")!=null && tree.getSelection().length>0  //$NON-NLS-1$
        				&& ((Set<NodeType>)tree.getSelection()[0].getData("types")).contains(NodeType.VALUE)) //$NON-NLS-1$
        			{
        			// The control that will be the editor must be a child of the Tree
        			Text newEditor = new Text(tree, SWT.NONE);
        			newEditor.setText(item.getText());
        			newEditor.addModifyListener(new ModifyListener() {
        				public void modifyText(ModifyEvent e) {
        					Text text = (Text)editor.getEditor();
        					editor.getItem().setText(text.getText());
        					modified=true;
        					lastEditedItem=item;
        					//updateTextEditor();
        				}
        			});

        			newEditor.selectAll();
        			newEditor.setFocus();
        			editor.setEditor(newEditor, item);
        			textList.add(newEditor);
        		}
        	}
        });
	}
	

	/**
	 * Creates the pages of the multi-page editor.
	 */
	protected void createPages() {
		createDesignPage();
		createSourcePage();
	}
	/**
	 * The <code>MultiPageEditorPart</code> implementation of this 
	 * <code>IWorkbenchPart</code> method disposes all nested editors.
	 * Subclasses may extend.
	 */
	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		super.dispose();
	}
	/**
	 * Saves the multi-page editor's document.
	 */
	public void doSave(IProgressMonitor monitor) {
		getEditor(1).doSave(monitor);
	}
	/**
	 * Saves the multi-page editor's document as another file.
	 * Also updates the text for page 0's tab, and updates this multi-page editor's input
	 * to correspond to the nested editor's.
	 */
	public void doSaveAs() {
		IEditorPart editor = getEditor(0);
		editor.doSaveAs();
		setPageText(0, editor.getTitle());
		setInput(editor.getEditorInput());
	}
	/* (non-Javadoc)
	 * Method declared on IEditorPart
	 */
	public void gotoMarker(IMarker marker) {
		setActivePage(0);
		IDE.gotoMarker(getEditor(0), marker);
	}
	/**
	 * The <code>MultiPageEditorExample</code> implementation of this method
	 * checks that the input is an instance of <code>IFileEditorInput</code>.
	 */
	public void init(IEditorSite site, IEditorInput editorInput)
		throws PartInitException {
		
		if (!(editorInput instanceof IFileEditorInput))
			throw new PartInitException(Messages.GameraMultiPageEditor_10);
		
		super.init(site, editorInput);
		
		IFile file = ((FileEditorInput) editorInput).getFile();
		this.setPartName(file.getName());
		Yaml y = new Yaml(new CustomClassLoaderConstructorExtensionPoint(Configuration.class.getClassLoader()));
		try {
			configuration=(Configuration) y.load(file.getContents());
		} catch (Exception e) {
			final Status status = 
					new Status(IStatus.ERROR,
							"SCS Admin", e.getLocalizedMessage(), e); //$NON-NLS-1$
			throw new PartInitException(status);
		}	
	}
	/* (non-Javadoc)
	 * Method declared on IEditorPart.
	 */
	public boolean isSaveAsAllowed() {
		return true;
	}
	/**
	 * Calculates the contents of page 2 when the it is activated.
	 */
	protected void pageChange(int newPageIndex) {
		super.pageChange(newPageIndex);
		if(newPageIndex==0 && tree.getTopItem()!=null){
			Yaml y = new Yaml(new CustomClassLoaderConstructorExtensionPoint(Configuration.class.getClassLoader()));

			IDocumentProvider dp = editor.getDocumentProvider();
			IDocument doc = dp.getDocument(editor.getEditorInput());
			try{
				configuration=(Configuration) y.load(doc.get());
				if(configuration!=null){
					tree.removeAll();
					updateTree();
				}
			}catch(Exception e){
				this.setActivePage(1);
				final Status status = 
						new Status(IStatus.ERROR,
								"SCS Admin", e.getLocalizedMessage(), e); //$NON-NLS-1$

				ErrorDialog errorDialog = new ErrorDialog(this.getContainer().getShell(), Messages.GameraMultiPageEditor_11,
						Messages.GameraMultiPageEditor_12, status, Status.ERROR);
				errorDialog.open();
			}
		}
		else{
			disposeTexts();
			if(modified){
    			updateTextEditor();
    			modified=false;
    		}		
		}
		
	}
	
	/**
	 * This method updates the text editor when the tree changes.
	 */
	public void updateTextEditor(){
		ConfiguratorConstructor constructor=new ConfiguratorConstructor(tree.getItems()[0]);
		Object configuration;
		try {
			configuration = constructor.generate();
			Yaml y = new Yaml(new CustomClassLoaderConstructor(Configuration.class.getClassLoader()));
			IDocumentProvider dp = editor.getDocumentProvider();
			IDocument doc = dp.getDocument(editor.getEditorInput());
			if(!doc.get().equals(y.dump(configuration))){
				doc.set(y.dump(configuration));
			}
		} catch (Exception e) {
			final Status status = 
					new Status(IStatus.ERROR,
							"SCS Admin", e.getLocalizedMessage(), e); //$NON-NLS-1$

			e.printStackTrace();
			ErrorDialog errorDialog = new ErrorDialog(this.getContainer().getShell(), Messages.GameraMultiPageEditor_13,
					Messages.GameraMultiPageEditor_14, status, Status.ERROR);
			errorDialog.open();
			if(lastEditedItem!=null){
				lastEditedItem.dispose();
			}
			updateTextEditor();
			
		} 
		
	}
	
	/**
	 * Closes all project files on project close.
	 */
	public void resourceChanged(final IResourceChangeEvent event){
		if(event.getType() == IResourceChangeEvent.PRE_CLOSE){
			Display.getDefault().asyncExec(new Runnable(){
				public void run(){
					IWorkbenchPage[] pages = getSite().getWorkbenchWindow().getPages();
					for (int i = 0; i<pages.length; i++){
						if(((FileEditorInput)editor.getEditorInput()).getFile().getProject().equals(event.getResource())){
							IEditorPart editorPart = pages[i].findEditor(editor.getEditorInput());
							pages[i].closeEditor(editorPart,true);
						}
					}
				}            
			});
		}
	}
	
	public void setLastEditedItem(TreeItem item){
		this.lastEditedItem=item;
	}
	
	public void disposeTexts(){
		for(Text t:textList){
			t.dispose();
		}
	}
}
