package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors;

import java.util.Map;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ListDialog;

/**
 * This class generates a dialog with the different class available and a descriptions about them.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class DescriptionListDialog extends ListDialog{
	
	private Label description;
	private Map<String,String> input;
	

	public DescriptionListDialog(Shell parent) {
		super(parent);
	}
	
	@Override
	protected Control createDialogArea(Composite container) {
		Composite dialog=(Composite) super.createDialogArea(container);
		dialog.setLayout(new GridLayout(1, true));
		

		Label lDescription=new Label(dialog, SWT.NONE);
		lDescription.setText(Messages.DescriptionListDialog_0);
		description=new Label(dialog, getTableStyle() | SWT.WRAP | SWT.V_SCROLL);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = convertHeightInCharsToPixels(8);
        description.setLayoutData(gd);
		this.getTableViewer().addSelectionChangedListener(new ISelectionChangedListener() {
			   public void selectionChanged(SelectionChangedEvent event) {
				   if(input!=null){
					   String selectedItem=(String) getTableViewer().getElementAt(getTableViewer()
							   .getTable().getSelectionIndex());
						setText(input.get(selectedItem));
				   }
			   }
			   });
		dialog.pack();
		return dialog;
		
	}
	
	private void setText(String text){
		description.setText(text);
		description.getParent().layout();
	}
	
	public void setInputWithMessage(Map<String,String> input) {
		this.input=input;
		super.setInput(input.keySet());
	}

}
