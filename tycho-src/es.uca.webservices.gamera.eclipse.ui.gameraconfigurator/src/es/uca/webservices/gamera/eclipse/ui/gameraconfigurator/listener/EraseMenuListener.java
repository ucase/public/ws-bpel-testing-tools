package es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.listener;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.TreeItem;

import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors.GameraMultiPageEditor;

/**
 * This listener erase a node from the tree.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class EraseMenuListener extends SelectionAdapter {
	
	private TreeItem treeItem;
	private GameraMultiPageEditor editor;
	
	public EraseMenuListener(TreeItem treeItem, GameraMultiPageEditor editor){
		this.treeItem=treeItem;
		this.editor=editor;
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		editor.disposeTexts();
		treeItem.dispose();
		editor.updateTextEditor();
	}
}
