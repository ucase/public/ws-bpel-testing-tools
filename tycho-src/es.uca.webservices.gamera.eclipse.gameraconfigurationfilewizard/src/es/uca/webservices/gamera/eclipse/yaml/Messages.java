package es.uca.webservices.gamera.eclipse.yaml;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "es.uca.webservices.gamera.eclipse.yaml.messages"; //$NON-NLS-1$
	public static String GAmeraConfigurationFileCreationPage_1;
	public static String GAmeraConfigurationFileCreationPage_2;
	public static String GAmeraConfigurationFileWizard_0;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
