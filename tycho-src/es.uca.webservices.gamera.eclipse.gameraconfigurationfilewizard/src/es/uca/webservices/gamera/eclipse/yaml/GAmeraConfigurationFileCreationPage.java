package es.uca.webservices.gamera.eclipse.yaml;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;
import org.yaml.snakeyaml.Yaml;



import es.uca.webservices.gamera.conf.Configuration;

/**
 * This class creates the interface to create a YAML file.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class GAmeraConfigurationFileCreationPage extends WizardNewFileCreationPage {
	

	public GAmeraConfigurationFileCreationPage(IStructuredSelection selection) {
		super("NewGAmeraConfigFileWizardPage", selection); //$NON-NLS-1$
        setTitle(Messages.GAmeraConfigurationFileCreationPage_1);
        setDescription(Messages.GAmeraConfigurationFileCreationPage_2);
        setFileExtension("yaml"); //$NON-NLS-1$
	}

	@Override
    protected InputStream getInitialContents() {
		Configuration emptyConfiguration=new Configuration();
		Yaml yaml=new Yaml();
	
		return new ByteArrayInputStream(yaml.dump(emptyConfiguration).getBytes());
    }

}
