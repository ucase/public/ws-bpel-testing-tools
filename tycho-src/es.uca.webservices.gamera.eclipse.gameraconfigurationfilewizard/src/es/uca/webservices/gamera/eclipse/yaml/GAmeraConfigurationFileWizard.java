package es.uca.webservices.gamera.eclipse.yaml;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

/**
 * This class creates the wizard to create a YAML file.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class GAmeraConfigurationFileWizard extends Wizard implements INewWizard{
	private IStructuredSelection selection;
    private GAmeraConfigurationFileCreationPage newFileWizardPage;
 

    public GAmeraConfigurationFileWizard() {

        setWindowTitle(Messages.GAmeraConfigurationFileWizard_0);

    } 

    @Override
    public void addPages() {

        newFileWizardPage = new GAmeraConfigurationFileCreationPage(selection);
        addPage(newFileWizardPage);
    }
   
    @Override
    public boolean performFinish() {
       
        IFile file = newFileWizardPage.createNewFile();
        if (file != null)
            return true;
        else
            return false;
    }

    public void init(IWorkbench workbench, IStructuredSelection selection) {
        this.selection = selection;
    }

}
