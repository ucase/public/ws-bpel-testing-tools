package es.uca.webservices.gamera.eclipse.bpel;


/**
 * This exception indicates that they was an error during the comparison process.
 * 
 * @author Juan Daniel Morcillo Regueiro
 */
public class ComparisonCancelledException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public ComparisonCancelledException(String message) {
		super(message);
	}

	public ComparisonCancelledException(Throwable cause) {
		super(cause);
	}

	public ComparisonCancelledException(String message, Throwable cause) {
		super(message, cause);
	}

}
