package es.uca.webservices.gamera.eclipse.bpel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.activebpel.rt.jetty.AeJettyRunner;
import org.xml.sax.SAXException;
import org.apache.commons.io.FileUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import net.bpelunit.framework.exception.ConfigurationException;
import net.bpelunit.framework.exception.DeploymentException;
import net.bpelunit.framework.exception.SpecificationException;
import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.InvalidProcessException;
import es.uca.webservices.bpel.activebpel.BPELPackagingException;
import es.uca.webservices.bpel.bpelunit.AbortOnFirstDifferenceListener.AbortOnDiff;
import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;
import es.uca.webservices.bpel.bpelunit.BPELUnitXMLResult;
import es.uca.webservices.bpel.bpelunit.ExecutionResults;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.gamera.exec.bpel.AbstractBPELExecutor;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.DummyProgressMonitor;
import es.uca.webservices.gamera.api.exec.GAProgressMonitor;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionType;
import es.uca.webservices.mutants.activebpel.ActiveBPELUtils;


/**
 * Executor for Web Service compositions written in WS-BPEL 2.0. and compatible with Eclipse.
 * @author Juan Daniel Morcillo Regueiro
 */
@Option(description="Executor for Web Service compositions written in WS-BPEL 2.0. and compatible with Eclipse.")
public class EclipseBPELExecutor extends AbstractBPELExecutor{
	
	private File outputFile = new File("original.out");
	private AeJettyRunner server;
	private File bprDirectory, fRootWorkDirectory, fOriginalResultFile;
	 
	@Option(type=OptionType.PATH_SAVE, fileExtensions={"xml"})
	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile != null ? new File(outputFile) : null;
	}

	public String getOutputFile() {
		return outputFile != null ? outputFile.getPath() : null;
	}
	
	@Override
	public void cleanup() throws PreparationException {

			try {
				server.stop();
				server.join();
				server=null;
				bprDirectory=null;
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			};
			
			if (fRootWorkDirectory != null && fRootWorkDirectory.exists()) {
	        	try {
	        		FileUtils.deleteDirectory(fRootWorkDirectory);
	        	} catch (Exception e) {
	        		e.printStackTrace();
	        	}
	        }	
	}

	@Override
	public ComparisonResults[] compare(GAProgressMonitor arg0,
			GAIndividual... arg1) throws ComparisonException {
		final List<ComparisonResults> lComparisons = new ArrayList<ComparisonResults>();
		final List<File> mutants=new ArrayList<File>();
		
		try {

			if (arg0 == null) {
				arg0 = new DummyProgressMonitor();
			}
			
			if(arg0.isCancelled()){
				throw new ComparisonCancelledException("Comparison cancelled");
			}
			
			setMutationDirectory(this.getOriginalProgramFile().getCanonicalPath());
			final GAIndividual[] individuals=arg1.clone();
			for(GAIndividual ind:individuals){
				
				mutants.add(generate(ind));
			}
			
			
			BPELUnitSpecification bpelUnitSpecification=new BPELUnitSpecification(this.getTestSuiteFile());
			bpelUnitSpecification.setDeploymentDirectory(bprDirectory);
			
			int cont=0;
			arg0.start(mutants.size());
			for(File f:mutants){
				
				if(arg0.isCancelled()){
					throw new ComparisonCancelledException("Comparison cancelled");
				}
				
				arg0.status(individuals[cont].individualFieldsToString());

				final EclipseMutantComparisonRunner comparisonRunner=new EclipseMutantComparisonRunner(this.getTestSuiteFile(), 
						new BPELUnitXMLResult(fOriginalResultFile));
				

				Pair<ExecutionResults, Set<Integer>> pair=null;
				List<ComparisonResult> lComparisonResult=new ArrayList<ComparisonResult>();
				try {
					pair = bpelUnitSpecification.run(AbortOnDiff.TEST, new BPELProcessDefinition(f), comparisonRunner);
					
					Set<Integer> results=pair.getRight();
					
					for(int i=0;i<pair.getLeft().getTestCaseCount();i++){
						if(results.contains(i)){
							lComparisonResult.add(ComparisonResult.DIFFERENT_OUTPUT);
						}
						else{
							lComparisonResult.add(ComparisonResult.SAME_OUTPUT);
						}
					}
					arg0.done(1);
				} catch (DeploymentException e) {
					for(int i=0;i<comparisonRunner.getTestCaseCount();i++){
						lComparisonResult.add(ComparisonResult.INVALID);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				lComparisons.add(new ComparisonResults(individuals[cont], lComparisonResult.toArray(
						new ComparisonResult[lComparisonResult.size()])));				
				
				f.delete();
				cont++;
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (GenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SpecificationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ComparisonCancelledException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		

		return lComparisons.toArray(new ComparisonResults[lComparisons.size()]);
	}

	@Override
	public List<String> getTestCaseNames() throws AnalysisException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void prepare() throws PreparationException {
			try {
				Pair<AeJettyRunner, File> pair=ActiveBPELUtils.startActiveBPEL(null, 8080, "full");
				server=pair.getLeft();
				bprDirectory=pair.getRight();
				
				if (bprDirectory != null) {
		            fRootWorkDirectory = bprDirectory.getParentFile();
		        }
				
				server.start();
				EclipseXMLDumperRunner runner = new EclipseXMLDumperRunner(new File(this.getTestSuite()));
				BPELUnitSpecification specification=new BPELUnitSpecification(new File(this.getTestSuite()));
				specification.setDeploymentDirectory(bprDirectory);
				ExecutionResults results=specification.run(new BPELProcessDefinition(new File(this.getOriginalProgram())), runner);
				fOriginalResultFile=results.getTestReportFile();
			} catch (final ConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final SpecificationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final DeploymentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final BPELPackagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final InvalidProcessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
