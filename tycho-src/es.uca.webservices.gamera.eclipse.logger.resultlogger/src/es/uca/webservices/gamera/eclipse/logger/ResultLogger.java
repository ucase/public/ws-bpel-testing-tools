package es.uca.webservices.gamera.eclipse.logger;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.api.util.Pair;

/**
 * This logger updates the result views with the results of the evolutionary algorithm.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
@Option(hidden=true)
public class ResultLogger implements GALogger{
	
	private List<ComparisonResults[]> alComparisonResults=new ArrayList<ComparisonResults[]>();

	/**
	 * This method sends the results to the plug-ins which show the results.
	 */
	@Override
	public void finished(GAState arg0) {
		//addGeneration(alComparisonResults, arg0);
		alComparisonResults.remove(0);

		GAHof hof = arg0.getHof();	
		AnalysisResults analysis = hof.getAnalysis();
		String[] names = analysis.getOperatorNames();
		Map<Integer, String> operatorsDictionary=new HashMap<Integer, String>();
		for(Entry<GAIndividual, ComparisonResults> pair : hof.entrySet()) {
			GAIndividual individual=pair.getKey();
			for(int i = 1; i <= individual.getOrder(); i++) {
			
			
				final BigInteger[] mutant = individual.getMutant(i);
				final BigInteger o = mutant[0];
				int position=1;
				for(int index=0; index<names.length && !names[o.intValue() - 1].equals(names[index]);index++){
					position++;
				}
				operatorsDictionary.put(position, names[o.intValue() - 1]);
			}
		}
		
		
		//Open the views if they aren't open
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				try {
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
						.showView("es.uca.webservices.gamera.eclipse.results.views.ResultsView");
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
						.showView("es.uca.webservices.gamera.eclipse.mutationstatistics.views.StatisticsView");

				} catch (PartInitException e) {
				
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
		
		// Send the OSGi event with the results
		BundleContext ctx = FrameworkUtil.
				getBundle(ResultLogger.class).getBundleContext();
        ServiceReference<EventAdmin> ref = ctx.getServiceReference(EventAdmin.class);
        EventAdmin eventAdmin = ctx.getService(ref);
        Map<String,Pair <List<ComparisonResults[]>, Map<Integer, String>>> properties = 
        		new HashMap<String, Pair < List<ComparisonResults[]>,
        		Map<Integer, String>>>();
        
        
        
        
        properties.put("MUTATIONRESULTS",  //$NON-NLS-1$
        		new Pair<List<ComparisonResults[]>, Map<Integer, String>>( 
        				alComparisonResults, 
        						operatorsDictionary));

     
        Event event = new Event("mutationcommunication/syncEvent", properties); //$NON-NLS-1$

       // eventAdmin.postEvent(event);
        eventAdmin.sendEvent(event);
		
	}

	@Override
	public void finishedAnalysis(GAState arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finishedComparison(GAState arg0, GAPopulation arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finishedComparison(GAState arg0, GAPopulation arg1,
			GAIndividual arg2, ComparisonResults arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finishedEvaluation(GAState arg0, GAPopulation arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void newGeneration(GAState arg0, GAPopulation arg1) {
		addGeneration(alComparisonResults, arg0);
		
	}
	
	/**
	 * This method concatenates the results of each generation.
	 */
	private void addGeneration(List<ComparisonResults[]> alResults, GAState state){
		GAHof hof = state.getHof();	
		ComparisonResults[] generationResults=new ComparisonResults[hof.entrySet().size()];
		int i=0;
		for(Entry<GAIndividual, ComparisonResults> pair : hof.entrySet()) {
			GAIndividual ind = (GAIndividual) pair.getKey();				

			ComparisonResult[] executionRow = pair.getValue().getRow();
			
			ComparisonResults results = new ComparisonResults();
			results.setRow(executionRow);
			results.setIndividual(ind);
			generationResults[i]=results;
			i++;
			
		}
		alResults.add(generationResults);
	}

	@Override
	public void started(GAState arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startedAnalysis(GAState arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startedComparison(GAState arg0, GAPopulation arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startedComparison(GAState arg0, GAPopulation arg1,
			GAIndividual arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startedEvaluation(GAState arg0, GAPopulation arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void appliedGeneticOperator(GAState arg0, String arg1,
			List<GAIndividual> arg2, List<GAIndividual> arg3) {
		// TODO Auto-generated method stub
		
	}

}
