package es.uca.webservices.bpelpackager.eclipse.builder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.activebpel.DeploymentArchivePackager;
import es.uca.webservices.bpelpackager.eclipse.Activator;

public class GenBPRBuilder extends IncrementalProjectBuilder {

	public static String ID = "genbpr.builder";

	@Override
	protected IProject[] build(int kind, Map<String, String> args, IProgressMonitor monitor) throws CoreException {
		
		final IProject proj = getProject();
		if (!proj.exists()) return null;

		proj.accept(new IResourceVisitor() {

			@Override
			public boolean visit(IResource resource) throws CoreException {
				if (resource instanceof IFile) {
					final IFile bpel = (IFile)resource;
					final IFile dest = bpel.getParent().getFile(new Path(bpel.getName() + ".bpr"));
					final File rawBPEL = new File(bpel.getLocationURI());
 
					if (bpel.getName().toLowerCase().endsWith(".bpel")) {
						try {
							if (!dest.exists()) {
								dest.create(new ByteArrayInputStream("".getBytes()), true, null);
							}
							bpel.deleteMarkers(Activator.MARKER, true, IResource.DEPTH_INFINITE);
							final BPELProcessDefinition def = new BPELProcessDefinition(rawBPEL);
							new DeploymentArchivePackager(def).generateBPR(new File(dest.getLocationURI()).getPath());
							dest.refreshLocal(IFile.DEPTH_INFINITE, null);
						}
						catch (Exception ex) {
							if (dest.exists()) {
								dest.delete(false, null);
							}
							IMarker marker = bpel.createMarker(Activator.MARKER);
							marker.setAttribute(IMarker.MESSAGE, "Failed to create " + dest.getName() + ": " + ex.getLocalizedMessage());
							marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);

							Activator.getDefault().logError("Failed to create " + dest.getName(), ex);
						}
					}
				}

				return resource instanceof IContainer;
			}
		});
		
		return null;
	}

}
