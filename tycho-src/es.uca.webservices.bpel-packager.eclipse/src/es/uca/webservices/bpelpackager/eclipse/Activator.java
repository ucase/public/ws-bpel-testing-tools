package es.uca.webservices.bpelpackager.eclipse;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Plug-in activator.
 *
 * @author Antonio García-Domínguez
 */
public class Activator extends AbstractUIPlugin {

	public static String ID = "es.uca.webservices.bpel-packager.eclipse";
	public static String MARKER = "genbpr.marker";

	private static Activator instance;

	public static Activator getDefault() {
		return instance;
	}

	public Activator() {
		instance = this;
	}

	public void logError(String msg, Throwable t) {
		getLog().log(new Status(IStatus.ERROR, Activator.ID, msg, t));
	}

}
