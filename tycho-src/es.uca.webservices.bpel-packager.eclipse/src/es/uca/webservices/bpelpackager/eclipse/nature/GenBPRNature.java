package es.uca.webservices.bpelpackager.eclipse.nature;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;

import es.uca.webservices.bpelpackager.eclipse.builder.GenBPRBuilder;

/**
 * Project nature that automatically generates .bpr files after every change.
 *
 * @author Antonio García-Domínguez
 */
public class GenBPRNature implements IProjectNature {

	public static String ID = "genbpr.nature";
	private IProject project;

	@Override
	public void configure() throws CoreException {
		final IProjectDescription desc = project.getDescription();
		final String builder = GenBPRBuilder.ID;
		if (findBuilder(desc, builder) == -1) {
			addBuilder(desc, builder);
		}
	}

	@Override
	public void deconfigure() throws CoreException {
		final IProjectDescription desc = project.getDescription();
		final String builder = GenBPRBuilder.ID;
		int posBuilder = findBuilder(desc, builder);
		if (posBuilder == -1) {
			// Nothing to do: the builder is not in the build spec
			return;
		}

		final ICommand[] oldSpec = desc.getBuildSpec();
		final ICommand[] newSpec = new ICommand[oldSpec.length - 1];
		if (posBuilder > 0) {
			System.arraycopy(oldSpec, 0, newSpec, 0, posBuilder);
		}
		if (posBuilder + 1 < oldSpec.length) {
			System.arraycopy(oldSpec, posBuilder + 1, newSpec, posBuilder, oldSpec.length - posBuilder - 1);
		}
		desc.setBuildSpec(newSpec);
		project.setDescription(desc, null);
	}

	@Override
	public IProject getProject() {
		return project;
	}

	@Override
	public void setProject(IProject project) {
		this.project = project;
	}

	/**
	 * Returns the position of the specified builder in the build spec, or -1 if it is not found.
	 */
	private int findBuilder(final IProjectDescription desc, final String builder)
	{
		int i = 0;
		for (ICommand cmd : desc.getBuildSpec()) {
			if (builder.equals(cmd.getBuilderName())) {
				return i;
			}
			++i;
		}
		return -1;
	}

	private void addBuilder(final IProjectDescription desc, String builder) throws CoreException {
		final ICommand[] oldSpec = desc.getBuildSpec();
		ICommand newCmd = desc.newCommand();
		newCmd.setBuilderName(builder);

		final ICommand[] newSpec = new ICommand[oldSpec.length + 1];
		System.arraycopy(oldSpec, 0, newSpec, 1, oldSpec.length);
		newSpec[0] = newCmd;
		desc.setBuildSpec(newSpec);
		project.setDescription(desc, null);
	}

}
