package es.uca.webservices.bpelpackager.eclipse.popup.actions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.InvalidProcessException;
import es.uca.webservices.bpel.activebpel.BPELPackagingException;
import es.uca.webservices.bpel.activebpel.DeploymentArchivePackager;

/**
 * Repackages a .bpel file as a .bpr file.
 *
 * @author Antonio García-Domínguez
 */
public class GenBPRAction extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final ISelection sel = HandlerUtil.getCurrentSelection(event);

		if (sel instanceof IStructuredSelection && !sel.isEmpty()) {
			final IStructuredSelection ssel = (IStructuredSelection) sel;
			final List<IFile> inputFiles = collectSelectedIFiles(ssel);
			final List<String> inputPaths = mapIFilesToPaths(inputFiles);

			try {
				assert inputPaths.size() == 1;
				final String pathToBPEL = inputPaths.get(0);
				generateBPR(pathToBPEL, pathToBPEL + ".bpr");

				final IProject project = inputFiles.get(0).getProject();
				project.refreshLocal(IProject.DEPTH_INFINITE, null);
			} catch (CoreException e) {
				throw new ExecutionException("Could not refresh the project", e);
			} catch (Exception e) {
				throw new ExecutionException("Could not generate the BPR file", e);
			}
		}

		return null;
	}

	private void generateBPR(String inputBPEL, String targetFile)
			throws XPathExpressionException, ParserConfigurationException,
			SAXException, IOException, InvalidProcessException,
			BPELPackagingException {
		final BPELProcessDefinition def = new BPELProcessDefinition(new File(
				inputBPEL));
		new DeploymentArchivePackager(def).generateBPR(targetFile);
	}

	private List<IFile> collectSelectedIFiles(final IStructuredSelection ssel)
			throws ExecutionException {
		final List<IFile> inputPaths = new ArrayList<IFile>();
		for (Object o : ssel.toList()) {
			if (o instanceof IFile) {
				final IFile iFile = (IFile) o;
				inputPaths.add(iFile);
			}
		}
		if (inputPaths.isEmpty()) {
			throw new ExecutionException("Selection cannot be empty");
		}
		return inputPaths;
	}

	private List<String> mapIFilesToPaths(final List<IFile> iFiles)
			throws ExecutionException {
		final List<String> inputPaths = new ArrayList<String>();
		for (IFile iFile : iFiles) {
			try {
				final File file = new File(iFile.getLocationURI());
				inputPaths.add(file.getCanonicalPath());
			} catch (IOException e) {
				throw new ExecutionException(
						"Error when converting IFile to File", e);
			}
		}
		return inputPaths;
	}

}
