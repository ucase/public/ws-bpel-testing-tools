package es.uca.webservices.bpelpackager.eclipse.nature;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;

/**
 * Adds the BPR generation nature to a project. Has bits of code from this
 * article:
 * 
 * http://codeandme.blogspot.com.es/2012/10/integrating-custom-builder.html
 * 
 * @author Antonio García-Domínguez
 */
public class AddNatureHandler extends AbstractNatureHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IProject project = getProject(event);

		try {
			if (!hasNature(project)) {
				final IProjectDescription description = project.getDescription();
				final String[] natures = description.getNatureIds();
				final String[] newNatures = new String[natures.length + 1];
				System.arraycopy(natures, 0, newNatures, 0, natures.length);
				newNatures[natures.length] = GenBPRNature.ID;
				description.setNatureIds(newNatures);
				project.setDescription(description, null);
			}

			return null;
		} catch (Exception ex) {
			throw new ExecutionException(
					"Could not add the BPR generation nature", ex);
		}
	}
}
