package es.uca.webservices.bpelpackager.eclipse.nature;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;

/**
 * Removes the .bpr generation nature from a project.
 *
 * @author Antonio García-Domínguez
 */
public class RemoveNatureHandler extends AbstractNatureHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IProject project = getProject(event);

		try {
			final IProjectDescription description = project.getDescription();
			final String[] natures = description.getNatureIds();
			final int naturePos = findNature(natures);

			if (naturePos != -1) {
				final String[] newNatures = new String[natures.length - 1];
				if (naturePos > 0) {
					System.arraycopy(natures, 0, newNatures, 0, naturePos);
				}
				if (naturePos + 1 < natures.length) {
					System.arraycopy(natures, naturePos + 1, newNatures, naturePos, natures.length - naturePos - 1);
				}
				description.setNatureIds(newNatures);
				project.setDescription(description, null);
			}

			return null;
		} catch (Exception ex) {
			throw new ExecutionException(
					"Could not remove the BPR generation nature", ex);
		}
	}

	private int findNature(final String[] natures) {
		int i = 0;
		for (String id : natures) {
			if (GenBPRNature.ID.equals(id)) {
				return i;
			}
			++i;
		}
		return -1;
	}

}
