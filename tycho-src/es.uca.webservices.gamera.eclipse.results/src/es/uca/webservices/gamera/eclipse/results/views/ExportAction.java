package es.uca.webservices.gamera.eclipse.results.views;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.eclipse.jface.action.Action;  
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;  
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;  
  

/**
 * This class exports the table in CSV format.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class ExportAction extends Action implements IWorkbenchAction{  

	private static final String ID = "es.uca.webservices.gamera.eclipse.results.views.ExportAction";  
	private Table tbResults;
	
	public ExportAction(Table tbResults){  
		setId(ID);  
		this.tbResults=tbResults;
	}  

	public void run() {  

		//Show the dialog to save the file
		final FileDialog fdExportFiles = new FileDialog(new Shell(), SWT.SAVE);
		fdExportFiles.open();
		if(!fdExportFiles.getFileName().equals("")){
			try {
				//Create the file
				PrintWriter exit=new PrintWriter(fdExportFiles.getFilterPath()+"/"+fdExportFiles.getFileName()+".csv");
				StringBuffer output=new StringBuffer();
				//Copy the table into the file
				for(int i=0; i<tbResults.getItemCount(); i++){
					for(int j=0; j<tbResults.getColumnCount(); j++){
						output.append(tbResults.getItem(i).getText(j));
						if(j!=tbResults.getColumnCount()-1){
								output.append(",");
						}
					}
					exit.println(output);
					output=output.delete(0,output.length());
				}
				exit.close();
			} catch (FileNotFoundException e1) {
			//	LOGGER.error("Exception to save the CSV", e1);
			}
		}
	}  

	public void dispose() {}  

}  