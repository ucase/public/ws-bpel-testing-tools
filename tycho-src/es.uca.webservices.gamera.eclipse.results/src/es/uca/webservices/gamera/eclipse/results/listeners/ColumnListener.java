package es.uca.webservices.gamera.eclipse.results.listeners;

import java.text.Collator;
import java.util.Locale;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

/**
 * This class sorts the table by the select column.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class ColumnListener extends SelectionAdapter {
	private Table tbResults;
	private int columnPosition;
	private int carry;
	
	/**
	 * Construct of the class
	 * @param tbResults Table with the results
	 * @param columnPosition Number of the column
	 */
	public ColumnListener(Table tbResults, int columnPosition, int carry){
		this.tbResults=tbResults;
		this.columnPosition=columnPosition;
		this.carry=carry;
	}
	
	@Override
	/**
	 * Column event handler
	 */
	public void widgetSelected(SelectionEvent e) {

	    quicksort(0, tbResults.getItems().length - 1);
	}

	private void quicksort(int low, int high) {
	    int i = low, j = high;
	    String pivot = tbResults.getItem(low + (high-low)/2).getText(columnPosition);
	    Collator collator = Collator.getInstance(Locale.getDefault());

	    // Divide into two lists
	    while (i <= j) {
	    	
	      while (collator.compare(tbResults.getItem(i).getText(columnPosition), pivot) < 0) {
	        i++;
	      }

	      while (collator.compare(tbResults.getItem(j).getText(columnPosition), pivot) > 0) {
	        j--;
	      }

	      if (i <= j) {
	        exchange(tbResults.getItem(i), tbResults.getItem(j));
	        i++;
	        j--;
	      }
	    }
	    // Recursion
	    if (low < j)
	      quicksort(low, j);
	    if (i < high)
	      quicksort(i, high);
	  }
	
	private void exchange (TableItem i, TableItem j){
		String[] aux1 = new String[tbResults.getColumnCount()];
		String[] aux2 = new String[tbResults.getColumnCount()];
		Color color1= i.getBackground(0);
		Color color2= j.getBackground(0);
		for(int k=0;k<tbResults.getColumnCount();k++){
			aux1[k]=i.getText(k);
			aux2[k]=j.getText(k);
		}
		i.setText(aux2);
		j.setText(aux1);
		
		for(int k=0;k<carry;k++){
			i.setBackground(k,color2);
			j.setBackground(k, color1);
		}
		
	}

}
