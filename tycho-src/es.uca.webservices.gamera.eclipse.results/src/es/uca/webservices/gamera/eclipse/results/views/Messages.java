package es.uca.webservices.gamera.eclipse.results.views;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "es.uca.webservices.gamera.eclipse.results.views.messages"; //$NON-NLS-1$
	public static String ResultsView_1;
	public static String ResultsView_2;
	public static String ResultsView_3;
	public static String ResultsView_4;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
