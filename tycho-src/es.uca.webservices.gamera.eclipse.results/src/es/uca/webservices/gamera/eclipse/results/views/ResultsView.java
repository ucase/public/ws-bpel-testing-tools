package es.uca.webservices.gamera.eclipse.results.views;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.*;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.SWT;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;


import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.util.Pair;
import es.uca.webservices.gamera.eclipse.results.listeners.ColumnListener;

/**
 * This class creates the view to show the mutation results.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class ResultsView extends ViewPart implements  ISelectionListener{

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "es.uca.webservices.gamera.eclipse.results.views.ResultsView"; //$NON-NLS-1$
	private Table tbResults;

	private int carry=0;
	Random randomGenerator = new Random();


	
	public ResultsView() {
        super();
	}
	    
    public void setFocus() {
    }
    
    public void createPartControl(final Composite parent) {
    	
    	parent.setLayout(new GridLayout(1, true));

		
		//Create the table
		tbResults=new Table(parent, SWT.BORDER);
		tbResults.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tbResults.setHeaderVisible(true);
		tbResults.setToolTipText(""); //$NON-NLS-1$
		tbResults.setLinesVisible(true);
		tbResults.setItemCount(0);
		tbResults.addControlListener(new ControlAdapter() {
		    public void controlResized(ControlEvent e) {
		    	for(TableColumn column:tbResults.getColumns()){
					column.setWidth(tbResults.getSize().x/tbResults.getColumnCount());
				}
		    	tbResults.redraw();
		    }
		});
		/*
		 * Update the table with the OSGi event.
		 */
		BundleContext ctx = FrameworkUtil.getBundle(ResultsView.class).getBundleContext();
		EventHandler handler = new EventHandler() {
			public void handleEvent(final Event event) {
				if(!parent.isDisposed()){
					parent.getDisplay().syncExec(new Runnable() {
						public void run() {
							carry=0;
							tbResults.clearAll();
							tbResults.setItemCount(0);

							//Clear the table
							while(tbResults.getColumnCount()>0){
								tbResults.getColumn(0).dispose();
							}

							@SuppressWarnings("unchecked")
							Pair <List<ComparisonResults[]>, Map<Integer, String>> pResults=(Pair <List<ComparisonResults[]>, 
									Map<Integer, String>>) event.getProperty("MUTATIONRESULTS"); //$NON-NLS-1$
							List<ComparisonResults[]> generationResults=pResults.getLeft();
							Map<Integer, String> operatorsDictionary=pResults.getRight();
							if(generationResults.size()>0){
								int maxNumOperator=0;
								int numTest=0;
								for(ComparisonResults[] results:generationResults){
									for(int i=0; i<results.length;i++){
										if(maxNumOperator<results[i].getIndividual().getOrder()){
											maxNumOperator=results[i].getIndividual().getOrder();
										}
										if(numTest<results[i].getRow().length){
											numTest=results[i].getRow().length;
										}
									}
								}
								
								final TableColumn tbGeneration= new TableColumn(tbResults, SWT.CENTER);
								tbGeneration.setResizable(false);
								tbGeneration.setText(Messages.ResultsView_4);
								tbGeneration.pack();
								
								carry++;
								
								for(int op=0;op<maxNumOperator;op++){
									
									final TableColumn tbcolOperator = new TableColumn(tbResults, SWT.CENTER);
									tbcolOperator.setResizable(false);
									tbcolOperator.setText(Messages.ResultsView_3+" "+(op+1));
									tbcolOperator.pack();
									carry++;
								}
								
								for(int i=0;i<carry;i++){
									tbResults.getColumn(i).addSelectionListener(new ColumnListener(tbResults, i, carry));

								}
								
								for(int i=0;i<numTest;i++){
									final TableColumn tbcolumn = new TableColumn(tbResults, SWT.CENTER);
									tbcolumn.setResizable(false);
									//Set the name of the column
									tbcolumn.setText(Messages.ResultsView_1+" "+(i+1)); //$NON-NLS-2$
									tbcolumn.pack();
									tbcolumn.addSelectionListener(new ColumnListener(tbResults, i+carry, carry));
								}
								for(TableColumn column:tbResults.getColumns()){

									column.setWidth(tbResults.getSize().x/tbResults.getColumnCount());
								}
								tbGeneration.addSelectionListener(new ColumnListener(tbResults, 0, carry));
								int row=0;
								int generation=0;
								int acumulation=0;
								List<Color> rgb=new ArrayList<Color>();
								for(ComparisonResults[] cResults: generationResults){
									if(cResults.length>0){
										Device device = Display.getCurrent ();
										if(generation%4==0){
											Random random=new Random();
											int r=random.nextInt(30);
											Color color1=new Color(device, acumulation, 220+r, 100+r);//green
											Color color2=new Color(device, 90+r, 220-r, 255-acumulation);//blue
											Color color3=new Color(device, 230-r, 255-acumulation, 50-r); //yellow
											Color color4=new Color(device, 230-r, 90+r, 255-acumulation);//red
											
											rgb.clear();
											
											rgb.add(color1);
											rgb.add(color3);
											rgb.add(color2);
											rgb.add(color4);
											acumulation+=10;
											acumulation=acumulation%255;
										}
	
										for(ComparisonResults r:cResults){
											addResult(generation, r, operatorsDictionary, row, rgb.get(generation%4));
											row++;
										}
										generation++;
									}
								}
								
							}
						}
					});
				}

			}
		};

		Dictionary<String,String> properties = new Hashtable<String, String>();
		properties.put(EventConstants.EVENT_TOPIC, "mutationcommunication/*"); //$NON-NLS-1$
		ctx.registerService(EventHandler.class, handler, properties);

		ExportAction exportAction = new ExportAction(tbResults);  
    	exportAction.setText(Messages.ResultsView_2);  
    	getViewSite().getActionBars().getMenuManager().add(exportAction);
    	
    	getSite().getPage().addSelectionListener("es.uca.webservices.gamera.eclipse.results",(ISelectionListener)this); //$NON-NLS-1$

    }
	
	/**
	 * Method to add an identification of mutant to the table
	 * @param mutantName Name of the mutant
	 * @param mutantLine Line of the mutant
	 * @param mutantValue Value of the mutant
	 */
	void addResult(int generation, ComparisonResults r, Map<Integer, String> operatorsDictionary, int row, Color color){
		
		
		
		final int newItemCount = tbResults.getItemCount()+1;
		tbResults.setItemCount(newItemCount);
		//Create the row
		TableItem tableItem = tbResults.getItem(newItemCount - 1);
		tableItem.setText(0, Integer.toString(generation));

		for(int i=0;i<r.getIndividual().getOrder();i++){
			
			String operator="{"+operatorsDictionary.get(r.getIndividual().getOperator().get(i).intValue())+
					", "+r.getIndividual().getLocation().get(i).toString()+", "+
					r.getIndividual().getAttribute().get(i).toString()+"}";
			
			tableItem.setText(i+1, operator);			
	
		}

		int column=0;
		for(ComparisonResult a:r.getRow()){
    		tbResults.getItem(row).setText(column+carry, String.valueOf(a.getValue()));
    		column++;
    	}
		
		//Put the color
		for(int i=0;i<carry;i++){
			tableItem.setBackground(i,color);
		}
	
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
	}
	

}