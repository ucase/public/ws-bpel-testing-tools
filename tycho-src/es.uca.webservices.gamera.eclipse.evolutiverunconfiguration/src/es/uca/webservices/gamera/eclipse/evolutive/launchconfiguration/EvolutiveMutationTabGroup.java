package es.uca.webservices.gamera.eclipse.evolutive.launchconfiguration;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;

/**
 * This class creates all the tabs of the launch configuration.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class EvolutiveMutationTabGroup extends AbstractLaunchConfigurationTabGroup{

	@Override
	public void createTabs(ILaunchConfigurationDialog dialog, String mode) {
		ILaunchConfigurationTab[] tabs = new ILaunchConfigurationTab[] {
				new EvolutiveTab(),
				new CommonTab()
		};
		setTabs(tabs);
		
	}

}
