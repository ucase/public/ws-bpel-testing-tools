package es.uca.webservices.gamera.eclipse.evolutive.launchconfiguration;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.yaml.snakeyaml.Yaml;


import es.uca.webservices.gamera.AbstractGenerationalAlgorithm;
import es.uca.webservices.gamera.GeneticAlgorithm;
import es.uca.webservices.gamera.conf.Configuration;
import es.uca.webservices.gamera.conf.ConfigurationValidationMode;

import es.uca.webservices.gamera.eclipse.monitor.MonitorLogger;
import es.uca.webservices.gamera.eclipse.logger.ResultLogger;
import es.uca.webservices.gamera.eclipse.ui.gameraconfigurator.editors.CustomClassLoaderConstructorExtensionPoint;

/**
 * This class launchs the launch configuration of GAmeraHOM and executes the evolutionary algorithm.
 * @author Juan Daniel Morcillo regueiro
 *
 */
public class EvolutiveLaunchConfigurationDelegate implements ILaunchConfigurationDelegate{

	@Override
	public void launch(ILaunchConfiguration configuration, String mode,
			ILaunch launch, IProgressMonitor monitor) throws CoreException {
		AbstractGenerationalAlgorithm generator = new GeneticAlgorithm();
		Yaml y = new Yaml(new CustomClassLoaderConstructorExtensionPoint(Configuration.class.getClassLoader()));
		try {
			
			IPath path=new Path(configuration.getAttribute("yamlPath", "")); //$NON-NLS-1$ //$NON-NLS-2$
			IFile file= ResourcesPlugin.getWorkspace().getRoot().getFile(path);	
			Configuration gameraConfiguration=(Configuration) y.load(file.getContents());
			gameraConfiguration.getLoggers().add(new ResultLogger());
			gameraConfiguration.getLoggers().add(new MonitorLogger(monitor));
			generator.setConfiguration(gameraConfiguration);

        	gameraConfiguration.getPRNG();
        	ConfigurationValidationMode validationMode=null;
        	for(ConfigurationValidationMode m:ConfigurationValidationMode.values()){
        		if(m.name().equals(configuration.getAttribute("validationModes", ""))){ //$NON-NLS-1$ //$NON-NLS-2$
        			validationMode=m;
        		}
        	}
        	gameraConfiguration.validate(validationMode);
			generator.run();
					
		} catch (final Exception e) {
			final Status status = 
					new Status(IStatus.ERROR,
							"SCS Admin", e.getLocalizedMessage(), e); //$NON-NLS-1$
			Display.getDefault().syncExec(new Runnable() {
				@Override
				public void run() {
					ErrorDialog errorDialog = new ErrorDialog(new Shell(), Messages.EvolutiveLaunchConfigurationDelegate_4,
							Messages.EvolutiveLaunchConfigurationDelegate_5, status, Status.ERROR);
					errorDialog.open();
					e.printStackTrace();
				}
			});
		}
		
	}

}
