package es.uca.webservices.gamera.eclipse.evolutive.launchconfiguration;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "es.uca.webservices.gamera.eclipse.evolutive.launchconfiguration.messages"; //$NON-NLS-1$
	public static String EvolutiveLaunchConfigurationDelegate_4;
	public static String EvolutiveLaunchConfigurationDelegate_5;
	public static String EvolutiveTab_0;
	public static String EvolutiveTab_1;
	public static String EvolutiveTab_12;
	public static String EvolutiveTab_2;
	public static String EvolutiveTab_3;
	public static String EvolutiveTab_5;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
