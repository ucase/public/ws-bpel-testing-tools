package es.uca.webservices.gamera.eclipse.evolutive.launchconfiguration;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import es.uca.webservices.gamera.eclipse.dialog.ExtensionValidator;
import es.uca.webservices.gamera.eclipse.dialog.TypeViewerFilter;
import es.uca.webservices.gamera.conf.ConfigurationValidationMode;

/**
 * This class implements the launch configuration for the evolutive mutation of
 * GAmeraHOM.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class EvolutiveTab extends AbstractLaunchConfigurationTab{
	
	private Text yamlPath;
	private Button[] validationModes;

	/**
	 * This method creates the interface of the launch configuration.
	 */
	@Override
	public void createControl(Composite parent) {
		
		Composite comp = new Composite(parent, SWT.NONE);
		setControl(comp);
		comp.setLayout(new GridLayout(1, true));
		comp.setFont(parent.getFont());
		
		Group gFile=new Group(comp, SWT.FILL);
		gFile.setLayout(new GridLayout(2, false));
		gFile.setText(Messages.EvolutiveTab_0);
		gFile.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, true, 1, 1));
		yamlPath=new Text(gFile, SWT.BORDER | SWT.READ_ONLY);
		yamlPath.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, true, 1, 1));
		Button btnBrowse=new Button(gFile, SWT.NONE);
		btnBrowse.setText(Messages.EvolutiveTab_1);
		btnBrowse.addSelectionListener(new SelectionAdapter(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				/*
				 * An dialog is created to show the files with YAML extension.
				 */
				ElementTreeSelectionDialog select=new ElementTreeSelectionDialog(new Shell(), new WorkbenchLabelProvider(),
					    new BaseWorkbenchContentProvider());
				select.setTitle(Messages.EvolutiveTab_2);
				select.setMessage(Messages.EvolutiveTab_3);
				select.setInput(ResourcesPlugin.getWorkspace().getRoot());
				List<String>extension=new ArrayList<String>();
				extension.add("yaml"); //$NON-NLS-1$
				select.addFilter(new TypeViewerFilter(extension));
				select.setAllowMultiple(false);
				select.setValidator(new ExtensionValidator());
				select.open();
				if(select.getResult()!=null){
					yamlPath.setText(((IFile)select.getResult()[0]).getFullPath().toString());
					update();
				}
			}
		});
		
		Group gValidationMode=new Group(comp, SWT.FILL);
		gValidationMode.setLayout(new GridLayout(1, false));
		gValidationMode.setText(Messages.EvolutiveTab_5);
		gValidationMode.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, true, 1, 1));
		List<ConfigurationValidationMode> modes=Arrays.asList(ConfigurationValidationMode.values());
		Collections.sort(modes);
		validationModes = new Button[modes.size()];
	    int cont=0;
		for(ConfigurationValidationMode c:modes){
			validationModes[cont] =this.createRadioButton(gValidationMode, c.name());
			 
			validationModes[cont].addSelectionListener(new SelectionAdapter(){
				@Override
				public void widgetSelected(SelectionEvent e) {
					updateLaunchConfigurationDialog();
				}
			});
			cont++;
		}
		
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * This method loads the last state.
	 */
	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		try {
			yamlPath.setText(configuration.getAttribute("yamlPath", "")); //$NON-NLS-1$ //$NON-NLS-2$
			boolean selected=false;
			for(Button b:validationModes){
				if(b.getText().equals(configuration.getAttribute("validationModes", ""))){ //$NON-NLS-1$ //$NON-NLS-2$
					b.setSelection(true);
					selected=true;
				}
				else{
					b.setSelection(false);
				}
			}
			if(!selected){
				validationModes[0].setSelection(true);
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * This method saves the current state.
	 */
	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute("yamlPath", yamlPath.getText()); //$NON-NLS-1$
		for(Button b:validationModes){
			if(b.getSelection()){
				configuration.setAttribute("validationModes",b.getText()); //$NON-NLS-1$
			}
		}
		
		
	}

	@Override
	public String getName() {
		return Messages.EvolutiveTab_12;
	}
	
	@Override
	public boolean isValid(ILaunchConfiguration launchConfig) {
		return !yamlPath.getText().equals(""); //$NON-NLS-1$

	}
	
	public void update(){
		this.updateLaunchConfigurationDialog();
	}

}
