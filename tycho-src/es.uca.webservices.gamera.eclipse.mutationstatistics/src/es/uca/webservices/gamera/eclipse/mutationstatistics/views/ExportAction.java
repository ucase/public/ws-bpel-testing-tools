package es.uca.webservices.gamera.eclipse.mutationstatistics.views;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.w3c.dom.DOMImplementation;
import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

/**
 * This class export the statistics in several formats.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class ExportAction extends Action implements IWorkbenchAction{
	
	private static final String ID = 
			"es.uca.webservices.gamera.eclipse.mutationstatistics.views.ExportAction";  
	
	private ChartPanel chart;
	
	public ExportAction(ChartPanel chart){  
		setId(ID);  
		this.chart=chart;
	}  

	/**
	 * Method to save the statistics.
	 */
	public void run() { 
		//Create the save dialog
		final FileDialog fdExportFiles = new FileDialog(new Shell(), SWT.SAVE);
		fdExportFiles.setFilterExtensions(new String[]{"*.pdf", "*.png", "*.svg"});
		fdExportFiles.open();
		String path=fdExportFiles.getFilterPath()+"/"+fdExportFiles.getFileName();

        if(!fdExportFiles.getFileName().equals("")){
        	//If the extension is png
			if(fdExportFiles.getFilterExtensions()[fdExportFiles.getFilterIndex()].equals("*.png")){
				saveAsPNG(path);
			}
			//If the extension is pdf
			if(fdExportFiles.getFilterExtensions()[fdExportFiles.getFilterIndex()].equals("*.pdf")){
                saveAsPDF(path);
			}
			//If the extension is svg
			if(fdExportFiles.getFilterExtensions()[fdExportFiles.getFilterIndex()].equals("*.svg")){
				saveAsSVG(path);
			}	
        }
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Method to save the statistic in png
	 * @param path Path to save the statistic
	 */
	public void saveAsPNG(final String path){
		try {
			//Get the size of the statistic
			final int width=chart.getSize().width;
			final int height=chart.getSize().height;
			//Create the png file
			ChartUtilities.saveChartAsPNG(new File(path+".png"), chart.getChart(), 
			        width, height);
		} catch (IOException e1) {
			//LOGGER.error("Exception to save the png", e1);
		}
	}
	
	/**
	 * Method to save the statistic in pdf
	 * @param path Path to save the statistic
	 */
	public void saveAsPDF(final String path){
		//Get the size of the statistic
		final int width=chart.getSize().width;
		final int height=chart.getSize().height;
		//BufferedImage buffer=chart.getChart().createBufferedImage(width, height);
		BufferedImage buffer=chart.getChart().createBufferedImage(width, height, 
				BufferedImage.TYPE_INT_RGB, chart.getChartRenderingInfo());
		
		
		
		PDDocument document;
		try {
			document=new PDDocument();
			PDPage page=new PDPage(new PDRectangle(width, height));
			document.addPage(page);
			
			PDXObjectImage ximage = new PDJpeg(document, buffer);
			PDPageContentStream content = new PDPageContentStream(document, page);
			content.drawXObject(ximage, 0, 0, width, height);
            content.close();
            document.save(path+".pdf");
            document.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (COSVisitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   		
}
	
	/**
	 * Method to save the statistic in svg
	 * @param path Path to save the statistic
	 */
	public void saveAsSVG(final String path){
		
		final int width=chart.getSize().width;
		final int height=chart.getSize().height;
		
		DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
        org.w3c.dom.Document document = domImpl.createDocument(null, "svg", null);

        //Create an instance of the SVG Generator
        SVGGraphics2D svgGenerator = new SVGGraphics2D(document);

        //Draw the chart in the SVG generator
        Rectangle2D bounds=new Rectangle2D.Double(0, 0, width, height);
        chart.getChart().draw(svgGenerator, bounds);

        //Write svg file
        OutputStream outputStream;
		try {
			outputStream = new FileOutputStream(path+".svg");
			Writer out = new OutputStreamWriter(outputStream, "UTF-8");
	        svgGenerator.stream(out, true /* use css */);						
	        outputStream.flush();
	        outputStream.close();
		} catch (IOException e1) {
			//LOGGER.error("Exception to save the svg", e1);
		}
	}

}