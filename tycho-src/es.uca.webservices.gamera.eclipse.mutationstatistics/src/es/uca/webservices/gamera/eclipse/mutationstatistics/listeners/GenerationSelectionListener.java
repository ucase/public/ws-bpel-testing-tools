package es.uca.webservices.gamera.eclipse.mutationstatistics.listeners;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Event;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.eclipse.mutationstatistics.views.Chart;

/**
 * This listener updates the statistics when the selected generation changes.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class GenerationSelectionListener extends SelectionAdapter {
	
	private Combo cbGeneration, cbOperator, cbState;
	private List<ComparisonResults[]> generationResults;
	private Map<String, List<Integer>> mutantsByOperator;
	private ChartPanel chartGlobal, chartByOperator;
	private DecimalFormat formatter = new DecimalFormat ("###.##"); //$NON-NLS-1$
	private List<BigInteger> selected;
	

	public GenerationSelectionListener(Combo cbGeneration, Combo cbOperator,
			Combo cbState, List<ComparisonResults[]> generationResults,
			Map<String, List<Integer>> mutantsByOperator,
			ChartPanel chartGlobal,
			ChartPanel chartByOperator, List<BigInteger> selected) {
		this.cbGeneration=cbGeneration;
		this.cbOperator=cbOperator;
		this.cbState=cbState;
		this.generationResults=generationResults;
		this.mutantsByOperator=mutantsByOperator;
		this.chartGlobal=chartGlobal;
		this.chartByOperator=chartByOperator;
		this.selected=selected;
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		selected.clear();
		 double globalValues[]={0,0,0};
		 int generation= Integer.valueOf(cbGeneration.getText());
		 //Calculate the percentages
		  getPercentsByOperator(globalValues,0, generationResults.get(generation).length, generationResults.get(generation));
		  DefaultPieDataset dpdGlobal = new DefaultPieDataset();

		  //Create the global statistic
		  dpdGlobal.setValue(Messages.GenerationListener_1+": "+ formatter.format(globalValues[0])+"%",globalValues[0]); //$NON-NLS-2$ //$NON-NLS-3$
		  dpdGlobal.setValue(Messages.GenerationListener_2+": "+ formatter.format(globalValues[2])+"%", globalValues[2]); //$NON-NLS-2$ //$NON-NLS-3$
		  dpdGlobal.setValue(Messages.GenerationListener_3+": "+ formatter.format(globalValues[1])+"%", globalValues[1]); //$NON-NLS-2$ //$NON-NLS-3$


		  chartGlobal.setChart(Chart.createChart(Messages.GenerationListener_4, 
				  ChartFactory.createPieChart3D(Messages.GenerationListener_5, dpdGlobal, true, true, false)));
		  chartGlobal.repaint();


		  DefaultCategoryDataset dcdMutantForOperator = new DefaultCategoryDataset();
		  for(String operator:mutantsByOperator.keySet()){
			  dcdMutantForOperator.setValue(mutantsByOperator.get(operator).get(generation), operator, ""); //$NON-NLS-1$
		  }

		  JFreeChart jfcMutantForOperator = ChartFactory.createBarChart3D(
				  Messages.GenerationListener_6,
				  Messages.GenerationListener_7,
				  Messages.GenerationListener_8,
				  dcdMutantForOperator,
				  PlotOrientation.VERTICAL,
				  true,
				  true,
				  false
				  );

		  chartByOperator.setChart(jfcMutantForOperator);
		  chartByOperator.getParent().repaint();
		  
		  cbOperator.notifyListeners(SWT.Selection, new Event());
		  cbState.notifyListeners(SWT.Selection, new Event());

	}
	
	/**
	 * Method to calculate the percentage by operator
	 * @param values Vector to save the percentages
	 * @param start Starting position of the operator on the vector of results
	 * @param end Ending position of the operator on the vector of results
	 */
	void getPercentsByOperator(double[] values, int start, int end, ComparisonResults[] cResults){
		for(int i=start; i<end;i++){
			ComparisonResults aux=cResults[i];
			int add=0;
			//Sum of the results of a mutation
			for(int j=0; j<aux.getRow().length;j++){
				add+=aux.getRow()[j].getValue();
			}
			//The mutant is alive
			if(add==0){
				values[0]++;
			}
			//The mutant is fail
			else if(add==aux.getRow().length*2){
				values[1]++;
			}
			//The mutant is dead
			else{
				values[2]++;
			}
		}
		//Create the percentages
		values[0]=values[0]*100/(end-start);
		values[1]=values[1]*100/(end-start);
		values[2]=values[2]*100/(end-start);
	}

}