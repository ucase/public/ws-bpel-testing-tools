package es.uca.webservices.gamera.eclipse.mutationstatistics.views;

import java.awt.Frame;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.eclipse.mutationstatistics.views.Messages;

/**
 * Class with utilities to create charts.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class Chart {
	/*
	 * This method creates a chart with a title and the data.
	 */
	static public JFreeChart createChart(String title, JFreeChart jfcPie){  

		PiePlot piePlot = (PiePlot) jfcPie.getPlot();
		piePlot.setDirection(Rotation.CLOCKWISE);
		if(title.equals("")){ //$NON-NLS-1$
			piePlot.setNoDataMessage(Messages.StatisticsView_18);
		}

		JFreeChart jfc = new JFreeChart(title, null, piePlot, true); 
		
		return jfc;
	}
	
	/*
	 * This method creates a panel.
	 */
	static public ChartPanel createChartComposite(Composite composite, JFreeChart jfc){
		Frame frame = SWT_AWT.new_Frame(composite);
		ChartPanel chart = new ChartPanel(jfc);  
        chart.setDisplayToolTips(true);  
        chart.setHorizontalAxisTrace(false);  
        chart.setVerticalAxisTrace(false); 
        frame.add(chart);
        return chart;
	}
	
	/**
	 * This method determines if a determinate ComparisonResults belongs
	 * to an operator.
	 */
	static private boolean addOperator(List<BigInteger> selected, 
			ComparisonResults result, Map<Integer, String> operatorsDictionary, 
			Map<String, Integer> operatorsByState){
		
		int cont=0;
		boolean available=true;
		for(BigInteger b: selected){
			if(result.getIndividual().getOperator().size()>cont &&
					!result.getIndividual().getOperator().get(cont).equals(b)){
				available=false;
			}
			cont++;
		}
		if(available && result.getIndividual().getOrder()>selected.size()){
			String operator=operatorsDictionary.get(result.getIndividual().getOperator().get(selected.size())
					.intValue());
            if(operatorsByState.containsKey(operator)){
            	operatorsByState.put(operator, operatorsByState.get(operator)+1);
            }
            else{
            	operatorsByState.put(operator, 1);
            }
            return true;
		}
		else if(available){
			if(operatorsByState.containsKey(Messages.StatisticsView_7)){
            	operatorsByState.put(Messages.StatisticsView_7, operatorsByState.get(Messages.StatisticsView_7)+1);
            }
            else{
            	operatorsByState.put(Messages.StatisticsView_7, 1);
            }
			return true;
		}
		return false;
		
	}
	
	/**
	 * This methods creates the statistics by state.
	 */
	static public JFreeChart stateChart(String title, int generation, String state, List<BigInteger> selected, 
			List<ComparisonResults[]>results, Map<Integer, String> operatorsDictionary){
		
		final DefaultPieDataset dpdState = new DefaultPieDataset();
		DecimalFormat formatter = new DecimalFormat ("###.##");
		Map<String, Integer> operatorsByState=new HashMap<String, Integer>();
		int total=0;
        for(ComparisonResults crAux : results.get(generation)){
			if(state.equals(Messages.StatisticsView_2) && crAux.isAlive()) {
                // The mutant is alive
				if(addOperator(selected, crAux, operatorsDictionary, operatorsByState)){
					total++;
				}
               
			}
			else if(state.equals(Messages.StatisticsView_4) && crAux.isInvalid()) {
                // The mutant is invalid
				if(addOperator(selected, crAux, operatorsDictionary, operatorsByState)){
					total++;
				}
                
			}
			else if(state.equals(Messages.StatisticsView_3) && crAux.isDead()) {
                // The mutant is dead
				if(addOperator(selected, crAux, operatorsDictionary, operatorsByState)){
					total++;
				}
                
			}	
		}
        boolean skip=false;
        for(String s:operatorsByState.keySet()){
        	double percentage=operatorsByState.get(s)*100/total;
        	dpdState.setValue(s+": "+ formatter.format(percentage)+"%",percentage); //$NON-NLS-1$ //$NON-NLS-2$
        	if(percentage==100 && s.equals(Messages.StatisticsView_7)){
        		skip=true;
        	}
        }
		
        if(!skip){
        	
        	if(!selected.isEmpty()){
        		title+="\n[";
        		for(BigInteger i:selected){
        			title+=operatorsDictionary.get(i.intValue())+", ";
        		}
        		title=title.substring(0, title.length()-2);
        		title+="]";
        	}
        	
			//Create the statistic
			JFreeChart jfcStatePie = ChartFactory.createPieChart3D(
				title,  // chart title
	            dpdState,             // data
	            true,               // include legend
	            true,
	            false
	        );
			PiePlot ppState = (PiePlot) jfcStatePie.getPlot();
	        ppState.setDirection(Rotation.CLOCKWISE);
	        ppState.setNoDataMessage(Messages.StatisticsView_18);
	
	        JFreeChart jfcState = new JFreeChart(title, null, ppState, true); 
	        return jfcState;
        }
        return null;
		
	}

}
