package es.uca.webservices.gamera.eclipse.mutationstatistics.views;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "es.uca.webservices.gamera.eclipse.mutationstatistics.views.messages"; //$NON-NLS-1$
	public static String StatisticsView_1;
	public static String StatisticsView_14;
	public static String StatisticsView_15;
	public static String StatisticsView_16;
	public static String StatisticsView_17;
	public static String StatisticsView_18;
	public static String StatisticsView_2;
	public static String StatisticsView_3;
	public static String StatisticsView_4;
	public static String StatisticsView_5;
	public static String StatisticsView_6;
	public static String StatisticsView_7;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
