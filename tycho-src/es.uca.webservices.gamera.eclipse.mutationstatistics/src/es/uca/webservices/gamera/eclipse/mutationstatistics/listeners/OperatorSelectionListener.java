package es.uca.webservices.gamera.eclipse.mutationstatistics.listeners;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;

import es.uca.webservices.gamera.api.ComparisonResults;

/**
 * This listener updates the statistics when the selected operator changes.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class OperatorSelectionListener extends SelectionAdapter{
	private Combo cbOperator;
	private Combo cbGeneration;
	private List<ComparisonResults[]> results;
	private Map<Integer, String> operatorsDictionary;
	private ChartPanel chart;
	private DecimalFormat formatter = new DecimalFormat ("###.##"); //$NON-NLS-1$

	public OperatorSelectionListener(Combo cbOperator, Combo cbGeneration,
			List<ComparisonResults[]> results,
			Map<Integer, String> operatorsDictionary, ChartPanel chart){
		this.cbOperator=cbOperator;
		this.cbGeneration=cbGeneration;
		this.results=results;
		this.operatorsDictionary=operatorsDictionary;
		this.chart=chart;
	}
	
	@Override
	/**
	 * State selection event handler
	 */
	public void widgetSelected(SelectionEvent e) {
		final DefaultPieDataset dpdOperator = new DefaultPieDataset();
		final String operator = cbOperator.getText();
		
		int alive=0;
		int dead=0;
		int invalid=0;
		int generation=Integer.valueOf(cbGeneration.getText());
        for(ComparisonResults crAux : results.get(generation)){
        	boolean check=false;
        	for(int op=0;op<crAux.getIndividual().getOrder() && !check;op++){
				if(operatorsDictionary.get(crAux.getIndividual().getOperator().get(op).intValue()).equals(operator)) {
					check=true;
	                if(crAux.isAlive()){
	                	alive++;
	                }
	                if(crAux.isDead()){
	                	dead++;
	                }
					if(crAux.isInvalid()){
						invalid++;
					}
				}
        	}
        }
        	int total=alive+dead+invalid;
        	if(total!=0){
	        	double percentage=alive*100/total;
	        	dpdOperator.setValue(Messages.OperatorSelectionListener_1+ formatter.format(percentage)+"%",percentage); //$NON-NLS-2$
	        	percentage=dead*100/total;
	        	dpdOperator.setValue(Messages.OperatorSelectionListener_2+ formatter.format(percentage)+"%",percentage); //$NON-NLS-2$
	        	percentage=invalid*100/total;
	        	dpdOperator.setValue(Messages.OperatorSelectionListener_3+ formatter.format(percentage)+"%",percentage); //$NON-NLS-2$
        	}
        	
		//Create the statistic
		JFreeChart jfcStatePie = ChartFactory.createPieChart3D(
			Messages.OperatorSelectionListener_4,  // chart title
            dpdOperator,             // data
            true,               // include legend
            true,
            false
        );
		PiePlot ppState = (PiePlot) jfcStatePie.getPlot();
        ppState.setDirection(Rotation.CLOCKWISE);
        ppState.setNoDataMessage(Messages.OperatorSelectionListener_5);

        JFreeChart jfcState = new JFreeChart(Messages.OperatorSelectionListener_6, null, ppState, true); 
          
        chart.setChart(jfcState);
        chart.repaint();
        
	}
}
