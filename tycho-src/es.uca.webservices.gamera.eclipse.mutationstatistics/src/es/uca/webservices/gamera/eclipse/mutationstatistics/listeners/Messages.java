package es.uca.webservices.gamera.eclipse.mutationstatistics.listeners;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "es.uca.webservices.gamera.eclipse.mutationstatistics.listeners.messages"; //$NON-NLS-1$
	public static String OperatorSelectionListener_1;
	public static String OperatorSelectionListener_2;
	public static String OperatorSelectionListener_3;
	public static String OperatorSelectionListener_4;
	public static String OperatorSelectionListener_5;
	public static String OperatorSelectionListener_6;
	public static String StateSelectionListener_3;
	public static String StateSelectionListener_4;
	public static String GenerationListener_1;
	public static String GenerationListener_2;
	public static String GenerationListener_3;
	public static String GenerationListener_4;
	public static String GenerationListener_5;
	public static String GenerationListener_6;
	public static String GenerationListener_7;
	public static String GenerationListener_8;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
