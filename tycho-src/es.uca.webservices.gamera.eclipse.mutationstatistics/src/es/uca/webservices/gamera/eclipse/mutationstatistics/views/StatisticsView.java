package es.uca.webservices.gamera.eclipse.mutationstatistics.views;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import org.jfree.data.general.DefaultPieDataset;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.util.Pair;
import es.uca.webservices.gamera.eclipse.mutationstatistics.listeners.DrillDownListener;
import es.uca.webservices.gamera.eclipse.mutationstatistics.listeners.GenerationSelectionListener;
import es.uca.webservices.gamera.eclipse.mutationstatistics.listeners.OperatorSelectionListener;
import es.uca.webservices.gamera.eclipse.mutationstatistics.listeners.StateSelectionListener;

/**
 * This class creates the view which shows the statistics.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class StatisticsView extends ViewPart implements  ISelectionListener{
	
	private ChartPanel panelGlobalComposite;
	private ChartPanel panelOperatorComposite;
	private ChartPanel panelStateComposite;
	private ChartPanel panelMutantsByOperator;
	private Combo cbGeneration;
	private Combo cbOperator;
	private Combo cbState;
	private ChartMouseListener chartListener=null;

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "es.uca.webservices.gamera.eclipse.mutationstatistics.views.StatisticsView"; //$NON-NLS-1$
	 
	/**
	 * The constructor.
	 */
	public StatisticsView() {
	}

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(final Composite parent) {
		parent.setLayout(new GridLayout(2, false));
		
		Label lblGeneration=new Label(parent, SWT.NONE);
		lblGeneration.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		lblGeneration.setText(Messages.StatisticsView_6);
		
		cbGeneration=new Combo(parent,  SWT.NONE);
		cbGeneration.setLayoutData(new GridData(SWT.LEFT, SWT.NONE, true, false,1,1));
		cbGeneration.setEnabled(false);


		//Empty statistic to show when the dialog is created
		DefaultPieDataset dpdClean = new DefaultPieDataset();  
		
		JFreeChart cleanChart=ChartFactory.createPieChart3D("",  //$NON-NLS-1$
				dpdClean, true, true, false);
		
		//Create the global statistic
		Composite chartGlobalComposite=new Composite(parent, SWT.NONE| SWT.EMBEDDED);
		chartGlobalComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		panelGlobalComposite=Chart.createChartComposite(
				chartGlobalComposite, Chart.createChart( "", cleanChart)); //$NON-NLS-1$ //$NON-NLS-2$
		
		
		//Create the mutants for operator statistics
		Composite chartMutantsByOperator=new Composite(parent, SWT.NONE| SWT.EMBEDDED);
		chartMutantsByOperator.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		panelOperatorComposite = Chart.createChartComposite(
				chartMutantsByOperator, Chart.createChart("", cleanChart)); //$NON-NLS-1$ //$NON-NLS-2$
        
        //Create the state statistic
		Composite chartMutantsByState=new Composite(parent, SWT.NONE| SWT.EMBEDDED);
		chartMutantsByState.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		panelStateComposite = Chart.createChartComposite(
				chartMutantsByState, Chart.createChart("", cleanChart)); //$NON-NLS-1$ //$NON-NLS-2$
        
        //Create the operator statistic
		Composite chartOperatorComposite=new Composite(parent, SWT.NONE| SWT.EMBEDDED);
		chartOperatorComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		panelMutantsByOperator = Chart.createChartComposite(
				chartOperatorComposite, Chart.createChart("", cleanChart)); //$NON-NLS-1$ //$NON-NLS-2$
		

		
		
        Composite stateComposite =new Composite(parent, SWT.NONE);
        stateComposite.setLayout(new GridLayout(2,true));
        
        //Combo to select the state
        cbState=new Combo(stateComposite, SWT.NONE);
        cbState.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
        cbState.setText(Messages.StatisticsView_1);
        cbState.setEnabled(false);
        cbState.add(Messages.StatisticsView_2);
        cbState.add(Messages.StatisticsView_3);
        cbState.add(Messages.StatisticsView_4);
        
 
        
        Composite operatorComposite =new Composite(parent, SWT.NONE);
        operatorComposite.setLayout(new GridLayout(2,true));
         
        //Combo to select the operator
        cbOperator=new Combo(operatorComposite, SWT.NONE);
        cbOperator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
        cbOperator.setText(Messages.StatisticsView_5);
        cbOperator.setEnabled(false);

		/*
		 * Update the statistics with the results sended by OSGi
		 */
        BundleContext ctx = FrameworkUtil.getBundle(StatisticsView.class).getBundleContext();
	    EventHandler handler = new EventHandler() {
	      public void handleEvent(final org.osgi.service.event.Event event) {
	    	  if(!parent.isDisposed()){
		    	  parent.getDisplay().syncExec(new Runnable() {
		    		  public void run() {
		    			  List<BigInteger> selected=new ArrayList<BigInteger>();
		    			 
		   
		    			  @SuppressWarnings("unchecked")
		    			  Pair <List<ComparisonResults[]>, Map<Integer, String>> pResults=(Pair <List<ComparisonResults[]>, 
		    					  Map<Integer, String>>) event.getProperty("MUTATIONRESULTS"); //$NON-NLS-1$
		    			  List<ComparisonResults[]> results=pResults.getLeft();
		    			  Map<Integer, String> operatorsDictionary=pResults.getRight();
		    			  Map<String, List<Integer>> mutantsByOperator = new HashMap<String, List<Integer>>();
	
		    			  for(int i:operatorsDictionary.keySet()){
		    				  List<Integer> mutantsByGeneration=new ArrayList<Integer>();
		    				  for(ComparisonResults[] cResults:results){
		    					  int cont = 0;
		    					  for(ComparisonResults c:cResults){
		    						  boolean check=false;
		    						  for(int op=0;op<c.getIndividual().getOrder() && !check;op++){
				    					  if(c.getIndividual().getOperator().get(op).intValue()==i){
				    						  cont++;
				    						  check=true;
				    					  }
		    						  }
			    				  }
		    					  mutantsByGeneration.add(cont);
		    				  }
		    				  
		    				  mutantsByOperator.put(operatorsDictionary.get(i), mutantsByGeneration);
		    			  }
		    			
		    			  if(results.size()>0){
		    				  cbGeneration.removeAll();
		    				  if(cbGeneration.getListeners(SWT.Selection).length>0){
		    					  cbGeneration.removeListener(SWT.Selection, cbGeneration.getListeners(SWT.Selection)[0]);
		    				  }
		    				  for(int i=0;i<results.size();i++){
			    				  cbGeneration.add(Integer.toString(i));
			    			  }
		    				  
		    				  cbGeneration.update();
		    				  
			    			  cbGeneration.addSelectionListener(new GenerationSelectionListener(cbGeneration, 
			    					  cbOperator, cbState, results, mutantsByOperator, 
			    					  panelGlobalComposite, panelOperatorComposite, selected));
			    			  cbGeneration.select(results.size()-1);
	
		    				  //Enable the combos and export buttons
		    				  
		    				  cbOperator.removeAll();
		    				  cbOperator.setEnabled(true);
		    				  cbState.setEnabled(true);
		    				  
		    				  cbOperator.setItems(mutantsByOperator.keySet().toArray(new String[mutantsByOperator.size()]));
		    				  
		    				  if(cbState.getListeners(SWT.Selection).length>0){
		    					  cbState.removeListener(SWT.Selection, cbState.getListeners(SWT.Selection)[0]);
		    				  }
		    				  
		    				  cbState.addSelectionListener(new StateSelectionListener(cbState, 
		    						  cbGeneration, results, operatorsDictionary, 
		    						  panelStateComposite, selected));
		    				  cbState.select(0);
		    				  cbState.notifyListeners(SWT.Selection, new Event());
		    				  
		    				  if(chartListener!=null){
		    					  panelStateComposite.removeChartMouseListener(chartListener);
		    				  }
		    				  
		    				  chartListener=new DrillDownListener(cbGeneration, cbState, 
		    						  selected, results, operatorsDictionary, panelStateComposite);
		    				  panelStateComposite.addChartMouseListener(chartListener);
		    				  
		    				  
		    				  if(cbOperator.getListeners(SWT.Selection).length>0){
		    					  cbOperator.removeListener(SWT.Selection, cbOperator.getListeners(SWT.Selection)[0]);
		    				  }
		    				  cbOperator.addSelectionListener(new OperatorSelectionListener(cbOperator, 
		    						  cbGeneration, results, operatorsDictionary, 
		    						  panelMutantsByOperator));
		    				  cbOperator.select(0);
		    				  cbOperator.notifyListeners(SWT.Selection, new Event());
	
		    				 
			    			  cbGeneration.notifyListeners(SWT.Selection, new Event());
		    				  cbGeneration.setEnabled(true);
		    			  }
		    		  }
		    	  });
	    	  }
	      }
	    };

	    Dictionary<String,String> properties = new Hashtable<String, String>();
	    properties.put(EventConstants.EVENT_TOPIC, "mutationcommunication/*"); //$NON-NLS-1$
	    ctx.registerService(EventHandler.class, handler, properties);
	    
	    ExportAction exportGlobal = new ExportAction(panelGlobalComposite);  
    	exportGlobal.setText(Messages.StatisticsView_14);  
    	getViewSite().getActionBars().getMenuManager().add(exportGlobal);
    	
    	ExportAction exportMutantByOperator = new ExportAction(panelMutantsByOperator);  
    	exportMutantByOperator.setText(Messages.StatisticsView_15);  
    	getViewSite().getActionBars().getMenuManager().add(exportMutantByOperator);
    	
    	ExportAction exportOperatorsByState = new ExportAction(panelStateComposite);  
    	exportOperatorsByState.setText(Messages.StatisticsView_16);  
    	getViewSite().getActionBars().getMenuManager().add(exportOperatorsByState);
    	
    	ExportAction exportStateByOperator = new ExportAction(panelMutantsByOperator);  
    	exportStateByOperator.setText(Messages.StatisticsView_17);  
    	getViewSite().getActionBars().getMenuManager().add(exportStateByOperator);
    	
    	getSite().getPage().addSelectionListener("es.uca.webservices.gamera.eclipse.mutationstatistics",(ISelectionListener)this);

	}


	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		//viewer.getControl().setFocus();
		
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		// TODO Auto-generated method stub
		
	}
}