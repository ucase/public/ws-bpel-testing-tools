package es.uca.webservices.gamera.eclipse.mutationstatistics.listeners;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.eclipse.mutationstatistics.views.Chart;

/**
 * This listeners controls the drill down when a sector is selected.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class DrillDownListener implements ChartMouseListener{
	
	private Combo cbGeneration, cbState;
	private List<BigInteger> selected;
	private List<ComparisonResults[]> results;
	private Map<Integer, String> dictionary;
	private ChartPanel chart;
	
	public DrillDownListener(Combo cbGeneration, Combo cbState, List<BigInteger> selected, 
			List<ComparisonResults[]>results, Map<Integer, String> dictionary, ChartPanel chart){
		this.cbGeneration=cbGeneration;
		this.cbState=cbState;
		this.selected=selected;
		this.results=results;
		this.dictionary=dictionary;
		this.chart=chart;
	}

	@Override
	public void chartMouseClicked(ChartMouseEvent arg0) {
		ChartEntity chartentity = arg0.getEntity();
        if (chartentity.getToolTipText()!=null){
        	if(!chartentity.getToolTipText().substring(0, Messages.StateSelectionListener_3.length())
        			.equals(Messages.StateSelectionListener_3)){
	        	for(Entry<Integer, String> entry:dictionary.entrySet()){
	        		if(chartentity.getToolTipText().substring(0, 3).equals(entry.getValue())){
	        			selected.add(new BigInteger(entry.getKey().toString()));
	        		}
	        	}
        	}
        }   
        else{
                if(!selected.isEmpty()){
                	selected.remove(selected.size()-1);
                }
        }
        Display.getDefault().syncExec(new Runnable() {
            public void run() {
            	JFreeChart jfcState = Chart.stateChart(Messages.StateSelectionListener_4, Integer.parseInt(cbGeneration.getText())
                		, cbState.getText(), 
                		selected, results, dictionary);
                  
            	if(jfcState!=null){
	                chart.setChart(jfcState);
	                chart.repaint();
            	}
            	else{
            		selected.remove(selected.size()-1);
            	}
            }
        });
        
		
	}

	@Override
	public void chartMouseMoved(ChartMouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}



}
