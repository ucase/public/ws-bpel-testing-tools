package es.uca.webservices.gamera.eclipse.mutationstatistics.listeners;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.eclipse.mutationstatistics.views.Chart;

/**
 * This listener updates the statistics when the selected state changes.
 * @author Juan Daniel Morcillo Regueiro
 *
 */
public class StateSelectionListener extends SelectionAdapter{
	private Combo cbState;
	private Combo cbGeneration;
	private List<ComparisonResults[]> results;
	private Map<Integer, String> operatorsDictionary;
	private ChartPanel chart;
	private List<BigInteger> selected;

	
	public StateSelectionListener(Combo cbState, Combo cbGeneration,
			List<ComparisonResults[]> results, Map<Integer, String> operatorsDictionary, ChartPanel chart,
			List<BigInteger> selected){
		this.cbState=cbState;
		this.cbGeneration=cbGeneration;
		this.results=results;
		this.operatorsDictionary=operatorsDictionary;
		this.chart=chart;
		this.selected=selected;
	}
	
	/**
	 * State selection event handler
	 */
	@Override
	public void widgetSelected(SelectionEvent e) {
		selected.clear();

        JFreeChart jfcState = Chart.stateChart(Messages.StateSelectionListener_4, Integer.parseInt(cbGeneration.getText())
        		, cbState.getText(), 
        		new ArrayList<BigInteger>(), results, operatorsDictionary);
          
        chart.setChart(jfcState);
        chart.repaint();
	}
}
