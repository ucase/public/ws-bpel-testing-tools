package es.uca.webservices.gamera.eclipse.tests;

import static org.junit.Assert.assertEquals;


import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swtbot.eclipse.finder.SWTWorkbenchBot;
import org.eclipse.swtbot.eclipse.finder.waits.Conditions;
import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotCombo;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotShell;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTable;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.eclipse.jface.dialogs.ErrorDialog;

public class TraditionalMutationTests {

	private static SWTWorkbenchBot	bot;
	 
	/**
	 * This method prepare the project for each test.
	 */
	@Before
	public void beforeClass() throws Exception {
		ErrorDialog.AUTOMATED_MODE = false;

		bot = new SWTWorkbenchBot();
		try{
			bot.viewByTitle("Welcome").close(); //$NON-NLS-1$
		}catch(WidgetNotFoundException e){}
		try{
			bot.menu("Window").menu("Open Perspective").menu("Java").click(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}catch(WidgetNotFoundException e){}
		
		
		bot.menu("Window").menu("Show View").menu("Other...").click(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		SWTBotShell shell = bot.shell("Show View"); //$NON-NLS-1$
		shell.activate();
		bot.tree().expandNode(Messages.TraditionalMutationTests_8).select(Messages.TraditionalMutationTests_106);
		bot.button("OK").click(); //$NON-NLS-1$
	}
 
 
	/**
	 * Test which tries configure correctly MuBPEL.
	 */
	@Test
	public void MuBPELCorrectConfiguration() throws Exception {
		
		List<String> paths=new ArrayList<String>();
		paths.add("resources/ApprovalService.wsdl"); //$NON-NLS-1$
		paths.add("resources/AssessorService.wsdl"); //$NON-NLS-1$
		paths.add("resources/data.spec"); //$NON-NLS-1$
		paths.add("resources/data.vm"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess-velocity.bpts"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess.bpel"); //$NON-NLS-1$
		paths.add("resources/LoanService.wsdl"); //$NON-NLS-1$
		paths.add("resources/loanServicePT.wsdl"); //$NON-NLS-1$
		paths.add("resources/out.xml"); //$NON-NLS-1$
		
		configureMuBPELLaunchConfiguration("MuBPELProject", paths); //$NON-NLS-1$
		
		try{
			if(bot.shell(Messages.TraditionalMutationTests_102).isActive()){
				Assert.fail();
			}
			while(bot.cTabItem(Messages.TraditionalMutationTests_103).isActive());
		}catch(WidgetNotFoundException e){}
		
		List<String>operators=new ArrayList<String>();
		operators.add("AIE"); //$NON-NLS-1$
		setOperators(operators);
		bot.button("Run").click(); //$NON-NLS-1$
		SWTBot results=bot.viewByTitle(Messages.TraditionalMutationTests_106).bot();
		while(results.table().rowCount()==0){
			try{
				if(bot.shell(Messages.TraditionalMutationTests_102).isActive()){
					Assert.fail();
				}
				while(bot.cTabItem(Messages.TraditionalMutationTests_103).isActive());
			}catch(WidgetNotFoundException e){}
			
		}

		assertEquals(2, results.table().rowCount());
		assertEquals(5, results.table().columnCount());

	}
	
	/**
	 * Test which configure incorrectly MuBPEL and checks if an error message is generated.
	 */
	@Test
	public void MuBPELErroneusMutation() throws Exception {
		List<String> paths=new ArrayList<String>();
		paths.add("resources/ApprovalServiceFail.wsdl"); //$NON-NLS-1$
		paths.add("resources/AssessorService.wsdl"); //$NON-NLS-1$
		paths.add("resources/data.spec"); //$NON-NLS-1$
		paths.add("resources/data.vm"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess-velocity.bpts"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess.bpel"); //$NON-NLS-1$
		paths.add("resources/LoanService.wsdl"); //$NON-NLS-1$
		paths.add("resources/loanServicePT.wsdl"); //$NON-NLS-1$
		paths.add("resources/out.xml"); //$NON-NLS-1$
		
		configureMuBPELLaunchConfiguration("MuBPELProject", paths); //$NON-NLS-1$
		
		try{
			if(bot.shell(Messages.TraditionalMutationTests_102).isActive()){
				Assert.fail();
			}
			while(bot.cTabItem(Messages.TraditionalMutationTests_103).isActive());
		}catch(WidgetNotFoundException e){}
		
		List<String>operators=new ArrayList<String>();
		operators.add("AIE"); //$NON-NLS-1$
		setOperators(operators);
		bot.button("Run").click(); //$NON-NLS-1$
		SWTBot results=bot.viewByTitle(Messages.TraditionalMutationTests_106).bot();
		
		
		
		while(!bot.activeShell().getText().equals(Messages.TraditionalMutationTests_102)){
			assertEquals(0, results.table().rowCount());
		}	
	}
	
	
	/**
	 * Test which configure incorrectly MuBPEL and checks if an error message is generated.
	 */
	@Test
	public void MuBPELErroneusAnalysis() throws Exception {
		List<String> paths=new ArrayList<String>();
		paths.add("resources/ApprovalService.wsdl"); //$NON-NLS-1$
		paths.add("resources/AssessorService.wsdl"); //$NON-NLS-1$
		paths.add("resources/data.spec"); //$NON-NLS-1$
		paths.add("resources/data.vm"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess-velocity.bpts"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcessFail.bpel"); //$NON-NLS-1$
		paths.add("resources/LoanService.wsdl"); //$NON-NLS-1$
		paths.add("resources/loanServicePT.wsdl"); //$NON-NLS-1$
		paths.add("resources/out.xml"); //$NON-NLS-1$
		
		configureMuBPELLaunchConfiguration("MuBPELProject", paths); //$NON-NLS-1$

		assertEquals(true, bot.shell(Messages.TraditionalMutationTests_102).isActive());

	}
	
	/**
	 * Test which checks if the comparison between mutants and original code is correct.
	 */
	@Test
	public void MuBPELComparison() throws Exception {
		List<String> paths=new ArrayList<String>();
		paths.add("resources/ApprovalService.wsdl"); //$NON-NLS-1$
		paths.add("resources/AssessorService.wsdl"); //$NON-NLS-1$
		paths.add("resources/data.spec"); //$NON-NLS-1$
		paths.add("resources/data.vm"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess-velocity.bpts"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess.bpel"); //$NON-NLS-1$
		paths.add("resources/LoanService.wsdl"); //$NON-NLS-1$
		paths.add("resources/loanServicePT.wsdl"); //$NON-NLS-1$
		paths.add("resources/out.xml"); //$NON-NLS-1$
		
		configureMuBPELLaunchConfiguration("MuBPELProject", paths); //$NON-NLS-1$
		
		try{
			if(bot.shell(Messages.TraditionalMutationTests_102).isActive()){
				Assert.fail();
			}
			while(bot.cTabItem(Messages.TraditionalMutationTests_103).isActive());
		}catch(WidgetNotFoundException e){}
		
		SWTBotShell shell = bot.shell("Run Configurations"); //$NON-NLS-1$
		shell.activate();
		String[] items=bot.list(0).getItems();
		bot.button("Close").click(); //$NON-NLS-1$
		bot.button("No").click(); //$NON-NLS-1$
		SWTBot results=bot.viewByTitle(Messages.TraditionalMutationTests_70).bot();
		
		for(String item: items){
			boolean encontrado=false;
			for(SWTBotTreeItem treeItem:results.tree().getAllItems()){
				if(treeItem.getText().equals(item)){
					encontrado=true;
				}
			}
			assertEquals(true, encontrado);	
		}
	}

	/**
	 * Test which tries to run two consecutive times MuBPEL.
	 */
	@Test
	public void MuBPELConsecutiveMutation() throws Exception {
		
		
		List<String> paths=new ArrayList<String>();
		paths.add("resources/ApprovalService.wsdl"); //$NON-NLS-1$
		paths.add("resources/AssessorService.wsdl"); //$NON-NLS-1$
		paths.add("resources/data.spec"); //$NON-NLS-1$
		paths.add("resources/data.vm"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess-velocity.bpts"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess.bpel"); //$NON-NLS-1$
		paths.add("resources/LoanService.wsdl"); //$NON-NLS-1$
		paths.add("resources/loanServicePT.wsdl"); //$NON-NLS-1$
		paths.add("resources/out.xml"); //$NON-NLS-1$
		
		configureMuBPELLaunchConfiguration("MuBPELProject", paths); //$NON-NLS-1$
		
		try{
			if(bot.shell(Messages.TraditionalMutationTests_102).isActive()){
				Assert.fail();
			}
			while(bot.cTabItem(Messages.TraditionalMutationTests_103).isActive());
		}catch(WidgetNotFoundException e){}
		
		List<String>operators=new ArrayList<String>();
		operators.add("AIE"); //$NON-NLS-1$
		setOperators(operators);
		bot.button("Run").click(); //$NON-NLS-1$
		SWTBot results=bot.viewByTitle(Messages.TraditionalMutationTests_106).bot();
		while(results.table().rowCount()==0){
			try{
				if(bot.shell(Messages.TraditionalMutationTests_102).isActive()){
					Assert.fail();
				}
				while(bot.cTabItem(Messages.TraditionalMutationTests_103).isActive());
			}catch(WidgetNotFoundException e){}
			
		}

		assertEquals(2, results.table().rowCount());
		assertEquals(5, results.table().columnCount());
		
		
		for(SWTBotTreeItem item:bot.tree().getAllItems()){
			item.select();
			bot.menu("Edit").menu("Delete").click(); //$NON-NLS-1$ //$NON-NLS-2$
			bot.checkBox().click();
			bot.button("OK").click(); //$NON-NLS-1$
		}
		
		bot.waitWhile(Conditions.shellIsActive("Delete Resources")); //$NON-NLS-1$
		
		paths=new ArrayList<String>();
		paths.add("resources/ApprovalService.wsdl"); //$NON-NLS-1$
		paths.add("resources/AssessorService.wsdl"); //$NON-NLS-1$
		paths.add("resources/data.spec"); //$NON-NLS-1$
		paths.add("resources/data.vm"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess.bpts"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess.bpel"); //$NON-NLS-1$
		paths.add("resources/LoanService.wsdl"); //$NON-NLS-1$
		paths.add("resources/loanServicePT.wsdl"); //$NON-NLS-1$
		paths.add("resources/out.xml"); //$NON-NLS-1$
		
		configureMuBPELLaunchConfiguration("MuBPELProject", paths); //$NON-NLS-1$
		
		try{
			if(bot.shell(Messages.TraditionalMutationTests_102).isActive()){
				Assert.fail();
			}
			while(bot.cTabItem(Messages.TraditionalMutationTests_103).isActive());
		}catch(WidgetNotFoundException e){}
		
		operators=new ArrayList<String>();
		operators.add("AIE"); //$NON-NLS-1$
		setOperators(operators);
		bot.button("Run").click(); //$NON-NLS-1$
		results=bot.viewByTitle(Messages.TraditionalMutationTests_106).bot();
		while(results.table().columnCount()<7){
			try{
				if(bot.shell(Messages.TraditionalMutationTests_102).isActive()){
					Assert.fail();
				}
				while(bot.cTabItem(Messages.TraditionalMutationTests_103).isActive());
			}catch(WidgetNotFoundException e){}
			
		}

		assertEquals(2, results.table().rowCount());
		assertEquals(7, results.table().columnCount());
	
	}
	
	/**
	 * This method configures MuBPEL
	 */
	void configureMuBPELLaunchConfiguration(String projectName, List<String> paths){
		String shellName=bot.activeShell().getText();
		bot.menu("File").menu("New").menu("Project...").click(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
 
		SWTBotShell shell = bot.shell("New Project"); //$NON-NLS-1$
		shell.activate();
		bot.tree().expandNode("General").select("Project"); //$NON-NLS-1$ //$NON-NLS-2$
		bot.button("Next >").click(); //$NON-NLS-1$
 
		bot.textWithLabel("Project name:").setText(projectName); //$NON-NLS-1$
 
		bot.button("Finish").click(); //$NON-NLS-1$

		bot.shell(shellName).activate();

		for(String path:paths){
			FileUtils.createFile(bot, projectName, path);
		}

		bot.menu("Run").menu("Run Configurations...").click(); //$NON-NLS-1$ //$NON-NLS-2$
		
		shell = bot.shell("Run Configurations"); //$NON-NLS-1$
		shell.activate();
		bot.tree().getTreeItem(Messages.TraditionalMutationTests_121).doubleClick();
		
		configureGAExecutor(projectName);

		bot.button(Messages.TraditionalMutationTests_122).click();
	
	}

	/**
	 * This methods configures the GAExecutor.
	 */
	public void configureGAExecutor(String projectName){
		
		SWTBotCombo selectImpl=bot.comboBox();
		selectImpl.setSelection(0);
		final SWTBotTable table=bot.table();
		for(int i=0;i<table.rowCount();i++){
			assertEquals(false, bot.button(Messages.TraditionalMutationTests_122).isEnabled());
			assertEquals(false, bot.button("Run").isEnabled()); //$NON-NLS-1$
			table.select(i);
			UIThreadRunnable.asyncExec(bot.getDisplay(), new VoidResult() {
				public void run() {
				table.widget.notifyListeners(SWT.MouseDoubleClick,
						new Event());
				}
			});
			
			bot.tree().expandNode(projectName);
			assertEquals(true, bot.tree().getAllItems().length>0);
			bot.tree().getAllItems()[0].getItems()[0].select();
			bot.button("OK").click(); //$NON-NLS-1$
			
		}
		assertEquals(true, bot.button(Messages.TraditionalMutationTests_122).isEnabled());
		assertEquals(false, bot.button("Run").isEnabled());   //$NON-NLS-1$
	}
	
	
	public void setOperators(List<String> operators){
		
		SWTBotShell shell = bot.shell("Run Configurations"); //$NON-NLS-1$
		shell.activate();
		assertEquals(false, bot.button("Run").isEnabled()); //$NON-NLS-1$
		for(String operator:operators){
			bot.list(0).select(operator);
			bot.button(1).click();
			
		}
		assertEquals(true, bot.button("Run").isEnabled()); //$NON-NLS-1$
		
	}
	
 
	/**
	 * This method cleans the project for the next test.
	 */
	@After
	public void clean() {
		try{
			SWTBotShell shell = bot.shell(Messages.TraditionalMutationTests_102);
			System.out.println(shell.getText());
			shell.activate();
			bot.button("OK").click(); //$NON-NLS-1$
			
		}catch(WidgetNotFoundException e){}
		
		try{
			bot.button("Close").click(); //$NON-NLS-1$
			bot.button("No").click(); //$NON-NLS-1$
			
		}catch(WidgetNotFoundException e){}
		
		for(SWTBotTreeItem item:bot.tree().getAllItems()){
			item.select();
			bot.menu("Edit").menu("Delete").click(); //$NON-NLS-1$ //$NON-NLS-2$
			bot.checkBox().click();
			bot.button("OK").click(); //$NON-NLS-1$
		}
		bot.viewByTitle(Messages.TraditionalMutationTests_106).close();


		bot.sleep(2000);
	}
}
