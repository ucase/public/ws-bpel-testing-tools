package es.uca.webservices.gamera.eclipse.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swtbot.eclipse.finder.waits.Conditions;
import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotShell;


public class FileUtils {
	/**
	 * This method copies the code of a file into a new project in SWTBot.
	 */
	static public void createFile(SWTBot bot, String projectName, String path){
		File file=new File(path);
		bot.menu("File").menu("New").menu("File").click();
		SWTBotShell shell=bot.shell("New File");
		shell.activate();
		bot.text(0).setText(projectName);
		bot.text(1).setText(file.getName());
		
		bot.button("Finish").click();
		
		try{
			bot.button("No").click();
		}catch(WidgetNotFoundException e){
			
		}

		bot.waitUntil(Conditions.shellCloses(shell));
		
		
		IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = workspaceRoot.getProject(projectName);
		// open if necessary
		if (project.exists() && !project.isOpen()){
			try {
				project.open(null);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		IFile eclipseFile=project.getFile(file.getName());
		try {
			eclipseFile.setContents(new FileInputStream(path), true, true, null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

}
