package es.uca.webservices.gamera.eclipse.tests;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swtbot.eclipse.finder.SWTWorkbenchBot;
import org.eclipse.swtbot.eclipse.finder.waits.Conditions;
import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotShell;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EvolutiveMutationTest {
	
	private static SWTWorkbenchBot	bot;
	 
	/**
	 * This method prepare the project for each test.
	 */
	@Before
	public void beforeClass() throws Exception {
		ErrorDialog.AUTOMATED_MODE = false;

		bot = new SWTWorkbenchBot();
		try{
			bot.viewByTitle("Welcome").close(); //$NON-NLS-1$
		}catch(WidgetNotFoundException e){}
		try{
			bot.menu("Window").menu("Open Perspective").menu("Java").click(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}catch(WidgetNotFoundException e){}
		
		
		bot.menu("Window").menu("Show View").menu("Other...").click(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		SWTBotShell shell = bot.shell("Show View"); //$NON-NLS-1$
		shell.activate();
		bot.tree().expandNode(Messages.EvolutiveMutationTest_8).select(Messages.EvolutiveMutationTest_181);
		bot.button("OK").click(); //$NON-NLS-1$
	}
	
	/**
	 * Test which tries configure correctly the genetic algorithm.
	 */
	@Test
	public void GAmeraHOMCorrectConfiguration() throws Exception {
		List<String> paths=new ArrayList<String>();
		paths.add("resources/ApprovalService.wsdl"); //$NON-NLS-1$
		paths.add("resources/AssessorService.wsdl"); //$NON-NLS-1$
		paths.add("resources/data.spec"); //$NON-NLS-1$
		paths.add("resources/data.vm"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess-velocity.bpts"); //$NON-NLS-1$
		paths.add("resources/loanApprovalProcess.bpel"); //$NON-NLS-1$
		paths.add("resources/LoanService.wsdl"); //$NON-NLS-1$
		paths.add("resources/loanServicePT.wsdl"); //$NON-NLS-1$
		paths.add("resources/out.xml"); //$NON-NLS-1$
		createYAML("GAmeraHOM", "test"); //$NON-NLS-1$ //$NON-NLS-2$
		for(String path:paths){
			FileUtils.createFile(bot, "GAmeraHOM", path); //$NON-NLS-1$
		}
		bot.tree(0).expandNode("GAmeraHOM"); //$NON-NLS-1$
		
		bot.tree(0).getTreeItem("GAmeraHOM").getNode("test.yaml").doubleClick(); //$NON-NLS-1$ //$NON-NLS-2$
		

		final SWTBotTreeItem configuration=bot.tree(1).expandNode(
				"es.uca.webservices.gamera.conf.Configuration"); //$NON-NLS-1$
		
		//Executor
		SWTBotTreeItem executor=configuration.getNode("Executor").select(); //$NON-NLS-1$
		
		executor.contextMenu(Messages.EvolutiveMutationTest_28).click();
		bot.button("OK").click(); //$NON-NLS-1$
		executor.expand();
		SWTBotTreeItem bpelExecutor=executor.getNode(0);
		bpelExecutor.expand();
		bpelExecutor.getNode(0).contextMenu(Messages.EvolutiveMutationTest_28).click();
		bot.tree().expandNode("GAmeraHOM"); //$NON-NLS-1$
		assertEquals(true, bot.tree().getAllItems().length>0);
		bot.tree().getAllItems()[0].getItems()[0].select();

		bot.button("OK").click(); //$NON-NLS-1$
		bpelExecutor.getNode(1).contextMenu(Messages.EvolutiveMutationTest_28).click();
		bot.tree().expandNode("GAmeraHOM"); //$NON-NLS-1$
		assertEquals(true, bot.tree().getAllItems().length>0);
		bot.tree().getAllItems()[0].getItems()[0].select();
		bot.button("OK").click(); //$NON-NLS-1$
		bpelExecutor.getNode(3).contextMenu(Messages.EvolutiveMutationTest_28).click();
		bot.tree().expandNode("GAmeraHOM"); //$NON-NLS-1$
		assertEquals(true, bot.tree().getAllItems().length>0);
		bot.tree().getAllItems()[0].getItems()[0].select();
		bot.button("OK").click(); //$NON-NLS-1$
		
		//Genetic Operators
		SWTBotTreeItem geneticOperators=configuration.getNode("Genetic Operators").select(); //$NON-NLS-1$
		geneticOperators.contextMenu(Messages.EvolutiveMutationTest_28).click();
		bot.button("OK").click(); //$NON-NLS-1$
		bot.button("OK").click(); //$NON-NLS-1$
		geneticOperators.expand();
	
		geneticOperators.getNode(0).expand();
		geneticOperators.getNode(0).getNode(0).expand();
		geneticOperators.getNode(0).getNode(0).getNode("Random Range").contextMenu(Messages.EvolutiveMutationTest_28).click(); //$NON-NLS-1$

		bot.text(0).setText("2"); //$NON-NLS-1$
		bot.button("OK").click(); //$NON-NLS-1$
		geneticOperators.getNode(0).getNode(0).getNode("Scale Factor").contextMenu(Messages.EvolutiveMutationTest_28).click(); //$NON-NLS-1$
		bot.text(0).setText("0.5"); //$NON-NLS-1$
		bot.button("OK").click(); //$NON-NLS-1$
		geneticOperators.getNode(0).getNode(1).expand();
		geneticOperators.getNode(0).getNode(1).getNode(0).expand();
		geneticOperators.getNode(0).getNode(1).getNode(0).getNode("0.0").doubleClick(); //$NON-NLS-1$
		bot.text(0).setText("0.2"); //$NON-NLS-1$
		configuration.select();

		//Included Operators
		SWTBotTreeItem operators=configuration.getNode("Included Operators").select(); //$NON-NLS-1$
		operators.contextMenu(Messages.EvolutiveMutationTest_28).click();
		bot.button("Select All").click(); //$NON-NLS-1$
		bot.button("OK").click(); //$NON-NLS-1$

		//Individual Generators
		SWTBotTreeItem individualGenerator=configuration.getNode("Individual Generators").select(); //$NON-NLS-1$
		individualGenerator.contextMenu(Messages.EvolutiveMutationTest_28).click();
		bot.button("OK").click(); //$NON-NLS-1$
		bot.button("OK").click(); //$NON-NLS-1$
		
		//Population Size
		configuration.getNode("Population Size").contextMenu(Messages.EvolutiveMutationTest_28).click(); //$NON-NLS-1$
		bot.text(0).setText("3"); //$NON-NLS-1$
		bot.button("OK").click(); //$NON-NLS-1$
		configuration.select();
		
		//Selection Operators
		SWTBotTreeItem selectionOperators=configuration.getNode("Selection Operators").select(); //$NON-NLS-1$
		selectionOperators.contextMenu(Messages.EvolutiveMutationTest_28).click();
		bot.button("OK").click(); //$NON-NLS-1$
		bot.button("OK").click(); //$NON-NLS-1$
		selectionOperators.expand();
		selectionOperators.getNode(0).expand();
		selectionOperators.getNode(0).getNode(1).expand();
		selectionOperators.getNode(0).getNode(1).getNode(0).expand();
		selectionOperators.getNode(0).getNode(1).getNode(0).getNode("0.0").doubleClick(); //$NON-NLS-1$
		bot.text(0).setText("0.3"); //$NON-NLS-1$
		configuration.select();	
		
		//TerminationConditions
		SWTBotTreeItem terminationConditions=configuration.getNode("Termination Conditions").select(); //$NON-NLS-1$
		terminationConditions.contextMenu(Messages.EvolutiveMutationTest_28).click();
		bot.button("OK").click(); //$NON-NLS-1$
		terminationConditions.expand();
		terminationConditions.getNode(0).expand();
		terminationConditions.getNode(0).getNode(0).expand();
		terminationConditions.getNode(0).getNode(0).getNode(0).expand();
		terminationConditions.getNode(0).getNode(0).getNode(0).doubleClick();
		bot.text(0).setText("3"); //$NON-NLS-1$
		configuration.select();	

		bot.multipageEditorByTitle("test.yaml").activatePage(Messages.EvolutiveMutationTest_117); //$NON-NLS-1$
		
		bot.menu("File").menu("Save").click(); //$NON-NLS-1$ //$NON-NLS-2$
		
		bot.menu("Run").menu("Run Configurations...").click(); //$NON-NLS-1$ //$NON-NLS-2$
		
		SWTBotShell shell = bot.shell("Run Configurations"); //$NON-NLS-1$
		shell.activate();
		bot.tree().getTreeItem(Messages.EvolutiveMutationTest_82).doubleClick();
		bot.button(Messages.EvolutiveMutationTest_83).click();
		bot.tree().getTreeItem("GAmeraHOM").expand(); //$NON-NLS-1$
		bot.tree().getTreeItem("GAmeraHOM").getNode(0).select(); //$NON-NLS-1$
		bot.button("OK").click(); //$NON-NLS-1$
		bot.button("Run").click(); //$NON-NLS-1$
		SWTBot results=bot.viewByTitle(Messages.EvolutiveMutationTest_181).bot();
		while(results.table().rowCount()==0){
			try{
				if(bot.shell(Messages.EvolutiveMutationTest_89).isActive()){
					Assert.fail();
				}
				while(bot.cTabItem(Messages.EvolutiveMutationTest_90).isActive());
			}catch(WidgetNotFoundException e){}
			
		}
		
		bot.multipageEditorByTitle("test.yaml").close(); //$NON-NLS-1$
	}
	
	/**
	 * Test which configure incorrectly the genetic algorithm and checks if an error message is generated.
	 */
	@Test
	public void GAmeraHOMIncorrectConfiguration() throws Exception {
		createYAML("GAmeraHOM", "test"); //$NON-NLS-1$ //$NON-NLS-2$
		bot.tree(0).expandNode("GAmeraHOM"); //$NON-NLS-1$
		bot.tree(0).getTreeItem("GAmeraHOM").getNode("test.yaml").doubleClick(); //$NON-NLS-1$ //$NON-NLS-2$
		

		final SWTBotTreeItem configuration=bot.tree(1).expandNode(
				"es.uca.webservices.gamera.conf.Configuration"); //$NON-NLS-1$
		
		//Selection Operators
		SWTBotTreeItem selectionOperators=configuration.getNode("Selection Operators").select(); //$NON-NLS-1$
		selectionOperators.contextMenu(Messages.EvolutiveMutationTest_28).click();
		bot.button("OK").click(); //$NON-NLS-1$
		bot.button("OK").click(); //$NON-NLS-1$
		selectionOperators.expand();
		selectionOperators.getNode(0).expand();
		selectionOperators.getNode(0).getNode(1).expand();
		selectionOperators.getNode(0).getNode(1).getNode(0).expand();
		selectionOperators.getNode(0).getNode(1).getNode(0).getNode("0.0").doubleClick(); //$NON-NLS-1$
		bot.text(0).setText("Word"); //$NON-NLS-1$
		configuration.select();	
		//Diálogo de error
		assertEquals(Messages.EvolutiveMutationTest_104, bot.activeShell().getText());
		bot.button("OK").click(); //$NON-NLS-1$
		
		assertEquals(bot.multipageEditorByTitle("test.yaml").getActivePageTitle(), Messages.EvolutiveMutationTest_107); //$NON-NLS-1$
		
		bot.menu("File").menu("Save").click(); //$NON-NLS-1$ //$NON-NLS-2$
		bot.multipageEditorByTitle("test.yaml").close(); //$NON-NLS-1$
	}
	
	/**
	 * Test which pastes a YAML code and checks if the tree is generated.
	 */
	@Test
	public void GAmeraHOMCodeToTree() throws Exception {
		createYAML("GAmeraHOM", "test"); //$NON-NLS-1$ //$NON-NLS-2$
		bot.tree(0).expandNode("GAmeraHOM"); //$NON-NLS-1$
		bot.tree(0).getTreeItem("GAmeraHOM").getNode("test.yaml").doubleClick(); //$NON-NLS-1$ //$NON-NLS-2$
		bot.multipageEditorByTitle("test.yaml").activatePage(Messages.EvolutiveMutationTest_117); //$NON-NLS-1$
		bot.menu("Edit").menu("Select All").click(); //$NON-NLS-1$ //$NON-NLS-2$
		bot.menu("Edit").menu("Delete").click(); //$NON-NLS-1$ //$NON-NLS-2$
		
		
		try{
			String text=""; //$NON-NLS-1$
	        InputStream ips=new FileInputStream("resources/example.yaml");  //$NON-NLS-1$
	        InputStreamReader ipsr=new InputStreamReader(ips);
	        BufferedReader br=new BufferedReader(ipsr);
	        String line;
	        while ((line=br.readLine())!=null){
	        	text+=line+"\n"; //$NON-NLS-1$
	        }
	        br.close(); 
	        bot.editorByTitle("test.yaml").toTextEditor().setText(text); //$NON-NLS-1$
	        bot.multipageEditorByTitle("test.yaml").activatePage(Messages.EvolutiveMutationTest_107); //$NON-NLS-1$
	        assertEquals("es.uca.webservices.gamera.conf.Configuration",  //$NON-NLS-1$
	        		bot.tree(1).getTreeItem("es.uca.webservices.gamera.conf.Configuration") //$NON-NLS-1$
	        		.getText());

	    }       
	    catch (Exception e){} 
		bot.menu("File").menu("Save").click(); //$NON-NLS-1$ //$NON-NLS-2$
		bot.multipageEditorByTitle("test.yaml").close(); //$NON-NLS-1$

	}
	
	/**
	 * Test which pastes an invalid YAML code and checks if an error message is generated.
	 */
	@Test
	public void GAmeraHOMFailCodeToTree() throws Exception {
		createYAML("GAmeraHOM", "test"); //$NON-NLS-1$ //$NON-NLS-2$
		bot.tree(0).expandNode("GAmeraHOM"); //$NON-NLS-1$
		bot.tree(0).getTreeItem("GAmeraHOM").getNode("test.yaml").doubleClick(); //$NON-NLS-1$ //$NON-NLS-2$
		bot.multipageEditorByTitle("test.yaml").activatePage(Messages.EvolutiveMutationTest_117); //$NON-NLS-1$
		bot.menu("Edit").menu("Select All").click(); //$NON-NLS-1$ //$NON-NLS-2$
		bot.menu("Edit").menu("Delete").click(); //$NON-NLS-1$ //$NON-NLS-2$
		bot.editorByTitle("test.yaml").toTextEditor().setText("Fail"); //$NON-NLS-1$ //$NON-NLS-2$

		try{
			bot.multipageEditorByTitle("test.yaml").activatePage(Messages.EvolutiveMutationTest_107); //$NON-NLS-1$
		}catch(Exception e){}
	    assertEquals(Messages.EvolutiveMutationTest_104, bot.activeShell().getText());
	    assertEquals(true, 
	    		bot.multipageEditorByTitle("test.yaml").isActivePage(Messages.EvolutiveMutationTest_117)); //$NON-NLS-1$
	    bot.button("OK").click(); //$NON-NLS-1$
		bot.menu("File").menu("Save").click(); //$NON-NLS-1$ //$NON-NLS-2$
		bot.multipageEditorByTitle("test.yaml").close(); //$NON-NLS-1$

	}
	
	/**
	 * Test which checks if the tree is converted to YAML code. 
	 */
	void createYAML(String projectName, String fileName){
		String shellName=bot.activeShell().getText();
		System.out.println(shellName);
		bot.menu("File").menu("New").menu("Project...").click(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
 
		SWTBotShell shell = bot.shell("New Project"); //$NON-NLS-1$
		shell.activate();
		bot.tree().expandNode("General").select("Project"); //$NON-NLS-1$ //$NON-NLS-2$
		bot.button("Next >").click(); //$NON-NLS-1$
 
		bot.textWithLabel("Project name:").setText(projectName); //$NON-NLS-1$
 
		bot.button("Finish").click(); //$NON-NLS-1$

		bot.shell(shellName).activate();

		bot.menu("File").menu("New").menu("Other...").click(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		shell=bot.shell("New"); //$NON-NLS-1$
		shell.activate();
		bot.tree().expandNode("Other").select(Messages.EvolutiveMutationTest_169); //$NON-NLS-1$
		bot.button("Next >").click(); //$NON-NLS-1$
		 
		shell=bot.shell(Messages.EvolutiveMutationTest_171);
		shell.activate();
		bot.text(0).setText(projectName);
		bot.text(1).setText(fileName);
		
		bot.button("Finish").click(); //$NON-NLS-1$
		
		try{
			bot.button("No").click(); //$NON-NLS-1$
		}catch(WidgetNotFoundException e){
			
		}

		bot.waitUntil(Conditions.shellCloses(shell));
	
	}
	
	/**
	 * This method cleans the project for the next test.
	 */
	@After
	public void clean() {
		try{
			SWTBotShell shell = bot.shell("Error"); //$NON-NLS-1$
			System.out.println(shell.getText());
			shell.activate();
			bot.button("OK").click(); //$NON-NLS-1$
			
		}catch(WidgetNotFoundException e){}
		
		try{
			bot.button("Close").click(); //$NON-NLS-1$
			bot.button("No").click(); //$NON-NLS-1$
			
		}catch(WidgetNotFoundException e){}
		
		for(SWTBotTreeItem item:bot.tree().getAllItems()){
			item.select();
			bot.menu("Edit").menu("Delete").click(); //$NON-NLS-1$ //$NON-NLS-2$
			bot.checkBox().click();
			bot.button("OK").click(); //$NON-NLS-1$
		}
		bot.viewByTitle(Messages.EvolutiveMutationTest_181).close();


		bot.sleep(2000);
	}

}
