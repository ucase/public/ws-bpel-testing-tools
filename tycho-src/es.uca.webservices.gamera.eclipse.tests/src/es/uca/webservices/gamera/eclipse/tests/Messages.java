package es.uca.webservices.gamera.eclipse.tests;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "es.uca.webservices.gamera.eclipse.tests.messages"; //$NON-NLS-1$
	public static String EvolutiveMutationTest_104;
	public static String EvolutiveMutationTest_107;
	public static String EvolutiveMutationTest_117;
	public static String EvolutiveMutationTest_169;
	public static String EvolutiveMutationTest_171;
	public static String EvolutiveMutationTest_181;
	public static String EvolutiveMutationTest_28;
	public static String EvolutiveMutationTest_8;
	public static String EvolutiveMutationTest_82;
	public static String EvolutiveMutationTest_83;
	public static String EvolutiveMutationTest_89;
	public static String EvolutiveMutationTest_90;
	public static String TraditionalMutationTests_102;
	public static String TraditionalMutationTests_103;
	public static String TraditionalMutationTests_106;
	public static String TraditionalMutationTests_121;
	public static String TraditionalMutationTests_122;
	public static String TraditionalMutationTests_70;
	public static String TraditionalMutationTests_8;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
