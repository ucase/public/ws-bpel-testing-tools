package es.uca.webservices.bpel.repo.constraints

import es.uca.webservices.bpel.repo.model.Entry
import java.io.File
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

trait ConstraintSuite {
  def constraint : Constraint
  def casedir : String
  def check(basedir: String) = constraint.check(
    new Entry(
      new File("src/test/resources/" + casedir + "/" + basedir)))
}

@RunWith(classOf[JUnitRunner])
class SameImportsForAllBPELTest extends FunSuite with ConstraintSuite {
  override val constraint = new SameImportsForAllBPEL()
  override val casedir = "SameImports"

  test("only one .bpel file") {
    check("onlyOne") match {
      case Some(msg) => fail("Since there is only one .bpel file, the check should pass, but it failed with " + msg)
      case None => // ok!
    }
  }

  test("two .bpel files - same imports") {
    check("shouldPass") match {
      case Some(msg) => fail(
	"Since both .bpel files have the same imports, "
	+ "the check should pass, but it failed with "
	+ msg)
      case None => // ok!
    }
  }

  test("two .bpel files - different imports") {
    check("shouldFail") match {
      case Some(_) => // ok!
      case None => fail(
	"Since the .bpel files have different imports, "
	+ "the check should have failed, but it did not")
    }
  }
}

@RunWith(classOf[JUnitRunner])
class OneOrMoreBPTSWithTemplatesTest extends FunSuite with ConstraintSuite {
  override val constraint = new OneOrMoreBPTSWithTemplates()
  override val casedir = "OneTemplateBasedBPTS"

  test("only fixed .bpel") {
    check("onlyFixed") match {
      case Some(_) => // ok
      case None => fail("Since there is only a fixed .bpts, the check should have failed, but it did not")
    }
  }

  test("only template-based .bpel") {
    check("onlyTemplateBased") match {
      case Some(msg) => fail("Since there is a template-based .bpts, the check shoud have passed, but it failed with " + msg)
      case None => // ok
    }
  }

  test("both fixed and template-based .bpel") {
    check("both") match {
      case Some(msg) => fail("Since there is a template-based .bpts, the check shoud have passed, but it failed with " + msg)
      case None => // ok
    }
  }
}
