package es.uca.webservices.bpel.repo.model

import java.io.File

// Entry in the WS-BPEL repository
class Entry(val basedir: File) {
  def name = basedir.getName
  def filesEndingWith(ext: String) = basedir.listFiles filter (_.getName.endsWith(ext))
  def bpelFiles = filesEndingWith(".bpel")
  def bptsFiles = filesEndingWith(".bpts")
  def wsdlFiles = filesEndingWith(".wsdl")
  def specFiles = filesEndingWith(".spec")
}

// Companion object for Entry
object Entry {
  def list(basedir: File) = {
    for (
      f <- basedir.listFiles
	    if f.isDirectory
	    if !f.getName.startsWith(".")
    )
    yield new Entry(f)
  }

  lazy val all = list(new File(".").getCanonicalFile)
}

class TestResult(val seconds: Double,
		 val failureMsg: Option[String],
		 val exception: Option[Throwable])
