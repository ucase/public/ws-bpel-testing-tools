package es.uca.webservices.bpel.repo

import constraints.Constraint
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.io.File
import java.net.InetAddress
import model.{Entry, TestResult}
import scala.collection.immutable.ListMap
import scala.xml.PrettyPrinter
import java.util

object CLIRunner {
  private def now = System.currentTimeMillis
  private def elapsed(before: Double) = (now - before) / 1000.0

  private def runTests(dirs: Array[File]) = {
    val entries =
      if (dirs.isEmpty) Entry.all
      else dirs map (new Entry(_))

    val results = for (
      entry <- entries;
      constraint <- Constraint.all;
      startTime = now;
      (result, exception) = try {
        constraint.check(entry) match {
          case r @ Some(msg) => (r, None)
          case _ => (None, None)
        }
      } catch {
        case ex => (None, Some(ex))
      }
    ) yield (entry, constraint) -> new TestResult(elapsed(startTime), result, exception)

    ListMap.empty ++ results.reverse
  }

  def stackTrace(ex: Throwable) = {
    val bos = new ByteArrayOutputStream
    ex.printStackTrace(new PrintStream(bos))
    bos.toString
  }

  def main(args: Array[String]) {
    val dirs = args map (new File(_))
    val badDirs = dirs filter (d => !d.canRead || !d.isDirectory)
    if (!dirs.isEmpty && !badDirs.isEmpty) {
      System.err.println(
        (
          if (badDirs.size == 1) badDirs(0) + " is not a readable directory"
          else badDirs.mkString("[", ", ", "]") + " are not readable directories"
        )
        + ": exiting..."
      )
      System.exit(1)
    }

    val startTime = now
    val results   = runTests(dirs)
    val totalTime = elapsed(startTime)
    val nErrors   = (results.values filter (_.exception != None)).size
    val nFailures = (results.values filter (_.failureMsg != None)).size
    val hostname  = try { InetAddress.getLocalHost.getHostName } catch { case _ : Throwable => "localhost" }
    val pkgPrefix = "es.uca.webservices.bpel.repo."

    val xml = <testsuite name={pkgPrefix + "Lint"}
                         time={totalTime.toString}
                         tests={(Entry.all.size * Constraint.all.size).toString}
                         errors={nErrors.toString}
                         failures={nFailures.toString}
                         hostname={hostname}>
    {
      for (((entry, constraint), result) <- results)
	yield <testcase classname={pkgPrefix + entry.name + "Test"} name={constraint.name} time={result.seconds.toString}>{
	  (result.exception, result.failureMsg) match {
	    case (Some(ex),  _) => <error message={ex.toString} type={ex.getClass.getName}>{stackTrace(ex)}</error>
	    case (_, Some(msg)) => <failure message={msg.toString} type="es.uca.webservices.bpel.repo.LintFailure"/>
	    case _ => // nothing
	  }
	}</testcase>
    }
    </testsuite>

    println(new PrettyPrinter(80, 2).format(xml))
  }
}
