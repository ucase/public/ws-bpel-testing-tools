package es.uca.webservices.bpel.repo.constraints

import es.uca.webservices.bpel.repo.model.Entry
import es.uca.webservices.mutants.subcommands.RunSubcommand
import es.uca.webservices.testgen.TestGeneratorRun
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.methods.GetMethod
import scala.xml.XML

abstract class Constraint {
  def name: String
  def check(c: Entry): Option[String]
}

object Constraint {
  lazy val all = List(
    new OneOrMoreBPEL,
    new OneOrMoreBPTS,
    new OneOrMoreBPTSWithTemplates,
    new OneOrMoreDataSpecification,
    new OneOrMoreDataFile,
    new NoArchives,
    new NoTemporaryFiles,
    new NoProjectFiles,
    new SameImportsForAllBPEL,
    new NoBPELExtensions,
    new AllBPTSBPELCombinationsPass,
    new WSDLFilesMeetWSIBasicProfile,
    new SpecFilesAreCorrect,
    new EntryHasWikiPage
  )
}

// FILE EXISTENCE CHECKS

private[constraints] trait OneOrMoreMatchingFile extends Constraint {
  def expectation: String
  def testFile(f: File): Boolean

  override def check(e: Entry): Option[String] = {
    (e.basedir.listFiles filter testFile).size match {
      case 0 => Some(expectation)
      case _ => None
    }
  }
}

private[constraints] class OneOrMoreBPEL extends Constraint with OneOrMoreMatchingFile {
  override val name = "OneOrMoreBPEL"
  override val expectation = "There should be at least one .bpel file"
  override def testFile(f: File) = f.getName.endsWith(".bpel")
}

private[constraints] class OneOrMoreBPTS extends Constraint with OneOrMoreMatchingFile {
  override val name = "OneOrMoreBPTS"
  override val expectation = "There should be at least one .bpts file"
  override def testFile(f: File) = f.getName.endsWith(".bpts")
}

private[constraints] class OneOrMoreBPTSWithTemplates extends Constraint with OneOrMoreMatchingFile {
  override val name = "OneOrMoreBPTSWithTemplates"
  override val expectation = "There should be at least one template-based .bpts file"
  override def testFile(f: File) = (f.getName.endsWith(".bpts") && (XML.loadFile(f) \\ "dataSource").size > 0)
}

private[constraints] class OneOrMoreDataSpecification extends Constraint with OneOrMoreMatchingFile {
  override val name = "OneOrMoreDataSpecification"
  override val expectation = "There should be at least one .spec file"
  override def testFile(f: File) = f.getName.endsWith(".spec")
}

private[constraints] class OneOrMoreDataFile extends Constraint with OneOrMoreMatchingFile {
  override val name = "OneOrMoreDataFile"
  override val expectation = "There should be at least one data file"
  override def testFile(f: File) = f.getName.endsWith(".csv") || f.getName.endsWith(".vm")
}

// FORBIDDEN FILE CHECKS

private[constraints] class NoProjectFiles extends Constraint {
  override val name = "NoProjectFiles"
  override def check(e: Entry): Option[String] = {
    val forbidden = List(
      ".classpath", ".project", ".settings", // Eclipse
      "build.xml", // Ant
      "nb-build.xml", "nbbuild", "nbdist", "nbproject" // NetBeans
      )

    if (e.basedir.listFiles exists (f => forbidden.contains(f.getName)))
      Some("Should not have any IDE- or build tool-related project files, but it did. Please remove any of these files: " + forbidden.mkString("\n\n- ", "\n- ", ""))
    else
      None
  }
}

private[constraints] class NoArchives extends Constraint {
  override val name = "NoArchives"
  override def check(e: Entry): Option[String] = {
    val forbiddenExtensions = List(".zip", ".bpr")
    val archives = e.basedir.list filter (filename => forbiddenExtensions exists (extension => filename.endsWith(extension)))

    archives.size match {
      case 0 => None
      case _ => Some("Should not have any files with the extensions " + forbiddenExtensions.mkString("(", ", ", ")"))
    }
  }
}

private[constraints] class NoTemporaryFiles extends Constraint {
  override val name = "NoTemporaryFiles"
  override def check(e: Entry): Option[String] = {
    val tmpFiles = e.basedir.list filter (name => name.endsWith("~") || name.startsWith("#"))
    tmpFiles.size match { 
      case 0 => None
      case _ => Some("Should not have any temporary files, but these were found: " + tmpFiles.mkString("(", ",", ")"))
    }
  }
}

// CONTENT CHECKS

private[constraints] class SameImportsForAllBPEL extends Constraint {
  override val name = "SameImportsForAllBPEL"

  override def check(e: Entry): Option[String] = {
    val importedLocs = (e.bpelFiles map (b => (b, Set(for (i <- XML.loadFile(b) \\ "import") yield i \ "@location")))).toMap

    val mismatched = (for (
      i <- 1 to e.bpelFiles.size;
      other <- e.bpelFiles drop i if importedLocs(e.bpelFiles(i - 1)) != importedLocs(other)
    ) yield (e.bpelFiles(i - 1), other))

    mismatched.size match {
      case 0 => None
      case x => Some(
        """|Every pair of .bpel files should import the same .wsdl and .xsd documents.
	   |However, these pairs import different .wsdl and .xsd documents:""".stripMargin
          + mismatched.mkString("\n- ", "\n- ", "\n"))
    }
  }
}

private[constraints] class NoBPELExtensions extends Constraint {
  override val name = "NoBPELExtensions"

  override def check(e: Entry): Option[String] = {
    val bpelWithExtensions = e.bpelFiles filter (f => (XML.loadFile(f) \\ "extensions").size > 0)

    bpelWithExtensions.size match {
      case 0 => None
      case x => Some(
        "%d of the %d .bpel files use non-standard WS-BPEL extensions: ".format(x, e.bpelFiles.size)
        + bpelWithExtensions.mkString("", ", ", "")
      )
    }
  }
}

private[constraints] class AllBPTSBPELCombinationsPass extends Constraint {
  override val name = "AllBPTSBPELCombinationsPass"

  private def pass(bpel: File, bpts: File) = {
    val fOutput = new File("bpts-output.xml")
    val ios     = new FileOutputStream(fOutput)

    try {
      try {
        val cmd = new RunSubcommand
        cmd.setOutputStream(ios)
        cmd.run(bpts.getPath, bpel.getPath)
      } finally {
        ios.close
      }

      (XML.loadFile(fOutput.getName) \ "@result").toString == "PASSED"
    } finally {
      fOutput.delete
    }
  }

  override def check(e: Entry): Option[String] = {
    val badCombinations = (
      for (bpel <- e.bpelFiles; bpts <- e.bptsFiles if !pass(bpel, bpts))
        yield (bpel, bpts)
    )
    
    badCombinations.size match {
      case 0 => None
      case _ => Some("Every .bpel should pass all the tests in every .bpts file,"
                     + " but these combinations do not work: "
                     + badCombinations.mkString("", ", ", ""))
    }
  }
}

private[constraints] class WSDLFilesMeetWSIBasicProfile extends Constraint {
  override val name = "WSDLFilesMeetWSIBasicProfile"

  override def check(e: Entry): Option[String] = {
    val wsdlFilenames = e.wsdlFiles map (_.getPath)
    val analyzer = new ServiceAnalyzer(wsdlFilenames: _*)
    analyzer.getMessageCatalog

    // If we're still here, no exception was thrown and everything is well
    None
  }
}

private[constraints] class SpecFilesAreCorrect extends Constraint {
  override val name = "SpecFilesAreCorrect"

  override def check(e: Entry): Option[String] = {
    var badSpecs = List[File]()
    for (spec <- e.specFiles)
      try {
        // Use the default strategy
        val generator = new TestGeneratorRun(spec.getPath, "velocity", 1, "")
        generator.run(new ByteArrayOutputStream)
      } catch {
        case _ : Throwable => badSpecs = spec :: badSpecs
      }

    badSpecs.size match {
      case 0 => None
      case x => Some("There were %d invalid .spec files: %s"
                     .format(x, badSpecs.mkString("(", ", ", ")")))
    }
  }
}

// WIKI CHECKS

private[constraints] class EntryHasWikiPage extends Constraint {
  private val WIKI_URL_PREFIX = "https://neptuno.uca.es/redmine/projects/wsbpel-comp-repo/wiki/"

  override val name = "EntryHasWikiPage"

  override def check(e: Entry): Option[String] = {
    val client = new HttpClient
    val url    = WIKI_URL_PREFIX + e.name
    val method = new GetMethod(url)
    client.executeMethod(method)

    method.getStatusCode match {
      case 200 => None
      case 404 => Some("The wiki page for %s at %s does not exist".format(e.name, url))
      case x => Some(
        ("There was a problem while checking if the wiki page for "
         + "%s at %s existed: status code is %d").format(e.name, url, x)
      )
    }
  }
}
