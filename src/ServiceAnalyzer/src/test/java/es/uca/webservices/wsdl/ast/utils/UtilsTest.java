/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.ast.utils;

import serviceAnalyzer.messageCatalog.TypeGA;
import serviceAnalyzer.messageCatalog.TypeTypedef;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class UtilsTest {

    @Test
    public void ElementsTest() throws Exception {
        assertEquals("", TypedefUtils.getElementsString(new String[] {"int", "float"}), "int,float");

        String [] elements = TypedefUtils.getElementsArray("int,float");
        assertEquals("", elements.length, 2);
        assertEquals("", elements[0], "int");
        assertEquals("", elements[1], "float");

        elements = TypedefUtils.getElementsArray(" int ,  float   ");
        assertEquals("", elements.length, 2);
        assertEquals("", elements[0], "int");
        assertEquals("", elements[1], "float");
    }

    @Test
    public void ValuesTest() throws Exception {
        assertEquals("", TypedefUtils.getValuesString(new String[] {"true", "false"}), "true,false");

        String [] values = TypedefUtils.getValuesArray("true,false");
        assertEquals("", values.length, 2);
        assertEquals("", values[0], "true");
        assertEquals("", values[1], "false");

        values = TypedefUtils.getValuesArray("   true  ,   false  ");
        assertEquals("", values.length, 2);
        assertEquals("", values[0], "true");
        assertEquals("", values[1], "false");

        values = TypedefUtils.getValuesArray(" () , [] , {} ");
        assertEquals("", values.length, 3);
        assertEquals("", values[0], "()");
        assertEquals("", values[1], "[]");
        assertEquals("", values[2], "{}");
    } 

    @Test
    public void SameLengthElementsListTest() throws Exception {
        assertTrue("", TypedefUtils.equalsElements(new String[] {"a", "b", "c"}, new String[] {"a", "b", "c"}));
        assertFalse("", TypedefUtils.equalsElements(new String[] {"a", "b", "c"}, new String[] {"x", "b", "c"}));
        assertFalse("", TypedefUtils.equalsElements(new String[] {"a", "b", "c"}, new String[] {"a", "x", "c"}));
        assertFalse("", TypedefUtils.equalsElements(new String[] {"a", "b", "c"}, new String[] {"a", "b", "x"}));
        assertFalse("", TypedefUtils.equalsElements(new String[] {"a", "b", "c"}, new String[] {"c", "a", "b"}));
    }

    @Test
    public void DifferentLengthElementsListTest() throws Exception {
        assertFalse("", TypedefUtils.equalsElements(new String[] {}, new String[] {"a", "b", "c"}));
        assertFalse("", TypedefUtils.equalsElements(new String[] {"a", "b", "c"}, new String[] {}));
        assertFalse("", TypedefUtils.equalsElements(new String[] {"a", "b"}, new String[] {"a", "b", "c"}));
        assertFalse("", TypedefUtils.equalsElements(new String[] {"a", "b", "c"}, new String[] {"a", "b"}));
    }

    @Test
    public void SameLengthValuesListTest() throws Exception {
        assertTrue("", TypedefUtils.equalsValues(new String[] {"a", "b", "c"}, new String[] {"a", "b", "c"}));
        assertFalse("", TypedefUtils.equalsValues(new String[] {"a", "b", "c"}, new String[] {"x", "b", "c"}));
        assertFalse("", TypedefUtils.equalsValues(new String[] {"a", "b", "c"}, new String[] {"a", "x", "c"}));
        assertFalse("", TypedefUtils.equalsValues(new String[] {"a", "b", "c"}, new String[] {"a", "b", "x"}));
        assertFalse("", TypedefUtils.equalsValues(new String[] {"a", "a", "a"}, new String[] {"a", "b", "x"}));
        assertFalse("", TypedefUtils.equalsValues(new String[] {"a", "b", "c"}, new String[] {"a", "a", "a"}));
        assertTrue("", TypedefUtils.equalsValues(new String[] {"a", "b", "c"}, new String[] {"c", "a", "b"}));
    }

    @Test
    public void DifferentLengthValuesListTest() throws Exception {
        assertFalse("", TypedefUtils.equalsValues(new String[] {}, new String[] {"a", "b", "c"}));
        assertFalse("", TypedefUtils.equalsValues(new String[] {"a", "b", "c"}, new String[] {}));
        assertFalse("", TypedefUtils.equalsValues(new String[] {"a", "b"}, new String[] {"a", "b", "c"}));
        assertFalse("", TypedefUtils.equalsValues(new String[] {"a", "b", "c"}, new String[] {"a", "b"}));
    }

     public void TypedefEqualsTest() throws Exception {
        TypeTypedef t1, t2, t3;

        t1 = TypeTypedef.Factory.newInstance();
        t1.setName(null);
        t1.setType(TypeGA.STRING);
        t1.setValues("[true:false]");

        t2 = TypeTypedef.Factory.newInstance();
        t2.setName(null);
        t2.setType(TypeGA.STRING);
        t2.setValues("[true:false]");

        t3 = TypeTypedef.Factory.newInstance();
        t3.setName(null);
        t3.setType(TypeGA.STRING);
        t3.setValues(TypedefUtils.getValuesString(new String[] {"true", "false"}));
        t3.setMin("3");

        assertTrue(TypedefUtils.equals(t1, t2));
        assertFalse(TypedefUtils.equals(t1, t3));
    }

}
