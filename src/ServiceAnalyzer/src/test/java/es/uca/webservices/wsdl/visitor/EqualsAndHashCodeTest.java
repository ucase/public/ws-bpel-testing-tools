/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package es.uca.webservices.wsdl.visitor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.junit.Test;

import serviceAnalyzer.messageCatalog.TypeGA;
import serviceAnalyzer.messageCatalog.TypeGA.Enum;
import serviceAnalyzer.messageCatalog.TypeTypedef;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.wsdl.ast.ASTNode;
import es.uca.webservices.wsdl.ast.AttrNode;
import es.uca.webservices.wsdl.ast.AttrNode.AVT;
import es.uca.webservices.wsdl.ast.ElemNode;
import es.uca.webservices.wsdl.ast.ForeachNode;
import es.uca.webservices.wsdl.ast.ForeachNode.TYPE;
import es.uca.webservices.wsdl.ast.ListNode;
import es.uca.webservices.wsdl.ast.VariableNode;
import es.uca.webservices.wsdl.ast.utils.TypedefUtils;


/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class EqualsAndHashCodeTest {
    @Test
    public void AttrNodeTest() throws Exception {
        AttrNode a, b, c;
        VariableNode v;
        v = new VariableNode(new QName("http://www.w3.org/2001/XMLSchema", "string"));
        a = new AttrNode(new QName("attribute"), v, AVT.DEFAULT, "default", false);
        b = new AttrNode(new QName("attribute"), v, AVT.DEFAULT, "default", false);
        assertTrue(a.equals(b));
        assertEquals(a.hashCode(), b.hashCode());

        c = new AttrNode(new QName("attributeC"), v, AVT.NONE, null, false);
        assertFalse(a.equals(c));
        assertFalse(a.hashCode() == c.hashCode());

        assertFalse(a.equals(v));
        assertFalse(a.hashCode() == v.hashCode());
    }

    public void ElemNodeTest() throws Exception {
        ElemNode a, b, c;

        VariableNode v = new VariableNode(new QName("http://www.w3.org/2001/XMLSchema", "string"));
        AttrNode at = new AttrNode(new QName("attribute"), v, AVT.DEFAULT, "default", false);

        a = new ElemNode(new QName("www.test.com", "element"), at, v);
        b = new ElemNode(new QName("www.test.com", "element"), at, v);
        assertTrue(a.equals(b));
        assertEquals(a.hashCode(), b.hashCode());

        c = new ElemNode(new QName("www.cest.com", "element"), at, v);
        assertFalse(a.equals(c));
        assertFalse(a.hashCode() == c.hashCode());

        assertFalse(a.equals(v));
        assertFalse(a.hashCode() == v.hashCode());
    }

    public void ForeachNodeTest() throws Exception {
        ForeachNode a, b, c;

        VariableNode v = new VariableNode(new QName("http://www.w3.org/2001/XMLSchema", "string"));

        a = new ForeachNode(v, TYPE.LIST, BigInteger.ZERO, BigInteger.TEN, null, "[a-z]*");
        b = new ForeachNode(v, TYPE.LIST, BigInteger.ZERO, BigInteger.TEN, null, "[a-z]*");

        assertTrue(a.equals(b));
        assertEquals(a.hashCode(), b.hashCode());

        c = new ForeachNode(v, TYPE.LIST, null, null, null, "[a-z]*");
        assertFalse(a.equals(c));
        assertFalse(a.hashCode() == c.hashCode());

        assertFalse(a.equals(v));
        assertFalse(a.hashCode() == v.hashCode());
    }

    public void ListNodeTest() throws Exception {
        ListNode a, b, c;

        VariableNode v1 = new VariableNode(new QName("http://www.w3.org/2001/XMLSchema", "string"));
        VariableNode v2 = new VariableNode(new QName("http://www.w3.org/2001/XMLSchema", "float"));
        List<ASTNode> nodes = new ArrayList<ASTNode>(2);
        nodes.add(new ElemNode(new QName("node1"), null, v1));
        nodes.add(new ElemNode(new QName("node2"), null, v2));
        a = new ListNode(nodes, true);
        b = new ListNode(nodes, true);

        assertTrue(a.equals(b));
        assertEquals(a.hashCode(), b.hashCode());

        c = new ListNode(nodes, false);
        assertFalse(a.equals(c));
        assertFalse(a.hashCode() == c.hashCode());

        assertFalse(a.equals(v2));
        assertFalse(a.hashCode() == v2.hashCode());
    }

    public void VariableNodeTest() throws Exception {
        VariableNode a, b, c;

        TypeTypedef typedef = TypeTypedef.Factory.newInstance();
        typedef = TypeTypedef.Factory.newInstance();
        typedef.setName(null);
        typedef.setType(TypeGA.STRING);
        typedef.setValues(TypedefUtils.getValuesString(new String[] {"true", "false"}));

        a = new VariableNode(new Pair<Enum, TypeTypedef>(null, typedef));
        b = new VariableNode(new QName("http://www.w3.org/2001/XMLSchema", "boolean"));

        assertTrue(a.equals(b));
        assertEquals(a.hashCode(), b.hashCode());

        typedef.setMin("4");
        c = new VariableNode(new Pair<Enum, TypeTypedef>(null, typedef));
        assertFalse(a.equals(c));
        assertFalse(a.hashCode() == c.hashCode());

        ElemNode e = new ElemNode(new QName("element"), null, null);
        assertFalse(a.equals(e));
        assertFalse(a.hashCode() == e.hashCode());
    }
}
