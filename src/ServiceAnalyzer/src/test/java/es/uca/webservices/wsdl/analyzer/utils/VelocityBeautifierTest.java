/*
 *  Copyright 2011 Antonio García-Domínguez (antonio.garciadominguez@uca.es)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.analyzer.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests for the Velocity template beautifier.
 * @author Antonio García-Domínguez
 */
public class VelocityBeautifierTest {

	private VelocityBeautifier beautifier;

	@Before
	public void setup() {
		beautifier = new VelocityBeautifier();
	}

	@Test
	public void nullString() {
		checkBeautified(null, null);
	}

	@Test
	public void emptyString() {
		checkBeautified("", "");
	}

	@Test
	public void noTags() {
		checkBeautified("foo", "foo");
	}

	@Test
	public void singleTag() {
		checkBeautified("<a/>", "<a/>");
	}

	@Test
	public void singleTagWithTextContent() {
		checkBeautified("<a>b</a>", "<a>b</a>");
	}

	@Test
	public void oneLevel() {
		checkBeautified("<a><b/><c/></a>", "<a>\n  <b/>\n  <c/>\n</a>");
	}

	@Test
	public void oneLevelWhitespace() {
		checkBeautified("<a> <b/> <c/> </a>", "<a>\n  <b/>\n  <c/>\n</a>");
	}

	@Test
	public void oneLevelText() {
		checkBeautified(" x <a> y <b/> z <c/> w </a> u",
				"x\n" +
				"<a>\n" +
				"  y\n" +
				"  <b/>\n" +
				"  z\n" +
				"  <c/>\n" +
				"  w\n" +
				"</a>\n" +
				"u");
	}

	@Test
	public void twoLevels() {
		checkBeautified("<a><b><c/><d/></b><e><f/><g/></e></a>",
				"<a>\n" +
				"  <b>\n" +
				"    <c/>\n" +
				"    <d/>\n" +
				"  </b>\n" +
				"  <e>\n" +
				"    <f/>\n" +
				"    <g/>\n" +
				"  </e>\n" +
				"</a>");
	}

	@Test
	public void twoLevelsAttributes() {
		checkBeautified("<a foo=\"2\"><b><c/><d bar=\"42\"/></b><e><f/><g/></e></a>",
				"<a foo=\"2\">\n" +
				"  <b>\n" +
				"    <c/>\n" +
				"    <d bar=\"42\"/>\n" +
				"  </b>\n" +
				"  <e>\n" +
				"    <f/>\n" +
				"    <g/>\n" +
				"  </e>\n" +
				"</a>");
	}

	private void checkBeautified(final String src, final String expected) {
		assertEquals(expected, beautifier.beautify(src));
	}
}
