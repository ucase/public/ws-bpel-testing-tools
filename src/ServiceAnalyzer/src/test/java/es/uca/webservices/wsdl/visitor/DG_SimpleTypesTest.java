/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.visitor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathConstants;

import org.junit.Test;

import es.uca.webservices.bpel.xml.XMLDocument;
import es.uca.webservices.wsdl.ast.Parser.EncodingStyle;
import es.uca.webservices.wsdl.ast.utils.TypedefUtils;

/**
 * 
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class DG_SimpleTypesTest extends DeclarationsGenBaseTest {

	private static final String NAMESPACE = "http://ServiceAnalyzerTests/SimpleTypes";

	public DG_SimpleTypesTest() {
		super("SimpleTypes");
	}

	@Test
	public void MinMaxInclusiveTest() throws Exception {
		QName element = new QName(NAMESPACE, "est1");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 1",
				declarationsXMLDoc, RESTRICTIONS.MIN, "1");
		assertTypedefHasRestricction("Debería tener máximo 50",
				declarationsXMLDoc, RESTRICTIONS.MAX, "50");
	}

	@Test
	public void MinMaxExclusiveTest() throws Exception {
		QName element = new QName(NAMESPACE, "est2");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 1 + 1 = 2",
				declarationsXMLDoc, RESTRICTIONS.MIN, "2");
		assertTypedefHasRestricction("Debería tener máximo 50 - 1 = 49 ",
				declarationsXMLDoc, RESTRICTIONS.MAX, "49");
	}

	@Test
	public void MinMaxLengthTest() throws Exception {
		QName element = new QName(NAMESPACE, "est3");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 10",
				declarationsXMLDoc, RESTRICTIONS.MIN, "10");
		assertTypedefHasRestricction("Debería tener máximo 15",
				declarationsXMLDoc, RESTRICTIONS.MAX, "15");

		element = new QName(NAMESPACE, "est4");
		declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 10",
				declarationsXMLDoc, RESTRICTIONS.MIN, "10");
		assertTypedefHasRestricction("Debería tener máximo 10",
				declarationsXMLDoc, RESTRICTIONS.MAX, "10");
	}

	@Test
	public void TypesWithMinMaxTest() throws Exception {
		QName element = new QName(NAMESPACE, "est5");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction(
				"Debería tener mínimo 1 solapando el mínimo (invisible) del tipo integer",
				declarationsXMLDoc, RESTRICTIONS.MIN, "1");
		assertTypedefHasRestricction(
				"Debería tener máximo 19 solapando el máximo (invisible) del tipo integer",
				declarationsXMLDoc, RESTRICTIONS.MAX, "19");

		element = new QName(NAMESPACE, "est6");
		declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction(
				"Debería tener mínimo 1 solapando el mínimo (invisible) del tipo int",
				declarationsXMLDoc, RESTRICTIONS.MIN, "1");

		element = new QName(NAMESPACE, "est16");
		declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction(
				"Debería tener mínimo 1 solapando el mínimo del tipo short por defecto",
				declarationsXMLDoc, RESTRICTIONS.MIN, "1");
		assertTypedefHasRestricction("Debería tener máximo 32767",
				declarationsXMLDoc, RESTRICTIONS.MAX, "32767");
	}

	@Test
	public void EnumerationsTest() throws Exception {
		QName element = new QName(NAMESPACE, "est7");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener values bad,good",
				declarationsXMLDoc, RESTRICTIONS.VALUES,
				TypedefUtils.getValuesString(new String[] { "bad", "good" }));

		element = new QName(NAMESPACE, "est8");
		declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTrue(
				"Debería traducir la restriccion enumeration con más restricciones",
				(Boolean) declarationsXMLDoc
						.evaluateExpression(
								"count(//*[local-name(.) = 'typedef']) = 1 "
										+ "and count(//*[local-name(.) = 'typedef']/@*) = 4 ",
								XPathConstants.BOOLEAN));
	}

	@Test
	public void fractionDigitsFacetTest() throws Exception {
		QName element = new QName(NAMESPACE, "est14");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("]", declarationsXMLDoc,
				RESTRICTIONS.FRACTION_DIGITS, "2");
	}

	@Test
	public void totalDigitsFacetTest() throws Exception {
		QName element = new QName(NAMESPACE, "est15");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("", declarationsXMLDoc,
				RESTRICTIONS.TOTAL_DIGITS, "5");
	}

	@Test
	public void patternFacetTest() throws Exception {
		QName element = new QName(NAMESPACE, "est20");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction(" ", declarationsXMLDoc,
				RESTRICTIONS.PATTERN, "\\d{3}-[A-Z]{2}");
	}

	@Test
	public void ListDatatypeTest() throws Exception {

		// Lista de un tipo primitivo
		QName element = new QName(NAMESPACE, "est9");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTrue(
				"El tipo simple list no se genera correctamente",
				(Boolean) declarationsXMLDoc
						.evaluateExpression(
								"count(//*[local-name(.) = 'typedef']) = 1 "
										+ "and count(//*[local-name(.) = 'typedef' and @type='list' and @element='int']) = 1 ",
								XPathConstants.BOOLEAN));

		// Idem al anterior pero con restricciones asociadas a la lista

		element = new QName(NAMESPACE, "est10");
		declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTrue(
				"El tipo simple list no se genera correctamente (2)",
				(Boolean) declarationsXMLDoc
						.evaluateExpression(
								"count(//*[local-name(.) = 'typedef']) = 1 "
										+ "and count(//*[local-name(.) = 'typedef' and @type='list' and @element='int' "
										+ "and @min='4' and @max='4']) = 1 ",
								XPathConstants.BOOLEAN));

		// Lista de un tipo con restricciones

		element = new QName(NAMESPACE, "est11");
		declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		String typedefName = (String) declarationsXMLDoc.evaluateExpression(
				"//*[local-name(.) = 'typedef' and @type != 'list']/@name",
				XPathConstants.STRING);
		assertTrue(
				"El tipo simple list no se genera correctamente (3)",
				(Boolean) declarationsXMLDoc
						.evaluateExpression(
								"count(//*[local-name(.) = 'typedef']) = 2 "
										+ "and count(//*[local-name(.) = 'typedef' and @type='list' and @element='"
										+ typedefName + "']) = 1 ",
								XPathConstants.BOOLEAN));

		// Idem al anterior pero también con restricciones asociadas a la lista

		element = new QName(NAMESPACE, "est12");
		declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTrue(
				"El tipo simple list no se genera correctamente (4)",
				(Boolean) declarationsXMLDoc
						.evaluateExpression(
								"count(//*[local-name(.) = 'typedef']) = 2 "
										+ "and count(//*[local-name(.) = 'typedef' and @type='list' and @element='"
										+ typedefName + "' and @max='5']) = 1 ",
								XPathConstants.BOOLEAN));
		// Nuevas restricciones de listas
	}

	@Test
	public void MinMaxInclusiveFloatTest() throws Exception {
		QName element = new QName(NAMESPACE, "est21");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 1.0",
				declarationsXMLDoc, RESTRICTIONS.MIN, "1.0");
		assertTypedefHasRestricction("Debería tener máximo 50.0",
				declarationsXMLDoc, RESTRICTIONS.MAX, "50.0");
	}

	@Test
	public void MinMaxExclusiveFloatTest() throws Exception {
		QName element = new QName(NAMESPACE, "est22");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 1.0",
				declarationsXMLDoc, RESTRICTIONS.MIN, "1.0");
		assertTypedefHasRestricction("Debería tener máximo 50.0",
				declarationsXMLDoc, RESTRICTIONS.MAX, "50.0");
	}

	@Test
	public void MinMaxDoubleTest() throws Exception {
		QName element = new QName(NAMESPACE, "est23");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 1.0",
				declarationsXMLDoc, RESTRICTIONS.MIN, "1.0");
		assertTypedefHasRestricction("Debería tener máximo 50.0",
				declarationsXMLDoc, RESTRICTIONS.MAX, "50.0");
	}

	@Test
	public void MinMaxDateTimeTest() throws Exception {
		QName element = new QName(NAMESPACE, "est24");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction(
				"Debería tener mínimo 2008-07-01T00:00:00.000+02:00",
				declarationsXMLDoc, RESTRICTIONS.MIN,
				"2008-07-01T00:00:00.000+02:00");
		assertTypedefHasRestricction(
				"Debería tener máximo 2009-09-29T03:49:44.999+02:00",
				declarationsXMLDoc, RESTRICTIONS.MAX,
				"2009-09-29T03:49:44.999+02:00");
	}

	@Test
	public void MinMaxTimeTest() throws Exception {
		QName element = new QName(NAMESPACE, "est25");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 13:00:00.001+01:00",
				declarationsXMLDoc, RESTRICTIONS.MIN, "13:00:00.001+01:00");
		assertTypedefHasRestricction("Debería tener máximo 14:30:00-05:00",
				declarationsXMLDoc, RESTRICTIONS.MAX, "14:30:00-05:00");
	}

	@Test
	public void MinMaxDateTest() throws Exception {
		QName element = new QName(NAMESPACE, "est26");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 13:00:00.001+01:00",
				declarationsXMLDoc, RESTRICTIONS.MIN, "2008-06-30");
		assertTypedefHasRestricction("Debería tener máximo 14:30:00-05:00",
				declarationsXMLDoc, RESTRICTIONS.MAX, "2012-02-29");
	}

	@Test
	public void MinMaxGYearMonthTest() throws Exception {
		QName element = new QName(NAMESPACE, "est27");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 2008-07+02:00",
				declarationsXMLDoc, RESTRICTIONS.MIN, "2008-07+02:00");
		assertTypedefHasRestricction("Debería tener máximo 2009-09-29+02:00",
				declarationsXMLDoc, RESTRICTIONS.MAX, "2009-08+02:00");
	}

	@Test
	public void MinMaxGYearTest() throws Exception {
		QName element = new QName(NAMESPACE, "est28");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 2008-07+02:00",
				declarationsXMLDoc, RESTRICTIONS.MIN, "2012+02:00");
		assertTypedefHasRestricction("Debería tener máximo 2009-09-29+02:00",
				declarationsXMLDoc, RESTRICTIONS.MAX, "2020+02:00");
	}

	@Test
	public void MinMaxGMonthTest() throws Exception {
		QName element = new QName(NAMESPACE, "est29");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 2008-07+02:00",
				declarationsXMLDoc, RESTRICTIONS.MIN, "--10+01:00");
		assertTypedefHasRestricction("Debería tener máximo 2009-09-29+02:00",
				declarationsXMLDoc, RESTRICTIONS.MAX, "--11+01:00");
	}

	@Test
	public void MinMaxGMonthDayTest() throws Exception {
		QName element = new QName(NAMESPACE, "est30");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 2008-07+02:00",
				declarationsXMLDoc, RESTRICTIONS.MIN, "--09-13+01:00");
		assertTypedefHasRestricction("Debería tener máximo 2009-09-29+02:00",
				declarationsXMLDoc, RESTRICTIONS.MAX, "--12-11+01:00");
	}

	@Test
	public void MinMaxGDayTest() throws Exception {
		QName element = new QName(NAMESPACE, "est31");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 2008-07+02:00",
				declarationsXMLDoc, RESTRICTIONS.MIN, "---01+01:00");
		assertTypedefHasRestricction("Debería tener máximo 2009-09-29+02:00",
				declarationsXMLDoc, RESTRICTIONS.MAX, "---30+01:00");
	}

	@Test
	public void MinMaxDurationTest() throws Exception {
		QName element = new QName(NAMESPACE, "est32");
		XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element,
				EncodingStyle.DOCUMENT);

		assertTypedefHasRestricction("Debería tener mínimo 2008-07+02:00",
				declarationsXMLDoc, RESTRICTIONS.MIN, "PT1S");
		assertTypedefHasRestricction("Debería tener máximo 2009-09-29+02:00",
				declarationsXMLDoc, RESTRICTIONS.MAX, "P2Y2M2DT2H2M-2S");
	}

	@Test
	public void union() throws Exception {
		QName element = new QName(NAMESPACE, "est33");
		XMLDocument xmldoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);

		assertEquals("There should be typedefs for the choice number, both possibilities and the resulting tuple",
				"4", xmldoc.evaluateExpression("count(//*[local-name() = 'typedef'])"));
		assertEquals("Choice numbers should be >= 1", "1", xmldoc.evaluateExpression("//*[@name = 'TChoice2']/@min"));
		assertEquals("Choice numbers should be <= 2", "2", xmldoc.evaluateExpression("//*[@name = 'TChoice2']/@max"));
	}
}
