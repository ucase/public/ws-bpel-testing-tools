package es.uca.webservices.wsdl.visitor;

import static org.junit.Assert.assertEquals;

import javax.xml.namespace.QName;

import org.junit.Test;

import es.uca.webservices.bpel.xml.XMLDocument;
import es.uca.webservices.wsdl.ast.Parser.EncodingStyle;

public class IntRestrictionTest extends DeclarationsGenBaseTest {

	public IntRestrictionTest() {
		super("IntRestriction");
	}

	@Test
	public void intRestriction() throws Exception {
		final QName element = new QName("http://example.com/shipping/ship.xsd", "shipOrder");
		final XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);

		assertEquals(
				"0",
				(String) declarationsXMLDoc
						.evaluateExpression("count(//*[local-name(.) = 'typedef' and @name='TItemsCount' and @type='float'])"));
		assertEquals(
				"1",
				(String) declarationsXMLDoc
						.evaluateExpression("count(//*[local-name(.) = 'typedef' and @name='TItemsCount' and @type='int'])"));
	}
}
