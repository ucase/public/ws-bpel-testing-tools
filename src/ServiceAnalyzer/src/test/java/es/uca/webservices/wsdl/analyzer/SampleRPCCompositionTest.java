/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.analyzer;

import static es.uca.webservices.wsdl.test.TestUtil.assertEqualXML;
import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.xml.xpath.XPathConstants;

import org.junit.Test;

import es.uca.webservices.bpel.xml.XMLDocument;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class SampleRPCCompositionTest extends CompositionsBaseTest {

    public SampleRPCCompositionTest() throws IOException {
        super("StyleSamples", new String[]{"SampleRPC"});
    }

    @Test
    public void EmptyMessageTest() throws Exception {
        ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);

        sa.generateMessageCatalog();
        XMLDocument doc = sa.getMessageCatalog();

        String decls = (String) doc.evaluateExpression(
                "//*[local-name(.) = 'operation' and @name = 'op1']//*[local-name(.) = 'decls']",
                XPathConstants.STRING);

        assertEquals(decls, "");

        String template = (String) doc.evaluateExpression(
                "//*[local-name(.) = 'operation' and @name = 'op1']//*[local-name(.) = 'template']",
                XPathConstants.STRING);

        assertEquals(template, "");
    }

    @Test
    public void ResponseInWrapperTest() throws Exception {
        ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);

        sa.generateMessageCatalog();
        XMLDocument doc = sa.getMessageCatalog();

        String template = (String) doc.evaluateExpression(
                "//*[local-name(.) = 'operation' and @name = 'op1']/*[local-name(.) = 'output']/*[local-name(.) = 'template']",
                XPathConstants.STRING);

        assertEqualXML("<o:op1Response xmlns:o=\"http://op1Namespace.com\" xmlns:s=\"http://schema1Namespace.com\">" // "Response"
                + "<op1Return>"
                + "<data1>$op1Response.get(0)</data1>"
                + "<s:RefDataElem>$op1Response.get(1)</s:RefDataElem>"
                + "</op1Return>"
                + "</o:op1Response>", template);
    }

    @Test
    public void QualifiedWSDLSoapBodyTest() throws Exception {
        ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);

        sa.generateMessageCatalog();
        XMLDocument doc = sa.getMessageCatalog();

        /* If wsdlsoap:body has a namespace attribute
        (like WS-I Basic Profile requires for RPC-style),
        then that is the namespace of the operation element in the SOAP message.
         */
        String template = (String) doc.evaluateExpression(
                "//*[local-name(.) = 'operation' and @name = 'op2']/*[local-name(.) = 'input']/*[local-name(.) = 'template']",
                XPathConstants.STRING);

        assertEqualXML("<o:op2 xmlns:o=\"http://op2Namespace.com\" xmlns:s=\"http://schema1Namespace.com\">" // operation name with wsdlsoap:body has a namespace (rpc wrapper)
                + "<op2In>" // part name (rpc wrapper)
                + "<data1>$op2.get(0)</data1>" // element
                + "<s:RefDataElem>$op2.get(1)</s:RefDataElem>"
                + "</op2In>"
                + "</o:op2>", template);
    }

    @Test
    public void UnqualifiedWSDLSoapBodyTest() throws Exception {
        ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);

        sa.generateMessageCatalog();
        XMLDocument doc = sa.getMessageCatalog();

        /* If wsdlsoap:body hasn't a namespace attribute
        (like WS-I Basic Profile requires for RPC-style),
        then we use as the namespace of the operation element in the SOAP message
        service's qname uri (the namespace of the wsdl taht contains the service)
         */
        String template = (String) doc.evaluateExpression(
                "//*[local-name(.) = 'operation' and @name = 'op3']/*[local-name(.) = 'input']/*[local-name(.) = 'template']",
                XPathConstants.STRING);

        assertEqualXML(
        	"<d:op3 xmlns:d=\"http://definitionsNamespace.com\" xmlns:s=\"http://schema1Namespace.com\">" // definition namespace
               + "<op3In1>" // part 1
               + "<data1>$op3.get(0).get(0)</data1>"
               + "<s:RefDataElem>$op3.get(0).get(1)</s:RefDataElem>"
               + "</op3In1>"
               + "<op3In2>" // part 2
               + "<s:RefDataElem>$op3.get(1)</s:RefDataElem>"
               + "</op3In2>"
               + "</d:op3>", template);
    }
}
