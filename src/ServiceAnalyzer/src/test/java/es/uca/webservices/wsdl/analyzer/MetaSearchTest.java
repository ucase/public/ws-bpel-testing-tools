/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.analyzer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import serviceAnalyzer.messageCatalog.TypeService;
import serviceAnalyzer.messageCatalog.TypeTemplate;
import es.uca.webservices.bpel.xml.XMLDocument;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class MetaSearchTest extends CompositionsBaseTest {

	private static final String METASEARCH_NS_URI = "http://examples.bpelunit.org/MetaSearch";

	public MetaSearchTest() throws IOException {
		super("MetaSearch", new String[] { "MetaSearch" });
	}

	@Test
	public void childElementsHaveNamespaces() throws Exception {
		ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);
		ServicesDocument sDoc = sa.generateMessageCatalog();

		// Obtain the template for the input message of the 'process' service
		// and parse it as an XML document
		final TypeService xmlService = getService(sDoc, "MetaSearch");
		final TypeTemplate xmlTemplate = xmlService
				.getPortArray(0)
				.getOperationArray(0)
				.getInput()
				.getTemplate();
		final String sTemplate = xmlTemplate.getStringValue().toString();
		XMLDocument xmldocTemplate = new XMLDocument(sTemplate);
		final Document docTemplate = xmldocTemplate.getDocument();

		// Check the root element
		final Element rootElement = docTemplate.getDocumentElement();
		assertEquals("MetaSearchProcessRequest", rootElement.getLocalName());
		assertEquals(METASEARCH_NS_URI, rootElement.getNamespaceURI());

		// Check that each child element has the right NS URI
		for (Node child = rootElement.getFirstChild(); child != null; child = child.getNextSibling()) {
			if (!(child instanceof Element)) continue;

			final Element childElement = (Element)child;
			assertEquals(METASEARCH_NS_URI, childElement.getNamespaceURI());
		}
	}

	private TypeService getService(ServicesDocument sDoc, final String serviceName)
			throws InvalidWSDLContentsException,
			MissingSchemaComponentException {
		TypeService srvMetaSearch = null;
		for (TypeService service : sDoc.getServices().getServiceArray()) {
			if (serviceName.equals(service.getName())) {
				srvMetaSearch = service;
			}
		}
		assertNotNull(srvMetaSearch);
		return srvMetaSearch;
	}
}
