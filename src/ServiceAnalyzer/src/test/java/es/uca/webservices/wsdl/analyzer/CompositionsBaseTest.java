/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.analyzer;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public abstract class CompositionsBaseTest {

    private final File compositionsFolder = new File("Compositions");
    protected File resourceFolder;
    protected final String[] wsdlFiles;
    protected SchemasAnalyzer types;

    public CompositionsBaseTest(String resourceFolder, String... wsdlFiles) throws IOException {
        this.resourceFolder = new File(resourceFolder);

        this.wsdlFiles = new String[wsdlFiles.length];
        for (int i = 0; i < wsdlFiles.length; ++i) {
            this.wsdlFiles[i] = getWSDLPath(new File(wsdlFiles[i]));
        }
    }

    private String getWSDLPath(File resourceFile) throws IOException {
        String url = getClass().getResource(
                "/" + compositionsFolder.getName()
                + "/" + resourceFolder.getName()
                + "/" + resourceFile.getName()
                + ".wsdl").toString();
        return url;
    }

    @Test
    public void MessageCatalogIsGeneratedCorrectly() throws Exception {
        final ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);
        sa.generateMessageCatalog();
        sa.printMessageCatalog();
    }
}
