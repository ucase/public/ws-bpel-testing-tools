/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.visitor;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.SchemaType;
import org.junit.Test;

import serviceAnalyzer.messageCatalog.TypeGA;
import serviceAnalyzer.messageCatalog.TypeGA.Enum;
import serviceAnalyzer.messageCatalog.TypeTypedef;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.wsdl.ast.ASTNode;
import es.uca.webservices.wsdl.ast.AttrNode;
import es.uca.webservices.wsdl.ast.ElemNode;
import es.uca.webservices.wsdl.ast.ForeachNode;
import es.uca.webservices.wsdl.ast.ForeachNode.TYPE;
import es.uca.webservices.wsdl.ast.ListNode;
import es.uca.webservices.wsdl.ast.Parser;
import es.uca.webservices.wsdl.ast.Parser.EncodingStyle;
import es.uca.webservices.wsdl.ast.RootNode;
import es.uca.webservices.wsdl.ast.VariableNode;
import es.uca.webservices.wsdl.ast.utils.TypedefUtils;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class ASTTest extends VisitorsBaseTest {

    private static final String NAMESPACE = "http://ServiceAnalyzerTests/Mix";

    public ASTTest() {
        super("Mix");
    }

    private void ASTDualTest(QName element, RootNode rootResult) throws Exception {
        DocumentStyleTest(element, rootResult);
        QName type = new QName(element.getNamespaceURI(), element.getLocalPart().substring(1));
        RPCStyleTest(type, element, rootResult);
    }

    private void DocumentStyleTest(QName element, RootNode docRoot) throws Exception {
        SchemaType schemaType = types.findGlobalElement(element);
        Parser parser = new Parser(element, schemaType, EncodingStyle.DOCUMENT);
        assertEquals("Dcoument style is being correctly applied", docRoot, parser.getRoot());
    }

    private void RPCStyleTest(QName type, QName wrapper, RootNode docRoot) throws Exception {
        Parser parser;
        if ("http://www.w3.org/2001/XMLSchema".equals(type.getNamespaceURI())) {
            parser = new Parser(wrapper, type, EncodingStyle.RPC);
        } else {
            SchemaType schemaType = types.findGlobalType(type);
            parser = new Parser(wrapper, schemaType, EncodingStyle.RPC);
        }
        assertEquals("RPC style is being correctly applied", docRoot, parser.getRoot());
    }

////////////////////////////////////////////////////////////////////////////////

    @Test
    public void BooleanIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "typeBoolean"),
                type = new QName("http://www.w3.org/2001/XMLSchema", "boolean");

        TypeTypedef typedef = TypeTypedef.Factory.newInstance();
        typedef = TypeTypedef.Factory.newInstance();
        typedef.setName(null);
        typedef.setType(TypeGA.STRING);
        typedef.setValues(TypedefUtils.getValuesString(new String[] {"true", "false"}));
        RootNode result = new RootNode(element, null, new VariableNode(new Pair<Enum, TypeTypedef>(null, typedef)));

        DocumentStyleTest(element, result);
        RPCStyleTest(type, element, result);
    }

    @Test
    public void IntegerIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "typeDecimal_integer"),
                type = new QName("http://www.w3.org/2001/XMLSchema", "integer");
        
        RootNode result = new RootNode(element, null, new VariableNode(new Pair<Enum, TypeTypedef>(TypeGA.INT, null)));
        DocumentStyleTest(element, result);
        RPCStyleTest(type, element, result);
    }

    @Test
    public void NMTOKENSIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "typeString_NMTOKENS"),
                type = new QName("http://www.w3.org/2001/XMLSchema", "NMTOKENS");

        TypeTypedef typedef = TypeTypedef.Factory.newInstance();
        typedef.setType(TypeGA.STRING);
        typedef.setPattern("\\c+");
        RootNode result = new RootNode(element, null,
                new ForeachNode(new VariableNode(new Pair<Enum, TypeTypedef>(null, typedef)), TYPE.LIST, BigInteger.ONE, null, null, null));
        DocumentStyleTest(element, result);
        RPCStyleTest(type, element, result);
    }

    @Test
    public void ListIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "est9");

        Pair<TypeGA.Enum, TypeTypedef> type = new Pair<Enum, TypeTypedef>(TypeGA.INT, null);
        RootNode result = new RootNode(element, null,
                new ForeachNode(new VariableNode(type), TYPE.LIST, null, null, null, null));
        ASTDualTest(element, result);
    }

    @Test
    public void List2IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "est10");

        Pair<TypeGA.Enum, TypeTypedef> type = new Pair<Enum, TypeTypedef>(TypeGA.INT, null);
        RootNode result = new RootNode(element, null,
                new ForeachNode(new VariableNode(type), TYPE.LIST, BigInteger.valueOf(40), BigInteger.valueOf(40), null, null));
        ASTDualTest(element, result);
    }

    @Test
    public void List3IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "est11");

        Pair<TypeGA.Enum, TypeTypedef> type = new Pair<Enum, TypeTypedef>(null, null);
        TypeTypedef typedef = TypeTypedef.Factory.newInstance();
        typedef = TypeTypedef.Factory.newInstance();
        typedef.setName(null);
        typedef.setType(TypeGA.STRING);
        typedef.setValues(TypedefUtils.getValuesString(new String[] {"bad","good"}));
        typedef.setMin("3");
        type.setRight(typedef);
        RootNode result = new RootNode(element, null,
                new ForeachNode(new VariableNode(type), TYPE.LIST, null, null, null, null));
        ASTDualTest(element, result);
    }

    @Test
    public void List4IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "est12");

        Pair<TypeGA.Enum, TypeTypedef> type = new Pair<Enum, TypeTypedef>(null, null);
        TypeTypedef typedef = TypeTypedef.Factory.newInstance();
        typedef = TypeTypedef.Factory.newInstance();
        typedef.setName(null);
        typedef.setType(TypeGA.STRING);
        typedef.setValues(TypedefUtils.getValuesString(new String[] {"bad","good"}));
        typedef.setMin("3");
        type.setRight(typedef);
        RootNode result = new RootNode(element, null,
                new ForeachNode(new VariableNode(type), TYPE.LIST, null, BigInteger.valueOf(5), null, null));
        ASTDualTest(element, result);
    }

    @Test
    public void Sequence1IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "esq1");

        Pair<TypeGA.Enum, TypeTypedef> type = new Pair<Enum, TypeTypedef>(TypeGA.STRING, null);
        RootNode result = new RootNode(element, null,
                new ElemNode(new QName("one"), null, new VariableNode(type)));
        ASTDualTest(element, result);
    }

    @Test
    public void Sequence2IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "esq1_1");

        Pair<TypeGA.Enum, TypeTypedef> type = new Pair<Enum, TypeTypedef>(TypeGA.STRING, null);
        List<ASTNode> nodes = new ArrayList<ASTNode>(2);
        nodes.add(new ElemNode(new QName("one"), null, new VariableNode(type)));
        nodes.add(new ElemNode(new QName("two"), null, new VariableNode(type)));
        RootNode result = new RootNode(element, null, new ListNode(nodes, true));
        ASTDualTest(element, result);
    }

    @Test
    public void Sequence3IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "esq1_2");

        Pair<TypeGA.Enum, TypeTypedef> type = new Pair<Enum, TypeTypedef>(TypeGA.STRING, null);
        List<ASTNode> nodes2 = new ArrayList<ASTNode>(2);
        nodes2.add(new ElemNode(new QName("one"), null, new VariableNode(type)));
        nodes2.add(new ElemNode(new QName("two"), null, new VariableNode(type)));
        ElemNode elem2 = new ElemNode(new QName("TWO"), null, new ListNode(
                nodes2, true));

        List<ASTNode> nodes = new ArrayList<ASTNode>(2);
        nodes.add(new ElemNode(new QName("ONE"), null, new VariableNode(type)));
        nodes.add(elem2);
        RootNode result = new RootNode(element, null, new ListNode(nodes, true));
        ASTDualTest(element, result);
    }

    @Test
    public void Sequence4IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "esq2");

        SchemaType elem = types.findGlobalElement(element);
        Parser parser = new Parser(element, elem, EncodingStyle.DOCUMENT);

        RootNode result = new RootNode(element, null, new ForeachNode(
                new ElemNode(new QName("one"), null, new VariableNode(new Pair<Enum, TypeTypedef>(TypeGA.STRING, null))),
                TYPE.OCCURS, BigInteger.ZERO, BigInteger.ONE, null, null));
        assertEquals(parser.getRoot(), result);

        QName type = new QName(NAMESPACE, "sq2");
        RPCStyleTest(type, element, result);

        QName element2 = new QName(NAMESPACE, "esq2_1");

        SchemaType elem2 = types.findGlobalElement(element2);
        Parser parser2 = new Parser(element2, elem2, EncodingStyle.DOCUMENT);
        assertEquals(parser.getRoot().getContents(), parser2.getRoot().getContents());
    }

    @Test
    public void Sequence5IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "esq3");

        SchemaType elem = types.findGlobalElement(element);
        Parser parser = new Parser(element, elem, EncodingStyle.DOCUMENT);

        RootNode result = new RootNode(element, null, new ForeachNode(
                new ElemNode(new QName("one"), null, new VariableNode(new Pair<Enum, TypeTypedef>(TypeGA.STRING, null))),
                TYPE.OCCURS, BigInteger.ZERO, null, null, null));
        assertEquals(parser.getRoot(), result);

        QName type = new QName(NAMESPACE, "sq3");
        RPCStyleTest(type, element, result);

        QName element2 = new QName(NAMESPACE, "esq3_1");

        SchemaType elem2 = types.findGlobalElement(element);
        Parser parser2 = new Parser(element2, elem2, EncodingStyle.DOCUMENT);
        assertEquals(parser.getRoot().getContents(), parser2.getRoot().getContents());
    }

    @Test
    public void Sequence6IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "esq4");

        List<ASTNode> nodes = new ArrayList<ASTNode>(2);
        nodes.add(new ElemNode(new QName("one"), null, new VariableNode(new Pair<Enum, TypeTypedef>(TypeGA.STRING, null))));
        nodes.add(new ElemNode(new QName("two"), null, new VariableNode(new Pair<Enum, TypeTypedef>(TypeGA.FLOAT, null))));
        RootNode result = new RootNode(element, null,
                new ForeachNode(new ListNode(nodes, true), TYPE.OCCURS, BigInteger.ZERO, null, null, null));
        ASTDualTest(element, result);
    }

    @Test
    public void Attr1IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "eat1");

        RootNode result = new RootNode(element,
                new AttrNode(new QName("total"),
                new VariableNode(new Pair<Enum, TypeTypedef>(TypeGA.FLOAT, null)),
                AttrNode.AVT.NONE, null, false),
                null);
        ASTDualTest(element, result);
    }

    @Test
    public void Attr2IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "eat2");

        List<ASTNode> nodes = new ArrayList<ASTNode>(3);
        nodes.add(new AttrNode(new QName("total"),
                new VariableNode(new Pair<Enum, TypeTypedef>(TypeGA.FLOAT, null)),
                AttrNode.AVT.NONE, null, false));
        nodes.add(new AttrNode(new QName("total1"),
                new VariableNode(new Pair<Enum, TypeTypedef>(TypeGA.FLOAT, null)),
                AttrNode.AVT.FIXED, "100", true));
        nodes.add(new AttrNode(new QName("total2"),
                new VariableNode(new Pair<Enum, TypeTypedef>(TypeGA.FLOAT, null)),
                AttrNode.AVT.DEFAULT, "200", true));
        RootNode result = new RootNode(element, new ListNode(nodes, true), null);
        ASTDualTest(element, result);
    }

    @Test
    public void Attr3IsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "eat3");

        RootNode result = new RootNode(element,
                new AttrNode(new QName("total"),
                new VariableNode(new Pair<Enum, TypeTypedef>(TypeGA.INT, null)),
                AttrNode.AVT.NONE, null, false),
                new VariableNode(new Pair<Enum, TypeTypedef>(TypeGA.STRING, null)));
        ASTDualTest(element, result);
    }
}
