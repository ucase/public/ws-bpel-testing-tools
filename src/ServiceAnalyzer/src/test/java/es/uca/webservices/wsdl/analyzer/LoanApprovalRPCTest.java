/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.analyzer;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import serviceAnalyzer.messageCatalog.TypeInput;
import serviceAnalyzer.messageCatalog.TypeOperation;
import serviceAnalyzer.messageCatalog.TypePort;
import serviceAnalyzer.messageCatalog.TypeService;
import serviceAnalyzer.messageCatalog.TypeVariable;

/**
 *
 * @author Cristina Jiménez-Gavilán (cristina.jimenezgavilan@alum.uca.es), Antonio García-Domínguez (antonio.garciadominguez@uca.es)
 */
public class LoanApprovalRPCTest extends CompositionsBaseTest {

    public LoanApprovalRPCTest() throws IOException {
        super("LoanApprovalRPC", new String[] {"LoanService", "ApprovalService", "AssessorService"});
    }

    @Test
    public void predefinedVariableNamesAreAvoided() throws Exception {
        final ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);
        final ServicesDocument catalog = sa.generateMessageCatalog();

        boolean bFound = false;
        for (TypeService service : catalog.getServices().getServiceArray()) {
        	if ("LoanService".equals(service.getName())) {
        		bFound = true;
        	}
        	else continue;

        	final TypePort port = service.getPortArray()[0];
        	final TypeOperation op = port.getOperationArray()[0];
			final TypeInput input = op.getInput();

			boolean bHasRenamed = false;
        	for (TypeVariable tv : input.getDecls().getVariableArray()) {
        		assertFalse("request".equals(tv.getName()));
        		bHasRenamed = "request0".equals(tv.getName());
        		if (bHasRenamed) {
        			break;
        		}
        	}
        	assertTrue(bHasRenamed);

        	final String sTemplate = input.getTemplate().getStringValue();
        	assertFalse(sTemplate.contains("$request."));
        	assertTrue(sTemplate.contains("$request0."));
        }
        assertTrue(bFound);
    }
}
