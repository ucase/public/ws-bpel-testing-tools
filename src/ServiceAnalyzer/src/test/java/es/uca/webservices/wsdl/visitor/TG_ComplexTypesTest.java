/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.visitor;

import static es.uca.webservices.wsdl.test.TestUtil.assertEqualXML;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.xml.namespace.QName;

import org.junit.Test;

import es.uca.webservices.wsdl.ast.Parser.EncodingStyle;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class TG_ComplexTypesTest extends TemplateGenBaseTest {

    private static final String NAMESPACE = "http://ServiceAnalyzerTests/ComplexTypes";

    public TG_ComplexTypesTest() {
        super("ComplexTypes");
    }

    @Test
    public void SequenceOneElementTest() throws Exception {
        QName element = new QName(NAMESPACE, "esq1");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:esq1 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"><one>$esq1</one></ns1:esq1>",
                template);

        assertVelocityResultIsCorrect(template,
                "#set($esq1='one')",
                "<ns1:esq1 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"><one>one</one></ns1:esq1>");
    }

    @Test
    public void SequenceTwoElementsTest() throws Exception {
        QName element = new QName(NAMESPACE, "esq1_1");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:esq1_1 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"><one>$esq1_1.get(0)</one><two>$esq1_1.get(1)</two></ns1:esq1_1>",
                template);

        assertVelocityResultIsCorrect(template,
                "#set($esq1_1=['one', 'two'])",
                "<ns1:esq1_1 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"><one>one</one><two>two</two></ns1:esq1_1>");
    }

    @Test
    public void SequenceRecursiveTest() throws Exception {
        QName element = new QName(NAMESPACE, "esq1_2");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:esq1_2 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"><ONE>$esq1_2.get(0)</ONE><TWO><one>$esq1_2.get(1).get(0)</one><two>$esq1_2.get(1).get(1)</two></TWO></ns1:esq1_2>",
                template);

        assertVelocityResultIsCorrect(template,
                "#set($esq1_2=['ONE', ['one', 'two']])",
                "<ns1:esq1_2 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"><ONE>ONE</ONE><TWO><one>one</one><two>two</two></TWO></ns1:esq1_2>");
    }

    @Test
    public void SequenceOneRepeatedElementTest() throws Exception {
        QName element = new QName(NAMESPACE, "esq2");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:esq2 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\">#foreach($V1 in $esq2) <one>$V1</one> #end</ns1:esq2>",
                template);

        QName element2 = new QName(NAMESPACE, "esq2_1");
        String template2 = GenerateTemplate(element2, EncodingStyle.DOCUMENT);
        assertEqualXML(template2.replace("esq2_1", "esq2"), template);

        QName element3 = new QName(NAMESPACE, "esq3");
        String template3 = GenerateTemplate(element3, EncodingStyle.DOCUMENT);
        assertEqualXML(template3.replace("esq3", "esq2"), template);

        QName element4 = new QName(NAMESPACE, "esq3_1");
        String template4 = GenerateTemplate(element4, EncodingStyle.DOCUMENT);
        assertEqualXML(template4.replace("esq3_1", "esq2"), template);
    }

    @Test
    public void SequenceOneOpcionalElementTest() throws Exception {
        QName element = new QName(NAMESPACE, "esq2");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);

        assertVelocityResultIsCorrect(template,
                "#set($esq2=[])",
                "<ns1:esq2 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"></ns1:esq2>");
    }

    @Test
    public void SequenceOneMaxUnboundElementTest() throws Exception {
        QName element = new QName(NAMESPACE, "esq3");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);

        assertVelocityResultIsCorrect(template,
                "#set($esq3=['one1', 'one2', 'one3'])",
                "<ns1:esq3 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"> <one>one1</one>  <one>one2</one>  <one>one3</one> </ns1:esq3>");
    }

    @Test
    public void SequenceTwoElementsWithRepetitionsTest() throws Exception {
        QName element = new QName(NAMESPACE, "esq4");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:esq4 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\">#foreach($V1 in $esq4) <one>$V1.get(0)</one><two>$V1.get(1)</two> #end</ns1:esq4>",
                template);

        assertVelocityResultIsCorrect(template,
                "#set($esq4=[['one0', 2.0], ['one1', 2.1]])",
                "<ns1:esq4 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"> <one>one0</one><two>2.0</two>  <one>one1</one><two>2.1</two> </ns1:esq4>");
    }

    @Test
    public void ComplexSequenceTest() throws Exception {
        QName element = new QName(NAMESPACE, "esq5");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:esq5 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\">#foreach($V1 in $esq5) #foreach($V2 in $V1.get(0)) <one>#foreach($V3 in $V2) $V3 #end</one> #end#foreach($V4 in $V1.get(1)) <two>$V4</two> #end<three>$V1.get(2)</three> #end</ns1:esq5>",
                template);

        assertVelocityResultIsCorrect(template,
                "#set($v13='true')"
                + "\n#set($v12=['two'])"
                + "\n#set($v11=[[1, 2, 3], [4, 5]]) "
                + "\n#set($v1=[$v11, $v12, $v13]) "
                + "\n#set($esq5=[$v1])",
                "<ns1:esq5 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\">  <one> 1  2  3 </one>  <one> 4  5 </one>  <two>two</two> <three>true</three> </ns1:esq5>");
    }

    @Test
    public void ComplexIterativeSequenceTest() throws Exception {
        QName element = new QName(NAMESPACE, "esq6");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:esq6 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\">#foreach($V1 in $esq6) <iterative>#foreach($V2 in $V1) #foreach($V3 in $V2.get(0)) <one>#foreach($V4 in $V3) $V4 #end</one> #end#foreach($V5 in $V2.get(1)) <two>$V5</two> #end<three>$V2.get(2)</three> #end</iterative> #end</ns1:esq6>",
                template);

        assertVelocityResultIsCorrect(template,
                "#set($v13='true')"
                + "\n#set($v12=['two'])"
                + "\n#set($v11=[[1, 2, 3], [4, 5]])"
                + "\n#set($v1=[$v11, $v12, $v13])"
                + "\n#set($esq5=[$v1])"
                + "\n#set($esq6=[$esq5])",
                "<ns1:esq6 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"> <iterative>  <one> 1  2  3 </one>  <one> 4  5 </one>  <two>two</two> <three>true</three> </iterative> </ns1:esq6>");
    }

    @Test
    public void singleAttribute() throws Exception {
        QName element = new QName(NAMESPACE, "eat1");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);

        assertEquals("<c:eat1 xmlns:c=\"http://ServiceAnalyzerTests/ComplexTypes\" #foreach($V1 in $eat1) total = \"$V1\" #end />", template);

        assertVelocityResultIsCorrect(template,
                "#set($eat1='retnull')",
                "<ns1:eat1 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\" />");
        assertVelocityResultIsCorrect(template,
                "#set($eat1=[100])",
                "<ns1:eat1 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"  total=\"100\" />");
    }

    @Test
    public void fixedAttribute() throws Exception {
        QName element = new QName(NAMESPACE, "eat2");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);

        assertEquals(
        	"<c:eat2 xmlns:c=\"http://ServiceAnalyzerTests/ComplexTypes\" total = \"$eat2.get(0)\" #foreach($V1 in $eat2.get(1)) total2 = \"$V1\" #end />",
            template);
       
        assertVelocityResultIsCorrect(template,
                "#set($eat2=[20.50, [26.99]])",
                "<ns1:eat2 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\" total=\"20.5\" total2=\"26.99\" />");
    }

    @Test
    public void AttributeInExtensionTest() throws Exception {
        QName element = new QName(NAMESPACE, "eat3");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEquals(
        	"<c:eat3 xmlns:c=\"http://ServiceAnalyzerTests/ComplexTypes\" #foreach($V1 in $eat3.get(0)) total = \"$V1\" #end>$eat3.get(1)</c:eat3>",
            template);

        assertVelocityResultIsCorrect(template,
                "#set($eat3=[[20], 'test'])",
                "<ns1:eat3 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\" total = \"20\">test</ns1:eat3>");
    }

    @Test
    public void ComplexTemplateTest() throws Exception {
        QName element = new QName(NAMESPACE, "eat4");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEquals(
           "<c:eat4 xmlns:c=\"http://ServiceAnalyzerTests/ComplexTypes\" " +
           "#foreach($V1 in $eat4.get(0)) pin = \"#foreach($V2 in $V1) $V2 #end\" #end " +
           "#foreach($V3 in $eat4.get(1)) total = \"$V3\" #end>" +
           "#foreach($V4 in $eat4.get(2))" +
           " #foreach($V5 in $V4.get(0))" +
           " <one #foreach($V6 in $V5.get(0)) total = \"$V6\" #end>$V5.get(1)</one>" +
           " #end" +
            "#foreach($V7 in $V4.get(1)) <two>$V7</two> #end" +
            "<three>$V4.get(2)</three> " +
           "#end" +
           "</c:eat4>",
           template);

        assertVelocityResultIsCorrect(template,
                "#set($eat4=[[[1,2,3]], [20.50], [[[[[100], 'one1'], [[], 'one2'], [[300], 'one3']], ['two'], 'false']]])",
                "<ns1:eat4 xmlns:ns1=\"http://ServiceAnalyzerTests/ComplexTypes\"  pin = \"1 2 3\"   total = \"20.5\">" +
                "  <one total=\"100\">one1</one>" +
                " <one>one2</one>" +
                "  <one total=\"300\">one3</one>" +
                "  <two>two</two>" +
                " <three>false</three>" +
                " </ns1:eat4>");
    }

    @Test
    public void AllTest() throws Exception {
    	QName element = new QName(NAMESPACE, "eall");
    	String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
    	assertEquals(
    		"<c:eall xmlns:c=\"http://ServiceAnalyzerTests/ComplexTypes\">"
   			+ "<one>$eall.get(0)</one>"
    		+ "<two>$eall.get(1)</two>"
   			+ "</c:eall>",
    		template);
    }

    @Test
    public void ChoiceTest() throws Exception {
    	QName element = new QName(NAMESPACE, "echo");
    	String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
    	assertEquals(
    		"<c:echo xmlns:c=\"http://ServiceAnalyzerTests/ComplexTypes\">"
    		+ " #if( $echo.get(0) == 1 ) "
    		+ "<one>$echo.get(1)</one>"
    		+ " #else "
    		+ "<two>$echo.get(2)</two>"
    		+ " #end "
    		+ "</c:echo>",
    		template);
    }

    @Test
    public void ChoiceThreeTest() throws Exception {
    	QName element = new QName(NAMESPACE, "echo2");
    	String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
		assertEquals(
				"<c:echo2 xmlns:c=\"http://ServiceAnalyzerTests/ComplexTypes\">"
						+ "#foreach($V1 in $echo2)"
						+ "  #if( $V1.get(0) == 1 )"
						+ " <one>$V1.get(1)</one>"
						+ " #elseif( $V1.get(0) == 2 )"
						+ " <two>$V1.get(2)</two>"
						+ " #else "
						+ "#foreach($V2 in $V1.get(3))"
						+ " <pair><a>$V2.get(0)</a><b>$V2.get(1)</b></pair> "
						+ "#end"
						+ " #end"
						+ "  #end"
						+ "</c:echo2>", template);
    }

    @Test
    public void WildcardTest() throws Exception {
    	QName element = new QName(NAMESPACE, "eany");
    	String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
    	assertTrue("Template starts with eany", template.startsWith("<c:eany "));
    	assertTrue("Template ends with eany", template.endsWith("</c:eany>"));
    	assertTrue("Template contains messageCatalog xmlns",
                template.contains("xmlns:m=\"http://serviceAnalyzer/messageCatalog\""));
    	assertTrue("Template contains ComplexTypes xmlns",
                template.contains("xmlns:c=\"http://ServiceAnalyzerTests/ComplexTypes\""));
    }
}
