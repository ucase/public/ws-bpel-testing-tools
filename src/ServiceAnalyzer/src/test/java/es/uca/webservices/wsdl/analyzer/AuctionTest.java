/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.analyzer;

import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class AuctionTest extends CompositionsBaseTest {

    public AuctionTest() throws IOException {
        super("Auction", new String[]{"auctionServiceInterface"});
    }

    @Override
    @Test(expected = es.uca.webservices.wsdl.analyzer.InvalidWSDLContentsException.class)
    public void MessageCatalogIsGeneratedCorrectly() throws Exception {
        ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);

        sa.generateMessageCatalog();
    }
}
