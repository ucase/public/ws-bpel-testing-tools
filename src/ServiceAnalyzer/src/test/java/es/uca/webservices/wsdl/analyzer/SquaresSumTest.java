/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.analyzer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import serviceAnalyzer.messageCatalog.*;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class SquaresSumTest extends CompositionsBaseTest {

    public SquaresSumTest() throws IOException {
        super("SquaresSum", new String[]{"squaresSum"});
    }

    @Test
    public void portsHaveAddresses() throws Exception {
        final ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);
        final ServicesDocument catalog = sa.generateMessageCatalog();
        final TypePort port = catalog.getServices().getServiceArray(0).getPortArray(0);
        assertEquals("http://localhost:8080/active-bpel/services/squaresSumService", port.getAddress());
    }

    @Test
    public void typedefRenamesArePropagatedToListTypes() throws Exception {
        ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);
        ServicesDocument catalog = sa.generateMessageCatalog();
        TypeDecls decls = catalog.getServices()
        		.getServiceArray(0).getPortArray(0)
        		.getOperationArray(0).getOutput()
        		.getDecls();

    	TypeTypedef typedefDocument = findTypedefByName("TSquaresSumResponse", decls);
        assertTrue("The TSquaresSumResponse typedef exists", typedefDocument != null);

        final String[] elements = typedefDocument.getElement().split(" *, *");
        assertEquals("TSquaresSumResponse should be a pair", 2, elements.length);
        for (String e : elements) {
        	assertTrue("All element types should be defined", findTypedefByName(e, decls) != null);
        }
    }

	private TypeTypedef findTypedefByName(final String name, TypeDecls decls) {
		TypeTypedef typedefDocument = null;
        for (TypeTypedef typedef : decls.getTypedefArray()) {
			if (name.equals(typedef.getName())) {
        		typedefDocument = typedef; 
        	}
        }
		return typedefDocument;
	}
}
