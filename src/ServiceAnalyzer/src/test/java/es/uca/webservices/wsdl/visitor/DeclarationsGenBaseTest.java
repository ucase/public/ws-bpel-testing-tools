/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.visitor;

import serviceAnalyzer.messageCatalog.TypeDecls;
import es.uca.webservices.wsdl.ast.utils.TypedefUtils;
import es.uca.webservices.wsdl.ast.Parser.EncodingStyle;
import javax.xml.xpath.XPathConstants;

import es.uca.webservices.bpel.xml.XMLDocument;
import es.uca.webservices.wsdl.ast.Parser;
import es.uca.webservices.wsdl.ast.RootNode;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlOptions;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public abstract class DeclarationsGenBaseTest extends VisitorsBaseTest {

    public enum RESTRICTIONS {

        MIN, MAX, VALUES, PATTERN, FRACTION_DIGITS, TOTAL_DIGITS;

        @Override
        public String toString() {
            switch (valueOf(this.getDeclaringClass(), this.name())) {
                case MIN:
                    return "min";
                case MAX:
                    return "max";
                case VALUES:
                    return "values";
                case PATTERN:
                    return "pattern";
                case FRACTION_DIGITS:
                    return "fractionDigits";
                case TOTAL_DIGITS:
                    return "totalDigits";
                default:
                    return null;
            }
        }
    };

    public DeclarationsGenBaseTest(String resourceFile) {
        super(resourceFile);
    }

    protected TypeDecls GenerateDeclarations(QName element, EncodingStyle style) throws Exception {
        Parser parser = null;
        if (style == EncodingStyle.DOCUMENT) {
            SchemaType elem = types.findGlobalElement(element);
            parser = new Parser(element, elem, style);
        } else {
            QName envelopment = new QName("http://ServiceAnalyzerTests/TypesCorrespondence/envelop", "type_" + element.getLocalPart());
            if (element.getNamespaceURI().compareTo("http://www.w3.org/2001/XMLSchema") == 0) {
                parser = new Parser(envelopment, element, style);
            } else {
                SchemaType elem = types.findGlobalType(element);
                parser = new Parser(envelopment, elem, style);
            }
        }

        DeclarationsGenerator dg = new DeclarationsGenerator(new RootNodeVariableMapper());
        //parser.getRoot().accept(dg, null);
        dg.visit((RootNode) parser.getRoot(), null);

        return dg.getDecls();
    }

    protected XMLDocument typeDecls2XMLDocument(TypeDecls decls) throws Exception {

        XmlOptions options = new XmlOptions();
        options.setSavePrettyPrint();
        options.setSavePrettyPrintIndent(2);
        decls.save(getResultFile(), options);

        return new XMLDocument(getResultFile());
    }

    protected XMLDocument GenerateDeclarationsXML(QName element, EncodingStyle style) throws Exception {
        TypeDecls decls = GenerateDeclarations(element, style);
        return typeDecls2XMLDocument(decls);
    }

    protected XMLDocument GenDeclarationsXMLAndCompStyles(QName element) throws Exception {

        TypeDecls declsDOC = GenerateDeclarations(element, EncodingStyle.DOCUMENT),
                declsRPC = GenerateDeclarations(new QName("http://www.w3.org/2001/XMLSchema", element.getLocalPart()), EncodingStyle.RPC);

        assertSameType("", declsDOC, declsRPC);

        return typeDecls2XMLDocument(declsDOC);
    }

    public void assertOnlyAVariable(String message, XMLDocument declarationsXMLDoc, String vType) throws Exception {
        assertTrue(message,
                (Boolean) declarationsXMLDoc.evaluateExpression(
                "count(/*[local-name(.) = 'variable']) = 1 "
                + "and count(/*[local-name(.) = 'typedef']) = 0 ",
                XPathConstants.BOOLEAN));
        assertEquals(message,
                (String) declarationsXMLDoc.evaluateExpression(
                "/*[local-name(.) = 'variable']/@type"),
                vType);
    }

    public void assertOnlyOneVariableAndHisTypedef(String message, XMLDocument declarationsXMLDoc,
            String tType, String[] tElements, String tMin, String tMax, String[] tValues,
            String tFractionDigits, String tTotalDigits, String tPattern) throws Exception {
        String typedefName = (String) declarationsXMLDoc.evaluateExpression(
                "//*[local-name(.) = 'typedef']/@name",
                XPathConstants.STRING);

        assertTrue(message,
                (Boolean) declarationsXMLDoc.evaluateExpression(
                "count(//*[local-name(.) = 'typedef']) = 1 "
                + "and count(//*[local-name(.) = 'variable']) = 1 "
                + "and count(//*[local-name(.) = 'variable' and @type='"
                + typedefName + "']) = 1",
                XPathConstants.BOOLEAN));

        assertExistsTypedef(message, declarationsXMLDoc,
                typedefName, tType, tElements,
                tMin, tMax, tValues,
                tFractionDigits, tTotalDigits, tPattern);
    }

    public void assertExistsTypedef(String message, XMLDocument declarationsXMLDoc,
            String tName, String tType, String[] tElements,
            String tMin, String tMax, String[] tValues,
            String tFractionDigits, String tTotalDigits, String tPattern) throws Exception {

        assertTrue(message,
                (Boolean) declarationsXMLDoc.evaluateExpression(
                "count(//*[local-name(.) = 'typedef' "
                + "and @name='" + tName + "' "
                + "and @type='" + tType + "']) = 1 ",
                XPathConstants.BOOLEAN));

        if (tElements == null) {
            assertTrue(message,
                    (Boolean) declarationsXMLDoc.evaluateExpression(
                    "count(//*[local-name(.) = 'typedef' and @name='" + tName + "']/@element) = 0 ",
                    XPathConstants.BOOLEAN));
        } else {
            String elementsList = (String) declarationsXMLDoc.evaluateExpression(
                    "//*[local-name(.) = 'typedef' and @name='" + tName + "']/@element");

            assertTrue(message, TypedefUtils.equalsElements(TypedefUtils.getElementsArray(elementsList), tElements));
        }
        if (tMin == null) {
            assertTrue("No debería tener mínimo",
                    (Boolean) declarationsXMLDoc.evaluateExpression(
                    "count(//*[local-name(.) = 'typedef' and @name='" + tName + "']/@min) = 0 ",
                    XPathConstants.BOOLEAN));
        } else {
            assertEquals("Debería tener mínimo " + tMin,
                    (String) declarationsXMLDoc.evaluateExpression(
                    "//*[local-name(.) = 'typedef' and @name='" + tName + "']/@min"),
                    tMin);
        }
        if (tMax == null) {
            assertTrue(message,
                    (Boolean) declarationsXMLDoc.evaluateExpression(
                    "count(//*[local-name(.) = 'typedef' and @name='" + tName + "']/@max) = 0 ",
                    XPathConstants.BOOLEAN));
        } else {
            assertEquals(message,
                    (String) declarationsXMLDoc.evaluateExpression(
                    "//*[local-name(.) = 'typedef' and @name='" + tName + "']/@max"),
                    tMax);
        }
        if (tValues == null) {
            assertTrue(message,
                    (Boolean) declarationsXMLDoc.evaluateExpression(
                    "count(//*[local-name(.) = 'typedef' and @name='" + tName + "']/@values) = 0 ",
                    XPathConstants.BOOLEAN));
        } else {
            String valuesList = (String) declarationsXMLDoc.evaluateExpression(
                    "//*[local-name(.) = 'typedef' and @name='" + tName + "']/@values");
            assertTrue(message, TypedefUtils.equalsValues(TypedefUtils.getValuesArray(valuesList), tValues));
        }
        if (tFractionDigits == null) {
            assertTrue(message,
                    (Boolean) declarationsXMLDoc.evaluateExpression(
                    "count(//*[local-name(.) = 'typedef' and @name='" + tName + "']/@fractionDigits) = 0 ",
                    XPathConstants.BOOLEAN));
        } else {
            assertEquals(message,
                    (String) declarationsXMLDoc.evaluateExpression(
                    "//*[local-name(.) = 'typedef' and @name='" + tName + "']/@fractionDigits"),
                    tFractionDigits);
        }
        if (tTotalDigits == null) {
            assertTrue(message,
                    (Boolean) declarationsXMLDoc.evaluateExpression(
                    "count(//*[local-name(.) = 'typedef' and @name='" + tName + "']/@totalDigits) = 0 ",
                    XPathConstants.BOOLEAN));
        } else {
            assertEquals(message,
                    (String) declarationsXMLDoc.evaluateExpression(
                    "//*[local-name(.) = 'typedef' and @name='" + tName + "']/@totalDigits"),
                    tTotalDigits);
        }
        if (tPattern == null) {
            assertTrue(message,
                    (Boolean) declarationsXMLDoc.evaluateExpression(
                    "count(//*[local-name(.) = 'typedef' and @name='" + tName + "']/@pattern) = 0 ",
                    XPathConstants.BOOLEAN));
        } else {
            assertEquals(message,
                    (String) declarationsXMLDoc.evaluateExpression(
                    "//*[local-name(.) = 'typedef' and @name='" + tName + "']/@pattern"),
                    tPattern);
        }
    }

    public void assertTypedefHasRestricction(String message, XMLDocument declarationsXMLDoc,
            RESTRICTIONS res, String value) throws Exception {

        assertTrue("Debería haber un único typedef",
                (Boolean) declarationsXMLDoc.evaluateExpression(
                "count(//*[local-name(.) = 'typedef']) = 1 ", XPathConstants.BOOLEAN));

        assertEquals(message,
                (String) declarationsXMLDoc.evaluateExpression(
                "//*[local-name(.) = 'typedef']/@"
                + res), value);
    }

    public void assertTypedefHasRestricction(String message, XMLDocument declarationsXMLDoc,
            String tName, RESTRICTIONS res, String value) throws Exception {
        assertEquals(message,
                (String) declarationsXMLDoc.evaluateExpression(
                "//*[local-name(.) = 'typedef' and @name='" + tName + "']/@" 
                + res), value);
    }

    public void assertSameType(String message, TypeDecls decls1, TypeDecls decls2) throws Exception {
      int numVariables = decls1.sizeOfVariableArray(),
      numTypedefs = decls1.sizeOfTypedefArray();
      
      assertEquals("", numVariables, decls2.sizeOfVariableArray());
      assertEquals("", numTypedefs, decls2.sizeOfTypedefArray());
      for (int i = 0; i < numVariables; ++i) {
        assertEquals("", decls1.getVariableArray(i).getType(), decls1.getVariableArray(i).getType());
      }
      for (int i = 0; i < numTypedefs; ++i) {
        assertTrue("", TypedefUtils.equals(decls1.getTypedefArray(i), decls1.getTypedefArray(i)));
      }
    }
}
