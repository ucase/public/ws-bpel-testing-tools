/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.visitor;

import es.uca.webservices.wsdl.analyzer.SchemasAnalyzer;
import java.io.File;
import java.io.IOException;
import org.junit.Before;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public abstract class VisitorsBaseTest {

    private final File resourceFolder = new File("Schemas");
    protected final File resourceFile;
    protected SchemasAnalyzer types;

    public VisitorsBaseTest(String resourceFile) {
        this.resourceFile = new File(resourceFile);
    }

    public File getXSDFile() throws IOException {

        return new File(getClass().getResource(
                "/" + resourceFolder.getName()
                + "/" + resourceFile.getName()
                + ".xsd").getFile());
    }

    public File getResultFile() throws IOException {
        return new File(getXSDFile().getCanonicalPath() + ".xml");
    }

    @Before
    public void setUp() throws Exception {
        File XSDFile = getXSDFile();
        this.types = new SchemasAnalyzer(XSDFile);
    }
}
