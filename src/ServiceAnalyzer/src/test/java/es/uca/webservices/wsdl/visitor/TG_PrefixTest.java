/*
 *  Copyright 2011 Antonio García-Domínguez (antonio.garciadominguez@uca.es)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.visitor;

import static org.junit.Assert.assertEquals;

import javax.xml.namespace.QName;

import org.junit.Test;

import es.uca.webservices.wsdl.ast.Parser.EncodingStyle;

/**
 * Checks for the generated namespace prefixes.
 * @author Antonio García-Domínguez
 */
public class TG_PrefixTest extends TemplateGenBaseTest {

    private static final String NAMESPACE = "http://www.example.org/prefix-cannot-start-with-number/2011";

	public TG_PrefixTest() {
		super("Prefixes");
	}

	@Test
	public void prefixCannotStartWithNumber() throws Exception {
        QName element = new QName(NAMESPACE, "elem");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEquals("<ns2:elem xmlns:ns2=\"" + NAMESPACE + "\">$elem</ns2:elem>", template);
	}
}
