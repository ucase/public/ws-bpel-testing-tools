/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.visitor;

import static es.uca.webservices.wsdl.test.TestUtil.assertEqualXML;

import javax.xml.namespace.QName;

import org.junit.Test;

import es.uca.webservices.wsdl.ast.Parser.EncodingStyle;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class TG_TypesCorrespondenceTest extends TemplateGenBaseTest {

    private static final String NAMESPACE = "http://ServiceAnalyzerTests/TypesCorrespondence";

    public TG_TypesCorrespondenceTest() {
        super("TypesCorrespondence");
    }

    @Test
    public void StringIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "string");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:string xmlns:ns1=\"http://ServiceAnalyzerTests/TypesCorrespondence\">$string</ns1:string>",
                template);
    }

    @Test
    public void BooleanIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "boolean");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:boolean xmlns:ns1=\"http://ServiceAnalyzerTests/TypesCorrespondence\">$boolean</ns1:boolean>",
                template);
    }

    @Test
    public void IDREFSIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "IDREFS");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:IDREFS xmlns:ns1=\"http://ServiceAnalyzerTests/TypesCorrespondence\">#foreach($V1 in $IDREFS) $V1 #end</ns1:IDREFS>",
                template);
    }

    @Test
    public void NMTOKENSIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "NMTOKENS");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:NMTOKENS xmlns:ns1=\"http://ServiceAnalyzerTests/TypesCorrespondence\">#foreach($V1 in $NMTOKENS) $V1 #end</ns1:NMTOKENS>",
                template);
    }

    @Test(expected = java.lang.UnsupportedOperationException.class)
    public void AnyTypeIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "anyType");
        GenerateTemplate(element, EncodingStyle.DOCUMENT);
    }
}
