/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.analyzer;


import static es.uca.webservices.wsdl.test.TestUtil.assertEqualXML;
import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.xml.xpath.XPathConstants;

import org.junit.Test;

import es.uca.webservices.bpel.xml.XMLDocument;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class SampleDOCCompositionTest extends CompositionsBaseTest {


    public SampleDOCCompositionTest() throws IOException {
        super("StyleSamples", new String[] {"SampleDOC"});
    }

    @Test
    public void EmptyMessageTest() throws Exception {
        ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);

        sa.generateMessageCatalog();
        XMLDocument doc = sa.getMessageCatalog();

        String decls = (String) doc.evaluateExpression(
                "//*[local-name(.) = 'operation' and @name = 'op1']//*[local-name(.) = 'decls']",
                XPathConstants.STRING);

        assertEquals(decls, "");

        String template = (String) doc.evaluateExpression(
                "//*[local-name(.) = 'operation' and @name = 'op1']//*[local-name(.) = 'template']",
                XPathConstants.STRING);

        assertEquals(template, "");
    }

    @Test
    public void OutputWrapperTest() throws Exception {
        ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);

        sa.generateMessageCatalog();
        XMLDocument doc = sa.getMessageCatalog();

        String template = (String) doc.evaluateExpression(
                "//*[local-name(.) = 'operation' and @name = 'op1']/*[local-name(.) = 'output']/*[local-name(.) = 'template']",
                XPathConstants.STRING);

        assertEqualXML(
                "<ns1:DataElem xmlns:ns1=\"http://schema2Namespace.com\">" // without "Response"
                + "<data1>$DataElem.get(0)</data1>"
                + "<ns2:RefDataElem xmlns:ns2=\"http://schema1Namespace.com\">$DataElem.get(1)</ns2:RefDataElem>"
                + "</ns1:DataElem>", template);
    }

    @Test
    public void QualifiedWSDLSoapBodyTest() throws Exception {
        ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);

        sa.generateMessageCatalog();
        XMLDocument doc = sa.getMessageCatalog();

        String template = (String) doc.evaluateExpression(
                "//*[local-name(.) = 'operation' and @name = 'op2']/*[local-name(.) = 'input']/*[local-name(.) = 'template']",
                XPathConstants.STRING);
        assertEqualXML(
                "<ns1:DataElem xmlns:ns1=\"http://schema2Namespace.com\">" // without "Response"
                + "<data1>$DataElem0.get(0)</data1>"
                + "<ns2:RefDataElem xmlns:ns2=\"http://schema1Namespace.com\">$DataElem0.get(1)</ns2:RefDataElem>"
                + "</ns1:DataElem>", template);
    }

    @Test
    public void UnqualifiedWSDLSoapBodyTest() throws Exception {
        ServiceAnalyzer sa = new ServiceAnalyzer(wsdlFiles);

        sa.generateMessageCatalog();
        XMLDocument doc = sa.getMessageCatalog();

        String template = (String) doc.evaluateExpression(
                "//*[local-name(.) = 'operation' and @name = 'op3']/*[local-name(.) = 'input']/*[local-name(.) = 'template']",
                XPathConstants.STRING);

        assertEqualXML(
               "<ns1:DataElem xmlns:ns1=\"http://schema2Namespace.com\">" // without "Response"
                + "<data1>$DataElem2.get(0)</data1>"
                + "<ns2:RefDataElem xmlns:ns2=\"http://schema1Namespace.com\">$DataElem2.get(1)</ns2:RefDataElem>"
                + "</ns1:DataElem>", template);
    }

}
