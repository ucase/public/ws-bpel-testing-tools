/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.visitor;

import static es.uca.webservices.wsdl.test.TestUtil.assertEqualXML;
import static org.junit.Assert.assertTrue;

import java.io.StringWriter;

import javax.xml.namespace.QName;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.xmlbeans.SchemaType;

import es.uca.webservices.wsdl.ast.Parser;
import es.uca.webservices.wsdl.ast.Parser.EncodingStyle;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public abstract class TemplateGenBaseTest extends VisitorsBaseTest {

    public TemplateGenBaseTest(String resourceFile) {
        super(resourceFile);
    }

    protected String GenerateTemplate(QName element, EncodingStyle style) throws Exception {
        Parser parser = null;
        if (style == EncodingStyle.DOCUMENT) {
            SchemaType elem = types.findGlobalElement(element);
            parser = new Parser(element, elem, style);
        } else {
            QName envelopment = new QName("http://ServiceAnalyzerTests/TypesCorrespondence/envelop", "type_" + element.getLocalPart());
            if (element.getNamespaceURI().compareTo("http://www.w3.org/2001/XMLSchema") == 0) {
                parser = new Parser(envelopment, element, style);
            } else {
                SchemaType elem = types.findGlobalType(element);
                parser = new Parser(envelopment, elem, style);
            }
        }

        TemplateGenerator tg = new TemplateGenerator(new RootNodeVariableMapper());
        String template = (String) parser.getRoot().accept(tg, null);
        assertVelocityTemplateCompileCorrectly(template);
        return template;
    }

    public void assertVelocityTemplateCompileCorrectly(String template) throws Exception {
        VelocityEngine engine = new VelocityEngine();
        engine.init();

        VelocityContext context = new VelocityContext();
        context.put("name", "");
        StringWriter writer = new StringWriter();
        engine.evaluate(context, writer, "", template);
    }

    public void assertVelocityResultIsCorrect(String template, String sets, String result) throws Exception {
        VelocityEngine engine = new VelocityEngine();
        engine.init();

        VelocityContext context = new VelocityContext();
        context.put("name", "");
        StringWriter writer = new StringWriter();
        engine.evaluate(context, writer, "", sets + "\n" + template);

        assertEqualXML(result, writer.toString());
        assertTrue(types.isValid(writer.toString()));
    }
}
