/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.visitor;

import static es.uca.webservices.wsdl.test.TestUtil.assertEqualXML;

import javax.xml.namespace.QName;

import org.junit.Test;

import es.uca.webservices.wsdl.ast.Parser.EncodingStyle;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class TG_SimpleTypesTest extends TemplateGenBaseTest {

    private static final String NAMESPACE = "http://ServiceAnalyzerTests/SimpleTypes";

    public TG_SimpleTypesTest() {
        super("SimpleTypes");
    }

    @Test
    public void PrimitiveTypeWithFacetsTest() throws Exception {
        QName element = new QName(NAMESPACE, "est1");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertEqualXML(
                "<ns1:est1 xmlns:ns1=\"http://ServiceAnalyzerTests/SimpleTypes\">$est1</ns1:est1>",
                template);
    }

    @Test
    public void ListDatatypeTest() throws Exception {
        QName element = new QName(NAMESPACE, "est9");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertVelocityResultIsCorrect(template,
                "#set($est9=[1, 2, 3])",
                "<ns1:est9 xmlns:ns1=\"http://ServiceAnalyzerTests/SimpleTypes\"> 1  2  3 </ns1:est9>");
    }

    @Test
    public void ListDatatypeTestWithFacets() throws Exception {
        QName element = new QName(NAMESPACE, "est10");
        String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
        assertVelocityResultIsCorrect(template,
                "#set($est10=[1, 2, 3, 4])",
                "<ns1:est10 xmlns:ns1=\"http://ServiceAnalyzerTests/SimpleTypes\"> 1  2  3  4 </ns1:est10>");
    }
   
    @Test
    public void UnionDatatype() throws Exception {
    	QName element = new QName(NAMESPACE, "est33");
    	String template = GenerateTemplate(element, EncodingStyle.DOCUMENT);
    	assertEqualXML("<s:est33 xmlns:s=\"http://ServiceAnalyzerTests/SimpleTypes\">" +
    			" #if( $est33.get(0) == 1 ) $est33.get(1) #else $est33.get(2) #end" +
    			" </s:est33>", template);
    }
}
