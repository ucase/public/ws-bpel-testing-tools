/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.visitor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathConstants;

import org.junit.Test;

import es.uca.webservices.bpel.xml.XMLDocument;
import es.uca.webservices.wsdl.ast.Parser.EncodingStyle;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class DG_ComplexTypesTest extends DeclarationsGenBaseTest {

    private static final String NAMESPACE = "http://ServiceAnalyzerTests/ComplexTypes";

    public DG_ComplexTypesTest() {
        super("ComplexTypes");
    }

    @Test
    public void sequence1() throws Exception {

        // Un sequence con un sólo elemento
        QName element = new QName(NAMESPACE, "esq1");
        XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);

        assertTrue("No se traduce correctamente sequence",
                (Boolean) declarationsXMLDoc.evaluateExpression(
                "count(//*) = 1 "
                + "and count(/*[local-name(.) = 'variable' and @name='esq1' and @type='string']) = 1 ",
                XPathConstants.BOOLEAN));

        // Un sequence con dos elementos
        element = new QName(NAMESPACE, "esq1_1");
        declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);

        assertTrue("No se traduce correctamente sequence",
                (Boolean) declarationsXMLDoc.evaluateExpression(
                "count(//*[local-name(.) = 'typedef' and @type='tuple' and @element='string, string']) = 1 ",
                XPathConstants.BOOLEAN));

        // Un sequence con dos elementos, siendo uno de ellos otro secuence
        element = new QName(NAMESPACE, "esq1_2");
        declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);

        String typedefName = (String) declarationsXMLDoc.evaluateExpression(
                "//*[local-name(.) = 'typedef' and @type='tuple' and @element='string, string']/@name",
                XPathConstants.STRING);
        assertTrue("No se traduce correctamente sequence",
                (Boolean) declarationsXMLDoc.evaluateExpression(
                "count(//*[local-name(.) = 'typedef' and @type='tuple' and @element='string, "
                + typedefName + "']) = 1 ",
                XPathConstants.BOOLEAN));
    }

    @Test
    public void sequence2() throws Exception {

        /* Un sequence con un sólo elemento */

        // El sequence es opcional
        QName element = new QName(NAMESPACE, "esq2");
        XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);

        assertTrue("No se traduce correctamente sequence",
                (Boolean) declarationsXMLDoc.evaluateExpression(
                "count(//*[local-name(.) = 'typedef' and @type='list' and @element='string'"
                + " and @min='0' and @max='1']) = 1 ",
                XPathConstants.BOOLEAN));

        // El elemento de dentro del sequence es el que es opcional
        element = new QName(NAMESPACE, "esq2_1");
        declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);

        assertTrue("No se traduce correctamente sequence",
                (Boolean) declarationsXMLDoc.evaluateExpression(
                "count(//*[local-name(.) = 'typedef' and @type='list' and @element='string'"
                + " and @min='0' and @max='1']) = 1 ",
                XPathConstants.BOOLEAN));
    }

    @Test
    public void sequence3() throws Exception {

        /* Un sequence con un sólo elemento */

        // El sequence puede repetirse indefinidas veces
        QName element = new QName(NAMESPACE, "esq3");
        XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);

        assertTrue("No se traduce correctamente sequence",
                (Boolean) declarationsXMLDoc.evaluateExpression(
                "count(//*[local-name(.) = 'typedef' and @type='list' and @min]) = 1 "
                + "and count(//*[local-name(.) = 'typedef' and @type='list' and @min and @max]) = 0 ",
                XPathConstants.BOOLEAN));

        // El elemento de dentro del sequence es el que se repite
        element = new QName(NAMESPACE, "esq3_1");
        declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);

        assertTrue("No se traduce correctamente sequence",
                (Boolean) declarationsXMLDoc.evaluateExpression(
                "count(//*[local-name(.) = 'typedef' and @type='list' and @min]) = 1 "
                + "and count(//*[local-name(.) = 'typedef' and @type='list' and @min and @max]) = 0 ",
                XPathConstants.BOOLEAN));
    }

    @Test
    public void sequence4() throws Exception {
        /* Un sequence con un varios elementos */

        // El sequence puede repetirse indefinidas veces
        QName element = new QName(NAMESPACE, "esq4");
        XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);

        assertEquals("No se traduce correctamente sequence",
                (String) declarationsXMLDoc.evaluateExpression(
                "//*[local-name(.) = 'typedef' and @type='list']/@element", XPathConstants.STRING),
                (String) declarationsXMLDoc.evaluateExpression(
                "//*[local-name(.) = 'typedef' and @type='tuple' and @element='string, float']/@name", XPathConstants.STRING));

        // Se repite el sequence y algunos elementos
        element = new QName(NAMESPACE, "esq5");
        declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT); // falta

    }

    @Test
    public void onlyOneAttributeAndNoContent() throws Exception {
        QName element = new QName(NAMESPACE, "eat1");
        XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);
        assertEquals("Attribute 'total' should have 'TEat1' type", "TEat1", declarationsXMLDoc.evaluateExpression("//*[@name='eat1']/@type"));
        assertEquals("Type 'TEat1' should have 'list' type (optional attribute)", "list", declarationsXMLDoc.evaluateExpression("//*[@name='TEat1']/@type"));
        assertEquals("Type 'TEat1' should have 'float' element type", "float", declarationsXMLDoc.evaluateExpression("//*[@name='TEat1']/@element"));
        assertEquals("Type 'TEat1' should have 0 min elements", "0", declarationsXMLDoc.evaluateExpression("//*[@name='TEat1']/@min"));
        assertEquals("Type 'TEat1' should have 1 max element", "1", declarationsXMLDoc.evaluateExpression("//*[@name='TEat1']/@max"));
    }

    @Test
    public void fixedAttribute() throws Exception {
        QName element = new QName(NAMESPACE, "eat2");
        XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);
        assertEquals("The tuple type should only have two elements: the fixed attribute should have been ignored",
        		"float, TAttrOpt_total2",
        		declarationsXMLDoc.evaluateExpression("//*[@name='TEat2']/@element"));
    }

    @Test
    public void all() throws Exception {
        QName element = new QName(NAMESPACE, "eall");
        XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);
        assertEquals("int, TTwo",
        	(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @type='tuple']/@element"));
        assertEquals("string",
        	(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @name='TTwo']/@type"));
    }

    @Test(expected = java.lang.UnsupportedOperationException.class)
    public void mixedContentIsUnsupported() throws Exception {
        QName element = new QName(NAMESPACE, "emix");
        GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);
    }

    @Test
    public void twoChoices() throws Exception {
        QName element = new QName(NAMESPACE, "echo");
        XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);
        assertEquals("TChoice2, int, TTwo",
        		(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @type='tuple']/@element"));
        assertEquals("1",
        		(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @name='TChoice2']/@min"));
        assertEquals("2",
        		(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @name='TChoice2']/@max"));
    }

    @Test
    public void threeChoicesWithRepetitions() throws Exception {
    	QName element = new QName(NAMESPACE, "echo2");
    	XMLDocument declarationsXMLDoc = GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);
        assertEquals("T4",
        		(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @name='TEcho2']/@element"));
        assertEquals("0",
        		(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @name='TEcho2']/@min"));
        assertEquals("0",
        		(String)declarationsXMLDoc.evaluateExpression("count(//*[local-name(.) = 'typedef' and @name='TEcho2']/@max)"));
        assertEquals("TChoice3, string, float, TList_Pair",
        		(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @name='T4']/@element"));

        assertEquals("2",
        		(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @name='TList_Pair']/@min"));
        assertEquals("4",
        		(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @name='TList_Pair']/@max"));

        assertEquals("1",
        		(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @name='TChoice3']/@min"));
        assertEquals("3",
        		(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @name='TChoice3']/@max"));
        assertEquals("int, int",
        		(String)declarationsXMLDoc.evaluateExpression("//*[local-name(.) = 'typedef' and @name='TPair']/@element"));
    }

    @Test
    public void wildcard() throws Exception {
        QName element = new QName(NAMESPACE, "eany");
        GenerateDeclarationsXML(element, EncodingStyle.DOCUMENT);
    }
}
