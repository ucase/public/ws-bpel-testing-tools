/*
 *  Copyright 2010 Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.visitor;

import serviceAnalyzer.messageCatalog.TypeGA;
import es.uca.webservices.bpel.xml.XMLDocument;

import javax.xml.namespace.QName;

import org.junit.Test;

/**
 *
 * @author Cristina Jiménez Gavilán (cristina.jimenezgavilan@alum.uca.es)
 */
public class DG_TypesCorrespondenceTest extends DeclarationsGenBaseTest {

    private static final String NAMESPACE = "http://ServiceAnalyzerTests/TypesCorrespondence";

    public DG_TypesCorrespondenceTest() {
        super("TypesCorrespondence");
    }

    ////////// XML-Schema Primitive Datatypes //////////
    // 1. string
    @Test
    public void StringIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "string");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.STRING.toString());
    }

    // 2. boolean
    @Test
    public void BooleanIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "boolean");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.STRING.toString(), null,
                null, null, new String[]{"true", "false"}, null, null, null);
    }

    // 3. decimal
    @Test
    public void DecimalIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "decimal");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.FLOAT.toString());
    }

    // 4. float
    @Test
    public void FloatIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "float");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.FLOAT.toString());
    }

    // 5. double
    @Test
    public void DoubleIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "double");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.FLOAT.toString());
    }
    // 6. duration

    @Test
    public void DurationIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "duration");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.DURATION.toString());
    }

    // 7. dateTime
    @Test
    public void DateTimeIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "dateTime");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.DATE_TIME.toString());
    }

    // 8. time
    @Test
    public void TimeIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "time");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.TIME.toString());
    }

    // 9. date
    @Test
    public void DateIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "date");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.DATE.toString());
    }

    // 10. gYearMonth
    @Test
    public void GYearMonthIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "gYearMonth");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.DATE.toString());
    }

    // 11. gYear
    @Test
    public void GYearIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "gYear");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.DATE.toString());
    }

    // 12. gMonthDay
    @Test
    public void GMonthDayIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "gMonthDay");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.DATE.toString());
    }

    // 13. gMonth
    @Test
    public void GMonthIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "gMonth");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.DATE.toString());
    }

    // 14. gDay
    @Test
    public void GDayIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "gDay");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.DATE.toString());
    }

    // 15. hexBinary
    @Test
    public void HexBinaryIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "hexBinary");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.STRING.toString(), null,
                null, null, null, null, null, "[0-9a-fA-F]+");
    }

    // 16. base64Binary
    @Test
    public void Base64BinaryIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "base64Binary");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.STRING.toString());
    }

    // 17. anyURI
    @Test
    public void AnyURIIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "anyURI");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.STRING.toString());
    }

    // 18. QName
    @Test
    public void QNameIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "QName");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.STRING.toString(), null,
                null, null, null, null, null, "([a-zA-Z_][a-zA-Z_0-9.-]*:)?[a-zA-Z_][a-zA-Z_0-9.-]*");
    }

    // 19. NOTATION
    @Test
    public void NOTATIONIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "NOTATION");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.STRING.toString());
    }

    ////////// XML-Schema Derived Datatypes //////////
    // 1. normalizedString
    @Test
    public void NormalizedStringIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "normalizedString");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.STRING.toString());
    }

    // 2. token
    @Test
    public void TokenIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "token");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.STRING.toString());
    }

    // 3. language
    @Test
    public void LanguageIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "language");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.STRING.toString(), null,
                null, null, null, null, null, "[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*");
    }

    // 4. NMTOKEN
    @Test
    public void NMTOKENIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "NMTOKEN");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.STRING.toString(), null,
                null, null, null, null, null, "\\c+");
    }

    // 5. NMTOKENS
    @Test
    public void NMTOKENSIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "NMTOKENS");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);
        assertExistsTypedef("", declarationsXMLDoc, "TStringP",
                TypeGA.STRING.toString(), null,
                null, null, null, null, null, "\\c+");
        assertExistsTypedef("", declarationsXMLDoc, "TNMTOKENS",
                TypeGA.LIST.toString(), new String[]{"TStringP"},
                "1", null, null, null, null, null);
    }

    // 6. Name
    @Test
    public void NameIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "Name");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.STRING.toString(), null,
                null, null, null, null, null, "\\i\\c*");
    }

    // 7. NCName
    @Test
    public void NCNameIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "NCName");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.STRING.toString(), null,
                null, null, null, null, null, "[\\i-[:]][\\c-[:]]*");
    }

    // 8. ID
    @Test
    public void IDIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "ID");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.STRING.toString());
    }

    // 9. IDREF
    @Test
    public void IDREFIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "IDREF");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.STRING.toString());
    }

    // 10. IDREFS
    @Test
    public void IDREFSIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "IDREFS");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.LIST.toString(), new String[]{TypeGA.STRING.toString()},
                null, null, null, null, null, null);
        // "1", null, null, null, null, null); ????
    }

    // 11. ENTITY
    @Test
    public void ENTITYIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "ENTITY");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.STRING.toString());
    }

    // 12. ENTITIES
    @Test
    public void ENTITIESIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "ENTITIES");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.LIST.toString(), new String[]{TypeGA.STRING.toString()},
                "1", null, null, null, null, null);
    }

    // 13. integer
    @Test
    public void IntegerIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "integer");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.INT.toString());
    }

    // 14. nonPositiveInteger
    @Test
    public void NonPositiveIntegerIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "nonPositiveInteger");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.INT.toString(), null, null, "0", null, null, null, null);
    }

    // 15. negativeInteger
    @Test
    public void NegativeIntegerIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "negativeInteger");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.INT.toString(), null, null, "-1", null, null, null, null);
    }

    // 16. long
    @Test
    public void LongIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "long");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.INT.toString(), null, "-9223372036854775808", "9223372036854775807", null, null, null, null);
    }

    // 17. int
    @Test
    public void IntIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "int");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyAVariable("", declarationsXMLDoc, TypeGA.INT.toString());

    }

    // 18. short
    @Test
    public void ShortIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "short");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.INT.toString(), null, "-32768", "32767", null, null, null, null);
    }

    // 19. byte
    @Test
    public void ByteIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "byte");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.INT.toString(), null, "-128", "127", null, null, null, null);
    }

    // 20. nonNegativeInteger
    @Test
    public void NonNegativeIntegerIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "nonNegativeInteger");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.INT.toString(), null, "0", null, null, null, null, null);
    }

    // 21. unsignedLong
    @Test
    public void UnsignedLongIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "unsignedLong");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.INT.toString(), null, "0", "18446744073709551615", null, null, null, null);
    }

    // 22. unsignedInt
    @Test
    public void UnsignedIntIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "unsignedInt");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.INT.toString(), null, "0", "4294967295", null, null, null, null);
    }

    // 23. unsignedShort
    @Test
    public void UnsignedShortIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "unsignedShort");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.INT.toString(), null, "0", "65535", null, null, null, null);
    }

    // 24. unsignedByte
    @Test
    public void UnsignedByteIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "unsignedByte");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.INT.toString(), null, "0", "255", null, null, null, null);
    }

    // 25. positiveInteger
    @Test
    public void PositiveIntegerIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "positiveInteger");
        XMLDocument declarationsXMLDoc = GenDeclarationsXMLAndCompStyles(element);

        assertOnlyOneVariableAndHisTypedef("", declarationsXMLDoc,
                TypeGA.INT.toString(), null, "1", null, null, null, null, null);
    }

    ////////// Others //////////
    @Test(expected = java.lang.UnsupportedOperationException.class)
    public void AnyTypeIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "anyType");
        GenDeclarationsXMLAndCompStyles(element);
    }

    @Test(expected = java.lang.UnsupportedOperationException.class)
    public void AnySimpleTypeIsGeneratedCorrectly() throws Exception {
        QName element = new QName(NAMESPACE, "anySimpleType");
        GenDeclarationsXMLAndCompStyles(element);
    }
}
// TO DO:
// Especificar messages
// Caso raro con IDREFS

