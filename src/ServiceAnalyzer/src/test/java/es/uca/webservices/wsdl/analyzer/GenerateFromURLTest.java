package es.uca.webservices.wsdl.analyzer;

import es.uca.webservices.wsdl.util.AbstractHTTPServerWithWSDLTest;
import org.junit.Test;

/**
 * Check that ServiceAnalyzer can generate catalogs from
 * a single URL.
 */
public class GenerateFromURLTest extends AbstractHTTPServerWithWSDLTest {

    @Test
    public void generateCatalogFromURL() throws Exception {
        final ServiceAnalyzerCommand cmd = new ServiceAnalyzerCommand();
        cmd.parseArgs(URL_PREFIX + DEFAULT_SERVER_PORT + ORDERS_MAIN_QUERY);
        cmd.run();
    }
}
