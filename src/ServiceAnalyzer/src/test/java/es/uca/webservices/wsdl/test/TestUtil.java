/*
 *  Copyright 2011 Antonio García-Domínguez (antonio.garciadominguez@uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.test;

import static org.junit.Assert.assertTrue;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceConstants;
import org.custommonkey.xmlunit.DifferenceListener;
import org.custommonkey.xmlunit.XMLUnit;
import org.w3c.dom.Node;

/**
 * Assortment of utility methods for performing tests.
 *
 * @author Antonio García-Domínguez
 */
public final class TestUtil {

	private static final class IgnorePrefixesDifferenceListener implements
			DifferenceListener {
		@Override
		public int differenceFound(Difference diff) {
			if (diff.getId() == DifferenceConstants.NAMESPACE_PREFIX_ID) {
				return DifferenceListener.RETURN_IGNORE_DIFFERENCE_NODES_IDENTICAL;
			}
			return DifferenceListener.RETURN_ACCEPT_DIFFERENCE;
		}

		@Override
		public void skippedComparison(Node arg0, Node arg1) {
			// do nothing
		}
	}

	private TestUtil() {}

	/**
	 * Asserts that the <code>obtained</code> XML fragment is equal to the <code>expected</code> one, after
	 * normalizing whitespace (see {@link XMLUnit#setNormalizeWhitespace(boolean)}) and ignoring differences
	 * in namespace prefixes (but not in namespace URIs). 
	 */
	public static void assertEqualXML(String expected, String obtained) throws Exception {
		XMLUnit.setNormalizeWhitespace(true);
		XMLUnit.setIgnoreWhitespace(true);

		final Diff diff = new Diff(expected, obtained);
		diff.overrideDifferenceListener(new IgnorePrefixesDifferenceListener());
		final DetailedDiff myDiff = new DetailedDiff(diff);
		for (Object o : myDiff.getAllDifferences()) {
			System.err.println(o);
		}
		assertTrue(myDiff.getAllDifferences().isEmpty());
	}
}