/*
 *  Copyright 2011-2013 Antonio García-Domínguez (antonio.garciadominguez@uca.es)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.wsdl.analyzer.utils;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple beautifier which puts each XML element in a different line and
 * properly indents everything. We can't beautify it with a regular XML
 * beautifier, as it is not valid XML.
 *
 * @author Antonio García-Domínguez
 */
public class VelocityBeautifier {

	private final static Pattern TAG_PATTERN = Pattern.compile("<.*?>");

	private int charsPerLevel = 2;
	private char indentChar = ' ';

	public int getCharsPerLevel() {
		return charsPerLevel;
	}

	public void setCharsPerLevel(int charsPerLevel) {
		this.charsPerLevel = charsPerLevel;
	}

	public char getIndentChar() {
		return indentChar;
	}

	public void setIndentChar(char indentChar) {
		this.indentChar = indentChar;
	}

	public String beautify(String rawTemplate) {
		if (rawTemplate == null) {
			return null;
		}

		Matcher matcher = TAG_PATTERN.matcher(rawTemplate);
		int lastEnd = 0;

		final Deque<Deque<String>> linesStack = new ArrayDeque<Deque<String>>();
		linesStack.add(new ArrayDeque<String>());
		while (matcher.find()) {
			final String tag = matcher.group();

			// Append the text content that was not part of the previous tag
			final String prefix = rawTemplate.substring(lastEnd, matcher.start()).trim();
			if (prefix.length() > 0) {
				linesStack.getFirst().add(prefix);
			}

			if (tag.startsWith("</")) {
				final Deque<String> contents = linesStack.removeFirst();

				final Deque<String> enclosing = linesStack.getFirst();
				if (contents.size() > 1) {
					for (String l : contents) {
						enclosing.add("  " + l);
					}
					enclosing.add(tag);
				} else {
					enclosing.add(enclosing.removeLast() + contents.getFirst() + tag);
				}
			}
			else {
				linesStack.getFirst().add(tag);
				if (!tag.endsWith("/>")) {
					linesStack.addFirst(new ArrayDeque<String>());
				}
			}

			lastEnd = matcher.end();
		}
		assert linesStack.size() == 1;

		// Add content after last tag
		final String suffix = rawTemplate.substring(lastEnd).trim();
		if (suffix.length() > 0) {
			linesStack.getFirst().add(suffix);
		}

		StringBuffer sbuf = new StringBuffer();
		boolean bFirst = true;
		for (String l : linesStack.removeFirst()) {
			if (bFirst) {
				bFirst = false;
			} else {
				sbuf.append('\n');
			}
			sbuf.append(l);
		}
		return sbuf.toString();
	}
}
