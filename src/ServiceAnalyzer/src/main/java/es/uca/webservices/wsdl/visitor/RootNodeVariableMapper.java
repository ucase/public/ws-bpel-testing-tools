package es.uca.webservices.wsdl.visitor;

import java.util.Arrays;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import es.uca.webservices.wsdl.ast.RootNode;

/**
 * Assigns variable names to individual {@link RootNode}s. Mostly
 * useful to ensure that both the {@link DeclarationsGenerator} and
 * {@link TemplateGenerator} classes use the same variable names.
 *
 * This class also takes care of avoiding using names with the same
 * names as the ones predefined in BPLEUnit, and ensuring variable
 * names are unique over the entire service catalog. 
 */
public class RootNodeVariableMapper {
	private Map<RootNode, String> variableNameMap = new IdentityHashMap<RootNode, String>();

	// Names that have been previously used and should not be used again
    private Set<String> usedNames = new HashSet<String>(Arrays.asList(
    		// BPELUnit predefined variable names cannot be used
    		"baseURL",
    		"collections",
    		"putName",
    		"testCaseCount",
    		"testSuiteName",
    		"testCaseName",
    		"partnerTrackName",
    		"partnerTrackURL",
    		"request",
    		"partnerTrackReceived",
    		"partnerTrackSent",
    		"xpath",
    		"printer"
    ));

	public String getVariableName(RootNode node) {
		String varName = variableNameMap.get(node);
		if (null == varName) {
			// Try first with the name of the element, and if that doesn't work
			// try adding a numeric suffix until it's unique.
			varName = node.getQName().getLocalPart();
			int iSuffix = 0;
			while (usedNames.contains(varName)) {
				varName = node.getQName().getLocalPart() + iSuffix++;
			}

			variableNameMap.put(node, varName);
			usedNames.add(varName);
		}

        return varName;
	}
}
