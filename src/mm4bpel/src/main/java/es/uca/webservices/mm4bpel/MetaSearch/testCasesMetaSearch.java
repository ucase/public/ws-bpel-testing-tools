package es.uca.webservices.mm4bpel.MetaSearch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.rits.cloning.Cloner;


/**
 * Clase que implementa todas las MR de la composicion MetaSearch, junto a la eliminacion de repeticiones.
 * 
 * @author Maria Azahara Camacho Magrinnan
 *
 */
public class testCasesMetaSearch{

	public testCasesMetaSearch(){}
	
	/**
	 * Funcion que controla la eleccion del usuario en cuanto a MR que quiere aplicar
	 * @param x Conjunto de casos de prueba originales
	 * @return Devuelve el conjunto de casos de prueba siguientes generados a partir del original "x"
	 * @throws IOException
	 */
	public Map<String,List<Object>> generate(HashMap<String, List<Object>> x) throws IOException {
		
		HashMap<String,List<Object> > salida = new HashMap<String,List<Object>>();
		
		System.out.println("\nIndique el numero de la relacion metamorfica que quiere aplicar en el rango 1-17");
		System.out.println("\n-EJEMPLO: para la MR1, introduzca el valor \"1\"");
		System.out.println("-En el caso especial de querer aplicar todas las MR introduzca el valor \"0\"");
		System.out.println("\tU otro que se encuentre fuera del rango establecido.");
		System.out.printf("\n\t\tNumero de la MR a aplicar: ");
		
		BufferedReader lectura = new BufferedReader(new InputStreamReader(System.in));
		String mr = lectura.readLine();
		
		System.out.println("\n¿Desea incluir casos de prueba repetidos en el conjunto final? ");
		System.out.println("\t Si: introduzca \"s\"");
		System.out.println("\t No: introduzca \"n\"");
		
		boolean correct = false;
		String repetidos = null;
		
		while(!correct)
		{
			System.out.printf("\n\t\tOpcion elegida: ");
			BufferedReader lectura2 = new BufferedReader(new InputStreamReader(System.in));
			repetidos = lectura2.readLine();
			
			if(!repetidos.equals("s") && !repetidos.equals("n"))
			{
				System.out.println("\nOPCION INCORRECTA.");
				System.out.println("\nVUELVA A INTENTARLO CON UNA OPCION CORRECTA ('s' o 'n').");
			}
			else
				correct = true;
		}
		
		System.out.println("Total de casos de prueba con los que comenzamos el estudio: " + x.get((String)x.keySet().iterator().next()).size());
				
		// Lo pasamos a entero para comparar la relacion metamorfica que se debe aplicar
		int mrInt = Integer.parseInt(mr);
		
		switch (mrInt){
			case 1:
				mr1(x,salida);
				break;
			case 2:
				mr2(x,salida);
				break;
			case 3:
				mr3(x,salida);
				break;
			case 4:
				mr4(x,salida);
				break;
			case 5:
				mr5(x,salida);
				break;
			case 6:
				mr6(x,salida);
				break;
			case 7:
				mr7(x,salida);
				break;
			case 8:
				mr8(x,salida);
				break;
			case 9:
				mr9(x,salida);
				break;
			case 10:
				mr10(x,salida);
				break;
			case 11:
				mr11(x,salida);
				break;
			case 12:
				mr12(x,salida);
				break;
			case 13:
				mr13(x,salida);
				break;
			case 14:
				mr14(x,salida);
				break;
			case 15:
				mr15(x,salida);
				break;
			case 16:
				mr16(x,salida);
				break;
			case 17:
				mr17(x,salida);
				break;
			default:
				mrAll(x,salida);
				break;
		}
		
		/*
		 * Guardamos en el map "finales" el conjunto completo de casos de prueba generados, teniendo en cuenta la opcion elegida por el usuario 
		 * al comenzar la ejecucion anteriormente.
		 */
		Map<String,List<Object>> finales = null;
		if(repetidos.equals("s"))
		{
			 finales = salida;
		}
		else if(repetidos.equals("n"))
		{
			finales = noRepeat(x,salida);
		}
		
		return finales;
	}
	
	/**
	 * Funcion que elimina los repetidos del conjunto de casos de prueba que se le pasa como map, tanto generados repetidos entre sí, como
	 * los que ya se encuentran en el conjunto de casos de prueba original.
	 * @param x
	 * @param salida
	 * @return Map que contiene aquellos casos de prueba siguiente que se han generado sin repeticiones.
	 */
	@SuppressWarnings("unchecked")
	private Map<String, List<Object>> noRepeat(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		if(!salida.isEmpty())
		{
			/*
			 * Adaptamos todos los valores que hay en el map de entrada 'x' como casos de prueba pertenecientes al 
			 * conjunto de casos de prueba original.
			 */
			Collection<List<Object>> aux = x.values();
			List<List<Object>> originales = new ArrayList<List<Object>>(aux);
						
			List<List<Object>> casosO = new ArrayList<List<Object>>();
			for(int i=0; i < originales.get(0).size(); i++)
			{
				List<Object> caso = new ArrayList<Object>();
				for(int j = 0; j<originales.size(); ++j){
					caso.add(originales.get(j).get(i));
				}
				casosO.add(caso);
			}
			
			/*
			 * Adaptamos todos los valores que hay en el map de nuevos casos 'salida' como casos de prueba pertenecientes al 
			 * conjunto de casos de prueba siguiente.
			 */
			Collection<List<Object>> temp = salida.values();
			List<List<Object>> valores = new ArrayList<List<Object>>(temp);
						
			List<List<Object>> casosS = new ArrayList<List<Object>>();
			for(int i=0; i < valores.get(0).size(); i++)
			{
				List<Object> caso = new ArrayList<Object>();
				for(int j = 0; j<valores.size(); ++j){
					caso.add(valores.get(j).get(i));
				}
				casosS.add(caso);
			}
			
			/*
			 * Vamos annadiendo a una lista de casos de prueba todos aquellos que no esten repetidos ya en el conjunto 
			 * original o en el propio conjunto generado como nuevos casos de prueba.
			 */
			List<List<Object>> casosF = new ArrayList<List<Object>>();
			
			for(int i=0; i<casosS.size(); ++i){
				if(!casosO.contains(casosS.get(i)) && !casosF.contains(casosS.get(i)))
					casosF.add(casosS.get(i));
			}
			
			
			/*
			 * Ahora, trasladamos todos los valores guardados en el conjunto 'casosF' a un map junto a la clave que le 
			 * corresponde a cada uno de los valores.
			 */
			/*
			 * Map final que devolveremos con los casos de prueba sin repetir
			 */
			Map<String,List<Object> > conjuntoFinal = new HashMap<String, List<Object>>(x);
			
			/*
			 * Obtenemos el conjunto de claves que conforman el map. No lo obtenemos a traves de la funcion "keySet()" debido 
			 * a que nos devuelve un set de valores ordenados de forma diferente a como lo necesitamos para que se correspondan 
			 * los valores obtenidos.
			 */
			List<String> claves = new ArrayList<String>();
			Iterator<String> iterIn = conjuntoFinal.keySet().iterator();
			while (iterIn.hasNext()) {
			    String keyIn = (String) iterIn.next();
			    claves.add(keyIn);
			}		    
			
			
			/*
			 * Transformamos los casos de prueba en listas de valores para cada una de las claves.
			 */
			List<Object> vals = new ArrayList<Object>();
			
			for(int i = 0; i<casosF.get(0).size(); ++i){
				
				List<Object> claveVal = new ArrayList<Object>();
				
				for(int j=0; j<casosF.size(); ++j){
					claveVal.add(casosF.get(j).get(i));
				}
				
				vals.add(claveVal);
			}
			/*
			 * El numero de elementos de 'vals' y 'claves' es el mismo, ya que uno representa las listas de valores de cada clave y 'claves'
			 * todas las keys que va a contener el map final. Asignamos en el map final a cada clave, su valor correspondiente. 
			 */
			
			for(int i=0; i<claves.size(); ++i){
				conjuntoFinal.put(claves.get(i), (List<Object>) vals.get(i));
			}
			
			return conjuntoFinal;
			
		}
		else
			return salida;
	}
	
	/**
	 * Funcion mr1 que simula la relacion metamorfica 1 del manual:
	 *   
	 * MR: consulta1 = consulta2 && maxResult1 = Count1 && maxResult2 = maxResult1-1 => Count2 = Count1-1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba
	 */
	@SuppressWarnings("unchecked")
	private void mr1(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		List<Object> consulta1 = x.get((String)"meta");
		/*
		 * Variable en la que vamos a guardar todos los count de los casos de prueba que tenemos
		 */
		List<Object> count1 = x.get((String)"numeric");
		
		/*
		 * Variable en la que vamos a guardar los maxResults de los casos de prueba. Para ello, 
		 * vamos a recorrer la lista guardada anteriormente (consulta1) y guardar unicamente 
		 * el ultimo elemento de cada una de las listas que contiene.
		 */
		List<Object> maxResult1 = new ArrayList<Object> ();		
		for(int i = 0; i<consulta1.size(); i++)
		{
			List<Object> aux = (List<Object>) consulta1.get(i);
			maxResult1.add(aux.get(3));
		}
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = maxResult1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			if(maxResult1.get(i).equals(count1.get(i)))
			{
				System.out.println("MR1 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los dos valores implicados en la MR (maxResult - count) aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("meta"))
				{
					aux = clon.deepClone(o);
					int maxRes1 = ((List<Integer>) o).get(3);
					maxRes1 = maxRes1-1;
					((List<Object>) aux).set(3,maxRes1);
				}
				else if(keyIn.equals("numeric"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val-1;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR1: " + newTestCases.size());		
	}

	
	
	/*
	 * Se ha modificado la MR de forma que en lugar de depender de la restricciones que establecia la plantilla CSV, lo unico que 
	 * se va a comprobar son los valores de count y maxResults iniciales. De esta forma, si el valor original de maxResult es mayor o 
	 * igual que el doble de count original, si multiplicamos por 2 el valor de count se duplicaran el numero de resultados y debera 
	 * funcionar sin problemas ese nuevo caso de prueba.
	 */	
	/**
	 * Funcion mr2 que simula la relacion metamorfica 2 del manual:
	 *  
	 * MR: maxResult1 >= Count1*2 && Count2 = Count1*2 && maxResults2 = maxResults1 => Resultados2 = Resultados1*2
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	@SuppressWarnings("unchecked")
	private void mr2(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
	 
		List<Object> consulta1 = x.get((String)"meta");
		/*
		 * Variable en la que vamos a guardar todos los count de los casos de prueba que tenemos
		 */
		List<Object> count1 = x.get((String)"numeric");
		
		/*
		 * Lista que va a guardar todas las condiciones de cada caso de prueba para comprobar si se puede o no 
		 * duplicar el numero de resultados.
		 */
		List<Object> cond1 = x.get((String)"conditions");
		
		/*
		 * Variable en la que vamos a guardar los maxResults de los casos de prueba. Para ello, 
		 * vamos a recorrer la lista guardada anteriormente (consulta1) y guardar unicamente 
		 * el ultimo elemento de cada una de las listas que contiene.
		 */
		List<Object> maxResult1 = new ArrayList<Object> ();		
		for(int i = 0; i<consulta1.size(); i++)
		{
			List<Object> aux = (List<Object>) consulta1.get(i);
			maxResult1.add(aux.get(3));
		}
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = maxResult1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			int c1 = (Integer) count1.get(i);
			int mR1 = (Integer) maxResult1.get(i);
			List<Object> condition1 = (List<Object>) cond1.get(i);
			
			/*
			 * Comprobamos que se cumpla la condicion entre maxResult y Count, pero ademas, que la lista de condiciones este 
			 * vacia ya que al incluir el nuevo resultado, el caso de prueba no tendra exigencias en ese aspecto y no tendra ningun 
			 * problema.
			 */
			if((mR1 >= (c1*2)) && (condition1.isEmpty()))
			{
				System.out.println("MR2 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();
		
		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			int Count = 0;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR (maxResult) aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("numeric"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val*2;
					Count = val;
				}
				/*
				 * Si se trata de la clave que hace referencia a los resultados de Google, incluimos en la lista de resultados un 
				 * total de "Count" resultados nuevos para hacer que el caso de prueba funcione. No trabajamos con los resultados de 
				 * MSN porque su acceso es mas complicado.
				 */
				else if(keyIn.equals("googleValues"))
				{
					aux = clon.deepClone(o);
					
					for(int j=0;j<Count;++j)
					{
						List<String> resNew = new ArrayList<String>();
						
						String url = randString(5);
						resNew.add(url);
						String title = randString(7);
						resNew.add(title);
						String descr = randString(7);
						resNew.add(descr);
						String enableOrigin = "false";
						resNew.add(enableOrigin);
						
						((List<Object>) aux).add(resNew);
					}					
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR2: " + newTestCases.size());
		
	}


	
	/**
	 * Funcion mr3 que simula la relacion metamorfica 3 del manual:
	 * 
	 * MR: consulta1 = consulta2 && Count1 = Count2 && maxResult2 = maxResult1*2 => Resultados2 = Resultados1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	@SuppressWarnings("unchecked")
	private void mr3(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		List<Object> consulta1 = x.get((String)"meta");
		
		/*
		 * Variable en la que vamos a guardar los maxResults de los casos de prueba. Para ello, 
		 * vamos a recorrer la lista guardada anteriormente (consulta1) y guardar unicamente 
		 * el ultimo elemento de cada una de las listas que contiene.
		 */
		List<Object> maxResult1 = new ArrayList<Object> ();		
		for(int i = 0; i<consulta1.size(); i++)
		{
			List<Object> aux = (List<Object>) consulta1.get(i);
			maxResult1.add(aux.get(3));
		}
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR, que en esta ocasion no se impone ninguna condicion especial
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = maxResult1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			System.out.println("MR3 aplicada en el caso de prueba " + (i+1));
			newTestCases.add(i);
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR (maxResult) aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("meta"))
				{
					aux = clon.deepClone(o);
					int maxRes1 = ((List<Integer>) o).get(3);
					maxRes1 = maxRes1*2;
					((List<Object>) aux).set(3,maxRes1);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR3: " + newTestCases.size());		
	}


	
	/**
	 * Funcion mr4 que simula la relacion metamorfica 4 del manual:
	 *  
	 * MR: consulta1 = consulta2 && Count1 = Count2 && maxResult2 = maxResult1*10 => Resultados2 = Resultados1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	@SuppressWarnings("unchecked")
	private void mr4(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		List<Object> consulta1 = x.get((String)"meta");
		
		/*
		 * Variable en la que vamos a guardar los maxResults de los casos de prueba. Para ello, 
		 * vamos a recorrer la lista guardada anteriormente (consulta1) y guardar unicamente 
		 * el ultimo elemento de cada una de las listas que contiene.
		 */
		List<Object> maxResult1 = new ArrayList<Object> ();		
		for(int i = 0; i<consulta1.size(); i++)
		{
			List<Object> aux = (List<Object>) consulta1.get(i);
			maxResult1.add(aux.get(3));
		}
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR, que en esta ocasion no se impone ninguna condicion especial
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = maxResult1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			System.out.println("MR4 aplicada en el caso de prueba " + (i+1));
			newTestCases.add(i);
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR (maxResult) aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("meta"))
				{
					aux = clon.deepClone(o);
					int maxRes1 = ((List<Integer>) o).get(3);
					maxRes1 = maxRes1*10;
					((List<Object>) aux).set(3,maxRes1);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR4: " + newTestCases.size());
	}


	
	/**
	 * Funcion mr5 que simula la relacion metamorfica 5 del manual:
	 *  
	 * MR: maxResult1 > Count1 && Count1 = Count2 && maxResult2 = maxResult1-1 => Resultados2 = Resultados1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	@SuppressWarnings("unchecked")
	private void mr5(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		List<Object> consulta1 = x.get((String)"meta");
		/*
		 * Variable en la que vamos a guardar todos los count de los casos de prueba que tenemos
		 */
		List<Object> count1 = x.get((String)"numeric");
		
		/*
		 * Variable en la que vamos a guardar los maxResults de los casos de prueba. Para ello, 
		 * vamos a recorrer la lista guardada anteriormente (consulta1) y guardar unicamente 
		 * el ultimo elemento de cada una de las listas que contiene.
		 */
		List<Object> maxResult1 = new ArrayList<Object> ();		
		for(int i = 0; i<consulta1.size(); i++)
		{
			List<Object> aux = (List<Object>) consulta1.get(i);
			maxResult1.add(aux.get(3));
		}
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = maxResult1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			int mR1 = (Integer) maxResult1.get(i);
			int c1 = (Integer) count1.get(i);
			
			if(mR1 > c1)
			{
				System.out.println("MR5 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR (maxResult) aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("meta"))
				{
					aux = clon.deepClone(o);
					int maxRes1 = ((List<Integer>) o).get(3);
					maxRes1 = maxRes1-1;
					((List<Object>) aux).set(3,maxRes1);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR5: " + newTestCases.size());	
	}


	
	/**
	 * Funcion mr6 que simula la relacion metamorfica 6 del manual:
	 *  
	 * MR: maxResult1 >= Count1 && Count1 = Count2 && maxResult2 = maxResult1+1 => Resultados2 = Resultados1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	@SuppressWarnings("unchecked")
	private void mr6(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		List<Object> consulta1 = x.get((String)"meta");
		/*
		 * Variable en la que vamos a guardar todos los count de los casos de prueba que tenemos
		 */
		List<Object> count1 = x.get((String)"numeric");
		
		/*
		 * Variable en la que vamos a guardar los maxResults de los casos de prueba. Para ello, 
		 * vamos a recorrer la lista guardada anteriormente (consulta1) y guardar unicamente 
		 * el ultimo elemento de cada una de las listas que contiene.
		 */
		List<Object> maxResult1 = new ArrayList<Object> ();		
		for(int i = 0; i<consulta1.size(); i++)
		{
			List<Object> aux = (List<Object>) consulta1.get(i);
			maxResult1.add(aux.get(3));
		}
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = maxResult1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			int mR1 = (Integer) maxResult1.get(i);
			int c1 = (Integer) count1.get(i);
			
			if(mR1 >= c1)
			{
				System.out.println("MR6 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR (maxResult) aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("meta"))
				{
					aux = clon.deepClone(o);
					int maxRes1 = ((List<Integer>) o).get(3);
					maxRes1 = maxRes1+1;
					((List<Object>) aux).set(3,maxRes1);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR6: " + newTestCases.size());
	}


	/*
	 * En esta relacion metamorfica lo que vamos a hacer es coger aquellos casos de prueba que no 
	 * cumplan la precondicion de la MR y asi generar un nuevo caso de prueba que debe obtener el mismo
	 * valor de Culture dentro de la composicion que los otros casos de prueba que si cumplen la 
	 * precondicion.
	 */
	/**
	 * Funcion mr7 que simula la relacion metamorfica 7 del manual:
	 *  
	 * MR: ((Language1 = "" && Country1 != "") || (Language1 != "" && Country1 = "")) && Language2 = Language1 && Country2 = Country1 => Culture2 = Culture1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	
	@SuppressWarnings("unchecked")
	private void mr7(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		List<Object> consulta1 = new ArrayList<Object>();
		consulta1.addAll(x.get((String)"meta"));
		
		/*
		 * Variable en la que vamos a guardar los Language y Country de los casos de prueba. Para ello, 
		 * vamos a recorrer la lista guardada anteriormente (consulta1) y guardar unicamente 
		 * el ultimo elemento de cada una de las listas que contiene.
		 */
		List<Object> language1 = new ArrayList<Object> ();
		List<Object> country1 = new ArrayList<Object> ();
		
		for(int i = 0; i<consulta1.size(); i++)
		{
			List<Object> aux = (List<Object>) consulta1.get(i);
			language1.add(aux.get(1));
			country1.add(aux.get(2));
		}
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = language1.size();
		System.out.println("\n\n Tam de casos de prueba: "+tam);
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			String lan1 = (String) language1.get(i);
			String cou1 = (String) country1.get(i);
			
			if(!((lan1.equals("") && !cou1.equals("")) || (!lan1.equals("") && cou1.equals(""))))
			{
				System.out.println("MR7 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = null;
				o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es la clave de los valores implicados en la condicion de la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("meta"))
				{
					aux = clon.deepClone(o);
					/*
					 * Ponemos el valor de country a vacio
					 */
					((List<Object>) aux).set(2,"");
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		
		System.out.println("Total de casos de prueba generados con la MR7: " + newTestCases.size());
	}
	

	/*
	 * Esta relacion metamorfica se encuentra divididad en dos partes en el manual original del PFC, pero, con solo trabajar 
	 * con el posible valor vacio de language es suficiente porque refleja las mismas posibilidades que con el valor de country.
	 */
	/**
	 * Funcion mr8 que simula la relacion metamorfica 8 del manual:
	 *  
	 * MR: Language1 != "" && Language2 != Language1 && Language2 = "" => Culture2 != Culture1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	@SuppressWarnings("unchecked")
	private void mr8(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		List<Object> consulta1 = new ArrayList<Object>();
		consulta1.addAll(x.get((String)"meta"));
		
		/*
		 * Variable en la que vamos a guardar los Language y Country de los casos de prueba. Para ello, 
		 * vamos a recorrer la lista guardada anteriormente (consulta1) y guardar unicamente 
		 * el ultimo elemento de cada una de las listas que contiene.
		 */
		List<Object> language1 = new ArrayList<Object> ();
		List<Object> country1 = new ArrayList<Object> ();
		
		for(int i = 0; i<consulta1.size(); i++)
		{
			List<Object> aux = (List<Object>) consulta1.get(i);
			language1.add(aux.get(1));
			country1.add(aux.get(2));
		}
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = language1.size();
		System.out.println("\n\n Tam de casos de prueba: "+tam);
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			String lan1 = (String) language1.get(i);
			
			if(!lan1.equals(""))
			{
				System.out.println("MR8 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = null;
				o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es la clave de los valores implicados en la condicion de la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("meta"))
				{
					aux = clon.deepClone(o);
					/*
					 * Ponemos el valor de language a vacio
					 */
					((List<Object>) aux).set(1,"");
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		
		System.out.println("Total de casos de prueba generados con la MR8: " + newTestCases.size());
		
	}

	/*
	 * Esta relacion metamorfica se utiliza con la intencion de generar casos de prueba invalidos de cara a la logica de la composicion.
	 * En MetaSearch no puede haber casos de prueba cuyo valor de Count sea mayor que el valor de MaxResult, y en esta MR se hace justo eso,
	 * generar casos de prueba con valores de Count mayores que MaxResults para comprobar la validez de la composicion y codigo.
	 */
	/*
	 * Se ha modificado la MR ya que contenia un fallo en el consecuente y parte de la condicion inicial, que en lugar de ser >= es solo >.
	 */
	/**
	 * Funcion mr9 que simula la relacion metamorfica 9 del manual:
	 *  
	 * MR: Consulta1 = Consulta2 && maxResult1 > Count1 && Count2 = Count1 && Count2 > maxResult2 => Resultados2 != Resultados1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	@SuppressWarnings("unchecked")
	private void mr9(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		List<Object> consulta1 = x.get((String)"meta");
		/*
		 * Variable en la que vamos a guardar todos los count de los casos de prueba que tenemos
		 */
		List<Object> count1 = x.get((String)"numeric");
		
		/*
		 * Variable en la que vamos a guardar los maxResults de los casos de prueba. Para ello, 
		 * vamos a recorrer la lista guardada anteriormente (consulta1) y guardar unicamente 
		 * el ultimo elemento de cada una de las listas que contiene.
		 */
		List<Object> maxResult1 = new ArrayList<Object> ();		
		for(int i = 0; i<consulta1.size(); i++)
		{
			List<Object> aux = (List<Object>) consulta1.get(i);
			maxResult1.add(aux.get(3));
		}
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = maxResult1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			int m1 = (Integer) maxResult1.get(i);
			int c1 = (Integer) count1.get(i); 
			if(m1 > c1)
			{
				System.out.println("MR9 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los dos valores implicados en la MR (maxResult - count) aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("meta"))
				{
					aux = clon.deepClone(o);
					int newMaxRes = (Integer) count1.get(newTestCases.get(i));
					
					((List<Object>) aux).set(3,newMaxRes);
				}
				else if(keyIn.equals("numeric"))
				{
					List<Object> consulta = (List<Object>) consulta1.get(newTestCases.get(i));
					aux = clon.deepClone(o);
					aux = consulta.get(3);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR9: " + newTestCases.size());		
		
	}


	/**
	 * Funcion mr10 que simula la relacion metamorfica 10 del manual:
	 *  
	 * MR: Consulta1 = Consulta2 && maxResult1 >= Count1 && Count2 = Count1 && maxResult2 = maxResult1*(-1) => Resultados2 != Resultados1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	@SuppressWarnings("unchecked")
	private void mr10(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		List<Object> consulta1 = x.get((String)"meta");
		/*
		 * Variable en la que vamos a guardar todos los count de los casos de prueba que tenemos
		 */
		List<Object> count1 = x.get((String)"numeric");
		
		/*
		 * Variable en la que vamos a guardar los maxResults de los casos de prueba. Para ello, 
		 * vamos a recorrer la lista guardada anteriormente (consulta1) y guardar unicamente 
		 * el ultimo elemento de cada una de las listas que contiene.
		 */
		List<Object> maxResult1 = new ArrayList<Object> ();		
		for(int i = 0; i<consulta1.size(); i++)
		{
			List<Object> aux = (List<Object>) consulta1.get(i);
			maxResult1.add(aux.get(3));
		}
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = maxResult1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			int m1 = (Integer) maxResult1.get(i);
			int c1 = (Integer) count1.get(i); 
			if(m1 >= c1)
			{
				System.out.println("MR10 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los dos valores implicados en la MR (maxResult) aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("meta"))
				{
					aux = clon.deepClone(o);
					int maxRes1 = ((List<Integer>) aux).get(3);
					maxRes1 = maxRes1*(-1);
					((List<Object>) aux).set(3,maxRes1);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR10: " + newTestCases.size());	
		
	}

	/**
	 * Funcion mr11 que simula la relacion metamorfica 11 del manual:
	 *  
	 * MR: ClientDelay1 < 10 && GoogleDelay1 < 10 && MSNDelay1 < 10 && ClientDelay2 = 10 && GoogleDelay2 = 10 && MSNDelay2 = 10 => Resultados2 = Resultados1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr11(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Guardamos todas las demoras temporales
		 */
		
		List<Object> clientDelay1 = x.get((String)"Client_Delay");
		List<Object> googleDelay1 = x.get((String)"Google_Delay");
		List<Object> msnDelay1 = x.get((String)"MSN_Delay");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = clientDelay1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			int client1 = (Integer) clientDelay1.get(i);
			int google1 = (Integer) googleDelay1.get(i);
			int msn1 = (Integer) msnDelay1.get(i);
			
			if((client1 < 10) && (google1 < 10) && (msn1 < 10))
			{
				System.out.println("MR11 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("Client_Delay") || keyIn.equals("Google_Delay") || keyIn.equals("MSN_Delay"))
				{
					aux = (Integer) clon.deepClone(o);
					aux = 10;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR11: " + newTestCases.size());			
	}


	/**
	 * Funcion mr12 que simula la relacion metamorfica 12 del manual:
	 *  
	 * MR: ClientDelay1 > 0 && GoogleDelay1 > 0 && MSNDelay1 > 0 && ClientDelay2 = 0 && GoogleDelay2 = 0 && MSNDelay2 = 0 => Resultados2 = Resultados1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr12(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Guardamos todas las demoras temporales
		 */
		
		List<Object> clientDelay1 = x.get((String)"Client_Delay");
		List<Object> googleDelay1 = x.get((String)"Google_Delay");
		List<Object> msnDelay1 = x.get((String)"MSN_Delay");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = clientDelay1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			int client1 = (Integer) clientDelay1.get(i);
			int google1 = (Integer) googleDelay1.get(i);
			int msn1 = (Integer) msnDelay1.get(i);
			
			if((client1 > 0) && (google1 > 0) && (msn1 > 0))
			{
				System.out.println("MR12 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("Client_Delay") || keyIn.equals("Google_Delay") || keyIn.equals("MSN_Delay"))
				{
					aux = (Integer) clon.deepClone(o);
					aux = 0;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR12: " + newTestCases.size());
	}


	/**
	 * Funcion mr13 que simula la relacion metamorfica 13 del manual:
	 *  
	 * MR: ClientDelay1 < 10 && GoogleDelay1 < 10 && MSNDelay1 < 10 && ClientDelay2 = ClientDelay1+1 && GoogleDelay2 = GoogleDelay1+1 && MSNDelay2 = MSNDelay1+1 => Resultados2 = Resultados1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr13(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Guardamos todas las demoras temporales
		 */
		
		List<Object> clientDelay1 = x.get((String)"Client_Delay");
		List<Object> googleDelay1 = x.get((String)"Google_Delay");
		List<Object> msnDelay1 = x.get((String)"MSN_Delay");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = clientDelay1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			int client1 = (Integer) clientDelay1.get(i);
			int google1 = (Integer) googleDelay1.get(i);
			int msn1 = (Integer) msnDelay1.get(i);
			
			if((client1 < 10) && (google1 < 10) && (msn1 < 10))
			{
				System.out.println("MR13 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("Client_Delay") || keyIn.equals("Google_Delay") || keyIn.equals("MSN_Delay"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val+1;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR13: " + newTestCases.size());
		
	}

	/**
	 * Funcion mr14 que simula la relacion metamorfica 14 del manual:
	 *  
	 * MR: ClientDelay1 > 0 && GoogleDelay1 > 0 && MSNDelay1 > 0 && ClientDelay2 = ClientDelay1-1 && GoogleDelay2 = GoogleDelay1-1 && MSNDelay2 = MSNDelay1-1 => Resultados2 = Resultados1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr14(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Guardamos todas las demoras temporales
		 */
		
		List<Object> clientDelay1 = x.get((String)"Client_Delay");
		List<Object> googleDelay1 = x.get((String)"Google_Delay");
		List<Object> msnDelay1 = x.get((String)"MSN_Delay");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = clientDelay1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos aquellos valores que coincidan y guardamos los indices para luego replicarlos, pero, con un valor menor en
			 * maxResult y count, que realmente es la variable numeric del fichero data.vm 
			 */
			int client1 = (Integer) clientDelay1.get(i);
			int google1 = (Integer) googleDelay1.get(i);
			int msn1 = (Integer) msnDelay1.get(i);
			
			if((client1 > 0) && (google1 > 0) && (msn1 > 0))
			{
				System.out.println("MR14 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("Client_Delay") || keyIn.equals("Google_Delay") || keyIn.equals("MSN_Delay"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val-1;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR14: " + newTestCases.size());
		
	}

	
	/*
	 * En esta MR lo que se va a obtener es otro conjunto de casos de prueba iguales a los originales, pero, 
	 * con la particularidad de que no tendran descripcion en los resultados de MSN. Por tanto, se trabajara 
	 * solo con aquellos casos de prueba que tengan resultados de MSN.
	 */
	/**
	 * Funcion mr15 que simula la relacion metamorfica 15 del manual:
	 *  
	 * MR: Consulta1 = Consulta2 && count(Descripciones2) = 0 && count(Descripciones1) = count(Resultados1) => Resultados2 = Resultados1

	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	@SuppressWarnings("unchecked")
	private void mr15(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		List<Object> consulta1 = new ArrayList<Object>();
		consulta1.addAll(x.get((String)"msnResponseValues"));
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = consulta1.size();
		System.out.println("\n\n Tam de casos de prueba: "+tam);
		
		
		for(int i=0;i<tam;i++)
		{
			/*
			 * En esta ocasion no se pone ninguna restriccion para elegir un caso de prueba u otro, ya que 
			 * no se debe cumplir ninguna condicion numerica o logica. Solo que contenga resultados y 
			 * descripciones por parte de MSN.
			 */
			
			/*
			 * Entramos en el primer nivel de la lista
			 */
			List<Object> aux = (List<Object>) consulta1.get(i);
			
			if(!(aux.isEmpty()))
			{
				/*
				 * Si no esta lo anterior vacio, entramos en el siguiente nivel en el que ya se encuentra el tipo de recurso,
				 * el offset y la lista de resultados por parte de MSN en ese caso.
				 */
				List<Object> aux2 = (List<Object>) aux.get(0);
				
				if(!(aux2.isEmpty()))
				{
					/*
					 * Si no esta vacio, ya nos encontramos en el nivel de los resultados, por lo que cada elemento será un resultado
					 * diferente para ese mismo caso de prueba.
					 */
					List<Object> aux3 = (List<Object>) aux2.get(2);
					
					/*
					 * Por tanto si no esta vacio, contendra descripciones y sera un caso de prueba al que aplicar la MR
					 */
					if(!(aux3.isEmpty()))
					{
						System.out.println("MR15 aplicada en el caso de prueba " + (i+1));
						newTestCases.add(i);
					}
				}
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = null;
				o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es la clave de los valores implicados en la condicion de la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("msnResponseValues"))
				{
					List<Object> temp = (List<Object>) clon.deepClone(o);
					
					/*
					 * Entramos en el nivel de los resultados.
					 */
					if(!(temp.isEmpty()))
					{
						List<Object> res = (List<Object>) temp.get(0);
						
						/*
						 * Entramos en el nivel de los valores de cada uno de los resultados
						 */
						if(!(res.isEmpty()))
						{
							List<Object> res1 = (List<Object>) res.get(2);
							
							/*
							 * Entramos en el nivel de las descripciones
							 */
							if(!(res1.isEmpty()))
							{
								for(int j = 0; j<res1.size(); j++)
								{
									List<Object> resultado = (List<Object>) res1.get(j);
									List<String> descript = (List<String>) resultado.get(1);
									
									/*
									 * Vaciamos el contenido de las descripciones
									 */
									descript.clear();
								}
							}
						}
					}
					/*
					 * Asignamos lo que hayamos hecho a aux para insertarlo mas adelante
					 */
					aux = temp;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR15: " + newTestCases.size());
	}

	/*
	 * En esta MR se van a buscar aquellos casos de prueba que tengan el valor 1 como Count y que el numero 
	 * de resultados que hayan aportado MSN y Google sean mayor que 1, pero, solo uno de ellos valido porque 
	 * el resto son repetidos.
	 */
	/**
	 * Funcion mr16 que simula la relacion metamorfica 16 del manual:
	 *  
	 * MR: Consulta1 = Consulta2 && count(Resultados1) > 1 && diferent(Resultados1) = 1 && count(Resultados2) = 1 => Count2 = Count1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	@SuppressWarnings("unchecked")
	private void mr16(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		List<Object> consulta1 = new ArrayList<Object>();
		consulta1.addAll(x.get((String)"msnResponseValues"));
		
		List<Object> consulta2 = new ArrayList<Object>();
		consulta2.addAll(x.get((String)"googleValues"));
		
		List<Object> count1 = new ArrayList<Object>();
		count1.addAll(x.get((String)"numeric"));
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = consulta1.size();
		System.out.println("\n\n Tam de casos de prueba: "+tam);
		
		
		for(int i=0;i<tam;i++)
		{	
			int resGoogle = 0;
			int resMSN = 0;
			int countI = (Integer) count1.get(i);
			
			if(countI == 1)
			{
				/*
				 * Entramos en el primer nivel de la lista
				 */
				List<Object> aux = (List<Object>) consulta1.get(i);
				
				if(!(aux.isEmpty()))
				{
					/*
					 * Si no esta lo anterior vacio, entramos en el siguiente nivel en el que ya se encuentra el tipo de recurso,
					 * el offset y la lista de resultados por parte de MSN en ese caso.
					 */
					List<Object> aux2 = (List<Object>) aux.get(0);
					
					/*
					 * Nivel de los resultados
					 */
					resMSN = aux2.size();
				}
				
				List<Object> temp = (List<Object>) consulta2.get(i);
				
				resGoogle = temp.size();
				
				if(resGoogle+resMSN > 1)
				{
					System.out.println("MR16 aplicada en el caso de prueba " + (i+1));
					newTestCases.add(i);
				}
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			boolean googleResult = true;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = null;
				o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es la clave de los valores implicados en la condicion de la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("googleValues"))
				{
					List<Object> temp = (List<Object>) clon.deepClone(o);
					
					/*
					 * Entramos en el nivel de los resultados.
					 */
					if(!(temp.isEmpty()))
					{
						/*
						 * Vamos limpiando todos los resultados excepto el primero
						 */
						for(int j = 1; j<temp.size(); j++)
						{
							List<Object> resultado = (List<Object>) temp.get(j);
							resultado.clear();
						}
					}
					else
						googleResult = false;
					

					aux = temp;
				}
				else if(keyIn.equals("msnResponseValues"))
				{
					List<Object> temp = (List<Object>) clon.deepClone(o);
					
					/*
					 * Entramos en el nivel de los resultados.
					 */
					if(!(temp.isEmpty()) && !googleResult)
					{
						List<Object> res = (List<Object>) temp.get(0);
						
						/*
						 * Entramos en el nivel de los valores de cada uno de los resultados
						 */
						if(!(res.isEmpty()))
						{
							List<Object> res1 = (List<Object>) res.get(2);
							
							/*
							 * Entramos en el nivel de las descripciones
							 */
							if(!(res1.isEmpty()))
							{
								/*
								 * Obtenemos los resultados a partir del segundo de ellos porque la condicion de 
								 * que hayamos entrado es que Google no tiene resultados, por tanto, si borramos 
								 * todos los de MSN, el caso de prueba fallara porque estara buscando un resultado 
								 * y no lo encontrara
								 */
								for(int j = 1; j<res1.size(); j++)
								{
									List<Object> resultado = (List<Object>) res1.get(j);
									resultado.clear();
								}
							}
						}
					}
					else
					{
						/*
						 * En caso contrario, querra decir que Google si tiene resultados y por tanto, lo unico que hay 
						 * que hacer es limpiar todos los datos.
						 */
						temp.clear();
					}
					/*
					 * Asignamos lo que hayamos hecho a aux para insertarlo mas adelante
					 */
					aux = temp;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR16: " + newTestCases.size());
		
	}


	/*
	 * Se trata de una MR en la cual se quiere comprobar que el valor de Count esta correctamente controlado 
	 * dentro de la composicion, ya que teniendo los resultados que sea y el valor de maxResult que sea, lo que 
	 * importa es el total de Count, que en este caso al ser siempre 0, la composicion no debe capturar ninguno 
	 * de los resultados que pueda tener Google o MSN.
	 */
	/**
	 * Funcion mr17 que simula la relacion metamorfica 17 del manual:
	 *  
	 * MR: Consulta1 = Consulta2 && maxResult1 = maxResult2 && Count2 = Count1*0 => count(Resultados2) = 0
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr17(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Variable en la que vamos a guardar todos los count de los casos de prueba que tenemos
		 */
		List<Object> count1 = x.get((String)"numeric");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = count1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todos los casos de prueba y lo añadimos como posibles originales a los que 
			 * poder aplicar esta MR, ya que no existe ninguna restriccion inicial. 
			 */
			System.out.println("MR17 aplicada en el caso de prueba " + (i+1));
			newTestCases.add(i);
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si se trata del valor de Count (numeric) aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				 if(keyIn.equals("numeric"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val*0;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR17: " + newTestCases.size());
		
	}

	/**
	 * Funcion que realiza la llamada de todas las MR implementadas
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mrAll(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		mr1(x,salida);
		mr2(x,salida);
		mr3(x,salida);
		mr4(x,salida);
		mr5(x,salida);
		mr6(x,salida);
		mr7(x,salida);
		mr8(x,salida);
		mr9(x,salida);
		mr10(x,salida);
		mr11(x,salida);
		mr12(x,salida);
		mr13(x,salida);
		mr14(x,salida);
		mr15(x,salida);
		mr16(x,salida);
		mr17(x,salida);
	}
	
	/**
	 * Funcion que genera String aleatorios con una longitud "tam" determinada. De especial utilidad en la MR2.
	 * 
	 * @param tam
	 * @return String 
	 */
	private String randString (int tam){
		String newString = "";
		Random r = new Random();
		int i = 0;
		
		while ( i < tam){
			char c = (char)r.nextInt(255);
			if ( (c >= '0' && c <='9') || (c >='A' && c <='Z') ){
				newString += c;
				i ++;
			}
		}
		return newString;
	}

}
