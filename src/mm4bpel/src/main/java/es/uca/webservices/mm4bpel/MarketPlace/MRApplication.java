package es.uca.webservices.mm4bpel.MarketPlace;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

import es.uca.webservices.testgen.formatters.VelocityFormatter;


/**
 * Clase principal desde la cual vamos a llamar a las relaciones metamórficas de la composición que estemos estudiando. 
 * 
 * @author Maria Azahara Camacho Magrinnan 
 *
 */
public final class MRApplication
{
	/**
	 * Función principal en la cual se abre el fichero que contiene el conjunto 
	 * de casos de prueba original y se comienza a trabajar con cada uno de ellos.
	 * @param args String[]
	 * @return void
	 */
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException
    {
    	System.out.println("Comienza la generación de nuevos casos de prueba...");
    	/*
	     * HashMap en el cual guardaremos cada una de las variables que contenga el fichero data.vm. La clave será el nombre de la variable
	     * y el contenido, todos los valores que tiene asignado en cada uno de los casos de prueba.
	     */
		HashMap<String,List<Object> > variables = new HashMap<String,List<Object>>();

		Velocity.init();
		final Context ctx = new VelocityContext();
		Velocity.evaluate(ctx, new StringWriter(),"data.vm", new FileReader(new File(System.getProperty("user.dir")+"/src/main/resources/java/MarketPlace/data.vm")));
    	
		for (Object key : ctx.getKeys()) {
			final String sKey = (String)key;
			Object value = ctx.get(sKey);
			
			if (value instanceof List) {
				//List<Object> value2 = (List<Object>)value;
				variables.put(sKey,(List<Object>) value);
			}
			else {
				List<Object> aux = new ArrayList<Object>();
				aux.add(value);
				variables.put(sKey,aux);				
			}
		}
		
		/*
		 * Llamamos a la función "generate()" para que obtenga todos y cada uno de los casos de prueba que 
		 * indique el usuario.
		 */
		testCasesMarketPlace i = new testCasesMarketPlace();
		Map<String,List<Object>> generados = i.generate(variables);
		
		int newValues = 0;
		
		if(!generados.isEmpty())
			newValues = generados.get((String)generados.keySet().iterator().next()).size();
			
		int oldValues = variables.get((String)variables.keySet().iterator().next()).size();
		
		System.out.println("\n                 ---RESULTADOS---");
		System.out.println("Nº de casos de prueba del conjunto original: " + oldValues);
		System.out.println("Nº de casos de prueba del conjunto siguiente: " + newValues);
		
		/*
		 * Generacion del nuevo fichero donde se va a plasmar el contexto inicial del fichero 
		 * Velocity junto a los valores generados.
		 */
		VelocityFormatter vf = new VelocityFormatter();
		
		String ruta = System.getProperty("user.dir")+"/src/main/resources/java/MarketPlace/data2.vm";
		FileOutputStream salida = new FileOutputStream(new File(ruta));
		
		vf.save(salida,generados);
		
		salida.close();
		
		System.out.printf("\nHa acabado la aplicación de relaciones metamórficas\n");
		
    }
}