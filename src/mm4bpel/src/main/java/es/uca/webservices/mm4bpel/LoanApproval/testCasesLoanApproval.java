package es.uca.webservices.mm4bpel.LoanApproval;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.rits.cloning.Cloner;


/**
 * Clase que implementa todas las MR de la composicion LoanApproval, junto a la eliminacion de repeticiones.
 * 
 * @author Maria Azahara Camacho Magrinnan
 *
 */
public class testCasesLoanApproval{

	public testCasesLoanApproval(){}
	
	/**
	 * Funcion que controla la eleccion del usuario en cuanto a MR que quiere aplicar
	 * @param x Conjunto de casos de prueba originales
	 * @return Devuelve el conjunto de casos de prueba siguientes generados a partir del original "x"
	 * @throws IOException
	 */
	public Map<String,List<Object>> generate(HashMap<String, List<Object>> x) throws IOException {
		
		HashMap<String,List<Object> > salida = new HashMap<String,List<Object>>();
		
		System.out.println("\nIndique el numero de la relacion metamorfica que quiere aplicar en el rango 1-12");
		System.out.println("\n-EJEMPLO: para la MR1, introduzca el valor \"1\"");
		System.out.println("-En el caso especial de querer aplicar todas las MR introduzca el valor \"0\"");
		System.out.println("\tU otro que se encuentre fuera del rango establecido.");
		System.out.printf("\n\t\tNumero de la MR a aplicar: ");
		
		BufferedReader lectura = new BufferedReader(new InputStreamReader(System.in));
		String mr = lectura.readLine();
		
		System.out.println("\n¿Desea incluir casos de prueba repetidos en el conjunto final? ");
		System.out.println("\t Si: introduzca \"s\"");
		System.out.println("\t No: introduzca \"n\"");
		
		boolean correct = false;
		String repetidos = null;
		
		while(!correct)
		{
			System.out.printf("\n\t\tOpcion elegida: ");
			BufferedReader lectura2 = new BufferedReader(new InputStreamReader(System.in));
			repetidos = lectura2.readLine();
			
			if(!repetidos.equals("s") && !repetidos.equals("n"))
			{
				System.out.println("\nOPCION INCORRECTA.");
				System.out.println("\nVUELVA A INTENTARLO CON UNA OPCION CORRECTA ('s' o 'n').");
			}
			else
				correct = true;
		}
		
		System.out.println("Total de casos de prueba con los que comenzamos el estudio: " + x.get((String)x.keySet().iterator().next()).size());
		
		// Lo pasamos a entero para comparar la relacion metamorfica que se debe aplicar
		int mrInt = Integer.parseInt(mr);
		
		switch (mrInt){
			case 1:
				mr1(x,salida);
				break;
			case 2:
				mr2(x,salida);
				break;
			case 3:
				mr3(x,salida);
				break;
			case 4:
				mr4(x,salida);
				break;
			case 5:
				mr5(x,salida);
				break;
			case 6:
				mr6(x,salida);
				break;
			case 7:
				mr7(x,salida);
				break;
			case 8:
				mr8(x,salida);
				break;
			case 9:
				mr9(x,salida);
				break;
			case 10:
				mr10(x,salida);
				break;
			case 11:
				mr11(x,salida);
				break;
			case 12:
				mr12(x,salida);
				break;
			default:
				mrAll(x,salida);
				break;
		}
		
		/*
		 * Guardamos en el map "finales" el conjunto completo de casos de prueba generados, teniendo en cuenta la opcion elegida por el usuario 
		 * al comenzar la ejecucion anteriormente.
		 */
		Map<String,List<Object>> finales = null;
		if(repetidos.equals("s"))
		{
			 finales = salida;
		}
		else if(repetidos.equals("n"))
		{
			finales = noRepeat(x,salida);
		}
		
		return finales;
	}
	
	/**
	 * Funcion que elimina los repetidos del conjunto de casos de prueba que se le pasa como map, tanto generados repetidos entre sí, como
	 * los que ya se encuentran en el conjunto de casos de prueba original.
	 * @param x
	 * @param salida
	 * @return Map que contiene aquellos casos de prueba siguiente que se han generado sin repeticiones.
	 */
	@SuppressWarnings("unchecked")
	private Map<String, List<Object>> noRepeat(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		if(!salida.isEmpty())
		{
			/*
			 * Adaptamos todos los valores que hay en el map de entrada 'x' como casos de prueba pertenecientes al 
			 * conjunto de casos de prueba original.
			 */
			Collection<List<Object>> aux = x.values();
			List<List<Object>> originales = new ArrayList<List<Object>>(aux);
						
			List<List<Object>> casosO = new ArrayList<List<Object>>();
			for(int i=0; i < originales.get(0).size(); i++)
			{
				List<Object> caso = new ArrayList<Object>();
				for(int j = 0; j<originales.size(); ++j){
					caso.add(originales.get(j).get(i));
				}
				casosO.add(caso);
			}
			
			/*
			 * Adaptamos todos los valores que hay en el map de nuevos casos 'salida' como casos de prueba pertenecientes al 
			 * conjunto de casos de prueba siguiente.
			 */
			Collection<List<Object>> temp = salida.values();
			List<List<Object>> valores = new ArrayList<List<Object>>(temp);
						
			List<List<Object>> casosS = new ArrayList<List<Object>>();
			for(int i=0; i < valores.get(0).size(); i++)
			{
				List<Object> caso = new ArrayList<Object>();
				for(int j = 0; j<valores.size(); ++j){
					caso.add(valores.get(j).get(i));
				}
				casosS.add(caso);
			}
			
			/*
			 * Vamos annadiendo a una lista de casos de prueba todos aquellos que no esten repetidos ya en el conjunto 
			 * original o en el propio conjunto generado como nuevos casos de prueba.
			 */
			List<List<Object>> casosF = new ArrayList<List<Object>>();
			
			for(int i=0; i<casosS.size(); ++i){
				if(!casosO.contains(casosS.get(i)) && !casosF.contains(casosS.get(i)))
					casosF.add(casosS.get(i));
			}
			
			
			/*
			 * Ahora, trasladamos todos los valores guardados en el conjunto 'casosF' a un map junto a la clave que le 
			 * corresponde a cada uno de los valores.
			 */
			/*
			 * Map final que devolveremos con los casos de prueba sin repetir
			 */
			Map<String,List<Object> > conjuntoFinal = new HashMap<String, List<Object>>(x);
			
			/*
			 * Obtenemos el conjunto de claves que conforman el map. No lo obtenemos a traves de la funcion "keySet()" debido 
			 * a que nos devuelve un set de valores ordenados de forma diferente a como lo necesitamos para que se correspondan 
			 * los valores obtenidos.
			 */
			List<String> claves = new ArrayList<String>();
			Iterator<String> iterIn = conjuntoFinal.keySet().iterator();
			while (iterIn.hasNext()) {
			    String keyIn = (String) iterIn.next();
			    claves.add(keyIn);
			}		    
			
			
			/*
			 * Transformamos los casos de prueba en listas de valores para cada una de las claves.
			 */
			List<Object> vals = new ArrayList<Object>();
			
			for(int i = 0; i<casosF.get(0).size(); ++i){
				
				List<Object> claveVal = new ArrayList<Object>();
				
				for(int j=0; j<casosF.size(); ++j){
					claveVal.add(casosF.get(j).get(i));
				}
				
				vals.add(claveVal);
			}
			/*
			 * El numero de elementos de 'vals' y 'claves' es el mismo, ya que uno representa las listas de valores de cada clave y 'claves'
			 * todas las keys que va a contener el map final. Asignamos en el map final a cada clave, su valor correspondiente. 
			 */
			
			for(int i=0; i<claves.size(); ++i){
				conjuntoFinal.put(claves.get(i), (List<Object>) vals.get(i));
			}
			
			return conjuntoFinal;
			
		}
		else
			return salida;
	}
	
	/**
	 * Funcion mr1 que simula la relacion metamorfica 1 del manual:
	 *   
	 * MR: req_amount1 ∗ 10 > 10000 && req_amount2 = req_amount1 ∗ 10 && ap_reply2 = not(ap_reply1) && 
	 * 			as_reply2 = as_reply1 => accepted2 = accepted1 && ap_reply2 = accepted2
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba
	 */
	private void mr1(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		/*
		 * Lista en la que vamos a guardar todas las respuestas de los aprobadores en los casos de prueba.
		 */
		List<Object> apReply1 = x.get((String)"ap_reply");
		/*
		 * Lista en la que vamos a guardar todas las resoluciones de los casos de prueba
		 */
		List<Object> accepted1 = x.get((String)"accepted");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 *10 > 10000). Comprobamos ademas que los valores entre la respuesta del aprobador y 
			 * la situacion final del caso de prueba sean opuestas porque es lo que hara que el caso de prueba siguiente tenga ese 
			 * valor opuesto.
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			String apR1 = (String) apReply1.get(i);
			String acp1 = (String) accepted1.get(i);
			
			if((rA1*10 > 10000) && ((apR1.equals("true") && acp1.equals("false")) || (apR1.equals("false") && acp1.equals("true"))))
			{
				System.out.println("MR1 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val*10;
				}
				else if(keyIn.equals("ap_reply"))
				{
					String val = (String) clon.deepClone(o);
					
					if(val.equals("true"))
						aux = "false";
					else if(val.equals("false"))
						aux = "true";
					else
						aux = clon.deepClone(o);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR1: " + newTestCases.size());		
	}


	/**
	 * Funcion mr2 que simula la relacion metamorfica 2 del manual:
	 *  
	 * MR: req_amount1 ∗ 10 > 10000 && req_amount2 = req_amount1 ∗ 10 && ap_reply2 = not(ap_reply1) && 
	 * 			as_reply2 = as_reply1 => accepted2 = not(accepted1) && ap_reply2 = accepted2
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr2(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		/*
		 * Lista en la que vamos a guardar todas las respuestas de los aprobadores en los casos de prueba.
		 */
		List<Object> apReply1 = x.get((String)"ap_reply");
		/*
		 * Lista en la que vamos a guardar todas las resoluciones de los casos de prueba
		 */
		List<Object> accepted1 = x.get((String)"accepted");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 *10 > 10000). En este caso comprobamos tambien que los valores entre la respuesta del 
			 * aprobador y la respuesta final sean iguales, ya que es lo que nos garantizara que el valor de accepted en el caso de 
			 * prueba siguiente sea opuesto.
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			String apR1 = (String) apReply1.get(i);
			String acp1 = (String) accepted1.get(i);
			
			if((rA1*10 > 10000) && ((apR1.equals("true") && acp1.equals("true")) || (apR1.equals("false") && acp1.equals("false"))))
			{
				System.out.println("MR2 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val*10;
				}
				else if(keyIn.equals("ap_reply") || keyIn.equals("accepted"))
				{
					String val = (String) clon.deepClone(o);
					
					if(val.equals("true"))
						aux = "false";
					else if(val.equals("false"))
						aux = "true";
					else
						aux = clon.deepClone(o);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los Total de casos de prueba generados con valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR2: " + newTestCases.size());	
		
	}


	
	/**
	 * Funcion mr3 que simula la relacion metamorfica 3 del manual:
	 * 
	 * MR: req_amount1 ∗ 10 <= 10000 && req_amount2 = req_amount1 ∗ 10 && ap_reply2 = ap_reply1 &&
	 * 			as_reply2 = as_reply1 => accepted2 = accepted1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr3(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 *10 <= 10000).
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			
			if(rA1*10 <= 10000)
			{
				System.out.println("MR3 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val*10;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR3: " + newTestCases.size());	
	}


	
	/**
	 * Funcion mr4 que simula la relacion metamorfica 4 del manual:
	 *   
	 * MR: req_amount1 ∗ 2 > 10000 && req_amount2 = req_amount1 ∗ 2 && ap_reply2 = not(ap_reply1) && 
	 * 			as_reply2 = as_reply1 => accepted2 = accepted1 && ap_reply2 = accepted2
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba
	 */
	private void mr4(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		/*
		 * Lista en la que vamos a guardar todas las respuestas de los aprobadores en los casos de prueba.
		 */
		List<Object> apReply1 = x.get((String)"ap_reply");
		/*
		 * Lista en la que vamos a guardar todas las resoluciones de los casos de prueba
		 */
		List<Object> accepted1 = x.get((String)"accepted");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 *2 > 10000). Comprobamos ademas que los valores entre la respuesta del aprobador y 
			 * la situacion final del caso de prueba sean opuestas porque es lo que hara que el caso de prueba siguiente tenga ese 
			 * valor opuesto.
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			String apR1 = (String) apReply1.get(i);
			String acp1 = (String) accepted1.get(i);
			
			if((rA1*2 > 10000) && ((apR1.equals("true") && acp1.equals("false")) || (apR1.equals("false") && acp1.equals("true"))))
			{
				System.out.println("MR4 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val*2;
				}
				else if(keyIn.equals("ap_reply"))
				{
					String val = (String) clon.deepClone(o);
					
					if(val.equals("true"))
						aux = "false";
					else if(val.equals("false"))
						aux = "true";
					else
						aux = clon.deepClone(o);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR4: " + newTestCases.size());		
	}


	
	/**
	 * Funcion mr5 que simula la relacion metamorfica 5 del manual:
	 *  
	 * MR: req_amount1 ∗ 2 > 10000 && req_amount2 = req_amount1 ∗ 2 && ap_reply2 = not(ap_reply1) && 
	 * 			as_reply2 = as_reply1 => accepted2 = not(accepted1) && ap_reply2 = accepted2
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr5(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		/*
		 * Lista en la que vamos a guardar todas las respuestas de los aprobadores en los casos de prueba.
		 */
		List<Object> apReply1 = x.get((String)"ap_reply");
		/*
		 * Lista en la que vamos a guardar todas las resoluciones de los casos de prueba
		 */
		List<Object> accepted1 = x.get((String)"accepted");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 *2 > 10000). En este caso comprobamos tambien que los valores entre la respuesta del 
			 * aprobador y la respuesta final sean iguales, ya que es lo que nos garantizara que el valor de accepted en el caso de 
			 * prueba siguiente sea opuesto.
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			String apR1 = (String) apReply1.get(i);
			String acp1 = (String) accepted1.get(i);
			
			if((rA1*2 > 10000) && ((apR1.equals("true") && acp1.equals("true")) || (apR1.equals("false") && acp1.equals("false"))))
			{
				System.out.println("MR5 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val*2;
				}
				else if(keyIn.equals("ap_reply") || keyIn.equals("accepted"))
				{
					String val = (String) clon.deepClone(o);
					
					if(val.equals("true"))
						aux = "false";
					else if(val.equals("false"))
						aux = "true";
					else
						aux = clon.deepClone(o);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR5: " + newTestCases.size());	
		
	}


	
	/**
	 * Funcion mr6 que simula la relacion metamorfica 6 del manual:
	 * 
	 * MR: req_amount1 ∗ 2 <= 10000 && req_amount2 = req_amount1 ∗ 2 && ap_reply2 = ap_reply1 &&
	 * 			as_reply2 = as_reply1 => accepted2 = accepted1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr6(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 *2 <= 10000).
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			
			if(rA1*2 <= 10000)
			{
				System.out.println("MR6 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val*2;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR6: " + newTestCases.size());	
	}


	/**
	 * Funcion mr7 que simula la relacion metamorfica 7 del manual:
	 *   
	 * MR: req_amount1 + 1 > 10000 && req_amount2 = req_amount1 + 1 && ap_reply2 = not(ap_reply1) && 
	 * 			as_reply2 = as_reply1 => accepted2 = accepted1 && ap_reply2 = accepted2
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba
	 */
	private void mr7(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		/*
		 * Lista en la que vamos a guardar todas las respuestas de los aprobadores en los casos de prueba.
		 */
		List<Object> apReply1 = x.get((String)"ap_reply");
		/*
		 * Lista en la que vamos a guardar todas las resoluciones de los casos de prueba
		 */
		List<Object> accepted1 = x.get((String)"accepted");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 + 1 > 10000). Comprobamos ademas que los valores entre la respuesta del aprobador y 
			 * la situacion final del caso de prueba sean opuestas porque es lo que hara que el caso de prueba siguiente tenga ese 
			 * valor opuesto.
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			String apR1 = (String) apReply1.get(i);
			String acp1 = (String) accepted1.get(i);
			
			if((rA1+1 > 10000) && ((apR1.equals("true") && acp1.equals("false")) || (apR1.equals("false") && acp1.equals("true"))))
			{
				System.out.println("MR7 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val+1;
				}
				else if(keyIn.equals("ap_reply"))
				{
					String val = (String) clon.deepClone(o);
					
					if(val.equals("true"))
						aux = "false";
					else if(val.equals("false"))
						aux = "true";
					else
						aux = clon.deepClone(o);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR7: " + newTestCases.size());		
	}


	
	/**
	 * Funcion mr8 que simula la relacion metamorfica 8 del manual:
	 *  
	 * MR: req_amount1 + 1 > 10000 && req_amount2 = req_amount1 + 1 && ap_reply2 = not(ap_reply1) && 
	 * 			as_reply2 = as_reply1 => accepted2 = not(accepted1) && ap_reply2 = accepted2
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr8(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		/*
		 * Lista en la que vamos a guardar todas las respuestas de los aprobadores en los casos de prueba.
		 */
		List<Object> apReply1 = x.get((String)"ap_reply");
		/*
		 * Lista en la que vamos a guardar todas las resoluciones de los casos de prueba
		 */
		List<Object> accepted1 = x.get((String)"accepted");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 + 1 > 10000). En este caso comprobamos tambien que los valores entre la respuesta del 
			 * aprobador y la respuesta final sean iguales, ya que es lo que nos garantizara que el valor de accepted en el caso de 
			 * prueba siguiente sea opuesto.
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			String apR1 = (String) apReply1.get(i);
			String acp1 = (String) accepted1.get(i);
			
			if((rA1+1 > 10000) && ((apR1.equals("true") && acp1.equals("true")) || (apR1.equals("false") && acp1.equals("false"))))
			{
				System.out.println("MR8 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val+1;
				}
				else if(keyIn.equals("ap_reply") || keyIn.equals("accepted"))
				{
					String val = (String) clon.deepClone(o);
					
					if(val.equals("true"))
						aux = "false";
					else if(val.equals("false"))
						aux = "true";
					else
						aux = clon.deepClone(o);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR8: " + newTestCases.size());	
		
	}


	
	/**
	 * Funcion mr9 que simula la relacion metamorfica 9 del manual:
	 * 
	 * MR: req_amount1 + 1 <= 10000 && req_amount2 = req_amount1 + 1 && ap_reply2 = ap_reply1 &&
	 * 			as_reply2 = as_reply1 => accepted2 = accepted1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr9(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 + 1 <= 10000).
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			
			if(rA1+1 <= 10000)
			{
				System.out.println("MR9 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val+1;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR9: " + newTestCases.size());	
	}


	/**
	 * Funcion mr10 que simula la relacion metamorfica 10 del manual:
	 *   
	 * MR: req_amount1 - 1 > 10000 && req_amount2 = req_amount1 - 1 && ap_reply2 = not(ap_reply1) && 
	 * 			as_reply2 = as_reply1 => accepted2 = accepted1 && ap_reply2 = accepted2
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba
	 */
	private void mr10(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		/*
		 * Lista en la que vamos a guardar todas las respuestas de los aprobadores en los casos de prueba.
		 */
		List<Object> apReply1 = x.get((String)"ap_reply");
		/*
		 * Lista en la que vamos a guardar todas las resoluciones de los casos de prueba
		 */
		List<Object> accepted1 = x.get((String)"accepted");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 - 1 > 10000). Comprobamos ademas que los valores entre la respuesta del aprobador y 
			 * la situacion final del caso de prueba sean opuestas porque es lo que hara que el caso de prueba siguiente tenga ese 
			 * valor opuesto.
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			String apR1 = (String) apReply1.get(i);
			String acp1 = (String) accepted1.get(i);
			
			if((rA1-1 > 10000) && ((apR1.equals("true") && acp1.equals("false")) || (apR1.equals("false") && acp1.equals("true"))))
			{
				System.out.println("MR10 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val-1;
				}
				else if(keyIn.equals("ap_reply"))
				{
					String val = (String) clon.deepClone(o);
					
					if(val.equals("true"))
						aux = "false";
					else if(val.equals("false"))
						aux = "true";
					else
						aux = clon.deepClone(o);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR10: " + newTestCases.size());		
	}


	
	/**
	 * Funcion mr11 que simula la relacion metamorfica 11 del manual:
	 *  
	 * MR: req_amount1 - 1 > 10000 && req_amount2 = req_amount1 - 1 && ap_reply2 = not(ap_reply1) && 
	 * 			as_reply2 = as_reply1 => accepted2 = not(accepted1) && ap_reply2 = accepted2
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr11(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		/*
		 * Lista en la que vamos a guardar todas las respuestas de los aprobadores en los casos de prueba.
		 */
		List<Object> apReply1 = x.get((String)"ap_reply");
		/*
		 * Lista en la que vamos a guardar todas las resoluciones de los casos de prueba
		 */
		List<Object> accepted1 = x.get((String)"accepted");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 - 1 > 10000). En este caso comprobamos tambien que los valores entre la respuesta del 
			 * aprobador y la respuesta final sean iguales, ya que es lo que nos garantizara que el valor de accepted en el caso de 
			 * prueba siguiente sea opuesto.
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			String apR1 = (String) apReply1.get(i);
			String acp1 = (String) accepted1.get(i);
			
			if((rA1-1 > 10000) && ((apR1.equals("true") && acp1.equals("true")) || (apR1.equals("false") && acp1.equals("false"))))
			{
				System.out.println("MR11 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val-1;
				}
				else if(keyIn.equals("ap_reply") || keyIn.equals("accepted"))
				{
					String val = (String) clon.deepClone(o);
					
					if(val.equals("true"))
						aux = "false";
					else if(val.equals("false"))
						aux = "true";
					else
						aux = clon.deepClone(o);
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR11: " + newTestCases.size());	
		
	}


	
	/**
	 * Funcion mr12 que simula la relacion metamorfica 12 del manual:
	 * 
	 * MR: req_amount1 - 1 <= 10000 && req_amount2 = req_amount1 - 1 && ap_reply2 = ap_reply1 &&
	 * 			as_reply2 = as_reply1 => accepted2 = accepted1
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mr12(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		
		/*
		 * Lista en la que vamos a guardar todas las cantidades requeridas por los usuarios.
		 */
		List<Object> reqAmount1 = x.get((String)"req_amount");
		
		/*
		 * Lista en la que vamos a recoger los indices de los casos de prueba que cumplen las 
		 * condiciones de esta MR
		 */
		List<Integer> newTestCases = new ArrayList<Integer>();
		
		int tam = reqAmount1.size();
		System.out.println("\n\n");
		
		
		for(int i=0;i<tam;i++)
		{
			/* 
			 * Recorremos todas las cantidades y guardamos los indices de aquellos casos en los que se cumplan las condiciones 
			 * establecidas en la MR (reqAmoun1 - 1 <= 10000).
			 */
			int rA1 = (Integer) reqAmount1.get(i);
			
			if(rA1-1 <= 10000)
			{
				System.out.println("MR12 aplicada en el caso de prueba " + (i+1));
				newTestCases.add(i);
			}
		}
		System.out.println("\n\n-----------------------------------------\n\n");
		int values = newTestCases.size();

		
		/*
		 * Bucle con el cual vamos a ir recorriendo todos los casos de prueba que tenemos, pero, 
		 * trabajando unicamente con aquellos casos de prueba que han cumplido las condiciones 
		 * anteriores. Es decir, solo vamos a tomar los casos de prueba cuyos indices hemos 
		 * guardado.
		 */
		for(int i = 0; i<values; i++){		
			
			System.out.println("Nuevo caso de prueba derivado del caso de prueba: " + (newTestCases.get(i)+1));
			System.out.println("=======================================================\n");
			
			// Variables para los valores nuevos a añadir (salida)
			Iterator<String> iter = salida.keySet().iterator();
			List<Object> value;	
		
			// Variables para los valores ya añadidos (x)
			Iterator<String> iterIn = x.keySet().iterator();
			String keyIn;
			List<Object> valueIn;
			
			// Mientras haya claves que leer, continuan las iteraciones
			while (iterIn.hasNext()) {
				
			    // Guardamos clave y contenido de x
			    keyIn = (String) iterIn.next();
				valueIn = x.get(keyIn);
				
				/*
				 * Si hay iterador siguiente para el map en el que vamos a guardar los nuevos casos de prueba, 
				 * guardamos clave y valor, en caso contrario, inicializamos la variable value como una lista 
				 */
				if(iter.hasNext())
				{					
					value = salida.get(keyIn);
				}
				else
				{
					value = new ArrayList<Object>();
				}
				
				/*
				 * Guardamos el objeto que se corresponde con una de las partes del caso de prueba que hay guardado en i
				 */
				Object o = valueIn.get(newTestCases.get(i));
				Object aux = null;
				
				Cloner clon = new Cloner();
					
				/*
				 * Si es uno de los valores implicados en la MR aplicamos la operacion
				 * correspondiente, en caso contrario, simplemente se lo asignamos a la variable aux para no 
				 * tener problemas en el codigo de despues.
				 */
				if(keyIn.equals("req_amount"))
				{
					int val = (Integer) clon.deepClone(o);
					aux = val-1;
				}
				else
				{
					aux = clon.deepClone(o);
				}
				
				/*
				 * Asignamos los nuevos valores que se van a guardar en el map de salida
				 */
				value.add(aux);
				salida.put(keyIn,value);
					
				System.out.println("Valor en " + keyIn + " : " + aux);
			}
			System.out.println("\n\n-----------------------------------------\n\n");
		}
		System.out.println("Total de casos de prueba generados con la MR12: " + newTestCases.size());	
	}

	/**
	 * Funcion que realiza la llamada de todas las MR implementadas
	 * 
	 * @param x - map que contiene el conjunto de casos de prueba inicial
	 * @param salida - map en el cual se van a guardar los nuevos casos de prueba 
	 */
	private void mrAll(Map<String, List<Object>> x, Map<String, List<Object>> salida) {
		mr1(x,salida);
		mr2(x,salida);
		mr3(x,salida);
		mr4(x,salida);
		mr5(x,salida);
		mr6(x,salida);
		mr7(x,salida);
		mr8(x,salida);
		mr9(x,salida);
		mr10(x,salida);
		mr11(x,salida);
		mr12(x,salida);
	}
}
