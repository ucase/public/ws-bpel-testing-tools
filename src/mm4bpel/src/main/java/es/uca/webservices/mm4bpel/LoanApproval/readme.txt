Composición: LoanApproval

Versión: 3-Velocity


Clases actuales:
	1_MRApplication -> Main del programa en el cual se lee del fichero Velocity "data.vm" los casos de prueba con los que vamos a trabajar. En versiones anteriores existía esta clase, pero, funcionaba de manera diferente: trabajaba con fichero CSV y no utilizaba ninguna de las bibliotecas de Java que la complementan ahora para formatear y poder trabajar con datos Velocity.
	
	2_testCasesMetaSearch -> Clase que se encarga de implementar todas las MR de la composición. Tienen una primera función "generate()" que le solicita al usuario la MR que quiere aplicar, si una en concreto de las que ya están implementadas o todas ellas juntas. También posee una función para controlar que el conjunto de casos de prueba siguientes no posea ningún caso de prueba repetido, ya sea respecto al conjunto de casos de prueba originales, como respecto de los casos de prueba generado.
	
	
Clases de versiones previas:
	1_Composicion -> Clase que no se utiliza en esta versión del software, pero, que previamente debido al formato CSV era necesario. Su función principal era la división y clasificación de los datos obtenidos del CSV y llamar a la clase "caso" con dichos datos y poner en funcionamiento todas las MR que se encontraban implementadas.
	
	2_Caso -> Clase que no se utiliza en esta versión del software, y que previamente se utilizaba para implementar todas las MR juntas, sin distinciones claras de qué funcionalidad tenía cada una ellas. En esta nueva versión se ha decidido implementarlas en la clase "testCasesMetaSearch" de forma que se vean bien diferenciadas y dándole la posibilidad al usuario de decidir cuál/es es la que quiere aplicar y no como ocurría en esta versión anterior, que obtenía un fichero con todas las MR aplicadas sin opción de poder elegir.
	

Ficheros en el directorio "resources":
	1_data.vm -> Fichero que contiene los casos de prueba que contiene el fichero data.csv, pero, en formato Velocity.
