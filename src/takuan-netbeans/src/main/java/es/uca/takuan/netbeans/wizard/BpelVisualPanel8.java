/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package es.uca.takuan.netbeans.wizard;

import es.uca.takuan.client.Remote;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public final class BpelVisualPanel8 extends JPanel {
    private Remote remote;
    private Project project;

    /** Creates new form BpelVisualPanel8 */
    public BpelVisualPanel8() {
        initComponents();
    }

    @Override
    public String getName() {
        return NbBundle.getMessage(BpelVisualPanel8.class, "WIZARD_RESULTS_TITLE");
    }

    /** Set the project
     * @param p
     */
    void setProject(Project p) {
        project = p;
    }

    /** Set the remote object
     * @param r
     */
    void setRemote(Remote r) {
        remote = r;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblDescription = new javax.swing.JLabel();
        panelInvariants = new javax.swing.JPanel();
        lblInvariants = new javax.swing.JLabel();
        cmdInvView = new javax.swing.JButton();
        cmdInvProject = new javax.swing.JButton();
        cmdInvSaveExternal = new javax.swing.JButton();
        panelBranch = new javax.swing.JPanel();
        lblBranch = new javax.swing.JLabel();
        cmdCoverageView = new javax.swing.JButton();
        cmdCoverageProject = new javax.swing.JButton();
        cmdCoverageSaveExternal = new javax.swing.JButton();

        lblDescription.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(lblDescription, org.openide.util.NbBundle.getMessage(BpelVisualPanel8.class, "WIZARD_RESULTS_DESCRIPTION")); // NOI18N
        lblDescription.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        panelInvariants.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(BpelVisualPanel8.class, "WIZARD_RESULTS_INVARIANTS"))); // NOI18N

        lblInvariants.setIcon(new javax.swing.ImageIcon(getClass().getResource("/es/uca/takuan/netbeans/info.png"))); // NOI18N

        cmdInvView.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(cmdInvView, org.openide.util.NbBundle.getMessage(BpelVisualPanel8.class, "WIZARD_RESULTS_VIEW")); // NOI18N
        cmdInvView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdInvViewActionPerformed(evt);
            }
        });

        cmdInvProject.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(cmdInvProject, org.openide.util.NbBundle.getMessage(BpelVisualPanel8.class, "WIZARD_RESULTS_ADD_PROJECT")); // NOI18N
        cmdInvProject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdInvProjectActionPerformed(evt);
            }
        });

        cmdInvSaveExternal.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(cmdInvSaveExternal, org.openide.util.NbBundle.getMessage(BpelVisualPanel8.class, "WIZARD_RESULTS_SAVE_EXTERNAL")); // NOI18N
        cmdInvSaveExternal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdInvSaveExternalActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout panelInvariantsLayout = new org.jdesktop.layout.GroupLayout(panelInvariants);
        panelInvariants.setLayout(panelInvariantsLayout);
        panelInvariantsLayout.setHorizontalGroup(
            panelInvariantsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelInvariantsLayout.createSequentialGroup()
                .addContainerGap()
                .add(lblInvariants)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cmdInvView, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cmdInvProject)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cmdInvSaveExternal)
                .addContainerGap())
        );
        panelInvariantsLayout.setVerticalGroup(
            panelInvariantsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelInvariantsLayout.createSequentialGroup()
                .add(panelInvariantsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(lblInvariants)
                    .add(panelInvariantsLayout.createSequentialGroup()
                        .add(22, 22, 22)
                        .add(panelInvariantsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(cmdInvSaveExternal)
                            .add(cmdInvProject)
                            .add(cmdInvView))))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        panelBranch.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(BpelVisualPanel8.class, "WIZARD_RESULTS_BRANCH"))); // NOI18N

        lblBranch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/es/uca/takuan/netbeans/branch.png"))); // NOI18N

        cmdCoverageView.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(cmdCoverageView, org.openide.util.NbBundle.getMessage(BpelVisualPanel8.class, "WIZARD_RESULTS_VIEW")); // NOI18N
        cmdCoverageView.setEnabled(false);
        cmdCoverageView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdCoverageViewActionPerformed(evt);
            }
        });

        cmdCoverageProject.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(cmdCoverageProject, org.openide.util.NbBundle.getMessage(BpelVisualPanel8.class, "WIZARD_RESULTS_ADD_PROJECT")); // NOI18N
        cmdCoverageProject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdCoverageProjectActionPerformed(evt);
            }
        });

        cmdCoverageSaveExternal.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(cmdCoverageSaveExternal, org.openide.util.NbBundle.getMessage(BpelVisualPanel8.class, "WIZARD_RESULTS_SAVE_EXTERNAL")); // NOI18N
        cmdCoverageSaveExternal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdCoverageSaveExternalActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout panelBranchLayout = new org.jdesktop.layout.GroupLayout(panelBranch);
        panelBranch.setLayout(panelBranchLayout);
        panelBranchLayout.setHorizontalGroup(
            panelBranchLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelBranchLayout.createSequentialGroup()
                .addContainerGap()
                .add(lblBranch)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cmdCoverageView, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cmdCoverageProject)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cmdCoverageSaveExternal)
                .addContainerGap())
        );
        panelBranchLayout.setVerticalGroup(
            panelBranchLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelBranchLayout.createSequentialGroup()
                .add(panelBranchLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(lblBranch)
                    .add(panelBranchLayout.createSequentialGroup()
                        .add(22, 22, 22)
                        .add(panelBranchLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(cmdCoverageSaveExternal)
                            .add(cmdCoverageProject)
                            .add(cmdCoverageView))))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(panelInvariants, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(lblDescription, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                    .add(panelBranch, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(lblDescription, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 38, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panelInvariants, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panelBranch, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(13, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    /** Commons **/

    /** Opens the editor
     * @param source The source File
     */
    private void editFile(File source) {
        try {
            FileObject file = FileUtil.toFileObject(source);
            DataObject dobj = DataObject.find(file);
            EditorCookie ec = (EditorCookie)dobj.getCookie(EditorCookie.class);
            ec.open();
        } catch (DataObjectNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    /** Copies a file to another one
     * @param source The source File
     */
    private void saveExternal(File source, String ext) {
        JFileChooser saveDialog = new JFileChooser();

        saveDialog.setMultiSelectionEnabled(false);
        if(ext != null)
            saveDialog.setFileFilter(new ExtensionFileFilter(new String [] {ext}));

        if(saveDialog.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
        {
            try {

                InputStream input   = null;
                OutputStream output = null;
                byte[] buffer       = new byte[256];

                input  = new FileInputStream(source);
                output = new FileOutputStream(saveDialog.getSelectedFile());

                int n;

                while ((n = input.read(buffer)) > 0) {
                    output.write(buffer, 0, n);
                }

                input.close();
                output.close();

            } catch (FileNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    /** Copies and adds an external file to the project
     * @param source The source File
     */
    private void addToProject(File source, String destName) {
        Sources sources             = ProjectUtils.getSources(project);
        SourceGroup sourceFolders[] = sources.getSourceGroups(Sources.TYPE_GENERIC);

        // Look for "src" folder
        FileObject folders[] = sourceFolders[0].getRootFolder().getChildren();
        int i;
        for(i = 0; i < folders.length; ++i) {
            if(folders[i].getName().equals("src")) {
                // Copy here
                try {
                    FileObject dest = folders[i].createData(destName);

                    OutputStream output = dest.getOutputStream();
                    InputStream  input  = new FileInputStream(source);

                    int n;
                    byte[] buffer = new byte[512];

                    while ((n = input.read(buffer)) > 0) {
                        output.write(buffer, 0, n);
                    }

                    input.close();
                    output.close();

                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
    }


    /** Event performed when the user want to save the invariants into a file
     * @param evt
     */
    private void cmdInvSaveExternalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdInvSaveExternalActionPerformed
        this.saveExternal(remote.getInvariantsFile(), null);
    }//GEN-LAST:event_cmdInvSaveExternalActionPerformed

    /** Event performed when the user wants to add the invariants file into the project
     * @param evt
     */
    private void cmdInvProjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdInvProjectActionPerformed
        this.addToProject(remote.getInvariantsFile(), "invariants.txt");
    }//GEN-LAST:event_cmdInvProjectActionPerformed

    /** Event performed when the user wants to see the invariants file
     * @param evt
     */
    private void cmdInvViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdInvViewActionPerformed
        this.editFile(remote.getInvariantsFile());
    }//GEN-LAST:event_cmdInvViewActionPerformed

    private void cmdCoverageViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdCoverageViewActionPerformed
        this.editFile(remote.getCoverageFile());
    }//GEN-LAST:event_cmdCoverageViewActionPerformed

    private void cmdCoverageProjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdCoverageProjectActionPerformed
        try {
            // The received file is a ZIP file, so uncompress and add
            ZipFile zipFile = new ZipFile(remote.getCoverageFile());
            Enumeration entries = zipFile.entries();

            while(entries.hasMoreElements()) {
                ZipEntry zipEntry;
                File     output;
                FileOutputStream outputStream;
                byte [] buffer = new byte[512];
                int count;
                InputStream zipStream;

                zipEntry = (ZipEntry)entries.nextElement();
                output   = File.createTempFile("TAKUAN", "COV");
                outputStream = new FileOutputStream(output);

                zipStream = zipFile.getInputStream(zipEntry);
                while((count = zipStream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, count);
                }
                outputStream.close();

                this.addToProject(output, zipEntry.getName());
                // Purge
                output.delete();
            }

            zipFile.close();

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this,
                        ex.getLocalizedMessage(), "ZIP Error",
                        JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_cmdCoverageProjectActionPerformed

    private void cmdCoverageSaveExternalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdCoverageSaveExternalActionPerformed
        this.saveExternal(remote.getCoverageFile(), "zip");
    }//GEN-LAST:event_cmdCoverageSaveExternalActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdCoverageProject;
    private javax.swing.JButton cmdCoverageSaveExternal;
    private javax.swing.JButton cmdCoverageView;
    private javax.swing.JButton cmdInvProject;
    private javax.swing.JButton cmdInvSaveExternal;
    private javax.swing.JButton cmdInvView;
    private javax.swing.JLabel lblBranch;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblInvariants;
    private javax.swing.JPanel panelBranch;
    private javax.swing.JPanel panelInvariants;
    // End of variables declaration//GEN-END:variables
}

