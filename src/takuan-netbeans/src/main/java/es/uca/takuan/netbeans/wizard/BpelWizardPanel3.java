/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uca.takuan.netbeans.wizard;

import es.uca.takuan.client.Bpel;
import java.awt.Component;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;
import javax.swing.event.ChangeListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;

public class BpelWizardPanel3 implements WizardDescriptor.Panel {

    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private Component component;

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    public Component getComponent() {
        if (component == null) {
            component = new BpelVisualPanel3();
        }
        return component;
    }

    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
    // If you have context help:
    // return new HelpCtx(SampleWizardPanel1.class);
    }

    public boolean isValid() {
        // If it is always OK to press Next or Finish, then:
        return true;
    // If it depends on some condition (form filled out...), then:
    // return someCondition();
    // and when this condition changes (last form field filled in...) then:
    // fireChangeEvent();
    // and uncomment the complicated stuff below.
    }

    public final void addChangeListener(ChangeListener l) {
    }

    public final void removeChangeListener(ChangeListener l) {
    }
    /*
    private final Set<ChangeListener> listeners = new HashSet<ChangeListener>(1); // or can use ChangeSupport in NB 6.0
    public final void addChangeListener(ChangeListener l) {
    synchronized (listeners) {
    listeners.add(l);
    }
    }
    public final void removeChangeListener(ChangeListener l) {
    synchronized (listeners) {
    listeners.remove(l);
    }
    }
    protected final void fireChangeEvent() {
    Iterator<ChangeListener> it;
    synchronized (listeners) {
    it = new HashSet<ChangeListener>(listeners).iterator();
    }
    ChangeEvent ev = new ChangeEvent(this);
    while (it.hasNext()) {
    it.next().stateChanged(ev);
    }
    }
     */

    /** Method to read the state
     * @param settings
     */
    public void readSettings(Object settings) {
        Bpel bpel;
        WizardDescriptor wizard;
        
        // Get
        wizard = (WizardDescriptor) settings;
        bpel = (Bpel) wizard.getProperty(BpelWizardAction.BPEL_OBJECT);
        BpelVisualPanel3 panel = (BpelVisualPanel3) component;
        
        // Clear the list
        DefaultListModel listModel = new DefaultListModel();
        panel.lstPoints.setModel(listModel);
        
        // Generate the tree
        TreeNode root = bpel.getTree(true);
        DefaultTreeModel model = new DefaultTreeModel(root);
        model = new DefaultTreeModel(root);
        panel.treeBpel.setModel(model);

        // Set the checkbox and trigger the action
        panel.checkDefault.getModel().setSelected(bpel.getInstrumentSequencesByDefault());
        panel.updatePointsLabel();
        
        // Get pre-instrumented list
        List<String> instrumented = bpel.getInstrumentalized(!bpel.getInstrumentSequencesByDefault());
        int i;
        
        for(i = 0; i < instrumented.size(); i++) {
            listModel.addElement(instrumented.get(i));
        }

        // Set the BPEL
        panel.setBpel(bpel);
    }

    /** Method to store the state
     * @param settings
     */
    public void storeSettings(Object settings) {
        Bpel bpel;
        WizardDescriptor wizard = (WizardDescriptor) settings;
        BpelVisualPanel3 panel = (BpelVisualPanel3) component;

        // Get the BPEL
        bpel = (Bpel) wizard.getProperty(BpelWizardAction.BPEL_OBJECT);

        // Set the default
        bpel.setInstrumentPointsByDefault(panel.checkDefault.isSelected());

        // Set the instrument points
        ListModel model = panel.lstPoints.getModel();
        boolean instrument = !panel.checkDefault.isSelected();
        int i;

        for(i = 0; i < model.getSize(); i++) {
            bpel.setInstrumentalize((String) model.getElementAt(i), instrument);
        }
    }
}

