/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package es.uca.takuan.client;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Clase auxiliar para Bpel
 * @see Bpel
 * @author Alejandro Álvarez Ayllón
 */
class NamespaceContext implements javax.xml.namespace.NamespaceContext {

    private Map <String,String> namespaces;

    /** Constructor
     */
    public NamespaceContext() {
        namespaces = new HashMap<String, String>();
    }

    /** Returns the namespace associated with the prefix
     * @param prefix The prefix
     * @return The namespace URI, null if it does not exist
     */
    public String getNamespaceURI(String prefix) {
        return namespaces.get(prefix);
    }

    /** Returns the prefix associated with the given namespace
     * @param uri
     * @return
     */
    public String getPrefix(String uri) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /** Gets the prefixes associated with a namespace
     * @param uri
     * @return
     */
    public Iterator getPrefixes(String uri) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /** Adds the URI and Prefix to the table
     * @param prefix The prefix (i.e. uca)
     * @param uri The namespace (i.e. http://www.uca.es/xpath/2007/11)
     */
    public void addNamespace(String prefix, String uri) {
        namespaces.put(prefix, uri);
    }

}
