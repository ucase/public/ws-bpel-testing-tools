/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package es.uca.takuan.netbeans.wizard;

import es.uca.takuan.client.EventListener;
import es.uca.takuan.client.Remote;
import es.uca.takuan.netbeans.components.TakuanOutputTopComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/** Cancel Action Listener
 * Listens for the Cancel button action
 * @author Alejandro Álvarez
 */
class CancelActionListener implements ActionListener {

    // Attributes
    private RemoteEventListener remoteEvent;

    /** Constructor
     * @param rel RemoteEventListener to call
     */
    public CancelActionListener(RemoteEventListener rel) {
        remoteEvent = rel;
    }

    public void actionPerformed(ActionEvent arg0) {
        remoteEvent.cancel();
    }
    
}
/** Event Listener
 * Implementation of the interface EventListener
 * @author Alejandro Álvarez
 */
public class RemoteEventListener extends EventListener {
    // Where to display the changes
    private RunningDialog dialog;

    /** Constructor
     * @param running The running dialog
     */
    public RemoteEventListener(RunningDialog running) {
        dialog = running;
        // Setup a listener for the cancel button
        CancelActionListener cancelListener = new CancelActionListener(this);
        dialog.getCancelButton().addActionListener(cancelListener);
    }

    /** Cancel method
     */
    public void cancel() {
        getRemote().cancel();
    }

    /** Called when a file has been successfully sended
     * @param fName The file name
     */
    @Override
    public void fileSended(String fName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /** State changed
     * @param state
     */
    @Override
    public void stateChanged(int state) {
        switch(state) {
            case Remote.CANCELLED:
                dialog.enableAccept();
                dialog.setProgressMessage("Process cancelled");
                break;
            case Remote.FINISHED:
                dialog.enableAccept();
                break;
            case Remote.ERROR:
                dialog.enableAccept();
                dialog.setProgressMessage("Error: " + getRemote().getErrorMessage());
                es.uca.takuan.netbeans.components.TakuanOutputTopComponent.getDefault().println("Error: " + getRemote().getErrorMessage());
                break;
            case Remote.CONNECTING:
                dialog.setProgress(1, 0);
                dialog.setProgressMessage("Connecting to " + getRemote().getUri() + "...");
                break;
            case Remote.CONNECTED:
                dialog.setProgress(1, 1);
                dialog.setProgressMessage("Connected to " + getRemote().getUri());
                break;
            case Remote.FILE_SENDING:
                dialog.setProgress(getRemote().getFilesTotal(), getRemote().getFilesSended());
                dialog.setProgressMessage("Sending " + getRemote().getFileSending() + "...");
                es.uca.takuan.netbeans.components.TakuanOutputTopComponent.getDefault().println("Sending " + getRemote().getFileSending());
                break;
            case Remote.RUNNING:
                int tcCount, tcReady;
                tcCount = getRemote().getTestCasesCount();
                tcReady = getRemote().getTestCasesReady();
                dialog.setProgress(tcCount, tcReady);
                dialog.setProgressMessage("Running test case " + Integer.toString(tcReady) + "/" + Integer.toString(tcCount));
                break;
            case Remote.PROCESSED:
                dialog.setProgress(1, 1);
                dialog.setProgressMessage("Composition processed, recovering...");
                break;
            case Remote.INSTRUMENTING:
                dialog.setProgress(0, 1);
                dialog.setProgressMessage("Instrumenting process...");
                break;
            case Remote.PROCESSING:
                dialog.setProgress(0, 1);
                dialog.setProgressMessage("Processing traces. This can take a long time.");
                break;
            case Remote.RECOVERING:
                dialog.setProgress(getRemote().getRecoveringLength(),
                                   getRemote().getRecoveringReady());
                dialog.setProgressMessage("Recovering results...");
                break;
            case Remote.RECOVERED:
                dialog.setProgress(1, 1);
                dialog.setProgressMessage("Results recovered.");
                break;
        }
    }

    /** File sending progress
     * @param size
     * @param sended
     */
    @Override
    public void fileSending(long size, long sended) {
        dialog.setProgress(100, (int) (100 * (sended / size)));
    }

    /** Trace message received
     * @param str The string
     */
    @Override
    public void trace(String str) {
        TakuanOutputTopComponent.getDefault().print(str);
    }
}
