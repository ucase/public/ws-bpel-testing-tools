/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package es.uca.takuan.netbeans.wizard;

import javax.swing.JPanel;
import org.openide.util.NbBundle;

public final class BpelVisualPanel2 extends JPanel {

    /** Creates new form BpelVisualPanel2 */
    public BpelVisualPanel2() {
        initComponents();
    }

    @Override
    public String getName() {
        return NbBundle.getMessage(BpelVisualPanel2.class, "WIZARD_GENERAL_TITLE");
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblDescription = new javax.swing.JLabel();
        checkInstumentByDefault = new javax.swing.JCheckBox();
        lblDepth = new javax.swing.JLabel();
        valDepth = new javax.swing.JSpinner();
        lblCovOutputFormat = new javax.swing.JLabel();
        comboCovOutput = new javax.swing.JComboBox();
        lblMapping = new javax.swing.JLabel();
        comboMapping = new javax.swing.JComboBox();
        chkComparability = new javax.swing.JCheckBox();
        chkFilterUnused = new javax.swing.JCheckBox();
        chkSimplify = new javax.swing.JCheckBox();
        chkGenerateMetrics = new javax.swing.JCheckBox();

        lblDescription.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(lblDescription, org.openide.util.NbBundle.getMessage(BpelVisualPanel2.class, "WIZARD_GENERAL_DESCRIPTION")); // NOI18N
        lblDescription.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        checkInstumentByDefault.setFont(new java.awt.Font("Dialog", 0, 12));
        checkInstumentByDefault.setSelected(true);
        org.openide.awt.Mnemonics.setLocalizedText(checkInstumentByDefault, org.openide.util.NbBundle.getMessage(BpelVisualPanel2.class, "WIZARD_GENERAL_INSTRUMENT_BY_DEFAULT")); // NOI18N

        lblDepth.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(lblDepth, org.openide.util.NbBundle.getMessage(BpelVisualPanel2.class, "WIZARD_GENERAL_MAXDEPTH")); // NOI18N

        valDepth.setFont(new java.awt.Font("Dialog", 0, 12));
        valDepth.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(0), Integer.valueOf(0), null, Integer.valueOf(1)));

        lblCovOutputFormat.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(lblCovOutputFormat, org.openide.util.NbBundle.getMessage(BpelVisualPanel2.class, "WIZARD_GENERAL_COV_OUTPUT")); // NOI18N

        comboCovOutput.setFont(new java.awt.Font("Dialog", 0, 12));
        comboCovOutput.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "XML", "Plain text" }));

        lblMapping.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(lblMapping, org.openide.util.NbBundle.getMessage(BpelVisualPanel2.class, "WIZARD_GENERAL_MATRIX_MODE")); // NOI18N

        comboMapping.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        comboMapping.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Matrix flattening", "Matrix slicing" }));

        chkComparability.setFont(new java.awt.Font("Dialog", 0, 12));
        chkComparability.setSelected(true);
        org.openide.awt.Mnemonics.setLocalizedText(chkComparability, org.openide.util.NbBundle.getMessage(BpelVisualPanel2.class, "WIZARD_ENABLE_COMPARABILITY")); // NOI18N

        chkFilterUnused.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(chkFilterUnused, org.openide.util.NbBundle.getMessage(BpelVisualPanel2.class, "WIZARD_UNUSED_FILTER")); // NOI18N

        chkSimplify.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        chkSimplify.setSelected(true);
        org.openide.awt.Mnemonics.setLocalizedText(chkSimplify, org.openide.util.NbBundle.getMessage(BpelVisualPanel2.class, "WIZARD_GENERAL_SIMPLIFY")); // NOI18N

        chkGenerateMetrics.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        chkGenerateMetrics.setSelected(true);
        org.openide.awt.Mnemonics.setLocalizedText(chkGenerateMetrics, org.openide.util.NbBundle.getMessage(BpelVisualPanel2.class, "WIZARD_GENERAL_METRICS")); // NOI18N

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(chkGenerateMetrics)
                    .add(chkSimplify)
                    .add(chkFilterUnused)
                    .add(chkComparability)
                    .add(checkInstumentByDefault)
                    .add(lblDescription, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(lblDepth)
                            .add(lblMapping)
                            .add(lblCovOutputFormat))
                        .add(37, 37, 37)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(comboMapping, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(comboCovOutput, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(valDepth, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 83, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(lblDescription, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 51, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(checkInstumentByDefault)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblDepth)
                    .add(valDepth, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(6, 6, 6)
                        .add(lblCovOutputFormat)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(lblMapping))
                    .add(layout.createSequentialGroup()
                        .add(comboCovOutput, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(comboMapping, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(chkComparability)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(chkFilterUnused)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(chkSimplify)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(chkGenerateMetrics)
                .addContainerGap(40, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JCheckBox checkInstumentByDefault;
    public javax.swing.JCheckBox chkComparability;
    public javax.swing.JCheckBox chkFilterUnused;
    public javax.swing.JCheckBox chkGenerateMetrics;
    public javax.swing.JCheckBox chkSimplify;
    public javax.swing.JComboBox comboCovOutput;
    public javax.swing.JComboBox comboMapping;
    private javax.swing.JLabel lblCovOutputFormat;
    private javax.swing.JLabel lblDepth;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblMapping;
    public javax.swing.JSpinner valDepth;
    // End of variables declaration//GEN-END:variables
}

