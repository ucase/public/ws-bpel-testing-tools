/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uca.takuan.netbeans.components;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

/**
 * Action which shows TakuanOutput component.
 */
public class TakuanOutputAction extends AbstractAction {

    public TakuanOutputAction() {
        super(NbBundle.getMessage(TakuanOutputAction.class, "CTL_TakuanOutputAction"));
        putValue(SMALL_ICON, new ImageIcon(Utilities.loadImage(TakuanOutputTopComponent.ICON_PATH, true)));
    }

    public void actionPerformed(ActionEvent evt) {
        TopComponent win = TakuanOutputTopComponent.findInstance();
        win.open();
        win.requestActive();
    }
}
