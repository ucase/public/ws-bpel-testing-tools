/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uca.takuan.netbeans.wizard;

import es.uca.takuan.client.Bpel;
import java.awt.Component;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;

public class BpelWizardPanel4 implements WizardDescriptor.Panel {
    
    private Bpel.Variable variables[];
    
    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private Component component;

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    public Component getComponent() {
        if (component == null) {
            component = new BpelVisualPanel4();
        }
        return component;
    }

    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
    // If you have context help:
    // return new HelpCtx(SampleWizardPanel1.class);
    }

    public boolean isValid() {
        // If it is always OK to press Next or Finish, then:
        return true;
    // If it depends on some condition (form filled out...), then:
    // return someCondition();
    // and when this condition changes (last form field filled in...) then:
    // fireChangeEvent();
    // and uncomment the complicated stuff below.
    }

    public final void addChangeListener(ChangeListener l) {
    }

    public final void removeChangeListener(ChangeListener l) {
    }
    /*
    private final Set<ChangeListener> listeners = new HashSet<ChangeListener>(1); // or can use ChangeSupport in NB 6.0
    public final void addChangeListener(ChangeListener l) {
    synchronized (listeners) {
    listeners.add(l);
    }
    }
    public final void removeChangeListener(ChangeListener l) {
    synchronized (listeners) {
    listeners.remove(l);
    }
    }
    protected final void fireChangeEvent() {
    Iterator<ChangeListener> it;
    synchronized (listeners) {
    it = new HashSet<ChangeListener>(listeners).iterator();
    }
    ChangeEvent ev = new ChangeEvent(this);
    while (it.hasNext()) {
    it.next().stateChanged(ev);
    }
    }
     */

    /** Reads the settings and initializes the panel
     * @param settings
     */
    public void readSettings(Object settings) {
        WizardDescriptor wizard = (WizardDescriptor) settings;
        Bpel bpel;
        BpelVisualPanel4 panel = (BpelVisualPanel4) component;
        
        // Get BPEL
        bpel = (Bpel) wizard.getProperty(BpelWizardAction.BPEL_OBJECT);
        
        // Clear
        panel.reset();
        
        // Add variables
        variables = bpel.getVariables();
        int i;
        
        for(i = 0; i < variables.length; i++)
            panel.addVariable(variables[i]);
    }

    /** Store changes
     * @param settings
     */
    public void storeSettings(Object settings) {
        WizardDescriptor wizard = (WizardDescriptor) settings;
        Bpel bpel;
        BpelVisualPanel4 panel = (BpelVisualPanel4) component;

        // Get the BPEL
        bpel = (Bpel) wizard.getProperty(BpelWizardAction.BPEL_OBJECT);

        // Store changes
        bpel.setVariables(variables);
    }
}

