/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uca.takuan.netbeans.wizard;

import java.awt.Component;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.HelpCtx;

public class BpelWizardPanel6 implements WizardDescriptor.Panel {

    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private Component component;
    
    /** Hash map with files and their FileObject
     */
    private Map<String, FileObject> files;
    
    /** Constructor
     */
    public BpelWizardPanel6() {
        super();
        files = new HashMap();
    }

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    public Component getComponent() {
        if (component == null) {
            component = new BpelVisualPanel6();
        }
        return component;
    }

    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
    // If you have context help:
    // return new HelpCtx(SampleWizardPanel1.class);
    }

    public boolean isValid() {
        // If it is always OK to press Next or Finish, then:
        return true;
    // If it depends on some condition (form filled out...), then:
    // return someCondition();
    // and when this condition changes (last form field filled in...) then:
    // fireChangeEvent();
    // and uncomment the complicated stuff below.
    }

    public final void addChangeListener(ChangeListener l) {
    }

    public final void removeChangeListener(ChangeListener l) {
    }
    /*
    private final Set<ChangeListener> listeners = new HashSet<ChangeListener>(1); // or can use ChangeSupport in NB 6.0
    public final void addChangeListener(ChangeListener l) {
    synchronized (listeners) {
    listeners.add(l);
    }
    }
    public final void removeChangeListener(ChangeListener l) {
    synchronized (listeners) {
    listeners.remove(l);
    }
    }
    protected final void fireChangeEvent() {
    Iterator<ChangeListener> it;
    synchronized (listeners) {
    it = new HashSet<ChangeListener>(listeners).iterator();
    }
    ChangeEvent ev = new ChangeEvent(this);
    while (it.hasNext()) {
    it.next().stateChanged(ev);
    }
    }
     */

    /** Initializes the form with the files inside the project
     * @param settings
     */
    public void readSettings(Object settings) {
        WizardDescriptor wizard = (WizardDescriptor) settings;
        Project project;
        DataObject dataObject, bpelDataObject;
        BpelVisualPanel6 panel = (BpelVisualPanel6) component;
        
        // Files list model
        DefaultListModel listModel = new DefaultListModel();
        panel.lstDependencies.setModel(listModel);
        
        // BPTS Model
        DefaultComboBoxModel comboModel = new DefaultComboBoxModel();
        panel.comboTestSuite.setModel(comboModel);
               
        // Get project node (root)
        project = (Project) wizard.getProperty(BpelWizardAction.PROJECT);
        
        // Get BPEL Data Object
        bpelDataObject = (DataObject) wizard.getProperty(BpelWizardAction.BPEL_DATA_OBJECT);
        
        // Initialize stack with project's members (if we have)
        if(project != null) {
            // Get sources
            Sources sources             = ProjectUtils.getSources(project);
            SourceGroup sourceFolders[] = sources.getSourceGroups(Sources.TYPE_GENERIC);
            FileObject file, src;

            int i;
            Stack<FileObject> stack     = new Stack<FileObject>();
            Stack<String>     pathStack = new Stack<String>();

            // Put source folders into the stack
            for(i = 0; i < sourceFolders.length; i++) {
                file = sourceFolders[i].getRootFolder();
                stack.push(file);
            }

            // Look for "src" folder
            src = null;
            while(!stack.isEmpty() && src == null) {
                file = (FileObject) stack.pop();
                if(file.isFolder()) {
                    FileObject children[];
                    children = file.getChildren();
                    for(i = 0; i < children.length; i++) {
                        stack.push(children[i]);
                        if(children[i].getName().equals("src"))
                            src = children[i];
                    }
                }
            }
            stack.clear();
            stack.push(src);
            pathStack.push("");

            // Look for files recursively
            String fName;
            String bpelPath = bpelDataObject.getPrimaryFile().getPath();

            while(!stack.isEmpty()) {
                file  = (FileObject) stack.pop();
                fName = (String) pathStack.pop();

                if(file.isData()) {
                    // It is a data node
                    if (file.getExt().equals("bpts")) {
                        // It is a BPTS
                        comboModel.addElement(fName);
                        files.put(fName, file);
                    } else if (!file.getPath().equals(bpelPath)) {
                        // Add to the list
                       listModel.addElement(fName);
                       files.put(fName, file);
                    }
                } else if(file.isFolder()) {
                    // It is a folder, we must look inside
                    FileObject children[];
                    String path;

                    children = file.getChildren();

                    for(i = 0; i < children.length; i++) {
                        stack.push(children[i]);

                        path = fName.isEmpty()?"":fName + "/";
                        path += children[i].getNameExt();
                        
                        pathStack.push(path);
                    }
                }
            } //end-while
        } // end-if project
    }

    /** Store the changes
     * @param settings
     */
    public void storeSettings(Object settings) {
        WizardDescriptor wizard = (WizardDescriptor) settings;
        BpelVisualPanel6 panel  = (BpelVisualPanel6) component;
        Map<String, File> fileList = new HashMap<String, File>();

        // Add the dependencies
        ListModel model = panel.lstDependencies.getModel();
        int i;

        for(i = 0; i < model.getSize(); i++) {
            // Is one of the project files
            if(files.containsKey(model.getElementAt(i)))
                fileList.put((String) model.getElementAt(i),
                             FileUtil.toFile(files.get(model.getElementAt(i))));
            else
            {
                // It is not in the project files, create
                File file = new File((String) model.getElementAt(i));
                fileList.put(file.getName(), file);
            }
        }

        // Add the BPTS
        ComboBoxModel comboModel = panel.comboTestSuite.getModel();
        String        bptsFName  = (String) comboModel.getSelectedItem();

        if(files.containsKey(bptsFName))
            fileList.put(bptsFName, FileUtil.toFile(files.get(bptsFName)));
        else {
            File file = new File(bptsFName);
            fileList.put(file.getName(), file);
        }

        // Store
        wizard.putProperty(BpelWizardAction.DEPENDENCIES, fileList);
    }
}

