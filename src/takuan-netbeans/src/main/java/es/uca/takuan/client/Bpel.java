/*
 * This class is intented to be used to parse a BPEL file,
 * and add instrumentalization points, UCA extensions, etc.
 */

package es.uca.takuan.client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;
import java.util.Vector;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * BPEL Bpel and modifier
 * @author Alejandro Álvarez Ayllón
 */
public class Bpel {
    // Constants
    final public static String BPEL_NAMESPACE = "http://docs.oasis-open.org/wsbpel/2.0/process/executable";
    final public static String UCA_NAMESPACE  = "http://www.uca.es/xpath/2007/11";
    
    final private String instrumentalizable[] = new String[]{"process",
                            "sequence", "flow", "if", "elseif", "else",
                            "while", "forEach", "repeatUntil", "scope",
                            "catch", "catchAll", "compensationHandler"};
    // Atributes
    private Document document;
    private Element  root;

    /** Class to handles variables
     */
    public class Variable {
        // Constants
        final public static String TYPE        = "type";
        final public static String ELEMENT     = "element";
        final public static String MESSAGETYPE = "messageType";

        final public static String INSTRUMENT_YES = "Yes";
        final public static String INSTRUMENT_NO  = "No";
        // Attributes
        private String name;
        private String type;
        private String baseType;
        private String instrument;
        
        /** Default constructor
         */
        protected Variable() {}
        
        /** Returns the name of the variable
         * @return  A String with the name
         */
        public String getName() {
            return name;
        }

        /** Sets the name of the variable
         * @param val   The name
         */
        public void setName(String val) {
            name = val;
        }
        
        /** Returns the type of the variable (usually type, element or messageType)
         * @return  The type of the variable
         */
        public String getType() {
            return type;
        }

        /** Sets the type of the variable
         * @param val   The type of the variable. Usually type, element or
         *              messageType is expected, but everything elese is also accepted
         */
        public void setType(String val) {
            type = val;
        }
        
        /** Returns the base type of the variable (i.e. xsd:int)
         * @return  The base type
         */
        public String getBaseType() {
            return baseType;
        }

        /** Sets the base type of the variable
         * @param val   The base type
         */
        public void setBaseType(String val) {
            baseType = val;
        }
        
        /** Returns the value of the "uca:instrument" attribute
         * @return  The value of that attribute. Usually, "yes" or "no"
         */
        public String getInstrument() {
            return instrument;
        }

        /** Sets the value of the "uca:instrument" attribute
         * @param val   Usually, a "yes", "no" or "" (default) is expected
         */
        public void setInstrument(String val) {
            instrument = val.toLowerCase();
        }
    }
    
    /** Checks if the element is instrumentalizable
     * @param localName The node localname
     * @return
     */
    private boolean isInstrumentalizable(Node node) {
        // Base URI must be BPEL Namespace
        if(node.getNamespaceURI() == null || !node.getNamespaceURI().equals(BPEL_NAMESPACE))
            return false;
        // Look into the array
        int i;
        for(i = 0; i < instrumentalizable.length; i++) {
            if(instrumentalizable[i].equals(node.getLocalName()))
                return true;
        }
        // None
        return false;
    }

    /** Set the internal attributes and checks the validity of the document
     * @param parser
     * @throws org.w3c.dom.DOMException
     */
    protected void setAndCheck(DOMParser parser) throws DOMException {
        document = parser.getDocument();
        root = document.getDocumentElement();
        // Check the root element type
        if(!root.getNamespaceURI().equals(BPEL_NAMESPACE))
            throw new DOMException(DOMException.NAMESPACE_ERR, "Expected root namespace: " + BPEL_NAMESPACE);
        else if(!root.getLocalName().equals("process"))
            throw new DOMException(DOMException.NOT_SUPPORTED_ERR, "Unknown root element: process expected");
    }
    
    /** Creates a new Bpel and parses the input source specified by systemId
     * @param systemId The string to be parsed
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public Bpel(String systemId) throws SAXException, IOException, DOMException {
        DOMParser parser = new DOMParser();
        parser.parse(systemId);
        setAndCheck(parser);
    }
    
    /** Creates a new Bpel and parse the file
     * @param file The file to be parsed
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public Bpel(File file) throws SAXException, IOException, DOMException {
        DOMParser parser = new DOMParser();
        parser.parse(file.toURI().toString());
        setAndCheck(parser);
    }
    
    /** Creates a new Bpel and parse the input stream
     * @param inputStream The stream to be parsed
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public Bpel(InputStream inputStream) throws SAXException, IOException, DOMException {
        DOMParser parser = new DOMParser();
        parser.parse(new InputSource(inputStream));
        setAndCheck(parser);
    }

    // METHODS
    
    /** Gets the process name
     * @return The process name (bpel:name)
     */
    public String getName() {
        return root.getAttribute("name");
    }
    
    /** Sets the process name
     * @param name The process name
     */
    public void setName(String name) {
        root.setAttribute("name", name);
    }
    
    /** Return the maximum instrumentalization depth
     * @return The maximum depth
     */
    public int getMaxDepth() {
        String string;
        int value;
        string = root.getAttributeNS(UCA_NAMESPACE, "maxInstrumentationDepth");
        // Get the integer
        if(string.isEmpty())
            value = 0;
        else if(string.toLowerCase().equals("unlimited"))
            value = 0;
        else
            try{
                value = Integer.parseInt(string);
            }catch(Throwable e) {
                value = 0;
            }
        // Return
        return value;
    }
    
    /** Sets the maximum instrumentalization depth
     * @param depth The maximum depth
     */
    public void setMaxDepth(int depth) {
        // Set the extension if not set
        if(!hasUcaExtension())
            addUcaExtension();
        // Get the string value
        String string = new String();
        if(depth == 0)
            string = "unlimited";
        else
            string = Integer.toString(depth);
        // Set the attribute
        root.setAttributeNS(UCA_NAMESPACE, "uca:maxInstrumentationDepth", string);
    }
    
    /** Tells if the variables will be instrumentalized by default
     * @return true or false
     */
    public boolean getInstrumentVariablesByDefault() {
        String value;
        value = root.getAttributeNS(UCA_NAMESPACE, "instrumentVariablesByDefault");

        return (value.isEmpty() || value.toLowerCase().equals("yes"));
    }
    
    /** Instrumentalize by default?
     * @param set true or false
     */
    public void setInstrumentVariablesByDefault(boolean set) {
        // Set the extension if not set
        if(!hasUcaExtension())
            addUcaExtension();
        // Set the property
        if(set)
            root.setAttributeNS(UCA_NAMESPACE, "uca:instrumentVariablesByDefault", "yes");
        else
            root.setAttributeNS(UCA_NAMESPACE, "uca:instrumentVariablesByDefault", "no");
    }

    /** Tells if the sequences will be instrumented by default
     * @return A boolean. "True" it they will be.
     */
    public boolean getInstrumentSequencesByDefault() {
        String value;
        value = root.getAttributeNS(UCA_NAMESPACE, "instrumentSequencesByDefault");

        return (value.isEmpty() || value.toLowerCase().equals("yes"));
    }

    /** Set if the sequences must be instrumented by default
     * @param set True if they must be
     */
    public void setInstrumentPointsByDefault(boolean set) {
        // Set the extension if not set
        if(!hasUcaExtension())
            addUcaExtension();
        // Set the property
        if(set)
            root.setAttributeNS(UCA_NAMESPACE, "uca:instrumentSequencesByDefault", "yes");
        else
            root.setAttributeNS(UCA_NAMESPACE, "uca:instrumentSequencesByDefault", "no");
    }
    
    /** Returns true if the UCA Namespace is set
     * @return A boolean
     */
    public boolean hasUcaNamespace() {
        return document.lookupPrefix(UCA_NAMESPACE) != null;
    }

    /** Checks if the UCA extension is defined
     * @return true if it is
     */
    public boolean hasUcaExtension() {
        NodeList nodeList;
        Element extensions, ext;
        // Get bpel:extensions
        nodeList = root.getElementsByTagNameNS(BPEL_NAMESPACE, "extensions");
        // If not set, obviously, we don't have UCA Extension
        if(nodeList.getLength() == 0)
            return false;
        // Extensions block (only one)
        extensions = (Element) nodeList.item(0);
        // Now get extension list
        nodeList = extensions.getElementsByTagNameNS(BPEL_NAMESPACE, "extension");
        // Iterate
        int i;
        for(i = 0; i < nodeList.getLength(); i++) {
            ext = (Element) nodeList.item(i);
            if(ext.getAttribute("namespace").equals(UCA_NAMESPACE))
                return true;
        }
        // Not found
        return false;
    }
    
    /** Adds the extension block about UCA namespace
     */
    public void addUcaExtension() {
        NodeList nodeList;
        Element extensions, ext;
        // Avoid if it is defined
        if(hasUcaExtension())
            return;
        // Look for extensions block
        nodeList = root.getElementsByTagNameNS(BPEL_NAMESPACE, "extensions");
        
        if(nodeList.getLength() == 0) {
            // If not set, insert
            extensions = document.createElementNS(BPEL_NAMESPACE, "extensions");
            root.insertBefore(extensions, root.getFirstChild());
        } else {
            // Get
            extensions = (Element) nodeList.item(0);
        }
        
        // We looked for the UCA extensions before, so if we are here
        // it is not defined :-)
        ext = document.createElementNS(BPEL_NAMESPACE, "extension");
        ext.setAttribute("mustUnderstand", "no");
        ext.setAttribute("namespace", UCA_NAMESPACE);
        extensions.appendChild(ext);
    }
    
    /** Removes UCA extension
     */
    public void removeUcaExtension () {
        NodeList nodeList;
        Element extensions, ext;
        // Get extensions block
        nodeList = root.getElementsByTagNameNS(BPEL_NAMESPACE, "extensions");
        if(nodeList.getLength() == 0)
            return;
        // Get UCA extension
        extensions = (Element) nodeList.item(0);
        nodeList = extensions.getElementsByTagNameNS(BPEL_NAMESPACE, "extension");
        // Remove
        int i;
        for(i = 0; i < nodeList.getLength(); i++) {
            ext = (Element) nodeList.item(i);
            if(ext.getAttribute("namespace").equals(UCA_NAMESPACE)) {
                extensions.removeChild(ext);
            }
        }
    }
    
    /** Gets all the defined variables
     * @return The defined variables
     */
    public Variable[] getVariables() {
        NodeList nodeList;
        Element variables, var;
        
        // Look for variables block
        nodeList = root.getElementsByTagNameNS(BPEL_NAMESPACE, "variables");
        if(nodeList.getLength() == 0) // Empty
            return new Variable[0];
        
        // Get all variables
        variables = (Element) nodeList.item(0);
        nodeList = variables.getElementsByTagNameNS(BPEL_NAMESPACE, "variable");
        
        // Iterate
        int i, j;
        String type, base, instrument;
        String baseTypes[] = {Variable.TYPE, Variable.ELEMENT, Variable.MESSAGETYPE};
        Variable[] array = new Variable[nodeList.getLength()];

        base = new String();
        type = new String();
        
        for(i = 0; i < nodeList.getLength(); i++) {
            var = (Element) nodeList.item(i);
            array[i] = new Variable();
            // Name
            array[i].setName(var.getAttribute("name"));
            // Type
            for(j = 0; j < baseTypes.length && type.isEmpty(); j++) {
                base = baseTypes[j];
                type = var.getAttribute(base);
            }
            // Set type
            array[i].setType(type);
            array[i].setBaseType(base);
            // Set instrument
            instrument = var.getAttributeNS(UCA_NAMESPACE, "inspeccionar");
            array[i].setInstrument(instrument);
        }
        return array;
    }

    /** Sets the defined variables. getVariables() should be used first.
     * @param vars The variables array
     */
    public void setVariables(Variable[] vars) {
        int i;
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        NamespaceContext namespaceContext = new NamespaceContext();
        XPathExpression expression;
        Object obj;
        Element elm;

        namespaceContext.addNamespace("bpel", BPEL_NAMESPACE);

        xpath.setNamespaceContext(namespaceContext);

        for(i = 0; i < vars.length; i++) {
            try {
                // Build XPath expression
                expression = xpath.compile("/bpel:process/bpel:variables/bpel:variable[@name='" + vars[i].getName() + "']");
                // Get
                obj = expression.evaluate(document, XPathConstants.NODE);
                // Check
                if(obj == null)
                    throw new XPathExpressionException("No results");
                else if(!(obj instanceof Element))
                    throw new XPathExpressionException("Unexpected result: " +  obj.getClass());

                // Change
                elm = (Element) obj;
                if(vars[i].getInstrument().length() != 0)
                    elm.setAttributeNS(UCA_NAMESPACE, "uca:inspeccionar", vars[i].getInstrument());
                else
                    elm.removeAttributeNS(UCA_NAMESPACE, "inspeccionar");

            } catch (XPathExpressionException ex) {
                Exceptions.printStackTrace(ex);
            }

        }
    }
    
    /** Returns de DOM
     * @return The DOM associated with the BPEL
     */
    public Document getDOM() {
        return document;        //
    }
    
    /** Get paths marked to instrument
     * @return
     */
    public List<String> getInstrumentalized(boolean set) {
        Vector<String> result = new Vector<String>();
        Stack<Element> stack = new Stack<Element>();
        Stack<String> pathStack = new Stack<String>();
        Element node;
        String val, path, expected;
        HashMap<String, Integer> countMap = new HashMap<String, Integer>();

        // Boolean to string
        expected = set?"yes":"no";

        // Look through the tree for nodes with the instrument
        // atribute set to "set" (Yes or No)
        stack.push(root);
        pathStack.push("/process");

        while(!stack.isEmpty()) {
            node = stack.pop();
            path = pathStack.pop();
            // Only if we can instrument it
            if(isInstrumentalizable(node)) {
                // Check attribute and add
                val = node.getAttributeNS(UCA_NAMESPACE, "instrument");
                if(!val.isEmpty() && val.toLowerCase().equals(expected)) {
                    result.add(path);
                }
                // Push all children into the stack
                NodeList children = node.getChildNodes();
                Node n;
                int i;
                Integer index;
                String nodePath;

                countMap.clear();

                for(i = 0; i < children.getLength(); i++) {
                    n = children.item(i);
                    if(n instanceof Element) {
                        // Node base path
                        nodePath = path + "/" + n.getLocalName();
                        // Get the current index
                        if(countMap.containsKey(nodePath))
                            index = countMap.get(nodePath);
                        else
                            index = 1;
                        // Increment
                        countMap.put(path, index+1);
                        // Node real path
                        nodePath = nodePath + "[" + index + "]";
                        // Push
                        stack.push((Element) n);
                        pathStack.push(nodePath);
                    }
                }
            }
        }

        // Return
        return result;
    }
    
    /** Marks a activity to be instrumentalized
     * @param expression The element as an XPath expression (process/sequence/if[0])
     * @param set A boolean
     */
    public void setInstrumentalize(String expression, boolean set) {
        if(!hasUcaExtension())
            addUcaExtension();
        
        try {
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();
            NamespaceContext namespaceContext = new NamespaceContext();
            String auxExpr = new String();

            namespaceContext.addNamespace("bpel", BPEL_NAMESPACE);
            xpath.setNamespaceContext(namespaceContext);

            auxExpr = expression.replaceAll("/", "/bpel:");
            if(auxExpr.endsWith("bpel:"))
                auxExpr = auxExpr.substring(-5);
            XPathExpression expr = xpath.compile(auxExpr);

            Object aux = expr.evaluate(document, XPathConstants.NODE);

            if(aux == null)
                throw new XPathExpressionException("No results: " + auxExpr);
            else if(!(aux instanceof Element))
                throw new XPathExpressionException("Unexpected node: " + aux.getClass());

            Element element = (Element) aux;
            String val;

            val = set?"yes":"no";
            element.setAttributeNS(UCA_NAMESPACE, "uca:instrument", val);

        } catch (XPathExpressionException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
    
    /** Gets the children of a given element
     * @param node The root element
     * @param instrumentalizable If it is true, only instrumentalizable nodes will be processed
     * @return A tree with all its children
     */
    protected TreeNode getTree(Element node, boolean instrumentalizable) {
        DefaultMutableTreeNode root;
        
        // Node
        root = new DefaultMutableTreeNode(node.getLocalName());
        
        // Get all children
        if(node.hasChildNodes())
        {
            Node child;
            MutableTreeNode subTree;
            int i, index;
            String id;
            HashMap<String, Integer> countMap = new HashMap<String, Integer>();
            
            NodeList nodeList =  node.getChildNodes();
            
            // Iterate
            for(i = 0; i < nodeList.getLength(); i++) {
                // Foreach child
                child = nodeList.item(i);
                if(child instanceof Element && isInstrumentalizable(child))
                {
                    // Add the sub-tree
                    subTree = (MutableTreeNode) getTree((Element) child, instrumentalizable);
                    // Add index
                    id = child.getLocalName();
                    if(countMap.containsKey(id))
                        index = countMap.get(id);
                    else
                        index = 1;
                    
                    subTree.setUserObject(id + "[" + index  + "]");
                    // Update
                    countMap.put(id, (Integer) (index + 1));
                    // Append
                    root.add(subTree);
                }
            }
        }
        
        // Return
        return root;
    }
    
    /** Returns a Tree with all the elements
     * @param instrumentalizable If it is true, only instrumentalizable nodes will be processed
     * @return A tree representing the BPEL Composition (just tag names)
     */
    public TreeNode getTree(boolean instrumentalizable) {
        return getTree(root, instrumentalizable);
    }
    
    /** Returns a Tree with all the elements.
     * Done for convenience.
     * @return A tree representing the BPEL Composition (just tag names)
     */
    public TreeNode getTree() {
        return getTree(false);
    }

    /** Returns an InputStream that can be used to read from the DOM
     * @return An InputStream
     */
    public InputStream getStream() {
        try {
            Source source = new DOMSource(root);
            StringWriter writer = new StringWriter();
            Result result = new StreamResult(writer);
            Transformer trans = TransformerFactory.newInstance().newTransformer();
            trans.transform(source, result);
            return new ByteArrayInputStream(writer.getBuffer().toString().getBytes());
        } catch (TransformerException ex) {
            Exceptions.printStackTrace(ex);
            return null;
        }
    }

    /** Returns the size of the document (in bytes)
     * @return
     */
    public long getSize() {
        try {
            Source source = new DOMSource(root);
            StringWriter writer = new StringWriter();
            Result result = new StreamResult(writer);
            Transformer trans = TransformerFactory.newInstance().newTransformer();
            trans.transform(source, result);
            return writer.getBuffer().toString().length();
        } catch (TransformerException ex) {
            Exceptions.printStackTrace(ex);
            return 0;
        }
    }

}
