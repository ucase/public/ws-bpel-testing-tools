/*
 * This class is intended to be used in the future to comunicate with
 * Takuan
 */

package es.uca.takuan.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

/**
 * This class is used to handle the comunication with the Takuan Servlet
 * @author Alejandro Álvarez Ayllón
 * @see EventListener
 */
public class Remote implements Runnable {

    /** Private class for convenience
     */
    private class FileInfo {
        public String      fileName;
        public InputStream stream;
        public long        size;

        private FileInfo(String fName, InputStream instream, long s) {
            fileName = fName;
            stream   = instream;
            size     = s;
        }
    }

    // Attributes
    private Queue<FileInfo>   filesQueue;
    private int               totalFiles;
    private String            remoteURI;
    private EventListener     listener;
    private int               state = CREATED;

    private int testCasesCount;
    private int testCasesEnded;

    private String            errMessage = null;

    private int downloadLen;
    private int downloadReady;
    private File invFile = null;
    private File covFile = null;

    private Map<String, String> parameters;

    /** Exception throwed when a state is invalid
     */
    public class InvalidState extends Exception {
        public InvalidState(int current, int expected) {
            super("Invalid state: " + Integer.toString(current) + ", " + Integer.toString(expected) + " expected ");
        }
    }

    // Constants
    public static final int CREATED       = 0;
    public static final int READY         = 1;
    public static final int CONNECTING    = 2;
    public static final int CONNECTED     = 3;
    public static final int FILE_SENDING  = 4;
    public static final int FILE_SENDED   = 5;
    public static final int INSTRUMENTING = 55;
    public static final int RUNNING      = 6;
    public static final int PROCESSING   = 65;
    public static final int PROCESSED    = 7;
    public static final int RECOVERING   = 8;
    public static final int RECOVERED    = 9;
    public static final int DISCONNECTED = 10;
    public static final int FINISHED     = 11;
    public static final int CANCELLED    = 20;
    public static final int ERROR        = 21;
    
    /** Constructor
     * @param listener The event listener
     */
    public Remote(EventListener listen) {
        listener = listen;
        listener.setRemote(this);
        setState(CREATED);

        filesQueue = new ConcurrentLinkedQueue<FileInfo>();
        totalFiles = 0;

        parameters = new HashMap<String, String>();
    }

    /** Gets the last error message
     * @return The error message
     */
    public String getErrorMessage() {
        return errMessage;
    }

    /** Sets the state and raise the event
     * @param stat The new state
     * @return false if the execution must end
     */
    private boolean setState(int stat) {
        // Check if the current state is CANCELLED
        // and the new one is not ERROR
        if(state == CANCELLED && stat != ERROR)
            return false;
        // Change
        state = stat;
        listener.stateChanged(state);
        return true;
    }

    /** Returns the current state
     * @return The current state
     */
    public int getState() {
        return state;
    }

    /** Cancel the process
     */
    public void cancel() {
        setState(CANCELLED);
    }

    /** Wait until STATE
     */
    @SuppressWarnings("empty-statement")
    public void wait(int stat) {
        while(stat != state);
    }

    /** Add a file to the queue to send into the server.
     * @param fName The file name
     * @param stream The input stream with the content of the file
     */
    public void addFile(String fName, InputStream stream, long size) throws InvalidState {
        if(state != READY)
            throw new InvalidState(state, READY);
        filesQueue.add(new FileInfo(fName, stream, size));
        totalFiles++;
    }

    /** Set the server URI
     * @param uri The server URI
     */
    public void setURI(String uri) throws InvalidState, MalformedURLException {
        if(state != READY)
            throw new InvalidState(state, READY);
        // Test
        new URL(uri);
        // Set
        remoteURI = uri;
        if(!remoteURI.endsWith("/"))
            remoteURI += "/";
    }

    /** Recovers the server URI
     * @return null if not set
     */
    public String getUri() {
        return remoteURI;
    }

    /** Start the sending
     */
    public void send() {
        setState(CONNECTING);
    }

    /** Returns the name of the file that is currently being sended
     * @return The file name
     */
    public String getFileSending() {
        if(filesQueue.isEmpty())
            return "";
        else
            return filesQueue.peek().fileName;
    }

    /** Returns the total amount of files that has been / will be sended
     * @return
     */
    public int getFilesTotal() {
        return totalFiles;
    }

    /** Returns the amount of files that has been sended
     * @return
     */
    public int getFilesSended() {
        return totalFiles - filesQueue.size();
    }

    /** Sets the number of test cases
     * @param n
     */
    protected void setTestCasesCount(int n) {
        testCasesCount = n;
    }

    /** Returns the number of test cases (must has been indicated by the server side)
     * @return
     */
    public int getTestCasesCount() {
        return testCasesCount;
    }

    /** Sets the number of test cases executed
     * @param n
     */
    protected void setTestCasesReady(int n) {
        testCasesEnded = n;
    }

    /** Returns how many test cases have been ran
     * @return
     */
    public int getTestCasesReady() {
        return testCasesEnded;
    }

    /** Returns the length of the file we are recovering
     * @return
     */
    public int getRecoveringLength() {
        return downloadLen;
    }

    /** Returns the recovered size (ready)
     * @return
     */
    public int getRecoveringReady() {
        return downloadReady;
    }

    /** Returns the invariant temporary file
     * @return A File object associated with the invariants file
     */
    public File getInvariantsFile() {
        return invFile;
    }

    /** Returns the coverage temporary file
     * @return A File object associated with the coverage file
     */
    public File getCoverageFile() {
        return covFile;
    }

    /** Adds a parameter for the execution engine. This is passed directly to Takuan
     * servlet.
     * @param key   The parameter name
     * @param value The value of the parameter
     */
    public void addParameter(String key, String value) {
        parameters.put(key, value);
    }

    /** Gets a resource from the remote server, and writes it
     * to a local file
     */
    protected void downloadResource(String remoteUrl, File local) throws IOException, Exception {

        if (!setState(RECOVERING)) {
            return;
        }
        
        URL url                      = new URL(remoteUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        
        connection.setRequestMethod("GET");
        connection.setDoInput(true);
        connection.connect();

        InputStream input = connection.getInputStream();
        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new Exception(Integer.toString(connection.getResponseCode()) + " " + connection.getResponseMessage());
        }

        downloadLen   = connection.getContentLength();
        downloadReady = 0;
        FileOutputStream fout = new FileOutputStream(local);

        int n;
        byte[] buffer = new byte[512];
        while ((n = input.read(buffer)) > 0) {
            fout.write(buffer, 0, n);
            downloadReady += n;
            listener.stateChanged(RECOVERING);
        }
        fout.close();

        if (!setState(RECOVERED)) {
            return;
        }
    }

    /** Main method called by the thread
     */
    public void run() {
        HttpURLConnection connection;
        URL               url;
        InputStream       input;
        byte              buffer[] = new byte[512];
        int               n;
        
        // New thread, we are ready
        if(!setState(READY))
            return;

        // Wait until start sended
        wait(CONNECTING);

        // Send files
        try{
            while(!filesQueue.isEmpty()) {
                FileInfo          file;
                OutputStream      outStream;
                int               sended;
                long              totalSended;

                // Get first file
                file = filesQueue.peek();

                // Connect to the server (maybe the underline connections keeps opened)
                if(!setState(CONNECTING))
                    return;
                url        = new URL(remoteURI + file.fileName);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("PUT");
                connection.setDoOutput(true);
                outStream  = connection.getOutputStream();
                connection.connect();

                // Send file
                if(!setState(FILE_SENDING)) {
                    connection = null;
                    return;
                }

                totalSended = 0;
                while((sended = file.stream.read(buffer)) > 0) {
                    outStream.write(buffer, 0, sended);
                    totalSended += sended;
                    listener.fileSending(file.size, totalSended);
                }

                outStream.flush();
                outStream.close();
                connection.disconnect();

                if(connection.getResponseCode() != HttpURLConnection.HTTP_ACCEPTED)
                    new IOException(connection.getResponseMessage());
                    
                connection = null;

                // Sended
                if(!setState(FILE_SENDED))
                    return;

                // Remove from queue
                filesQueue.poll();
            } // while
        } catch(MalformedURLException ex) {
            errMessage = ex.getMessage();
            setState(ERROR);
            return;
        } catch(IOException ex) {
            errMessage = ex.getMessage();
            setState(ERROR);
            return;
        }

        // Run
        if(!setState(INSTRUMENTING))
            return;
        try {
            // Add parameters to the URL (query)
            String baseUrl = new String(remoteURI + "run?");

            for(String key : parameters.keySet()) {
                baseUrl += key + "=" + (String) parameters.get(key).replace(' ', '_') + "&";
            }

            // Supress last &
            if(baseUrl.endsWith("&"))
                baseUrl = baseUrl.substring(0, baseUrl.length() - 1);

            // Create connection
            url = new URL(baseUrl);
            listener.trace("Run request: " + url.toString() + "\n");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);

            connection.connect();
            input = connection.getInputStream();

            // Main loop: get trace and send events
            DOMParser parser = new DOMParser();
            String msgLine = null;

            parser.setFeature("http://xml.org/sax/features/namespaces", false);
            while((n = input.read(buffer)) > 0) {
                String    received;
                Element   root;

                received = new String(buffer, 0, n);
                listener.trace(received);
                // Process the line
                String lines[];
                String line;
                int i;

                lines   = received.split("\n");

                for(i = 0; i < lines.length; i++) {
                    line = lines[i];

                    // Messages
                    if(line.startsWith("<takuan:"))
                        msgLine = line;
                    else if(msgLine != null)
                        msgLine = msgLine.concat(line);

                    // End of message
                    if(msgLine != null && msgLine.endsWith("/>")) {
                        parser.parse(new InputSource(new StringReader(msgLine)));
                        root = parser.getDocument().getDocumentElement();

                        // Test case count
                        if(root.getNodeName().equals("takuan:testCaseCount")) {
                            setTestCasesCount(Integer.parseInt(root.getAttribute("total")));
                            setTestCasesReady(0);

                            if(!setState(RUNNING)) {
                                input.close();
                                connection = null;
                                return;
                            }
                            
                        }
                        // Test case executed
                        else if(root.getNodeName().equals("takuan:testCaseEnded")) {
                            setTestCasesReady(Integer.parseInt(root.getAttribute("finished")));

                            if(!setState(RUNNING)) {
                                input.close();
                                connection = null;
                                return;
                            }
                            
                        }
                        // Processing
                        else if(root.getNodeName().equals("takuan:processing")) {
                            if(!setState(PROCESSING)) {
                                input.close();
                                connection = null;
                                return;
                            }
                        }
                        // Done
                        else if(root.getNodeName().equals("takuan:done")) {
                            if(!setState(PROCESSED)) {
                                input.close();
                                connection = null;
                                return;
                            }
                        }

                        // Clear
                        msgLine = null;
                    }
                } //end-for
            }

            // Error?
            if(connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                input = connection.getErrorStream();
                if(input != null)
                    while(input.read(buffer) > 0) {
                        listener.trace(buffer.toString());
                    }
                errMessage = connection.getResponseMessage();
                if(errMessage == null)
                    errMessage = "Error during execution (" + Integer.toString(connection.getResponseCode()) + ")";
                setState(ERROR);
                return;
            }

            // Recovering invariants
            invFile = File.createTempFile("TAKUAN", "INVARIANTS");
            listener.trace("Writing invariants into " + invFile.getAbsolutePath() + "\n");
            this.downloadResource(remoteURI + "invariants", invFile);

            // Recovering coverage
            covFile = File.createTempFile("TAKUAN", "COVERAGE");
            listener.trace("Writing coverage info into " + covFile.getAbsolutePath() + "\n");
            this.downloadResource(remoteURI + "coverage", covFile);

        } catch(Exception ex) {
            errMessage = ex.getMessage();
            if(errMessage == null)
                errMessage = "Exception caught: " + ex.getClass().getCanonicalName();
            setState(ERROR);
            return;
        }

        // Finished
        connection = null;
        setState(DISCONNECTED);
        setState(FINISHED);
        return;
    }
}
