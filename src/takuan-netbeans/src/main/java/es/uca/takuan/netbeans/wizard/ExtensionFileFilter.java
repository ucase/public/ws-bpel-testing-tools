package es.uca.takuan.netbeans.wizard;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 * Extension-based file filter
 * @author Alejandro Álvarez Ayllón
 */
class ExtensionFileFilter extends FileFilter {

    private String [] acceptedExtensions;

    /**
     * Constructor
     * @param extensions
     */
    public ExtensionFileFilter(String[] extensions)
    {
        acceptedExtensions = extensions;
    }

    /** Indicates if a file is accepted by this filter
     * @param f
     * @return
     */
    public boolean accept(File f)
    {
        String fName = f.getName();
        int    i     = fName.lastIndexOf(".");

        if(i > 0 && f.isFile()) {
            String ext = fName.substring(i+1).toLowerCase();

            for(String e : acceptedExtensions)
                if(e.equals(ext))
                    return true;

            return false;
        }

        return false;
    }

    /** Description of the filter
     * @return
     */
    @Override
    public String getDescription() {
        String descr = new String();

        for(String e : acceptedExtensions) {
            descr += "*." + e + " ";
        }

        return descr;
    }

}
