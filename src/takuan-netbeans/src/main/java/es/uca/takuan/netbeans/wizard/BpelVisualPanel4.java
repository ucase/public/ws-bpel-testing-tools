/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package es.uca.takuan.netbeans.wizard;

import es.uca.takuan.client.Bpel;
import es.uca.takuan.client.Bpel.Variable;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import org.openide.util.NbBundle;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;


/** Private class to handle events from ComboBox
 * @author Alejandro Álvarez
 */
final class ComboActionHandler implements ActionListener {

    private static ComboActionHandler instance = null;
    
    private Map<JComboBox, Variable> associatedVar;

    /** Private constructor
     */
    private ComboActionHandler() {
        associatedVar = new HashMap<JComboBox, Variable>();
    }

    /** A method to get the singleton
     * @return
     */
    public static ComboActionHandler getInstance() {
        if(instance != null)
            return instance;
        else
            return (instance = new ComboActionHandler());
    }

    /** Sets the map that will be used to change the variables properties
     * @param vm
     */
    public void addVariable(JComboBox combo, Variable var) {
        associatedVar.put(combo, var);
    }

    /** Called when an action is performed
     * @param ev
     */
    public void actionPerformed(ActionEvent ev) {
        JComboBox combo = (JComboBox)ev.getSource();

        if(associatedVar.containsKey(combo)) {
            String val = (String)combo.getSelectedItem();

            if(!val.equals(Bpel.Variable.INSTRUMENT_YES) && !val.equals(Bpel.Variable.INSTRUMENT_NO))
                val = new String();

            associatedVar.get(combo).setInstrument(val);
        }
    }

    /** Recover the map
     */
    public Map<JComboBox, Variable> getMap()
    {
        return this.associatedVar;
    }
}
/** Form
 * @author Alejandro Álvarez
 */
public final class BpelVisualPanel4 extends JPanel {

    /** Creates new form BpelVisualPanel4 */
    public BpelVisualPanel4() {
        initComponents();
        // Change the layout manager (GroupLayout is confusing and anoying)
        panelMain.setLayout(new BoxLayout(panelMain, BoxLayout.PAGE_AXIS));
    }

    /** Returns the form name
     * @return
     */
    @Override
    public String getName() {
        return NbBundle.getMessage(BpelVisualPanel4.class, "WIZARD_VARIABLES_TITLE");
    }

    /** Adds a variable to the form
     * @param name Variable name
     * @param type Variable type
     * @param instrument Instrument?
     */
    public void addVariable(Variable var)
    {
        JLabel label;
        JComboBox combo;
        
        // Create label
        Dimension labelDimension = new Dimension(150, 25);
        label = new JLabel(var.getName());
        label.setPreferredSize(labelDimension);
        
        // Create combobox
        DefaultComboBoxModel comboModel = new DefaultComboBoxModel();
        combo = new JComboBox(comboModel);
        
        comboModel.addElement("<not set>");
        comboModel.addElement(Bpel.Variable.INSTRUMENT_YES);
        comboModel.addElement(Bpel.Variable.INSTRUMENT_NO);
      
        if(var.getInstrument().isEmpty())
            comboModel.setSelectedItem("<not set>");
        else if(var.getInstrument().equals(Bpel.Variable.INSTRUMENT_YES))
            comboModel.setSelectedItem(Bpel.Variable.INSTRUMENT_YES);
        else
            comboModel.setSelectedItem(Bpel.Variable.INSTRUMENT_NO);

        // Add to the handler
        ComboActionHandler.getInstance().addVariable(combo, var);
        combo.addActionListener(ComboActionHandler.getInstance());
        
        // Add components
        Box box = new Box(BoxLayout.LINE_AXIS);
        box.setAlignmentX(Box.LEFT_ALIGNMENT);
        box.add(label);
        box.add(Box.createHorizontalStrut(5));
        box.add(combo);
        panelMain.add(box);
        box.setMaximumSize(new Dimension(400 ,25));
        
        // Update
        panelMain.validate();
    }

    /** Clears the panel
     */
    void reset() {
        panelMain.removeAll();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblDescription = new javax.swing.JLabel();
        panelVars = new javax.swing.JScrollPane();
        panelMain = new javax.swing.JPanel();
        cmdSetInstrument = new javax.swing.JButton();
        cmdSetIgnore = new javax.swing.JButton();

        lblDescription.setFont(new java.awt.Font("Dialog", 0, 12));
        org.openide.awt.Mnemonics.setLocalizedText(lblDescription, org.openide.util.NbBundle.getMessage(BpelVisualPanel4.class, "WIZARD_VARIABLES_DESCRIPTION")); // NOI18N
        lblDescription.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        panelVars.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        panelVars.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        org.jdesktop.layout.GroupLayout panelMainLayout = new org.jdesktop.layout.GroupLayout(panelMain);
        panelMain.setLayout(panelMainLayout);
        panelMainLayout.setHorizontalGroup(
            panelMainLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 451, Short.MAX_VALUE)
        );
        panelMainLayout.setVerticalGroup(
            panelMainLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 227, Short.MAX_VALUE)
        );

        panelVars.setViewportView(panelMain);

        org.openide.awt.Mnemonics.setLocalizedText(cmdSetInstrument, "Set all to instrument");
        cmdSetInstrument.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdSetInstrumentActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(cmdSetIgnore, "Set all to ignore");
        cmdSetIgnore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdSetIgnoreActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, panelVars, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, lblDescription, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(cmdSetInstrument)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cmdSetIgnore)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(lblDescription, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 54, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panelVars, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 206, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cmdSetIgnore)
                    .add(cmdSetInstrument))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Set all the variables to instrument
     * @param evt
     */
    private void cmdSetInstrumentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdSetInstrumentActionPerformed
        Map<JComboBox, Variable> variables;

        variables = ComboActionHandler.getInstance().getMap();

        for(JComboBox combo: variables.keySet()) {
            combo.setSelectedItem(Bpel.Variable.INSTRUMENT_YES);
            variables.get(combo).setInstrument(Bpel.Variable.INSTRUMENT_YES);
        }
    }//GEN-LAST:event_cmdSetInstrumentActionPerformed

    /**
     * Set all the variables NOT to instrument
     * @param evt
     */
    private void cmdSetIgnoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdSetIgnoreActionPerformed
        Map<JComboBox, Variable> variables;

        variables = ComboActionHandler.getInstance().getMap();

        for(JComboBox combo: variables.keySet()) {
            combo.setSelectedItem(Bpel.Variable.INSTRUMENT_NO);
            variables.get(combo).setInstrument(Bpel.Variable.INSTRUMENT_NO);
        }
    }//GEN-LAST:event_cmdSetIgnoreActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdSetIgnore;
    private javax.swing.JButton cmdSetInstrument;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JPanel panelMain;
    private javax.swing.JScrollPane panelVars;
    // End of variables declaration//GEN-END:variables
}

