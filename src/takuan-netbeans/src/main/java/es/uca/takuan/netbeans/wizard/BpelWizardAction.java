package es.uca.takuan.netbeans.wizard;

import java.awt.Component;
import java.awt.Dialog;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.JComponent;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.WizardDescriptor;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.modules.ModuleInfo;
import org.openide.util.Lookup;
import org.openide.util.Exceptions;
import org.openide.filesystems.FileObject;
import es.uca.takuan.client.Bpel;
import es.uca.takuan.netbeans.components.TakuanOutputTopComponent;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.NotifyDescriptor.Message;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

/**
 * @author Alejandro Álvarez
 * @version 0.0.1
 * New action insalled in the "Build toolbar", for BPEL compositions
 */
public final class BpelWizardAction extends CookieAction {
    // Constants
    private final String BPEL_MODULE = "org.netbeans.modules.bpel.core";
    private final String BPEL_CLASS  = "org.netbeans.modules.bpel.core.BPELDataObject";
    
    // String constants for properties
    public static final String BPEL_OBJECT      = "BPEL_OBJECT";
    public static final String BPEL_DATA_OBJECT = "BPEL_DATA_OBJECT";
    public static final String PROJECT          = "PROJECT";
    public static final String DEPENDENCIES     = "DEPENDENCIES";
    public static final String SERVER_URI       = "SERVLET_URI";
    public static final String REMOTE           = "REMOTE";

    // Parameters for the server
    public static final String COVERAGE_OUTPUT  = "SERVER_COVERAGE_OUTPUT";
    public static final String FILTER_UNUSED    = "SERVER_FILTER_UNUSED";
    public static final String COMPARABILITIES  = "SERVER_COMPARABILITIES";
    public static final String SIMPLIFY         = "SERVER_SIMPLIFY";
    public static final String GENERATE_METRICS = "SERVER_GENERATE_METRICS";
    public static final String MAPPING          = "SERVER_MAPPING";

    // Atributes
    private WizardDescriptor.Panel[] panels;
    private Class BpelDataObject;
      
    // METHODS
    
    /**
     * Call the parent method, search for the BPEL Plugin, and gets the
     * class used for BPEL Files
     */
    @Override
    public void initialize()
    {
        Iterator<? extends ModuleInfo> i;
        super.initialize();
        // Default DataObject
        BpelDataObject = DataObject.class;
        // Get all modules installed
        Collection<? extends ModuleInfo> modules = Lookup.getDefault().lookupAll(ModuleInfo.class);
        // Empty?
        if(modules.isEmpty())
        {
            ErrorManager.getDefault().log(ErrorManager.WARNING, "ModuleInfo/s couldn't be retrieved");
            return;
        }
        // Get iterator
        i = modules.iterator();
        // Search
        while(i.hasNext() && BpelDataObject == DataObject.class)
        {
            ModuleInfo module;
            module = i.next();
            // Module found
            if(module.getCodeNameBase().equals(BPEL_MODULE))
            {
                if(module.isEnabled()) {
                    try {
                        ClassLoader loader = module.getClassLoader();
                        BpelDataObject = loader.loadClass(BPEL_CLASS);
                    } catch (ClassNotFoundException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                } else {
                    ErrorManager.getDefault().log(ErrorManager.WARNING, BPEL_CLASS + " founded, but it is not active");
                    return;
                }
            }
        }
        // If we have DataObject, not found
        if(BpelDataObject == DataObject.class)
            ErrorManager.getDefault().log(ErrorManager.WARNING, BPEL_CLASS + " not found");
    }
    
    /**
     * Initialize panels representing individual wizard's steps and sets
     * various properties for them influencing wizard appearance.
     */
    private WizardDescriptor.Panel[] getPanels() {
        if (panels == null) {
            panels = new WizardDescriptor.Panel[]{
                new BpelWizardPanel1(),
                new BpelWizardPanel2(),
                new BpelWizardPanel3(),
                new BpelWizardPanel4(),
                new BpelWizardPanel5(),
                new BpelWizardPanel6(),
                new BpelWizardPanel7(),
                new BpelWizardPanel8()
            };
            String[] steps = new String[panels.length];
            for (int i = 0; i < panels.length; i++) {
                Component c = panels[i].getComponent();
                // Default step name to component name of panel. Mainly useful
                // for getting the name of the target chooser to appear in the
                // list of steps.
                steps[i] = c.getName();
                if (c instanceof JComponent) { // assume Swing components
                    JComponent jc = (JComponent) c;
                    // Sets step number of a component
                    jc.putClientProperty("WizardPanel_contentSelectedIndex", new Integer(i));
                    // Sets steps names for a panel
                    jc.putClientProperty("WizardPanel_contentData", steps);
                    // Turn on subtitle creation on each step
                    jc.putClientProperty("WizardPanel_autoWizardStyle", Boolean.TRUE);
                    // Show steps on the left side with the image on the background
                    jc.putClientProperty("WizardPanel_contentDisplayed", Boolean.TRUE);
                    // Turn on numbering of all steps
                    jc.putClientProperty("WizardPanel_contentNumbered", Boolean.TRUE);
                }
            }
        }
        return panels;
    }

    public String getName() {
        return NbBundle.getMessage(BpelWizardAction.class, "CTL_BpelWizardAction");
    }

    @Override
    public String iconResource() {
        return "es/uca/takuan/netbeans/icon24x24.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }

    @Override
    protected int mode() {
        return CookieAction.MODE_EXACTLY_ONE;
    }

    @Override
    protected Class[] cookieClasses(){
        return new Class[]{BpelDataObject};
    }

    @Override
    protected void performAction(Node[] arg0) {
        try {
            // Check length
            if (arg0.length < 1) {
                ErrorManager.getDefault().log(ErrorManager.ERROR, "Must have one element at least");
                return;
            }
            // Create
            WizardDescriptor wizardDescriptor = new WizardDescriptor(getPanels());
            // {0} will be replaced by WizardDesriptor.Panel.getComponent().getName()
            wizardDescriptor.setTitleFormat(new MessageFormat("{0}"));
            wizardDescriptor.setTitle("Takuan");

            // Pass to the wizard the BPEL data object
            Lookup lookup = arg0[0].getLookup();
            MultiDataObject dataObject = (MultiDataObject) lookup.lookup(BpelDataObject);
            wizardDescriptor.putProperty(BPEL_DATA_OBJECT, dataObject);
            
            // Get the parent project
            Project project = OpenProjects.getDefault().getMainProject();

            // We can live without it, but just tell to the user
            if(project == null) {
                Message msg = new Message(NbBundle.getMessage(BpelWizardAction.class, "NO_PROJECT")
                        , Message.OK_CANCEL_OPTION);
                if(DialogDisplayer.getDefault().notify(msg) == Message.CANCEL_OPTION)
                    return;
            }

            // Store
            wizardDescriptor.putProperty(PROJECT, project);

            // Create the BPEL Bpel, parse the content of the file, and pass it to the wizard
            FileObject bpelFile = dataObject.getPrimaryFile();
            Bpel bpel = new Bpel(bpelFile.getInputStream());
            wizardDescriptor.putProperty(BPEL_OBJECT, bpel);
            TakuanOutputTopComponent.getDefault().println(bpelFile.getNameExt() + " loaded and parsed");

            wizardDescriptor.setTitle("Takuan [" + bpel.getName() + "]");

            // Show output window
            es.uca.takuan.netbeans.components.TakuanOutputTopComponent.getDefault().open();

            // Show
            Dialog dialog = DialogDisplayer.getDefault().createDialog(wizardDescriptor);
            dialog.setVisible(true);
            dialog.toFront();
            boolean cancelled = wizardDescriptor.getValue() != WizardDescriptor.FINISH_OPTION;
        } catch (FileNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } catch (DOMException ex) {
            Exceptions.printStackTrace(ex);
        } catch (SAXException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
