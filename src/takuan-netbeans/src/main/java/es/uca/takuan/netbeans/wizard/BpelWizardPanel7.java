/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uca.takuan.netbeans.wizard;

import es.uca.takuan.client.Bpel;
import es.uca.takuan.client.EventListener;
import es.uca.takuan.client.Remote;
import es.uca.takuan.client.Remote.InvalidState;
import es.uca.takuan.netbeans.components.TakuanOutputTopComponent;
import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;

public class BpelWizardPanel7 implements WizardDescriptor.Panel {

    private boolean runOnlyOnce;

    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private Component component;

    /** Constructor
     */
    public BpelWizardPanel7() {
        runOnlyOnce = true;
    }

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    public Component getComponent() {
        if (component == null) {
            component = new BpelVisualPanel7();
        }
        return component;
    }

    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
    // If you have context help:
    // return new HelpCtx(SampleWizardPanel1.class);
    }

    public boolean isValid() {
        // If it is always OK to press Next or Finish, then:
        return true;
    // If it depends on some condition (form filled out...), then:
    // return someCondition();
    // and when this condition changes (last form field filled in...) then:
    // fireChangeEvent();
    // and uncomment the complicated stuff below.
    }

    public final void addChangeListener(ChangeListener l) {
    }

    public final void removeChangeListener(ChangeListener l) {
    }
    /*
    private final Set<ChangeListener> listeners = new HashSet<ChangeListener>(1); // or can use ChangeSupport in NB 6.0
    public final void addChangeListener(ChangeListener l) {
    synchronized (listeners) {
    listeners.add(l);
    }
    }
    public final void removeChangeListener(ChangeListener l) {
    synchronized (listeners) {
    listeners.remove(l);
    }
    }
    protected final void fireChangeEvent() {
    Iterator<ChangeListener> it;
    synchronized (listeners) {
    it = new HashSet<ChangeListener>(listeners).iterator();
    }
    ChangeEvent ev = new ChangeEvent(this);
    while (it.hasNext()) {
    it.next().stateChanged(ev);
    }
    }
     */

    // You can use a settings object to keep track of state. Normally the
    // settings object will be the WizardDescriptor, so you can use
    // WizardDescriptor.getProperty & putProperty to store information entered
    // by the user.
    public void readSettings(Object settings) {
        runOnlyOnce = true;
    }

    public void storeSettings(Object settings) {

        // Be sure to run only once
        // I don't know why, this method is called twice
        if(runOnlyOnce) {
            runOnlyOnce = false;

            WizardDescriptor wizard;
            BpelVisualPanel7 panel = (BpelVisualPanel7) component;

            // Get
            wizard = (WizardDescriptor) settings;

            // URI
            wizard.putProperty(BpelWizardAction.SERVER_URI, panel.txtTakuanServletURI.getText());

            // Recover Wizard settings
            DataObject dataObject          = (DataObject) wizard.getProperty(BpelWizardAction.BPEL_DATA_OBJECT);
            Bpel       bpel                = (Bpel) wizard.getProperty(BpelWizardAction.BPEL_OBJECT);
            String     uri                 = panel.txtTakuanServletURI.getText();
            Map<String, File> dependencies = (Map<String, File>) wizard.getProperty(BpelWizardAction.DEPENDENCIES);

            // Create dialog
            JFrame frame = new JFrame();
            RunningDialog running = new RunningDialog(frame, true);

            // Create client thread
            EventListener listener = new RemoteEventListener(running);
            Remote remote = new Remote(listener);
            Thread thread = new Thread(remote);

            // Set execution parameters
            Map<String, Object> properties = wizard.getProperties();

            for(String key : properties.keySet()) {
                // Process properties that begin with SERVER_
                if(key.startsWith("SERVER_")) {
                    String subKey;

                    subKey = key.substring(7).toLowerCase();
                    remote.addParameter(subKey, (String) properties.get(key));
                }
            }

            // Run
            thread.start();
            TakuanOutputTopComponent.getDefault().println("Client thread running (" + thread.getName() + ")");
            remote.wait(Remote.READY);

            try{
                File file;

                // Connect
                remote.setURI(uri);

                // Queue files
                remote.addFile(dataObject.getPrimaryFile().getNameExt(), bpel.getStream(), bpel.getSize());

                Set<Entry<String, File>> files = dependencies.entrySet();
                Iterator <Entry<String, File>> i = files.iterator();
                while(i.hasNext()) {
                    Entry<String, File> entry;

                    entry = i.next();
                    file  = entry.getValue();
                    remote.addFile(entry.getKey(),
                                   new FileInputStream(file),
                                   file.length());
                }

                // Start with the sending
                remote.send();

            } catch (InvalidState ex) {
                Exceptions.printStackTrace(ex);
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null,
                        ex.getMessage(),
                        "Error reading a file",
                        JOptionPane.ERROR_MESSAGE + JOptionPane.CANCEL_OPTION);
                return;
            } catch(MalformedURLException ex) {
                JOptionPane.showMessageDialog(null,
                        ex.getMessage(),
                        "Malformed URL",
                        JOptionPane.ERROR_MESSAGE + JOptionPane.CANCEL_OPTION);
                return;
            }

            // Show dialog
            running.setVisible(true);
            running.toFront();

            // When we are here, the thread has ended
            TakuanOutputTopComponent.getDefault().println("Client thread ended (" + thread.getName() + ")");

            // Save the remote object
            wizard.putProperty(BpelWizardAction.REMOTE, remote);
        }
    }
}

