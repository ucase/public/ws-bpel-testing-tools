package es.uca.webservices.testgen.strategies.random;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeString;

public class PoissonStrategyTest {
	private PoissonStrategy gen;

	@Before
	public void setup() {
		gen = new PoissonStrategy(40);
		gen.setSeed("0000000000000000");
	}
	
	@Test
	public void intMinMax() throws GenerationException {
		final String pattern = "[0-9]{2}";
		final BigInteger min = new BigInteger("10");
        final BigInteger max = new BigInteger("99");
		final TypeInt tInt = new TypeInt();
		tInt.setMinValue(min);
		tInt.setMaxValue(max);
		final Pattern pat = Pattern.compile(pattern);
		for (int i = 0; i < 100; ++i) {
			final BigInteger generated =  gen.visit(tInt);
			//System.out.println(generated);
			assertTrue(pat.matcher(generated.toString()).matches());
		}
	}
	
	@Test
	public void intGen() throws GenerationException {
		final TypeInt tInt = new TypeInt();
		for (int i = 0; i < 100; ++i) {
			final BigInteger generated =  gen.visit(tInt);
			System.out.println(generated);
			
		}
	}
	
	@Test
    public void generatesFloat() throws GenerationException {
        TypeFloat tFloat = new TypeFloat();
        tFloat.setMinValue(new BigDecimal("10"));
        tFloat.setMaxValue(new BigDecimal("99"));
        for (int i = 0; i < 10; ++i) {
            final BigDecimal value = gen.visit(tFloat);
            System.out.println(value);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compareTo(tFloat.getMinValue()) >= 0);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compareTo(tFloat.getMaxValue()) <= 0);
        }
    }
	
	@Test
    public void generatesIntWithTotalDigits() throws GenerationException {
        TypeInt tInt = new TypeInt();
        tInt.setTotalDigits(2);
        for (int i = 0; i < 10; ++i) {
            final BigInteger value = gen.visit(tInt);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compareTo(new BigInteger("10")) >= 0);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compareTo(new BigInteger("99")) <= 0);
        }
    }
	
	@Test
    public void generatesFloatWithTotalDigits() throws GenerationException {
        TypeFloat tFloat = new TypeFloat();
        tFloat.setFractionDigits(1);
        tFloat.setTotalDigits(3);
        for (int i = 0; i < 10; ++i) {
            final BigDecimal value = gen.visit(tFloat);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compareTo(new BigDecimal("10")) >= 0);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compareTo(new BigDecimal("99")) <= 0);
        }
    }
	
	@Test(expected = UnsupportedOperationException.class)
	public void generateesString() throws GenerationException{
		TypeString tString = new TypeString();
		gen.visit(tString);
	}

}
