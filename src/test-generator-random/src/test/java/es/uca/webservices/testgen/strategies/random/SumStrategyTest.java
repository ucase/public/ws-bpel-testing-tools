package es.uca.webservices.testgen.strategies.random;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;

public class SumStrategyTest {
	private SumStrategy gen;
	private double sum=10;

	@Before
	public void setup() {
		gen = new SumStrategy();
		gen.setSeed("0000000000000000");
		gen.init(sum);
	}
	
	@Test
	public void sameInt() throws Exception{
		final TypeInt tInta = new TypeInt();
		final TypeInt tIntb = new TypeInt();
		final TypeInt tIntc = new TypeInt();
		
		Object[] generated = gen.generate(tInta,tIntb,tIntc);
		BigInteger sumGenerated = new BigInteger("0");
		for(int i=0;i<generated.length;++i){
			sumGenerated=sumGenerated.add(new BigInteger(generated[i].toString()));
		}
		assertEquals(new BigInteger(""+(int)sum),sumGenerated);
	}
	
	
	@Test
	public void sameDecimal() throws Exception{
		final TypeFloat tFloata = new TypeFloat();
		final TypeFloat tFloatb = new TypeFloat();
		final TypeFloat tFloatc = new TypeFloat();
		
		Object[] generated = gen.generate(tFloata,tFloatb,tFloatc);
		BigDecimal sumGenerated = new BigDecimal(0);
		for(int i=0;i<generated.length;++i){
			sumGenerated=sumGenerated.add(new BigDecimal(generated[i].toString()));
		}
		assertEquals("Sum must be " + sum + " and was " + sumGenerated,sum,sumGenerated.doubleValue(),0.1);
	}

}
