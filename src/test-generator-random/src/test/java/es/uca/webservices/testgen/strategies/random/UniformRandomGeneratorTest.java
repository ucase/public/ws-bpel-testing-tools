package es.uca.webservices.testgen.strategies.random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;

/**
 * Tests for the uniform random generator.
 */
public class UniformRandomGeneratorTest {

	private UniformRandomStrategy gen;

	@Before
	public void setup() {
		gen = new UniformRandomStrategy();
		gen.setSeed("0000000000000000");
	}

	/**
	 * Checks that the randomly generated strings only
	 * contain alphanumeric Latin characters. These are
	 * safe to use inside an XML document and most file
	 * formats.
	 */
	@Test
	public void stringsAreAlphanumeric() {
		final TypeString tString = new TypeString();

		final Pattern pat = Pattern.compile("[a-zA-Z0-9]*");
		for (int i = 0; i < 100; ++i) {
			final String generated = gen.visit(tString);
			assertTrue(pat.matcher(generated).matches());
		}
	}

	@Test
	public void stringsWithPatternAreCorrect() {
		final String pattern = "[0-9A-Z]{1,3}(\\.[A-Z]{3}(\\.X){0,1}){0,1}";
		final TypeString tString = new TypeString(pattern);
		final Pattern pat = Pattern.compile(pattern);
		for (int i = 0; i < 100; ++i) {
			final String generated = gen.visit(tString);
			assertTrue(pat.matcher(generated).matches());
		}
	}

	@Test
	public void alwaysSameStringFromSameSeed() {
		final TypeString tString = new TypeString("[a-z]{1,3}[.](com|org)");
		final String seed = "0000000000000000";

		gen.setSeed(seed);
		final String expected = gen.visit(tString);

		for (int i = 0; i < 200; ++i) {
			gen.setSeed(seed);
			final String generated = gen.visit(tString);
			assertEquals("The random generator should always produce the same string from the same seed", expected, generated);
			//System.out.println("Expected: " + expected + ", generated: " + generated);
		}
	}

	@Test
	public void intMinMax() throws Exception {
		final String pattern = "[0-9]{2}";
		final BigInteger min = new BigInteger("10");
        final BigInteger max = new BigInteger("99");
		final TypeInt tInt = new TypeInt();
		tInt.setMinValue(min);
		tInt.setMaxValue(max);
		final Pattern pat = Pattern.compile(pattern);
		for (int i = 0; i < 100; ++i) {
			final BigInteger generated = gen.visit(tInt);
			assertTrue(pat.matcher(generated.toString()).matches());
		}
	}
	
	@Test
	public void intAllowedValues () throws Exception{
		final TypeInt tInt = new TypeInt();
		List<BigInteger> listBigInteger = new ArrayList<BigInteger>(100);
		for(int i=1; i<10;++i){
		listBigInteger.add(new BigInteger(i+""));
		}
		tInt.setAllowedValues(listBigInteger);
		for (int i = 0; i < 100; ++i) {
			final BigInteger generated = gen.visit(tInt);
			assertTrue(listBigInteger.contains(generated));
		}
	}

	@Test
	public void listString () throws Exception{
		final TypeList tList = new TypeList(new TypeString("[a-z]{1,3}[.](com|org)"),1,5);
		for (int i = 0; i < 10; ++i) {
			gen.visit(tList);
		}
	}
	
	@Test
	public void uniformString () throws Exception{
		final TypeString tString = new TypeString("a[bc][de]");
		final int expectedValue=25;
		final Map<String, Integer> generatedValue = new HashMap<String, Integer>();
		generatedValue.put("abd", 0);
		generatedValue.put("abe", 0);
		generatedValue.put("acd", 0);
		generatedValue.put("ace", 0);
		for (int i = 0; i < 100; ++i) {
			final String generated = gen.visit(tString);
			generatedValue.put(generated,generatedValue.get(generated)+1);
		}
		double chiSquared = 0;
		for(String key : generatedValue.keySet()){
			chiSquared = chiSquared + (Math.pow(generatedValue.get(key)-expectedValue, 2)/expectedValue);
		}
		//http://www.mat.uda.cl/hsalinas/cursos/2009/estadistica/semestre%202/tabla-ji.pdf
		final double criticalValue = 7.82;
		assertTrue(chiSquared < criticalValue);
	}
	
	@Test
	public void uniformEnumerated () throws Exception{
		final TypeInt tInt = new TypeInt();
		List<BigInteger> listBigInteger = new ArrayList<BigInteger>(4);
		for(int i=0; i<=3;++i){
			listBigInteger.add(new BigInteger(i+""));
		}
		tInt.setAllowedValues(listBigInteger);
		final int expectedValue=25;
		final int[] generatedValue = new int []{0,0,0,0}; 
		for (int i = 0; i < 100; ++i) {
			final BigInteger generated = gen.visit(tInt);
			++generatedValue[generated.intValue()];
		}
		double chiSquared = 0;
		for(int i=0;i<generatedValue.length;++i){
			chiSquared = chiSquared + (Math.pow(generatedValue[i]-expectedValue, 2)/expectedValue);
		}
		//http://www.mat.uda.cl/hsalinas/cursos/2009/estadistica/semestre%202/tabla-ji.pdf
		final double criticalValue = 7.82;
		assertTrue(chiSquared < criticalValue);
	}
}
