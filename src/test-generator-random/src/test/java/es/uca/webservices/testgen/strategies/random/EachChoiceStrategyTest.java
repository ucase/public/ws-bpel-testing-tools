package es.uca.webservices.testgen.strategies.random;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.types.TypeInt;

public class EachChoiceStrategyTest {
	
	private EachChoiceStrategy gen;
	
	@Before
	public void setup() {
		gen = new EachChoiceStrategy();
		gen.setSeed("0000000000000000");
	}

	@Test
	public void intMaxMin() throws Exception{
		final TypeInt tInta = new TypeInt(new BigInteger(""+0), new BigInteger(""+1));
		final TypeInt tIntb = new TypeInt(new BigInteger(""+0), new BigInteger(""+1));
		
		Object[] generated = new Object[2];
		for(int i=0; i<2; ++i){
			generated[i] = gen.generate(tInta,tIntb);
			assertArrayEquals(new Object[]{new BigInteger(""+i),new BigInteger(""+i)},(Object[])generated[i]);
		}
	}

	@Test
	public void intAllowedValue() throws Exception{
		final List<BigInteger> allowedValueA = new ArrayList<BigInteger>();
		allowedValueA.add(new BigInteger(""+0));
		allowedValueA.add(new BigInteger(""+1));
		final TypeInt tInta = new TypeInt(allowedValueA);
		final List<BigInteger> allowedValueB = new ArrayList<BigInteger>();
		allowedValueB.add(new BigInteger(""+0));
		allowedValueB.add(new BigInteger(""+1));
		allowedValueB.add(new BigInteger(""+2));
		final TypeInt tIntb = new TypeInt(allowedValueB);
		final int numGenerated = 10;
		Object[] generated = new Object[numGenerated];
		for(int i=0; i<numGenerated; ++i){System.out.println();
			generated[i] = gen.generate(tInta,tIntb);
			//System.out.println(((Object[])generated[i])[0] + " " + ((Object[])generated[i])[1]);
		}
		assertArrayEquals(new Object[]{new BigInteger(""+0),new BigInteger(""+0)},(Object[])generated[0]);
		assertArrayEquals(new Object[]{new BigInteger(""+1),new BigInteger(""+1)},(Object[])generated[1]);
		assertEquals(new BigInteger(""+2),((Object[])generated[2])[1]);
		
	}
}
