package es.uca.webservices.testgen.strategies.random;

import static org.junit.Assert.assertArrayEquals;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeString;

public class CombStrategyTest {
	
	private CombStrategy gen;
	
	@Before
	public void setup() {
		gen = new CombStrategy();
	}
	
	@Test
	public void checkComb() throws Exception{
		final TypeInt tInta = new TypeInt(new BigInteger(""+0), new BigInteger(""+4));
		List<String> listValues = new ArrayList<String>();
		listValues.add("hello");
		listValues.add("bye");
		final TypeString tString = new TypeString(listValues);
		
		Object[] generated = new Object[5];
		for(int i=0; i<5; ++i){
			generated[i] = gen.generate(tInta,tString);
		}
		assertArrayEquals(new Object[]{new BigInteger("0"), "hello"}, (Object[])generated[0]);
		assertArrayEquals(new Object[]{new BigInteger("4"), "bye"}, (Object[])generated[1]);
		assertArrayEquals(new Object[]{new BigInteger("2"), "bye"},(Object[]) generated[2]);
		assertArrayEquals(new Object[]{new BigInteger("1"), "bye"},(Object[]) generated[3]);
		assertArrayEquals(new Object[]{new BigInteger("3"), "hello"},(Object[]) generated[4]);
		
	}

}
