package es.uca.webservices.testgen.strategies.random;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;

public class EqualValueTest {

	private EqualValueStrategy gen;

	@Before
	public void setup() {
		gen = new EqualValueStrategy();
		gen.init("Uniform");
		gen.setSeed("0000000000000000");
	}

	@Test
	public void sameInt() throws Exception {
		final TypeInt tInta = new TypeInt();
		final TypeInt tIntb = new TypeInt();

		Object[] generated = gen.generate(tInta, tIntb);
		assertTrue(generated[0] == generated[1]);
	}

	@Test
	public void sameString() throws Exception {
		final TypeString tStringa = new TypeString();
		final TypeString tStringb = new TypeString();

		Object[] generated = gen.generate(tStringa, tStringb);
		assertTrue(generated[0] == generated[1]);
	}

	@Test
	public void sameList() throws Exception {
		final TypeList tLista = new TypeList(new TypeString());
		final TypeList tListb = new TypeList(new TypeString());

		Object[] generated = gen.generate(tLista, tListb);
		assertTrue(generated[0] == generated[1]);
	}

	@Test(expected = GenerationException.class)
	public void exception() throws Exception {
		final TypeString tString = new TypeString();
		final TypeInt tInt = new TypeInt();

		Object[] generated = gen.generate(tString, tInt);
		assertTrue(generated[0] == generated[1]);
	}

}
