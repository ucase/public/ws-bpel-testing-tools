package es.uca.webservices.testgen.strategies.random;

import java.util.HashMap;
import java.util.Map;

import es.uca.webservices.testgen.api.generators.IStrategy;

/**
 * Contains useful constants for test-generator-random and its dependencies.
 *
 * @author Antonio García-Domínguez
 */
public final class Constants {

	private Constants() {
		// this class is not meant to be instantiated
	}

	/**
	 * Key in {@link #RANDOM_STRATEGIES} for the default catch-all strategy
	 * (normally, uniform distribution).
	 */
	public static final String DEFAULT_STRATEGY = "Uniform";

	/**
	 * Simple name-based map of random strategies, for its use in
	 * EqualValueStrategy and other strategies that use delegation.
	 */
	public static final Map<String, Class<? extends IStrategy>> RANDOM_STRATEGIES
		= new HashMap<String, Class<? extends IStrategy>>();

	static {
		RANDOM_STRATEGIES.put("Uniform", UniformRandomStrategy.class);
		RANDOM_STRATEGIES.put("Gaussian", GaussianStrategy.class);
		RANDOM_STRATEGIES.put("Normal", GaussianStrategy.class);
		RANDOM_STRATEGIES.put("Poisson", PoissonStrategy.class);
		RANDOM_STRATEGIES.put("Exponential", ExponentialStrategy.class);
		RANDOM_STRATEGIES.put("Sum", SumStrategy.class);
		RANDOM_STRATEGIES.put("EachChoice", EachChoiceStrategy.class);
		RANDOM_STRATEGIES.put("Comb", CombStrategy.class);
		RANDOM_STRATEGIES.put("EqualValue", EqualValueStrategy.class);
	}

}
