package es.uca.webservices.testgen.strategies.random;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uncommons.maths.random.GaussianGenerator;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTuple;

public class GaussianStrategy extends AbstractNumericStrategy {

	private static final Logger LOGGER = LoggerFactory.getLogger(GaussianStrategy.class);

	private double mean = 0;
	private double standardDeviation = 1;
	private GaussianGenerator gen;

	@Override
	public void init(Object... options) {
		if (options.length == 0) {
			return;
		}
		if (options.length != 2) {
			throw new IllegalArgumentException("Wrong number of arguments: should be either 0 or 2, but was " + options.length);
		}

		mean = Double.parseDouble(options[0].toString());
		standardDeviation = Double.parseDouble(options[1].toString());
	}

	public GaussianStrategy(double mean, double standardDeviation) {
		this.mean = mean;
		this.standardDeviation = standardDeviation;
	}

	public GaussianStrategy() {
		// for SnakeYAML and reflection: do not delete!
	}

	public GaussianGenerator getGaussianGenerator() {
		if (null == gen) {
			gen = new GaussianGenerator(mean, standardDeviation, getPRNG());
		}
		return gen;
	}

	public void setGaussianGenerator(GaussianGenerator gaussianGenerator) {
		this.gen = gaussianGenerator;
	}

	@Override
	public Object visit(TypeList typeList) throws GenerationException {
		return random(typeList.getMinNumElement(), typeList.getMaxNumElement());
	}

	@Override
	public Object visit(TypeString typeString) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate strings");
	}

	@Override
	public Object visit(TypeTuple typeTuple) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate tuples");
	}

	private void checkParameters(Number min, Number max){
		final double rangeUp = mean + 2 * standardDeviation;
		final double rangeLow =mean + (-2) * standardDeviation;
		if(rangeUp > max.doubleValue() || rangeLow < min.doubleValue()){
			LOGGER.warn("Out of the range: the random values ​​may have to be cut");
		}
	}

	@Override
	protected double random(double min, double max) {
		checkParameters(min, max);
		final double rand = getGaussianGenerator().nextValue().doubleValue();
		if (rand > max) {
			return max;
		}
		if (rand < min) {
			return min;
		}
		return rand;
	}

	@Override
    protected long random(long min, long max) {
		checkParameters(min, max);
        long rand = getGaussianGenerator().nextValue().longValue();
        if (rand > max) {
			return max;
		}
		if (rand < min) {
			return min;
		}
		return rand;
    }

	@Override
	public void setPRNG(Random prng) {
		super.setPRNG(prng);

		// Invalidate the GaussianGenerator, so it is recreated with the new PRNG on the next call
		gen = null;
	}
}
