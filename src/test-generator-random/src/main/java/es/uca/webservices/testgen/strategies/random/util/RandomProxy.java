package es.uca.webservices.testgen.strategies.random.util;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.testgen.api.generators.IRandomStrategy;

/**
 * {@link Random} reimplementation that acts as a proxy for this
 * CompositeStrategy's PRNG. Useful for making sure that strategies that
 * contain other strategies enforce the usage of a single PRNG with a
 * centrally controlled seed.
 *
 * @author Antonio García-Domínguez
 */
public final class RandomProxy extends Random {
	private static final Logger LOGGER = LoggerFactory.getLogger(RandomProxy.class);
	private static final long serialVersionUID = 1L;

	private IRandomStrategy provider;

	public RandomProxy(IRandomStrategy prngProvider) {
		this.provider = prngProvider;
	}

	@Override
	public boolean nextBoolean() {
		return provider.getPRNG().nextBoolean();
	}

	@Override
	public void nextBytes(byte[] bytes) {
		provider.getPRNG().nextBytes(bytes);
	}

	@Override
	public double nextDouble() {
		return provider.getPRNG().nextDouble();
	}

	@Override
	public float nextFloat() {
		return provider.getPRNG().nextFloat();
	}

	@Override
	public synchronized double nextGaussian() {
		return provider.getPRNG().nextGaussian();
	}

	@Override
	public int nextInt() {
		return provider.getPRNG().nextInt();
	}

	@Override
	public int nextInt(int n) {
		return provider.getPRNG().nextInt(n);
	}

	@Override
	public long nextLong() {
		return provider.getPRNG().nextLong();
	}

	@Override
	public synchronized void setSeed(long seed) {
		LOGGER.trace("Ignoring request for setSeed: only CompositeStrategy#setSeed will be honored");
	}
}