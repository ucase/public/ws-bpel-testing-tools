package es.uca.webservices.testgen.strategies.random;

import java.util.Random;

import org.uncommons.maths.random.MersenneTwisterRNG;

import es.uca.webservices.testgen.api.generators.IRandomStrategy;
import es.uca.webservices.testgen.api.generators.IStrategy;

/**
 * Common superclass for all {@link IStrategy} classes that use a PRNG in some way.
 *
 * @author Antonio García-Domínguez
 */
public abstract class AbstractRandomStrategy implements IRandomStrategy {
	private Random prng;

	@Override
	public void init(Object... options) throws IllegalArgumentException {
		// By default, strategies do not take options. Override if that is not the case.
		if (options.length > 0) {
			throw new IllegalArgumentException(this.getClass().getName() + " does not accept any options");
		}
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}

	@Override
	public void setSeed(String seed) {
		if (prng instanceof MersenneTwisterRNG || null == prng) {
			prng = new MersenneTwisterRNG(seed.getBytes());
		} else {
			prng.setSeed(Long.valueOf(seed));
		}
	}

	/**
	 * Returns the pseudo-random number generator currently in use.
	 */
	public synchronized Random getPRNG() {
		if (null == prng) {
			// It takes a while to collect enough entropy, so we lazily
			// initialize this field.
			prng = new MersenneTwisterRNG();
		}
		return prng;
	}
}