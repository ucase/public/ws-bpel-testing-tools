package es.uca.webservices.testgen.strategies.random;

import java.util.Random;

import org.uncommons.maths.random.PoissonGenerator;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTuple;

public class PoissonStrategy extends AbstractNumericStrategy {
	
	private double mean = 1;
	private PoissonGenerator gen;

	@Override
	public void init(Object... options) throws IllegalArgumentException {
		if (options.length == 0) {
			return;
		}
		if (options.length != 2) {
			throw new IllegalArgumentException("Wrong number of arguments: should be either 0 or 2, but was " + options.length);
		}
		mean = Double.parseDouble(options[0].toString());
	}
	
	public PoissonStrategy (double mean){
		this.mean = mean;
	}
	public PoissonStrategy (){
		// for SnakeYAML and reflection: do not delete!
	}
	
	public PoissonGenerator getPoissonGenerator() {
		if (null == gen) {
			gen = new PoissonGenerator(mean, getPRNG());
		}
		return gen;
	}

	public void setPoissonGenerator(PoissonGenerator poissonGenerator) {
		this.gen = poissonGenerator;
	}

	@Override
	public Object visit(TypeList typeList) throws GenerationException {
		return random(typeList.getMinNumElement(), typeList.getMaxNumElement());
	}

	@Override
	public Object visit(TypeString typeString) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate strings");
	}

	@Override
	public Object visit(TypeTuple typeTuple) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate tuples");
	}

	@Override
	protected double random(double minValue, double maxValue) {
		final double rand = getPoissonGenerator().nextValue().doubleValue();
		if (rand > maxValue) {
			return maxValue;
		}
		if (rand < minValue) {
			return minValue;
		}
		return rand;
	}

	@Override
	protected long random(long min, long max) {
		long rand = getPoissonGenerator().nextValue().longValue();
        if (rand > max) {
			return max;
		}
		if (rand < min) {
			return min;
		}
		return rand;
	}

	@Override
	public void setPRNG(Random prng) {
		super.setPRNG(prng);

		// Invalidate the PoissonGenerator, so it is recreated with the new PRNG on the next call
		gen = null;
	}
}
