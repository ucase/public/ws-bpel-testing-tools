package es.uca.webservices.testgen.strategies.random;

import java.math.BigInteger;
import java.util.IdentityHashMap;
import java.util.Map;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.generators.ITypeVisitor;
import es.uca.webservices.testgen.api.types.AbstractNumber;
import es.uca.webservices.testgen.api.types.TypeDate;
import es.uca.webservices.testgen.api.types.TypeDateTime;
import es.uca.webservices.testgen.api.types.TypeDuration;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTime;
import es.uca.webservices.testgen.api.types.TypeTuple;

public class EachChoiceStrategy  extends AbstractRandomStrategy implements ITypeVisitor {
	private static final int MAXVALUEGENERATED = 50;

	private Map<IType, Integer> valuesGenerated = new IdentityHashMap<IType, Integer>();
	private Map<IType, Boolean> allValuesGenerated = new IdentityHashMap<IType, Boolean>();

	@Override
	public Object[] generate(IType... abstractTypes) throws GenerationException {
		final Object[] values = new Object[abstractTypes.length];
		for (int i = 0; i < values.length; i++) {
			values[i] = visit(abstractTypes[i]);
		}
		return values;
	}

	@Override
	public Object visit(IType type) throws GenerationException {
		return type.accept(this);
	}

	@Override
	public Object visit(TypeInt typeInt) throws GenerationException {
		if(!typeInt.getAllowedValues().isEmpty()){
			return generatedAllowedValues(typeInt);
		}else{
			final BigInteger max=typeInt.getMaxValue();
			final BigInteger min=typeInt.getMinValue();
			if(max.subtract(min).compareTo(new BigInteger(""+MAXVALUEGENERATED))==1){
				throw new GenerationException("The range [max-min] can´t be greater than 50");
			}
			if(allValuesGenerated.containsKey(typeInt)){
				return new BigInteger(""+randomInteger(0,MAXVALUEGENERATED));
			}else{
				int valueIndex=0;
				if(valuesGenerated.containsKey(typeInt)){
					valueIndex = valuesGenerated.get(typeInt);
				}
				if(valueIndex == MAXVALUEGENERATED){
					allValuesGenerated.put(typeInt, true);
				}
				final BigInteger generated = min.add(new BigInteger(""+valueIndex));
				valuesGenerated.put(typeInt, ++valueIndex);
				return generated;
			}
		}
	}

	private Object generatedAllowedValues(AbstractNumber<?> type)
			throws GenerationException {
		if(type.getAllowedValues().size() > MAXVALUEGENERATED){
			throw new GenerationException("List of values can´t be greater than 50");
		}
		if(allValuesGenerated.containsKey(type)){
			return randomInteger(0,type.getAllowedValues().size()-1);
		}else if(valuesGenerated.containsKey(type)){
			int valueIndex = valuesGenerated.get(type);
			if(valueIndex == type.getAllowedValues().size()-1){
				allValuesGenerated.put(type, true);
				
			}
			Object generated = type.getAllowedValues().get(valueIndex);
			valuesGenerated.put(type, ++valueIndex);
			return generated;
		}else {
			Object generated = type.getAllowedValues().get(0);
			valuesGenerated.put(type, 1);
			return generated;
		}
	}
	
	private String generatedListValues(TypeString type)
			throws GenerationException {
		if(type.getListValues().size() > MAXVALUEGENERATED){
			throw new GenerationException("List of values can´t be greater than 50");
		}
		if(allValuesGenerated.containsKey(type)){
			final int index = randomInteger(0,type.getListValues().size()-1);
			return (String) type.getListValues().get(index);
		}else if(valuesGenerated.containsKey(type)){
			int valueIndex = valuesGenerated.get(type);
			if(valueIndex == type.getListValues().size()-1){
				allValuesGenerated.put(type, true);
				
			}
			Object generated = type.getListValues().get(valueIndex);
			valuesGenerated.put(type, ++valueIndex);
			return (String) generated;
		}else {
			Object generated = type.getListValues().get(0);
			valuesGenerated.put(type, 1);
			return (String) generated;
		}
	}

	@Override
	public Object visit(TypeDate typeDate) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate dates");
	}

	@Override
	public Object visit(TypeDateTime typeDateTime) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate datetime");
	}

	@Override
	public Object visit(TypeDuration typeDuration) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate duration");
	}

	@Override
	public BigInteger visit(TypeFloat typeFloat) throws GenerationException {
		if(!typeFloat.getAllowedValues().isEmpty()){
			return (BigInteger) generatedAllowedValues(typeFloat);
		}else{
			throw new UnsupportedOperationException("Cannot generate without list of values");
		}
	}

	@Override
	public Object visit(TypeList typeList) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate list");
	}

	@Override
	public String visit(TypeString typeString) throws GenerationException {
		if(!typeString.getListValues().isEmpty()){
			return generatedListValues(typeString);
		}else{
			throw new UnsupportedOperationException("Cannot generate without list of values");
		}
	}

	@Override
	public Object visit(TypeTime typeTime) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate times");
	}

	@Override
	public Object visit(TypeTuple typeTuple) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate tuples");
	}

	/**
     * Generates a random integer over the range [min, max].
     */
    private int randomInteger(int min, int max) {
        return getPRNG().nextInt(max - min + 1) + min;
    }
}
