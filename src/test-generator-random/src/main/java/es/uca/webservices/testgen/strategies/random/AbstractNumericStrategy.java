package es.uca.webservices.testgen.strategies.random;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.generators.ITypeVisitor;
import es.uca.webservices.testgen.api.types.TypeDate;
import es.uca.webservices.testgen.api.types.TypeDateTime;
import es.uca.webservices.testgen.api.types.TypeDuration;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeTime;

/**
 * Abstract superclass for all strategies that generate values using random
 * distributions (uniform, exponential, gaussian, Poisson and so on).
 */
public abstract class AbstractNumericStrategy extends AbstractRandomStrategy implements ITypeVisitor {

	private static final int DEFAULT_SECOND = 0;
	private static final int DEFAULT_MINUTE = 0;
	private static final int DEFAULT_HOUR = 0;
	private static final int DEFAULT_DAY = 1;
	private static final int DEFAULT_MONTH = 0;
	private static final int DEFAULT_YEAR = 1970;
	private static DatatypeFactory dataTypeFactory;

	public AbstractNumericStrategy() {
		super();
	}

	@Override
	public Object[] generate(IType... abstractTypes) throws GenerationException {
		final Object[] values = new Object[abstractTypes.length];
		for (int i = 0; i < values.length; i++) {
			values[i] = visit(abstractTypes[i]);
		}
		return values;
	}

	@Override
	public Object visit(IType abstractType) throws GenerationException {
		return abstractType.accept(this);
	}

	@Override
	public BigInteger visit(TypeInt typeInt) throws GenerationException {
		if (!typeInt.getAllowedValues().isEmpty()) {
			throw new UnsupportedOperationException(
					"TypeInt.getAllowedValues: not implement Gaussian strategy");
		} else {
			int totalDigits = typeInt.getTotalDigits();
			if (totalDigits == 0) {
				return random(typeInt.getMinValue(), typeInt.getMaxValue());
			} else {
				final BigInteger minTotalDigits = BigInteger.TEN
						.pow(totalDigits - 1);
				final BigInteger maxTotalDigits = BigInteger.TEN.pow(
						totalDigits).subtract(BigInteger.ONE);
	
				final BigInteger min = typeInt.getMinValue().compareTo(
						minTotalDigits) == 1 ? typeInt.getMinValue()
						: minTotalDigits;
				final BigInteger max = typeInt.getMaxValue().compareTo(
						maxTotalDigits) == -1 ? typeInt.getMaxValue()
						: maxTotalDigits;
				return random(min, max);
			}
		}
	}

	@Override
	public BigDecimal visit(TypeFloat typeFloat) throws GenerationException {
		if (!typeFloat.getAllowedValues().isEmpty()) {
			throw new UnsupportedOperationException(
					"TypeFloat.getAllowedValues: not implement Gaussian strategy");
		} else {
			int totalDigits = typeFloat.getTotalDigits();
			BigDecimal bd = null;
			final int fractionDigits = typeFloat.getFractionDigits() > 0 ? typeFloat
					.getFractionDigits() : 0;
			double randDouble;
			if (totalDigits == 0) {
				randDouble = random(typeFloat.getMinValue().floatValue(),
						typeFloat.getMaxValue().floatValue());
			} else {
				final float minTotalDigits = (float) Math.pow(10, totalDigits
						- 1 - fractionDigits);
				final float maxTotalDigits = (float) Math.pow(10, totalDigits
						- fractionDigits) - 1;
				final float min = typeFloat.getMinValue().floatValue() > minTotalDigits ? typeFloat
						.getMinValue().floatValue() : minTotalDigits;
				final float max = typeFloat.getMaxValue().floatValue() < maxTotalDigits ? typeFloat
						.getMinValue().floatValue() : maxTotalDigits;
				randDouble = random(min, max);
			}
			bd = new BigDecimal(randDouble + "");
			if (typeFloat.getFractionDigits() >= 0) {
				bd = bd.setScale(fractionDigits, BigDecimal.ROUND_HALF_UP);
			}
			return bd;
		}
	}

	public XMLGregorianCalendar visit(TypeTime typeTime) throws GenerationException {
		final Calendar minTime = toCalendar(typeTime.getMinValue());
		final Calendar maxTime = toCalendar(typeTime.getMaxValue());
		final Calendar randTime = random(minTime, maxTime);

		final DatatypeFactory factory = getDatatypeFactory();
		XMLGregorianCalendar calendar = factory.newXMLGregorianCalendarTime(
				randTime.get(Calendar.HOUR_OF_DAY),
				randTime.get(Calendar.MINUTE), randTime.get(Calendar.SECOND),
				DatatypeConstants.FIELD_UNDEFINED,
				DatatypeConstants.FIELD_UNDEFINED);
		return calendar;
	}

	@Override
	public XMLGregorianCalendar visit(TypeDate typeDate) throws GenerationException {
		final Calendar minCalendar = toCalendar(typeDate.getMinValue());
		final Calendar maxCalendar = toCalendar(typeDate.getMaxValue());
		final Calendar randCalendar = random(minCalendar, maxCalendar);

		final DatatypeFactory factory = getDatatypeFactory();
		XMLGregorianCalendar calendar = factory.newXMLGregorianCalendarDate(
				randCalendar.get(Calendar.YEAR),
				randCalendar.get(Calendar.MONTH) + 1,
				randCalendar.get(Calendar.DAY_OF_MONTH),
				DatatypeConstants.FIELD_UNDEFINED);
		return calendar;
	}

	@Override
	public XMLGregorianCalendar visit(TypeDateTime typeDateTime) throws GenerationException {
		final Calendar minCalendar = toCalendar(typeDateTime.getMinValue());
		final Calendar maxCalendar = toCalendar(typeDateTime.getMaxValue());
		final Calendar randCalendar = random(minCalendar, maxCalendar);
	
		final DatatypeFactory factory = getDatatypeFactory();
		XMLGregorianCalendar calendar = factory.newXMLGregorianCalendar(
				randCalendar.get(Calendar.YEAR),
				randCalendar.get(Calendar.MONTH) + 1,
				randCalendar.get(Calendar.DAY_OF_MONTH),
				randCalendar.get(Calendar.HOUR_OF_DAY),
				randCalendar.get(Calendar.MINUTE),
				randCalendar.get(Calendar.SECOND),
				randCalendar.get(Calendar.MILLISECOND),
				DatatypeConstants.FIELD_UNDEFINED);
		return calendar;
	}

	/**
	 * Generates a TypeDuration random of the typeDuration the terms of the
	 * received
	 * 
	 * @param typeDuration
	 * 
	 * @return
	 * 
	 * @throws GenerationException
	 */
	
	@Override
	public Duration visit(TypeDuration typeDuration) throws GenerationException {
		final Duration minDuration = typeDuration.getMinValue();
		final Duration maxDuration = typeDuration.getMaxValue();
		return random(minDuration, maxDuration);
	}

	/**
     * Generated a BigInteger randomly distributed over the range [min, max]
     * 
     * @param min
     * @param max
     * @return
     */
	protected BigInteger random(BigInteger minValue, BigInteger maxValue){
		final long rand = random(minValue.longValue(), maxValue.longValue());
		return new BigInteger(rand + "");
	}

    /**
     * Generated a float randomly distributed over the range [min, max]
     * 
     * @param max
     * @param min
     * @return
     */
	protected abstract double random(double minValue, double maxValue);

    /**
     * Generated a Calendar randomly distributed over the range [min, max]
     * @param minCalendar
     * @param maxCalendar
     * @return 
     */
	protected Calendar random(Calendar minCalendar, Calendar maxCalendar){
		final long minMillis = minCalendar.getTimeInMillis();
        final long maxMillis = maxCalendar.getTimeInMillis();
        final long rndMillis = random(minMillis, maxMillis);
        final Calendar randCalendar = new GregorianCalendar();
        randCalendar.setTimeInMillis(rndMillis);
        return randCalendar;
	}

    /**
     * Generate a random long integer over the range [min, max]
     * 
     * @param max
     * @param min
     * @return
     */
	protected abstract long random(long min, long max);

	/**
     * Generated a Duration randomly distributed over the range [min, max]
     * @param min
     * @param max
     * @return 
     */
    
    protected Duration random(Duration min, Duration max) throws GenerationException {
        final long minMillis = min.getTimeInMillis(new GregorianCalendar(1900,0,1));
        final long maxMillis = max.getTimeInMillis(new GregorianCalendar(1900,0,1));
        final long rndMillis = random(minMillis, maxMillis);
        
        final DatatypeFactory factory = getDatatypeFactory();
        return factory.newDuration(rndMillis);
    }

    /**
	 * Constructor Calendar with a XMLGregorianCalendar
	 * 
	 * @param xmlCalendar
	 * @return
	 */
	protected Calendar toCalendar(final XMLGregorianCalendar xmlCalendar) {
		final int year = xmlCalendar.getYear() != DatatypeConstants.FIELD_UNDEFINED ? xmlCalendar.getYear() : DEFAULT_YEAR;
		final int month = xmlCalendar.getMonth() != DatatypeConstants.FIELD_UNDEFINED ? xmlCalendar.getMonth() - 1 : DEFAULT_MONTH;
		final int day = xmlCalendar.getDay() != DatatypeConstants.FIELD_UNDEFINED ? xmlCalendar.getDay() : DEFAULT_DAY;
		final int hour = xmlCalendar.getHour() != DatatypeConstants.FIELD_UNDEFINED ? xmlCalendar.getHour() : DEFAULT_HOUR;
		final int minute = xmlCalendar.getMinute() != DatatypeConstants.FIELD_UNDEFINED ? xmlCalendar.getMinute() : DEFAULT_MINUTE;
		final int second = xmlCalendar.getSecond() != DatatypeConstants.FIELD_UNDEFINED ? xmlCalendar.getSecond() : DEFAULT_SECOND;

		final Calendar calendar = new GregorianCalendar(year, month, day, hour, minute, second);
		calendar.setLenient(false);
		return calendar;
	}

	protected synchronized DatatypeFactory getDatatypeFactory()
			throws GenerationException {
		if (dataTypeFactory == null) {
			try {
				dataTypeFactory = DatatypeFactory.newInstance();
			} catch (DatatypeConfigurationException ex) {
				throw new GenerationException(ex);
			}
		}
		return dataTypeFactory;
	}

}