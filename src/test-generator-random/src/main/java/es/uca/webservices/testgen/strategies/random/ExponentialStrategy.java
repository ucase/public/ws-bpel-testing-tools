package es.uca.webservices.testgen.strategies.random;

import java.util.Random;

import org.uncommons.maths.random.ExponentialGenerator;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTuple;

public class ExponentialStrategy extends AbstractNumericStrategy {
	
	private double rate = 10;
	private ExponentialGenerator gen;

	@Override
	public void init(Object... options) {
		if (options.length == 0) {
			return;
		}
		if (options.length != 1) {
			throw new IllegalArgumentException("Wrong number of arguments: should be either 0 or 1, but was " + options.length);
		}
		rate = Double.parseDouble(options[0].toString());
	}
	
	public ExponentialStrategy (double rate){
		this.rate = rate;
	}
	
	public ExponentialStrategy() {
		// for SnakeYAML and reflection: do not delete!
	}
	
	public ExponentialGenerator getExponentialGenerator() {
		if (null == gen) {
			gen = new ExponentialGenerator(rate, getPRNG());
		}
		return gen;
	}
	
	public void setExponentialGenerator(ExponentialGenerator exponentialGenerator) {
		this.gen = exponentialGenerator;
	}
	
	@Override
	public Object visit(TypeList typeList) throws GenerationException {
		return random(typeList.getMinNumElement(), typeList.getMaxNumElement());
	}

	@Override
	public Object visit(TypeString typeString) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate strings");
	}

	@Override
	public Object visit(TypeTuple typeTuple) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate tuples");
	}

	@Override
	protected double random(double minValue, double maxValue) {
		final double rand = getExponentialGenerator().nextValue().doubleValue();
		if (rand > maxValue) {
			return maxValue;
		}
		if (rand < minValue) {
			return minValue;
		}
		return rand;
	}

	@Override
	protected long random(long min, long max) {
		long rand = getExponentialGenerator().nextValue().longValue();
        if (rand > max) {
			return max;
		}
		if (rand < min) {
			return min;
		}
		return rand;
	}

	@Override
	public void setPRNG(Random prng) {
		super.setPRNG(prng);

		// Invalidate the current ExponentialGenerator, so it is recreated on the next call
		gen = null;
	}
}
