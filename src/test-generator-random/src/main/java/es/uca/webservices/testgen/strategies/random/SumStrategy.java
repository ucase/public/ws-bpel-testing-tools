package es.uca.webservices.testgen.strategies.random;

import java.math.BigDecimal;
import java.math.BigInteger;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;

public class SumStrategy extends AbstractRandomStrategy {
	private BigDecimal sum;

	@Override
	public Object[] generate(IType... abstractTypes) throws GenerationException {
		checkTypes(abstractTypes);
		if(abstractTypes[0] instanceof TypeFloat){
			return generatedBigDecimal(abstractTypes);
		} else {
			return generatedBigInteger(abstractTypes);
		}
	}

	private Object[] generatedBigDecimal(IType... abstractTypes) {
		final BigDecimal[] valuesDouble = new BigDecimal[abstractTypes.length];
		BigDecimal sumHelp = sum;
		for (int i = 0; i < abstractTypes.length-1; ++i) {
			final double percent = getPRNG().nextDouble();
			valuesDouble[i] = sumHelp.multiply(new BigDecimal(percent));
			sumHelp = sumHelp.subtract(valuesDouble[i]);
		}
		valuesDouble[valuesDouble.length - 1] = sumHelp;

		final Object[] values = new Object[valuesDouble.length];
		System.arraycopy(valuesDouble, 0, values, 0, valuesDouble.length);
		return values;
	}
	
	private Object[] generatedBigInteger(IType... abstractTypes) {
		// Random only produces up to nextLong, so we'll use longs inside this function anyway 
		final long[] valuesLong = new long[abstractTypes.length];
		long sumHelp = sum.longValue();
		for (int i = 0; i < abstractTypes.length-1; ++i) {
			final long newValue = getPRNG().nextLong() % (sumHelp + 1);
			valuesLong[i] = newValue;
			sumHelp = sumHelp - newValue;
		}
		valuesLong[valuesLong.length - 1] = sumHelp;

		final Object[] values = new Object[valuesLong.length];
		for (int i = 0; i < values.length; ++i) {
			values[i] = new BigInteger(""+valuesLong[i]);
		}
		return values;
	}

	@Override
	public void init(Object... options) throws IllegalArgumentException {
		if (options.length != 1) {
			throw new IllegalArgumentException("Wrong number of arguments: should be 1, was " + options.length);
		}
		sum = new BigDecimal(options[0].toString());
	}

	private void checkTypes(IType... abstractTypes)
			throws GenerationException {
		if(abstractTypes[0] instanceof TypeInt || abstractTypes[0] instanceof TypeFloat){
			for(IType type : abstractTypes){
				if(type.getClass() != abstractTypes[0].getClass()){
					throw new GenerationException("The types must be equal");
				}
			}
		} else {
			throw new GenerationException("The types must be TypeInt or TypeFloat");
		}
	}

}
