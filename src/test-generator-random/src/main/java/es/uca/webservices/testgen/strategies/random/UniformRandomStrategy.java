/*
 *  Copyright 2011-2013 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.strategies.random;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import nl.flotsam.xeger.Xeger;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTuple;

/**
 * Class that generates random test cases.
 * 
 */
public class UniformRandomStrategy extends AbstractNumericStrategy  {
    private Map<String, Xeger> automatonCache = new HashMap<String, Xeger>();

    public UniformRandomStrategy() {
    	// no-arg constructor
    }

	@Override
	public void init(Object... options) throws IllegalArgumentException {
		if (options.length > 0) {
			throw new IllegalArgumentException("No options are available");
		}
	}

	@Override
	public void setPRNG(Random prng) {
		super.setPRNG(prng);
		updateAutomatonCache();
	}

	@Override
	public void setSeed(String seed) {
		super.setSeed(seed);
		updateAutomatonCache();
	}
	
	@Override
	public BigInteger visit(TypeInt typeInt) throws GenerationException {
		if (!typeInt.getAllowedValues().isEmpty()) {
			int rand = random(0,typeInt.getAllowedValues().size()-1);
			return typeInt.getAllowedValues().get(rand);
		} else {
			return super.visit(typeInt);
		}
	}
	
	@Override
	public BigDecimal visit(TypeFloat typeFloat) throws GenerationException {
		if (!typeFloat.getAllowedValues().isEmpty()) {
			int rand = random(0,typeFloat.getAllowedValues().size()-1);
			return typeFloat.getAllowedValues().get(rand);
		} else {
			return super.visit(typeFloat);
		}
	}

    /**
     * Generated a List<Object> random of the typeList the terms of the received
     * 
     * @param typeList
     * @return
     */
	@Override
    public List<Object> visit(TypeList typeList) throws GenerationException {
        final int size = random(typeList.getMinNumElement(), typeList.getMaxNumElement());
        final List<Object> result = new ArrayList<Object>(size);
        for (int i = 0; i < size; ++i) {
            result.add(visit(typeList.getType()));
        }
        return result;
    }

    /**
     * Generated a List<Object> random of the typeTuple the terms of the
     * received
     * 
     * @param typeTuple
     * @return
     */
	@Override
    public List<Object> visit(TypeTuple typeTuple) throws GenerationException {
        final List<Object> result = new ArrayList<Object>(typeTuple.size());
        for (int i = 0; i < typeTuple.size(); ++i) {
            result.add(visit(typeTuple.getIType(i)));
        }
        return result;
    }

    /**
     * Generated a String random of the typeString the terms of the received
     * 
     * @param typeString
     * @return
     */
	@Override
    public String visit(TypeString typeString) {
        if (typeString.getListValues().isEmpty() && !typeString.isSetPattern()) {
            return random(random(typeString.getMinLength(), typeString.getMaxLength()));
        } else if (typeString.isSetPattern()) {
        	return getXeger(typeString.getPattern()).generate();
        } else {
            return (String) random(typeString.getListValues());
        }
    }

	@Override
    protected BigInteger random(BigInteger min, BigInteger max) {
        final BigInteger diff = max.subtract(min);
        BigInteger result = null;
        do {
            final BigInteger rand = new BigInteger(diff.bitLength(), getPRNG());
            result = min.add(rand);
        } while (result.compareTo(max) > 0);
        return result;
    }

    @Override
	protected long random(long min, long max) {
	    long rndLong = getPRNG().nextLong();
	    if (rndLong < 0) {
	        rndLong = -rndLong;
	    }
	    return rndLong % (max - min + 1) + min;
	}

	@Override
	protected double random(double min, double max) {
	    return (max - min) * getPRNG().nextFloat() + min;
	}

	/**
     * Generated an int randomly distributed over the range [min, max]
     * 
     * @param max
     * @param min
     * @return
     */
    public int random(int min, int max) {
        return getPRNG().nextInt(max - min + 1) + min;
    }

    /**
     * Generates a random alphanumeric string of a certain <code>length</code>.
     * 
     * @param length
     * @return
     */
    private String random(int length) {
        return getXeger("[A-Za-z0-9]{" + length + "}").generate();
    }

    /**
     * Returns a random item from the list
     * @param listValues
     * @return 
     */
    private Object random(List<?> listValues) {
        int pos = random(0, listValues.size() - 1);
        return listValues.get(pos);
    }

    private Xeger getXeger(String pattern) {
    	if (automatonCache.containsKey(pattern)) {
    		return automatonCache.get(pattern);
    	}
    	else {
    		final Xeger xeger = new Xeger(pattern, getPRNG());
    		automatonCache.put(pattern, xeger);
    		return xeger;
    	}
    }

	private void updateAutomatonCache() {
		for (Map.Entry<String, Xeger> entry : automatonCache.entrySet()) {
			entry.getValue().setRandom(getPRNG());
		}
	}

}
