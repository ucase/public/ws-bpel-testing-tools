package es.uca.webservices.testgen.strategies.random;

import java.util.Random;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IRandomStrategy;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;

/**
 * Strategy that ensures the value of two or more targets is the same. It is not random
 * by itself, but it might need a PRNG in the nested strategy is random.
 */
public class EqualValueStrategy implements IRandomStrategy {

	private IStrategy strategy;

	@Override
	public void init(Object... options) throws IllegalArgumentException {
		if(options.length > 0 && options[0] instanceof String){
			final String strategyName = ((String) options[0]).replaceAll("\"", "");
			if(!Constants.RANDOM_STRATEGIES.containsKey(strategyName)){
				throw new IllegalArgumentException("Strategy " + options[0] + " does not exist");
			}

			try {
				strategy = getStrategyByName(strategyName);
			} catch (Exception e) {
				throw new IllegalArgumentException(e);
			}
			if (strategy.getClass() == this.getClass()) {
				throw new IllegalArgumentException("Cannot nest EqualValue strategies inside each other");
			}

			final Object[] optionsStrategy = new Object[options.length -1];
			System.arraycopy(options, 1, optionsStrategy, 0, optionsStrategy.length);
			strategy.init(optionsStrategy);
		} else {
			try {
				strategy = getStrategyByName(Constants.DEFAULT_STRATEGY);
			} catch (Exception e) {
				throw new IllegalArgumentException(e);
			}
			strategy.init(options);
		}
		
	}

	@Override
	public Object[] generate(IType... abstractTypes) throws GenerationException {
		checkEqualTypes(abstractTypes);
		final Object value = strategy.generate(abstractTypes[0]);
		final Object[] values = new Object[abstractTypes.length];
		for(int i=0; i<values.length;++i){
			values[i]=value;
		}
		return values;
	}

	private IStrategy getStrategyByName(final String strategyName) throws InstantiationException, IllegalAccessException {
		return Constants.RANDOM_STRATEGIES.get(strategyName).newInstance();
	}

	private void checkEqualTypes(IType... abstractTypes)
			throws GenerationException {
		for(IType type : abstractTypes){
			if(type.getClass() != abstractTypes[0].getClass()){
				throw new GenerationException("The types must be equal");
			}
		}
	}

	@Override
	public void setSeed(String seed) {
		if (strategy instanceof IRandomStrategy) {
			((IRandomStrategy)strategy).setSeed(seed);
		}
	}

	@Override
	public Random getPRNG() {
		if (strategy instanceof IRandomStrategy) {
			return ((IRandomStrategy)strategy).getPRNG();
		}
		throw new IllegalArgumentException("The nested strategy is not an IRandomStrategy: cannot return a PRNG");
	}

	@Override
	public void setPRNG(Random prng) {
		if (strategy instanceof IRandomStrategy) {
			((IRandomStrategy)strategy).setPRNG(prng);
		}
	}

}
