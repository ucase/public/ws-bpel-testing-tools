package es.uca.webservices.testgen.strategies.random;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.generators.ITypeVisitor;
import es.uca.webservices.testgen.api.types.AbstractNumber;
import es.uca.webservices.testgen.api.types.TypeDate;
import es.uca.webservices.testgen.api.types.TypeDateTime;
import es.uca.webservices.testgen.api.types.TypeDuration;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTime;
import es.uca.webservices.testgen.api.types.TypeTuple;

public class CombStrategy extends AbstractRandomStrategy implements ITypeVisitor {
	private static final int MAXCOMBGENERATED = 50;

	/**
	 * How many values from this type have been produced?
	 */
	private Map<IType, Integer> valuesGenerated = new IdentityHashMap<IType, Integer>();

	/**
	 * Have all the values from this IType been produced? In that case, the
	 * strategy will start to perform random sampling of all the possible
	 * values.
	 * 
	 * We need to use a IdentityHashMap, as there is no equivalent
	 * IdentityHashSet in Java SE.
	 */
	private Map<IType, Boolean> allValuesGenerated = new IdentityHashMap<IType, Boolean>();

	private int numberOfCombinations;
	private int pos = 0;

	@Override
	public Object[] generate(IType... abstractTypes) throws GenerationException {
		// Set up an array with all the possible values
		checkCombination(abstractTypes);
		final List<Object[]> allValues = allValuesAbstractTypes(abstractTypes);
		
		final Object[] allCombinations = cartesianProduct(allValues);
		final List<Object> combCombinations = buildCombination(allCombinations);
		pos = (pos + 1) % numberOfCombinations;

		return (Object[]) combCombinations.get(pos-1);
	}

	private List<Object> buildCombination(Object[] allCombinations) {
		final int last = allCombinations.length -1;
		List<Object> result = new ArrayList<Object>();
		result.add(allCombinations[0]); // The 1st combination is added
		result.add(allCombinations[last]); 	 // Also the last combination
		float p=(last+1)/2;
		result.add(allCombinations[Math.round(p)]); // The middle also is added
		
		while(result.size()<allCombinations.length){
			float k = (float)p/2; // k moves by the middle point of each interval
			int counter = 2;
			while(k < last && result.size()<allCombinations.length){
				final Object c = allCombinations[Math.round(k)];
				if(!result.contains(c)){
					result.add(c);
				}
				k= counter * Math.round(p/2);// when k is multiplied by counter, it advances to the next interval
				++counter;
			}
			p=p/2;
		}

		return result;
	}

	private Object[] cartesianProduct (final List<Object[]> allValues) {
		numberOfCombinations = 1;
		for(int i = 0;i<allValues.size(); ++i){
			numberOfCombinations = allValues.get(i).length * numberOfCombinations;
		}
		
		Object[] result = allValues.get(0);
		for(int i=1; i<allValues.size(); ++i){
			result = cartesianProductAux(result, allValues.get(i));
		}
		return result;
	}
	
	private Object[] cartesianProductAux (final Object[] a, final Object[] b) {
		List<Object[]> result = new ArrayList<Object[]>();
		for(Object aO:a){
			for(Object bO:b){
				result.add(new Object[]{aO,bO});
			}
		}
		
		return result.toArray();
	}

	/**
	 * Create a List<Object[]> with all values of abstractTypes
	 * @param abstractTypes
	 * @return
	 */
	private List<Object[]> allValuesAbstractTypes(IType... abstractTypes) {
		final List<Object[]> allValues = new ArrayList<Object[]>(abstractTypes.length);
		for (int i = 0; i < abstractTypes.length; i++) {
			if(abstractTypes[i] instanceof TypeInt){
				
				if(!((TypeInt)abstractTypes[i]).getAllowedValues().isEmpty()){
					allValues.add(((TypeInt)abstractTypes[i]).getAllowedValues().toArray());
				}else{
					BigInteger min=((TypeInt)abstractTypes[i]).getMinValue();
					final BigInteger max=((TypeInt)abstractTypes[i]).getMaxValue();
					final List<BigInteger> allowedValues = new ArrayList<BigInteger>();
					while(min.compareTo(max)!=1){
						allowedValues.add(min);
						min=min.add(BigInteger.ONE);
					}
					allValues.add(allowedValues.toArray());
				}
				
			}else if(abstractTypes[i] instanceof TypeFloat){				
				allValues.add(((TypeFloat)abstractTypes[i]).getAllowedValues().toArray());
								
			}else if(abstractTypes[i] instanceof TypeString){
				allValues.add(((TypeString)abstractTypes[i]).getListValues().toArray());				
			}
		}
		return allValues;
	}
	
	/**
	 * Check that all is correct
	 * @param abstractTypes
	 * @throws GenerationException
	 */
	private void checkCombination(IType... abstractTypes) throws GenerationException{
		BigInteger numComb = BigInteger.ONE;
		for (int i = 0; i < abstractTypes.length; i++) {
			if(abstractTypes[i] instanceof TypeInt){
				
				if(!((TypeInt)abstractTypes[i]).getAllowedValues().isEmpty()){
					numComb=numComb.multiply(new BigInteger(""+((TypeInt)abstractTypes[i]).getAllowedValues().size()));
					if(numComb.compareTo(new BigInteger(""+MAXCOMBGENERATED))==1){
						throw new GenerationException("Num of combination is greather than " + MAXCOMBGENERATED);
					}
				}else{
					final BigInteger min=((TypeInt)abstractTypes[i]).getMinValue();
					final BigInteger max=((TypeInt)abstractTypes[i]).getMinValue();
					numComb=numComb.multiply(max.subtract(min));
					if(numComb.compareTo(new BigInteger(""+MAXCOMBGENERATED))==1){
						throw new GenerationException("Num of combination is greather than " + MAXCOMBGENERATED);
					}
				}
				
			}else if(abstractTypes[i] instanceof TypeFloat){
				
				if(((TypeFloat)abstractTypes[i]).getAllowedValues().isEmpty()){
					throw new UnsupportedOperationException("CombStrategy with TypeFloat: Cannot generate without list of values");
				}else {
					numComb=numComb.multiply(new BigInteger(""+((TypeFloat)abstractTypes[i]).getAllowedValues().size()));
					if(numComb.compareTo(new BigInteger(""+MAXCOMBGENERATED))==1){
						throw new GenerationException("Num of combination is greather than " + MAXCOMBGENERATED);
					}
				}				
								
			}else if(abstractTypes[i] instanceof TypeString){
				if(((TypeString)abstractTypes[i]).getListValues().isEmpty()){
					throw new UnsupportedOperationException("CombStrategy with TypeString: Cannot generate without list of values");
				}else {
					numComb=numComb.multiply(new BigInteger(""+((TypeString)abstractTypes[i]).getListValues().size()));
					if(numComb.compareTo(new BigInteger(""+MAXCOMBGENERATED))==1){
						throw new GenerationException("Num of combination is greather than " + MAXCOMBGENERATED);
					}
				}
			}
		}
	}

	@Override
	public Object visit(IType type) throws GenerationException {
		return type.accept(this);
	}

	@Override
	public Object visit(TypeInt typeInt) throws GenerationException {
		if(!typeInt.getAllowedValues().isEmpty()){
			return generatedAllowedValues(typeInt);
		}else{
			final BigInteger max=typeInt.getMaxValue();
			final BigInteger min=typeInt.getMinValue();
			if(max.subtract(min).compareTo(new BigInteger(""+MAXCOMBGENERATED))==1){
				throw new GenerationException("The range [max-min] can´t be greater than 50");
			}
			if(allValuesGenerated.containsKey(typeInt)){
				return new BigInteger(""+randomInteger(0,MAXCOMBGENERATED));
			}else{
				int valueIndex=0;
				if(valuesGenerated.containsKey(typeInt)){
					valueIndex = valuesGenerated.get(typeInt);
				}
				if(valueIndex == MAXCOMBGENERATED){
					allValuesGenerated.put(typeInt, true);
				}
				final BigInteger generated = min.add(new BigInteger(""+valueIndex));
				valuesGenerated.put(typeInt, ++valueIndex);
				return generated;
			}
		}
	}

	private Object generatedAllowedValues(AbstractNumber<?> type)
			throws GenerationException {
		if(type.getAllowedValues().size() > MAXCOMBGENERATED){
			throw new GenerationException("List of values can´t be greater than 50");
		}

		if (allValuesGenerated.containsKey(type)){
			return randomInteger(0,type.getAllowedValues().size()-1);
		}
		else if (valuesGenerated.containsKey(type)){
			int valueIndex = valuesGenerated.get(type);
			if(valueIndex == type.getAllowedValues().size()-1){
				allValuesGenerated.put(type, true);
			}
			Object generated = type.getAllowedValues().get(valueIndex);
			valuesGenerated.put(type, ++valueIndex);
			return generated;
		} else {
			Object generated = type.getAllowedValues().get(0);
			valuesGenerated.put(type, 1);
			return generated;
		}
	}
	
	private String generatedListValues(TypeString type)
			throws GenerationException {
		if(type.getListValues().size() > MAXCOMBGENERATED){
			throw new GenerationException("List of values can´t be greater than 50");
		}
		if(allValuesGenerated.containsKey(type)){
			final int index = randomInteger(0,type.getListValues().size()-1);
			return (String) type.getListValues().get(index);
		}else if(valuesGenerated.containsKey(type)){
			int valueIndex = valuesGenerated.get(type);
			if(valueIndex == type.getListValues().size()-1){
				allValuesGenerated.put(type, true);
				
			}
			Object generated = type.getListValues().get(valueIndex);
			valuesGenerated.put(type, ++valueIndex);
			return (String) generated;
		}else {
			Object generated = type.getListValues().get(0);
			valuesGenerated.put(type, 1);
			return (String) generated;
		}
	}

	@Override
	public Object visit(TypeDate typeDate) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate dates");
	}

	@Override
	public Object visit(TypeDateTime typeDateTime) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate datetime");
	}

	@Override
	public Object visit(TypeDuration typeDuration) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate duration");
	}

	@Override
	public BigInteger visit(TypeFloat typeFloat) throws GenerationException {
		if(!typeFloat.getAllowedValues().isEmpty()){
			return (BigInteger) generatedAllowedValues(typeFloat);
		}else{
			throw new UnsupportedOperationException("Cannot generate without list of values");
		}
	}

	@Override
	public Object visit(TypeList typeList) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate list");
	}

	@Override
	public String visit(TypeString typeString) throws GenerationException {
		if(!typeString.getListValues().isEmpty()){
			return generatedListValues(typeString);
		}else{
			throw new UnsupportedOperationException("Cannot generate without list of values");
		}
	}

	@Override
	public Object visit(TypeTime typeTime) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate times");
	}

	@Override
	public Object visit(TypeTuple typeTuple) throws GenerationException {
		throw new UnsupportedOperationException("Cannot generate tuples");
	}

	/**
     * Generates a random integer over the range [min, max].
     * 
     * @param max
     * @param min
     * @return
     */
    private int randomInteger(int min, int max) {
        return getPRNG().nextInt(max - min + 1) + min;
    }

}
