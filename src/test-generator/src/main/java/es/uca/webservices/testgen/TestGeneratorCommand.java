/*
 *  Copyright 2011-2014 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es),
 *  Antonio García-Domínguez (antonio.garciadominguez@uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * Implements the 'testgenerator' command.
 * 
 */
public class TestGeneratorCommand {
	private static final int DEFAULT_NUM_TESTS = 5;

	private static final int MAX_ARGS_SPEC = 1;
	private static final int MIN_ARGS_SPEC = 1;
	private static final int MAX_ARGS_WSDL = 5;
	private static final int MIN_ARGS_WSDL = 4;

	static final String HELP_OPTION = "help";
    static final String CSV_OPTION = "csv";
    static final String VELOCITY_OPTION = "velocity";
    static final String OUTPUT_OPTION = "output";
    static final String SEED_OPTION = "seed";
    static final String NTESTS_OPTION = "ntests";
    static final String STRATEGY_OPTION = "strategy";

	private static final String USAGE = 
	        "- from a Spec file:\n" +
	        "    testgenerator [options] data.spec\n" +
	        "\n" +
	        "- from a WSDL file:\n" +
	        "    testgenerator [options] file.wsdl service operation (in|out|err)";

	private static final String DESCRIPTION =
            "Generates test cases from ServiceAnalyzer message catalogs or Spec specifications.\n"
            + "When using SA message catalogs, it is necessary to specify the message type,\n"
            + "operation name and service name.";

    private static final Logger LOGGER = LoggerFactory.getLogger(TestGeneratorCommand.class);
    private static Properties properties;

    private List<String> fNonOptionArgs = new ArrayList<String>();
    private boolean fRequestedHelp = false;
	private int minArguments = MIN_ARGS_WSDL;
    private int maxArguments = MAX_ARGS_WSDL;

    private String formatter;
    private String extension;
	private String seed;
	private String strategy = "";
    private int nTests = DEFAULT_NUM_TESTS;
    private OutputStream outputStream;

    /**
     * Main entry point from the command line.
     * @param args Arguments received from the console.
     */
    public static void main(String[] args) throws IOException, ParserException, GenerationException {
    	final TestGeneratorCommand cmd = new TestGeneratorCommand();
		try {
			cmd.parseArgs(args);
			cmd.run();
		} catch (IllegalArgumentException ex) {
			cmd.printHelp(cmd.createOptionParser());
		} catch (Exception ex) {
			LOGGER.error("There was an error while generating test cases", ex);
		}
    }

    public static synchronized String getVersion() throws IOException {
	    if (properties == null) {
	        properties = new Properties();
	        InputStream propStream = TestGeneratorCommand.class.getResourceAsStream("/test-generator.properties");
	        try {
	            properties.load(propStream);
	        } finally {
	            propStream.close();
	        }
	    }
	    return properties.getProperty("testgen.version");
	}

    /**
     * Returns a quick summary of the basic usage of this command.
     */
    public final String getUsage() {
        return USAGE;
    }

    /**
     * Returns a long description of this command.
     */
    public final String getDescription() {
        return DESCRIPTION;
    }

    /**
     * Returns the list of all non-option arguments.
     */
    protected final List<String> getNonOptionArgs() {
        return fNonOptionArgs;
    }

    /**
     * Returns the formatter to be used.
     */
    public String getFormatter() {
		return formatter;
	}

    /**
     * Changes the formatter to be used.
     */
	public void setFormatter(String formatter) {
		this.formatter = formatter;
	}

    /**
     * Returns the number of tests to be run.
     */
	public int getNumTests() {
		return nTests;
	}

    /**
     * Changes the number of tests to be run.
     */
	public void setNumTests(int numRandTest) {
		this.nTests = numRandTest;
	}

    /**
     * Returns the stream to which the generated values will be dumped after formatting.
     */
	public OutputStream getOutputStream() {
		return outputStream;
	}

    /**
     * Changes the stream to which the generated values will be dumped after formatting.
     */
	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

    /**
     * Returns the seed for the PRNG used to generate the values.
     */
	public String getSeed() {
		return seed;
	}

    /**
     * Changes the seed for the PRNG used to generate the values.
     */
	public void setSeed(String seed) {
		this.seed = seed;
	}

	/**
	 * Returns the seed for the PRNG used to generate the values.
	 * @see #setStrategy(String)
	 */
	public String getStrategy() {
		return strategy;
	}

    /**
     * Returns the seed for the PRNG used to generate the values. If
	 * <code>null</code>, a seed will be obtained from a source of entropy
	 * provided by the operating system (/dev/*random in UNIX).
     */
	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}

	/**
     * Returns the option parser. Please do not forget to reuse the OptionParser
     * created by the superclass in your subclasses!
     */
    protected OptionParser createOptionParser() {
        OptionParser parser = new OptionParser();
        parser.accepts(HELP_OPTION, "Provides help on the subcommand");
        parser.accepts(CSV_OPTION, "Produce tests in CSV format (simple but does not support lists or tuples)");
        parser.accepts(VELOCITY_OPTION, "Produce tests in Velocity format (supports everything)");
        parser.accepts(OUTPUT_OPTION, "Sends the output to F (by default, output is written to stdout)").withRequiredArg().ofType(File.class).describedAs("F");
        parser.accepts(SEED_OPTION, "Sets a particular 128-bit string as the seed for the PRNG").withRequiredArg().ofType(String.class).describedAs("S");
        parser.accepts(NTESTS_OPTION, "Generates N test cases").withRequiredArg().ofType(Integer.class).describedAs("N").defaultsTo(DEFAULT_NUM_TESTS);
        parser.accepts(STRATEGY_OPTION, "For Spec files, uses the specified strategy instead of the default one").withRequiredArg().ofType(String.class).describedAs("S");
        return parser;
    }

    /**
	 * Parses the arguments given in the command line. If the wrong options are
	 * used, not enough arguments are given, or the user requests it, it will
	 * print help about the subcommand and its usage. Otherwise, it will run the
	 * subcommand.
	 * 
	 * @throws FileNotFoundException
	 *             Could not find the file to which the results should be
	 *             written.
	 * 
	 * @throws IllegalArgumentException
	 *             Wrong number of non-option arguments, unknown option, or
	 *             wrong value used as argument to some option.
	 */
    public final void parseArgs(String... args) throws FileNotFoundException {
        try {
            final OptionSet options = createOptionParser().parse(args);
            parseOptions(options);
            fNonOptionArgs = options.nonOptionArguments();
            if (!this.fRequestedHelp) {
                if (fNonOptionArgs.size() < 1) {
                    throw new IllegalArgumentException("Wrong number of arguments");
                }

            	// TODO: accept an existing catalog.xml as well
                final String file = fNonOptionArgs.get(0);
                configureByExtension(getFileExtension(file));
                fNonOptionArgs = options.nonOptionArguments();
                validateNonOptionArgs();
            }
        } catch (OptionException ex) {
            throw new IllegalArgumentException(ex.getLocalizedMessage(), ex);
        }
    }

	/**
     * Runs the subcommand. If the user has requested help, it will be printed
     * to stderr. This method cannot be overridden.
     *
     * @throws Exception
     *             There was a problem running the subcommand.
     */
    public final void run() throws IOException, ParserException, GenerationException {
        if (fRequestedHelp) {
            printHelp(createOptionParser());
        } else {
            TestGeneratorRun tgr;
            if (extension.equals("wsdl")) {
                tgr = generateFromWSDL();
            } else if (extension.equals("spec")) {
                tgr = generateFromSpec();
            } else {
            	throw new IllegalArgumentException("Unknown file extension: " + extension);
            }

            if (getSeed() != null) {
            	tgr.setSeed(getSeed());
            }
            tgr.run(getOutputStream());
        }
    }

	/**
     * Show the system help
     * @param parser
     * @throws IOException 
     */
    private void printHelp(OptionParser parser) throws IOException {
    	System.err.println("TestGenerator v" + getVersion() + "\n");
		System.err.println("Usage: ");
		System.err.println(getUsage());
		System.err.println();
		System.err.println("Description:\n");
		System.err.println(getDescription());
		System.err.println();

		parser.printHelpOn(System.err);
    }

    /**
	 * Extracts the extension of a filename and converts it to lower case,
	 * removing surrounding whitespace.
	 */
    private String getFileExtension(final String file) {
		final String[] extensions = file.split("\\.");
		return extensions[extensions.length - 1].toLowerCase().trim();
	}

	/**
     * Configure argument parsing depending on the extension of the source file.
     * @param extension 
     */
    private void configureByExtension(String extension) {
        this.extension = extension;
        if (extension.equals("wsdl")) {
            minArguments = MIN_ARGS_WSDL;
            maxArguments = MAX_ARGS_WSDL;
        } else if (extension.equals("spec")) {
            minArguments = MIN_ARGS_SPEC;
            maxArguments = MAX_ARGS_SPEC;
        } else {
            throw new IllegalArgumentException("Invalid file extension: should be .wsdl or .spec");
        }
    }

    private void parseOptions(OptionSet options) throws FileNotFoundException {
        if (options.has(HELP_OPTION)) {
            this.fRequestedHelp = true;
        }

        if (options.has(CSV_OPTION)) {
        	setFormatter(CSV_OPTION);
        } else {
        	setFormatter(VELOCITY_OPTION);
        }

        if (options.has(OUTPUT_OPTION)) {
        	setOutputStream(new FileOutputStream((File)options.valueOf(OUTPUT_OPTION)));
        } else {
        	setOutputStream(System.out);
        }

        if (options.has(SEED_OPTION)) {
        	setSeed((String)options.valueOf(SEED_OPTION));
        }

        if (options.has(NTESTS_OPTION)) {
        	setNumTests((Integer)options.valueOf(NTESTS_OPTION));
        }

        if (options.has(STRATEGY_OPTION)) {
        	setStrategy((String)options.valueOf(STRATEGY_OPTION));
        }
    }

    private void validateNonOptionArgs() {
        if (getNonOptionArgs().size() < minArguments || getNonOptionArgs().size() > maxArguments) {
            throw new IllegalArgumentException("Wrong number of arguments");
        } 
    }

    private TestGeneratorRun generateFromWSDL() {
        final String path = fNonOptionArgs.get(0);
        final String service = fNonOptionArgs.get(1);
        final String operation = fNonOptionArgs.get(2);
        final String type = fNonOptionArgs.get(3).toUpperCase(Locale.ENGLISH);

        if (!(type.equals("IN") || type.equals("OUT") || type.equals("ERR"))) {
            throw new IllegalArgumentException("Wrong type '" + type + "': expected IN, OUT or ERR");
        }
        // TODO: TestGeneratorRun shouldn't have to use different constructors for WSDL and SPEC - instead, it should accept an IParser/IStrategy/IFormatter triplet and take arguments for each of these
        // TODO: Let the user pick the fault to be generated, instead of generating inputs for all of them
        return new TestGeneratorRun(path, service, operation, type, getFormatter(), getNumTests());
    }

    private TestGeneratorRun generateFromSpec() {
        final String path = fNonOptionArgs.get(0);

        // TODO: TestGeneratorRun shouldn't have to use different constructors for WSDL and SPEC - instead, it should accept an IParser/IStrategy/IFormatter triplet
        return new TestGeneratorRun(path, getFormatter(), getNumTests(), getStrategy());
    }

}
