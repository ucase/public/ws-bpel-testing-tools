package es.uca.webservices.vmutils;

import java.io.IOException;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Counts the number of test cases in one or more .vm files.
 *
 * @author Antonio García Domínguez
 */
public class VMCount {
	private static final Logger LOGGER = LoggerFactory.getLogger(VMCount.class);

	public static void main(String[] args) {
		Velocity.init();
		try {
			new VMCount().run(args);
		} catch (IOException e) {
			LOGGER.error("Error while counting test cases", e);
		}
	}

	public void run(String[] args) throws IOException {
		if (args.length < 1) {
			printUsage();
		}

		for (String vmFile : args) {
			Context ctx = VMUtils.loadDataFile(vmFile);
			System.out.println(VMUtils.countTests(ctx));
		}
	}

	private void printUsage() {
		System.err.println("Usage: vmcount (.vm files...)");
		System.exit(1);
	}
}
