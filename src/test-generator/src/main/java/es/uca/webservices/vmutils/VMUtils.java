package es.uca.webservices.vmutils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

/**
 * Base abstract class with a collection of useful methods for console
 * utilities for Velocity .vm files.
 *
 * @author Antonio García Domínguez
 */
public final class VMUtils {
	
	private VMUtils() {}

	public static Context loadDataFile(File fDataFile) throws IOException {
		Context dataCtx = new VelocityContext();
		final FileReader fR = new FileReader(fDataFile);
		try {
			Velocity.evaluate(dataCtx, new StringWriter(), "vmutils", fR);
			return dataCtx;
		} finally {
			fR.close();
		}
	}

	public static Context loadDataFile(String pathToVM) throws IOException {
		return loadDataFile(new File(pathToVM));
	}

	/**
	 * Find the maximum length of the lists in the variables - this will be used
	 * to tell apart iterated and non-iterated variables. All iterated variables
	 * will have their values set using lists with the maximum length.
	 */
	@SuppressWarnings("rawtypes")
	public static int countTests(Context dataCtx) {
		int nTests = 0;
		for (Object key : dataCtx.getKeys()) {
			final String sKey = (String) key;
			final Object value = dataCtx.get(sKey);
			if (value instanceof List) {
				nTests = Math.max(nTests, ((List) value).size());
			}
		}
		return nTests;
	}

}