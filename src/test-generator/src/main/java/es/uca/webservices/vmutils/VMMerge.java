package es.uca.webservices.vmutils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.testgen.formatters.VelocityFormatter;

/**
 * Command line utility that reads the <code>#set</code> instructions
 * with lists of values from matching variables and produces a new .vm
 * file with the concatenation of their lists.
 *
 * @author Antonio García-Domínguez
 */
public class VMMerge {

	private static final Logger LOGGER = LoggerFactory.getLogger(VMMerge.class);

	public static void main(String[] args) {
		Velocity.init();
		try {
			new VMMerge().run(args);
		} catch (Exception e) {
			LOGGER.error("There was an error while merging the .vm files", e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void run(String[] args) throws IOException {
		if (args.length < 2) {
			printUsage();
		}

		Context mainCtx = VMUtils.loadDataFile(args[0]);
		for (int i = 1; i < args.length; ++i) {
			Context otherCtx = VMUtils.loadDataFile(args[i]);
			merge(args[0], args[i], mainCtx, otherCtx);
		}
		final Map values = new HashMap();
		for (Object o : mainCtx.getKeys()) {
			values.put(o, mainCtx.get((String)o));
		}
		
		VelocityFormatter fmt = new VelocityFormatter();
		fmt.save(System.out, values);
	}
	

	@SuppressWarnings("unchecked")
	private void merge(String pathToMainVM, String pathToAuxVM, Context mainCtx, Context auxCtx) {
		for (Object auxKey : auxCtx.getKeys()) {
			final String sKey = (String)auxKey;
			final Object mValue = mainCtx.get(sKey);
			final Object aValue = auxCtx.get(sKey);

			if (mValue == null) {
				mainCtx.put(sKey, aValue);
			}
			else if (mValue instanceof List && aValue instanceof List) {
				final List<Object> ml = (List<Object>)mValue, ol = (List<Object>)aValue;
				ml.addAll(ol);
			}
			else {
				throw new IllegalArgumentException(String.format(
					"Mismatched types for '%s': '%s' has type '%s' and '%s' has type '%s'",
					sKey,
					pathToMainVM, mValue.getClass().getName(),
					pathToAuxVM, aValue.getClass().getName()
				));
			}
		}
	}

	private void printUsage() {
		System.err.println("Usage: vmmerge (.vm files...)");
		System.exit(1);
	}

}
