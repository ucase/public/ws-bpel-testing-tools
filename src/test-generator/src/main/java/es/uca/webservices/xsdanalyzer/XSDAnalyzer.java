package es.uca.webservices.xsdanalyzer;

import es.uca.webservices.specgen.SpecGenerator;
import es.uca.webservices.testgen.TestGeneratorCommand;
import es.uca.webservices.wsdl.analyzer.SchemasAnalyzer;
import es.uca.webservices.wsdl.analyzer.utils.VelocityBeautifier;
import es.uca.webservices.wsdl.ast.Parser;
import es.uca.webservices.wsdl.visitor.DeclarationsGenerator;
import es.uca.webservices.wsdl.visitor.RootNodeVariableMapper;
import es.uca.webservices.wsdl.visitor.TemplateGenerator;
import org.apache.xmlbeans.SchemaType;

import javax.xml.namespace.QName;
import java.io.File;
import java.io.IOException;

/**
 * Generates Velocity templates and TestSpec declarations for XML Schema elements.
 */
public class XSDAnalyzer {

    public static void main(String[] args) throws Exception {
        new XSDAnalyzer().run(args);
    }

    public void run(String[] args) throws Exception {
        if (args.length != 4) {
            printUsage();
        }

        final String subcmd = args[0];
        final File fXSD = new File(args[1]);
        final QName  elementQName = new QName(args[2], args[3]);
        final SchemasAnalyzer sAnalyzer = new SchemasAnalyzer(fXSD);
        final SchemaType element = sAnalyzer.findGlobalElement(elementQName);
        final Parser parser = new Parser(elementQName, element, Parser.EncodingStyle.DOCUMENT);

        if ("template".equals(subcmd)) {
            System.out.println(generateTemplate(parser));
        }
        else if ("decls".equals(subcmd)) {
            System.out.println(generateDeclarations(parser));
        }
        else {
            printUsage();
        }
    }

    private String generateDeclarations(Parser parser) {
        final DeclarationsGenerator dg = new DeclarationsGenerator(new RootNodeVariableMapper());
        parser.getRoot().accept(dg, null);

        final StringBuffer sbuf = new StringBuffer("");
        final SpecGenerator specGenerator = new SpecGenerator();
        specGenerator.generate(dg.getTypedefArray(), sbuf);
        specGenerator.generate(dg.getVariableArray(), sbuf);

        return sbuf.toString();
    }

    private String generateTemplate(Parser parser) {
        final String rawTemplate = new TemplateGenerator(new RootNodeVariableMapper()).visit(parser.getRoot(), null);
        return new VelocityBeautifier().beautify(rawTemplate);
    }

    private void printUsage() throws IOException {
        System.err.println("XSDAnalyzer v" + TestGeneratorCommand.getVersion());
        System.err.println("Usage: xsd-analyzer (template|decls) (.xsd file) (element namespace URI) (element name)");
        System.exit(1);
    }

}
