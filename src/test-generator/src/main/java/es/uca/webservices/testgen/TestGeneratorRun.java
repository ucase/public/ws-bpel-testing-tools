/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IRandomStrategy;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.parsers.IParser;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.api.utils.GeneratorUtils;
import es.uca.webservices.testgen.formatters.CSVFormatter;
import es.uca.webservices.testgen.formatters.VelocityFormatter;
import es.uca.webservices.testgen.parsers.sanalyzer.SACatalogParser;
import es.uca.webservices.testgen.parsers.spec.xtext.integration.XtextSpecParser;

/**
 * Class in charge of for program execution
 */
public class TestGeneratorRun {

    private String path;
    private String formatter;
    private String service = "", operation = "", type = "";
    private String seed;
    private int numTest;
    private String strategy="";

    /**
     * Constructor for WSDL data
     * @param path
     * @param operation
     * @param service
     * @param type
     * @param formatter
     * @param numTest 
     */
    public TestGeneratorRun(String path, String operation, String service, String type, String formatter, int numTest) {
        this.path = path;
        this.service = service;
        this.operation = operation;
        this.type = type;
        this.numTest = numTest;
        this.formatter = formatter;
        
    }
    
    /**
     * Constructor for SPEC data
     * @param path
     * @param formatter
     * @param numTest 
     */
    public TestGeneratorRun(String path, String formatter, int numTest,String strategy) {
        this.path = path;
        this.numTest = numTest;
        this.formatter = formatter;
        this.strategy = strategy;
    }

    /**
     * Run the application
     * @throws ParserException
     * @throws IOException
     * @throws GenerationException 
     */
    public void run(OutputStream os) throws ParserException, IOException, GenerationException {
        IParser parser;
		if (operation.length() == 0) {
			parser = new XtextSpecParser(path);
		} else {
			parser = new SACatalogParser(path);
		}
		final List<IType> types = parser.parse(operation, service, type);

        IFormatter fmt = null;
		if (formatter.equals("velocity")) {
			fmt = new VelocityFormatter();
		} else { // At this moment, the other possibility is csv
			fmt = new CSVFormatter();
		}

        IStrategy tg = parser.getStrategy(strategy);
        if (tg instanceof IRandomStrategy && seed != null) {        	
        	((IRandomStrategy)tg).setSeed(seed);
        }
        GeneratorUtils.generate(numTest, types, fmt, tg);
        
        fmt.save(os);
    }

    public String getSeed() {
		return seed;
	}

	public void setSeed(String seed) {
		this.seed = seed;
	}

}
