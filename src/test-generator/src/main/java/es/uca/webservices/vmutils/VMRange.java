package es.uca.webservices.vmutils;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.testgen.formatters.VelocityFormatter;

/**
 * Command line utility that selects a subsequence of the test cases in a .vm
 * file.
 * 
 * @author Antonio García-Domínguez
 */
public class VMRange {
	private static final Logger LOGGER = LoggerFactory.getLogger(VMRange.class);
	private static final String SREGEX_RANGE = "([0-9]+)-([0-9]+)";
	private static final Pattern REGEX_RANGE = Pattern.compile(SREGEX_RANGE);

	public static void main(String args[]) {
		Velocity.init();
		try {
			final int retcode = new VMRange().run(args);
			if (retcode != 0) {
				System.exit(retcode);
			}
		} catch (Exception ex) {
			LOGGER.error(
					"There was a problem while selecting a range of the tests",
					ex);
			System.exit(1);
		}
	}

	public int run(String[] args) throws Exception {
		if (args.length < 2) {
			return printUsage();
		}

		final Context dataCtx = VMUtils.loadDataFile(args[0]);
		final BitSet includedTests = computeTestBitSet(args, dataCtx);
		final Map<String, List<Object>> subseq = selectDataWithBitSet(dataCtx, includedTests);

		new VelocityFormatter().save(System.out, subseq);
		return 0;
	}

	@SuppressWarnings("unchecked")
	private Map<String, List<Object>> selectDataWithBitSet(final Context dataCtx, final BitSet includedTests) {
		final Map<String, List<Object>> subseq = new HashMap<String, List<Object>>();
		for (Object oKey : dataCtx.getKeys()) {
			final String sKey = (String)oKey;
			final List<Object> allValues = (List<Object>)dataCtx.get(sKey);

			final ArrayList<Object> subseqValues = new ArrayList<Object>();
			for (int i = includedTests.nextSetBit(0); i >= 0; i = includedTests.nextSetBit(i + 1)) {
				subseqValues.add(allValues.get(i));
			}
			subseq.put(sKey, subseqValues);
		}
		return subseq;
	}

	private BitSet computeTestBitSet(String[] args, final Context dataCtx) {
		// Compute bitset from the ranges passed by the user
		final int nCases = VMUtils.countTests(dataCtx);
		final BitSet includedTests = new BitSet(nCases);
		for (int i = 1; i < args.length; i++) {
			final String arg = args[i];
			Matcher m = REGEX_RANGE.matcher(arg);
			if (!m.matches()) {
				throw new IllegalArgumentException(String.format(
						"Argument '%s' does not match the expected regex '%s'",
						arg, SREGEX_RANGE));
			}

			final int start = Math.max(0, Integer.valueOf(m.group(1)) - 1);
			final int end = Math.min(nCases, Integer.valueOf(m.group(2)));
			includedTests.set(start, end);
		}
		return includedTests;
	}

	private int printUsage() {
		System.err.println(
			"Usage: vmrange (path to .vm) (start-end ranges, starting from 1, both inclusive)...\n" +
			"\n" +
			"Examples:\n" +
			"  - first 10 tests: 'vmrange foo.vm 1-10'\n" +
			"  - second and fourth tests: 'vmrange foo.vm 2-2 4-4"
		);
		return 1;
	}
}
