package es.uca.webservices.vmutils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

/**
 * Invokes a Velocity template multiple times, using the specified Velocity data file.
 * Generates a sequence of files according to the pattern specified by the user or to
 * the {@link #DEFAULT_FILENAME_PATTERN} if none has been provided.
 */
public class VMApply {

    public static final String DEFAULT_FILENAME_PATTERN = "f%02d.xml";

    public static void main(String[] args) throws IOException {
        new VMApply().run(args);
    }

    public void run(String[] args) throws IOException {
        if (args.length < 2 || args.length > 3) {
            printUsage();
        }
        final File fTemplate = new File(args[0]);
        final String sFilenamePattern = args.length > 2 ? args[2] : DEFAULT_FILENAME_PATTERN;

        Velocity.init();
        final Context dataCtx = VMUtils.loadDataFile(args[1]);
        final int nTests = VMUtils.countTests(dataCtx);

        for (int i = 0; i < nTests; ++i) {
            final Context ctx = generateIteratedContext(dataCtx, nTests, i);
            final String sOut = applyTemplate(fTemplate, ctx).trim();
            final File fOut = new File(String.format(sFilenamePattern, i));
            FileUtils.write(fOut, sOut);
        }
    }

	private String applyTemplate(File fTemplate, Context ctx) throws IOException {
		final StringWriter sW = new StringWriter();
		final FileReader reader = new FileReader(fTemplate);
		try {
			Velocity.evaluate(ctx, sW, "applyTemplate", reader);
			return sW.toString();
		} finally {
			reader.close();
		}
	}

	@SuppressWarnings("rawtypes")
	private Context generateIteratedContext(Context dataCtx, int nTests, int currentTest) {
		Context ctx = new VelocityContext();
		for (Object key : dataCtx.getKeys()) {
			final String sKey = (String) key;
			final Object value = dataCtx.get(sKey);
			if (isIteratedVar(nTests, value)) {
				ctx.put(sKey, ((List) value).get(currentTest));
			} else {
				ctx.put(sKey, value);
			}
		}
		return ctx;
	}
	
	@SuppressWarnings("rawtypes")
	private boolean isIteratedVar(int nTests, Object value) {
		return value instanceof List && ((List) value).size() == nTests;
	}

    private void printUsage() {
        System.err.println("vmapply " +
                "(path to template) (path to Velocity data file)" +
                " [filename pattern: '" + DEFAULT_FILENAME_PATTERN + "' by default]");
        System.exit(1);
    }
}
