typedef int (min=1, max=6) CaraDado;
CaraDado dado1,dado2;

strategy DadoTrucado{
	dado1 : Gaussian(3,1)
	dado2 : Uniform()
}

strategy UnDadoTrucado{
	dado2 : Gaussian()
}
