typedef date (min="1990-01-01", max="2012-12-31") DateIncome;
typedef string (pattern="[A-Z][a-z]{0,10}") Nombre;
typedef float (min = 0, max = 5000) TotalCost;
typedef int (min = 0, max = 1000) Quantity;
typedef tuple(element={Nombre,TotalCost,Quantity}) Cost;
typedef list (min=1,max=6,element={Cost}) Costs;
typedef float (min = 0, max = 50000) Total;
typedef tuple (element={Nombre,Total,Costs}) Section;
typedef list (min=1,max=5,element={Section}) Sections;

typedef float (min = 0, max = 30000) TotalRank;

typedef float (min = -30000, max = 6000) Taxation;

typedef int (min = 0, max = 2) Marketing;

typedef string (values={"TV","radio","leaflets"}) AdvertisingMedia;
typedef float (min = 200, max = 5000) AdvertisingCost;
typedef tuple (element={AdvertisingMedia, AdvertisingCost}) AdvertisignTuple;
typedef list (min=1,max=3,element={AdvertisignTuple}) Advertisign;

typedef int (min = 3, max = 4274967295) OffersPercent;
typedef tuple (element={Nombre,OffersPercent}) Offer;
typedef list (min=6,max=6,element={Offer}) Offers;

typedef int (min = -20, max = 6) Fault;

DateIncome fecha;
Sections sectionsEstablishment1;
Sections sectionsEstablishment2;
TotalRank totalRank;
Taxation taxation;
Marketing marketing;
Advertisign advertisign;
Offers offers;

int a,b,c,d;

strategy estrategia{ 
	a, b : EqualValue("Gaussian",0,1)
	c, d : Gaussian(0,1)
}

typedef tuple (element={float, int}) tupla;
typedef list (element={tupla}) listaTupla;

listaTupla l;

strategy listaGaussian{
    l[1][1].size, l[1][2]: Gaussian(3, 1)
    totalRank : Normal()
}