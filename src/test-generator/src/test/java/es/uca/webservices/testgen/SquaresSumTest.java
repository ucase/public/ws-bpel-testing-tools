/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import es.uca.webservices.testgen.api.generators.IType;
import org.junit.Test;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.formatters.PrintIFormatterTest;
import es.uca.webservices.testgen.formatters.VelocityFormatter;
import es.uca.webservices.testgen.parsers.MessageDirection;
import es.uca.webservices.testgen.parsers.sanalyzer.SACatalogParser;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer;

public class SquaresSumTest extends PrintIFormatterTest {

    @Test
    public void readSquaresSum() throws Exception {
        ServiceAnalyzer services = new ServiceAnalyzer(
                "src/test/resources/squaresSum/squaresSum.wsdl");
        ServicesDocument document = services.generateMessageCatalog();
        SACatalogParser lectura = new SACatalogParser(document);
        lectura.parse("squaresSumService", "squaresSumOperation",
                MessageDirection.IN);

        List<IType> expectedTypes = new ArrayList<IType>();
        TypeInt tInt = new TypeInt(0, Long.parseLong("4294967295"));
        expectedTypes.add(tInt);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        lectura.parse("squaresSumService", "squaresSumOperation",
                MessageDirection.OUT);

        expectedTypes.clear();
        TypeList tList = new TypeList(tInt, 1);
        TypeTuple tTuple = new TypeTuple(tInt, tList);
        expectedTypes.add(tTuple);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

    }

    @SuppressWarnings("unchecked")
	@Test
    public void generatorSquaresSum() throws GenerationException {
        final UniformRandomStrategy tg = new UniformRandomStrategy();
        tg.setSeed("0000000000000000");

        TypeInt tInt = new TypeInt(0, Long.parseLong("4294967295"));
        TypeList tList = new TypeList(tInt, 1);
        TypeTuple tTuple = new TypeTuple(tInt, tList);
        for (int i = 0; i < 10; ++i) {
            List<?> test = tg.visit(tTuple);
            BigInteger valueInt = (BigInteger) test.get(0);
            assertTrue(valueInt.compareTo(tInt.getMaxValue()) <= 0 && valueInt.compareTo(tInt.getMinValue()) >= 0);

            List<BigInteger> valueList = (List<BigInteger>) test.get(1);
            assertTrue(valueList.size() >= tList.getMinNumElement() && valueList.size() <= tList.getMaxNumElement());
            for (BigInteger bigInt : (List<BigInteger>) valueList) {
                assertTrue(bigInt.compareTo(tInt.getMaxValue()) <= 0 && bigInt.compareTo(tInt.getMinValue()) >= 0);
            }
        }

        for (int i = 0; i < 10; ++i) {
            BigInteger valueInt = tg.visit(tInt);
            assertTrue(valueInt.compareTo(tInt.getMaxValue()) <= 0 && valueInt.compareTo(tInt.getMinValue()) >= 0);
        }
    }

    @Test
    public void printSquaresSum() {
        final String nameVariable = "squaresSumResponse";
        final IFormatter fmt = new VelocityFormatter();
        List<Object> list = new ArrayList<Object>();
        list.add(1);
        list.add(-48);
        list.add(13);
        List<Object> tuple = new ArrayList<Object>();
        //la tupla tiene como primer elemento un entero y como segundo una lista de entero
        tuple.add(48);
        tuple.add(list);

        fmt.suiteStart();
        fmt.testStart();
        fmt.valueFor(nameVariable, tuple);
        fmt.valueFor(nameVariable, tuple);
        fmt.testEnd();
        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo("#set($squaresSumResponse = [\n  [\n    48,\n    [1, -48, 13]\n  ],\n  [\n    48,\n    [1, -48, 13]\n  ]\n])")));
    }
}
