/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.formatters;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


import org.junit.Test;

import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.formatters.CSVFormatter;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *Prueba para prototipado de la salida
 *
 */
public class CSVFormatterTest extends PrintIFormatterTest {

    @Test
    public void printCSVTest() {
        final String nameVariable = "nameVariable";
        final IFormatter fmt = new CSVFormatter();

        fmt.suiteStart();
        for (int i = 0; i < 4; ++i) {
            fmt.testStart();
            fmt.valueFor(nameVariable, i);
            fmt.testEnd();
        }
        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo("nameVariable\n0\n1\n2\n3")));
    }
    
    @Test
    public void printCSV2VariableTest() {
        final String nameVariable = "nameVariable";
        final IFormatter fmt = new CSVFormatter();

        fmt.suiteStart();
        for (int i = 0; i < 4; ++i) {
            fmt.testStart();
            fmt.valueFor(nameVariable, i);
            fmt.valueFor(nameVariable+"2", 4 + i);
            fmt.testEnd();
        }
        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo("nameVariable, nameVariable2\n0, 4\n1, 5\n2, 6\n3, 7")));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void CSVException() {
        final String nameVariable = "nameVariable";
        final IFormatter fmt = new CSVFormatter();
        final List<String> values = new ArrayList<String>();
        values.add("Unsupported");
        values.add("Unsupported");
        fmt.suiteStart();

        fmt.testStart();
        fmt.valueFor(nameVariable, values);
        fmt.testEnd();

        fmt.suiteEnd();

        saveToString(fmt);
    }
    
    @Test
    public void printDatesTest() throws Exception {
     
        final IFormatter fmt = new CSVFormatter();
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar xmlCalendar = factory.newXMLGregorianCalendar("2011-08-13");
        Duration duration = factory.newDuration("P11Y9M24DT1H");
        fmt.suiteStart();
        
            fmt.testStart();
            fmt.valueFor("duration", duration);
            fmt.valueFor("calendar", xmlCalendar);
            fmt.testEnd();
        
        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo("duration, calendar\n"
                + "\"P11Y9M24DT1H\", \"2011-08-13\"")));
    }
}