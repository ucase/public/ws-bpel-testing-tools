/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uca.webservices.testgen.formatters;

import es.uca.webservices.specgen.SpecGenerator;
import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.parsers.spec.xtext.integration.XtextSpecParser;

import java.io.ByteArrayOutputStream;
import java.io.File;

import org.apache.commons.io.FileUtils;

/**
 *
 * @author migue
 */
public abstract class PrintIFormatterTest {
    protected String saveToString(final IFormatter fmt) {
        ByteArrayOutputStream bOS = new ByteArrayOutputStream();
        fmt.save(bOS);
        String pVelocity = new String(bOS.toByteArray()).trim();
        return pVelocity;
    }

	protected void generatedSpecIsValid(SpecGenerator generator) throws Exception {
		final String spec = generator.generate();
		final File fTmp = File.createTempFile("tmp", ".spec");
		try {
			FileUtils.writeStringToFile(fTmp, spec);
			XtextSpecParser parser = new XtextSpecParser(fTmp.getAbsolutePath());
	    	parser.parse();
		}
		finally {
			fTmp.delete();
		}
	}
}
