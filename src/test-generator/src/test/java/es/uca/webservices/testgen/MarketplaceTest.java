/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.formatters.PrintIFormatterTest;
import es.uca.webservices.testgen.formatters.VelocityFormatter;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Test;

import es.uca.webservices.testgen.parsers.MessageDirection;
import es.uca.webservices.testgen.parsers.sanalyzer.SACatalogParser;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer;
import java.util.ArrayList;
import java.util.List;
import serviceAnalyzer.messageCatalog.ServicesDocument;

/**
 *Pruebas para marketplace.wsdl
 *
 */
public class MarketplaceTest extends PrintIFormatterTest {

    @Test
    public void readMarketplace() throws Exception {
        ServiceAnalyzer services = new ServiceAnalyzer(
                "src/test/resources/MarketPlace/marketplace.wsdl");
        ServicesDocument document = services.generateMessageCatalog();
        SACatalogParser lectura = new SACatalogParser(document);
        lectura.parse("marketplaceBuyer", "submit",
                MessageDirection.IN);

        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(new TypeTuple(new TypeString(), new TypeInt()));
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse("marketplaceBuyer", "submit",
                MessageDirection.OUT);

        expectedTypes.add(new TypeString());
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse("marketplaceSeller", "submit",
                MessageDirection.IN);
        expectedTypes.add(new TypeTuple(new TypeString(), new TypeInt()));
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse("marketplaceSeller", "submit",
                MessageDirection.OUT);

        expectedTypes.add(new TypeString());
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

    }

    @Test
    public void generatorMarketplace() throws GenerationException {
        final UniformRandomStrategy tg = new UniformRandomStrategy();
        tg.setSeed("0000000000000000");

        TypeInt tInt = new TypeInt();
        TypeTuple tTuple = new TypeTuple(new TypeString(), tInt);
        for (int i = 0; i < 10; ++i) {
            List<?> test = tg.visit(tTuple);
            String valueString = (String) test.get(0);
            assertTrue(valueString.matches("\\w*"));

            BigInteger valueInt = (BigInteger) test.get(1);
            assertTrue(valueInt.compareTo(tInt.getMaxValue()) <= 0 && valueInt.compareTo(tInt.getMinValue()) >= 0);

        }
    }

    @Test
    public void printMarketPlace() {
        final String nameVariable = "market";
        final IFormatter fmt = new VelocityFormatter();
        List<Object> list = new ArrayList<Object>();
        list.add("MarketPlace");
        list.add(13);

        fmt.suiteStart();
        fmt.testStart();
        fmt.valueFor(nameVariable, list);
        fmt.valueFor(nameVariable, list);
        fmt.testEnd();
        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo("#set($market = [\n  [\"MarketPlace\", 13],\n  [\"MarketPlace\", 13]\n])")));
    }
}
