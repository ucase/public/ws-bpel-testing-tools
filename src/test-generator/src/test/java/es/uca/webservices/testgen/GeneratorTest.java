/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeDate;
import es.uca.webservices.testgen.api.types.TypeDateTime;
import es.uca.webservices.testgen.api.types.TypeDuration;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTime;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;

/**
 * Test for generator of data.
 */
public class GeneratorTest {

    private final UniformRandomStrategy tg = new UniformRandomStrategy();

    @Before
    public void setup() {
    	// We need a 128-bit seed for the Mersenne Twister
        tg.setSeed("0000000000000000");
    }

    @Test
    public void generatesIntWithinRange() throws Exception {
        final int minValue = 5, maxValue = 10;
        final TypeInt tInt = new TypeInt(minValue, maxValue);
        for (int i = 0; i < maxValue; ++i) {
            final BigInteger value = (BigInteger) tg.visit(tInt);
            int intValue = value.intValue();
            assertTrue("Generated value should be greater or equal to the minimum value", intValue >= minValue);
            assertTrue("Generated value should be less than or equal to the maximum value", intValue <= maxValue);
        }
    }

    @Test
    public void generatesIntWithTotalDigits() throws Exception {
        TypeInt tInt = new TypeInt();
        tInt.setTotalDigits(3);
        for (int i = 0; i < 10; ++i) {
            final BigInteger value = tg.visit(tInt);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compareTo(new BigInteger("100")) >= 0);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compareTo(new BigInteger("999")) <= 0);
        }
    }

    @Test
    public void generatesFloat() throws Exception {
        TypeFloat tFloat = new TypeFloat();
        tFloat.setMinValue(new BigDecimal("20"));
        tFloat.setMaxValue(new BigDecimal("200"));
        for (int i = 0; i < 10; ++i) {
            final BigDecimal value = tg.visit(tFloat);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compareTo(tFloat.getMinValue()) >= 0);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compareTo(tFloat.getMaxValue()) <= 0);
        }
    }
    
    @Test
    public void generatesFloatWithTotalDigits() throws Exception {
        TypeFloat tFloat = new TypeFloat();
        tFloat.setFractionDigits(1);
        tFloat.setTotalDigits(3);
        for (int i = 0; i < 10; ++i) {
            final BigDecimal value = tg.visit(tFloat);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compareTo(new BigDecimal("10")) >= 0);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compareTo(new BigDecimal("99")) <= 0);
        }
    }

    @Test
    public void generatesFloatWith3FractionalDigits() throws Exception {
        TypeFloat tFloat = new TypeFloat();
        tFloat.setFractionDigits(3);
        for (int i = 0; i < 10; ++i) {
            final BigDecimal value = tg.visit(tFloat);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compareTo(tFloat.getMinValue()) >= 0);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compareTo(tFloat.getMaxValue()) <= 0);
        }
    }

    @Test
    public void generateIntInSingletonRange() throws Exception {
        final int minValue = 1;
        final TypeInt tInt = new TypeInt(minValue, minValue);
        final BigInteger value = tg.visit(tInt);
        assertEquals(value.intValue(), minValue);
    }

    @Test
    public void generatesStrings() throws Exception {
        List<String> list = new ArrayList<String>(2);
        list.add("hola");
        list.add("hello");
        final TypeString tString = new TypeString(list);
        for (int i = 0; i < 10; ++i) {
            final String value = (String) tg.visit((IType) tString);
            assertTrue(value.equals("hola") || value.equals("hello"));
        }

        TypeString tString2 = new TypeString();
        for (int i = 0; i < 10; ++i) {
            final String value = (String) tg.visit((IType) tString2);
            assertTrue(value.matches("\\w*"));//comprueba si es una cadena alfanumérica, incluida cadena vacía
        }

        String regex = "[ab]{4,6}c";
        TypeString tString3 = new TypeString(regex);
        for (int i = 0; i < 10; ++i) {
            final String value = (String) tg.visit((IType) tString3);
            assertTrue(value.matches(regex));
        }
    }

    @Test
    public void generatesDate() throws Exception {
        final TypeDate tDate = new TypeDate();
        for (int i = 0; i < 10; ++i) {
            final XMLGregorianCalendar value = tg.visit(tDate);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compare(tDate.getMaxValue()) <= 0);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compare(tDate.getMinValue()) >= 0);
        }
    }

    @Test
    public void generatesDateEqualsYear() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar date1 = factory.newXMLGregorianCalendarDate(2010, 1, 1, DatatypeConstants.FIELD_UNDEFINED);
        XMLGregorianCalendar date2 = factory.newXMLGregorianCalendarDate(2010, 12, 31, DatatypeConstants.FIELD_UNDEFINED);
        TypeDate tDate1 = new TypeDate(date1, date2);
        for (int i = 0; i < 20; ++i) {
            final XMLGregorianCalendar value = tg.visit(tDate1);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compare(tDate1.getMaxValue()) <= 0);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compare(tDate1.getMinValue()) >= 0);
        }

    }

    @Test
    public void generatsDateEqualsMonth() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar date1 = factory.newXMLGregorianCalendarDate(2010, 2, 1, DatatypeConstants.FIELD_UNDEFINED);
        XMLGregorianCalendar date2 = factory.newXMLGregorianCalendarDate(2010, 2, 28, DatatypeConstants.FIELD_UNDEFINED);
        TypeDate tDate = new TypeDate(date1, date2);
        for (int i = 0; i < 20; ++i) {
            final XMLGregorianCalendar value = tg.visit(tDate);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compare(tDate.getMaxValue()) <= 0);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compare(tDate.getMinValue()) >= 0);
        }
    }

    @Test
    public void generatesTime() throws Exception {
        final TypeTime tTime = new TypeTime();
        for (int i = 0; i < 10; ++i) {
            final XMLGregorianCalendar value = tg.visit(tTime);
            assertTrue(value.compare(tTime.getMaxValue()) <= 0);
            assertTrue(value.compare(tTime.getMinValue()) >= 0);
        }
    }

    @Test
    public void generatesTimeEquealHour() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar time1 = factory.newXMLGregorianCalendarTime(8, 0, 13, DatatypeConstants.FIELD_UNDEFINED);
        XMLGregorianCalendar time2 = factory.newXMLGregorianCalendarTime(8, 45, 0, DatatypeConstants.FIELD_UNDEFINED);
        TypeTime tTime = new TypeTime(time1, time2);
        for (int i = 0; i < 20; ++i) {
            final XMLGregorianCalendar value = tg.visit(tTime);
            assertTrue(value.compare(tTime.getMaxValue()) <= 0);
            assertTrue(value.compare(tTime.getMinValue()) >= 0);
        }
    }

    @Test
    public void generatesTimeEquealMinute() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar time1 = factory.newXMLGregorianCalendarTime(8, 0, 0, DatatypeConstants.FIELD_UNDEFINED);
        XMLGregorianCalendar time2 = factory.newXMLGregorianCalendarTime(8, 0, 59, DatatypeConstants.FIELD_UNDEFINED);
        TypeTime tTime = new TypeTime(time1, time2);
        for (int i = 0; i < 20; ++i) {
            final XMLGregorianCalendar value = tg.visit(tTime);
            assertTrue(value.compare(tTime.getMaxValue()) <= 0);
            assertTrue(value.compare(tTime.getMinValue()) >= 0);
        }
    }

    @Test
    public void generatesDateTime() throws Exception {
        final TypeDateTime tDateTime = new TypeDateTime();
        for (int i = 0; i < 10; ++i) {
            final XMLGregorianCalendar value = tg.visit(tDateTime);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compare(tDateTime.getMaxValue()) <= 0);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compare(tDateTime.getMinValue()) >= 0);
        }
    }

    @Test
    public void generatesDateTimeEqualsYear() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar dateTime1 = factory.newXMLGregorianCalendar(2011, 1, 1, 23, 59, 59, 59, DatatypeConstants.FIELD_UNDEFINED);
        XMLGregorianCalendar dateTime2 = factory.newXMLGregorianCalendar(2011, 12, 31, 0, 0, 0, 0, DatatypeConstants.FIELD_UNDEFINED);
        TypeDateTime tDateTime1 = new TypeDateTime(dateTime1, dateTime2);
        for (int i = 0; i < 20; ++i) {
            final XMLGregorianCalendar value = tg.visit(tDateTime1);
            assertTrue(value.compare(tDateTime1.getMaxValue()) <= 0);
            assertTrue(value.compare(tDateTime1.getMinValue()) >= 0);
        }
    }

    @Test
    public void generatesDateTimeEqualsMonth() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar dateTime1 = factory.newXMLGregorianCalendar(2011, 9, 24, 8, 0, 13, 0, DatatypeConstants.FIELD_UNDEFINED);
        XMLGregorianCalendar dateTime2 = factory.newXMLGregorianCalendar(2011, 9, 24, 8, 45, 0, 0, DatatypeConstants.FIELD_UNDEFINED);
        TypeDateTime tDateTime = new TypeDateTime(dateTime1, dateTime2);
        for (int i = 0; i < 20; ++i) {
            final XMLGregorianCalendar value = tg.visit(tDateTime);
            assertTrue(value.compare(tDateTime.getMaxValue()) <= 0);
            assertTrue(value.compare(tDateTime.getMinValue()) >= 0);
        }
    }

    @Test
    public void generatesDateTimeEqualsDay() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar dateTime1 = factory.newXMLGregorianCalendar(2011, 9, 24, 8, 0, 13, 0, DatatypeConstants.FIELD_UNDEFINED);
        XMLGregorianCalendar dateTime2 = factory.newXMLGregorianCalendar(2011, 9, 24, 8, 45, 0, 0, DatatypeConstants.FIELD_UNDEFINED);
        TypeDateTime tDateTime = new TypeDateTime(dateTime1, dateTime2);
        for (int i = 0; i < 20; ++i) {
            final XMLGregorianCalendar value = tg.visit(tDateTime);
            assertTrue(value.compare(tDateTime.getMaxValue()) <= 0);
            assertTrue(value.compare(tDateTime.getMinValue()) >= 0);
        }
    }

    @Test
    public void generatesDuration() throws Exception {
        final TypeDuration tDuration = new TypeDuration();
        for (int i = 0; i < 10; ++i) {
            final Duration value = tg.visit(tDuration);
            assertTrue("Generated value should be less than or equal to the maximum value", value.compare(tDuration.getMaxValue()) <= 0);
            assertTrue("Generated value should be greater or equal to the minimum value", value.compare(tDuration.getMinValue()) >= 0);
        }
    }

    
    @Test
    public void generatesDurationEqualsMonths() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        Duration duration1 = factory.newDuration("P11Y1M1D");
        Duration duration2 = factory.newDuration("P11Y1M31D");
        TypeDuration tDuration1 = new TypeDuration(duration1, duration2);
        for (int i = 0; i < 10; ++i) {
            final Duration value = tg.visit(tDuration1);
            System.out.println(value);
            assertTrue(value.isShorterThan(tDuration1.getMaxValue()) || value.equals(tDuration1.getMaxValue()));
            assertTrue(value.isLongerThan(tDuration1.getMinValue()) || value.equals(tDuration1.getMinValue()));
        }
    }

    @Test
    public void generatesDurationEqualsYear() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        Duration duration1 = factory.newDuration("P11Y1M");
        Duration duration2 = factory.newDuration("P11Y11M");
        TypeDuration tDuration = new TypeDuration(duration1, duration2);
        for (int i = 0; i < 10; ++i) {
            final Duration value = tg.visit(tDuration);
            System.out.println(value);
            assertTrue(value.isShorterThan(tDuration.getMaxValue()) || value.equals(tDuration.getMaxValue()));
            assertTrue(value.isLongerThan(tDuration.getMinValue()) || value.equals(tDuration.getMinValue()));
        }
    }
     
}
