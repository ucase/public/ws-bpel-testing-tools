/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import es.uca.webservices.specgen.SpecGenerationException;
import es.uca.webservices.specgen.SpecGenerator;
import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.formatters.PrintIFormatterTest;
import es.uca.webservices.testgen.formatters.VelocityFormatter;
import es.uca.webservices.testgen.parsers.MessageDirection;
import es.uca.webservices.testgen.parsers.sanalyzer.SACatalogParser;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer;

/**
 * Pruebas para ApprovalService.wsdl
 */
public class ApprovalServiceTest extends PrintIFormatterTest {

	private static final String WSDL = "src/test/resources/LoanApprovalRPC/ApprovalService.wsdl";
    private static final String SERVICE = "ApprovalService";
	private static final String OPERATION = "approve";

	@Test
    public void readApprovalService() throws Exception {
        ServiceAnalyzer services = new ServiceAnalyzer(WSDL);
        ServicesDocument document = services.generateMessageCatalog();
        SACatalogParser lectura = new SACatalogParser(document);
        lectura.parse(SERVICE, OPERATION, MessageDirection.IN);

        List<IType> expectedTypes = new ArrayList<IType>();
        TypeTuple tTuple = new TypeTuple(new TypeString(), new TypeString(), new TypeInt());
        expectedTypes.add(tTuple);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse(SERVICE, OPERATION, MessageDirection.OUT);
        List<String> values = new ArrayList<String>();
        values.add("true");
        values.add("false");
        TypeString tString = new TypeString(values);
        expectedTypes.add(tString);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse(SERVICE, OPERATION, MessageDirection.ERR);
        expectedTypes.add(new TypeInt());
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));
    }

    @Test
    public void generatorApprovalService() throws GenerationException {
        final UniformRandomStrategy tg = new UniformRandomStrategy();
        tg.setSeed("0000000000000000");

        TypeInt tInt = new TypeInt();
        TypeTuple tTuple = new TypeTuple(new TypeString(), new TypeString(), tInt);
        for (int i = 0; i < 10; ++i) {
            List<?> test = tg.visit(tTuple);
            String valueString = (String) test.get(0);
            assertTrue(valueString.matches("\\w*"));

            valueString = (String) test.get(1);
            assertTrue(valueString.matches("\\w*"));

            BigInteger valueInt = (BigInteger) test.get(2);
            assertTrue(valueInt.compareTo(tInt.getMaxValue()) <= 0 && valueInt.compareTo(tInt.getMinValue()) >= 0);

        }

        List<String> list = new ArrayList<String>(2);
        list.add("true");
        list.add("false");
        TypeString tString = new TypeString(list);
        for (int i = 0; i < 10; ++i) {
            String value = tg.visit(tString);
            assertTrue(value.equals("true") || value.equals("false"));
        }
    }

    @Test
    public void printApprovalService() {
        final String nameVariable = OPERATION;
        final IFormatter fmt = new VelocityFormatter();
        List<Object> list = new ArrayList<Object>();
        list.add("hola");
        list.add("hello");
        list.add(13);

        fmt.suiteStart();
        fmt.testStart();
        fmt.valueFor(nameVariable, list);
        fmt.valueFor(nameVariable, list);
        fmt.testEnd();
        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo("#set($approve = [\n  [\"hola\", \"hello\", 13],\n  [\"hola\", \"hello\", 13]\n])")));
    }

    @Test
    public void generateSpecIn() throws Exception {
    	SpecGenerator generator = createGenerator();
    	generator.setDirection(MessageDirection.IN);
    	generatedSpecIsValid(generator);
    }

    @Test
    public void generateSpecOut() throws Exception {
    	SpecGenerator generator = createGenerator();
    	generator.setDirection(MessageDirection.OUT);
    	generatedSpecIsValid(generator);
    }

    @Test(expected=SpecGenerationException.class)
    public void generateSpecErrMissingFaultName() throws Exception {
    	SpecGenerator generator = createGenerator();
    	generator.setDirection(MessageDirection.ERR);
    	generatedSpecIsValid(generator);
    }

    @Test
    public void generateSpecErr() throws Exception {
    	SpecGenerator generator = createGenerator();
    	generator.setDirection(MessageDirection.ERR);
    	generator.setFaultName("loanProcessFault");
    	generatedSpecIsValid(generator);
    }

	private SpecGenerator createGenerator() {
		SpecGenerator generator = new SpecGenerator();
    	generator.setSource(new File(WSDL));
    	generator.setServiceName(SERVICE);
    	generator.setOperationName(OPERATION);
		return generator;
	}
}
