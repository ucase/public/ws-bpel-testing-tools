/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.junit.Test;

import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.vmutils.VMUtils;

public class CommandTest {

	private static final String SEED_ZERO = "0000000000000000";
	private static final String RESOURCE_PREFIX = "src/test/resources/";
	private static final String APPROVAL_WSDL = RESOURCE_PREFIX + "LoanApprovalRPC/ApprovalService.wsdl";
	private static final String INT_SPEC = RESOURCE_PREFIX + "Spec/int.spec";
	private static final String STRATEGY_SPEC = RESOURCE_PREFIX	+ "Spec/strategy.spec";
	private static final String STRATEGYLARGE_SPEC = RESOURCE_PREFIX + "Spec/strategyLarge.spec";

	@Test(expected = IllegalArgumentException.class)
	public void errorWithNumArguments() throws Exception {
		runTG();
	}

	@Test(expected = IllegalArgumentException.class)
	public void errorWithNumArgumentsSpec() throws Exception {
		runTG(INT_SPEC + " notvalid notvalid");
	}

	@Test(expected = ParserException.class)
	public void errorArgumentsFileDoesNotExist() throws Exception {
		runTG("./noFile.wsdl", "Service", "Operation", "IN");
	}

	@Test(expected = ParserException.class)
	public void errorArgumentsServiceDoesNotExist() throws Exception {
		runTG(APPROVAL_WSDL, "Service", "approve", "IN");
	}

	@Test(expected = ParserException.class)
	public void errorArgumentsOperationDoesNotExist() throws Exception {
		runTG(APPROVAL_WSDL, "ApprovalService", "Operation", "IN");
	}

	@Test(expected = IllegalArgumentException.class)
	public void errorOptionDoesNotExist() throws Exception {
		runTG("-NoExist");
	}

	@Test
	public void optionHelp() throws Exception {
		runTG("--" + TestGeneratorCommand.HELP_OPTION);
	}

	@Test
	public void xmlNoException() throws Exception {
		runTG(
			"--" + TestGeneratorCommand.SEED_OPTION, SEED_ZERO,
			APPROVAL_WSDL, "ApprovalService", "approve", "IN");
	}

	@Test
	public void specNoException() throws Exception {
		runTG("--" + TestGeneratorCommand.SEED_OPTION, SEED_ZERO, INT_SPEC);
	}

	@Test
	public void specNoExceptionCSV() throws Exception {
		runTG(
			"--" + TestGeneratorCommand.SEED_OPTION, SEED_ZERO,
			"--" + TestGeneratorCommand.CSV_OPTION,
			INT_SPEC);
	}

	@Test
	public void specTwiceSameSeedProducesSameTests() throws Exception {
		final File f1 = generateTests(SEED_ZERO);
		final File f2 = generateTests(SEED_ZERO);
		assertEquals(
			"Multiple executions with the same seed should produce different results",
			FileUtils.checksumCRC32(f1),
			FileUtils.checksumCRC32(f2));
	}

	@Test
	public void specTwiceDiffSeedProducesDiffTests() throws Exception {
		final File f1 = generateTests(SEED_ZERO);
		final File f2 = generateTests("1111222233334444");
		assertTrue(
			"Multiple executions with different seeds should produce different results",
			FileUtils.checksumCRC32(f1) != FileUtils.checksumCRC32(f2));
	}
	
	@Test
	public void specStrategyNoException() throws Exception {
		final File f1 = File.createTempFile("testgen", ".csv");
		runTG(
			"--" + TestGeneratorCommand.OUTPUT_OPTION, f1.getCanonicalPath(),
			"--" + TestGeneratorCommand.SEED_OPTION, SEED_ZERO,
			"--" + TestGeneratorCommand.NTESTS_OPTION, "5",
			"--" + TestGeneratorCommand.CSV_OPTION,
			"--" + TestGeneratorCommand.STRATEGY_OPTION, "DadoTrucado",
			STRATEGY_SPEC);

		// Open the file and make sure it has 6 lines (header + 5 tests), with commas in them
		BufferedReader bR = null;
		try {
			bR = new BufferedReader(new FileReader(f1));

			int nLines = 0;
			String line;
			while ((line = bR.readLine()) != null) {
				if (!"".equals(line)) {
					assertTrue("The output produced with CSV formatting should be a CSV file", line.contains(","));
					++nLines;
				}
			}
			assertEquals("There should be 1 line for the header and 5 lines for each test case", 6, nLines);
		} finally {
			if (bR != null) {
				bR.close();
			}
		}
	}

	@Test
	public void specStrategyLargeNoException() throws Exception {
		File f1 = File.createTempFile("testgen", ".vm");
		runTG(
			"--" + TestGeneratorCommand.OUTPUT_OPTION, f1.getCanonicalPath(),
			"--" + TestGeneratorCommand.SEED_OPTION, SEED_ZERO,
			"--" + TestGeneratorCommand.NTESTS_OPTION, "2",
			"--" + TestGeneratorCommand.VELOCITY_OPTION,
			"--" + TestGeneratorCommand.STRATEGY_OPTION, "estrategia",
			STRATEGYLARGE_SPEC);

		// Try to load the file as a data file
		Velocity.init();
		VMUtils.loadDataFile(f1);
	}

	@Test
	public void generatedVMIsParsable() throws Exception {
		final File f1 = generateTests(SEED_ZERO);
		Velocity.init();
		VMUtils.loadDataFile(f1);
	}

	@Test
	public void numberOfTestsIsHonored() throws Exception {
		final File f1 = File.createTempFile("testgen", ".vm");
		runTG(
			"--" + TestGeneratorCommand.OUTPUT_OPTION, f1.getCanonicalPath(),
			"--" + TestGeneratorCommand.SEED_OPTION, SEED_ZERO,
			"--" + TestGeneratorCommand.NTESTS_OPTION, "11",
			INT_SPEC);

		Velocity.init();
		final Context context = VMUtils.loadDataFile(f1);
		final List<?> dados = (List<?>) context.get("dado");
		assertEquals(11, dados.size());
	}

	private File generateTests(String seed) throws Exception {
		final File fTemp = File.createTempFile("testgen", ".vm");
		fTemp.deleteOnExit();
		runTG(
			"--" + TestGeneratorCommand.OUTPUT_OPTION, fTemp.getCanonicalPath(),
			"--" + TestGeneratorCommand.SEED_OPTION, seed,
			INT_SPEC);

		return fTemp;
	}

	private TestGeneratorCommand runTG(String... args) throws Exception {
		TestGeneratorCommand tgc = new TestGeneratorCommand();
		tgc.parseArgs(args);
		tgc.run();

		return tgc;
	}
}
