/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import es.uca.webservices.specgen.SpecGenerator;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.formatters.PrintIFormatterTest;
import es.uca.webservices.testgen.parsers.MessageDirection;
import es.uca.webservices.testgen.parsers.sanalyzer.SACatalogParser;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer;

/**
 *Pruebas para marketplace.wsdl
 *
 */
public class CIRBETest extends PrintIFormatterTest {

	private static final String LOAN_ID_PATTERN = "[0-9A-Z]{1,3}(\\.[A-Z]{3}(\\.X){0,1}){0,1}";
	private static final String WSDL = "src/test/resources/LoanApprovalExtended/CIRBE.wsdl";
	private static final String SERVICE = "obligacionesCIRBEwsdlService";
    private static final String OPERATION = "ConsultaYalmacenaCIRBE";

	@Test
    public void readMarketplace() throws Exception {
        ServiceAnalyzer services = new ServiceAnalyzer(WSDL);
        ServicesDocument document = services.generateMessageCatalog();
        SACatalogParser lectura = new SACatalogParser(document);
        lectura.parse(SERVICE, OPERATION, MessageDirection.IN);

        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(new TypeTuple(new TypeString(), new TypeFloat(), new TypeString(LOAN_ID_PATTERN)));
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse(SERVICE, OPERATION, MessageDirection.OUT);
        List<String> values = new ArrayList<String>();
        values.add("Hipoteca");
        values.add("TarjetaCredito");
        values.add("Prestamo");
        values.add("Ninguno");
        TypeString tString = new TypeString(values);
        TypeTuple tTuple = new TypeTuple(new TypeFloat(), tString);
        TypeList tList = new TypeList(tTuple, 1);
        expectedTypes.add(tList);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));
    }

    @Test
    public void generatorASNEF() throws GenerationException {
        final UniformRandomStrategy tg = new UniformRandomStrategy();
        tg.setSeed("0000000000000000");

        List<String> values = new ArrayList<String>();
        values.add("Hipoteca");
        values.add("TarjetaCredito");
        values.add("Prestamo");
        values.add("Ninguno");
        TypeString tString = new TypeString(values);
        TypeFloat tFloat = new TypeFloat();
        TypeTuple tTuple = new TypeTuple(tFloat, tString);
        TypeList tList = new TypeList(tTuple, 1);
        for (int i = 0; i < 10; ++i) {
            List<?> value = tg.visit(tList);
            assertTrue(value.size() > 0);
            for (Object o : value) {
                List<?> l = (List<?>) o;
                String s = (String) l.get(1);
                assertTrue(values.contains(s));
            }
        }
    }

    @Test
    public void inputGeneratesValidSpec() throws Exception {
    	SpecGenerator generator = createGenerator();
    	generator.setDirection(MessageDirection.IN);
    	generatedSpecIsValid(generator);
    }

    @Test
    public void outputGeneratesValidSpec() throws Exception {
    	SpecGenerator generator = createGenerator();
    	generator.setDirection(MessageDirection.OUT);
    	generatedSpecIsValid(generator);
    }

	private SpecGenerator createGenerator() {
		SpecGenerator generator = new SpecGenerator();
    	generator.setSource(new File(WSDL));
    	generator.setServiceName(SERVICE);
    	generator.setOperationName(OPERATION);
		return generator;
	}

}
