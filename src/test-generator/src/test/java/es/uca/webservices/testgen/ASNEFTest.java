/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import es.uca.webservices.specgen.SpecGenerator;
import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.formatters.PrintIFormatterTest;
import es.uca.webservices.testgen.formatters.VelocityFormatter;
import es.uca.webservices.testgen.parsers.MessageDirection;
import es.uca.webservices.testgen.parsers.sanalyzer.SACatalogParser;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer;

/**
 * Pruebas para marketplace.wsdl
 */
public class ASNEFTest extends PrintIFormatterTest {

	private static final String WSDL = "src/test/resources/LoanApprovalExtended/ASNEF.wsdl";
	private static final String SERVICE = "morosidadASNEF_WSDLService";
    private static final String OPERATION = "ASNEF_Operation";

	@Test
    public void readASNEF() throws Exception {
        ServiceAnalyzer services = new ServiceAnalyzer(WSDL);
        ServicesDocument document = services.generateMessageCatalog();
        SACatalogParser lectura = new SACatalogParser(document);
        lectura.parse(SERVICE, OPERATION, MessageDirection.IN);

        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(new TypeString());
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse(SERVICE, OPERATION, MessageDirection.OUT);
        List<String> values = new ArrayList<String>();
        values.add("true");
        values.add("false");
        TypeString tString = new TypeString(values);
        expectedTypes.add(tString);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));
    }

    @Test
    public void generatorASNEF() {
        final UniformRandomStrategy tg = new UniformRandomStrategy();
        tg.setSeed("0000000000000000");

        List<String> list = new ArrayList<String>(2);
        list.add("true");
        list.add("false");
        TypeString tString = new TypeString(list);
        for (int i = 0; i < 10; ++i) {
            String value = tg.visit(tString);
            assertTrue(value.equals("true") || value.equals("false"));
        }
        
        for (int i = 0; i < 10; ++i) {
            String test = tg.visit(new TypeString());
            assertTrue(test.matches("\\w*"));
        }

    }

    @Test
    public void printASNEFT() {
        final String nameVariable = "ASNEF_OperationResponse";
        final IFormatter fmt = new VelocityFormatter();
        List<Object> list = new ArrayList<Object>();
        list.add("true");
        list.add("false");

        fmt.suiteStart();
        fmt.testStart();
        fmt.valueFor(nameVariable, list);
        fmt.valueFor(nameVariable, list);
        fmt.testEnd();
        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo("#set($ASNEF_OperationResponse = [\n  [\"true\", \"false\"],\n  [\"true\", \"false\"]\n])")));
    }

    @Test
    public void generateSpecIn() throws Exception {
    	SpecGenerator gen = createGenerator();
    	gen.setDirection(MessageDirection.IN);
    	generatedSpecIsValid(gen);
    }

	@Test
    public void generateSpecOut() throws Exception {
    	SpecGenerator gen = createGenerator();
    	gen.setDirection(MessageDirection.OUT);
    	generatedSpecIsValid(gen);
    }

	private SpecGenerator createGenerator() {
		SpecGenerator gen = new SpecGenerator();
		gen.setSource(new File(WSDL));
		gen.setServiceName(SERVICE);
		gen.setOperationName(OPERATION);
		return gen;
	}
}
