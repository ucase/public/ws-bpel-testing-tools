/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import es.uca.webservices.specgen.SpecGenerator;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.formatters.PrintIFormatterTest;
import es.uca.webservices.testgen.parsers.MessageDirection;
import es.uca.webservices.testgen.parsers.sanalyzer.SACatalogParser;
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer;

/**
 *Pruebas para MetaSearch.wsdl
 *
 */
public class MetaSearchTest extends PrintIFormatterTest {

	private static final String WSDL = "src/test/resources/MetaSearch/MetaSearch.wsdl";
	private static final String META_OPERATION = "process";
	private static final String META_SERVICE = "MetaSearch";
	private static final String MSN_OPERATION = "Search";
	private static final String MSN_SERVICE = "MSNSearchService";
	private static final String GOO_OPERATION = "doGoogleSearch";
	private static final String GOO_SERVICE = "GoogleSearchService";

	@Test
    public void readMetaSearch() throws Exception {
		// TODO: split this test into many smaller ones and create the IType instances by properly parsing them
		// TODO: look at the other tests to see if we have the same problem
        ServiceAnalyzer services = new ServiceAnalyzer(WSDL);
        ServicesDocument document = services.generateMessageCatalog();
        SACatalogParser lectura = new SACatalogParser(document);
        lectura.parse(GOO_SERVICE, GOO_OPERATION, MessageDirection.IN);

        List<IType> expectedTypes = new ArrayList<IType>();
        TypeString tString = new TypeString();
        TypeInt tInt = new TypeInt();
        TypeTuple tTuple = new TypeTuple(tString, tString, tString, tString, tInt, tInt);
        expectedTypes.add(tTuple);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse(GOO_SERVICE, GOO_OPERATION, MessageDirection.OUT);

        TypeTuple tTuple1 = new TypeTuple(tString, tString, tString);
        TypeList tList = new TypeList(tTuple1, 0);
        expectedTypes.add(tList);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse(META_SERVICE, META_OPERATION, MessageDirection.IN);

        TypeTuple tTuple2 = new TypeTuple(tString, tString, tString, tInt);
        expectedTypes.add(tTuple2);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse(META_SERVICE, META_OPERATION, MessageDirection.OUT);

        TypeTuple tTuple3 = new TypeTuple(tString, tString, tString, tString);
        TypeList tList2 = new TypeList(tTuple3, 0);
        TypeTuple tTuple4 = new TypeTuple(tInt, tInt, tInt, tList2);
        expectedTypes.add(tTuple4);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse(META_SERVICE, META_OPERATION, MessageDirection.ERR);

        TypeTuple tTupleErr = new TypeTuple(tString, tString);
        expectedTypes.add(tTupleErr);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse(MSN_SERVICE, MSN_OPERATION, MessageDirection.IN);
        List<String> valuesT1 = new ArrayList<String>();
        valuesT1.add("Moderate");
        valuesT1.add("Strict");
        valuesT1.add("Off");
        TypeString tStringT1 = new TypeString(valuesT1);
        List<String> valuesT2 = new ArrayList<String>();
        valuesT2.add("None");
        valuesT2.add("MarkQueryWords");
        valuesT2.add("DisableSpellCorrectForSpecialWords");
        valuesT2.add("DisableHostCollapsing");
        TypeString tStringT2 = new TypeString(valuesT2);
        TypeList tListT3 = new TypeList(tStringT2);
        TypeFloat tFloat = new TypeFloat();
        TypeList tListT4 = new TypeList(tFloat, 0, 1);
        TypeTuple tTupleT5 = new TypeTuple(tFloat, tFloat, tListT4);
        TypeList tListT6 = new TypeList(tTupleT5, 0, 1);
        List<String> valuesT7 = new ArrayList<String>();
        valuesT7.add("Web");
        valuesT7.add("News");
        valuesT7.add("Ads");
        valuesT7.add("InlineAnswers");
        valuesT7.add("PhoneBook");
        valuesT7.add("WordBreaker");
        valuesT7.add("Spelling");
        TypeString tStringT7 = new TypeString(valuesT7);
        List<String> valuesT8 = new ArrayList<String>();
        valuesT8.add("All");
        valuesT8.add("Title");
        valuesT8.add("Description");
        valuesT8.add("Url");
        valuesT8.add("DisplayUrl");
        valuesT8.add("CacheUrl");
        valuesT8.add("Source");
        valuesT8.add("SearchTags");
        valuesT8.add("Phone");
        valuesT8.add("DateTime");
        valuesT8.add("Address");
        valuesT8.add("Location");
        valuesT8.add("SearchTagsArray");
        valuesT8.add("Summary");
        TypeString tStringT8 = new TypeString(valuesT8);
        TypeList tListT9 = new TypeList(tStringT8);
        TypeTuple tTupleT10 = new TypeTuple(tStringT7, tInt, tInt, tListT9);
        TypeList tListT11 = new TypeList(tTupleT10, 0);
        TypeTuple tTupleT12 = new TypeTuple(tString, tString, tString, tStringT1, tListT3, tListT6, tListT11);
        expectedTypes.add(tTupleT12);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse(MSN_SERVICE, MSN_OPERATION,
                MessageDirection.OUT);
        TypeString t1 = tStringT7;
        TypeList t2 = new TypeList(tString, 0, 1);
        TypeTuple t3 = new TypeTuple(tInt, tInt, tInt, tInt, tInt, tInt);
        TypeList t4 = new TypeList(t3, 0, 1);
        TypeTuple t5 = new TypeTuple(t2, t2, t2, t2, t2, t2, t2);
        TypeList t6 = new TypeList(t5, 0, 1);
        TypeList t7 = new TypeList(tFloat, 0, 1);
        TypeTuple t8 = new TypeTuple(tFloat, tFloat, t7);
        TypeList t9 = new TypeList(t8, 0, 1);
        TypeTuple t10 = new TypeTuple(tString, tString);
        TypeList t11 = new TypeList(t10, 0);
        TypeList t12 = new TypeList(t11, 0, 1);
        TypeTuple t13 = new TypeTuple(t2, t2, t2, t2, t2, t2, t2, t2, t4, t6, t9, t12, t2);
        TypeList t14 = new TypeList(t13, 0);
        TypeTuple t15 = new TypeTuple(t1, tInt, tInt, t14);
        TypeList t16 = new TypeList(t15, 0);
        expectedTypes.add(t16);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));
    }

	@Test
	public void msnInputGeneratesValidSpec() throws Exception {
		SpecGenerator generator = createMSNGenerator();
		generator.setDirection(MessageDirection.IN);
		generatedSpecIsValid(generator);
	}

	@Test
	public void msnOutputGeneratesValidSpec() throws Exception {
		SpecGenerator generator = createMSNGenerator();
		generator.setDirection(MessageDirection.OUT);
		generatedSpecIsValid(generator);
	}

	private SpecGenerator createMSNGenerator() {
		SpecGenerator generator = new SpecGenerator();
		generator.setSource(new File(WSDL));
		generator.setServiceName(MSN_SERVICE);
		generator.setOperationName(MSN_OPERATION);
		return generator;
	}
}
