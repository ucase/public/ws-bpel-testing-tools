package es.uca.webservices.testgen.formatters;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.formatters.VelocityFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VelocityFormatterTest extends PrintIFormatterTest {

    @Test
    public void printVelocityTest() {
        final String nameVariable = "nameVariable";
        final IFormatter fmt = new VelocityFormatter();

        fmt.suiteStart();
        for (int i = 0; i < 4; ++i) {
            fmt.testStart();
            fmt.valueFor(nameVariable, i);
            fmt.testEnd();
        }
        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo("#set($nameVariable = [0, 1, 2, 3])")));
    }

    @Test
    public void printDatesTest() throws Exception {
     
        final IFormatter fmt = new VelocityFormatter();
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar xmlCalendar = factory.newXMLGregorianCalendar("2011-08-13");
        Duration duration = factory.newDuration("P11Y9M24DT1H");
        fmt.suiteStart();
        
            fmt.testStart();
            fmt.valueFor("duration", duration);
            fmt.valueFor("calendar", xmlCalendar);
            fmt.testEnd();
        
        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo("#set($duration = [\"P11Y9M24DT1H\"])\n"
                                + "#set($calendar = [\"2011-08-13\"])")));
    }

    @Test
    public void printTwoLevelList() throws Exception {
        final IFormatter fmt = new VelocityFormatter();

        fmt.suiteStart();

        fmt.testStart();
        fmt.valueFor("lMain", new ArrayList<Integer>());
        fmt.testEnd();

        fmt.testStart();
        final List<List<Integer>> lSecond = new ArrayList<List<Integer>>();
        lSecond.add(Arrays.asList(2, 3, 4));
        lSecond.add(Arrays.asList(5, 6, 7));
        fmt.valueFor("lMain", lSecond);
        fmt.testEnd();

        fmt.testStart();
        final List<List<Integer>> lThird = new ArrayList<List<Integer>>();
        lThird.add(Arrays.<Integer>asList());
        lThird.add(Arrays.asList(1, 8, 9));
        fmt.valueFor("lMain", lThird);
        fmt.testEnd();

        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo(
                "#set($lMain = [\n  [],\n  [\n    [2, 3, 4],\n    [5, 6, 7]\n  ],\n  [\n    [],\n    [1, 8, 9]\n  ]\n])")));
    }

    @Test
    public void printMixedListWithNestedLists() throws Exception {
        final IFormatter fmt = new VelocityFormatter();

        fmt.suiteStart();
        fmt.testStart();
        fmt.valueFor("l", Arrays.asList("Foo", Arrays.asList(1, 2, 3)));
        fmt.testEnd();
        fmt.testStart();
        fmt.valueFor("l", Arrays.asList("Bar", Arrays.asList(4, 5, 6)));
        fmt.testEnd();
        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo("#set($l = [\n  [\n    \"Foo\",\n    [1, 2, 3]\n  ],\n  [\n    \"Bar\",\n    [4, 5, 6]\n  ]\n])")));
    }
}
