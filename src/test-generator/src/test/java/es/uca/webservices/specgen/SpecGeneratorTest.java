package es.uca.webservices.specgen;

import java.io.File;
import java.math.BigInteger;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import serviceAnalyzer.messageCatalog.TypeDecls;
import serviceAnalyzer.messageCatalog.TypeGA;
import serviceAnalyzer.messageCatalog.TypeTypedef;
import es.uca.webservices.testgen.parsers.spec.xtext.integration.XtextSpecParser;

/**
 * Unit tests for the parts of {@link SpecGenerator} that are hard to cover
 * with the regular examples.
 *
 * @author  Antonio Garcia-Dominguez
 */
public class SpecGeneratorTest {

    @Test
    public void testFloat() throws Exception {
        final TypeDecls decls = TypeDecls.Factory.newInstance();
        final TypeTypedef td = decls.addNewTypedef();
        td.setName("TExample");
        td.setType(TypeGA.FLOAT);
        td.setFractionDigits(BigInteger.valueOf(2));
        td.setTotalDigits(BigInteger.valueOf(4));

        assertValidSpecIsGenerated(decls);
    }

    @Test
    public void testBinary() throws Exception {
        final TypeDecls decls = TypeDecls.Factory.newInstance();
        final TypeTypedef td = decls.addNewTypedef();
        td.setName("TYesNo");
        td.setType(TypeGA.INT);
        td.setValues("0,1");

        assertValidSpecIsGenerated(decls);
    }

    @Test
    public void testNoRestrictions() throws Exception {
        final TypeDecls decls = TypeDecls.Factory.newInstance();
        final TypeTypedef td = decls.addNewTypedef();
        td.setName("TAliasForString");
        td.setType(TypeGA.STRING);

        assertValidSpecIsGenerated(decls);
    }

    private void assertValidSpecIsGenerated(TypeDecls decls) throws Exception {
        final SpecGenerator gen = new SpecGenerator();
        final String spec = gen.generate(decls);
		final File fTmp = File.createTempFile("tmp", ".spec");
		try {
			FileUtils.writeStringToFile(fTmp, spec);
			XtextSpecParser parser = new XtextSpecParser(fTmp.getAbsolutePath());
	    	parser.parse();
		}
		finally {
			fTmp.delete();
		}
    }
}
