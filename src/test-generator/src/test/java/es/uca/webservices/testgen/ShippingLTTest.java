/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import java.util.ArrayList;
import java.util.List;

import es.uca.webservices.testgen.api.generators.IType;
import org.junit.Test;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.formatters.PrintIFormatterTest;
import es.uca.webservices.testgen.parsers.MessageDirection;
import es.uca.webservices.testgen.parsers.sanalyzer.SACatalogParser;
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer;

/**
 *Pruebas para marketplace.wsdl
 *
 */
public class ShippingLTTest extends PrintIFormatterTest {

        @Test
    public void readMarketplace() throws Exception {
        ServiceAnalyzer services = new ServiceAnalyzer(
                "src/test/resources/ShippingServiceAsynchronous/shippingLT.wsdl");
        ServicesDocument document = services.generateMessageCatalog();
        services.printMessageCatalog();
        SACatalogParser lectura = new SACatalogParser(document);
        lectura.parse("shippingService", "shippingRequest",
                MessageDirection.IN);

        TypeInt tInt = new TypeInt(1,50);
        List<String> values = new ArrayList<String>();
        values.add("true");
        values.add("false");
        TypeString tString = new TypeString(values);
        TypeTuple tTuple = new TypeTuple(new TypeInt(), tInt, tString);
        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(tTuple);


    }
}
