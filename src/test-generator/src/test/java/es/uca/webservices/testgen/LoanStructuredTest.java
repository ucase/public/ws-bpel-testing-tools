/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen;

import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.formatters.PrintIFormatterTest;
import es.uca.webservices.testgen.formatters.VelocityFormatter;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


import org.junit.Test;

import es.uca.webservices.testgen.parsers.MessageDirection;
import es.uca.webservices.testgen.parsers.sanalyzer.SACatalogParser;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import serviceAnalyzer.messageCatalog.ServicesDocument;

public class LoanStructuredTest extends PrintIFormatterTest {

    @Test
    public void readLoanStructured() throws Exception {

        ServiceAnalyzer services = new ServiceAnalyzer(
                "src/test/resources/loanStructured/ServicioPrestamos.wsdl");
        ServicesDocument document = services.generateMessageCatalog();
        SACatalogParser lectura = new SACatalogParser(document);
        lectura.parse("ServicioPrestamosService", "concederCredito",
                MessageDirection.IN);

        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(new TypeFloat());
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

        expectedTypes.clear();
        lectura.parse("ServicioPrestamosService", "concederCredito",
                MessageDirection.OUT);
        List<String> values = new ArrayList<String>();
        values.add("true");
        values.add("false");
        TypeString tString = new TypeString(values);
        expectedTypes.add(tString);
        assertThat(lectura.getTypes(), is(equalTo(expectedTypes)));

    }

    @Test
    public void generatorAprovalService() throws Exception {
        final UniformRandomStrategy tg = new UniformRandomStrategy();
        tg.setSeed("0000000000000000");

        TypeFloat tFloat = new TypeFloat();
        for (int i = 0; i < 10; ++i) {
            final BigDecimal value = tg.visit(tFloat);
            assertTrue(value.compareTo(tFloat.getMaxValue()) <= 0 && value.compareTo(tFloat.getMinValue()) >= 0);

        }

        List<String> list = new ArrayList<String>(2);
        list.add("true");
        list.add("false");
        TypeString tString = new TypeString(list);
        for (int i = 0; i < 10; ++i) {
            String value = tg.visit(tString);
            assertTrue(value.equals("true") || value.equals("false"));
        }
    }

    @Test
    public void printVelocityTest() {
        final String nameVariable = "ApprovalRequest";
        final IFormatter fmt = new VelocityFormatter();

        fmt.suiteStart();

        fmt.testStart();
        fmt.valueFor(nameVariable, (float) 0.1);
        fmt.valueFor(nameVariable, (float) 1.223);
        fmt.valueFor(nameVariable, (float) 2.3341);
        fmt.valueFor(nameVariable, (float) -13.54763);
        fmt.testEnd();

        fmt.suiteEnd();

        String pVelocity = saveToString(fmt);
        assertThat(pVelocity, is(equalTo("#set($" + nameVariable + " = [0.1, 1.223, 2.3341, -13.54763])")));
    }
}
