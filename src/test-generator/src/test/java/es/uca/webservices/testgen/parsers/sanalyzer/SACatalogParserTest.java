/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.parsers.sanalyzer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.parsers.MessageDirection;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer;

/**
 * Pruebas para procesado del XML
 */
public class SACatalogParserTest {

    @Test(expected = ParserException.class)
    public void FileNoExist() throws Exception {
        new SACatalogParser("./fileNoExist.xml");
    }

    @Test
    public void readTacService() throws Exception {
        ServiceAnalyzer services = new ServiceAnalyzer(
                "src/test/resources/TacService/tacService.wsdl");
        ServicesDocument document = services.generateMessageCatalog();

        List<IType> type = new ArrayList<IType>();
        TypeString tString = new TypeString();
        TypeList tList = new TypeList(tString, 0);
        type.add(tList);

        SACatalogParser lectura = new SACatalogParser(document);
        lectura.parse("tacService", "reverseLines",
                MessageDirection.OUT);
        assertThat(lectura.getTypes(), is(equalTo(type)));

        lectura.parse("tacService", "reverseLines",
                MessageDirection.IN);

        assertThat(lectura.getTypes(), is(equalTo(type)));
    }

    @Test
    public void checkTestGenerator() throws Exception {
        ServiceAnalyzer services = new ServiceAnalyzer(
                "src/test/resources/squaresSum/squaresSum.wsdl");
        ServicesDocument document = services.generateMessageCatalog();
        SACatalogParser lectura = new SACatalogParser(document);
        lectura.parse("squaresSumService", "squaresSumOperation", MessageDirection.OUT);
        List<IType> type = lectura.getTypes();
        UniformRandomStrategy tg = new UniformRandomStrategy();
        List<Object> randAT = tg.visit((TypeTuple) type.get(0));

        assertThat(randAT.size(), is(equalTo(2)));
        TypeTuple tTuple = (TypeTuple) type.get(0);
        TypeInt tInt = (TypeInt) tTuple.getIType(0);
        assertTrue(tInt.getMinValue().compareTo((BigInteger) randAT.get(0)) <= 0);
        assertTrue(tInt.getMaxValue().compareTo((BigInteger) randAT.get(0)) >= 0);
    }

    @Test(expected = ParserException.class)
    public void operationNoExist() throws Exception {
        SACatalogParser lectura = new SACatalogParser("./messageCatalog.xml");
        lectura.parse("LoanServiceService", "NoExiste", MessageDirection.IN);//
    }

    @Test(expected = ParserException.class)
    public void serviceNoExist() throws Exception {
        SACatalogParser lectura = new SACatalogParser("./messageCatalog.xml");
        lectura.parse("NoExiste", "grantLoan", MessageDirection.IN);//
    }
}
