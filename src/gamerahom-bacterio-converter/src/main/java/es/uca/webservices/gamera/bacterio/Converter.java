package es.uca.webservices.gamera.bacterio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.bacterio.cli.CLIRunner;
import es.uca.webservices.gamera.record.AnalysisSimulationRecord;
import es.uca.webservices.gamera.record.ComparisonSimulationRecord;
import es.uca.webservices.gamera.record.SimulationRecord;

public class Converter {

	private static final BigInteger NANOS_TO_MILLIS = BigInteger.valueOf(1000000);
	private static final Logger LOGGER = LoggerFactory.getLogger(Converter.class);

	public SimulationRecord convert(File analysisResults, File resultsGamera) throws IOException {
		final AnalysisSimulationRecord aR = createAnalysisSimulationRecord(analysisResults);
		final Map<GAIndividual, ComparisonSimulationRecord> cRM = createComparisonSimulationRecordMap(resultsGamera);
		final List<String> testNames = readTestNames(resultsGamera);
		final SimulationRecord sR = new SimulationRecord();

		sR.setAnalysisRecord(aR);
		sR.setComparisonRecords(cRM);
		sR.setTestCaseNames(testNames);
		sR.setMaxOrder(1);
		sR.setMissingIndividualsAreInvalid(true);
		sR.setRecorder("Bacterio through GAmeraHOM converter " + CLIRunner.getVersion());

		return sR;
	}

	private List<String> readTestNames(File resultsGamera) throws IOException {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(resultsGamera));
			readUpToLine(reader, "Killing Matrix");

			final String[] versionLine = reader.readLine().split("\\s");
			assert "Version".equals(versionLine[0]) : "First field in Version line should be 'Version'";

			final List<String> lTestCases = Arrays.asList(versionLine);
			return lTestCases.subList(1, lTestCases.size());
		}
		finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					LOGGER.error("I/O exception while closing the reader", e);
				}
			}
		}
	}

	public AnalysisSimulationRecord createAnalysisSimulationRecord(	File analysisFile) throws IOException {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(analysisFile));

			readUpToLine(reader, "Analysis time");
			final long analysisMillis = Long.parseLong(reader.readLine());

			readUpToLine(reader, "Operators");
			final String[] opNames = reader.readLine().split("\\s");

			readUpToLine(reader, "Locations");
			final BigInteger[] locs = mapToBigIntegers(0, reader.readLine().split("\\s"));

			readUpToLine(reader, "Attributes");
			final BigInteger[] attrs = mapToBigIntegers(0, reader.readLine().split("\\s"));

			return new AnalysisSimulationRecord(
				new AnalysisResults(opNames, locs, attrs), BigInteger.valueOf(analysisMillis).multiply(NANOS_TO_MILLIS));
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					LOGGER.error("I/O exception while closing the reader", e);
				}
			}
		}
	}

	public Map<GAIndividual, ComparisonSimulationRecord> createComparisonSimulationRecordMap(File resultsGameraFile) throws IOException {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(resultsGameraFile));

			final Map<GAIndividual, ComparisonSimulationRecord> cSRM = new HashMap<GAIndividual, ComparisonSimulationRecord>(); 
			readUpToLine(reader, "Killing Matrix");
			// Discard the 'Version...' line
			reader.readLine();
			// Read each of the comparison results now
			{
				String line;
				while ((line = reader.readLine()) != null && !"Costs Matrix".equals(line)) {
					final String[] components = line.split("\\t+");
					final GAIndividual ind = mapToIndividual(components);
					final Integer[] row = mapToComparisonResults(1, components);
					cSRM.put(ind, new ComparisonSimulationRecord(row, BigInteger.ZERO));
				}
			}

			readUpToLine(reader, "Times Matrix");
			// Discard the 'Version...' line
			reader.readLine();
			// Read the test case times now, sum them and convert to milliseconds
			{
				String line;
				while ((line = reader.readLine()) != null) {
					final String[] components = line.split("\\t+");
					final GAIndividual ind = mapToIndividual(components);
					final BigInteger[] testCaseNanos = mapToBigIntegers(1, components);
					cSRM.get(ind).setTestWallNanos(testCaseNanos);
					cSRM.get(ind).setTotalWallNanos(sum(testCaseNanos));
				}
			}

			return cSRM;
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					LOGGER.error("I/O exception while closing the reader", e);
				}
			}
		}
	}

	private BigInteger sum(BigInteger... values) {
		BigInteger sum = BigInteger.ZERO;
		for (BigInteger x : values) {
			sum = sum.add(x);
		}
		return sum;
	}

	private GAIndividual mapToIndividual(final String[] components) {
		final BigInteger[] indCmp = mapToBigIntegers(0, components[0].split("\\s+"));
		return new GAIndividual(indCmp[0], indCmp[1], indCmp[2], true);
	}

	private Integer[] mapToComparisonResults(int start, String[] components) {
		final Integer[] results = new Integer[components.length - start];
		for (int i = start; i < components.length; ++i) {
			results[i - start] = "X".equals(components[i]) ? 1 : 0;
		}
		return results;
	}

	private BigInteger[] mapToBigIntegers(int start, String... numbers) {
		final BigInteger[] results = new BigInteger[numbers.length - start];
		for (int i = start; i < numbers.length; ++i) {
			results[i - start] = numbers[i].trim().length() > 0 ? new BigInteger(numbers[i]) : BigInteger.ZERO;
		}
		return results;
	}

	private void readUpToLine(BufferedReader reader, String delimiterLine) throws IOException {
		String line;
		while ((line = reader.readLine()) != null && !delimiterLine.equals(line));
	}
}
