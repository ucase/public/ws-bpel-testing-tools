package es.uca.webservices.gamera.bacterio.cli;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.bacterio.Converter;
import es.uca.webservices.gamera.record.SimulationRecord;

/**
 * Entry point from the command line.
 * 
 * @author Antonio García-Domínguez
 */
public class CLIRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(CLIRunner.class);
	private static final int BAD_USAGE = 1;
	private static final int IO_EXCEPTION = BAD_USAGE + 1;
	private static Properties props;

	public static void main(String[] args) {
		final int statusCode = new CLIRunner().run(args);
		if (statusCode != 0) {
			System.exit(statusCode);
		}
	}

	public static String getVersion() throws IOException {
		return getProperty("gamerahombacterio.version");
	}

	public int run(String[] args) {
		try {
			if (args.length != 3) {
				System.err.println("GAmeraHOM Bacterio results converter " + getVersion());
				System.err.println("Usage: " + getProperty("launcher.name")
						+ " AnalysisResults.txt ResultsGamera.txt destination.(yaml|raw)");
				return BAD_USAGE;
			}
			run(new File(args[0]), new File(args[1]), new File(args[2]));
			return 0;
		} catch (IOException e) {
			LOGGER.error("I/O exception", e);
			return IO_EXCEPTION;
		}
	}

	public void run(File analysisResults, File resultsGamera, File destination) throws IOException {
		final Converter converter = new Converter();
		final SimulationRecord r = converter.convert(analysisResults, resultsGamera);
		r.saveTo(destination);
	}

	private static synchronized String getProperty(String property)
			throws IOException {
		if (props == null) {
			props = new Properties();
			props.load(CLIRunner.class
					.getResourceAsStream("/gamerahombacterio.properties"));
		}
		return props.getProperty(property);
	}
}
