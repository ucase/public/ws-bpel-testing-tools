package es.uca.webservices.gamera.bacterio;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.math.BigInteger;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.record.AnalysisSimulationRecord;
import es.uca.webservices.gamera.record.ComparisonSimulationRecord;
import es.uca.webservices.gamera.record.SimulationRecord;

public class ConverterTest {

	private static final File MONOPOLY_ANALYSIS = new File("src/test/resources/monopoly/AnalysisResults.txt"); 
	private static final File MONOPOLY_RESULTS = new File("src/test/resources/monopoly/resultsGamera.txt"); 

	private Converter converter;

	@Before
	public void setUp() {
		converter = new Converter();
	}

	@Test
	public void monopolyAnalysis() throws Exception {
		final AnalysisSimulationRecord aR = converter.createAnalysisSimulationRecord(MONOPOLY_ANALYSIS);
		assertEquals(10000000, aR.getTotalWallNanos().intValue());
		assertArrayEquals(
			new String[] {"lcr", "ror", "inc", "uoi", "dec", "swa", "aor", "nuf", "tex", "abs"},
			aR.getOperatorNames());
		assertEquals(BigInteger.valueOf(140), aR.getLocationCounts()[1]);
		assertEquals(BigInteger.valueOf(4), aR.getFieldRanges()[6]);
	}

	@Test
	public void monopolyResults() throws Exception {
		final Map<GAIndividual, ComparisonSimulationRecord> cRM
			= converter.createComparisonSimulationRecordMap(MONOPOLY_RESULTS);

		{
			final GAIndividual ind = new GAIndividual(4, 9, 1, true);
			final ComparisonSimulationRecord cR = cRM.get(ind);
			// We apply integer (truncating) division from Bacterio's ns to GAmera's ms
			assertEquals(728893778L, cR.getTotalWallNanos().longValue());
			assertEquals(0, cR.getTestWallNanos()[0].longValue());
			assertEquals(3483175, cR.getTestWallNanos()[1].longValue());
			assertEquals(1, cR.getFirstTestWithDifferentOutput());
			assertEquals(3483175, cR.getNanosUpToFirstDeath().longValue());
		}

		{
			final GAIndividual ind = new GAIndividual(10, 9, 1, true);
			final ComparisonSimulationRecord cR = cRM.get(ind);
			assertEquals(3775738684L, cR.getTotalWallNanos().longValue());
			assertEquals(cR.getTestCaseCount(), cR.getFirstTestWithDifferentOutput());
			assertEquals(3775738684L, cR.getNanosUpToFirstDeath().longValue());
		}

		{
			final GAIndividual ind = new GAIndividual(6, 24, 1, true);
			final ComparisonSimulationRecord cR = cRM.get(ind);
			assertEquals(new Integer(0), cR.getRow()[0]);
			assertEquals(new Integer(1), cR.getRow()[1]);
			assertEquals(new Integer(0), cR.getRow()[2]);
			assertEquals(new Integer(0), cR.getRow()[3]);
			assertEquals(new Integer(1), cR.getRow()[4]);
		}
	}

	@Test
	public void monopolyConversion() throws Exception {
		final SimulationRecord r = converter.convert(MONOPOLY_ANALYSIS, MONOPOLY_RESULTS);

		assertEquals(0, r.getCleanupWallNanos().intValue());
		assertEquals(0, r.getPrepareWallNanos().intValue());
		assertEquals(1, r.getMaxOrder());
		assertEquals(SimulationRecord.CURRENT_FORMAT_VERSION, r.getFormatVersion());
		assertEquals("edu.uclm.esi.iso5.juegos.monopoly.dominio.tests.DadosTest_testTirarDados", r.getTestCaseNames().get(0));
		assertTrue(r.getRecorder().contains("Bacterio"));
		assertTrue(r.getMissingIndividualsAreInvalid());
		assertNotNull(r.getAnalysisRecord());
		assertNotNull(r.getComparisonRecords());
	}
}
