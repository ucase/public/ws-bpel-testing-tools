// MainWindow.cs created with MonoDevelop
// User: lo at 10:43 21/09/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//
using System;
using System.Diagnostics;
using System.ComponentModel;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using Gtk;
using Glade;
using System.Threading;
using GLib;

public partial class MainWindow: Gtk.Window
{	
	public struct ProgressData {
			public Gtk.Window window;
			public Gtk.ProgressBar pbar;
			public uint timer;
			public bool activity_mode;
		}
		
	static ProgressData pdata;
	
	static bool progress_timeout()
	{		
		if (pdata.activity_mode)
			pdata.pbar.Pulse();
		
		return true;
	}
			
	public MainWindow (): base (Gtk.WindowType.Toplevel)
	{
		Build ();
	}
	
	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	// Pestaña de Análisis
	
	protected virtual void OnButtonAceptarClicked (object sender, System.EventArgs e)
	{
		string bpelPath  = Filechooser1.Filename;
		string bpelName  = System.IO.Path.GetFileNameWithoutExtension(bpelPath);
		string outputDir = Directorychooser1.Filename;

		string analysisOrigPath = System.IO.Path.Combine(outputDir, bpelName + ".bpel.operadores_original");
		string analysisMutPath  = System.IO.Path.Combine(outputDir, bpelName + ".bpel.operadores");
		
		System.Diagnostics.Process r = new System.Diagnostics.Process();
		r = System.Diagnostics.Process.Start("touch analizar.sh");
		r = System.Diagnostics.Process.Start("touch " + analysisOrigPath);
		r = System.Diagnostics.Process.Start("touch " + analysisMutPath);
		r = System.Diagnostics.Process.Start("chmod +x analizar.sh");
		r.WaitForExit();
		r.Close();
		
		string cabecera = "#! /bin/bash" + "\n";
		string orden = "mubpel analyze " + bpelPath + " > " + analysisOrigPath + "\n";
		string name = "analizar.sh";
		System.IO.StreamWriter sw = new System.IO.StreamWriter(name);
		sw.WriteLine(cabecera);
		sw.WriteLine(orden);
		sw.Close();
		
		System.Diagnostics.Process p = new System.Diagnostics.Process();
		p = System.Diagnostics.Process.Start("analizar.sh");
		p.WaitForExit();
		p.Close();
		
		ModificaCheckBox();
		
		VisualizarActivos(analysisOrigPath);
		
		System.Diagnostics.Process d = new System.Diagnostics.Process();
		d = System.Diagnostics.Process.Start("rm analizar.sh");
		d.WaitForExit();
		d.Close();
	}	

	protected virtual void OnButtonSiguienteClicked (object sender, System.EventArgs e)
	{
		string bpelPath  = Filechooser1.Filename;
		string bpelName  = System.IO.Path.GetFileNameWithoutExtension(bpelPath);
		string outputDir = Directorychooser1.Filename;

		string analysisMutPath  = System.IO.Path.Combine(outputDir, bpelName + ".bpel.operadores");
		string analysisOrigPath = System.IO.Path.Combine(outputDir, bpelName + ".bpel.operadores_original");
		
		Activos(analysisOrigPath, analysisMutPath);

		Filechooser2.SelectFilename(bpelPath);
		Filechooser3.SelectFilename(bpelPath);
		Directorychooser2.SelectFilename(outputDir);
		Directorychooser3.SelectFilename(outputDir);
		notebook1.NextPage();
	}

	
	// Pestaña de Generación y Ejecución
	
	protected virtual void OnButtonTodosClicked (object sender, System.EventArgs e)
	{
		string bpelPath  = Filechooser2.Filename;
		string bpelName  = System.IO.Path.GetFileNameWithoutExtension(bpelPath);
		string outputDir = Directorychooser2.Filename;
		string analysisMutPath = System.IO.Path.Combine(outputDir, bpelName + ".bpel.operadores");
		string analysisOrigPath = System.IO.Path.Combine(outputDir, bpelName + ".bpel.operadores_original");
		
		if (System.IO.File.Exists(analysisMutPath)){ // Comprobamos que existe el fichero de análisis

			if (sbNtest.Text.Length != 0){  // Comprobamos que está el número de test
				string fmutantes = System.IO.Path.Combine(outputDir, "mutantes-generados-aux.dat");
				string delimitaStr = " \t";
				char [] delimita = delimitaStr.ToCharArray();
				
				Filechooser3.SelectFilename(bpelPath);
				Filechooser4.SelectFilename(bpelPath);
				
				Directorychooser3.SelectFilename(outputDir);
				Directorychooser4.SelectFilename(outputDir);
				
				InstruccionesConsola(outputDir);
				string bptsPath = FilechooserText.Filename;
				string bpelOutputPath = System.IO.Path.Combine(outputDir, bpelName + ".out");
				
				string name      = "script.sh";
				string namejec   = "scriptejec.sh";
				string namecomp  = "scriptcomp.sh";
				string cabecera  = "#! /bin/bash" + "\n";
				string Operador  = "";
				string ordenejec = String.Format("mubpel run {0} {1} > {2}", bptsPath, bpelPath, bpelOutputPath);
				int contOp = 1;
		
				System.IO.StreamWriter sw = new System.IO.StreamWriter(name);
				sw.WriteLine(cabecera);
				
				System.IO.StreamWriter ssw = new System.IO.StreamWriter(namejec);
				ssw.WriteLine(cabecera);
				ssw.WriteLine("ActiveBPEL.sh start");
				ssw.WriteLine(ordenejec);
				ssw.Close();
				
				// Inicio de la medición del tiempo
				
				string inicio = System.DateTime.Now.TimeOfDay.ToString(); 
				
				// Lanzamos el script de ejecución
				
				ScriptEjecucion();
				
				// Preparamos los scripts apply y compare
				
				System.IO.StreamWriter sc = new System.IO.StreamWriter(namecomp);
				sc.WriteLine(cabecera);
				
				System.IO.StreamReader sr = new System.IO.StreamReader(analysisMutPath);
				
				System.IO.StreamWriter wm = new System.IO.StreamWriter(fmutantes);
							
				while ((Operador = sr.ReadLine()) != null){ // Leemos del fichero resultante
					
					if (Activado(contOp, analysisOrigPath, analysisMutPath)){ // Controlamos que sólo se ejecuten los activos
						
						// Leemos la instrucción y la convertimos
						Array op = Operador.Split(delimita);
						string instF = op.GetValue(1).ToString();
						int instruccionF = Convert.ToInt32(instF);
						instruccionF += 1;
						
						// Leemos los atributos del operador y lo convertimos
						string atrF = op.GetValue(2).ToString();
						int atributoF = Convert.ToInt32(atrF);
						atributoF += 1;
						
						// Con esto ya sabemos las iteraciones que hay que hacer
						// Sumamos uno a los valores anteriores, para el bucle de generación
						
						if(instruccionF != 0){
							
							for (int instruccion = 1; instruccion != instruccionF; instruccion++){
								for (int atributo = 1; atributo != atributoF; atributo++){
									string mutantName    = String.Format("m-{0:d}-{1}-{2}.bpel", contOp, instruccion, atributo);
									string mutantPath    = System.IO.Path.Combine(System.IO.Path.Combine(outputDir, "mutantes"), mutantName);
									string mutantResName = String.Format("salida{0}{1}{2}{3}.out", bpelName, op.GetValue(0), instruccion, atributo);							
									string mutantResPath = System.IO.Path.Combine(System.IO.Path.Combine(outputDir, "Salida"), mutantResName);
									string individualFields = contOp + " " + instruccion + " " + atributo;

									wm.WriteLine(individualFields + " ");
									string orden     = String.Format("mubpel apply {0} {1} > {2}", bpelPath, individualFields, mutantPath);
									string ordencomp = String.Format("mubpel compare {0} {1} {2} {3} > {4}", bptsPath, bpelPath, bpelOutputPath, mutantPath, mutantResPath);

									sw.WriteLine(orden);
									sc.WriteLine(ordencomp);
								}
							}
						}
					}
					contOp++;
				}
				
				sw.Close();
				sc.Close();
				sr.Close();
				wm.Close();
								
				// Lanzamos los scripts apply y compare
				
				ScriptsApplyCompare();
				
				// Finalizamos la medición temporal
				
				string fin = System.DateTime.Now.TimeOfDay.ToString();
			    double tiempo = CalcularSegundos(inicio, fin);
					
				string datos = System.IO.Path.Combine(outputDir, "datos.dat");
				System.IO.StreamWriter d = new System.IO.StreamWriter(datos);
				d.WriteLine("GA");
				d.WriteLine("valores");
				d.WriteLine("1_" + sbNtest.Text + "_0.00_0.00");
				d.WriteLine("tiempo");
				d.WriteLine(tiempo);
				d.Close();
				
				CrearFicheroMutante(bpelPath, outputDir);
				
				notebook1.NextPage();
			}
			else{
				
				VentanaFaltaBpts();
			}
		}
		else{
			
			VentanaFaltaFAnalisis();
			
			Filechooser1.SelectFilename(bpelPath);
		
			Directorychooser1.SelectFilename(outputDir);
			
			notebook1.PrevPage();
		}
	}	
	
	protected virtual void OnButtonSelectClicked (object sender, System.EventArgs e)
	{
		string bpelPath  = Filechooser2.Filename;
		string bpelName  = System.IO.Path.GetFileNameWithoutExtension(bpelPath);
		string bptsPath  = FilechooserText.Filename;
		string outputDir = Directorychooser2.Filename;
		string analysisOrigPath = System.IO.Path.Combine(outputDir, bpelName + ".bpel.operadores");
		
		if (System.IO.File.Exists(analysisOrigPath)){
			
			if (sbNtest.Text.Length != 0){
				
				Filechooser3.SelectFilename(bpelPath);
				Filechooser4.SelectFilename(bpelPath);
			
				Directorychooser3.SelectFilename(outputDir);
				Directorychooser4.SelectFilename(outputDir);
				
				// Preparamos el fichero gamera.conf
				string confPath = System.IO.Path.Combine(outputDir, "gamera.conf");
				string dataPath = System.IO.Path.Combine(outputDir, "datos.dat");
				System.Diagnostics.Process r = new System.Diagnostics.Process();
				r = System.Diagnostics.Process.Start("touch script.sh");
				r = System.Diagnostics.Process.Start("touch " + confPath);
				r = System.Diagnostics.Process.Start("touch " + dataPath);
				r = System.Diagnostics.Process.Start("chmod +x script.sh");
				r.WaitForExit();
				r.Close();
				
				string fanalisis = bpelName + ".bpel.operadores";
				string name = "script.sh";
				string cabecera = "#! /bin/bash \n";
				string orden = "gamera seed " + spbSemilla.Text.ToString() + " file " + confPath;
				
				System.IO.StreamWriter w = new System.IO.StreamWriter(name);
				w.WriteLine(cabecera);
				w.WriteLine(orden);
				w.Close();
				
				string valor = "";
				double conversion;
				
				string linea1 = "TAMPOBLACION " + sbTpob.Text;
				string linea2 = "NUMTEST " + sbNtest.Text;
				//string linea3 = "NUMGENERACIONES " + sbNgen.Text;
				
				valor = sbPmuta.Text;
				conversion = Convert.ToDouble(valor);
				conversion = conversion / 100;
				valor = conversion.ToString();
				string pmuta = valor;
				string linea4 = "PMUTACION " + DeComaAPunto(valor);
				
				valor = sbPcruce.Text;
				conversion = Convert.ToDouble(valor);
				conversion = conversion / 100;
				valor = conversion.ToString();
				string pcruce = valor;
				string linea5 = "PCRUCE " + DeComaAPunto(valor);
				
				valor = sbPsust.Text;
				conversion = Convert.ToDouble(valor);
				conversion = conversion / 100;
				valor = conversion.ToString();
				string linea6 = "PSUSTITUIR " + DeComaAPunto(valor);
				
				valor = sbPnuevo.Text;
				conversion = Convert.ToDouble(valor);
				conversion = conversion / 100;
				valor = conversion.ToString();
				string linea7 = "PNUEVO " + DeComaAPunto(valor);
				
				string linea8  = "BPELORIGINAL 1 " + Filechooser2.Filename;
				string linea9  = "BPELOUT 0 "      + bpelName + ".out";
				string linea10 = "TESTSUITE 1 "    + bptsPath;

				string paramsSuffix = sbTpob.Text + "_" + sbNtest.Text + "_" + pcruce + "_" + pmuta + ".dat";
				string linea11 = "SALIDAGAMERA 1 generaciones" + paramsSuffix;
				string linea12 = "ESTADISTICAS 1 estadisticas" + paramsSuffix;
				string linea13 = "DIFERENTES 0";
				string linea14 = "BDMUTANTES 1 mutantes-generados" + paramsSuffix;

				valor = sbPmg.Text;
				conversion = Convert.ToDouble(valor);
				conversion = conversion / 100;
				valor = conversion.ToString();
				string linea15 = "PMUTANTES " + DeComaAPunto(valor);
				
				string linea16 = "DIRECTORIOSALIDA 1 " + outputDir;
				string linea17 = "ANALIZADOR 1 " + System.IO.Path.Combine(outputDir, fanalisis);
		
				string nombrefich = System.IO.Path.Combine(outputDir, "gamera.conf");
				System.IO.StreamWriter sw = new System.IO.StreamWriter(nombrefich);
				sw.WriteLine(linea1);
				sw.WriteLine(linea2);
				//sw.WriteLine(linea3);
				sw.WriteLine(linea4);
				sw.WriteLine(linea5);
				sw.WriteLine(linea6);
				sw.WriteLine(linea7);
				sw.WriteLine(linea8);
				sw.WriteLine(linea9);
				sw.WriteLine(linea10);
				sw.WriteLine(linea11);
				sw.WriteLine(linea12);
				sw.WriteLine(linea13);
				sw.WriteLine(linea14);
				sw.WriteLine(linea15);
				sw.WriteLine(linea16);
				sw.WriteLine(linea17);
				sw.Close();

				// Comenzamos la medición del tiempo
				
				string inicio = System.DateTime.Now.TimeOfDay.ToString();
				ScriptGamera();
				
				// Finalizamos la medición temporal
				string fin = System.DateTime.Now.TimeOfDay.ToString();
				
				double tiempo = CalcularSegundos(inicio, fin);
				
				string datos = System.IO.Path.Combine(outputDir, "datos.dat");
				System.IO.StreamWriter d = new System.IO.StreamWriter(datos);
				d.WriteLine("AG");
				d.WriteLine("valores");
				d.WriteLine(sbTpob.Text + "_" + sbNtest.Text + "_" + pcruce + "_" + pmuta);
				d.WriteLine("tiempo");
				d.WriteLine(tiempo);
				d.Close();
				
				notebook1.NextPage();
				
			}
			else{
				
				VentanaFaltaBpts();
			}
		}
		else{
			
			VentanaFaltaFAnalisis();
		
			string direccion = Filechooser2.Filename.ToString();
			Filechooser1.SelectFilename(direccion);
			
			string direccionD = Directorychooser2.Filename.ToString();
			Directorychooser1.SelectFilename(direccionD);
			
			notebook1.PrevPage();
		}
	}
	
	protected virtual void OnButoonAleatoriaClicked (object sender, System.EventArgs e)
	{
		Random objeto = new Random();
		int numero = objeto.Next(0, 65536);
		spbSemilla.Text = numero.ToString();
	}
	
	protected virtual void OnFilechooserTextSelectionChanged (object sender, System.EventArgs e)
	{
		string cadena = "";
		int cont = 0;
		System.IO.StreamReader sr = new System.IO.StreamReader(FilechooserText.Filename);
		
		while ((cadena = sr.ReadLine()) != null){
			if (cadena.Contains("</tes:testCase>")){
				cont++;
			}
		}
		sr.Close();
		
		sbNtest.Text = cont.ToString();
	}

	protected virtual void OnFilechooserGameraSelectionChanged (object sender, System.EventArgs e)
	{
		string file = filechooserGamera.Filename;
		string cadena = "";
		string delimitaStr = " \t";
		char [] delimita = delimitaStr.ToCharArray();
		NumberFormatInfo separador = new NumberFormatInfo();
		separador.NumberDecimalSeparator = ".";
		System.IO.StreamReader sr = new System.IO.StreamReader(file);
		
		while ((cadena = sr.ReadLine()) != null){
			
			if (cadena.Contains("TAMPOBLACION")){
				Array num = cadena.Split(delimita);
				string valor = num.GetValue(1).ToString();
				sbTpob.Text = valor;
			}
			
			if (cadena.Contains("NUMTEST")){
				if (sbNtest.Text.Equals("")){
					Array num = cadena.Split(delimita);
					string valor = num.GetValue(1).ToString();
					sbNtest.Text = valor;
				}
			}
			
			/*if (cadena.Contains("NUMGENERACIONES")){
				Array num = cadena.Split(delimita);
				string valor = num.GetValue(1).ToString();
				sbNgen.Text = valor;
			}*/
			
			if (cadena.Contains("PMUTACION")){
				Array num = cadena.Split(delimita);
				string valor = num.GetValue(1).ToString();
				double nvalor = Convert.ToDouble(valor,separador);
				nvalor = nvalor * 100;
				sbPmuta.Text = nvalor.ToString();
			}
			
			if (cadena.Contains("PCRUCE")){
				Array num = cadena.Split(delimita);
				string valor = num.GetValue(1).ToString();
				double nvalor = Convert.ToDouble(valor, separador);
				nvalor = nvalor * 100;
				sbPcruce.Text = nvalor.ToString();
			}
			
			if (cadena.Contains("PSUSTITUIR")){
				Array num = cadena.Split(delimita);
				string valor = num.GetValue(1).ToString();
				double nvalor = Convert.ToDouble(valor, separador);
				nvalor = nvalor * 100;
				sbPsust.Text = nvalor.ToString();
			}
			
			if (cadena.Contains("PNUEVO")){
				Array num = cadena.Split(delimita);
				string valor = num.GetValue(1).ToString();
				double nvalor = Convert.ToDouble(valor, separador);
				nvalor = nvalor * 100;
				sbPnuevo.Text = nvalor.ToString();
			}
			
			if (cadena.Contains("PMUTANTES")){
				Array num = cadena.Split(delimita);
				string valor = num.GetValue(1).ToString();
				double nvalor = Convert.ToDouble(valor, separador);
				nvalor = nvalor * 100;
				sbPmg.Text = nvalor.ToString();
			}
		}
	
		sr.Close();
	}
	
	
	// Pestaña de comparativas de mutantes
	
	protected virtual void OnBtnVisualizarClicked (object sender, System.EventArgs e)
	{
		
		string ruta = Directorychooser3.Filename + "/";
		
		if (System.IO.File.Exists(ruta + "datos.dat")) {

			string op = cbbOperador.ActiveText;
			string detalles = cbbDetalles.ActiveText;
			string OpDet = NumOperador(op);
			OpDet += "-" + detalles;

			if (cbbDetalles.ActiveText != " " && cbbOperador.ActiveText != " ") {
			
				System.Diagnostics.Process c = new System.Diagnostics.Process();
				c = System.Diagnostics.Process.Start("mkdir -p " + ruta + "Comparativas");
				c = System.Diagnostics.Process.Start("touch " + ruta + "Comparativas/scriptXmlDiff.sh");
				c = System.Diagnostics.Process.Start("touch " + ruta + "Comparativas/metiq" + OpDet + ".bpel");
				c = System.Diagnostics.Process.Start("touch " + ruta + "Comparativas/diferencia.xml");
				c.WaitForExit();
				c.Close();
				
				System.Diagnostics.Process s = new System.Diagnostics.Process();
				s = System.Diagnostics.Process.Start("chmod +x " + ruta + "Comparativas/scriptXmlDiff.sh");
				s.WaitForExit();
				s.Close();
				
				Filtro(ruta + "mutantes/m-" + OpDet + ".bpel", ruta);
				Filtro(Filechooser3.Filename.ToString(), ruta);

				// exePath from www.eggheadcafe.comcommunity/aspnet/2/10096144/how-to-get-the-path-of-an-executable-file-using-c.aspx
				string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
				string xmldiffPath = System.IO.Path.Combine(exePath, "xmldiff.pl");
				string orden = "perl " + xmldiffPath + " -X " + Filechooser3.Filename.ToString() + " " + ruta + "mutantes/m-" + OpDet + ".bpel > " + ruta + "Comparativas/diferencia.xml";
				string fichero = ruta + "Comparativas/scriptXmlDiff.sh";
				
				System.IO.StreamWriter sw = new System.IO.StreamWriter(fichero);
				sw.WriteLine("#! /bin/bash");
				sw.WriteLine(orden);
				sw.Close();
				
				System.Diagnostics.Process d = new System.Diagnostics.Process();
				d = System.Diagnostics.Process.Start(ruta + "Comparativas/scriptXmlDiff.sh");
				d.WaitForExit();
				d.Close();
				
				string fichMEtiq = ruta + "Comparativas/metiq" + OpDet + ".bpel";
				string fichOEtiq = ruta + "Comparativas/" + SeleccionarNombre(Filechooser3.Filename) + "etiq.bpel";
				string MuestraOrig = ruta + "Comparativas/" + SeleccionarNombre(Filechooser3.Filename) + "muestra.bpel";
				string MuestraMut = ruta + "Comparativas/mmuestra" + OpDet + ".bpel";
				string linea = "";
				string ficheroXmldiff = ruta + "Comparativas/diferencia.xml";
				
				System.IO.StreamReader diff = new System.IO.StreamReader(ficheroXmldiff);
				System.IO.StreamWriter swo = new System.IO.StreamWriter(fichOEtiq);
				System.IO.StreamWriter swm = new System.IO.StreamWriter(fichMEtiq);
				System.IO.StreamWriter MOriginal = new System.IO.StreamWriter(MuestraOrig);
				System.IO.StreamWriter MMutante = new System.IO.StreamWriter(MuestraMut);
				
				string sublinea = "";
				int contR = CuentaEtiquetas(ficheroXmldiff, "#r");
				int contV = CuentaEtiquetas(ficheroXmldiff, "#v");
				
				while (!diff.EndOfStream){
					linea = diff.ReadLine();
						
					/* Para controlar los diferentes cambios que hayan de realizarse en los ficheros etiquetados, tanto original 
					 * como mutante, el procedimiento más óptimo es el de ir escribiendo en el fichero desde el que ya tiene las 
					 * diferencias "ficheroXmldiff" (ya que está bien identado) y cuando aparezcan las etiquetas <rojo> o <verde> 
					 * nos indicarán si hay que escribir en el etiquetado original o bien en el etiquetado mutante */
					
					if (linea.Contains("#r")){
						swm.WriteLine(linea);
						sublinea = linea.Substring(2);
						MMutante.WriteLine(sublinea);
						if (contR > contV){
							swo.WriteLine("#v       ");
							MOriginal.WriteLine("       ");
						}
					}
					else if (linea.Contains("#v")){
						swo.WriteLine(linea);
						sublinea = linea.Substring(2);
						MOriginal.WriteLine(sublinea);
						if (contR < contV){
							swm.WriteLine("#r       ");
							MMutante.WriteLine("       ");
						}
					}
					else {
						swo.WriteLine(linea);
						swm.WriteLine(linea);
						MMutante.WriteLine(linea);
						MOriginal.WriteLine(linea);
					}
				}
				
				diff.Close();
				swo.Close();
				swm.Close();
				MMutante.Close();
				MOriginal.Close();
							
				System.Diagnostics.Process b = new System.Diagnostics.Process();	
				b = System.Diagnostics.Process.Start("rm " + ruta + "Comparativas/scriptXmlDiff.sh");
				b = System.Diagnostics.Process.Start("rm " + ruta + "Comparativas/diferencia.xml");
				b.WaitForExit();
				b.Close();
				
				TextBuffer bufferOrig;
				bufferOrig = textviewOrig.Buffer;
				System.IO.StreamReader srfo = new System.IO.StreamReader(MuestraOrig);
				bufferOrig.Text = srfo.ReadToEnd();
				srfo.Close();
				
				AplicarEstiloVerde(bufferOrig, "v", fichOEtiq);
				
				TextBuffer bufferMut;
				bufferMut = textviewMut.Buffer;
				System.IO.StreamReader srfm = new System.IO.StreamReader(MuestraMut);
				bufferMut.Text = srfm.ReadToEnd();
				srfm.Close();
				
				AplicarEstiloRojo(bufferMut, "r", fichMEtiq);
			}
			else {
				VentanaFaltaOpr();
			}			
		}
		else {
			VentanaFaltaDatos();
			notebook1.PrevPage();
			notebook1.PrevPage();
		}
	}	

	
	// Pestaña de Resultados

	protected virtual void OnBtnVisualizarMutantesClicked (object sender, System.EventArgs e)
	{
		System.Diagnostics.Process v = new System.Diagnostics.Process();
		v = System.Diagnostics.Process.Start("emacs " + Directorychooser4.Filename + "/generaciones" + LeerFichero(Directorychooser4.Filename) + ".dat");
		v.WaitForExit();
		v.Close();
	}

	protected virtual void OnBtnVisualizarMutantesGClicked (object sender, System.EventArgs e)
	{
		System.Diagnostics.Process v = new System.Diagnostics.Process();
		v = System.Diagnostics.Process.Start("emacs " + Directorychooser4.Filename + "/mutantes-generados" + LeerFichero(Directorychooser4.Filename) + ".dat");
		v.WaitForExit();
		v.Close();
	}

	protected virtual void OnBtnVisualizarEstadisticasClicked (object sender, System.EventArgs e)
	{
		System.Diagnostics.Process v = new System.Diagnostics.Process();
		v = System.Diagnostics.Process.Start("emacs " + Directorychooser4.Filename + "/estadisticas" + LeerFichero(Directorychooser4.Filename) + ".dat");
		v.WaitForExit();
		v.Close();
	}

	protected virtual void OnBtnVisualizarGameraClicked (object sender, System.EventArgs e)
	{
		System.Diagnostics.Process v = new System.Diagnostics.Process();
		v = System.Diagnostics.Process.Start("emacs " + Directorychooser4.Filename + "/resumen.dat");
		v.WaitForExit();
		v.Close();
	}

	protected virtual void OnButtonCalcularClicked (object sender, System.EventArgs e)
	{
		string ruta = Directorychooser4.Filename + "/";
		
		if (System.IO.File.Exists(ruta + "datos.dat")) {
			
			int contVivos = 0;
			int contMuertos = 0;
			int contError = 0;
			int contGenerados = 0;
			
			string linea = "";
			string delimitaStr = " \t";
			char [] delimita = delimitaStr.ToCharArray();
			string fileOut = "mutantes-generados" + LeerFichero(Directorychooser4.Filename.ToString()) + ".dat";  
			string file =  System.IO.Path.Combine(ruta, fileOut);
			System.IO.StreamReader sr = new System.IO.StreamReader(file);
	
			ControldeBotones();
			
			System.Diagnostics.Process r = new System.Diagnostics.Process();
			r = System.Diagnostics.Process.Start("touch " + file);
			r.WaitForExit();
			r.Close();
			
			while ((linea = sr.ReadLine()) != null){
				
				Array es = linea.Split(delimita);
				string numE = es.GetValue(4).ToString();
				int estado = Convert.ToInt32(numE);
				
				switch (estado){
				case 0:
					contVivos++;
					break;
				case 1:
					contMuertos++;
					break;
				default:
					contError++;
					break;
				}
				
				contGenerados++;		
			}
			
			sr.Close();
	
			string fileInput = Filechooser4.Filename;
			string fich = SeleccionarNombre(fileInput);
			
			EntryMutTotal.Text = Cuentas(Directorychooser4.Filename, fich).ToString();
			EntryVivos.Text = contVivos.ToString();
			EntryMuertos.Text = contMuertos.ToString();
			EntryError.Text = contError.ToString();
			EntryMutGen.Text = contGenerados.ToString();
			EntryTiempo.Text =  LeerTiempo(Directorychooser4.Filename);
			
			string tabla = Tabla(Directorychooser4.Filename, fich, fileOut).ToString();
			string tablaTest = TablaTest(Directorychooser4.Filename, fileOut).ToString();
			
			string linea1 = "tiempo " + EntryTiempo.Text;
			string linea2 = "Num_mutantes_totales " + EntryMutTotal.Text;
			string linea3 = "Num_mutantes_generados " + EntryMutGen.Text;
			string linea4 = "Num_mutantes_vivos " + EntryVivos.Text;
			string linea5 = "Num_mutantes_muertos " + EntryMuertos.Text;
			string linea6 = "Num_mutantes_error " + EntryError.Text;
			
			string nomfich = ruta + "/resumen.dat";	
			string tablas = ruta + "/tablas.txt";
		
			System.IO.StreamWriter swt = new System.IO.StreamWriter(tablas);
			swt.WriteLine(tabla);
			swt.WriteLine("\n");
			swt.WriteLine(tablaTest);
			swt.Close();
			
			System.IO.StreamWriter sw = new System.IO.StreamWriter(nomfich);
			sw.WriteLine(linea1);
			sw.WriteLine(linea2);
			sw.WriteLine(linea3);
			sw.WriteLine(linea4);
			sw.WriteLine(linea5);
			sw.WriteLine(linea6);
			sw.WriteLine("\n");
			sw.WriteLine(tabla);
			sw.WriteLine("\n");
			sw.WriteLine(tablaTest);
			sw.Close();
		
			string XmlFile = ruta + "/tablas.xml";
			
			CrearFicheroXml(XmlFile, tablas);
					
			System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
			xml.Load(XmlFile);
	
			Leer(XmlFile, tablas);
		}
		else {
			VentanaFaltaDatos();
			notebook1.PrevPage();
		}
	}	
	
	
	// Métodos de la pestaña de análisis
	
	public void ModificaCheckBox()
	{
		checkBISV.Sensitive = checkBEAA.Sensitive = checkBEEU.Sensitive = checkBERR.Sensitive = false;
		checkBELL.Sensitive = checkBECC.Sensitive = checkBECN.Sensitive = checkBEMD.Sensitive = false;
		checkBEMF.Sensitive = checkBACI.Sensitive = checkBAFP.Sensitive = checkBASF.Sensitive = false;
		checkBAIS.Sensitive = checkBAIE.Sensitive = checkBAWR.Sensitive = checkBAJC.Sensitive = false;
		checkBASI.Sensitive = checkBAPM.Sensitive = checkBAPA.Sensitive = checkBXMF.Sensitive = false;
		checkBXRF.Sensitive = checkBXMC.Sensitive = checkBXMT.Sensitive = checkBXTF.Sensitive = false;
		checkBXER.Sensitive = checkBXEE.Sensitive = false;

	}
	
	public void VisualizarActivos(string fileOutputName)
	{
		StreamReader lector;
		string linea = "";
		string delimitaStr = " \t";
		char [] delimita = delimitaStr.ToCharArray();
		string [] split;
		int cont = 1;
		
		try{
			lector = File.OpenText(fileOutputName);
			
			while((linea=lector.ReadLine()) != null){
			split = linea.Split(delimita);
				if (!split[1].Contains("0")){
					SeleccionarOperador(cont);					
				}
				cont++;
			}
			lector.Close();
		}
		catch (Exception e){
			System.Console.WriteLine("Problemas al cargar el fichero.{0}", e);
	    }
	}
	
	
	public void SeleccionarOperador(int cont)
	{
		switch(cont){
		case 1:
			checkBISV.Active = true;
			checkBISV.Sensitive = true;
			break;
		case 2: 
			checkBEAA.Active = true;
			checkBEAA.Sensitive = true;
		    break;
		case 3:
			checkBEEU.Active = true;
			checkBEEU.Sensitive = true;
			break;
		case 4:
			checkBERR.Active = true;
			checkBERR.Sensitive = true;
			break;
		case 5:
			checkBELL.Active = true;
			checkBELL.Sensitive = true;
			break;
		case 6:
			checkBECC.Active = true;
			checkBECC.Sensitive = true;
			break;
		case 7:
			checkBECN.Active = true;
			checkBECN.Sensitive = true;
			break;
		case 8:
			checkBEMD.Active = true;
			checkBEMD.Sensitive = true;
			break;
		case 9:
			checkBEMF.Active = true;
			checkBEMF.Sensitive = true;
			break;
		case 10:
			checkBACI.Active = true;
			checkBACI.Sensitive = true;
			break;
		case 11:
			checkBAFP.Active = true;
			checkBAFP.Sensitive = true;
			break;
		case 12:
			checkBASF.Active = true;
			checkBASF.Sensitive = true;
			break;
		case 13:
			checkBAIS.Active = true;
			checkBAIS.Sensitive = true;
			break;
		case 14:
			checkBAIE.Active = true;
			checkBAIE.Sensitive = true;
			break;
		case 15:
			checkBAWR.Active = true;
			checkBAWR.Sensitive = true;
			break;
		case 16:
			checkBAJC.Active = true;
			checkBAJC.Sensitive = true;
			break;
		case 17:
			checkBASI.Active = true;
			checkBASI.Sensitive = true;
			break;
		case 18:
			checkBAPM.Active = true;
			checkBAPM.Sensitive = true;
			break;
		case 19:
			checkBAPA.Active = true;
			checkBAPA.Sensitive = true;
			break;
		case 20:
			checkBXMF.Active = true;
			checkBXMF.Sensitive = true;
			break;
		case 21:
			checkBXRF.Active = true;
			checkBXRF.Sensitive = true;
			break;
		case 22:
			checkBXMC.Active = true;
			checkBXMC.Sensitive = true;
			break;
		case 23:
			checkBXMT.Active = true;
			checkBXMT.Sensitive = true;
			break;
		case 24:
			checkBXTF.Active = true;
			checkBXTF.Sensitive = true;
			break;
		case 25:
			checkBXER.Active = true;
			checkBXER.Sensitive = true;
			break;
		case 26:
			checkBXEE.Active = true;
			checkBXEE.Sensitive = true;
			break;
		}
	}

	public void Activos(string analysisOrigPath, string analysisMutPath)
	{
		StreamReader lector;
		string linea = "";
		string delimitaStr = " \t";
		char [] delimita = delimitaStr.ToCharArray();
		string [] split;
		
		try{
			lector = File.OpenText(analysisOrigPath);
			System.IO.StreamWriter sw = new System.IO.StreamWriter(analysisMutPath);

			int cont = 1;
			
			while((linea=lector.ReadLine()) != null){
				split = linea.Split(delimita);
				if (!split[1].Contains("0")){
					if(!LeerOperadorActivado(cont)){
						linea = modificar(linea);
					}
				}
				sw.WriteLine(linea);
				cont++;
			}
			sw.Close();			
			lector.Close();
		}
		catch (Exception e){
			System.Console.WriteLine("Problemas al cargar el fichero.{0}", e);
	    }	
	}

	
	public bool LeerOperadorActivado(int cont)
	{
		bool res = false;
		
		switch(cont){
		case 1:
			if (checkBISV.Active == false)
				res = false;
			else
				res = true;
			break;
		case 2: 
			if (checkBEAA.Active == false)
				res = false;
			else
				res = true;
		    break;
		case 3:
			if (checkBEEU.Active == false)
				res = false;
			else
				res = true;
			break;
		case 4:
			if (checkBERR.Active == false)
				res = false;
			else
				res = true;
			break;
		case 5:
			if (checkBELL.Active == false)
				res = false;
			else
				res = true;
			break;
		case 6:
			if (checkBECC.Active == false)
				res = false;
			else
				res = true;
			break;
		case 7:
			if(checkBECN.Active == false)
				res = false;
			else
				res = true;
			break;
		case 8:
			if(checkBEMD.Active == false)
				res = false;
			else
				res = true;
			break;
		case 9:
			if(checkBEMF.Active == false)
				res = false;
			else
				res = true;
			break;
		case 10:
			if(checkBACI.Active == false)
				res = false;
			else
				res = true;
			break;
		case 11:
			if (checkBAFP.Active == false)
				res = false;
			else
				res = true;
			break;
		case 12:
			if(checkBASF.Active == false)
				res = false;
			else
				res = true;
			break;
		case 13:
			if (checkBAIS.Active == false)
				res = false;
			else
				res = true;
			break;
		case 14:
			if(checkBAIE.Active == false)
				res = false;
			else
				res = true;
			break;
		case 15:
			if (checkBAWR.Active == false)
				res = false;
			else
				res = true;
			break;
		case 16:
			if(checkBAJC.Active == false)
				res = false;
			else
				res = true;
			break;
		case 17:
			if (checkBASI.Active == false)
				res = false;
			else
				res = true;
			break;
		case 18:
			if(checkBAPM.Active == false)
				res = false;
			else
				res = true;
			break;
		case 19:
			if(checkBAPA.Active == false)
				res = false;
			else
				res = true;
			break;
		case 20:
			if(checkBXMF.Active == false)
				res = false;
			else
				res = true;
			break;
		case 21:
			if(checkBXRF.Active == false)
				res = false;
			else
				res = true;
			break;
		case 22:
			if(checkBXMC.Active == false)
				res = false;
			else
				res = true;
			break;
		case 23:
			if(checkBXMT.Active == false)
				res = false;
			else
				res = true;
			break;
		case 24:
			if(checkBXTF.Active == false)
				res = false;
			else
				res = true;
			break;
		case 25:
			if (checkBXER.Active == false)
				res = false;
			else
				res = true;
			break;
		case 26:
			if(checkBXEE.Active == false)
				res = false;
			else
				res = true;
			break;
		}
		return res;
	}

	string modificar(string linea){
		
		string delimitaStr = " \t";
		char [] delimita = delimitaStr.ToCharArray();
		string [] split;
		string nueva = "";
		
		split = linea.Split(delimita);
		nueva = split[0] + " 0 0";
		return nueva;
	}
	
		
	// Métodos de la pestaña de generación y ejecución
	
	public void InstruccionesConsola(string outputDir)
	{
		System.Diagnostics.Process r = new System.Diagnostics.Process();
		r = System.Diagnostics.Process.Start("touch script.sh");
		r = System.Diagnostics.Process.Start("touch scriptejec.sh");
		r = System.Diagnostics.Process.Start("touch scriptcomp.sh");
		r = System.Diagnostics.Process.Start("mkdir " + System.IO.Path.Combine(outputDir, "mutantes"));
		r = System.Diagnostics.Process.Start("mkdir " + System.IO.Path.Combine(outputDir, "Salida"));
		r = System.Diagnostics.Process.Start("chmod +x script.sh");
		r = System.Diagnostics.Process.Start("chmod +x scriptejec.sh");
		r = System.Diagnostics.Process.Start("chmod +x scriptcomp.sh");
		r.WaitForExit();
		r.Close();
	}
	
	public void VentanaFaltaFAnalisis()
	{
		pdata = new ProgressData();
		pdata.activity_mode = true;
		pdata.window = new Gtk.Window(Gtk.WindowType.Toplevel);
	    pdata.window.Move(150, 200);
		pdata.window.Resizable = true;

		pdata.window.Title = "Aviso Koura";
		pdata.window.BorderWidth = 0;
	
		VBox vbox = new Gtk.VBox(false, 5);
		vbox.BorderWidth = 10;
		pdata.window.Add(vbox);
		vbox.Show();
		
		HBox hbox = new HBox(false, 2);
		hbox.BorderWidth = 6;
		vbox.PackStart(hbox);
		
		Image img = new Image("img/advertencia.jpg");
		hbox.PackStart(img,false,false,2);
		img.Show();
		
		Label l = new Label();
		l.MaxWidthChars = 40;
		l.Markup = "No existe el fichero de análisis.\nPara generarlo iremos a la primera pestaña.";
		hbox.PackStart(l,false,false,0);
	
		Button button = new Gtk.Button("cerrar");
		button.Clicked += button_click;
		vbox.PackStart(button, false, false, 0);

		button.CanDefault = true;
	
		button.GrabDefault();
		button.Show();
	
		pdata.window.ShowAll();
	}
	
	public void VentanaFaltaBpts()
	{
		pdata = new ProgressData();
		pdata.activity_mode = true;
		pdata.window = new Gtk.Window(Gtk.WindowType.Toplevel);
	    pdata.window.Move(150, 200);
		pdata.window.Resizable = true;

		pdata.window.Title = "Aviso Koura";
		pdata.window.BorderWidth = 2;
		
		VBox vbox = new Gtk.VBox(false, 5);
		vbox.BorderWidth = 10;
		pdata.window.Add(vbox);
		vbox.Show();
		
		HBox hbox = new HBox(false, 2);
		hbox.BorderWidth = 6;
		vbox.PackStart(hbox);
		
		Image img = new Image("img/advertencia.jpg");
		hbox.PackStart(img,false,false,2);
		img.Show();
		
		Label l = new Label("Falta añadir el fichero .bpts");
		l.MaxWidthChars = 40;
		hbox.PackEnd(l,false,false,0);
	
		Button button = new Gtk.Button("cerrar");
		button.Clicked += button_click;
		
		vbox.PackStart(button, false, false, 0);

		button.CanDefault = true;
	
		button.GrabDefault();
		button.Show();
	
		pdata.window.ShowAll();
	}
	
	public void ScriptEjecucion()
	{
		System.Diagnostics.Process x = new System.Diagnostics.Process();	
		x = System.Diagnostics.Process.Start("./scriptejec.sh");
		x.WaitForExit();
			
		System.Diagnostics.Process b = new System.Diagnostics.Process();	
		b = System.Diagnostics.Process.Start("rm scriptejec.sh");
		b.WaitForExit();
		b.Close();
	}

	public void ScriptsApplyCompare ()
	{
		System.Diagnostics.Process p = new System.Diagnostics.Process();	
		p = System.Diagnostics.Process.Start("./script.sh");
		p.WaitForExit();
		p.Close();
	
		System.Diagnostics.Process d = new System.Diagnostics.Process();	
		d = System.Diagnostics.Process.Start("rm script.sh");
		d.WaitForExit();
		d.Close();
		
		System.Diagnostics.Process c = new System.Diagnostics.Process();	
		c = System.Diagnostics.Process.Start("./scriptcomp.sh");
		c.WaitForExit();
		c.Close();	
		
		System.Diagnostics.Process dl = new System.Diagnostics.Process();	
		dl = System.Diagnostics.Process.Start("rm scriptcomp.sh");
		dl.WaitForExit();
		dl.Close();		    
	}

	public void CrearFicheroMutante(string bpelPath, string outputDir)
	{
		string wmutantes = System.IO.Path.Combine(outputDir, "mutantes-generados1_" + sbNtest.Text + "_0.00_0.00.dat");
		string rmutantes = System.IO.Path.Combine(outputDir, "mutantes-generados-aux.dat");
		string bpelName = System.IO.Path.GetFileNameWithoutExtension(bpelPath);

		string fichero = "";
		string linea = "";
		string lineasalida = "";
		string operador = "";
		string instruccion = "";
		string atributo = "";
		string estado = "";
		string test = "";
		
		System.IO.StreamWriter wm = new System.IO.StreamWriter(wmutantes);
		System.IO.StreamReader rm = new System.IO.StreamReader(rmutantes);
		
		while ((linea = rm.ReadLine()) != null) {
			
			operador    = LeerOperador(linea);
			instruccion = LeerInstruccion(linea);
			atributo    = LeerAtributo(linea);
			
			// creamos el fichero a leer
			
			fichero = System.IO.Path.Combine(System.IO.Path.Combine(outputDir, "Salida"),
			                                 "salida" + bpelName + operador + instruccion + atributo + ".out");

			System.IO.StreamReader sr = new System.IO.StreamReader(fichero);
			lineasalida = sr.ReadLine();
			estado = EstadoMutante(lineasalida);
			test = CasoTest(lineasalida);
			sr.Close();
			
			string lineaNueva = "0:0:0:" + linea + "ESTADO: " + estado + "\t" + test;
			wm.WriteLine(lineaNueva);
		}			
		
		rm.Close();
		wm.Close();
		
		System.Diagnostics.Process b = new System.Diagnostics.Process();	
		//b = System.Diagnostics.Process.Start("rm " + rmutantes);
		b.WaitForExit();
		b.Close();	
	}
	
	public string LeerOperador(string linea)
	{
		string op = "";

		string delimitaStr = " \t";
		char [] delimita = delimitaStr.ToCharArray();
		
		string[] split = linea.Split(delimita);
		string operador = split[0];
		int num = Convert.ToInt32(operador);
		
		switch(num){
		case 1:
			op = "ISV";
			break;
		case 2: 
			op = "EAA";
		    break;
		case 3:
			op = "EEU";
			break;
		case 4:
			op = "ERR";
			break;
		case 5:
			op = "ELL";
			break;
		case 6:
			op = "ECC";
			break;
		case 7:
			op = "ECN";
			break;
		case 8:
			op = "EMD";
			break;
		case 9:
			op = "EMF";
			break;
		case 10:
			op = "ACI";
			break;
		case 11:
			op = "AFP";
			break;
		case 12:
			op = "ASF";
			break;
		case 13:
			op = "AIS";
			break;
		case 14:
			op = "AIE";
			break;
		case 15:
			op = "AWR";
			break;
		case 16:
			op = "AJC";
			break;
		case 17:
			op = "ASI";
			break;
		case 18:
			op = "APM";
			break;
		case 19:
			op = "APA";
			break;
		case 20:
			op = "XMF";
			break;
		case 21:
			op = "XRF";
			break;
		case 22:
			op = "XMC";
			break;
		case 23:
			op = "XMT";
			break;
		case 24:
			op = "XTF";
			break;
		case 25:
			op = "XER";
			break;
		case 26:
			op = "XEE";
			break;
		}

		return op;
	}
	
	
	public string LeerInstruccion(string linea)
	{
		string delimitaStr = " \t";
		char [] delimita = delimitaStr.ToCharArray();
		
		string[] split = linea.Split(delimita);
		string instruccion = split[1];
		
		return instruccion;
	}	
	
	
	public string LeerAtributo(string linea)
	{
		string delimitaStr = " \t";
		char [] delimita = delimitaStr.ToCharArray();
		
		string[] split = linea.Split(delimita);
		string atributo = split[2];
		return atributo;
	}	
	
	
	public string EstadoMutante(string lineasalida)
	{
		string estado = "";

		if (lineasalida.Contains("2")){
		    estado = "2";
		}
		else if (!lineasalida.Contains("1")){
			estado = "0";
		}
		else {
			estado = "1";
		}
		return estado;
	}
	
	public string CasoTest(string lineasalida)
	{
		string test = "";

		if (lineasalida.Contains("2")){
		    test = "-1";
		}
		else if (!lineasalida.Contains("1")){
			test = "-1";
		}
		else {
			int posicion = lineasalida.IndexOf("1");
			posicion = (posicion / 2 ) + 1;
			test = posicion.ToString();
		}
		
		return test;
	}
	
	public bool Activado(int contadorOp, string analysisMutPath, string analysisOrigPath)
	{				
		bool res = false;
				
		// Hay que comprobar que existe el fichero.operadores, 
		// en caso de no existir se utilizará el fichero.operadores.original
		
		if (System.IO.File.Exists(analysisMutPath)){
			
			res = BuscarOperador(analysisMutPath, contadorOp);
		}
		else if (System.IO.File.Exists(analysisOrigPath)){
			
			res = BuscarOperador(analysisOrigPath, contadorOp);
		}
		
		return res;
	}
	
	public bool BuscarOperador(string ruta, int contadorOp)
	{
		
		System.IO.StreamReader sr = new System.IO.StreamReader(ruta);
		string linea;
		string delimitaStr = " \t";
		string[] split;
		string op = "";
		char [] delimita = delimitaStr.ToCharArray();
		bool res = false;
		
		while ((linea = sr.ReadLine()) != null) {
			
			switch(contadorOp) {
			
			case 1:
				if (linea.Contains("ISV")){
					
					split = linea.Split(delimita);
					op = split[1];
										
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 2: 
				if (linea.Contains("EAA")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
			    break;
			case 3:
				if (linea.Contains("EUU")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 4:
				if (linea.Contains("ERR")){
					
					split = linea.Split(delimita);
					op = split[1];
										
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 5:
				if (linea.Contains("ELL")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 6:
				if (linea.Contains("ECC")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 7:
				if (linea.Contains("ECN")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 8:
				if (linea.Contains("EMD")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 9:
				if (linea.Contains("EMF")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 10:
				if (linea.Contains("ACI")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 11:
				if (linea.Contains("AFP")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 12:
				if (linea.Contains("ASF")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 13:
				if (linea.Contains("ASI")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 14:
				if (linea.Contains("AIE")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 15:
				if (linea.Contains("AWR")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 16:
				if (linea.Contains("AJC")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 17:
				if (linea.Contains("ASI")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 18:
				if (linea.Contains("APM")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 19:
				if (linea.Contains("APA")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 20:
				if (linea.Contains("XMF")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 21:
				if (linea.Contains("XRF")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 22:
				if (linea.Contains("XMC")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 23:
				if (linea.Contains("XMT")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 24:
				if (linea.Contains("XTF")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 25:
				if (linea.Contains("XER")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			case 26:
				if (linea.Contains("XEE")){
					
					split = linea.Split(delimita);
					op = split[1];
					
					if (op.Contains("0"))
						res = false;
					else
						res = true;
				}
				break;
			}
		}
		return res;
	}
	
	public double CalcularSegundos(string inicio, string fin)
	{
		string delimitaStr = ":";
		char [] delimita = delimitaStr.ToCharArray();
				
		string[] splitf = fin.Split(delimita);
		string Fhoras = splitf[0];
		string Fminutos = splitf[1];
		string Fsegundos = splitf[2];
		
		double horasF = Convert.ToDouble(Fhoras);
		double minutosF = Convert.ToDouble(Fminutos);
		double segundosF = Convert.ToDouble(Fsegundos);
		segundosF = segundosF / 10000000;
		
		string[] spliti = inicio.Split(delimita);
		string Ihoras = spliti[0];
		string Iminutos = spliti[1];
		string Isegundos = spliti[2];
		
		double horasI = Convert.ToDouble(Ihoras);
		double minutosI = Convert.ToDouble(Iminutos);
		double segundosI = Convert.ToDouble(Isegundos);
		segundosI = segundosI / 10000000;
		
		double tiempoI = 0.0;
		double tiempoF = 0.0;
		
		tiempoI = (horasI * 3600) + (minutosI * 60) + segundosI;
		tiempoF = (horasF * 3600) + (minutosF * 60) + segundosF;
		
		double tiempoT = tiempoF - tiempoI;
			
		return tiempoT;
	}
	
	public void ScriptGamera ()
  	{	
		System.Diagnostics.Process p = new System.Diagnostics.Process();
		p = System.Diagnostics.Process.Start("./script.sh");
		p.WaitForExit();
		p.Close();
		
		System.Diagnostics.Process d = new System.Diagnostics.Process();	
		d = System.Diagnostics.Process.Start("rm script.sh");
		d.WaitForExit();
		d.Close();
	}
	
	public string DeComaAPunto(string cadena)
	{
		string numero = "";
		
		if (cadena.Length > 1){
				
			string delimitaStr = ",";
			char [] delimita = delimitaStr.ToCharArray();
				
			string[] split = cadena.Split(delimita);
			string Pentera = split[0];
			string Pdecimal = split[1]; 
		
			numero = Pentera + "." + Pdecimal;
		}
		else{
			numero = cadena;
		}
		
		return numero;
	}
	
	
	
	// Métodos de la pestaña comparativas mutantes
	

	public void Filtro(string file, string ruta)
	{
		System.Diagnostics.Process c = new System.Diagnostics.Process();	
		c = System.Diagnostics.Process.Start("touch " + ruta + "aux.bpel");
		c.WaitForExit();
		c.Close();
		
		string fileaux = ruta + "aux.bpel";
		System.IO.StreamReader srf = new System.IO.StreamReader(file);
		System.IO.StreamWriter swf = new System.IO.StreamWriter(fileaux);
		
		string linea = "";
		
		while ((linea = srf.ReadLine()) != null){
			if (linea.Contains("&gt;")){
				string correcta = linea.Replace("&gt;", ">");
				swf.WriteLine(correcta);
			}
			else
				swf.WriteLine(linea);
		}
		
		srf.Close();
		swf.Close();
		
		System.Diagnostics.Process m = new System.Diagnostics.Process();	
		m = System.Diagnostics.Process.Start("mv " + fileaux + " " + file);
		m.WaitForExit();
		m.Close();
	}
	
	public string NumOperador(string op)
	{
		string fich = Directorychooser3.Filename.ToString() + "/" + SeleccionarNombre(Filechooser3.Filename.ToString()) + ".bpel.operadores_original";
		int cont = 0;
		string res = "";
		System.IO.StreamReader rf = new System.IO.StreamReader(fich);
		string linea = "";
		
		while ((linea = rf.ReadLine()) != null){
			cont++;
			if (linea.Contains(op))
				res = cont.ToString();
		}
		
		return res;
	}
	
	public void AplicarEstiloRojo(TextBuffer buffer, string style, string fich)
	{
		
		TextTag tagG = new TextTag ("r");
		tagG.Background = "#FF0000";	
		buffer.TagTable.Add (tagG);
		
		TextIter beginIter, endIter;
		
		int contR = CuentaEtiquetas(fich, "#r");
				
		while (contR != 0){
			
			int begin = Inicio(style, fich);
			int end = begin + 1; 
	
			beginIter = buffer.GetIterAtLine(begin);
			endIter = buffer.GetIterAtLine(end); 
			buffer.ApplyTag (style, beginIter, endIter);
			EliminaEtiqueta(fich, "#r");
			contR = CuentaEtiquetas(fich, "#r");
		}
	}
	
	public void AplicarEstiloVerde(TextBuffer buffer, string style, string fich)
	{		
		TextTag tagR  = new TextTag ("v");
		tagR.Background = "#00BB00";
		buffer.TagTable.Add (tagR);
		
		TextIter beginIter, endIter;
		
		int contV = CuentaEtiquetas(fich, "#v");
		
		while (contV != 0){
			
			int begin = Inicio(style, fich);
			int end = begin + 1; 
	
			beginIter = buffer.GetIterAtLine(begin);
			endIter = buffer.GetIterAtLine(end); 
			buffer.ApplyTag (style, beginIter, endIter);
			EliminaEtiqueta(fich, "#v");
			contV = CuentaEtiquetas(fich, "#v");
		}
	}
	
	public int CuentaEtiquetas(string fich, string etiq)
	{
		int cont = 0;
		string linea = "";
		
		System.IO.StreamReader sr = new System.IO.StreamReader(fich);
		
		while ((linea = sr.ReadLine()) != null){
			if (linea.Contains(etiq))
				cont++;
		}
		
		sr.Close();
		return cont;
	}
	
	public void EliminaEtiqueta(string fich, string etiqueta)
	{
		string linea = "";
		string eliminada = "";
		string fichAux = fich + "aux";
		bool eliminado = false;
		System.IO.StreamReader sr = new System.IO.StreamReader(fich);
		System.IO.StreamWriter sw = new System.IO.StreamWriter(fichAux);
		
		while ((linea = sr.ReadLine()) != null) {	
			
			if (linea.Contains(etiqueta) && !eliminado){
				eliminada = linea.Substring(2);
				sw.WriteLine(eliminada);
				eliminado = true;
			}
			else {
				sw.WriteLine(linea);
			}
		}		
			
		sw.Close();
		sr.Close();		
		
		System.Diagnostics.Process m = new System.Diagnostics.Process();	
		m = System.Diagnostics.Process.Start("mv " + fichAux + " " + fich);
		m.WaitForExit();
		m.Close();
	}
	
	public int Inicio(string style, string fich)
	{
		int cont = 0;
		int inicio = 0;
		System.IO.StreamReader sr = new System.IO.StreamReader(fich);
		string linea = "";
		string etiqueta = "#" + style;
		
		do {	
			cont++;
			linea = sr.ReadLine();
			if (linea.Contains(etiqueta))
				inicio = cont - 1;
		} while ((linea != null) && (!linea.Contains(etiqueta)));
		
		sr.Close();
		return inicio;
	}

	protected virtual void OnCbbEstadoChanged (object sender, System.EventArgs e)
	{
		
		if (cbbEstado.Active != 0){
			string fichdatos = Directorychooser3.Filename.ToString();
			string res = "";
			
			ListStore listacombo = new ListStore(typeof(string));
			cbbOperador.Model = listacombo;
			listacombo.Clear();
			
			res = LeerFichero(fichdatos);
			
			string fichMutGen = System.IO.Path.Combine(Directorychooser3.Filename, "mutantes-generados" + res + ".dat");
			System.IO.StreamReader mut = new System.IO.StreamReader(fichMutGen);
			string linea = "";
			string lista = "";
			
			/* Después de abrir el fichero mutantes-generados que contiene la información 
			 * sobre los estados de los mutantes, tenemos que ir leyendo la primera columna
			 * (para saber el mutante correspondiente) y la cuarta (para saber el estado).
			 * Según el estado de los mutantes, se escribirán en ccbOperador (refrescando según
			 * se cambie), hay que limpiar los estados */
			
			listacombo.AppendValues(" ");
			
			while ((linea = mut.ReadLine()) != null){
				string delimitaStr = ": \t";
				char [] delimita = delimitaStr.ToCharArray();
				Array c = linea.Split(delimita);
				string colum1 = c.GetValue(3).ToString();
				string colum4 = c.GetValue(8).ToString();
				
				
				if (cbbEstado.Active == 1){
					if (colum4 == "0"){
						string NomMut = LeerOperador(colum1);
						
						if (!lista.Contains(NomMut)){
							listacombo.AppendValues(NomMut);
							lista += " " + NomMut;
						}
					}
				}
				
				if (cbbEstado.Active == 2){
					if (colum4 == "1"){
						string NomMut = LeerOperador(colum1);
						
						if (!lista.Contains(NomMut)){
							listacombo.AppendValues(NomMut);
							lista += " " + NomMut;
						}
					}
				}
				
				if (cbbEstado.Active == 3){
					if (colum4 == "2"){
						string NomMut = LeerOperador(colum1);
						
						if (!lista.Contains(NomMut)){
							listacombo.AppendValues(NomMut);
							lista += " " + NomMut;
						}
					}
				}
			}
			
			lista = "";
		}
		else{
			
			ListStore listacomboOp = new ListStore(typeof(string));
			cbbOperador.Model = listacomboOp;
			listacomboOp.Clear();
			
			ListStore listacomboD = new ListStore(typeof(string));
			cbbDetalles.Model = listacomboD;
			listacomboD.Clear();
			
			TextBuffer bufferOrig;
			bufferOrig = textviewOrig.Buffer;
			bufferOrig.Clear();
			
			TextBuffer bufferMut;
			bufferMut = textviewMut.Buffer;
			bufferMut.Clear();
		}			
	}

	protected virtual void OnCbbOperadorChanged (object sender, System.EventArgs e)
	{
		string fichdatos = Directorychooser3.Filename.ToString();
		string res = "";
		
		ListStore listacombo = new ListStore(typeof(string));
		cbbDetalles.Model = listacombo;
		listacombo.Clear();
		
		res = LeerFichero(fichdatos);
		
		string fichMutGen = Directorychooser3.Filename.ToString();
		fichMutGen += "/mutantes-generados" + res + ".dat";
		System.IO.StreamReader mut = new System.IO.StreamReader(fichMutGen);
		string linea = "";
		
		/* Después de abrir el fichero mutantes-generados que contiene la información 
		 * sobre los estados de los mutantes, tenemos que ir leyendo la primera columna
		 * (para saber el mutante correspondiente) y la cuarta (para saber el estado).
		 * Según el estado de los mutantes, se escribirán en ccbOperador (refrescando según
		 * se cambie), hay que limpiar los estados */
				
		listacombo.AppendValues(" ");
		
		while ((linea = mut.ReadLine()) != null){
			string delimitaStr = ": \t";
			char [] delimita = delimitaStr.ToCharArray();
			Array c = linea.Split(delimita);
			string colum1 = c.GetValue(3).ToString();
			string colum2 = c.GetValue(4).ToString();
			string colum3 = c.GetValue(5).ToString();
			string colum4 = c.GetValue(8).ToString();
			
			string NomMut = LeerOperador(colum1);
			
			if (cbbOperador.ActiveText == NomMut){
				
				if (cbbEstado.Active == 1){
					if (colum4 == "0"){
						string Detalles = colum2 + "-" + colum3;
						listacombo.AppendValues(Detalles);
					}
				}
				
				if (cbbEstado.Active == 2){
					if (colum4 == "1"){
						string Detalles = colum2 + "-" + colum3;
						listacombo.AppendValues(Detalles);
					}
				}
				
				if (cbbEstado.Active == 3){
					if (colum4 == "2"){
						string Detalles = colum2 + "-" + colum3;
						listacombo.AppendValues(Detalles);
					}
				}
			}
		}
	}

	
	
	// Métodos de la pestaña de resultados
	
	public void VentanaFaltaOpr()
	{
		pdata = new ProgressData();
		pdata.activity_mode = true;
		pdata.window = new Gtk.Window(Gtk.WindowType.Toplevel);
	    pdata.window.Move(150, 200);
		pdata.window.Resizable = true;

		pdata.window.Title = "Aviso Koura";
		pdata.window.BorderWidth = 0;
	
		VBox vbox = new Gtk.VBox(false, 5);
		vbox.BorderWidth = 10;
		pdata.window.Add(vbox);
		vbox.Show();
		
		HBox hbox = new HBox(false, 2);
		hbox.BorderWidth = 6;
		vbox.PackStart(hbox);
		
		Image img = new Image("img/advertencia.jpg");
		hbox.PackStart(img,false,false,2);
		img.Show();
		
		Label l = new Label();
		l.MaxWidthChars = 40;
		l.Markup = "No se ha seleccionado correctamente el operador\na comparar, vuelva al listado para completar el\nmutante: Operador y Detalles";
		hbox.PackStart(l,false,false,0);
	
		Button button = new Gtk.Button("cerrar");
		button.Clicked += button_click;
		vbox.PackStart(button, false, false, 0);

		button.CanDefault = true;
	
		button.GrabDefault();
		button.Show();
	
		pdata.window.ShowAll();
	}
	
	public void VentanaFaltaDatos()
	{
		pdata = new ProgressData();
		pdata.activity_mode = true;
		pdata.window = new Gtk.Window(Gtk.WindowType.Toplevel);
	    pdata.window.Move(150, 200);
		pdata.window.Resizable = true;

		pdata.window.Title = "Aviso Koura";
		pdata.window.BorderWidth = 0;
	
		VBox vbox = new Gtk.VBox(false, 5);
		vbox.BorderWidth = 10;
		pdata.window.Add(vbox);
		vbox.Show();
		
		HBox hbox = new HBox(false, 2);
		hbox.BorderWidth = 6;
		vbox.PackStart(hbox);
		
		Image img = new Image("img/advertencia.jpg");
		hbox.PackStart(img,false,false,2);
		img.Show();
		
		Label l = new Label();
		l.MaxWidthChars = 40;
		l.Markup = "No se han generado mutantes en este\ndirectorio de trabajo, para ello en la\npestaña \"Generación y ejecución de\nmutantes\" seleccionaremos el método\npara efectuar dichas acciones";
		hbox.PackStart(l,false,false,0);
	
		Button button = new Gtk.Button("cerrar");
		button.Clicked += button_click;
		vbox.PackStart(button, false, false, 0);

		button.CanDefault = true;
	
		button.GrabDefault();
		button.Show();
	
		pdata.window.ShowAll();
	}

	public int Cuentas(string directory, string file)
	{
        string fileWork = System.IO.Path.Combine(directory, file + ".bpel.operadores_original");
        System.IO.StreamReader sr = new System.IO.StreamReader(fileWork);
        int cont = 0;
        int contOp = 1;
        string Operador = "";
        string delimitaStr = " \t";
        char [] delimita = delimitaStr.ToCharArray();

        while (contOp != 26){  // Leemos todos los operadores
                
                if ((Operador = sr.ReadLine()) != null){ // Leemos del fichero resultante

                        // Leemos la instrucción y la convertimos
                        Array op = Operador.Split(delimita);
                        string instF = op.GetValue(1).ToString();
                        int instruccionF = Convert.ToInt32(instF);
                        
                        // Leemos los atributos del operador y lo convertimos
                        string atrF = op.GetValue(2).ToString();
                        int atributoF = Convert.ToInt32(atrF);

                        // Con esto ya sabemos el número de mutantes totales                                    
                        cont = cont + (instruccionF * atributoF);       
                }
                contOp++;
        }
        
        return cont;
    }
	
	public string TablaTest(string directory, string filem)
	{
		string filemuta = System.IO.Path.Combine(directory, filem);
		string delimita = " \t";
		char [] delimitaM = delimita.ToCharArray();
		string linea = "";
		string cabecera = "Número del Test  ";
		string resto = "Mutantes muertos ";
		string parrafo = "";
		int contador = 0;
		int test = 0;
		int maxtext;
		
		maxtext = TextMaximo(filemuta);
				
		while (test <= maxtext){
			
			System.IO.StreamReader sr = new System.IO.StreamReader(filemuta);
			
			while ((linea = sr.ReadLine()) != null){
				Array t = linea.Split(delimitaM);
				string numt = t.GetValue(4).ToString();
				int numtest = Convert.ToInt32(numt);
				
				if (numtest == test)
					contador++;
			}
			
			cabecera = cabecera + "  |  " + test;
			resto = resto + "  |  " + contador;
			sr.Close();
			contador = 0;
			test++;
		}
		cabecera = cabecera + "\n";
		resto = resto + "\n";
		parrafo = cabecera + resto;
		
		return parrafo;
	}
	
	public int TextMaximo(string filemuta)
	{
		string linea = "";
		string delimita = " \t";
		char [] delimitaM = delimita.ToCharArray();
		int max = -1;
		
		System.IO.StreamReader sr = new System.IO.StreamReader(filemuta);
			
		while ((linea = sr.ReadLine()) != null){
			Array t = linea.Split(delimitaM);
			string numt = t.GetValue(4).ToString();
			int numtest = Convert.ToInt32(numt);
			
			if (numtest > max)
				max = numtest;
		}
		
		sr.Close();
		return max;
	}
	
	public string Tabla(string directory, string file, string filem)
	{
		string fileWork = System.IO.Path.Combine(directory, file + ".bpel.operadores");
		string filemuta = System.IO.Path.Combine(directory, filem);
		System.IO.StreamReader sr = new System.IO.StreamReader(fileWork);
		
		string Operador = "";
		string delimitaStr = ": \t";
		string delimitaStrM = ": \t";
		char [] delimita = delimitaStr.ToCharArray();
		char [] delimitaM = delimitaStrM.ToCharArray();
		string parrafo = "";
		string linealect = "";
		string linea = "Opr    ";
		string lvivos = "Viv   ";
		string lmuertos = "Mts   ";
		string lerror = "Err   ";
		string lgenerados = "Gen   ";
		int contOp = 1;
		int vivos = 0;
		int muertos = 0;
		int error = 0;
		int generados = 0;		
		
		while ((Operador = sr.ReadLine()) != null){ // Leemos del fichero resultante

				// Leemos la instrucción y la convertimos
				Array op = Operador.Split(delimita);
				string instF = op.GetValue(1).ToString();
				int instruccionF = Convert.ToInt32(instF);
			
				if (instruccionF != 0){
					// Leemos los atributos del operador y lo convertimos
					string atrF = op.GetValue(0).ToString();
					linea = linea + "  |  " + atrF;
					System.IO.StreamReader srm = new System.IO.StreamReader(filemuta);
				
					while((linealect = srm.ReadLine()) != null){
						Array l = linealect.Split(delimitaM);
						string col1 = l.GetValue(3).ToString();
						string col2 = l.GetValue(8).ToString();
						int num = Convert.ToInt32(col1);
						int estado = Convert.ToInt32(col2);
						
						if(contOp == num){
							generados++;
							switch(estado){
							case 0:
								vivos++;
								break;
							case 1:
								muertos++;
								break;
							default:
								error++;
								break;
							}
						}
					}
					
					srm.Close();
					lvivos = lvivos + "   |   " + vivos.ToString();
					lmuertos = lmuertos + "   |   " + muertos.ToString();
					lerror = lerror + "   |   " + error.ToString();
					lgenerados = lgenerados + "   |   " + generados.ToString();
				
					vivos = 0;
					muertos = 0;
					error = 0;
					generados = 0;
				}
			contOp++;
		}
		sr.Close();
		linea = linea + "\n";
		lvivos = lvivos + "\n";
		lmuertos = lmuertos + "\n";
		lerror = lerror + "\n";
		lgenerados = lgenerados + "\n";
		parrafo = linea + lvivos + lmuertos + lerror + lgenerados;
		
		return parrafo;
	}
	
	public void CrearFicheroXml(string XmlFile, string tablas)
	{
		
		string delimitaStr = "|";
		char [] delimita = delimitaStr.ToCharArray();
		
		System.IO.StreamReader reader = new System.IO.StreamReader(tablas);
		System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(XmlFile, System.Text.Encoding.UTF8);
			
		// Usa indentación por legibilidad
		writer.Formatting = System.Xml.Formatting.Indented;
		// Escribe la declaración del XML
		writer.WriteStartDocument();
		
		// Escribe el elemento raiz
		writer.WriteStartElement("Ejecucion");
		
		writer.WriteStartElement("Operadores");
		
		// Vamos a crear un xml en el que se muestran los resultados de cada operador
		
		string linea1 = "";
		string linea2 = ""; 
		string linea3 = ""; 
		string linea4 = "";
		string linea5 = "";
		
		linea1 = reader.ReadLine();
		linea2 = reader.ReadLine();
		linea3 = reader.ReadLine();
		linea4 = reader.ReadLine();
		linea5 = reader.ReadLine();
		
		Array op = linea1.Split(delimita);
		Array vi = linea2.Split(delimita);
		Array mu = linea3.Split(delimita);
		Array er = linea4.Split(delimita);
		Array gn = linea5.Split(delimita);
		
		int colum = ContarElementos(linea1);
		
		for (int cont = 1; cont <= colum + 1; cont++){
			
			writer.WriteStartElement("Operador");
			
			string valorOp = op.GetValue(cont).ToString();
			writer.WriteElementString("Nombre", valorOp.Trim());
			
			string valorGn = gn.GetValue(cont).ToString();
			writer.WriteElementString("Generados", valorGn.Trim());
			
			string valorVi = vi.GetValue(cont).ToString();
			writer.WriteElementString("Vivos", valorVi.Trim());
			
			string valorMu = mu.GetValue(cont).ToString();
			writer.WriteElementString("Muertos", valorMu.Trim());
			
			string valorEr = er.GetValue(cont).ToString();
			writer.WriteElementString("Error", valorEr.Trim());
			
			writer.WriteEndElement();
		}
		
		writer.WriteEndElement();
		
		// Se finaliza la lectura de la primera tabla
		
		do { 
			linea1 = reader.ReadLine();
		} while (linea1 == "");
		
		linea2 = reader.ReadLine();
		
		// Escribe el elemento raiz segunda tabla
		
		writer.WriteStartElement("Tests");
		
		op = linea1.Split(delimita);
		mu = linea2.Split(delimita);
		colum = ContarElementos(linea1);
			
		for (int cont = 1; cont <= colum + 1; cont++) {
			
			writer.WriteStartElement("Test");
			
			string valor = op.GetValue(cont).ToString();		
			writer.WriteElementString("NumTest", valor.Trim());
			
			string valorMu = mu.GetValue(cont).ToString();
			writer.WriteElementString("MutMuertos", valorMu.Trim());
			
			writer.WriteEndElement();
		}
		
		writer.WriteEndElement();
		
		writer.WriteElementString("Tiempo", EntryTiempo.Text);
		
		writer.WriteStartElement("Mutantes");
		writer.WriteElementString("Total", EntryMutTotal.Text);
		writer.WriteElementString("MGenerados", EntryMutGen.Text);
		writer.WriteElementString("MVivos", EntryVivos.Text);
		writer.WriteElementString("MMuertos", EntryMuertos.Text);
		writer.WriteElementString("MErrores", EntryError.Text);
		writer.WriteEndElement();
		writer.WriteEndElement();
		
		reader.Close();
		writer.Flush();
     	writer.Close();
	}
	
	public int ContarElementos(string linea)
	{
		int num = 0;		
		string delimitaStr = "|";
		char [] delimita = delimitaStr.ToCharArray();
		Array c = linea.Split(delimita);
		string subc = c.GetValue(0).ToString();
		int l = subc.Length + 1;
		linea = linea.Remove(0, l);

		
		while (linea.Contains("|")){
			Array t = linea.Split(delimita);
			subc = t.GetValue(0).ToString();
			int longitud = subc.Length + 1;
			linea = linea.Remove(0, longitud);

			num ++;
		}
				
		return num;
	}
	
	public string  LeerFichero(string ruta)
	{
		string file = ruta + "/datos.dat";
		string linea;
		string res = "";
		
		System.IO.StreamReader sr = new System.IO.StreamReader(file);
		
		while((linea = sr.ReadLine()) != null) {
			if (linea.Contains("valores")){
				linea = sr.ReadLine();
				res = linea;
			}				
		}
		
		sr.Close();
		return res;	
	}
	
	public string LeerTiempo(string ruta)
	{
		string file = ruta + "/datos.dat";
		string linea;
		string res = "";
		
		System.IO.StreamReader sr = new System.IO.StreamReader(file);
		
		while((linea = sr.ReadLine()) != null) {
			if (linea.Contains("tiempo")){
				linea = sr.ReadLine();
				res = linea.Substring(0,6);
			}				
		}
		
		sr.Close();
		return res;				 
	}
	
	public void ControldeBotones()
	{
		string ruta = Directorychooser4.Filename + "/datos.dat";
		string linea = "";
		
		System.IO.StreamReader srd = new System.IO.StreamReader(ruta);
		
		linea = srd.ReadLine();
		srd.Close();
		
		if (linea.Contains("AG")){
			btnVisualizarMutantes.Visible = true;
			btnVisualizarEstadisticas.Visible = true;
		}
		else{
			btnVisualizarMutantes.Visible = false;
			btnVisualizarEstadisticas.Visible = false;
		}
		
	}
	
	//Esta Funcion permite Leer un Archivo XML
     public void Leer(string XmlFile, string tablas)
     {
		
		TextBuffer buffer;
		buffer = Textview.Buffer;
		
		//Declaramos un lector para el XML indicando el nombre de este
       StreamReader reader = new StreamReader (XmlFile, System.Text.Encoding.UTF8);                  

       //Indicamos cual es el nombre de lector XML a usar en este caso es reader
       System.Xml.XmlTextReader xml = new System.Xml.XmlTextReader(reader);
       xml.Namespaces = false;

       //Hacemos un ciclo para leer cada uno de los nodos del XML
             while (xml.Read ())
             {
            //Vamos a leer cada uno de los elementos que contiene el archivo XML
           
              switch (xml.NodeType)
              {
                     case System.Xml.XmlNodeType.Element:
                        switch (xml.Name)
                          {
                               case "Ejecucion":
									buffer.Text = "Resúmenes: Operadores y Test\n";
                                    break;
                               case "Operadores": 
									buffer.Text += "Resumen de los operadores";
                                     break;
							   case "Tests": 
									buffer.Text += "\n\nResumen de los Test";
                                     break;
                               case "operador":
								    buffer.Text += "\n";
                                    break;
                               case "Nombre":
                                      buffer.Text += "\n\nOperador - " + xml.ReadString ();
                                       break;
                               case "Vivos":
                                      buffer.Text += "\nMutantes vivos: " + xml.ReadString();
                                       break;
							   case "Muertos":
                                      buffer.Text += "\nMutantes muertos: " + xml.ReadString();
                                       break;
							   case "Generados":
                                      buffer.Text += "\nMutantes generados: " + xml.ReadString();
                                       break;
							   case "Error":
                                      buffer.Text += "\nMutantes erróneos: " + xml.ReadString();
                                       break;
							   case "NumTest":
                                      buffer.Text += "\nTest número - " + xml.ReadString();
                                       break;
							   case "MutMuertos":
                                      buffer.Text += "\nMutantes que ha matado: " + xml.ReadString();
                                       break;
							   case "Test":
                                      buffer.Text += "\n";
                                       break;
                           }
                       break;
                 }
             }

             //Cerramos el lector de XML
             xml.Close ();
  			XmlConEstilo(XmlFile, buffer);
    }

	public void XmlConEstilo(string XmlFile, TextBuffer buffer)
	{
		TextTag cabecera = new TextTag("Cabeceras");
		cabecera.Style = Pango.Style.Oblique;
		cabecera.Weight = Pango.Weight.Ultrabold;
		cabecera.SizePoints = 16.0;
		cabecera.Foreground = "#C0C0C0";
		cabecera.Underline = Pango.Underline.Single;
		buffer.TagTable.Add (cabecera);
		
		TextTag titulo = new TextTag("Titulo");
		titulo.Style = Pango.Style.Oblique;
		titulo.Weight = Pango.Weight.Light;
		titulo.SizePoints = 12.0;
		titulo.Foreground = "#000080";
		titulo.Underline = Pango.Underline.Low;
		buffer.TagTable.Add (titulo);
		
		TextTag nombre = new TextTag("Nombre");
		nombre.Weight = Pango.Weight.Heavy;
		nombre.SizePoints = 11.0;
		nombre.Foreground = "#FFA500";
		nombre.Stretch = Pango.Stretch.Expanded;
		buffer.TagTable.Add (nombre);
		
		TextIter beginIter, endIter;
		
		int begin = 0;
		int end = "Resúmenes: Operadores y Test".Length;
		int cont = 1;
		
		beginIter = buffer.GetIterAtOffset(begin);
		endIter = buffer.GetIterAtOffset(end);
		buffer.ApplyTag ("Cabeceras", beginIter, endIter);
		beginIter = buffer.GetIterAtLine(cont);
		endIter = buffer.GetIterAtLine(cont + 1);
		buffer.ApplyTag ("Titulo", beginIter, endIter);
		
		cont = cont + 2;
		
		int numOp = ContarOperadores(XmlFile);
		
		for (int op = 0; op != numOp; op++){
			beginIter = buffer.GetIterAtLine(cont);
			endIter = buffer.GetIterAtLine(cont + 1);
			buffer.ApplyTag("Nombre", beginIter, endIter);
			cont += 6;
		}
		
		
		beginIter = buffer.GetIterAtLine(cont);
		endIter = buffer.GetIterAtLine(cont + 1);
		buffer.ApplyTag("Titulo", beginIter, endIter);
		
	}
	
	public int ContarOperadores(string file)
	{
		int num = 0;
		System.IO.StreamReader sr = new System.IO.StreamReader(file);
		string linea = "";
		
		while ((linea = sr.ReadLine()) != null){
			if (linea.Contains("<Nombre>"))
				num++;
		}
		
		return num;
	}
	
	public int EncontrarEtiqueta(string etiqueta, string fich)
	{
		int cont = 0;
		int posicion = 0;
		System.IO.StreamReader sr = new System.IO.StreamReader(fich);
		string linea = "";
		
		while ((linea = sr.ReadLine()) != null){
			if (linea.Contains(etiqueta))
				posicion = cont;
			
			cont += linea.Length;
		}
		
		return posicion;
	}

	
	// Métodos comunes
	
	public void BarraTemporal()
	{
		pdata = new ProgressData();
		pdata.activity_mode = true;
		pdata.window = new Gtk.Window(Gtk.WindowType.Toplevel);
	    pdata.window.Move(150, 200);
		pdata.window.Resizable = true;

		pdata.window.Title = "Aviso Koura";
		pdata.window.BorderWidth = 0;
		
		VBox vbox = new Gtk.VBox(false, 5);
		vbox.BorderWidth = 10;
		pdata.window.Add(vbox);
		vbox.Show();
		
		Label l = new Label("Esta ventana se cerrará cuando finalice el proceso o haciendo clik en close");
		vbox.PackStart(l,false,false,0);
	
		Button button = new Gtk.Button("close");
		button.Clicked += button_click;		
		vbox.PackStart(button, false, false, 0);

		button.CanDefault = true;
	
		button.GrabDefault();
		button.Show();
		
		/* Create the GtkProgressBar */
		pdata.pbar = new Gtk.ProgressBar();
		pdata.pbar.Text = "En progreso...";
		vbox.PackStart(pdata.pbar, false, false, 0);
		pdata.pbar.Show();	
		/* Add a timer callback to update the value of the progress bar*/
		pdata.timer = GLib.Timeout.Add(100, new GLib.TimeoutHandler (progress_timeout) );

		pdata.window.ShowAll();
	}

	public string SeleccionarNombre(string ficheroRuta)
	{
		return System.IO.Path.GetFileNameWithoutExtension(ficheroRuta);
	}

	public string SeleccionarNombreMutante(string ficheroRuta)
	{
		string delimitaStr1 = "/";
		char [] delimita1 = delimitaStr1.ToCharArray();
				
		string[] split1 = ficheroRuta.Split(delimita1);
		int longd = split1.Length;
		string nombre = split1[longd-2];
		
		return nombre;
	}	
	
	static void button_click (object obj, EventArgs args)
	{
		pdata.window.Destroy();
	}

}