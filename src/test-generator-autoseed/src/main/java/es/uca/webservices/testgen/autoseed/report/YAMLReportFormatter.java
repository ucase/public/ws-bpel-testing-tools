package es.uca.webservices.testgen.autoseed.report;

import java.io.PrintStream;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;

import javax.xml.transform.TransformerException;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.constants.Constant;

/**
 * YAML Report Formatter
 * 
 * @author Valentín Liñeiro Barea
 */
public class YAMLReportFormatter extends AbstractReportFormatter {
	
	/// Indent level
	private int indentLevel = 0;
	
	/**
	 * Creates a YAML Report Formatter
	 * @param reportLevel
	 */
	public YAMLReportFormatter(ReportLevels reportLevel) {
		super(reportLevel);
	}

	@Override
	public void formatStart(PrintStream os) {
		os.println("# Automated Seeding Report");
		os.println("---");
	}

	@Override
	public void formatEnd(PrintStream os) throws TransformerException {
		os.println("...");
	}

	@Override
	public void formatInformationStart(PrintStream os) {
		os.println("info:");
		indentLevel++;
	}

	@Override
	public void formatInformationBPEL(String bpelPath, PrintStream os) {
		indent(os);
		os.println("bpel: " + bpelPath);
	}

	@Override
	public void formatInformationSpec(String specPath, PrintStream os) {
		indent(os);
		os.println("spec: " + specPath);
	}

	@Override
	public void formatInformationBPTS(String bptsPath, PrintStream os) {
		indent(os);
		os.println("bpts: " + bptsPath);
	}

	@Override
	public void formatInformationEnd(PrintStream os) {
		indentLevel--;
	}

	@Override
	public void formatConstantsStart(PrintStream os) {
		os.println("constants:");
		indentLevel++;
	}

	@Override
	public void formatConstantsElement(Entry<Constant, Set<String>> en,
			PrintStream os) {
		String typeClassName = en.getKey().getClass().getSimpleName();
		String typeName = typeClassName.substring(0, typeClassName.lastIndexOf('C'));
		indent(os);
		os.println("-");
		indentLevel++;
		indent(os);
		os.println("type: " + typeName);
		indent(os);
		os.println("value: " + en.getKey().getValue());
		indent(os);
		os.println("bpelVariables:");
		indentLevel++;
		for(String var: en.getValue()) {
			indent(os);
			os.println("- " + var);
		}
		indentLevel--;
		indentLevel--;
	}

	@Override
	public void formatConstantsEnd(PrintStream os) {
		indentLevel--;
	}

	@Override
	public void formatVariablesStart(PrintStream os) {
		os.println("variables:");
		indentLevel++;
	}

	@Override
	public void formatVariablesElement(IType var, PrintStream os) {
		String typeClassName = var.getClass().getSimpleName();
		String typeName = typeClassName.substring(typeClassName.indexOf('e') + 1);
		indent(os);
		os.println("-");
		indentLevel++;
		indent(os);
		os.println("type: " + typeName);
		indent(os);
		os.println("value: " + var.getNameVariable());
		indentLevel--;
	}

	@Override
	public void formatVariablesEnd(PrintStream os) {
		indentLevel--;
	}

	@Override
	public void formatMappingStart(PrintStream os) {
		os.println("mapping:");
		indentLevel++;
	}

	@Override
	public void formatMappingElement(Entry<String, Set<String>> en,
			PrintStream os) {
		indent(os);
		os.println("-");
		indentLevel++;
		indent(os);
		os.println("specVariable: " + en.getKey());
		indent(os);
		os.println("bpelVariables:");
		indentLevel++;
		for(String var: en.getValue()) {
			indent(os);
			os.println("- " + var);
		}
		indentLevel--;
		indentLevel--;
	}

	@Override
	public void formatMappingEnd(PrintStream os) {
		indentLevel--;
	}

	@Override
	public void formatValidVariablesStart(PrintStream os) {
		os.println("validVariables:");
		indentLevel++;
	}

	@Override
	public void formatValidVariablesElement(
			Entry<IType, Pair<SortedSet<String>, Set<Constant>>> en,
			PrintStream os) {
		String typeClassName = en.getKey().getClass().getSimpleName();
		String typeName = typeClassName.substring(typeClassName.indexOf('e') + 1);
		for(String var: en.getValue().getLeft()) {
			indent(os);
			os.println("-");
			indentLevel++;
			indent(os);
			os.println("type: " + typeName);
			indent(os);
			os.println("name: " + var);
			indent(os);
			os.println("constants:");
			indentLevel++;
			for(Constant cons: en.getValue().getRight()) {
				indent(os);
				os.println("- " + cons.getValue().toString());
			}
			indentLevel--;
			indentLevel--;
		}
	}

	@Override
	public void formatValidVariablesEnd(PrintStream os) {
		indentLevel--;
	}

	@Override
	public void formatNumberOfTestCasesStart(PrintStream os) {
		os.print("numberOfTestCases: ");
	}

	@Override
	public void formatNumberOfTestCasesElement(int numberOfTestCases,
			PrintStream os) {
		os.println(numberOfTestCases);
	}

	@Override
	public void formatNumberOfTestCasesEnd(PrintStream os) {
	}
	
	/**
	 * Indent exit
	 * @param n
	 * @param os
	 */
	private void indent(PrintStream os) {
		for(int i = 0; i < indentLevel; ++i) {
			os.print(" ");
		}
	}

}
