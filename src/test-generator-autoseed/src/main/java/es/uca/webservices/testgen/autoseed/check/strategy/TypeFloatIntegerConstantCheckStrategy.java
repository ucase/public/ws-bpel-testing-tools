package es.uca.webservices.testgen.autoseed.check.strategy;

import java.math.BigDecimal;

import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.autoseed.constants.IntegerConstant;

/**
 * Float (Integer value) check restrictions strategy
 * 
 * @author Valentín Liñeiro Barea
 */
public class TypeFloatIntegerConstantCheckStrategy implements ICheckStrategy {
	
	/// Type
	private TypeFloat typeFloat;
	
	/// Constant
	private IntegerConstant constFloat;
	
	/**
	 * Creates a float check strategy
	 * @param tF
	 * @param fC
	 */
	public TypeFloatIntegerConstantCheckStrategy(TypeFloat tF, IntegerConstant fC) {
		this.typeFloat = tF;
		this.constFloat = fC;
	}

	@Override
	public boolean checkRestrictions() {
		boolean ok = true;
		if (typeFloat.getAllowedValues().size() > 0
				&& !typeFloat.getAllowedValues().contains(constFloat.getValue())) {
			ok = false;
		}
		if (ok && typeFloat.getMinValue().compareTo(new BigDecimal(constFloat.getValue())) > 0) {
			ok = false;
		}
		if (ok && typeFloat.getMaxValue().compareTo(new BigDecimal(constFloat.getValue())) < 0) {
			ok = false;
		}
		return ok;
	}

}
