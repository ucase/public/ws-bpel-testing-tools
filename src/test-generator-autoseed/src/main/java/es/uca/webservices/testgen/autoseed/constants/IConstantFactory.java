package es.uca.webservices.testgen.autoseed.constants;

import javax.xml.datatype.DatatypeConfigurationException;

/**
 * Interface of constant factory
 * 
 * @author Valentín Liñeiro Barea
 */
public interface IConstantFactory {
	/**
	 * Returns a proper Constant object
	 * @param cons
	 * @return
	 * @throws DatatypeConfigurationException 
	 */
	Constant newConstant(String cons) throws DatatypeConfigurationException;
}
