package es.uca.webservices.testgen.autoseed.report;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.transform.TransformerException;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.utils.ValidVariablesMap;

/**
 * Abstract Report Formatter
 * 
 * @author Valentín Liñeiro Barea
 */
public abstract class AbstractReportFormatter implements IReportFormatter {
	
	/// Report Level
	private ReportLevels reportLevel;
	
	/**
	 * Initializes Report Level of formatter
	 * @param reportLevel
	 */
	public AbstractReportFormatter(ReportLevels reportLevel) {
		this.reportLevel = reportLevel;
	}

	@Override
	public void format(Report report, PrintStream os) throws TransformerException {
		formatStart(os);
		formatInformation(report, os);
		if(reportLevel.equals(ReportLevels.ADVANCED) ||
				reportLevel.equals(ReportLevels.FULL)) {
			formatConstants(report.getConstants(), os);
			formatVariables(report.getVariables(), os);
			if(reportLevel.equals(ReportLevels.FULL)) {
				formatMapping(report.getMapping(), os);
			}
		}
		formatValidVariables(report.getValidVariables(), os);
		formatEnd(os);
	}

	/**
	 * Format Start
	 * @param os
	 */
	public abstract void formatStart(PrintStream os);
	
	/**
	 * Format End
	 * @param os
	 * @throws TransformerConfigurationException 
	 * @throws TransformerException 
	 */
	public abstract void formatEnd(PrintStream os) throws TransformerException;

	/**
	 * Specific format for initial info
	 * @param report
	 * @param os
	 */
	public void formatInformation(Report report, PrintStream os) {
		formatInformationStart(os);
		formatInformationBPEL(report.getBpelPath(), os);
		formatInformationSpec(report.getSpecPath(), os);
		if(!"".equals(report.getBptsPath())) {
			formatInformationBPTS(report.getBptsPath(), os);
		}
		formatInformationEnd(os);
	}

	/**
	 * Format Information Start
	 * @param os
	 */
	public abstract void formatInformationStart(PrintStream os);
	
	/**
	 * Format Information BPEL
	 * @param bpelPath
	 * @param os
	 */
	public abstract void formatInformationBPEL(String bpelPath, PrintStream os);
	
	/**
	 * Format Information Spec
	 * @param specPath
	 * @param os
	 */
	public abstract void formatInformationSpec(String specPath, PrintStream os);
	
	/**
	 * Format Information BPTS
	 * @param bptsPath
	 * @param os
	 */
	public abstract void formatInformationBPTS(String bptsPath, PrintStream os);
	
	/**
	 * Format Information End
	 * @param os
	 */
	public abstract void formatInformationEnd(PrintStream os);

	/**
	 * Specific format for constants
	 * @param constants
	 * @param os
	 */
	public void formatConstants(Map<Constant, Set<String>> constants, PrintStream os) {
		formatConstantsStart(os);
		for(Entry<Constant, Set<String>> en: constants.entrySet()) {
			formatConstantsElement(en, os);
		}
		formatConstantsEnd(os);
	}

	/**
	 * Format Constants Start
	 * @param os
	 */
	public abstract void formatConstantsStart(PrintStream os);
	
	/**
	 * Format Constants Element
	 * @param en
	 * @param os
	 */
	public abstract void formatConstantsElement(Entry<Constant, Set<String>> en, PrintStream os);
	
	/**
	 * Format Constants End
	 * @param os
	 */
	public abstract void formatConstantsEnd(PrintStream os);

	/**
	 * Specific format for variables
	 * @param variables
	 * @param os
	 */
	public void formatVariables(List<IType> variables, PrintStream os) {
		formatVariablesStart(os);
		for(IType var: variables) {
			formatVariablesElement(var, os);
		}
		formatVariablesEnd(os);
	}

	/**
	 * Format Variables Start
	 * @param os
	 */
	public abstract void formatVariablesStart(PrintStream os);
	
	/**
	 * Format Variable
	 * @param var
	 * @param os 
	 */
	public abstract void formatVariablesElement(IType var, PrintStream os);
	
	/**
	 * Format Variable End
	 * @param os
	 */
	public abstract void formatVariablesEnd(PrintStream os);

	/**
	 * Specific format for mapping
	 * @param mapping
	 * @param os
	 */
	public void formatMapping(Map<String, Set<String>> mapping, PrintStream os) {
		if(mapping != null) {
			formatMappingStart(os);
			for(Entry<String, Set<String>> en: mapping.entrySet()) {
				formatMappingElement(en, os);
			}
			formatMappingEnd(os);
		}
	}

	/**
	 * Format Mapping Start
	 * @param os
	 */
	public abstract void formatMappingStart(PrintStream os);
	
	/**
	 * Format Mapping Element
	 * @param en
	 * @param os
	 */
	public abstract void formatMappingElement(Entry<String, Set<String>> en,
			PrintStream os);
	
	/**
	 * Format Mapping End
	 * @param os
	 */
	public abstract void formatMappingEnd(PrintStream os);

	/**
	 * Specific format for valid variables
	 * @param validVariables
	 * @param os
	 */
	public void formatValidVariables(ValidVariablesMap validVariables,
			PrintStream os) {
		formatValidVariablesStart(os);
		for(Entry<IType, Pair<SortedSet<String>, Set<Constant>>> en: validVariables.entrySet()) {
			formatValidVariablesElement(en, os);
		}
		formatValidVariablesEnd(os);
		formatNumberOfTestCasesStart(os);
		formatNumberOfTestCasesElement(validVariables.numberOfTestCases(), os);
		formatNumberOfTestCasesEnd(os);
	}	

	/**
	 * Format Valid Variables Start
	 * @param os
	 */
	public abstract void formatValidVariablesStart(PrintStream os);
	
	/**
	 * Format Valid Variables Element
	 * @param en
	 * @param os
	 */
	public abstract void formatValidVariablesElement(
			Entry<IType, Pair<SortedSet<String>, Set<Constant>>> en,
			PrintStream os);
	
	/**
	 * Format Valid Variables End
	 * @param os
	 */
	public abstract void formatValidVariablesEnd(PrintStream os);
	
	/**
	 * Format Number Of Test Cases Start
	 * @param os
	 */
	public abstract void formatNumberOfTestCasesStart(PrintStream os);
	
	/**
	 * Format Number Of Test Cases Element
	 * @param numberOfTestCases
	 * @param os
	 */
	public abstract void formatNumberOfTestCasesElement(int numberOfTestCases,
			PrintStream os);
	
	/**
	 * Format Number Of Test Cases End
	 * @param os
	 */
	public abstract void formatNumberOfTestCasesEnd(PrintStream os);

}
