package es.uca.webservices.testgen.autoseed.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.constants.Constant;

/**
 * Class in charge of save valid variables and constants
 * 
 * @author Valentín Liñeiro Barea
 */
public class ValidVariablesMap {
	
	/// Present number of test cases
	private int testCaseNumber = 0;
	
	/// Map of Type, Pair of Sets of Variables, Constants
	private Map<IType, Pair<SortedSet<String>, Set<Constant>>> dict;
	
	/**
	 * Creates a VariableConstantDictionary
	 */
	public ValidVariablesMap() {
		this.dict = new HashMap<IType, Pair<SortedSet<String>,Set<Constant>>>();
	}
	
	/**
	 * Adds a new variable and constant
	 * @param type
	 * @param cons
	 */
	public void put(IType type, Constant cons) {
		if(!dict.containsKey(type)) {
			dict.put(type, new Pair<SortedSet<String>, Set<Constant>>(
							new TreeSet<String>(), new HashSet<Constant>()));
		}
		
		int formerNumOfVars = dict.get(type).getLeft().size();
		int formerNumOfVals = dict.get(type).getRight().size();
		
		dict.get(type).getLeft().add(type.getNameVariable());
		dict.get(type).getRight().add(cons);
		
		int newNumOfVars = dict.get(type).getLeft().size();
		int newNumOfVals = dict.get(type).getRight().size();
				
		///Changing number of test cases
		testCaseNumber = testCaseNumber - (formerNumOfVars * formerNumOfVals) + 
				(newNumOfVars * newNumOfVals);
	}
	
	/**
	 * Calculates the number of test cases we need to generate
	 * @return
	 */
	public int numberOfTestCases() {
		return testCaseNumber;
	}
	
	/**
	 * Gets variables and constants without type
	 * @return
	 */
	public Collection<Pair<SortedSet<String>, Set<Constant>>> values() {
		return dict.values();
	}
	
	/**
	 * Gets entry set
	 * @return
	 */
	public Set<Entry<IType, Pair<SortedSet<String>, Set<Constant>>>> entrySet() {
		return dict.entrySet();
	}
	
	/**
	 * Calculates number of valid variables
	 * @return
	 */
	public int numberOfValidVariables() {
		int number = 0;
		for(Pair<SortedSet<String>, Set<Constant>> ent: dict.values()) {
			number += ent.getLeft().size();
		}
		return number;
	}
	
	/**
	 * Checks if dictionary is empty
	 * @return
	 */
	public boolean isEmpty() {
		return dict.isEmpty();
	}
}
