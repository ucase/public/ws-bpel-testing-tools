package es.uca.webservices.testgen.autoseed.report;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Report Formatter Factory Interface
 * 
 * @author Valentín Liñeiro Barea
 */
public interface IReportFormatterFactory {

	/**
	 * Creates proper formatter checking the format given
	 * @param format
	 * @param level
	 * @return
	 * @throws ParserConfigurationException 
	 */
	IReportFormatter newFormatter(String format, ReportLevels level) throws ParserConfigurationException;
	
}
