package es.uca.webservices.testgen.autoseed.reader.variables;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.generators.ITypeVisitor;
import es.uca.webservices.testgen.api.types.TypeDate;
import es.uca.webservices.testgen.api.types.TypeDateTime;
import es.uca.webservices.testgen.api.types.TypeDuration;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTime;
import es.uca.webservices.testgen.api.types.TypeTuple;

/**
 * Visitor for searching for inner variables in lists and tuples
 * 
 * @author Valentín Liñeiro Barea
 */
public class TypeVisitor implements ITypeVisitor {
	
	/// Logger
	private static final Logger LOGGER = LoggerFactory.getLogger(TypeVisitor.class);
	
	/**
	 * Returns simple type
	 * @param arg0
	 * @return
	 */
	private List<IType> simpleType(IType arg0) {
		List<IType> tL = new ArrayList<IType>();
		tL.add(arg0);
		return tL;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<IType> visit(IType arg0) throws GenerationException {
		LOGGER.debug("Accepting");
		return (List<IType>) arg0.accept(this);
	}

	@Override
	public List<IType> visit(TypeDate arg0) throws GenerationException {
		if(arg0.getNameVariable().endsWith(".")) {
			arg0.setNameVariable(arg0.getNameVariable().substring(0, arg0.getNameVariable().lastIndexOf('.')));
		}
		LOGGER.debug("Found Date: " + arg0.getNameVariable());
		return simpleType(arg0);
	}

	@Override
	public List<IType> visit(TypeDateTime arg0) throws GenerationException {
		if(arg0.getNameVariable().endsWith(".")) {
			arg0.setNameVariable(arg0.getNameVariable().substring(0, arg0.getNameVariable().lastIndexOf('.')));
		}
		LOGGER.debug("Found DateTime: " + arg0.getNameVariable());
		return simpleType(arg0);
	}

	@Override
	public List<IType> visit(TypeDuration arg0) throws GenerationException {
		if(arg0.getNameVariable().endsWith(".")) {
			arg0.setNameVariable(arg0.getNameVariable().substring(0, arg0.getNameVariable().lastIndexOf('.')));
		}
		LOGGER.debug("Found Duration: " + arg0.getNameVariable());
		return simpleType(arg0);
	}

	@Override
	public List<IType> visit(TypeFloat arg0) throws GenerationException {
		if(arg0.getNameVariable().endsWith(".")) {
			arg0.setNameVariable(arg0.getNameVariable().substring(0, arg0.getNameVariable().lastIndexOf('.')));
		}
		LOGGER.debug("Found Float: " + arg0.getNameVariable());
		return simpleType(arg0);
	}

	@Override
	public List<IType> visit(TypeInt arg0) throws GenerationException {
		if(arg0.getNameVariable().endsWith(".")) {
			arg0.setNameVariable(arg0.getNameVariable().substring(0, arg0.getNameVariable().lastIndexOf('.')));
		}
		LOGGER.debug("Found Integer: " + arg0.getNameVariable());
		return simpleType(arg0);
	}

	@Override
	public List<IType> visit(TypeList arg0) throws GenerationException {
		LOGGER.debug("Found List: " + arg0.getNameVariable());
		List<IType> tp = new ArrayList<IType>();
		if(arg0.getNameVariable().endsWith(".")) {
			arg0.setNameVariable(arg0.getNameVariable() + "get");
			arg0.getType().setNameVariable(arg0.getNameVariable() + "(-1).");
		}
		else {
			arg0.getType().setNameVariable(arg0.getNameVariable() + ".get(-1).");
		}
		
		tp.addAll(visit(arg0.getType()));
		return tp;
	}

	@Override
	public List<IType> visit(TypeString arg0) throws GenerationException {
		if(arg0.getNameVariable().endsWith(".")) {
			arg0.setNameVariable(arg0.getNameVariable().substring(0, arg0.getNameVariable().lastIndexOf('.')));
		}
		LOGGER.debug("Found String: " + arg0.getNameVariable());
		return simpleType(arg0);
	}

	@Override
	public List<IType> visit(TypeTime arg0) throws GenerationException {
		if(arg0.getNameVariable().endsWith(".")) {
			arg0.setNameVariable(arg0.getNameVariable().substring(0, arg0.getNameVariable().lastIndexOf('.')));
		}
		LOGGER.debug("Found Time: " + arg0.getNameVariable());
		return simpleType(arg0);
	}

	@Override
	public List<IType> visit(TypeTuple arg0) throws GenerationException {
		LOGGER.debug("Found Tuple: " + arg0.getNameVariable());
		List<IType> tp = new ArrayList<IType>();
		if(arg0.getNameVariable().endsWith(".")) {
			arg0.setNameVariable(arg0.getNameVariable() + "get");
			for(int i = 0; i < arg0.size(); ++i) {
				arg0.getIType(i).setNameVariable(arg0.getNameVariable() + "(" + i + ").");
				tp.addAll(visit(arg0.getIType(i)));
			}
		}
		else {
			for(int i = 0; i < arg0.size(); ++i) {
				arg0.getIType(i).setNameVariable(arg0.getNameVariable() + ".get(" + i + ").");
				tp.addAll(visit(arg0.getIType(i)));
			}
		}
		return tp;
	}

}
