package es.uca.webservices.testgen.autoseed.constants;

/**
 * Class of string constants
 * 
 * @author Valentín Liñeiro Barea
 */
public class StringConstant implements Constant {

	/// Value
	private String value;
	
	/**
	 * Creates a string constant
	 * @param value
	 */
	public StringConstant(String value) {
		this.value = value;
	}
	
	@Override
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return (this.getValue().equals("")) ? "''" : this.getValue();
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof StringConstant) ? 
				((StringConstant) o).getValue().equals(value) : false;
	}
	
	@Override
	public int hashCode() {
		return value.hashCode();
	}

}
