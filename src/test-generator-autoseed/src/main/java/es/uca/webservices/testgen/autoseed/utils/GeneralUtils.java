package es.uca.webservices.testgen.autoseed.utils;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Class with some utils for the rest of classes
 *
 * @author Valentín Liñeiro Barea
 */
public final class GeneralUtils {

	/**
	 * Hide constructor
	 */
	private GeneralUtils() {}


	/**
	 * Puts new entry into map
	 * @param map
	 * @param key
	 * @param value
	 */
	public static <K, V> void putNew(Map<K, Set<V>> map, K key, V value) {
		if(!map.containsKey(key)) {
			map.put(key, new HashSet<V>());
		}
		if(value != null) {
			map.get(key).add(value);
		}
	}

}
