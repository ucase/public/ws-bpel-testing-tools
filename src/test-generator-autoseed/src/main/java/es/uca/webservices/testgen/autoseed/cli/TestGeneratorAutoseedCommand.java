package es.uca.webservices.testgen.autoseed.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.testgen.autoseed.algorithm.AutomaticSeeding;
import es.uca.webservices.testgen.autoseed.algorithm.AutomaticSeedingException;
import es.uca.webservices.testgen.autoseed.report.ReportLevels;

/**
 * Command line runner for the test-generator-autoseed tool
 *
 * @author Valentín Liñeiro Barea
 */
public final class TestGeneratorAutoseedCommand {

	/// Logger
	private static final Logger LOGGER = LoggerFactory.getLogger(TestGeneratorAutoseedCommand.class);

	/// Properties (version)
	private static Properties properties;

	/// Options
	private static final String HELP_OPTION = "help";
	private static final String REPORT_FORMAT_OPTION = "report-format";
	private static final String REPORT_LEVEL_OPTION = "report-level";
	private static final String REPORT_OUTPUT_OPTION = "report-output";
	private static final String COUNT_OPTION = "count";
	private static final String OPTIMIZATION_OPTION = "O";
	private static final String OUTPUT_OPTION = "output";
	private static final String SEED_OPTION = "seed";

	/// Data strings
	private static final String NAME = "testgenerator-autoseed";
    private static final String USAGE = "file.bpel file.spec";
    private static final String DESCRIPTION =
            "Generates test cases from bpel process files and Spec specifications with automated seeding.";

    /// Help flag
    private boolean fRequestedHelp = false;

    /// Optimization flag
    private boolean optimization = false;

    /// Optimization file name
    private String optimizationFileName = "";

    /// Report format
    private String reportFormat = "plain";

    /// Report level
    private ReportLevels reportLevel = ReportLevels.BASIC;

    /// Report output
    private OutputStream reportOutput = System.err;

    /// Count flag
    private boolean count = false;

    /// CL options
    private List<String> fNonOptionArgs = new ArrayList<String>();

    /// Output where vm file is shown
    private OutputStream outputStream = System.out;

    /// Seed
    private String seed = "";

	/**
	 * Prints version of the tool
	 * @return Tool's version
	 */
	public static synchronized String getVersion() {
		if (properties == null) {
			properties = new Properties();
			try {
				properties.load(TestGeneratorAutoseedCommand.class.getResourceAsStream("/test-generator-autoseed.properties"));
			} catch (IOException e) {
				LOGGER.error("Could not load test-generator-autoseed.properties", e);
			}
		}
		return properties.getProperty("autoseed.version");
	}

	/**
	 * @return the name
	 */
	public static String getName() {
		return NAME;
	}

	/**
	 * @return the usage
	 */
	public static String getUsage() {
		return USAGE;
	}

	/**
	 * @return the description
	 */
	public static String getDescription() {
		return DESCRIPTION;
	}

	/**
     * Show the system help
	 * @param object
     * @param parser
     * @throws IOException
     */
    private void printHelp(OptionParser parser) throws IOException {
    	PrintStream stream = System.err;
    	stream.println(getName() + " v" + getVersion() + "\n\n" +
    						"Usage: "  + getName() + " " + getUsage() + "\n\n" +
    						"Description:\n\n" +
    						getDescription() + "\n\n");
        parser.printHelpOn(stream);
    }

	/**
	 * Main
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		final TestGeneratorAutoseedCommand cmd = new TestGeneratorAutoseedCommand();
		try {
			cmd.parseArgs(args);
			cmd.run();
		} catch (IllegalArgumentException ex) {
			LOGGER.error("Illegal argument", ex);
			cmd.printHelp(cmd.createOptionParser());
		} catch (Exception ex) {
			LOGGER.error("There was an error while generating test cases", ex);
		}
	}

	/**
	 * Runs the automatic seeding process
	 * @throws IOException
	 * @throws AutomaticSeedingException
	 */
	public void run() throws IOException, AutomaticSeedingException {
		if (fRequestedHelp) {
            printHelp(createOptionParser());
        } else {
            AutomaticSeeding aGen = new AutomaticSeeding(fNonOptionArgs.get(0), fNonOptionArgs.get(1),
            		optimizationFileName);
            aGen.setSeed(seed);
            aGen.setReportFormat(reportFormat);
            aGen.setReportLevel(reportLevel);
            aGen.setReportOutput(reportOutput);
            aGen.setCount(count);
            aGen.run(outputStream);
        }
	}

	/**
	 * Creates option parser
	 * @return
	 */
	private OptionParser createOptionParser() {
		OptionParser parser = new OptionParser();
        parser.accepts(HELP_OPTION, "Provides help on the subcommand");
        parser.accepts(COUNT_OPTION, "Counts number of test cases generated by automated seeding");
        parser.accepts(OPTIMIZATION_OPTION, "Activates optimization options when generating the test suite").withRequiredArg().
        	describedAs("BPTS File").ofType(String.class);
        parser.accepts(OUTPUT_OPTION, "Sends the output to the specified file "
        		+ "(by default, output is written to stdout)").withRequiredArg().ofType(File.class);
        parser.accepts(REPORT_FORMAT_OPTION, "Sets report format to the given one.\n \n"
        		+ "Report formats available are:\n \nnone\tNo report generated\nplain\tText plain report\nxml\t XML file report\nyaml YAML file report\n \n"
        		+ "If not indicated, report format set is PLAIN.").withRequiredArg().describedAs("Format").ofType(String.class);
        parser.accepts(REPORT_LEVEL_OPTION, "Sets report level to the given one.\n \n"
        		+ "Report levels available are:\n \nBASIC\tValid variables report only\n"
        		+ "ADVANCED\tConstants, variables and valid variables report\nFULL\tFull information report\n \n"
        		+ "If not indicated, report level set is BASIC.").withRequiredArg().ofType(ReportLevels.class);
        parser.accepts(REPORT_OUTPUT_OPTION, "Sends the report output to the specified file "
        		+ "(by default, output is written to stderr)").withRequiredArg().ofType(File.class);
        parser.accepts(SEED_OPTION, "Sets a particular 128-bit string as the seed for the PRNG").
        	withRequiredArg().describedAs("16 B String").ofType(String.class);
        return parser;
	}

	/**
	 * Parses arguments from command line
	 * @param args
	 * @throws FileNotFoundException
	 */
	public void parseArgs(String... args) throws FileNotFoundException {
		try {
            final OptionSet options = createOptionParser().parse(args);
            parseOptions(options);
            fNonOptionArgs = options.nonOptionArguments();
            if (!this.fRequestedHelp) {
                if (fNonOptionArgs.size() != 2) {
                    throw new IllegalArgumentException("Wrong number of arguments");
                }

                fNonOptionArgs = options.nonOptionArguments();
                validateNonOptionsArgs();
            }
        } catch (OptionException ex) {
            throw new IllegalArgumentException(ex.getLocalizedMessage(), ex);
        }
	}

	/**
     * Checks if are valid NonOptionsArgs
     */
    private void validateNonOptionsArgs() {
    	final String bpelFile = fNonOptionArgs.get(0);
    	final String specFile = fNonOptionArgs.get(1);

    	final String[] extensionBpel = bpelFile.split("\\.");
    	final String[] extensionSpec = specFile.split("\\.");

    	if(!extensionBpel[extensionBpel.length - 1].toLowerCase().trim().equals("bpel") ||
    			!extensionSpec[extensionSpec.length - 1].toLowerCase().trim().equals("spec")) {
    		throw new IllegalArgumentException("Invalid file extension: should be .bpel and .spec");
    	}

    	if(this.optimization) {
    		final String[] extensionBpts = optimizationFileName.split("\\.");
    		if(!extensionBpts[extensionBpts.length - 1].toLowerCase().trim().equals("bpts")) {
    			throw new IllegalArgumentException("Invalid file extension: should be .bpts");
    		}
    	}
    }

    /**
     * Parses options from command line
     * @param options
     * @throws FileNotFoundException
     */
	private void parseOptions(OptionSet options) throws FileNotFoundException {
        if (options.has(HELP_OPTION)) {
            this.fRequestedHelp = true;
        }
        if (options.has(OPTIMIZATION_OPTION)) {
        	this.optimization = true;
        	this.optimizationFileName = (String) options.valueOf(OPTIMIZATION_OPTION);
        }
        if (options.has(OUTPUT_OPTION)) {
        	this.outputStream = new FileOutputStream((File) options.valueOf(OUTPUT_OPTION));
        }
        if (options.has(REPORT_FORMAT_OPTION)) {
        	this.reportFormat = (String) options.valueOf(REPORT_FORMAT_OPTION);
        }
        if (options.has(REPORT_LEVEL_OPTION)) {
        	this.reportLevel = (ReportLevels) options.valueOf(REPORT_LEVEL_OPTION);
        }
        if (options.has(REPORT_OUTPUT_OPTION)) {
        	this.reportOutput = new FileOutputStream((File) options.valueOf(REPORT_OUTPUT_OPTION));
        }
        if (options.has(COUNT_OPTION)) {
        	this.count = true;
        }
        if (options.has(SEED_OPTION)) {
        	this.seed = (String) options.valueOf(SEED_OPTION);
        }
    }

}
