package es.uca.webservices.testgen.autoseed.reader.variables;

import java.util.List;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * Interface of type reader
 * 
 * @author Valentín Liñeiro Barea
 */
public interface TypeReader {
	
	/**
	 * Get variables
	 * @return
	 * @throws ParserException 
	 */
	List<IType> getTypes() throws ParserException;
	
	/**
	 * Get variables (with inner variables)
	 * @return
	 * @throws GenerationException 
	 * @throws ParserException
	 */
	List<IType> getSimpleTypes() throws ParserException, GenerationException;
	
}
