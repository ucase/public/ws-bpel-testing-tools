package es.uca.webservices.testgen.autoseed.source;

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class with some utils to BPTSSource
 * @author Valentín Liñeiro Barea
 *
 */
public final class BPTSSourceUtils {
	
	/**
	 * Hidden constructor
	 */
	private BPTSSourceUtils() {}
	
	/**
	 * Gets template element content
	 * @param domNode
	 * @param a
	 * @param exprs
	 * @param listVar
	 */
	public static void getTemplateElementContent(Node domNode, String a, Set<String> exprs, Map<String, String> listVar) {
		String path = a;
		if (domNode.getNodeType() == Node.TEXT_NODE) {
			String value = domNode.getNodeValue().trim();
			readVelocityFromTemplate(value, path, exprs, listVar);
			
		} else {
			path += ((domNode.getPrefix() == null || !domNode.getPrefix().equals("")) ? domNode.getPrefix()
					+ ":" : "")
					+ domNode.getLocalName() + "/";
			NodeList children = domNode.getChildNodes();
			for (int i = 0; i < children.getLength(); ++i) {
				getTemplateElementContent(children.item(i), path, exprs, listVar);
			}
		}
	}

	/**
	 * Reads velocity template and extracts information
	 * @param value
	 * @param path
	 * @param exprs
	 * @param listVar
	 */
	private static void readVelocityFromTemplate(String a, String b,
			Set<String> exprs, Map<String, String> listVar) {
		String value = a;
		String path = b;
		if(!value.startsWith("#end")) {
			if(value.startsWith("#foreach")) {
				if(value.contains("#if")) {
					value = value.replaceFirst("#if\\(.*\\)", "");
				}
				updateListOfVariableExpressions(value, listVar);
			}
			else if(value.startsWith("#if")) {
				if(value.contains("#foreach")) {
					value = value.replaceAll("[ \\t\\r?\\n]+", " ");
					value = value.replaceAll("#if(.*) #", "#");
					updateListOfVariableExpressions(value, listVar);
				}
			}
			else {
				for(Entry<String, String> en: listVar.entrySet()) {
					if(value.contains(en.getKey())) {
						value = value.replace(en.getKey(), en.getValue());
					}
				}
				path += value;
				if (!value.equals("")) {
					exprs.add(path);
				}
			}
		}
	}

	/**
	 * Updates list of variable expressions
	 * @param value
	 * @param listVar
	 */
	private static void updateListOfVariableExpressions(String value,
			Map<String, String> listVar) {
		String varToEliminate = value.substring(value.indexOf('$'), value.indexOf(' '));
		String varList = value.substring(value.lastIndexOf('$'), value.lastIndexOf(')'));
		for(Entry<String, String> en: listVar.entrySet()) {
			if(varList.contains(en.getKey())) {
				varList = varList.replace(en.getKey(), en.getValue());
			}
		}
		listVar.put(varToEliminate, varList + ".get(-1)");
	}

}
