package es.uca.webservices.testgen.autoseed.constants;

import java.math.BigDecimal;

/**
 * Class of float constants
 * 
 * @author Valentín Liñeiro Barea
 */
public class FloatConstant implements Constant {

	/// Value
	private BigDecimal value;
	
	/**
	 * Creates a float constant
	 * @param value
	 */
	public FloatConstant(String value) {
		this.value = new BigDecimal(value);
	}
	
	@Override
	public BigDecimal getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof FloatConstant) ? 
				((FloatConstant) o).getValue().equals(value) : false;
	}
	
	@Override
	public int hashCode() {
		return value.hashCode();
	}

}
