package es.uca.webservices.testgen.autoseed.algorithm;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.check.algorithm.SpecBPELVariableMapper;
import es.uca.webservices.testgen.autoseed.check.algorithm.SpecTypeChecker;
import es.uca.webservices.testgen.autoseed.check.algorithm.TypeChecker;
import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.generators.AutoSeeder;
import es.uca.webservices.testgen.autoseed.generators.SpecAutoSeeder;
import es.uca.webservices.testgen.autoseed.reader.constants.BPELConstReader;
import es.uca.webservices.testgen.autoseed.reader.constants.ConstReader;
import es.uca.webservices.testgen.autoseed.reader.variables.SpecReader;
import es.uca.webservices.testgen.autoseed.reader.variables.TypeReader;
import es.uca.webservices.testgen.autoseed.report.Report;
import es.uca.webservices.testgen.autoseed.report.ConcreteFormatterFactory;
import es.uca.webservices.testgen.autoseed.report.IReportFormatter;
import es.uca.webservices.testgen.autoseed.report.ReportLevels;
import es.uca.webservices.testgen.autoseed.source.BPELSource;
import es.uca.webservices.testgen.autoseed.source.BPTSSource;
import es.uca.webservices.testgen.autoseed.utils.ValidVariablesMap;

/**
 * Class in charge of generate a test suite with automatic seeding
 *
 * @author Valentín Liñeiro Barea
 */
public class AutomaticSeeding {

	/// Logger
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AutomaticSeeding.class);

	/// Const reader
	private ConstReader constReader;

	/// Spec file parser
	private TypeReader typeReader;

	/// Type checker
	private TypeChecker typeChecker;

	/// Auto Seeder
	private AutoSeeder seeder;

	/// Report
	private Report report = new Report();

	/// Report Format
	private String reportFormat;

	/// Report Level
	private ReportLevels reportLevel;

	/// Report Output
	private PrintStream reportOutput;

	/// Count testCases flag
	private boolean count = false;

	/**
	 * Creates an AutomaticSeeding object
	 * @param bpelPath Path to bpel file
	 * @param specPath Path to spec file
	 * @throws AutomaticSeedingException
	 */
	public AutomaticSeeding(String bpelPath, String specPath, String bptsPath)
			throws AutomaticSeedingException {
		try {
			BPELSource bpel = new BPELSource(bpelPath);
			report.setBpelPath(bpelPath);
			BPTSSource bpts = null;
			if(!bptsPath.equals("")) {
				LOGGER.debug("BPTS provided: " + bptsPath);
				bpts = new BPTSSource(bptsPath);
				report.setBptsPath(bptsPath);
			}
			constReader = new BPELConstReader(bpel);
			typeReader = new SpecReader(specPath);
			typeChecker = (bpts != null) ? new SpecTypeChecker(
					new SpecBPELVariableMapper(bpel, bpts)) : new SpecTypeChecker();
			seeder = new SpecAutoSeeder();
			report.setSpecPath(specPath);
		} catch (Exception e) {
			throw new AutomaticSeedingException(e);
		}
	}

	/**
	 * @param seed
	 */
	public void setSeed(String seed) {
		LOGGER.debug("Setting PRNG seed to " + seed);
		this.seeder.setSeed(seed);
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(boolean count) {
		LOGGER.debug("Setting count option to " + count);
		this.count = count;
	}

	/**
	 * @param reportFormat the reportFormat to set
	 */
	public void setReportFormat(String reportFormat) {
		this.reportFormat = reportFormat;
	}

	/**
	 * @param reportLevel the reportLevel to set
	 */
	public void setReportLevel(ReportLevels reportLevel) {
		this.reportLevel = reportLevel;
	}

	/**
	 * @param reportOutput the reportOutput to set
	 */
	public void setReportOutput(OutputStream reportOutput) {
		this.reportOutput = (!reportOutput.equals(System.err)) ?
				new PrintStream(reportOutput) :
					(PrintStream) reportOutput;
	}

	/**
	 * Runs the automated seeding test suite generation
	 * @param os
	 * @throws AutomaticSeedingException
	 */
	public void run(OutputStream os) throws AutomaticSeedingException {
		try {
			//0. Sets Report Formatter
			IReportFormatter repFmt = ConcreteFormatterFactory.getReportFormatterFactory()
					.newFormatter(reportFormat, reportLevel);
			//1. Read constants
			LOGGER.debug("Reading constants from BPEL Process file");
			Map<Constant, Set<String>> consts = constReader.getConstants();
			report.setConstants(consts);
			if (consts.isEmpty()) {
				LOGGER.warn("There are no constants in the BPEL file");

			} else {
				//2. Read types (and inner types)
				LOGGER.debug("Reading variables from spec file");
				List<IType> types = typeReader.getSimpleTypes();
				report.setVariables(types);
				if(types.isEmpty()) {
					LOGGER.warn("There are no variables in the spec file");

				} else {
					//3. Search for valid variables
					LOGGER.debug("Searching for valid variables");
					ValidVariablesMap validVariables;
					if(typeChecker.getVariableMapper() != null) {
						report.setMapping(typeChecker.getVariableMapper().getMapping());
					}
					validVariables = typeChecker.getValidVariables(types, consts);
					report.setValidVariables(validVariables);
					if(validVariables.isEmpty()) {
						LOGGER.warn("There are no valid variables for the constants found");
					} else {
						//4. Generate a test suite and modify it
						repFmt.format(report, reportOutput);
						if(this.count) {
							new PrintStream(os).println(validVariables.numberOfTestCases());
						}
						else {
							LOGGER.debug("Creating test suite with automatic seeding");
							seeder.getTestSuite(typeReader.getTypes(), validVariables, os);
						}
					}
				}
			}
		} catch(Exception e) {
			throw new AutomaticSeedingException(e);
		}
	}
}
