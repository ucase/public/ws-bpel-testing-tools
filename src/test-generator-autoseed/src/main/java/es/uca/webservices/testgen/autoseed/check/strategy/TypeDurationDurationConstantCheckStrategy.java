package es.uca.webservices.testgen.autoseed.check.strategy;

import es.uca.webservices.testgen.api.types.TypeDuration;
import es.uca.webservices.testgen.autoseed.constants.DurationConstant;

/**
 * Duration check restrictions strategy
 * 
 * @author Valentín Liñeiro Barea
 */
public class TypeDurationDurationConstantCheckStrategy implements ICheckStrategy {
	
	/// Type
	private TypeDuration typeDuration;
	
	/// Value
	private DurationConstant constDuration;
	
	/**
	 * Creates a duration check strategy
	 * @param tD
	 * @param dC
	 */
	public TypeDurationDurationConstantCheckStrategy(TypeDuration tD, DurationConstant dC) {
		this.typeDuration = tD;
		this.constDuration = dC;
	}

	@Override
	public boolean checkRestrictions() {
		boolean ok = true;
		if(typeDuration.getMinValue().compare(constDuration.getValue()) > 0) {
			ok = false;
		}
		if(ok && typeDuration.getMaxValue().compare(constDuration.getValue()) < 0) {
			ok = false;
		}
		return ok;
	}

}
