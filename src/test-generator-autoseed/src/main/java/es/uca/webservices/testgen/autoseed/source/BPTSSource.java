package es.uca.webservices.testgen.autoseed.source;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import net.bpelunit.framework.xml.suite.XMLAnyElement;
import net.bpelunit.framework.xml.suite.XMLTestSuite;
import net.bpelunit.framework.xml.suite.XMLTestSuiteDocument;

import org.apache.commons.io.FileUtils;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ibm.wsdl.util.xml.DOMUtils;

import es.uca.webservices.bpel.xml.XMLDocument;

/**
 * Class for BPTS sources
 *
 * @author Valentín Liñeiro Barea
 */
public class BPTSSource extends XMLSource {

	/// Name of element packing templates with more than one element
	public static final String DUMMY_TEMPLATE_ELEMENT = "dummyTemplate";

	/// Relative path (for vm sources)
	private String relativePath;

	/// BPTS model
	private XMLTestSuite model;

	/// List of templates
	private Set<Element> templates;

	/**
	 * Constructor
	 * @param bptsPath
	 * @throws XmlException
	 * @throws IOException
	 */
	public BPTSSource(String bptsPath) throws XmlException, IOException {
		this.relativePath = bptsPath.substring(0, bptsPath.lastIndexOf('/') + 1);
		this.model = XMLTestSuiteDocument.Factory.parse(new File(bptsPath)).getTestSuite();
	}

	/**
	 * Search for all templates in BPTS
	 * @return
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 */
	public Set<Element> getTemplates()
			throws SAXException, IOException, ParserConfigurationException {
		if(templates == null) {

			templates = new HashSet<Element>();
			templates.addAll(getAllTemplates(model));
		}
		return templates;
	}

	/**
	 * Helper method in order to search for templates
	 * @param element
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private List<Element> getAllTemplates(XmlObject element)
			throws SAXException, IOException, ParserConfigurationException {
		List<Element> temp = new LinkedList<Element>();
		if(element instanceof XMLAnyElement && element.getDomNode().getLocalName().equals("template")) {
			Element template = getTemplateElement((XMLAnyElement) element);
			if(template != null) {
				temp.add(template);
			}
		}
		XmlObject[] children = element.selectPath("./*");
		for(XmlObject object: children) {
			temp.addAll(getAllTemplates(object));
		}
		return temp;
	}

	/**
	 * Get template main element from BPTS template
	 * @param element
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	private Element getTemplateElement(XMLAnyElement element) throws IOException,
		SAXException, ParserConfigurationException {
		return (element.getSrc() != null) ? getTemplateElementFromExternalSrc(element.getSrc()) :
			getTemplateElementFromDOM((Element) element.getDomNode());
	}

	/**
	 * Gets main template element from DOM
	 * @param domNode
	 * @return
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 */
	private Element getTemplateElementFromDOM(Element element) throws SAXException, IOException,
		ParserConfigurationException {
		Element elem = null;
		int number = es.uca.webservices.testgen.autoseed.source.DOMUtils.getNumberOfElementChildren(element);
		if(number == 1) {
			elem = DOMUtils.getFirstChildElement(element);
		}
		else if(number > 1) {
			elem = groupElements(element);
		}
		if(elem == null) {
			String textTemplate = DOMUtils.getChildCharacterData(element);
			if(textTemplate != null && !"".equals(textTemplate)) {
				elem = new XMLDocument(textTemplate).getDocument().getDocumentElement();
			}
		}
		return elem;
	}

	/**
	 * Group child template elements into a dummy one
	 * @param element
	 * @return
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	private Element groupElements(Element element) throws SAXException,
		IOException, ParserConfigurationException {
		Element elem = null;
		String helpDoc = "<" + DUMMY_TEMPLATE_ELEMENT + "></" + DUMMY_TEMPLATE_ELEMENT + ">";
		Document createDoc = new XMLDocument(helpDoc).getDocument();
		elem = createDoc.getDocumentElement();
		NodeList children = element.getChildNodes();
		for(int i = 0; i < children.getLength(); ++i) {
			if(children.item(i).getNodeType() == Node.ELEMENT_NODE) {
				elem.appendChild(createDoc.importNode(children.item(i), true));
			}
		}
		return elem;
	}

	/**
	 * Gets main template element from external source
	 * @param src
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	private Element getTemplateElementFromExternalSrc(String src) throws IOException,
		SAXException, ParserConfigurationException {
		String content = FileUtils.readFileToString(
				new File(this.relativePath + src), "utf-8");
		content = XMLUtils.clean(extractElement(content).trim());
		return (!content.equals("")) ?
				new XMLDocument(content).getDocument().getDocumentElement() :
					null;
	}

	/**
	 * Gets element from an string template, skipping velocity previous statements
	 * @param content
	 * @return
	 */
	private String extractElement(String content) {
		return (content.indexOf('<') != -1) ?
				content.substring(content.indexOf('<')) : "";
	}

	@Override
	protected void start() {
		this.getNamespaces().putAll(getAllNS(model));
	}

}
