package es.uca.webservices.testgen.autoseed.check.strategy;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.constants.Constant;

/**
 * Interface of check strategy factory
 * 
 * @author Valentín Liñeiro Barea
 */
public interface ICheckStrategyFactory {
	
	/**
	 * Returns a proper check strategy given type and constant
	 * @param type
	 * @param cons
	 * @return
	 */
	ICheckStrategy newCheckStrategy(IType type, Constant cons);

}
