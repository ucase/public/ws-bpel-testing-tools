package es.uca.webservices.testgen.autoseed.report;

import java.util.List;
import java.util.Map;
import java.util.Set;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.utils.ValidVariablesMap;

/**
 * Automatic Seeding Report
 *
 * @author Valentín Liñeiro Barea
 */
public class Report {

	/// BPEL Path
	private String bpelPath = "";

	/// Spec Path
	private String specPath = "";

	/// BPTS Path
	private String bptsPath = "";

	/// Constants
	private Map<Constant, Set<String>> constants;

	/// Variables
	private List<IType> variables;

	/// Mapping
	private Map<String, Set<String>> mapping;

	/// Report
	private ValidVariablesMap validVariables;

	/**
	 * @return the bpelPath
	 */
	public String getBpelPath() {
		return bpelPath;
	}

	/**
	 * @param bpelPath the bpelPath to set
	 */
	public void setBpelPath(String bpelPath) {
		this.bpelPath = bpelPath;
	}

	/**
	 * @return the specPath
	 */
	public String getSpecPath() {
		return specPath;
	}

	/**
	 * @param specPath the specPath to set
	 */
	public void setSpecPath(String specPath) {
		this.specPath = specPath;
	}

	/**
	 * @return the bptsPath
	 */
	public String getBptsPath() {
		return bptsPath;
	}

	/**
	 * @param bptsPath the bptsPath to set
	 */
	public void setBptsPath(String bptsPath) {
		this.bptsPath = bptsPath;
	}

	/**
	 * @return the constants
	 */
	public Map<Constant, Set<String>> getConstants() {
		return constants;
	}

	/**
	 * @param constants the constants to set
	 */
	public void setConstants(Map<Constant, Set<String>> constants) {
		this.constants = constants;
	}

	/**
	 * @return the variables
	 */
	public List<IType> getVariables() {
		return variables;
	}

	/**
	 * @param variables the variables to set
	 */
	public void setVariables(List<IType> variables) {
		this.variables = variables;
	}

	/**
	 * @return the mapping
	 */
	public Map<String, Set<String>> getMapping() {
		return mapping;
	}

	/**
	 * @param mapping the mapping to set
	 */
	public void setMapping(Map<String, Set<String>> mapping) {
		this.mapping = mapping;
	}

	/**
	 * @return the report
	 */
	public ValidVariablesMap getValidVariables() {
		return validVariables;
	}

	/**
	 * @param report the report to set
	 */
	public void setValidVariables(ValidVariablesMap report) {
		this.validVariables = report;
	}

}
