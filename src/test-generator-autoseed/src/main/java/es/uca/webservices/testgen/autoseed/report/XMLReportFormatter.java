package es.uca.webservices.testgen.autoseed.report;

import java.io.PrintStream;
import java.util.Date;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.constants.Constant;

/**
 * XML Report Formatter
 * 
 * @author Valentín Liñeiro Barea
 */
public class XMLReportFormatter extends AbstractReportFormatter {
	
	/// XML NS
	private static final String NS = "http://uca.es/test-generator-autoseed/report";
	
	/// XML Prefix
	private static final String PREFIX = "rep";
	
	/// XML DOM
	private Document document;
	
	/**
	 * Creates a XML Report Formatter
	 * @param reportLevel
	 * @throws ParserConfigurationException 
	 */
	public XMLReportFormatter(ReportLevels reportLevel) throws ParserConfigurationException {
		super(reportLevel);
		DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
		fact.setNamespaceAware(true);
		DocumentBuilder db = fact.newDocumentBuilder();
		document = db.newDocument();
		document.setXmlVersion("1.0");
	}
	
	@Override
	public void formatStart(PrintStream os) {
		Element report = createElement("report");
		report.setAttribute("xmlns:" + PREFIX, NS);
		document.appendChild(report);
	}

	@Override
	public void formatEnd(PrintStream os) throws TransformerException {
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
	    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	    
		DOMSource source = new DOMSource(document);
		StreamResult result = new StreamResult(os); 
		transformer.transform(source, result);
	}

	@Override
	public void formatInformationStart(PrintStream os) {
		Element info = createElement("info");
		document.getDocumentElement().appendChild(info);
		Element date = createElement("date");
		addText(date, new Date().toString());
		document.getDocumentElement().getLastChild().appendChild(date);
	}

	@Override
	public void formatInformationBPEL(String bpelPath, PrintStream os) {
		Element bpel = createElement("bpel");
		addText(bpel, bpelPath);
		document.getDocumentElement().getLastChild().appendChild(bpel);
	}

	@Override
	public void formatInformationSpec(String specPath, PrintStream os) {
		Element spec = createElement("spec");
		addText(spec, specPath);
		document.getDocumentElement().getLastChild().appendChild(spec);
	}

	@Override
	public void formatInformationBPTS(String bptsPath, PrintStream os) {
		Element bpts = createElement("bpts");
		addText(bpts, bptsPath);
		document.getDocumentElement().getLastChild().appendChild(bpts);
	}

	@Override
	public void formatInformationEnd(PrintStream os) {
	}

	@Override
	public void formatConstantsStart(PrintStream os) {
		Element constants = createElement("constants");
		document.getDocumentElement().appendChild(constants);
	}

	@Override
	public void formatConstantsElement(Entry<Constant, Set<String>> en,
			PrintStream os) {
		String typeClassName = en.getKey().getClass().getSimpleName();
		String typeName = typeClassName.substring(0, typeClassName.lastIndexOf('C'));
		Element constant = createElement("constant");
		constant.setAttribute("type", typeName);
		constant.setAttribute("value", en.getKey().getValue().toString());
		
		for(String var: en.getValue()) {
			Element bpelVariable = createElement("bpelVariable");
			addText(bpelVariable, var);
			constant.appendChild(bpelVariable);
		}
		
		document.getDocumentElement().getLastChild().appendChild(constant);
	}

	@Override
	public void formatConstantsEnd(PrintStream os) {
	}

	@Override
	public void formatVariablesStart(PrintStream os) {
		Element variables = createElement("variables");
		document.getDocumentElement().appendChild(variables);
	}

	@Override
	public void formatVariablesElement(IType var, PrintStream os) {
		String typeClassName = var.getClass().getSimpleName();
		String typeName = typeClassName.substring(typeClassName.indexOf('e') + 1);
		Element variable = createElement("variable");
		variable.setAttribute("type", typeName);
		variable.setAttribute("value", var.getNameVariable());
		document.getDocumentElement().getLastChild().appendChild(variable);
	}

	@Override
	public void formatVariablesEnd(PrintStream os) {
	}

	@Override
	public void formatMappingStart(PrintStream os) {
		Element mapping = createElement("mapping");
		document.getDocumentElement().appendChild(mapping);
	}

	@Override
	public void formatMappingElement(Entry<String, Set<String>> en,
			PrintStream os) {	
		Element entry = createElement("mappingEntry");
		entry.setAttribute("specVariable", en.getKey());
		
		for(String var: en.getValue()) {
			Element bpelVariable = createElement("bpelVariable");
			addText(bpelVariable, var);
			entry.appendChild(bpelVariable);
		}
		
		document.getDocumentElement().getLastChild().appendChild(entry);
	}

	@Override
	public void formatMappingEnd(PrintStream os) {
	}

	@Override
	public void formatValidVariablesStart(PrintStream os) {
		Element validVariables = createElement("validVariables");
		document.getDocumentElement().appendChild(validVariables);
	}

	@Override
	public void formatValidVariablesElement(
			Entry<IType, Pair<SortedSet<String>, Set<Constant>>> en,
			PrintStream os) {
		String typeClassName = en.getKey().getClass().getSimpleName();
		String typeName = typeClassName.substring(typeClassName.indexOf('e') + 1);
		for(String var: en.getValue().getLeft()) {			
			Element validVariables = createElement("validVariables");
			validVariables.setAttribute("type", typeName);
			validVariables.setAttribute("name", var);
			
			for(Constant cons: en.getValue().getRight()) {
				Element constant = createElement("constant");
				constant.setAttribute("value", cons.getValue().toString());
				validVariables.appendChild(constant);
			}
			
			document.getDocumentElement().getLastChild().appendChild(validVariables);
		}
	}

	@Override
	public void formatValidVariablesEnd(PrintStream os) {
	}

	@Override
	public void formatNumberOfTestCasesStart(PrintStream os) {
		Element numberOfTestCases = createElement("numberOfTestCases");
		document.getDocumentElement().appendChild(numberOfTestCases);
	}

	@Override
	public void formatNumberOfTestCasesElement(int numberOfTestCases,
			PrintStream os) {
		document.getDocumentElement().getLastChild().appendChild(
				document.createTextNode(Integer.valueOf(numberOfTestCases).toString()));
	}

	@Override
	public void formatNumberOfTestCasesEnd(PrintStream os) {
	}
	
	/**
	 * Creates proper Element
	 * @param name
	 * @return
	 */
	private Element createElement(String name) {
		return document.createElementNS(NS, PREFIX + ":" + name);
	}
	
	/**
	 * Adds text Node Child to Element
	 * @param bpel
	 * @param bpelPath
	 */
	private void addText(Element element, String text) {
		element.appendChild(document.createTextNode(text));
	}
	
}
