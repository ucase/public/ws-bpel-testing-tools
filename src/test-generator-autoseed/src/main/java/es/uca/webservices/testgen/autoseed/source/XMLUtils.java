package es.uca.webservices.testgen.autoseed.source;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.xmlbeans.XmlObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import es.uca.webservices.bpel.util.Pair;

/**
 * Class with some utils for the rest of classes
 *
 * @author Valentín Liñeiro Barea
 */
public final class XMLUtils {

	/// Logger
	private static final Logger LOGGER = LoggerFactory
			.getLogger(XMLUtils.class);

	/**
	 * Hide constructor
	 */
	private XMLUtils() {}

	/**
	 * Gets expression from model node
	 * @param el
	 * @return
	 */
	public static String getExpression(XmlObject el) {
		return el.getDomNode().getFirstChild().getNodeValue().trim();
	}

	/**
	 * Formats DOM tree to variable
	 * @param item
	 * @param map
	 * @return
	 */
	public static String format(Node item, Map<String, String> map) {
		String output = "";
		if (item.getLocalName().equals("variableReference")) {
			output = "$" + format(item.getFirstChild(), map);
		} else if (item.getLocalName().equals("qname")) {
			Element el = (Element) item;
			String prefix = el.getAttribute("prefix");
			output = (prefix.equals("")) ? el.getAttribute("localPart") :
				changeNS(prefix + ":" + el.getAttribute("localPart"), map);
		} else if (item.getLocalName().equals("children")) {
			output = format(item.getFirstChild(), map) + "/"
					+ format(item.getLastChild(), map);
		} else if (item.getLocalName().equals("predicate")) {
			output = format(item.getFirstChild(), map);
		} else if (item.getLocalName().equals("functionCall")) {
			output = format(item.getLastChild(), map);
		}
		return output;
	}

	/**
	 * Format literal for BPTS
	 * @param domNode
	 * @param a
	 * @param exprs
	 */
	public static void formatLiteral(Node domNode, String a, Set<String> exprs) {
		String path = a;
		if (domNode.getNodeType() == Node.TEXT_NODE) {
			path = path +  "'" + domNode.getNodeValue().trim() + "'";
			if (!domNode.getNodeValue().trim().equals("")) {
				exprs.add(path);
			}
		} else {
			if (!domNode.getLocalName().equals("literal")) {
				path += ((!domNode.getPrefix().equals("")) ? domNode.getPrefix()
						+ ":" : "")
						+ domNode.getLocalName() + "/";
			}
			NodeList children = domNode.getChildNodes();
			for (int i = 0; i < children.getLength(); ++i) {
				formatLiteral(children.item(i), path, exprs);
			}
		}
	}

	/**
	 * Get query and constant from BPEL
	 * @param exprs
	 * @param map
	 * @return
	 */
	public static Set<Pair<String, String>> getQueryAndConstant(Set<String> exprs, Map<String, String> map) {
		Set<Pair<String, String>> results = new HashSet<Pair<String, String>>();
		for (String elem : exprs) {
			int firstIndex = elem.indexOf('\'');
			String query = "";
			String constant = elem.substring(elem.indexOf('\'') + 1, elem.lastIndexOf('\''));
			if(firstIndex != 0) {
				query = elem.substring(0, elem.indexOf('\'') - 1);
				String[] splitted = query.split("/");
				List<String> splittedToList = getList(splitted);
				for(int i = 0; i < splittedToList.size(); ++i) {
					splittedToList.set(i, changeNS(splittedToList.get(i), map));
				}
				query = "";
				for(int i = 1; i < splittedToList.size(); ++i) {
					query = query + "/" + splittedToList.get(i);
				}
			}
			LOGGER.debug("Found constant: " + constant + " and query: " + query);
			results.add(new Pair<String, String>(constant, query));
		}
		return results;
	}

	/**
	 * Gets query and variable from BPEL
	 * @param exprs
	 * @param map
	 * @return
	 */
	public static Set<Pair<String, String>> getQueryAndVariable(Set<String> exprs, Map<String, String> map) {
		Set<Pair<String, String>> results = new HashSet<Pair<String, String>>();
		for (String elem : exprs) {
			String[] splitted = elem.split("/");
			List<String> splittedToList = getList(splitted);
			for(int i = 0; i < splittedToList.size() - 1; ++i) {
				splittedToList.set(i, changeNS(splittedToList.get(i), map));
			}
			results.add(new Pair<String, String>(splittedToList
					.get(splittedToList.size() - 1), getString(splittedToList)));
		}
		return results;
	}

	/**
	 * Change NS of a string
	 * @param string
	 * @param map
	 * @return
	 */
	public static String changeNS(String string, Map<String, String> map) {
		LOGGER.debug("Changing namespace of " + string);
		String[] separated = string.split(":");
		return (separated.length > 1) ? "{" + map.get(separated[0]) + "}" + separated[1] :
			string;
	}

	/**
	 * Change prefixes for NS expressions
	 * @param toVar
	 * @param ns
	 * @return
	 */
	public static String convertNS(String toVar, Map<String, String> ns) {
		String res = "";
		String[] expr = toVar.split("/");
		for(String ex: expr) {
			if(ex.matches(".+:.+")) {
				res = res + changeNS(ex, ns) + "/";
			}
			else {
				res = res + ex + "/";
			}
		}
		return res.substring(0, res.length() - 1);
	}

	/**
	 * Changes NS for NS URIs into expression
	 * @param varExpr
	 * @param map
	 * @return
	 */
	public static String changeNSPrefixesForURIs(String varExpr, Map<String, String> map) {
		String finalExpr = varExpr;
		String[] splitted = varExpr.split("/");
		if(splitted.length > 1) {
			List<String> splittedToList = getList(splitted);
			for(int i = 0; i < splittedToList.size(); ++i) {
				splittedToList.set(i, XMLUtils.changeNS(splittedToList.get(i), map));
			}
			finalExpr = getCompleteString(splittedToList);
		}
		return finalExpr;
	}

	/**
	 * Takes an array and returns a list
	 * @param array
	 * @return a list
	 */
	public static <T> List<T> getList(T[] array) {
		List<T> list = new LinkedList<T>();
		for(T elem: array) {
			list.add(elem);
		}
		return list;
	}

	/**
	 * Takes a list of Strings and concatenates it (not first and last element)
	 * @param splittedToList
	 * @return a string
	 */
	public static String getString(List<String> splittedToList) {
		String res = "";
		for(int i = 1; i < splittedToList.size() - 1; ++i) {
			res = res + "/" + splittedToList.get(i);
		}
		return res;
	}

	/**
	 * Takes a list of Strings and concatenates it
	 * @param splittedToList
	 * @return a string
	 */
	public static String getCompleteString(List<String> splittedToList) {
		String res = (splittedToList.size() > 0) ? splittedToList.get(0) : "";
		for(int i = 1; i < splittedToList.size(); ++i) {
			res = res + "/" + splittedToList.get(i);
		}
		return res;
	}

	/**
	 * Cleans expression
	 * @param trim
	 * @return
	 */
	public static String clean(String trim) {
		String cleaned = trim.replaceAll("#foreach\\(\\$\\w+ in (\\$\\w+(\\.get\\(\\d+\\))*)\\) (\\w+) "
				+ "= \"(\\$\\w+)\" #end", "$3 = \"$1\"");
		String cleaned2 = cleaned.replaceAll("#foreach\\(\\$\\w+ in (\\$\\w+(\\.get\\(\\d+\\))*)\\) (\\w+) "
				+ "= \" #if\\( \\$\\w+(\\.get\\(\\d+\\))* == \\w+ \\) \\$\\w+(\\.get\\(\\d+\\))* (#elseif\\( \\$\\w+(\\.get\\(\\d+\\))* == \\w+ \\) \\$\\w+(\\.get\\(\\d+\\))* )*#else "
				+ "\\$\\w+(\\.get\\(\\d+\\))* #end \" #end", "$3 = \"$1\"");
		String cleaned3 = cleaned2.replaceAll("#foreach\\(\\$\\w+ in (\\$\\w+(\\.get\\(\\d+\\))*)\\) (\\w+) "
				+ "= \"#foreach\\(\\$\\w+(\\.get\\(\\d+\\))* in \\$\\w+(\\.get\\(\\d+\\))*\\) "
				+ "\\$\\w+(\\.get\\(\\d+\\))* #end\" #end", "$3 = \"$1\"");
		return cleaned3;
	}

}
