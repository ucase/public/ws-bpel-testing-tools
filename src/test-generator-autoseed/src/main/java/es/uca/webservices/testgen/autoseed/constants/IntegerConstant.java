package es.uca.webservices.testgen.autoseed.constants;

import java.math.BigInteger;

/**
 * Class of integer constants
 * 
 * @author Valentín Liñeiro Barea
 */
public class IntegerConstant implements Constant {
	
	/// Value
	private BigInteger value;
	
	/**
	 * Creates an integer constant
	 * @param value
	 */
	public IntegerConstant(String value) {
		this.value = new BigInteger(value);
	}
	
	@Override
	public BigInteger getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof IntegerConstant) ? 
				((IntegerConstant) o).getValue().equals(value) : false;
	}
	
	@Override
	public int hashCode() {
		return value.hashCode();
	}
	
}
