package es.uca.webservices.testgen.autoseed.report;

import java.lang.reflect.Constructor;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Formatter Factory
 *
 * @author Valentín Liñeiro Barea
 */
public final class ConcreteFormatterFactory implements IReportFormatterFactory {

	/**
	 * Singleton
	 */
	private ConcreteFormatterFactory() {}

	/// Singleton instance
	private static IReportFormatterFactory instance;

	/**
	 * Returns singleton instance
	 * @return
	 */
	public static IReportFormatterFactory getReportFormatterFactory() {
		if(instance == null) {
			instance = new ConcreteFormatterFactory();
		}
		return instance;
	}

	/*
	 * Now, we use the Reflections API to choose at runtime the proper formatter
	 */
	@Override
	public IReportFormatter newFormatter(String format,
			ReportLevels level) throws ParserConfigurationException {
		IReportFormatter formatter = new DummyReportFormatter(level);
		try {
			Class<?> cl = Class.forName(this.getClass().getPackage().getName() +
					"." + format.toUpperCase() + "ReportFormatter");
			Constructor<?> con = cl.getConstructor(level.getClass());
			formatter = (IReportFormatter) con.newInstance(level);
		} catch (Exception e) {
			formatter = new DummyReportFormatter(level);
		}
		return formatter;
	}

}
