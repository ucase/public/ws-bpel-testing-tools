package es.uca.webservices.testgen.autoseed.algorithm;

/**
 *	Class in charge of wrapping inner exceptions
 *	in automatic seeding process
 *
 *	@author Valentín Liñeiro Barea
 */
public class AutomaticSeedingException extends Exception {

	///Embedded exception
	private Exception exception;

	///For Throwable elements
	private static final long serialVersionUID = 7713816874815902172L;

	/**
	 * Constructor
	 * @param e
	 */
	public AutomaticSeedingException(Exception exception) {
		this.exception = exception;
	}

	/**
	 * Gets message
	 */
	public String getMessage() {
		return exception.getMessage();
	}
}
