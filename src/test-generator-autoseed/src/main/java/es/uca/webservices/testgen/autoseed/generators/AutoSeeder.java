package es.uca.webservices.testgen.autoseed.generators;

import java.io.OutputStream;
import java.util.List;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.utils.ValidVariablesMap;

/**
 * Interface of AutoSeeder
 *
 * @author Valentín Liñeiro Barea
 */
public interface AutoSeeder {

	/**
	 * Sets seed of PRNG
	 * @param seed
	 */
	void setSeed(String seed);

	/**
	 * Gets test suite created with Automatic Seeding
	 * @param types
	 * @param dict
	 * @param os
	 * @throws GenerationException
	 */
	void getTestSuite(List<IType> types, ValidVariablesMap dict,
			OutputStream os) throws GenerationException;

}
