package es.uca.webservices.testgen.autoseed.check.strategy;

import java.lang.reflect.Constructor;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.constants.Constant;

/**
 * Factory of check strategies
 *
 * @author Valentín Liñeiro Barea
 */
public final class ConcreteCheckStrategyFactory implements ICheckStrategyFactory {

	/// Singleton instance
	private static ICheckStrategyFactory instance;

	/**
	 * Singleton
	 */
	private ConcreteCheckStrategyFactory() {}

	/**
	 * Returns singleton instance
	 * @return
	 */
	public static ICheckStrategyFactory getCheckStrategyFactory() {
		if(instance == null) {
			instance = new ConcreteCheckStrategyFactory();
		}
		return instance;
	}

	@Override
	public ICheckStrategy newCheckStrategy(IType type, Constant cons) {
		/*
		 * Now, we use the Reflections API to choose at runtime the proper check strategy
		 */
		ICheckStrategy strategy;
		try {
			Class<?> cl = Class.forName(this.getClass().getPackage().getName() +
					"." + type.getClass().getSimpleName() + cons.getClass().getSimpleName() +
					"CheckStrategy");
			Constructor<?> con = cl.getConstructor(type.getClass(), cons.getClass());
			strategy = (ICheckStrategy) con.newInstance(type, cons);
		} catch (Exception e) {
			strategy = new DummyCheckStrategy();
		}
		return strategy;
	}

}
