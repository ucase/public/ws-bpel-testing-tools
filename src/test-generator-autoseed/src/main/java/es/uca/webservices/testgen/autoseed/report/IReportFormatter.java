package es.uca.webservices.testgen.autoseed.report;

import java.io.PrintStream;

import javax.xml.transform.TransformerException;

/**
 * Report Formatter Interface
 * 
 * @author Valentín Liñeiro Barea
 */
public interface IReportFormatter {
	
	/**
	 * Format report and save results in the OutputStream indicated
	 * @param report
	 * @param os
	 * @throws TransformerConfigurationException 
	 * @throws TransformerException 
	 */
	void format(Report report, PrintStream os) throws TransformerException;

}
