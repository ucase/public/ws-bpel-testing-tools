package es.uca.webservices.testgen.autoseed.constants;

import javax.xml.datatype.DatatypeConfigurationException;

/**
 * Class of date time constants
 * 
 * @author Valentín Liñeiro Barea
 */
public class DateTimeConstant extends AbstractDateConstant {

	/**
	 * Creates a date time constant
	 * @param cons
	 * @throws DatatypeConfigurationException
	 */
	public DateTimeConstant(String cons) throws DatatypeConfigurationException {
		super();
		if(!cons.matches("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z?")) {
			throw new IllegalArgumentException("Only time values are allowed");
		}
		init(cons);
	}
	
	@Override
	public String toString() {
		return this.getValue().toString();
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof DateTimeConstant) ? 
				((DateTimeConstant) o).getValue().equals(this.getValue()) : false;
	}
	
	@Override
	public int hashCode() {
		return this.getValue().hashCode();
	}

}
