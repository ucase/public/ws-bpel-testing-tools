package es.uca.webservices.testgen.autoseed.source;

import java.util.LinkedList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class with some DOM Utils
 * 
 * @author Valentín Liñeiro Barea
 */
public final class DOMUtils {
	
	/**
	 * Hiding constructor
	 */
	private DOMUtils() {}

	/**
	 * Gets number of element children
	 * @param el
	 * @return
	 */
	public static int getNumberOfElementChildren(Element el) {
		int number = 0;
		NodeList children = el.getChildNodes();
		for(int i = 0; i < children.getLength(); ++i) {
			if(children.item(i).getNodeType() == Node.ELEMENT_NODE) {
				number++;
			}
		}
		return number;
	}

	/**
	 * Check equality between two Elements
	 * @param e1
	 * @param e2
	 * @return
	 */
	public static boolean isEqual(Element e1, Element e2) {
		boolean ok = DOMUtils.checkNSEquality(e1.getNamespaceURI(), e2.getNamespaceURI());		
		ok = ok && e1.getLocalName().equals(e2.getLocalName());
		if(!ok && getNumberOfElementChildren(e1) >= 1) {
			ok = DOMUtils.checkLeftChildrenEquality(e1, e2);
		}
		else if(!ok && getNumberOfElementChildren(e2) >= 1) {
			ok = DOMUtils.checkRightChildrenEquality(e1, e2);
		}
		return ok;
	}

	/**
	 * Checks right children equality between two Elements
	 * @param e1
	 * @param e2
	 * @return
	 */
	private static boolean checkRightChildrenEquality(Element e1, Element e2) {
		boolean ok = false;
		List<Element> children = DOMUtils.getElementChildren(e2);
		for(int i = 0; i < getNumberOfElementChildren(e2) && !ok; ++i) {
			Element child = children.get(i);
			ok = isEqual(e1, child);
		}
		return ok;
	}

	/**
	 * Checks left children equality between two Elements
	 * @param e1
	 * @param e2
	 * @return
	 */
	private static boolean checkLeftChildrenEquality(Element e1, Element e2) {
		boolean ok = false;
		List<Element> children = DOMUtils.getElementChildren(e1);
		for(int i = 0; i < getNumberOfElementChildren(e1) && !ok; ++i) {
			Element child = children.get(i);
			ok = isEqual(child, e2);
		}
		return ok;
	}

	/**
	 * Check NS equality between two namespaces
	 * @param namespaceURI
	 * @param namespaceURI2
	 * @return
	 */
	private static boolean checkNSEquality(String e1, String e2) {
		boolean ok = (e1 == null && e2 == null);
		if(!ok) {
			ok = (e1 != null && e2 != null);
			if(ok) {
				ok = e1.equals(e2);
			}
			else if(e1 == null || e2 == null) {
				ok = true;
			}
		}
		return ok;
	}

	/**
	 * Gets element children 
	 * @param tE
	 * @return
	 */
	public static List<Element> getElementChildren(Element tE) {
		List<Element> elements = new LinkedList<Element>();
		NodeList children = tE.getChildNodes();
		for(int i = 0; i < children.getLength(); ++i) {
			if(children.item(i).getNodeType() == Node.ELEMENT_NODE) {
				elements.add((Element) children.item(i));
			}
		}
		return elements;
	}

}
