package es.uca.webservices.testgen.autoseed.constants;

/**
 * Class with some utils to check type of constants
 * 
 * @author Valentín Liñeiro Barea
 */
public final class ConstantTypeUtils {
	
	/**
	 * Hidding constructor
	 */
	private ConstantTypeUtils() {}

	/**
	 * Checks if a number is integer or float
	 * @param number
	 * @return 
	 */
	public static boolean isFloat(String number) {
		boolean ok = true;
		if(isNumber(number)) {
			Float num = Float.parseFloat(number);
			ok = (num - num.intValue()) > 0.0;
		} else {
			ok = false;
		}
		return ok;
	}
	
	/**
	 * Checks if a string is a number
	 * @param number
	 * @return
	 */
	@SuppressWarnings("unused")
	public static boolean isNumber(String number) {
		boolean ok = true;
		try {
			Float num = Float.parseFloat(number);
		} catch(NumberFormatException e) {
			ok = false;
		}
		return ok;
	}
	
	/**
	 * Checks if a string is a dateTime
	 * @param dT
	 * @return
	 */
	public static boolean isDateTime(String dT) {
		return dT.substring(1, dT.length() - 1).
				matches("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z?");
	}
	
	/**
	 * Checks if a string is a date
	 * @param dT
	 * @return
	 */
	public static boolean isDate(String dT) {
		return dT.substring(1, dT.length() - 1).
				matches("\\d{4}-\\d{2}-\\d{2}");
	}
	
	/**
	 * Checks if a string is a time
	 * @param dT
	 * @return
	 */
	public static boolean isTime(String dT) {
		return dT.substring(1, dT.length() - 1).
				matches("\\d{2}:\\d{2}:\\d{2}Z?");
	}
	
	/**
	 * Checks if a string is a duration
	 * @param dT
	 * @return
	 */
	public static boolean isDuration(String dT) {
		return dT.substring(1, dT.length() - 1).
				matches("P(\\d+Y)?(\\d+M)?(\\d+D)?T(\\d+H)?(\\d+M)?(\\d+S)?");
	}
	
}
