package es.uca.webservices.testgen.autoseed.constants;

import javax.xml.datatype.DatatypeConfigurationException;

/**
 * Class of date constants
 * 
 * @author Valentín Liñeiro Barea
 */
public class DateConstant extends AbstractDateConstant {

	/**
	 * Creates a date constant
	 * @param cons
	 * @throws DatatypeConfigurationException
	 */
	public DateConstant(String cons) throws DatatypeConfigurationException {
		super();
		if(!cons.matches("\\d{4}-\\d{2}-\\d{2}")) {
			throw new IllegalArgumentException("Only date values are allowed");
		}
		init(cons);
	}
	
	@Override
	public String toString() {
		return this.getValue().toString();
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof DateConstant) ? 
				((DateConstant) o).getValue().equals(this.getValue()) : false;
	}
	
	@Override
	public int hashCode() {
		return this.getValue().hashCode();
	}
	
}
