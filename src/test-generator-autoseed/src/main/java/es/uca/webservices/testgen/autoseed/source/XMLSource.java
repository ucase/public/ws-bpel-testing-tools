package es.uca.webservices.testgen.autoseed.source;

import java.util.HashMap;
import java.util.Map;

import org.apache.xmlbeans.XmlObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

/**
 * Class that represents an XML Source
 *
 * @author Valentín Liñeiro Barea
 */
public abstract class XMLSource {

	/// Logger
	private static final Logger LOGGER = LoggerFactory
					.getLogger(XMLSource.class);

	///Namespaces mapping
	private Map<String, String> namespaces;

	/**
	 * @return the namespaces
	 */
	public Map<String, String> getNamespaces() {
		return namespaces;
	}

	/**
	 * Get al namespaces of elements
	 * @return
	 */
	public Map<String, String> getNS() {
		if(namespaces == null) {
			LOGGER.debug("Getting namespaces from XML document");
			namespaces = new HashMap<String, String>();
			start();
		}
		return namespaces;
	}

	/**
	 * Helper method in order to search for all namespaces
	 * @param element
	 * @return
	 */
	protected Map<String, String> getAllNS(XmlObject element) {
		Map<String, String> ns = new HashMap<String, String>();
		if(element.getDomNode() instanceof Element) {
			Element templates = (Element) element.getDomNode();
			NamedNodeMap attrs = templates.getAttributes();
			for(int i = 0; i < attrs.getLength(); ++i) {
				Attr a = (Attr) attrs.item(i);
				if(a.getPrefix() != null && a.getPrefix().equals("xmlns")) {
					LOGGER.debug(a.getLocalName() + " : " + a.getValue());
					ns.put(a.getLocalName(), a.getValue());
				}
			}
		}
		XmlObject[] children = element.selectPath("./*");
		for(XmlObject object: children) {
			ns.putAll(getAllNS(object));
		}
		return ns;
	}

	/**
	 * Starts NS search with the proper model in subclasses
	 */
	protected abstract void start();

}
