package es.uca.webservices.testgen.autoseed.check.strategy;

/**
 * Interface of check strategy
 * 
 * @author Valentín Liñeiro Barea
 */
public interface ICheckStrategy {
	
	/**
	 * Check restrictions of type and constant given
	 * @return
	 */
	boolean checkRestrictions();
	
}
