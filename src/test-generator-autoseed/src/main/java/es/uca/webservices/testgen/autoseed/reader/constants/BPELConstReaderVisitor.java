package es.uca.webservices.testgen.autoseed.reader.constants;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.oasisOpen.docs.wsbpel.x20.process.executable.TActivity;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TAssign;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TBooleanExpr;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TCondition;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TCopy;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TDeadlineExpr;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TDurationExpr;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TElseif;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TEventHandlers;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TForEach;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TFrom;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TIf;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TLiteral;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TOnAlarmEvent;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TOnAlarmPick;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TPick;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TQuery;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TRepeatUntil;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TScope;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TSource;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TSources;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TTargets;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TTo;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TWait;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TWhile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.testgen.autoseed.constants.ConstantTypeUtils;
import es.uca.webservices.testgen.autoseed.source.BPELSource;
import es.uca.webservices.testgen.autoseed.source.XMLUtils;
import es.uca.webservices.testgen.autoseed.utils.GeneralUtils;
import es.uca.webservices.xpath.parser.ParseException;

/**
 * Class in charge of visit a BPEL process to get all constants
 * and related variables
 * 
 * @author Valentín Liñeiro Barea
 */
public class BPELConstReaderVisitor implements ConstReaderVisitor {
	
	///Logger
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BPELConstReaderVisitor.class);
	
	///Assign Literal
	private static final String ASSIGN_STRING = " <- ";
	
	///BPEL Process
	private BPELSource bpel;
	
	///Raw Constants
	private Map<String, Set<String>> rawConstants;
	
	/**
	 * Creates a BPELConstantReaderVisitor with a BPEL Source
	 * @param bpel
	 */
	public BPELConstReaderVisitor(BPELSource bpel) {
		this.bpel = bpel;
	}
	
	/**
	 * Gets BPELSource
	 * @return
	 */
	public BPELSource getBpelSource() {
		return bpel;
	}

	/**
	 * Get all raw constants from a BPEL process
	 * @return
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 */
	public Map<String, Set<String>> getRawConstants() throws XPathExpressionException, ParseException, 
		ParserConfigurationException {
		if(rawConstants == null) {
			LOGGER.debug("Reading raw constants from " + bpel.getModel().getName());
			readRawConstants();
		}
		return rawConstants;
	}

	/**
	 * Read all raw constants from a BPEL process
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 */
	private void readRawConstants() throws XPathExpressionException, ParseException, 
		ParserConfigurationException {
		rawConstants = new HashMap<String, Set<String>>();
		
		for(TActivity act: bpel.getActivities()) {
			LOGGER.debug("Visiting " + act.getDomNode().getLocalName() + " : " + act.getName());
			visit(act);
		}
	}

	/**
	 * Main visitor method
	 * @param act
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 */
	@Override
	public void visit(TActivity act) throws XPathExpressionException, ParseException, 
		ParserConfigurationException {
		TSources sources = act.getSources();
		if (sources != null) {
			for (TSource source : sources.getSourceArray()) {
				TCondition expr = source.getTransitionCondition();
				if (expr != null) {
					LOGGER.debug("Searching in transitionCondition");
					getVariablesAndConstantsFromExpression(
							XMLUtils.getExpression(expr));
				}
			}
		}
		TTargets targets = act.getTargets();
		if (targets != null) {
			TCondition expr = targets.getJoinCondition();
			if (expr != null) {
				LOGGER.debug("Searching in joinCondition");
				getVariablesAndConstantsFromExpression(
						XMLUtils.getExpression(expr));
			}
		}
		
		accept(act);
	}
	
	/**
	 * Built-in accept method
	 * @param act 
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 */
	private void accept(TActivity act) throws XPathExpressionException, ParseException, 
		ParserConfigurationException {
		if(act instanceof TAssign) {
			visit((TAssign) act);
		}
		else if(act instanceof TForEach) {
			visit((TForEach) act);
		}
		else if(act instanceof TIf) {
			visit((TIf) act);
		}
		else if(act instanceof TWhile) {
			visit((TWhile) act);
		}
		else if(act instanceof TRepeatUntil) {
			visit((TRepeatUntil) act);
		}
		else if(act instanceof TPick) {
			visit((TPick) act);
		}
		else if(act instanceof TWait) {
			visit((TWait) act);
		}
		else if(act instanceof TScope) {
			visit((TScope) act);
		}
	}

	/**
	 * Inspects components of an expression
	 * @param components
	 * @return
	 */
	private Pair<String, String> inspectComponents(NodeList components) {
		String cons = "";
		String var = "";
		for (int j = 0; j < components.getLength(); ++j) {
			if (components.item(j).getLocalName().contains("Constant")) {
				cons = components.item(j).getTextContent();
			} else if (components.item(j).getLocalName()
					.contains("function")) {				
				Pair<String, String> results = inspectFunction(components.item(j));
				cons = ("".equals(cons)) ? results.getLeft() : cons;
				var = ("".equals(var)) ? results.getRight() : var;
			} else if (components.item(j).getLocalName()
					.contains("variable")
					|| components.item(j).getLocalName()
							.contains("children")) {
				var = XMLUtils.format(components.item(j), bpel.getNS());
			}
		}
		return new Pair<String, String>(cons, var);
	}

	/**
	 * Inspects function node
	 * @param item
	 * @return
	 */
	private Pair<String, String> inspectFunction(Node item) {
		String cons = "";
		String var = "";
		String name = ((Element) item.getFirstChild())
				.getAttribute("localPart");
		if (name.equals("true") || name.equals("false")) {
			cons = "'" + name + "'";
		} else if (name.equals("contains")) {
			var = XMLUtils.format(item.getChildNodes().item(1), bpel.getNS());
			cons = item.getLastChild()
					.getTextContent();
		} else if (name.equals("getVariableProperty")) {
			String varName = item.getChildNodes()
					.item(1).getTextContent();
			String propName = item.getLastChild()
					.getTextContent();
			var = "$" + varName.substring(1, varName.length() - 1)
					+ "-"
					+ propName.substring(1, propName.length() - 1);
		} else if (!name.equals("count")) {
			var = XMLUtils.format(item, bpel.getNS());
		}
		
		return new Pair<String, String>(cons, var);
	}

	@Override
	public void visit(TScope act) {
		TEventHandlers eventHandlers = act.getEventHandlers();
		if (eventHandlers != null) {
			for (TOnAlarmEvent onAlarm : eventHandlers.getOnAlarmArray()) {
				String expr = getDurationExpression(onAlarm.getFor(), onAlarm.getUntil());
				if (!"".equals(expr)) {
					LOGGER.debug(expr);
					GeneralUtils.putNew(rawConstants, expr, null);
				}
			}
		}
	}

	@Override
	public void visit(TWait act) {
		String expr = getDurationExpression(act.getFor(), act.getUntil());
		if (!"".equals(expr)) {
			LOGGER.debug(expr);
			GeneralUtils.putNew(rawConstants, expr, null);
		}
	}

	/**
	 * Gets constant from duration expression
	 * @param for1
	 * @param until
	 * @return
	 */
	private String getDurationExpression(TDurationExpr for1, TDeadlineExpr until) {
		String expr = "";
		if (for1 != null) {
			expr = XMLUtils.getExpression(for1);
		}
		if (until != null) {
			expr = XMLUtils.getExpression(until);
		}
		return expr;
	}

	@Override
	public void visit(TPick act) {
		for (TOnAlarmPick onAlarm : act.getOnAlarmArray()) {
			String expr = getDurationExpression(onAlarm.getFor(), onAlarm.getUntil());
			if (!"".equals(expr)) {
				LOGGER.debug(expr);
				GeneralUtils.putNew(rawConstants, expr, null);
			}
		}
	}
	
	/**
	 * Gets condition expression
	 * @param expr
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 */
	private void getConditionExpression(TBooleanExpr expr) throws XPathExpressionException, ParseException, ParserConfigurationException {
		getVariablesAndConstantsFromExpression(XMLUtils.getExpression(expr));
	}

	@Override
	public void visit(TRepeatUntil act) throws XPathExpressionException, ParseException, 
		ParserConfigurationException {
		getConditionExpression(act.getCondition());
	}

	@Override
	public void visit(TWhile act) throws XPathExpressionException, ParseException, 
		ParserConfigurationException {
		getConditionExpression(act.getCondition());
	}

	@Override
	public void visit(TIf act) throws XPathExpressionException, ParseException, 
		ParserConfigurationException {
		getConditionExpression(act.getCondition());
		for (TElseif ei : act.getElseifArray()) {
			getConditionExpression(ei.getCondition());
		}
	}

	@Override
	public void visit(TForEach act) throws XPathExpressionException, ParseException, ParserConfigurationException {
		String var = "$" + act.getCounterName();
		String consStartCV = getConstants(XMLUtils.getExpression(
				act.getStartCounterValue()));
		if (!consStartCV.equals("")) {
			LOGGER.debug(consStartCV + ASSIGN_STRING + var);
			GeneralUtils.putNew(rawConstants, consStartCV, var);
		}
		String consFinalCV = getConstants(XMLUtils.getExpression(
				act.getFinalCounterValue()));
		if (!consFinalCV.equals("")) {
			LOGGER.debug(consFinalCV + ASSIGN_STRING + var);
			GeneralUtils.putNew(rawConstants, consFinalCV, var);
		}
	}

	@Override
	public void visit(TAssign act) throws XPathExpressionException, ParseException, ParserConfigurationException {
		for (TCopy copy : act.getCopyArray()) {
			LOGGER.debug("Visiting copy");
			TFrom from = copy.getFrom();
			if (from.getVariable() == null && from.getPartnerLink() == null) {
				String varExpr = getVariableFromTo(copy.getTo());
				LOGGER.debug("Variable found in to: " + varExpr);
				if (from.getLiteral() == null) {
					LOGGER.debug("Searching in from expression");
					String cons = getConstants(XMLUtils.getExpression(from));
					if (!cons.equals("")) {
						LOGGER.debug(cons + ASSIGN_STRING + varExpr);
						GeneralUtils.putNew(rawConstants, cons, varExpr);
					}
				} else {
					LOGGER.debug("Searching in from literal expression");
					TLiteral lit = from.getLiteral();
					String aux = "";
					Set<String> exprs = new TreeSet<String>();
					XMLUtils.formatLiteral(lit.getDomNode(), aux, exprs);
					Set<Pair<String, String>> result = XMLUtils.getQueryAndConstant(exprs, bpel.getNS());
					for(Pair<String, String> res: result) {
						if(!res.getLeft().equals("")) {
							String cons = (ConstantTypeUtils.isNumber(res.getLeft())) ? res.getLeft() : "'" + res.getLeft() + "'";
							LOGGER.debug(cons + ASSIGN_STRING + varExpr +  res.getRight());
							GeneralUtils.putNew(rawConstants, cons, varExpr + res.getRight());
						}
					}
				}
			}
		}
	}
	
	/**
	 * Get variable from to
	 * @param to
	 * @return
	 */
	private String getVariableFromTo(TTo to) {
		String varExpr = "";
		if (to.getPartnerLink() == null) {
			if (to.getVariable() != null) {
				if (to.getProperty() != null) {
					LOGGER.debug("Getting variable with property");
					varExpr = getVariableFromProperty(to.getProperty(), to.getVariable());
				} else {
					LOGGER.debug("Getting variable with part and query");
					varExpr = getVariableFromPartAndQuery(to.getPart(), 
							to.getQuery(), to.getVariable());
				}
			} else {
				LOGGER.debug("Getting variable from expression");
				varExpr = getVariableFromExpression(to);
			}
		}
		return varExpr;
	}
	
	/**
	 * Gets variable from expression
	 * @param to
	 * @return
	 */
	private String getVariableFromExpression(TTo to) {
		String varExpr = XMLUtils.getExpression(to);
		LOGGER.debug("Getting variable from expression: " + varExpr);
		return XMLUtils.changeNSPrefixesForURIs(varExpr, bpel.getNS());
	}

	/**
	 * Gets variable name from part and query
	 * @param part
	 * @param query
	 * @param variable
	 * @return
	 */
	private String getVariableFromPartAndQuery(String p, TQuery q, String variable) {
		String varExpr = "";
		String part = (p != null) ? "." + p : "";
		String query = (q != null) ? ((XMLUtils.getExpression(q).startsWith("/")) ? 
				XMLUtils.getExpression(q) : "/" + XMLUtils.getExpression(q))
				: "";
		varExpr = "$" + variable+ part + query;
		return XMLUtils.changeNSPrefixesForURIs(varExpr, bpel.getNS());
	}

	/**
	 * Get variable name from variable property
	 * @param property
	 * @param variable
	 * @return
	 */
	private String getVariableFromProperty(QName property, String variable) {
		LOGGER.debug("Getting variable from property: " + property);
		String pre = (property.getPrefix().equals("")) ? ""
				: property.getPrefix() + ":";
		return "$" + variable + "-" + pre + property.getLocalPart();
	}

	/**
	 * Gets constants from an expression
	 * @param expression
	 * @return
	 * @throws ParseException
	 * @throws ParserConfigurationException
	 * @throws XPathExpressionException
	 */
	private String getConstants(String expression) throws ParseException, ParserConfigurationException, 
		XPathExpressionException {
		String cons = "";
		NodeList results = bpel.getNodeListFromExpression(expression, 
				BPELConstReaderConstants.XPATH_CONSTANT_EXPRESSION);
		for (int i = 0; i < results.getLength(); ++i) {
			if (results.item(i).getLocalName().equals("qname")) {
				cons = "'"
						+ ((Element) results.item(i)).getAttribute("localPart")
						+ "'";
			} else {
				cons = results.item(i).getTextContent();
			}
		}
		return cons;
	}
	
	/**
	 * Gets variables and constants from an expression
	 * @param expression
	 * @param val
	 * @throws ParseException
	 * @throws ParserConfigurationException
	 * @throws XPathExpressionException
	 */
	private void getVariablesAndConstantsFromExpression(String expression) 
			throws ParseException, ParserConfigurationException, XPathExpressionException {
		LOGGER.debug("Current expression: " + expression);
		NodeList results = bpel.getNodeListFromExpression(expression, 
				BPELConstReaderConstants.XPATH_CONSTANT_VARIABLE_EXPRESSION);
		for (int i = 0; i < results.getLength(); ++i) {
			Pair<String, String> consAndVar = inspectComponents(results.item(i).getChildNodes());
			if (!consAndVar.getLeft().equals("") && !consAndVar.getRight().equals("")) {
				LOGGER.debug("Constant found: " + consAndVar.getLeft());
				LOGGER.debug("Variable found: " + consAndVar.getRight());
				LOGGER.debug(consAndVar.getLeft() + ASSIGN_STRING + consAndVar.getRight());
				GeneralUtils.putNew(rawConstants, consAndVar.getLeft(), consAndVar.getRight());
			}
		}
	}
}
