package es.uca.webservices.testgen.autoseed.reader.constants;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.oasisOpen.docs.wsbpel.x20.process.executable.TActivity;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TAssign;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TForEach;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TIf;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TPick;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TRepeatUntil;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TScope;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TWait;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TWhile;

import es.uca.webservices.xpath.parser.ParseException;

/**
 * ConstReader Visitor Interface
 * 
 * @author Valentín Liñeiro Barea
 */
public interface ConstReaderVisitor {

	/**
	 * Read constants and related variables from an unknown activity
	 * (double dispatch)
	 * @param act
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 */
	void visit(TActivity act) throws XPathExpressionException, ParseException, ParserConfigurationException;
	
	/**
	 * Read constants and related variables from an <assign> activity
	 * @param act
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 */
	void visit(TAssign act) throws XPathExpressionException, ParseException, ParserConfigurationException;
	
	/**
	 * Read constants and related variables from a <forEach> activity
	 * @param act
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 */
	void visit(TForEach act) throws XPathExpressionException, ParseException, ParserConfigurationException;
	
	/**
	 * Read constants and related variables from an <if> activity
	 * @param act
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 */
	void visit(TIf act) throws XPathExpressionException, ParseException, ParserConfigurationException;
	
	/**
	 * Read constants and related variables from a <while> activity
	 * @param act
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 */
	void visit(TWhile act) throws XPathExpressionException, ParseException, ParserConfigurationException;
	
	/**
	 * Read constants and related variables from a <repeatUntil> activity
	 * @param act
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 */
	void visit(TRepeatUntil act) throws XPathExpressionException, ParseException, ParserConfigurationException;
	
	/**
	 * Read constants and related variables from a <pick> activity
	 * @param act
	 */
	void visit(TPick act);
	
	/**
	 * Read constants and related variables from a <wait> activity
	 * @param act
	 */
	void visit(TWait act);
	
	/**
	 * Read constants and related variables from an <scope> activity
	 * @param act
	 */
	void visit(TScope act);
}
