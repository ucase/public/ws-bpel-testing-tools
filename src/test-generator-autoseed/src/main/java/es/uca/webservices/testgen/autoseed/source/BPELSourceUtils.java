package es.uca.webservices.testgen.autoseed.source;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.wsdl.Definition;
import javax.wsdl.Fault;
import javax.wsdl.Input;
import javax.wsdl.Message;
import javax.wsdl.Operation;
import javax.wsdl.Output;
import javax.wsdl.Part;
import javax.wsdl.Port;
import javax.wsdl.Service;
import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;

import org.oasisOpen.docs.wsbpel.x20.process.executable.TVariable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.ibm.wsdl.util.xml.DOMUtils;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import serviceAnalyzer.messageCatalog.TypeFault;
import serviceAnalyzer.messageCatalog.TypeOperation;
import serviceAnalyzer.messageCatalog.TypeTemplate;
import es.uca.webservices.bpel.xml.XMLDocument;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.parsers.sanalyzer.SACatalogUtils;

/**
 * Class with some utils to BPELSource objects
 * 
 * @author Valentín Liñeiro Barea
 */
public final class BPELSourceUtils {
	
	/// Logger
	private static final Logger LOGGER = LoggerFactory
				.getLogger(BPELSourceUtils.class);
	
	/**
	 * Hidding constructor
	 */
	private BPELSourceUtils() {}

	/**
	 * Gets info for messageType given (service name, operation name, direction and fault name)
	 * @param definitions
	 * @param varMessage
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String[] findMessageInfo(List<Definition> definitions, QName varMessage) {

		String[] info = new String[] {"", "", ""};
		
		for(Definition d: definitions) {
			if("".equals(info[2])) {
				for(Service s: (Collection<Service>) d.getAllServices().values()) {
					if("".equals(info[2])) {
						info[0] = s.getQName().getLocalPart();
						for(Port p: (Collection<Port>) s.getPorts().values()) {
							for(Operation op: (List<Operation>) p.getBinding().getPortType().getOperations()) {
								if("".equals(info[2])) {
									info[1] = op.getName();
									info[2] = findMessageDirection(op, varMessage);		
								}
							}
						}
					}
				}
			}
		}
		
		return info;
	}

	/**
	 * Gets message direction of message
	 * @param op
	 * @param varMessage 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static String findMessageDirection(Operation op, QName varMessage) {
		String direction = findMessageDirectionInInput(op.getInput(), varMessage);
		if("".equals(direction)) {
			direction = findMessageDirectionInOutput(op.getOutput(), varMessage);
		}
		if("".equals(direction)) {
			direction = findMessageIntoFaults((Collection<Fault>) op.getFaults().values(), 
					varMessage);
		}
		return direction;
	}
	
	/**
	 * Finds message direction into faults of an operation
	 * @param values
	 * @param varMessage
	 * @return
	 */
	private static String findMessageIntoFaults(Collection<Fault> values,
			QName varMessage) {
		String direction = "";
		for(Fault f: values) {
			Message faultMessage = f.getMessage();
			if(varMessage.equals(faultMessage.getQName())) {
				direction = f.getName();
				break;
			}
		}
		return direction;
	}

	/**
	 * Finds message direction into output of an operation
	 * @param output
	 * @param varMessage
	 * @return
	 */
	private static String findMessageDirectionInOutput(Output output,
			QName varMessage) {
		String direction = "";
		if(output != null) {
			Message outputMessage = output.getMessage();
			if(outputMessage != null && varMessage.equals(outputMessage.getQName())) {
				direction = "OUT";
			}
		}
		return direction;
	}

	/**
	 * Finds message direction into input of an operation
	 * @param input
	 * @param varMessage
	 * @return
	 */
	private static String findMessageDirectionInInput(Input input,
			QName varMessage) {
		String direction = "";
		if(input != null) {
			Message inputMessage = input.getMessage();
			if(inputMessage != null && varMessage.equals(inputMessage.getQName())) {
				direction = "IN";
			}
		}
		return direction;
	}

	/**
	 * Gets template element from ServiceAnalyzer catalog and info given
	 * @param catalog
	 * @param info
	 * @return
	 * @throws ParserException 
	 */
	public static TypeTemplate getTemplate(ServicesDocument catalog,
			String[] info) throws ParserException {
		TypeTemplate temp = null;
		
		if(catalog != null) {
			TypeOperation operation = SACatalogUtils.findOperationByName(
				SACatalogUtils.findServiceByName(catalog.getServices(), info[0]), info[1]);
			if(info[2].equals("IN")) {
				temp = operation.getInput().getTemplate();
			}
			else if(info[2].equals("OUT")) {
				temp = operation.getOutput().getTemplate();
			}
			else {
				for(TypeFault fault: operation.getFaultArray()) {
					if(fault.getName().equals(info[2])) {
						temp = fault.getTemplate();
						break;
					}
				}
			}
		}
		
		return temp;
	}

	/**
	 * Gets complete name of a variable
	 * @param var
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<String> getNameWithPart(List<Definition> definitions, TVariable var) {
		List<String> names = new LinkedList<String>();
		String name = "$" + var.getName();
		QName messageType = var.getMessageType();
		LOGGER.debug("Message type: " + messageType);
		if(messageType != null) {
			for(Definition d: definitions) {
				Message mes = d.getMessage(messageType);
				if(mes != null) {
					List<Part> parts = mes.getOrderedParts(null);
					for(Part part: parts) {
						names.add(name + "." + part.getName());
					}
				}
			}
		}
		return names;
	}

	/**
	 * Gets template DOM Element from ServiceAnalyzer TypeTemplate
	 * @param template
	 * @return
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	public static Element getTemplateElement(TypeTemplate temp) throws SAXException, 
		IOException, ParserConfigurationException {
		Element elem = null;
		
		if(temp != null) {
			String finalTemp = XMLUtils.clean(temp.getStringValue().trim());
			elem = new XMLDocument(finalTemp).getDocument().getDocumentElement();
			if(es.uca.webservices.testgen.autoseed.source.DOMUtils.getNumberOfElementChildren(elem) == 1) {
				Element child = DOMUtils.getFirstChildElement(elem);
				if(es.uca.webservices.testgen.autoseed.source.DOMUtils.getNumberOfElementChildren(child) > 0) {
					elem = child;
				}
			}
		}
		
		return elem;
	}

}
