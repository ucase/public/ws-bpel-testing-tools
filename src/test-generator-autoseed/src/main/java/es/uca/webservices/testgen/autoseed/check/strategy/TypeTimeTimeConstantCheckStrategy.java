package es.uca.webservices.testgen.autoseed.check.strategy;

import es.uca.webservices.testgen.api.types.TypeTime;
import es.uca.webservices.testgen.autoseed.constants.TimeConstant;

/**
 * Time check restrictions strategy
 * 
 * @author Valentín Liñeiro Barea
 */
public class TypeTimeTimeConstantCheckStrategy implements ICheckStrategy {
	
	/// Type
	private TypeTime typeTime;
	
	/// Constant
	private TimeConstant constTime;
	
	/**
	 * Creates a time check strategy
	 * @param tT
	 * @param tC
	 */
	public TypeTimeTimeConstantCheckStrategy(TypeTime tT, TimeConstant tC) {
		this.typeTime = tT;
		this.constTime = tC;
	}

	@Override
	public boolean checkRestrictions() {
		boolean ok = true;
		if(typeTime.getMinValue().compare(constTime.getValue()) > 0) {
			ok = false;
		}
		if(ok && typeTime.getMaxValue().compare(constTime.getValue()) < 0) {
			ok = false;
		}
		return ok;
	}

}
