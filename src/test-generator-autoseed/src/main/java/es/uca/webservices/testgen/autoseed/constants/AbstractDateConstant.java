package es.uca.webservices.testgen.autoseed.constants;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Abstract class of date-related constants
 * 
 * @author Valentín Liñeiro Barea
 */
public abstract class AbstractDateConstant implements Constant {
	
	/// Value
	private XMLGregorianCalendar value;
	
	/// Factory of date values
	private DatatypeFactory factory;
	
	/**
	 * Initialize factory of values
	 * @throws DatatypeConfigurationException
	 */
	public AbstractDateConstant() throws DatatypeConfigurationException {
		this.factory = DatatypeFactory.newInstance();
	}
	
	/**
	 * Initialize date constant value
	 * @param cons
	 */
	protected void init(String cons) {
		this.value = factory.newXMLGregorianCalendar(cons);
	}

	@Override
	public XMLGregorianCalendar getValue() {
		return value;
	}
	
}
