package es.uca.webservices.testgen.autoseed.reader.constants;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.constants.ConstantFactory;
import es.uca.webservices.testgen.autoseed.source.BPELSource;
import es.uca.webservices.xpath.parser.ParseException;

public class BPELConstReader implements ConstReader {
	
	///Logger
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BPELConstReader.class);
	
	///BPEL process visitor
	private BPELConstReaderVisitor visitor;
	
	///Constants and related variables
	private Map<Constant, Set<String>> constants;
	
	/**
	 * Creates a BPELConstReader with a BPELSource
	 * @param bpel
	 */
	public BPELConstReader(BPELSource bpel) {
		this.visitor = new BPELConstReaderVisitor(bpel);
	}
	
	@Override
	public Map<Constant, Set<String>> getConstants() throws XPathExpressionException, ParseException,
		ParserConfigurationException, DatatypeConfigurationException {
		if(constants == null) {
			LOGGER.debug("Reading constants from " + visitor.getBpelSource().getModel().getName());
			readConstants();
		}
		return constants;
	}

	/**
	 * Read constants from BPEL source
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 * @throws DatatypeConfigurationException 
	 */
	private void readConstants() throws XPathExpressionException, ParseException, 
		ParserConfigurationException, DatatypeConfigurationException {
		Map<String, Set<String>> rawConstants = visitor.getRawConstants();
		constants = new HashMap<Constant, Set<String>>();
		for(Entry<String, Set<String>> en: rawConstants.entrySet()) {
			LOGGER.debug("Discovering type of constants");
			putNewConstant(ConstantFactory.getConstantFactory().newConstant(en.getKey()), en.getValue());
		}
	}

	/**
	 * Puts a new constant in the constants map
	 * @param newConstant
	 * @param value
	 */
	private void putNewConstant(Constant newConstant,
			Set<String> value) {
		if(!constants.containsKey(newConstant)) {
			constants.put(newConstant, new HashSet<String>());
		}
		constants.get(newConstant).addAll(value);
	}
}
