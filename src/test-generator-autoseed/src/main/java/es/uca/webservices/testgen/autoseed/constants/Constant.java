package es.uca.webservices.testgen.autoseed.constants;

/**
 *	Interface of constants in BPEL Process
 */
public interface Constant {	
	/**
	 * Gets the value of the constant
	 * @return
	 */
	Object getValue();

}
