package es.uca.webservices.testgen.autoseed.check.strategy;

import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.autoseed.constants.StringConstant;

/**
 * String check restrictions strategy
 * 
 * @author Valentín Liñeiro Barea
 */
public class TypeStringStringConstantCheckStrategy implements ICheckStrategy {
	
	/// Type
	private TypeString typeString;
	
	/// Constant
	private StringConstant constString;
	
	/**
	 * Creates a string check strategy
	 * @param tS
	 * @param sC
	 */
	public TypeStringStringConstantCheckStrategy(TypeString tS, StringConstant sC) {
		this.typeString = tS;
		this.constString = sC;
	}

	@Override
	public boolean checkRestrictions() {
		boolean ok = true;
		if (typeString.getListValues().size() > 0 && 
				!typeString.getListValues().contains(constString.getValue())) {
			ok = false;
		}
		if (ok && typeString.isSetPattern() && 
				!constString.getValue().matches(typeString.getPattern())) {
			ok = false;
		}
		if (ok && typeString.getMinLength() > constString.getValue().length()) {
			ok = false;
		}
		if (ok && typeString.getMaxLength() < constString.getValue().length()) {
			ok = false;
		}
		return ok;
	}

}
