package es.uca.webservices.testgen.autoseed.report;

import java.io.PrintStream;
import java.util.Collection;
import java.util.Date;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.CellStyle.AbbreviationStyle;
import org.nocrala.tools.texttablefmt.CellStyle.HorizontalAlign;
import org.nocrala.tools.texttablefmt.CellStyle.NullStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.constants.Constant;

/**
 * Text Plain Report Formatter
 *
 * @author Valentín Liñeiro Barea
 */
public class PLAINReportFormatter extends AbstractReportFormatter {

	///Header Cell Style
	private CellStyle headerStyle;

	///Data Cell Style
	private CellStyle dataStyle;

	///Info Table
	private Table infoTable;

	///Constants Table
	private Table constantsTable;

	///Variables Table
	private Table variablesTable;

	///Mapping Table
	private Table mappingTable;

	///Valid Variables Table
	private Table validVariablesTable;

	/// Separator Character
	private static final char CHAR_SEPARATOR = '-';

	/// Number of Columns
	private static final int TWO_COLUMNS = 2;
	private static final int THREE_COLUMNS = 3;

	/**
	 * Creates a Text Plain Report Formatter
	 * @param reportLevel
	 */
	public PLAINReportFormatter(ReportLevels reportLevel) {
		super(reportLevel);
		headerStyle = new CellStyle(HorizontalAlign.center, AbbreviationStyle.crop, NullStyle.emptyString);
		dataStyle = new CellStyle(HorizontalAlign.left, AbbreviationStyle.crop, NullStyle.emptyString);
		infoTable = new Table(TWO_COLUMNS, BorderStyle.CLASSIC, ShownBorders.ALL, false, "");
		constantsTable = new Table(THREE_COLUMNS, BorderStyle.CLASSIC, ShownBorders.HEADER_AND_COLUMNS, false, "");
		variablesTable = new Table(TWO_COLUMNS, BorderStyle.CLASSIC, ShownBorders.HEADER_AND_COLUMNS, false, "");
		mappingTable = new Table(TWO_COLUMNS, BorderStyle.CLASSIC, ShownBorders.HEADER_AND_COLUMNS, false, "");
		validVariablesTable = new Table(THREE_COLUMNS, BorderStyle.CLASSIC, ShownBorders.HEADER_AND_COLUMNS, false, "");
	}

	@Override
	public void formatStart(PrintStream os) {
		String title = "Automated Seeding Process Report";
		String separator = fill(CHAR_SEPARATOR, title.length());
		os.println(title);
		os.println(separator);
		os.println();
	}

	@Override
	public void formatEnd(PrintStream os) {
	}

	@Override
	public void formatInformationStart(PrintStream os) {
		String title = "General Information";
		String separator = fill(CHAR_SEPARATOR, title.length());
		os.println(title);
		os.println(separator);
		os.println();
		infoTable.addCell(" Report Date ", dataStyle);
		infoTable.addCell(" " + new Date().toString() + " ", dataStyle);
	}

	@Override
	public void formatInformationBPEL(String bpelPath, PrintStream os) {
		infoTable.addCell(" BPEL Process ", dataStyle);
		infoTable.addCell(" " + bpelPath + " ", dataStyle);
	}

	@Override
	public void formatInformationSpec(String specPath, PrintStream os) {
		infoTable.addCell(" Spec Specification ", dataStyle);
		infoTable.addCell(" " + specPath + " ", dataStyle);
	}

	@Override
	public void formatInformationBPTS(String bptsPath, PrintStream os) {
		infoTable.addCell(" BPTS Test Suite ", dataStyle);
		infoTable.addCell(" " + bptsPath + " ", dataStyle);
	}

	@Override
	public void formatInformationEnd(PrintStream os) {
		os.println(infoTable.render());
		os.println();
	}

	@Override
	public void formatConstantsStart(PrintStream os) {
		String title = "Constants Report";
		String separator = fill(CHAR_SEPARATOR, title.length());
		os.println(title);
		os.println(separator);
		os.println();
		constantsTable.addCell(" Constant ", headerStyle);
		constantsTable.addCell(" Type ", headerStyle);
		constantsTable.addCell(" BPEL Variable ", headerStyle);
	}

	@Override
	public void formatConstantsElement(Entry<Constant, Set<String>> en,
			PrintStream os) {
		String typeClassName = en.getKey().getClass().getSimpleName();
		String typeName = typeClassName.substring(0, typeClassName.lastIndexOf('C'));
		for(String var: en.getValue()) {
			constantsTable.addCell(" " + en.getKey().getValue().toString() + " ", dataStyle);
			constantsTable.addCell(" " + typeName + " ", dataStyle);
			constantsTable.addCell(" " + var + " ", dataStyle);
		}
	}

	@Override
	public void formatConstantsEnd(PrintStream os) {
		os.println(constantsTable.render());
		os.println();
	}

	@Override
	public void formatVariablesStart(PrintStream os) {
		String title = "Basic Variables Report";
		String separator = fill(CHAR_SEPARATOR, title.length());
		os.println(title);
		os.println(separator);
		os.println();
		variablesTable.addCell(" Variable ", headerStyle);
		variablesTable.addCell(" Type ", headerStyle);
	}

	@Override
	public void formatVariablesElement(IType var, PrintStream os) {
		String typeClassName = var.getClass().getSimpleName();
		String typeName = typeClassName.substring(typeClassName.indexOf('e') + 1);
		variablesTable.addCell(" " + var.getNameVariable() + " ", dataStyle);
		variablesTable.addCell(" " + typeName + " ", dataStyle);
	}

	@Override
	public void formatVariablesEnd(PrintStream os) {
		os.println(variablesTable.render());
		os.println();
	}

	@Override
	public void formatMappingStart(PrintStream os) {
		String title = "Mapping Report";
		String separator = fill(CHAR_SEPARATOR, title.length());
		os.println(title);
		os.println(separator);
		os.println();
		mappingTable.addCell(" Spec Variable ", headerStyle);
		mappingTable.addCell(" BPEL Variable ", headerStyle);
	}

	@Override
	public void formatMappingElement(Entry<String, Set<String>> en,
			PrintStream os) {
		for(String var: en.getValue()) {
			mappingTable.addCell(" " + en.getKey() + " ", dataStyle);
			mappingTable.addCell(" " + var + " ", dataStyle);
		}
	}

	@Override
	public void formatMappingEnd(PrintStream os) {
		os.println(mappingTable.render());
		os.println();
	}

	@Override
	public void formatValidVariablesStart(PrintStream os) {
		String title = "Valid variables and related constants";
		String separator = fill('-', title.length());
		os.println(title);
		os.println(separator);
		os.println();
		validVariablesTable.addCell(" Name ", headerStyle);
		validVariablesTable.addCell(" Type ", headerStyle);
		validVariablesTable.addCell(" Constants ", headerStyle);
	}

	@Override
	public void formatValidVariablesElement(
			Entry<IType, Pair<SortedSet<String>, Set<Constant>>> en,
			PrintStream os) {
		String typeClassName = en.getKey().getClass().getSimpleName();
		String typeName = typeClassName.substring(typeClassName.indexOf('e') + 1);
		for(String var: en.getValue().getLeft()) {
			validVariablesTable.addCell(" " + var + " ", dataStyle);
			validVariablesTable.addCell(" " + typeName + " ", dataStyle);
			validVariablesTable.addCell(" " + PLAINReportFormatter.getSeparatedValues(en.getValue().getRight(), ", ") + " ", dataStyle);
		}
	}

	@Override
	public void formatValidVariablesEnd(PrintStream os) {
		os.println(validVariablesTable.render());
		os.println();
	}

	@Override
	public void formatNumberOfTestCasesStart(PrintStream os) {
		String title = "Number of test cases";
		String separator = fill('-', title.length());
		os.println(title);
		os.println(separator);
		os.println();
	}

	@Override
	public void formatNumberOfTestCasesElement(int numberOfTestCases,
			PrintStream os) {
		os.println(numberOfTestCases);
	}

	@Override
	public void formatNumberOfTestCasesEnd(PrintStream os) {
		os.println();
	}

	/**
	 * Gets String with values of a collection separated by separator
	 * @param collection
	 * @param separator
	 * @return
	 */
	public static <T> String getSeparatedValues(Collection<T> collection, String separator) {
		String values = "";
		int currentIndex = 0;
		int size = collection.size();
		for(T t: collection) {
			String sep = (currentIndex != size - 1) ? separator : "";
			values = values + t.toString() + sep;
			currentIndex++;
		}
		return values;
	}

	/**
	 * Gets a line with the character given
	 * @param c
	 * @param n
	 * @return
	 */
	public static String fill(char c, int n) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < n; ++i) {
			sb.append(c);
		}
		return sb.toString();
	}
}
