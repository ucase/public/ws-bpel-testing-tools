package es.uca.webservices.testgen.autoseed.check.algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

/**
 * Class with mapping utils
 *
 * @author Valentín Liñeiro Barea
 */
public final class MappingUtils {

	// Hiding utility class constructor
	private MappingUtils() {}

	/**
	 * Takes a map and returns the inverse map
	 * @param map
	 * @return
	 */
	public static <K, V> Map<V, Set<K>> getInverseSetMap(Map<K, Set<V>> map) {
		Map<V, Set<K>> newMap = new HashMap<V, Set<K>>();
		for(Entry<K, Set<V>> en: map.entrySet()) {
			for(V v: en.getValue()) {
				if(!newMap.containsKey(v)) {
					newMap.put(v, new HashSet<K>());
				}
				newMap.get(v).add(en.getKey());
			}
		}
		return newMap;
	}
}
