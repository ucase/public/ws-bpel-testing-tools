package es.uca.webservices.testgen.autoseed.reader.variables;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.parsers.IParser;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.parsers.spec.xtext.integration.XtextSpecParser;

/**
 * Class in charge of reading variables from an spec
 * 
 * @author Valentín Liñeiro Barea
 */
public class SpecReader implements TypeReader {
	
	/// Logger
	private static final Logger LOGGER = LoggerFactory.getLogger(SpecReader.class);
	
	/// Spec file parser
	private IParser spec;
	
	/// Type visitor
	private TypeVisitor visitor;
	
	/// Original variables
	private List<IType> types;
	
	/// Original variables + inner variables
	private List<IType> allTypes;
	
	/**
	 * Creates a spec reader given path to spec file
	 * @param specPath
	 * @throws TypeReaderException 
	 */
	public SpecReader(String specPath) {
		this.spec = new XtextSpecParser(specPath);
		this.visitor = new TypeVisitor();
	}
	
	/**
	 * Helper method that reads types from spec
	 * @throws ParserException 
	 */
	private void readTypes() throws ParserException {
		LOGGER.debug("Initializing set of real types");
		types = spec.parse();
	}
	
	/**
	 * Helper method that reads inner types from spec
	 * @throws GenerationException 
	 * @throws ParserException 
	 * @throws TypeReaderException
	 */
	private void searchInnerTypes() throws ParserException, GenerationException {
		LOGGER.debug("Initializing set of variables");
		allTypes = new ArrayList<IType>();
		for(IType type: this.getTypes()) {
			LOGGER.debug("Begin of variable");
			allTypes.addAll((List <IType>) visitor.visit(type));
			LOGGER.debug("End of variable");
		}
	}
	
	@Override
	public List<IType> getTypes() throws ParserException {
		if(types == null)
			readTypes();
		return types;
	}

	@Override
	public List<IType> getSimpleTypes() throws ParserException, GenerationException {
		if(allTypes == null)
			searchInnerTypes();
		return allTypes;
	}
	
}
