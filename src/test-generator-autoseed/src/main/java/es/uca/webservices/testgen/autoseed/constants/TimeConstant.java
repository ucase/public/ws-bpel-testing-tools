package es.uca.webservices.testgen.autoseed.constants;

import javax.xml.datatype.DatatypeConfigurationException;

/**
 * Class of Time constants
 * 
 * @author Valentín Liñeiro Barea
 */
public class TimeConstant extends AbstractDateConstant {

	/**
	 * Creates a time constant
	 * @param cons
	 * @throws DatatypeConfigurationException
	 */
	public TimeConstant(String cons) throws DatatypeConfigurationException {
		super();
		if(!cons.matches("\\d{2}:\\d{2}:\\d{2}Z?")) {
			throw new IllegalArgumentException("Only time values are allowed");
		}
		init(cons);
	}
	
	@Override
	public String toString() {
		return this.getValue().toString();
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof TimeConstant) ? 
				((TimeConstant) o).getValue().equals(this.getValue()) : false;
	}
	
	@Override
	public int hashCode() {
		return this.getValue().hashCode();
	}

}
