package es.uca.webservices.testgen.autoseed.check.strategy;

/**
 * Dummy check restrictions strategy
 * 
 * @author Valentín Liñeiro Barea
 */
public class DummyCheckStrategy implements ICheckStrategy {

	//By default, not valid
	@Override
	public boolean checkRestrictions() {
		return false;
	}

}
