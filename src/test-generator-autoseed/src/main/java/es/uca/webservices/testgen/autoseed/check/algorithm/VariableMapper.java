package es.uca.webservices.testgen.autoseed.check.algorithm;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.xpath.parser.ParseException;

/**
 * Interface of Variable mapper
 *
 * @author Valentín Liñeiro Barea
 */
public interface VariableMapper {

	/**
	 * Gets mapping
	 * @return
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserException
	 * @throws ParserConfigurationException
	 * @throws ParseException
	 * @throws XPathExpressionException
	 * @throws VariableMapperException
	 */
	Map<String, Set<String>> getMapping() throws XPathExpressionException,
			ParseException, ParserConfigurationException, ParserException,
			SAXException, IOException;

}