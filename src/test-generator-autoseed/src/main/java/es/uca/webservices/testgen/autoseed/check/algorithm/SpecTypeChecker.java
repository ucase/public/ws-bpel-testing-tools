package es.uca.webservices.testgen.autoseed.check.algorithm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.check.strategy.ConcreteCheckStrategyFactory;
import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.utils.ValidVariablesMap;

/**
 * Class in charge of check restrictions of types
 *
 * @author Valentín Liñeiro Barea
 */
public class SpecTypeChecker implements TypeChecker {

	/// Logger
	private static final Logger LOGGER = LoggerFactory
					.getLogger(SpecTypeChecker.class);

	/// Variable Mapper
	private VariableMapper varMapper;

	/**
	 * Default constructor
	 */
	public SpecTypeChecker() {}

	/**
	 * Creates the type checker providing the VariableMapper
	 * @param mapper
	 */
	public SpecTypeChecker(VariableMapper mapper) {
		this.varMapper= mapper;
	}

	@Override
	public VariableMapper getVariableMapper() {
		return varMapper;
	}

	@Override
	public void setVariableMapper(VariableMapper mapper) {
		this.varMapper = mapper;
	}

	/**
	 * Checks if a constant is assignable to a variable
	 * @param var
	 * @param c
	 * @param set
	 * @param map
	 * @return
	 */
	private boolean isValid(IType var, Constant c, Set<String> set) {
		boolean typeOk = ConcreteCheckStrategyFactory.getCheckStrategyFactory()
				.newCheckStrategy(var, c).checkRestrictions();
		return (varMapper == null) ? typeOk : typeOk &&
			checkMapping(var.getNameVariable(), set);
	}

	@Override
	public ValidVariablesMap getValidVariables(List<IType> variables, Map<Constant, Set<String>> constants) {
		ValidVariablesMap dict = new ValidVariablesMap();
		//Cache for checks done
		Map<Constant, Map<IType, Map<String, Boolean>>> visited = new HashMap<Constant, Map<IType,Map<String,Boolean>>>();
		LOGGER.debug("Checking valid constants for variables");
		for(Entry<Constant, Set<String>> c: constants.entrySet()) {
			visited.put(c.getKey(), new HashMap<IType, Map<String,Boolean>>());
			for(IType var: variables) {
				visited.get(c.getKey()).put(var, new HashMap<String, Boolean>());
				String logMessage = "Adding [" + var.getNameVariable() + " , " + c.getKey().getValue() + "]";
				if(!visited.get(c.getKey()).get(var).containsKey(var.getNameVariable())) {
					boolean valid = isValid(var, c.getKey(), c.getValue());
					if(valid) {
						LOGGER.debug(logMessage);
						dict.put(var, c.getKey());
					}
					visited.get(c.getKey()).get(var).put(var.getNameVariable(), valid);
				} else {
					if(visited.get(c.getKey()).get(var).get(var.getNameVariable())) {
						LOGGER.debug(logMessage);
						dict.put(var, c.getKey());
					}
				}
			}
		}
		return dict;
	}

	/**
	 * Checks mapping of spec to BPEL variables
	 * @param nameVariable
	 * @param value
	 * @param map
	 * @return
	 */
	private boolean checkMapping(String nameVariable,
			Set<String> value) {
		boolean ok = false;
		Map<String, Set<String>> map;
		try {
			map = varMapper.getMapping();
		}
		catch(Exception ex) {
			map = new HashMap<String, Set<String>>();
		}
		Set<String> validMappedExpressions = map.get(nameVariable);
		if(validMappedExpressions != null) {
			for(String var: value) {
				if(validMappedExpressions.contains(var)) {
					ok = true;
					break;
				}
			}
		}
		return ok;
	}

}
