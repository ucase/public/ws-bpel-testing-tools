package es.uca.webservices.testgen.autoseed.check.algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.oasisOpen.docs.wsbpel.x20.process.executable.TActivity;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TAssign;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TCopy;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TFrom;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TQuery;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TTo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import es.uca.webservices.testgen.autoseed.reader.constants.BPELConstReaderConstants;
import es.uca.webservices.testgen.autoseed.source.BPELSource;
import es.uca.webservices.testgen.autoseed.source.XMLUtils;
import es.uca.webservices.testgen.autoseed.utils.GeneralUtils;
import es.uca.webservices.xpath.parser.ParseException;

/**
 * Class in charge of doing Analysis of BPEL Processes
 *
 * @author Valentín Liñeiro Barea
 */
public class BPELAnalyzer {

	///Logger
	private static final Logger LOGGER = LoggerFactory
				.getLogger(BPELAnalyzer.class);

	/// Sources of assignments
	private Map<String, Set<String>> sources;

	/// BPEL Source
	private BPELSource bpel;

	/**
	 * Creates a BPELDataFlowAnalyzer
	 * @param bpel
	 */
	public BPELAnalyzer(BPELSource bpel) {
		this.bpel = bpel;
	}

	/**
	 * Get all data flow sources from variables
	 * @return
	 * @throws ParserConfigurationException
	 * @throws ParseException
	 * @throws XPathExpressionException
	 * @throws Exception
	 */
	public Map<String, Set<String>> getDataSources()
			throws XPathExpressionException, ParseException, ParserConfigurationException {
		if(sources == null) {
			readDataSources();
		}
		return sources;
	}

	private void readDataSources()
			throws XPathExpressionException, ParseException, ParserConfigurationException {
		LOGGER.debug("Doing BPEL DataFlow Analysis");
		this.sources = new HashMap<String, Set<String>>();
		for(TActivity act: bpel.getActivities()) {
			if(act instanceof TAssign) {
				analyzeAssign((TAssign) act);
			}
		}
		LOGGER.debug("End of BPEL DataFlow Analysis");
	}

	private void analyzeAssign(TAssign a) throws XPathExpressionException,
		ParseException, ParserConfigurationException {
		for(TCopy cp: a.getCopyArray()) {
			analyzeCopy(cp);
		}
	}

	private void analyzeCopy(TCopy cp) throws XPathExpressionException,
		ParseException, ParserConfigurationException {
		TFrom from = cp.getFrom();
		Set<String> fromVars = analyzeReadVariables(from);
		TTo to = cp.getTo();
		String toVar = analyzeWrittenVariable(to);
		if(!fromVars.contains(toVar)) {
			String nToVar = XMLUtils.convertNS(toVar, bpel.getNS());
			for(String fromVar: fromVars) {
				String nFromVar = XMLUtils.convertNS(fromVar, bpel.getNS());
				LOGGER.debug(nToVar + " <- " + nFromVar);
				GeneralUtils.putNew(sources, nToVar, nFromVar);
			}
		}
	}

	private Set<String> analyzeReadVariables(TFrom from) throws XPathExpressionException, ParseException,
		ParserConfigurationException {
		Set<String> vars = new TreeSet<String>();
		String var = from.getVariable();
		if (var != null) {
			QName prop = from.getProperty();
			if (prop != null) {
				String property = prop.getLocalPart();
				// 2
				vars.add("$" + var + "-" + property);
			} else {
				String part = from.getPart();
				TQuery query = from.getQuery();
				String queryContent = null;
				if (query != null) {
					queryContent = XMLUtils.getExpression(query);
				}
				String finalVar = "$" + var;
				if (part != null) {
					finalVar = finalVar + "." + part;
				}
				if (queryContent != null) {
					if (queryContent.startsWith("/")) {
						finalVar += queryContent;
					} else {
						finalVar = finalVar + "/" + queryContent;
					}
				}
				// 3
				vars.add(finalVar);
			}
		}
		else {
			// 4
			if (from.getLiteral() == null) {
				String expression = XMLUtils.getExpression(from);
				Set<String> variablesFound = findVariablesIntoExpression(expression);
				for (String v : variablesFound) {
					vars.add(v);
				}
			}
		}
		return vars;
	}

	private String analyzeWrittenVariable(TTo to) {
		String varExpr = "";
		if (to.getPartnerLink() == null) {
			if (to.getVariable() != null) {
				if (to.getProperty() != null) {
					String pre = (to.getProperty().getPrefix().equals("")) ? ""
							: to.getProperty().getPrefix() + ":";
					varExpr = "$" + to.getVariable() + "-" + pre
							+ to.getProperty().getLocalPart();
				} else {
					String part = (to.getPart() != null) ? "." + to.getPart()
							: "";
					String query = (to.getQuery() != null) ? ((XMLUtils.getExpression(to
							.getQuery()).startsWith("/")) ? XMLUtils.getExpression(to
							.getQuery()) : "/" + XMLUtils.getExpression(to.getQuery()))
							: "";
					varExpr = "$" + to.getVariable() + part + query;
				}
			} else {
				String expr = XMLUtils.getExpression(to);
				varExpr = expr;
			}
		}
		return varExpr;
	}

	private Set<String> findVariablesIntoExpression(String expression) throws ParseException,
			ParserConfigurationException, XPathExpressionException {
		Set<String> vars = new HashSet<String>();

		NodeList relExpr = bpel.getNodeListFromExpression(expression,
				BPELConstReaderConstants.XPATH_VARIABLE_EXPRESSION);

		for (int i = 0; i < relExpr.getLength(); ++i) {
			final Element var = (Element) relExpr.item(i);
			if((var.getLocalName().equals("qname") &&
					var.getParentNode().getLocalName().equals("variableReference")) ||
					var.getLocalName().equals("children")) {
				String variable;
				if(var.getLocalName().equals("children")) {
					variable = getCompleteVariablePath(var);
				} else {
					variable = "$" + var.getAttribute("localPart");
				}
				vars.add(variable);
			}
		}

		return vars;
	}

	private String getCompleteVariablePath(Element children) {
		String name = "";
		String localPart = "localPart";
		NodeList list = children.getChildNodes();
		for (int i = 0; i < list.getLength(); ++i) {
			final Element el = (Element) list.item(i);
			if(el.getLocalName().equals("children") || el.getLocalName().equals("predicate")) {
				name += getCompleteVariablePath(el);
			} else if(el.getLocalName().equals("variableReference") &&
					!el.getParentNode().getLocalName().equals("predicate")) {
				name = name + "$" + ((Element) el.getFirstChild()).getAttribute(localPart);
			} else {
				String prefix = (!"".equals(((Element) el).getAttribute("prefix"))) ?
						((Element) el).getAttribute("prefix") + ":" : "";
				name = name + "/" + prefix + ((Element) el).getAttribute(localPart);
			}
		}
		return name;
	}

}
