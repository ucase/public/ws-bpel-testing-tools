package es.uca.webservices.testgen.autoseed.check.strategy;

import es.uca.webservices.testgen.api.types.TypeDate;
import es.uca.webservices.testgen.autoseed.constants.DateConstant;

/**
 * Date check restrictions strategy
 * 
 * @author Valentín Liñeiro Barea
 */
public class TypeDateDateConstantCheckStrategy implements ICheckStrategy {
	
	/// Type
	private TypeDate typeDate;
	
	/// Value
	private DateConstant constDate;
	
	/**
	 * Create a date check strategy
	 * @param tD
	 * @param dC
	 */
	public TypeDateDateConstantCheckStrategy(TypeDate tD, DateConstant dC) {
		this.typeDate = tD;
		this.constDate = dC;
	}

	@Override
	public boolean checkRestrictions() {
		boolean ok = true;
		if(typeDate.getMinValue().compare(constDate.getValue()) > 0) {
			ok = false;
		}
		if(ok && typeDate.getMaxValue().compare(constDate.getValue()) < 0) {
			ok = false;
		}
		return ok;
	}

}
