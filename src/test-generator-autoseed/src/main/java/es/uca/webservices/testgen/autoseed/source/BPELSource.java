package es.uca.webservices.testgen.autoseed.source;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.wsdl.Definition;
import javax.wsdl.WSDLException;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;
import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.oasisOpen.docs.wsbpel.x20.process.executable.ProcessDocument;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TActivity;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TImport;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TProcess;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TScope;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TVariable;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.InvalidProcessException;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer;
import es.uca.webservices.xpath.ConversorXMLXPath;
import es.uca.webservices.xpath.parser.ParseException;

/**
 * Class for BPEL sources
 *
 * @author Valentín Liñeiro Barea
 */
public class BPELSource extends XMLSource {

	/// Logger
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BPELSource.class);

	/// BPEL model
	private TProcess model;

	/// BPEL process definition
	private BPELProcessDefinition bpel;

	/// Set of wsdl definitions
	private List<Definition> definitions;

	/// Catalog of services messages
	private ServicesDocument catalog;

	/// BPEL Activities
	private List<TActivity> activities;

	/// BPEL Variables
	private Map<TVariable, Element> variables;

	/**
	 * Constructor
	 *
	 * @param path
	 * @throws IOException
	 * @throws XmlException
	 * @throws InvalidProcessException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws XPathExpressionException
	 * @throws WSDLException
	 */
	public BPELSource(String path) throws XmlException, IOException, XPathExpressionException,
		ParserConfigurationException, SAXException, InvalidProcessException, WSDLException {
		File file = new File(path);
		String relativePath = path.substring(0, path.lastIndexOf('/') + 1);

		this.model = ProcessDocument.Factory.parse(file).getProcess();
		this.bpel = new BPELProcessDefinition(file);

		WSDLReader wsdlReader = WSDLFactory.newInstance().newWSDLReader();
		wsdlReader.setFeature("javax.wsdl.verbose", false);
		this.definitions = new LinkedList<Definition>();
		for (TImport im : model.getImportArray()) {
			if (im.getImportType().equals(
					"http://schemas.xmlsoap.org/wsdl/")) {
				definitions.add(wsdlReader.readWSDL(null,
						relativePath + im.getLocation()));
			}
		}
		try {
			catalog = new ServiceAnalyzer(definitions).generateMessageCatalog();
		} catch (Exception ex) {
			LOGGER.warn("There was a problem generating template catalog. Optimization will not be applied.");
		}
	}

	/**
	 * Returns process model
	 *
	 * @return
	 */
	public TProcess getModel() {
		return model;
	}

	/**
	 * Returns bpel process definition
	 *
	 * @return
	 */
	public BPELProcessDefinition getBpel() {
		return bpel;
	}

	/**
	 * Returns all bpel process web services definitions
	 *
	 * @return
	 */
	public List<Definition> getDefinitions() {
		return definitions;
	}

	/**
	 * Returns all activities from bpel composition
	 *
	 * @return
	 */
	public List<TActivity> getActivities() {
		if(activities == null) {
			readActivities();
		}
		return activities;
	}

	/**
	 * Read all activities from BPEL process
	 */
	private void readActivities() {
		activities = new LinkedList<TActivity>();
		activities.addAll(getAllActivities(model));
	}

	/**
	 * Helper function to get all activities from bpel
	 *
	 * @param element
	 * @return
	 */
	private List<TActivity> getAllActivities(XmlObject element) {
		List<TActivity> all = new LinkedList<TActivity>();
		if (element instanceof TActivity) {
			LOGGER.debug("Discovered new " + element.getDomNode().getNodeName());
			all.add((TActivity) element);
		}
		XmlObject[] children = element.selectPath("./*");
		for (XmlObject object : children) {
			all.addAll(getAllActivities(object));
		}
		return all;
	}

	/**
	 * Returns all variables from bpel process
	 *
	 * @return
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserException
	 */
	public Set<TVariable> getVariables()
			throws ParserException, SAXException, IOException, ParserConfigurationException {
		if(variables == null) {
			readVariables();
		}
		return variables.keySet();
	}

	/**
	 * Reads all BPEL variables
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserException
	 */
	private void readVariables()
			throws ParserException, SAXException, IOException, ParserConfigurationException {
		variables = new HashMap<TVariable, Element>();
		variables.putAll(getAllVariables(model));
	}

	/**
	 * Helper function to get all variables from bpel
	 *
	 * @param element
	 * @return
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserException
	 */
	private Map<TVariable, Element> getAllVariables(XmlObject element)
			throws ParserException, SAXException, IOException, ParserConfigurationException {
		Map<TVariable, Element> all = new HashMap<TVariable, Element>();
		TVariables vars = null;
		if (element instanceof TProcess) {
			vars = ((TProcess) element).getVariables();
		} else if (element instanceof TScope) {
			vars = ((TScope) element).getVariables();
		}
		if (vars != null) {
			for (TVariable var : vars.getVariableArray()) {
				all.put(var, getTemplate(var));
			}
		}
		XmlObject[] children = element.selectPath("./*");
		for (XmlObject object : children) {
			all.putAll(getAllVariables(object));
		}
		return all;
	}

	@Override
	protected void start() {
		this.getNamespaces().putAll(getAllNS(model));
	}

	/**
	 * Gets template element from variable type message
	 *
	 * @param var
	 * @return
	 * @throws ParserException
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public Element getTemplate(TVariable var) throws ParserException,
			SAXException, IOException, ParserConfigurationException {
		Element elem = null;
		if(variables == null) {
			readVariables();
		}
		if(!variables.containsKey(var)) {
			QName varMessage = var.getMessageType();
			if (varMessage != null) {
				String[] info = BPELSourceUtils.findMessageInfo(definitions,
						varMessage);
				elem = BPELSourceUtils.getTemplateElement(BPELSourceUtils
						.getTemplate(catalog, info));
			}
		}
		else {
			elem = variables.get(var);
		}
		return elem;
	}

	/**
	 * Returns a nodelist with expression parts
	 * @param expression
	 * @param exp
	 * @return
	 * @throws ParseException
	 * @throws ParserConfigurationException
	 * @throws XPathExpressionException
	 */
	public NodeList getNodeListFromExpression(String expression, String exp)
			throws ParseException, ParserConfigurationException, XPathExpressionException {
		Document xpathDom = ConversorXMLXPath.xpath2DOM(expression);
		return (NodeList) bpel.evaluateExpression(
				xpathDom.getDocumentElement(), exp, XPathConstants.NODESET);
	}
}
