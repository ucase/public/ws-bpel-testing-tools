package es.uca.webservices.testgen.autoseed.reader.constants;

import java.util.Map;
import java.util.Set;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.xpath.parser.ParseException;

/**
 * Interface of constants reader
 * 
 * @author Valentín Liñeiro Barea
 */
public interface ConstReader {
	
	/**
	 * Gets constants from a source file
	 * @return
	 * @throws DatatypeConfigurationException 
	 * @throws ParserConfigurationException 
	 * @throws ParseException 
	 * @throws XPathExpressionException 
	 * @throws ConstReaderException
	 */
	Map<Constant, Set<String>> getConstants() throws XPathExpressionException, ParseException,
		ParserConfigurationException, DatatypeConfigurationException;
	
}