package es.uca.webservices.testgen.autoseed.reader.constants;

/**
 * Class with xpath constants expressions
 * 
 * @author Valentín Liñeiro Barea
 */
public final class BPELConstReaderConstants {

	/**
	 * Hiding constructor
	 */
	private BPELConstReaderConstants() {}
	
	///XPath constants expression
	public static final String XPATH_CONSTANT_EXPRESSION = "/uca:expression/uca:functionCall[child::uca:qname/attribute::localPart = 'starts-with']/uca:stringConstant | "
			+ "/uca:expression/uca:functionCall[child::uca:qname/attribute::localPart = 'contains']/uca:stringConstant | "
			+ "/uca:expression/uca:functionCall[child::uca:qname/attribute::localPart = 'string']/uca:stringConstant | "
			+ "/uca:expression/uca:stringConstant | "
			+ "/uca:expression/uca:functionCall[child::uca:qname/attribute::localPart = 'number']/uca:numberConstant | "
			+ "/uca:expression/uca:numberConstant | "
			+ "/uca:expression/uca:functionCall/uca:qname[@localPart = 'true' or @localPart = 'false']";
	
	///XPath constants with related variables expression
	public static final String XPATH_CONSTANT_VARIABLE_EXPRESSION = "/uca:expression//*[self::uca:eq or self::uca:neq or self::uca:lt or self::uca:le or self::uca:gt or self::uca:ge] |"
			+ "/uca:expression/uca:functionCall";
	
	///XPath variables expression
	public static final String XPATH_VARIABLE_EXPRESSION = "/uca:expression//uca:variableReference[not(parent::uca:children)]/uca:qname | " + 
			"/uca:expression//uca:children";
	
}
