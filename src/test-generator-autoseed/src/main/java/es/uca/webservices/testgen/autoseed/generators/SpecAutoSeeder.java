package es.uca.webservices.testgen.autoseed.generators;

import java.io.OutputStream;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IRandomStrategy;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.utils.GeneratorUtils;
import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.utils.ValidVariablesMap;
import es.uca.webservices.testgen.formatters.AbstractFormatter;
import es.uca.webservices.testgen.formatters.VelocityFormatter;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;

/**
 * Class in charge to do automatic seeding
 *
 * @author Valentín Liñeiro Barea
 */
public class SpecAutoSeeder implements AutoSeeder {

	/// Logger
	private static final Logger LOGGER = LoggerFactory
				.getLogger(SpecAutoSeeder.class);

	/// Bytes of PRNG seed
	private static final int BYTES_SEED = 16;

	/// Velocity formatter
	private IFormatter vFmt;

	/// Random Generator
	private IStrategy gen;

	/**
	 * Creates an AutoSeeder
	 */
	public SpecAutoSeeder() {
		this.vFmt = new VelocityFormatter();
		this.gen = new UniformRandomStrategy();
	}

	@Override
	public void setSeed(String seed) {
		if(this.gen instanceof IRandomStrategy &&
				seed.getBytes().length == BYTES_SEED) {
			((IRandomStrategy) this.gen).setSeed(seed);
		}
	}

	/**
	 * Creates and modifies a test suite with automatic seeding
	 * @param types
	 * @param dict
	 * @param os
	 * @throws GenerationException
	 */
	@Override
	public void getTestSuite(List<IType> types,
			ValidVariablesMap dict,
			OutputStream os) throws GenerationException {

		GeneratorUtils.generate(dict.numberOfTestCases(), types,
				vFmt, gen);
		applyAutomatedSeeding(dict, (AbstractFormatter) vFmt);
		vFmt.save(os);
	}

	/**
	 * Helper method that generates a random index in [0, max-1]
	 * @param max
	 * @return
	 */
	private int getRandomIndex(int max) {
		return ((UniformRandomStrategy) gen).getPRNG().nextInt(max);
	}

	/**
	 * Helper method that reads index from variable name path
	 * @param name
	 * @param size
	 * @return
	 */
	private int getIndexFromName(String name, int size) {
		Integer index = Integer.parseInt(
				name.substring(name.lastIndexOf('(') + 1,
				name.lastIndexOf(')')));
		//If index is -1, we have a list, we seed in random position
		return (index == -1) ? getRandomIndex(size) : index;
	}

	/**
	 * Applies automatic seeding
	 * @param vars
	 * @param fmt
	 */
	@SuppressWarnings("unchecked")
	private void applyAutomatedSeeding(ValidVariablesMap vars,
			AbstractFormatter fmt) {
		//For each variable type
		int testCase = 0;
		for(Pair<SortedSet<String>, Set<Constant>> ent: vars.values()) {
			//For each variable
			for(String var: ent.getLeft()) {
				LOGGER.debug("Modifying variable " + var + " in test Case " + testCase);
				//If it's complex, we discover its parts
				String[] parts = var.split("\\.");
				//For each constant
				for(Constant c: ent.getRight()) {
					//We get the first part of variable's name
					String key = parts[0];
					LOGGER.debug("Key: " + key);
					//We get the test suite associated
					List<Object> tCase = fmt.getValues().get(key);
					LOGGER.debug("Initial test case: " + tCase);
					if(parts.length == 1) {
						//It's a simple type, change is trivial
						tCase.set(testCase, c.getValue());
					} else {
						//We iterate into structure until we reach
						//the element we want to change
						Object list = tCase.get(testCase);
						Object li = tCase.get(testCase);
						LOGGER.debug("Initial list: " + list.toString());
						for(int i = 1; i < parts.length - 1; ++i) {
							if(((List<Object>) li).isEmpty()) {
								LOGGER.debug("Skipping modification: Generated list is empty");
								break;
							}
							int index = getIndexFromName(parts[i], ((List<Object>) li).size());
							LOGGER.debug("Modifying element " + index);
							li = ((List<Object>) li).get(index);
							LOGGER.debug("Current list: " + li.toString());
						}
						//When the list is empty, we don't modify it
						if(!((List<Object>) li).isEmpty()) {
							int index = getIndexFromName(parts[parts.length - 1], ((List<Object>) li).size());
							LOGGER.debug("Modifying element " + index);
							((List<Object>) li).set(index, c.getValue());
							LOGGER.debug("Modified list: " + li.toString());
							LOGGER.debug("Final list: " + list.toString());
							tCase.set(testCase, list);
						}
					}
					LOGGER.debug("Final test case: " + tCase);
					testCase++;
				}
			}
		}
	}
}
