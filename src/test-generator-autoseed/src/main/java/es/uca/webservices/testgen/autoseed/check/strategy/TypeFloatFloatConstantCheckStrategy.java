package es.uca.webservices.testgen.autoseed.check.strategy;

import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.autoseed.constants.FloatConstant;

/**
 * Float check restrictions strategy
 * 
 * @author Valentín Liñeiro Barea
 */
public class TypeFloatFloatConstantCheckStrategy implements ICheckStrategy {
	
	/// Type
	private TypeFloat typeFloat;
	
	/// Constant
	private FloatConstant constFloat;
	
	/**
	 * Creates a float check strategy
	 * @param tF
	 * @param fC
	 */
	public TypeFloatFloatConstantCheckStrategy(TypeFloat tF, FloatConstant fC) {
		this.typeFloat = tF;
		this.constFloat = fC;
	}

	@Override
	public boolean checkRestrictions() {
		boolean ok = true;
		if (typeFloat.getAllowedValues().size() > 0
				&& !typeFloat.getAllowedValues().contains(constFloat.getValue())) {
			ok = false;
		}
		if (ok && typeFloat.getMinValue().compareTo(constFloat.getValue()) > 0) {
			ok = false;
		}
		if (ok && typeFloat.getMaxValue().compareTo(constFloat.getValue()) < 0) {
			ok = false;
		}
		return ok;
	}

}
