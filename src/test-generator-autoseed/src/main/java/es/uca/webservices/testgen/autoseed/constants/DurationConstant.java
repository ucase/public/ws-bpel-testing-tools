package es.uca.webservices.testgen.autoseed.constants;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;

/**
 * Class of duration constants
 * 
 * @author Valentín Liñeiro Barea
 */
public class DurationConstant implements Constant {

	/// Value
	private Duration value;
	
	/**
	 * Creates a duration constant
	 * @param cons
	 * @throws DatatypeConfigurationException
	 */
	public DurationConstant(String cons) throws DatatypeConfigurationException {
		DatatypeFactory factory = DatatypeFactory.newInstance();
		if(!cons.matches("P(\\d+Y)?(\\d+M)?(\\d+D)?T(\\d+H)?(\\d+M)?(\\d+S)?")) {
			throw new IllegalArgumentException("Only duration values are allowed");
		}
		this.value = factory.newDuration(cons);
	}
	
	@Override
	public Duration getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof DurationConstant) ? 
				((DurationConstant) o).getValue().equals(value) : false;
	}
	
	@Override
	public int hashCode() {
		return value.hashCode();
	}

}
