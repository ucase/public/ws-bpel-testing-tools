package es.uca.webservices.testgen.autoseed.check.strategy;

import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.autoseed.constants.IntegerConstant;

/**
 * Integer check restrictions strategy
 * 
 * @author Valentín Liñeiro Barea
 */
public class TypeIntIntegerConstantCheckStrategy implements ICheckStrategy {
	
	/// Type
	private TypeInt typeInt;
	
	/// Constant
	private IntegerConstant consInt;
	
	/**
	 * Creates an integer check strategy
	 * @param tI
	 * @param iC
	 */
	public TypeIntIntegerConstantCheckStrategy(TypeInt tI, IntegerConstant iC) {
		this.typeInt = tI;
		this.consInt = iC;
	}

	@Override
	public boolean checkRestrictions() {
		boolean ok = true;
		if (typeInt.getAllowedValues().size() > 0 && 
				!typeInt.getAllowedValues().contains(consInt.getValue())) {
			ok = false;
		}
		if (ok && typeInt.getMinValue().compareTo(consInt.getValue()) > 0) {
			ok = false;
		}
		if (ok && typeInt.getMaxValue().compareTo(consInt.getValue()) < 0) {
			ok = false;
		}
		return ok;
	}

}
