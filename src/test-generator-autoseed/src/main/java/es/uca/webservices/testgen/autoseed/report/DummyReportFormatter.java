package es.uca.webservices.testgen.autoseed.report;

import java.io.PrintStream;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.constants.Constant;

/**
 * Dummy Report Formatter
 * 
 * @author Valentín Liñeiro Barea
 */
public class DummyReportFormatter extends AbstractReportFormatter {

	/**
	 * Creates a Dummy Report Formatter
	 * @param reportLevel
	 */
	public DummyReportFormatter(ReportLevels reportLevel) {
		super(reportLevel);
	}

	@Override
	public void formatStart(PrintStream os) {
	}

	@Override
	public void formatEnd(PrintStream os) {
	}

	@Override
	public void formatInformationStart(PrintStream os) {
	}

	@Override
	public void formatInformationBPEL(String bpelPath, PrintStream os) {
	}

	@Override
	public void formatInformationSpec(String specPath, PrintStream os) {
	}

	@Override
	public void formatInformationBPTS(String bptsPath, PrintStream os) {
	}

	@Override
	public void formatInformationEnd(PrintStream os) {
	}

	@Override
	public void formatConstantsStart(PrintStream os) {
	}

	@Override
	public void formatConstantsElement(Entry<Constant, Set<String>> en,
			PrintStream os) {
	}

	@Override
	public void formatConstantsEnd(PrintStream os) {
	}

	@Override
	public void formatVariablesStart(PrintStream os) {
	}

	@Override
	public void formatVariablesElement(IType var, PrintStream os) {
	}

	@Override
	public void formatVariablesEnd(PrintStream os) {
	}

	@Override
	public void formatMappingStart(PrintStream os) {
	}

	@Override
	public void formatMappingElement(Entry<String, Set<String>> en,
			PrintStream os) {
	}

	@Override
	public void formatMappingEnd(PrintStream os) {
	}

	@Override
	public void formatValidVariablesStart(PrintStream os) {
	}

	@Override
	public void formatValidVariablesElement(
			Entry<IType, Pair<SortedSet<String>, Set<Constant>>> en,
			PrintStream os) {
	}

	@Override
	public void formatValidVariablesEnd(PrintStream os) {
	}

	@Override
	public void formatNumberOfTestCasesStart(PrintStream os) {
	}

	@Override
	public void formatNumberOfTestCasesElement(int numberOfTestCases,
			PrintStream os) {
	}

	@Override
	public void formatNumberOfTestCasesEnd(PrintStream os) {
	}

}
