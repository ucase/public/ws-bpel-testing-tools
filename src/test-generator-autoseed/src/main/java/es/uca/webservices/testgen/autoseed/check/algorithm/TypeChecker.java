package es.uca.webservices.testgen.autoseed.check.algorithm;

import java.util.List;
import java.util.Map;
import java.util.Set;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.utils.ValidVariablesMap;

public interface TypeChecker {

	/**
	 * Return valid variables and constants assigned
	 * @param variables
	 * @param map
	 * @param consts
	 * @return
	 */
	ValidVariablesMap getValidVariables(List<IType> variables,
			Map<Constant, Set<String>> constants);

	/**
	 * Gets variable mapper of TypeChecker
	 * @return
	 */
	VariableMapper getVariableMapper();

	/**
	 * Sets variable mapper of TypeChecker
	 * @param mapper
	 */
	void setVariableMapper(VariableMapper mapper);

}