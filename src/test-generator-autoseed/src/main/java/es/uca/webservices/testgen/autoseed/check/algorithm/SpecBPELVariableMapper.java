package es.uca.webservices.testgen.autoseed.check.algorithm;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.oasisOpen.docs.wsbpel.x20.process.executable.TVariable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.autoseed.source.BPELSource;
import es.uca.webservices.testgen.autoseed.source.BPELSourceUtils;
import es.uca.webservices.testgen.autoseed.source.BPTSSource;
import es.uca.webservices.testgen.autoseed.source.BPTSSourceUtils;
import es.uca.webservices.testgen.autoseed.source.DOMUtils;
import es.uca.webservices.testgen.autoseed.source.XMLUtils;
import es.uca.webservices.testgen.autoseed.utils.GeneralUtils;
import es.uca.webservices.xpath.parser.ParseException;

public class SpecBPELVariableMapper implements VariableMapper {

	/// Logger
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SpecBPELVariableMapper.class);

	/// BPEL composition
	private BPELSource bpel;

	/// BPEL DataFlow Analyzer
	private BPELAnalyzer dataFlow;

	/// BPTS test suite
	private BPTSSource bpts;

	/// All namespaces
	private Map<String, String> ns;

	/// Mapping between bpel and spec variables
	private Map<String, Set<String>> spec2bpel;

	/**
	 * Creates a variable mapper and takes all namespaces
	 * @param bpel
	 * @param bpts
	 */
	public SpecBPELVariableMapper(BPELSource bpel, BPTSSource bpts) {
		this.bpel = bpel;
		this.bpts = bpts;
		this.dataFlow = new BPELAnalyzer(this.bpel);
		ns = new HashMap<String, String>();
		ns.putAll(bpel.getNS());
		ns.putAll(bpts.getNS());
	}

	/**
	 * Gets mapping
	 * @return
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserException
	 * @throws ParserConfigurationException
	 * @throws ParseException
	 * @throws XPathExpressionException
	 * @throws VariableMapperException
	 */
	public Map<String, Set<String>> getMapping()
			throws XPathExpressionException, ParseException, ParserConfigurationException,
			ParserException, SAXException, IOException {
		if(spec2bpel == null) {
			LOGGER.debug("Making mapping spec variables -> BPEL variables");
			makeMapping();
		}
		return spec2bpel;
	}

	/**
	 * Makes mapping between spec and bpel variables
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserException
	 * @throws ParseException
	 * @throws XPathExpressionException
	 * @throws Exception
	 */
	private void makeMapping()
			throws XPathExpressionException, ParseException, ParserConfigurationException,
			ParserException, SAXException, IOException {
		spec2bpel = new HashMap<String, Set<String>>();
		Map<String, Set<String>> directSources = dataFlow.getDataSources();
		Map<String, Set<String>> inverseSources = MappingUtils.getInverseSetMap(directSources);

		for(TVariable bV: bpel.getVariables()) {
			LOGGER.debug("Making mapping for " + bV.getName());
			Element tempElement = bpel.getTemplate(bV);
			if(tempElement != null) {
				for(Element tE: bpts.getTemplates()) {
					if(tE.getLocalName().equals("dummyTemplate")) {
						for(Element child: DOMUtils.getElementChildren(tE)) {
							checkTemplateElements(bV, tempElement, child, directSources, inverseSources);
						}
					}
					else {
						checkTemplateElements(bV, tempElement, tE, directSources, inverseSources);
					}
				}
			}
		}
	}

	/**
	 * Checks template elements and gets the variable paths
	 * @param var
	 * @param e1
	 * @param e2
	 * @param directSources
	 * @param inverseSources
	 */
	private void checkTemplateElements(TVariable var, Element e1, Element e2,
			Map<String, Set<String>> directSources,
			Map<String, Set<String>> inverseSources) {
		if(DOMUtils.isEqual(e1, e2)) {
			String auxPath = "";
			Set<String> expressions = new TreeSet<String>();
			Map<String, String> mapAux = new TreeMap<String, String>();
			BPTSSourceUtils.getTemplateElementContent(e2, auxPath, expressions, mapAux);
			Set<Pair<String, String>> pairs = XMLUtils.getQueryAndVariable(expressions, ns);
			for(Pair<String, String> pr: pairs) {
				String key = pr.getLeft().substring(1, pr.getLeft().length());
				List<String> varParts = BPELSourceUtils.getNameWithPart(bpel.getDefinitions(), var);
				for(String varPart: varParts) {
					String bpelVar = varPart + pr.getRight();
					if(key.matches("\\w+(\\.get\\(-?\\d+\\))*")) {
						LOGGER.debug(key + " <- " + bpelVar);
						GeneralUtils.putNew(spec2bpel, key, bpelVar);
						if(directSources.containsKey(bpelVar)) {
							for(String str: directSources.get(bpelVar)) {
								LOGGER.debug(key + " <- " + str);
								GeneralUtils.putNew(spec2bpel, key, str);
							}
						}
						if(inverseSources.containsKey(bpelVar)) {
							for(String str: inverseSources.get(bpelVar)) {
								LOGGER.debug(key + " <- " + str);
								GeneralUtils.putNew(spec2bpel, key, str);
							}
						}
					}
				}
			}
		}
	}
}
