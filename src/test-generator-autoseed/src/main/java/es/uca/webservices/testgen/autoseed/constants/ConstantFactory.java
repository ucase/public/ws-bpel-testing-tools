package es.uca.webservices.testgen.autoseed.constants;

import java.math.BigDecimal;

import javax.xml.datatype.DatatypeConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory of constants
 *
 * @author Valentín Liñeiro Barea
 */
public final class ConstantFactory implements IConstantFactory {

	/// Logger
	private static final Logger LOGGER = LoggerFactory
				.getLogger(ConstantFactory.class);

	/// Singleton instance
	private static IConstantFactory instance;

	/**
	 * Singleton
	 */
	private ConstantFactory() {}

	/**
	 * Returns singleton instance
	 * @return
	 */
	public static IConstantFactory getConstantFactory() {
		if(instance == null) {
			instance = new ConstantFactory();
		}
		return instance;
	}

	/**
	 * @see es.uca.webservices.testgen.autoseed.constants.IConstantFactory#newConstant(java.lang.String)
	 */
	@Override
	public Constant newConstant(String cons) throws DatatypeConfigurationException {
		Constant con;
		LOGGER.debug("String received: " + cons);
		if(ConstantTypeUtils.isNumber(cons)) {
			if(ConstantTypeUtils.isFloat(cons)) {
				LOGGER.debug("Creating new float constant: " + cons);
				con = new FloatConstant(cons);
			} else {
				LOGGER.debug("Creating new integer constant: " + cons);
				con = new IntegerConstant(new BigDecimal(cons).toBigInteger().toString());
			}
		} else if(ConstantTypeUtils.isDateTime(cons)) {
			LOGGER.debug("Creating new dateTime constant: " + cons);
			con = new DateTimeConstant(cons.substring(1, cons.length() - 1));
		} else if(ConstantTypeUtils.isDate(cons)) {
			LOGGER.debug("Creating new date constant: " + cons);
			con = new DateConstant(cons.substring(1, cons.length() - 1));
		} else if(ConstantTypeUtils.isTime(cons)) {
			LOGGER.debug("Creating new time constant: " + cons);
			con = new TimeConstant(cons.substring(1, cons.length() - 1));
		} else if(ConstantTypeUtils.isDuration(cons)) {
			LOGGER.debug("Creating new duration constant: " + cons);
			con = new DurationConstant(cons.substring(1, cons.length() - 1));
		} else {
			LOGGER.debug("Creating new string constant: " + cons);
			con = new StringConstant(cons.substring(1, cons.length() - 1));
		}
		return con;
	}

}
