package es.uca.webservices.testgen.autoseed.check.strategy;

import es.uca.webservices.testgen.api.types.TypeDateTime;
import es.uca.webservices.testgen.autoseed.constants.DateTimeConstant;

/**
 * DateTime check restrictions strategy
 * 
 * @author Valentín Liñeiro Barea
 */
public class TypeDateTimeDateTimeConstantCheckStrategy implements ICheckStrategy {
	
	/// Type
	private TypeDateTime typeDateTime;
	
	/// Constant
	private DateTimeConstant constDateTime;
	
	/**
	 * Creates a date time check strategy
	 * @param tD
	 * @param dC
	 */
	public TypeDateTimeDateTimeConstantCheckStrategy(TypeDateTime tD, DateTimeConstant dC) {
		this.typeDateTime = tD;
		this.constDateTime = dC;
	}

	@Override
	public boolean checkRestrictions() {
		boolean ok = true;
		if(typeDateTime.getMinValue().compare(constDateTime.getValue()) > 0) {
			ok = false;
		}
		if(ok && typeDateTime.getMaxValue().compare(constDateTime.getValue()) < 0) {
			ok = false;
		}
		return ok;
	}

}
