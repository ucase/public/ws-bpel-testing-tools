package es.uca.webservices.testgen.autoseed.report;

/**
 * Different Report Levels
 * 
 * @author Valentín Liñeiro Barea
 */
public enum ReportLevels {
	BASIC, ADVANCED, FULL
}
