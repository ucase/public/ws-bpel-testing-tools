// Spec complejo de prueba para la lectura de tipos

// Prueba entero
typedef int (min = 0, max = 1000) Integer;
Integer var_1;

// Prueba flotante
typedef float (min = 0.0, max = 0.5) Float;
Float var_2;

// Prueba string
typedef string (values = {"Papa", "Pepe"}) String;
String var_3;

// Prueba fecha
typedef date (min="1990-08-20", max="2013-01-01") Date;
Date var_4;

// Prueba fecha y hora
typedef dateTime (min="1990-08-20T13:35:00", max="2013-01-01T00:00:00") DateTime;
DateTime var_5;

// Prueba hora
typedef time (min="01:00:00", max="18:15:00") Time;
Time var_6;

// Prueba Duracion
typedef duration (min="PT1H0M0S", max="PT12H0M0S") Duration;
Duration var_7;

// Prueba Lista de elementos simples
typedef list (min=1, max=3, element={Duration}) ListSimple;
ListSimple var_8;

// Prueba Tupla de elementos simples
typedef tuple (element={Float, String}) TupleSimple;
TupleSimple var_9;

// Prueba Lista de Lista
typedef list (min=0, max=10, element={ListSimple}) ListComplex1;
ListComplex1 var_10;

// Prueba de Tupla de Tupla
typedef tuple(element={TupleSimple, Time}) TupleComplex1;
TupleComplex1 var_11;

// Prueba de Lista de Tupla
typedef list (min=1, max=11, element={TupleSimple}) ListComplex2;
ListComplex2 var_12;

// Prueba de Tupla de Lista
typedef tuple (element={ListSimple, DateTime}) TupleComplex2;
TupleComplex2 var_13;
