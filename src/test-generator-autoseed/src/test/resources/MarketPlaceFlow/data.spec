/*
  Basic delays. We need to make sure the buyer
  and seller delays are always different from
  each other.
*/
typedef float (values={.5,1.5,2.5}) BuyerDelay;
typedef float (values={1.0,2.0,4.0}) SellerDelay;

/*
  Type declarations for the items offered by the
  seller and the bids sent from the buyer.
*/
typedef string (pattern="[a-zA-Z0-9]{3,10}") ItemName;
typedef int (min=0,max=100) Price;

/*
  Roll 1d10 to make the seller or buyer fail (if <= 2).
*/
typedef int (min=1, max=10) Dice;

// --- VARIABLES ---

BuyerDelay buyerDelay;
SellerDelay sellerDelay;
ItemName itemName;
Price askingPrice;
Price offer;
Dice buyerDice;
Dice sellerDice;
