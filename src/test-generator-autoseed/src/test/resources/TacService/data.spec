typedef string (values={"A", "B", "C"}) Line;
typedef list (min=0,max=6, element={Line}) Lines;
typedef int (min=0, max=2) Delay;

Lines lines;
Delay delay;