/*
  Para generar 10 casos de prueba a partir de esta especificación, hay que usar
  esta orden (tras ejecutar "install.sh gamera" para instalar TestGenerator):

  testgenerator data.spec 10 > data.vm
*/

typedef int (min=0,max=20000) LoanAmount;
LoanAmount ApprovalRequest;

typedef string (values={"high", "low", "middle"}) RiskLevel;
RiskLevel AssessorResponse;

typedef string (values={"true", "false"}) Boolean;
Boolean ApprovalResponse;

