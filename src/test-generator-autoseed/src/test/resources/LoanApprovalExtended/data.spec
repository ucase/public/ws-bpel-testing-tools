/* SOLICITUD Y AVAL */

typedef string (pattern="[0-9]{8}[A-Z]") DNI;
typedef float  (min=2900.0, max=6100.0) CantidadPrestamo;
typedef float  (min=10.0, max=48.0) MesesPagar;
typedef string (values={"Grupo1", "Grupo2", "Grupo3", "Grupo4", "Grupo5"}) GrupoProfesion;
typedef string (values={"Soltero", "Viudo", "Divorciado", "Separado", "Casado"}) EstadoCivil;
typedef string (values={"SinEstudios", "PrimariosNoFinalizados", "PrimariosFinalizados", "ESO", "BACHILLERATO", "IngTecnica", "Ingenieria"}) Estudios;
typedef float  (min=0.0, max=1000.0) CantidadGasto;
typedef float  (min=1000.0, max=10000.0) CantidadSalario;

typedef tuple (
    element={
	DNI, CantidadPrestamo, MesesPagar,
	GrupoProfesion, EstadoCivil, Estudios,
	CantidadGasto, CantidadGasto, CantidadGasto, CantidadGasto,
	CantidadSalario
    }
) SolicitudPrestamo;

SolicitudPrestamo CLIENTE_Operation;
SolicitudPrestamo AVAL_OperationResponse;

/* SEGURIDAD SOCIAL */

// Using purely random start and end dates makes it too hard to meet the "happy path",
// so instead we generate random jobs from a random starting date, using regular date
// arithmetic.

typedef date   (min="2000-01-01", max="2011-01-01") FechaInicioPrimerTrabajo;
typedef string (values={"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11"}) TPuesto;
typedef string (values={"100", "200", "401", "408", "501", "508"}) TContrato;
typedef int    (min=30, max=1825) DiasContrato;
typedef int    (min=0, max=180) DiasEntreContratos;
typedef tuple  (element={DiasContrato, DiasEntreContratos, TPuesto, TContrato}) TTrabajo;
typedef list   (element=TTrabajo, min=1, max=10) TList_Trabajo;
typedef tuple  (element={FechaInicioPrimerTrabajo, TList_Trabajo}) TSSOCIAL_OperationResponse;

TSSOCIAL_OperationResponse SSOCIAL_OperationResponse;
TSSOCIAL_OperationResponse SSOCIAL_Avalista;

/* ASNEF */

// Using a 50/50 split for this is too harsh - a 90/10 split is better
typedef int (min=1, max=100) Percentage;
Percentage ASNEF_OperationResponse;
Percentage ASNEF_Avalista;

/* CIRBE */

typedef string (values={"Hipoteca", "TarjetaCredito", "Prestamo", "Ninguno"}) TipoObligacion;
typedef tuple (element={CantidadGasto, TipoObligacion}) Obligacion;
typedef list (element=Obligacion, min=1, max=5) Obligaciones;
typedef string (values={"true", "false"}) Boolean;

Obligaciones ConsultaYalmacenaCIRBEResponse;
Boolean CancelaNuevaObligacionCIRBEResponse;

/* REGISTRO PROPIEDAD */

// Again, using equal probabilities makes it hard to generate a successful test case
typedef string (values={"LibreDeCarga", "HipotecadaConNosotros", "HipotecadaOtraEntidad", "Embargada", "Ninguna"}) EstadoPropiedad;
typedef list (element=Percentage, min=1, max=3) EstadoPropiedades;

EstadoPropiedades REGPROPIEDAD_OperationResponse;
EstadoPropiedades REGPROPIEDAD_Avalista;
