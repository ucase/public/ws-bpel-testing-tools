typedef int (min=0, max = 200000) Quantity;
typedef string (values={"true", "false"}) ApReply;
typedef string (values={"low", "high"}) AsReply;

Quantity req_amount;
ApReply ap_reply;
AsReply as_reply;
