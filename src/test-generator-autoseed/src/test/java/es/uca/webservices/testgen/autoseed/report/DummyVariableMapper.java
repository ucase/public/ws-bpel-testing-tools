package es.uca.webservices.testgen.autoseed.report;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.autoseed.check.algorithm.VariableMapper;
import es.uca.webservices.xpath.parser.ParseException;

/**
 * Variable Mapper Mock Up
 *
 * @author Valentín Liñeiro Barea
 */
public class DummyVariableMapper implements VariableMapper {

	private Map<String, Set<String>> mapping;

	public DummyVariableMapper(Map<String, Set<String>> mapping) {
		this.mapping = mapping;
	}

	@Override
	public Map<String, Set<String>> getMapping()
			throws XPathExpressionException, ParseException,
			ParserConfigurationException, ParserException, SAXException,
			IOException {
		return mapping;
	}

}
