package es.uca.webservices.testgen.autoseed.constants;

/**
 * Class with some constants for tests
 * 
 * @author Valentín Liñeiro Barea
 */
public final class TestConstants {

	/// Resources Path
	public static final String RESOURCES_PATH = "src/test/resources/";
	
	/// Default Spec name
	public static final String SPEC_NAME = "data.spec";
	
	/// Generic Spec Path
	public static final String GENERIC_SPEC_PATH = RESOURCES_PATH + "/Spec/" + SPEC_NAME;

}
