package es.uca.webservices.testgen.autoseed.algorithm;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;

import es.uca.webservices.testgen.autoseed.constants.TestConstants;

/**
 * LoanApprovalDoc integration test
 *
 * @author Valentín Liñeiro Barea
 */
public class LoanApprovalExtendedTest extends AutomaticSeedingTest {

	/**
	 * Test Configuration
	 */
	@Before
	public void setUp() {
		config();
	}

	private void config() {
		final String TEST_PATH = this.getClass().getSimpleName().substring(0,
				this.getClass().getSimpleName().lastIndexOf('T'));
		BPEL_PATH = TestConstants.RESOURCES_PATH + "/" + TEST_PATH + "/" +
				TEST_PATH + ".bpel";
		SPEC_PATH = TestConstants.RESOURCES_PATH + "/" + TEST_PATH + "/" +
				TestConstants.SPEC_NAME;
		BPTS_PATH = TestConstants.RESOURCES_PATH + "/" + TEST_PATH + "/" +
				TEST_PATH + ".bpts";
	}

	@Override
	public void automaticSeedingTestAsserts(String[] lines) {
		assertThat("Assert 1", lines[0].contains("IngTecnica"), is(equalTo(true)));
		assertThat("Assert 2", lines[0].contains("Soltero"), is(equalTo(true)));
		assertThat("Assert 3", lines[0].contains("3000"), is(equalTo(true)));
	}

	@Override
	public void automaticSeedingTestWithOptimizationAsserts(String[] lines) {
		assertThat("Assert 1", lines[0].contains("IngTecnica"), is(equalTo(true)));
		assertThat("Assert 2", lines[0].contains("Soltero"), is(equalTo(true)));
		assertThat("Assert 3", lines[0].contains("3000"), is(equalTo(true)));
	}

}
