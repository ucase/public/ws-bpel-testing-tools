package es.uca.webservices.testgen.autoseed.generators;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.TestUtils;
import es.uca.webservices.testgen.autoseed.check.algorithm.SpecTypeChecker;
import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.constants.DateConstant;
import es.uca.webservices.testgen.autoseed.constants.DateTimeConstant;
import es.uca.webservices.testgen.autoseed.constants.DurationConstant;
import es.uca.webservices.testgen.autoseed.constants.FloatConstant;
import es.uca.webservices.testgen.autoseed.constants.IntegerConstant;
import es.uca.webservices.testgen.autoseed.constants.StringConstant;
import es.uca.webservices.testgen.autoseed.constants.TestConstants;
import es.uca.webservices.testgen.autoseed.constants.TimeConstant;
import es.uca.webservices.testgen.autoseed.generators.SpecAutoSeeder;
import es.uca.webservices.testgen.autoseed.reader.variables.SpecReader;
import es.uca.webservices.testgen.autoseed.utils.ValidVariablesMap;

/**
 * AutoSeeder Test
 *
 * @author Valentín Liñeiro Barea
 */
public class AutoseederTest {

	/// Variables
	private List<IType> types;

	/// Constants
	private Map<Constant, Set<String>> consts;

	/// Dictionary
	private ValidVariablesMap dict;

	/// AutoSeeder
	private SpecAutoSeeder autoSeeder;

	@Before
	public void setUp() throws Exception {
		SpecReader reader = new SpecReader(TestConstants.GENERIC_SPEC_PATH);
		List<IType> simpleTypes = reader.getSimpleTypes();
		types = reader.getTypes();
		initialize();
		dict = new SpecTypeChecker().getValidVariables(simpleTypes, consts);
		autoSeeder = new SpecAutoSeeder();
	}

	/**
	 * Initializes map of constants
	 * @return
	 */
	private void initialize() throws Exception {
		consts = new HashMap<Constant, Set<String>>();
		consts.put(new IntegerConstant("1"), new HashSet<String>());
		consts.put(new IntegerConstant("10000"), new HashSet<String>());
		consts.put(new FloatConstant("0.5"), new HashSet<String>());
		consts.put(new FloatConstant("1.5"), new HashSet<String>());
		consts.put(new StringConstant("Papa"), new HashSet<String>());
		consts.put(new StringConstant("Popo"), new HashSet<String>());
		consts.put(new DateConstant("2012-08-20"), new HashSet<String>());
		consts.put(new DateConstant("2014-08-20"), new HashSet<String>());
		consts.put(new DateTimeConstant("2012-01-01T00:00:00"), new HashSet<String>());
		consts.put(new DateTimeConstant("2014-01-01T00:00:00"), new HashSet<String>());
		consts.put(new TimeConstant("15:00:00"), new HashSet<String>());
		consts.put(new TimeConstant("19:00:00"), new HashSet<String>());
		consts.put(new DurationConstant("PT2H0M0S"), new HashSet<String>());
		consts.put(new DurationConstant("PT15H0M0S"), new HashSet<String>());
	}

	/**
	 * AutoSeeder Test
	 */
	@Test
	public void autoSeederTest() throws Exception {
		ByteArrayOutputStream bOS = new ByteArrayOutputStream();
		autoSeeder.getTestSuite(types, dict, bOS);
		String[] lines = TestUtils.convertOutput(bOS);
		assertThat("Output contains Integer", lines[0].contains("1"), is(equalTo(true)));
		assertThat("Output contains Float", lines[1].contains("0.5"), is(equalTo(true)));
		assertThat("Output contains String", lines[2].contains("Papa"), is(equalTo(true)));
		assertThat("Output contains Date", lines[3].contains("2012-08-20"), is(equalTo(true)));
		assertThat("Output contains DateTime", lines[4].contains("2012-01-01T00:00:00"), is(equalTo(true)));
		assertThat("Output contains Time", lines[5].contains("15:00:00"), is(equalTo(true)));
		assertThat("Output contains Duration", lines[6].contains("PT2H0M0S"), is(equalTo(true)));
	}

}
