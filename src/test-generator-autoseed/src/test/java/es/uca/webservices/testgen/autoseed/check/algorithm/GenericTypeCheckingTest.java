package es.uca.webservices.testgen.autoseed.check.algorithm;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assume;
import org.junit.Test;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.reader.constants.BPELConstReader;
import es.uca.webservices.testgen.autoseed.reader.variables.SpecReader;
import es.uca.webservices.testgen.autoseed.source.BPELSource;
import es.uca.webservices.testgen.autoseed.source.BPTSSource;
import es.uca.webservices.testgen.autoseed.utils.ValidVariablesMap;

/**
 * Generic TypeChecking Test
 *
 * @author Valentín Liñeiro Barea
 */
public abstract class GenericTypeCheckingTest {

	/// TypeChecker
	protected TypeChecker typeChecker;

	/// Types
	protected List<IType> types;

	/// Consts
	protected Map<Constant, Set<String>> consts;

	///Variable Mapper
	protected VariableMapper mapper;

	/*
	 * Configuration method
	 */
	public void config(String bpelPath, String bptsPath, String specPath) throws Exception {
		BPELSource bpel = new BPELSource(bpelPath);
		BPTSSource bpts = (!bptsPath.equals("")) ? new BPTSSource(bptsPath) : null;
		mapper = (bpts != null) ? new SpecBPELVariableMapper(bpel, bpts) : null;
		typeChecker = new SpecTypeChecker(mapper);
		consts = (consts == null) ?
				new BPELConstReader(bpel).getConstants() :
					consts;
		types = (types == null) ?
				new SpecReader(specPath).getSimpleTypes() :
					types;
	}

	/**
	 * Number of Valid Variables Found Test
	 * @throws Exception
	 */
	@Test
	public void numberOfValidVariablesAndTestCasesFound() throws Exception {
		typeChecker.setVariableMapper(null);
		ValidVariablesMap dict =
				typeChecker.getValidVariables(types, consts);
		numberOfVariablesAndTestCasesFoundAsserts(dict);
	}

	/**
	 * Number of variables mapped Test
	 * @throws Exception
	 */
	@Test
	public void numberOfVariablesMapped() throws Exception {
		Assume.assumeNotNull(mapper);
		numberOfVariablesMappedAsserts(mapper.getMapping());
	}

	/**
	 * Number of Valid Variables Found Test
	 * @throws Exception
	 */
	@Test
	public void numberOfValidVariablesAndTestCasesFoundWithOptimization() throws Exception {
		Assume.assumeNotNull(mapper);
		ValidVariablesMap dict =
				typeChecker.getValidVariables(types, consts);
		numberOfVariablesAndTestCasesFoundAssertsWithOptimization(dict);
	}

	/**
	 * Asserts for number of variables and test cases found
	 * @param dict
	 * @throws Exception
	 */
	public abstract void numberOfVariablesAndTestCasesFoundAsserts(ValidVariablesMap dict)
			throws Exception;

	/**
	 * Number of variables mapped asserts
	 * @param mapping
	 */
	public abstract void numberOfVariablesMappedAsserts(Map<String, Set<String>> mapping);

	/**
	 * Asserts for number of variables and test cases found with optimization
	 * @param dict
	 * @throws Exception
	 */
	public abstract void numberOfVariablesAndTestCasesFoundAssertsWithOptimization(ValidVariablesMap dict)
			throws Exception;

}
