package es.uca.webservices.testgen.autoseed.algorithm;

import java.io.ByteArrayOutputStream;

import org.junit.Test;

import es.uca.webservices.testgen.autoseed.TestUtils;
import es.uca.webservices.testgen.autoseed.report.ReportLevels;

/**
 * Generic Integration Test
 *
 * @author Valentín Liñeiro Barea
 */
public abstract class AutomaticSeedingTest {

	/**
	 * Config
	 */
	protected String BPEL_PATH;

	protected String BPTS_PATH;

	protected String SPEC_PATH;

	public void processConfig(AutomaticSeeding process) {
		process.setCount(false);
		process.setReportFormat("");
		process.setReportLevel(ReportLevels.BASIC);
	}

	/**
	 * Automatic Seeding Test
	 * @throws Exception
	 */
	@Test
	public void automaticSeedingTest() throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		AutomaticSeeding process = new AutomaticSeeding(BPEL_PATH, SPEC_PATH, "");
		processConfig(process);
        process.run(os);
        String[] lines = TestUtils.convertOutput(os);
        automaticSeedingTestAsserts(lines);
	}

	/**
	 * Automatic Seeding Optimized Test
	 * @throws Exception
	 */
	@Test
	public void automaticSeedingTestWithOptimization() throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		AutomaticSeeding process = new AutomaticSeeding(BPEL_PATH, SPEC_PATH, BPTS_PATH);
		processConfig(process);
        process.run(os);
        String[] lines = TestUtils.convertOutput(os);
        automaticSeedingTestWithOptimizationAsserts(lines);
	}

	/**
	 * Asserts for the test given
	 * @param lines
	 */
	public abstract void automaticSeedingTestAsserts(String[] lines);

	/**
	 * Asserts for the test given
	 * @param lines
	 */
	public abstract void automaticSeedingTestWithOptimizationAsserts(String[] lines);

}
