package es.uca.webservices.testgen.autoseed.check.algorithm;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Map;
import java.util.Set;

import org.junit.Before;

import es.uca.webservices.testgen.autoseed.constants.TestConstants;
import es.uca.webservices.testgen.autoseed.utils.ValidVariablesMap;

/**
 * TypeChecking tests for MetaSearchForEach
 *
 * @author Valentín Liñeiro Barea
 */
public class SquaresSum1Test extends GenericTypeCheckingTest {

	/**
	 * Test Configuration
	 */
	private final String TEST_PATH = this.getClass().getSimpleName().substring(0,
			this.getClass().getSimpleName().lastIndexOf('T'));

	private final String BPEL_PATH = TestConstants.RESOURCES_PATH + "/" + TEST_PATH + "/" +
			TEST_PATH + ".bpel";

	private final String SPEC_PATH = TestConstants.RESOURCES_PATH + "/" + TEST_PATH + "/" +
			TestConstants.SPEC_NAME;

	private final String BPTS_PATH = TestConstants.RESOURCES_PATH + "/" + TEST_PATH + "/" +
			TEST_PATH + ".bpts";

	@Before
	public void setUp() throws Exception {
		config(BPEL_PATH, BPTS_PATH, SPEC_PATH);
	}

	@Override
	public void numberOfVariablesAndTestCasesFoundAsserts(ValidVariablesMap dict)
			throws Exception {
		assertThat("Number of different valid variables", dict.numberOfValidVariables(),
				is(equalTo(1)));
		assertThat("Number of test cases", dict.numberOfTestCases(),
				is(equalTo(2)));
	}

	@Override
	public void numberOfVariablesMappedAsserts(Map<String, Set<String>> mapping) {
		//Not tested here
	}

	@Override
	public void numberOfVariablesAndTestCasesFoundAssertsWithOptimization(ValidVariablesMap dict)
			throws Exception {
		assertThat("Number of different valid variables", dict.numberOfValidVariables(),
				is(equalTo(0)));
		assertThat("Number of test cases", dict.numberOfTestCases(),
				is(equalTo(0)));
	}

}
