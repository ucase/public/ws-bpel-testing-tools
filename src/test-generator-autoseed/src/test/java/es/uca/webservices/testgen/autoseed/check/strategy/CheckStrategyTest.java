package es.uca.webservices.testgen.autoseed.check.strategy;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.check.algorithm.SpecTypeChecker;
import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.constants.DateConstant;
import es.uca.webservices.testgen.autoseed.constants.DateTimeConstant;
import es.uca.webservices.testgen.autoseed.constants.DurationConstant;
import es.uca.webservices.testgen.autoseed.constants.FloatConstant;
import es.uca.webservices.testgen.autoseed.constants.IntegerConstant;
import es.uca.webservices.testgen.autoseed.constants.StringConstant;
import es.uca.webservices.testgen.autoseed.constants.TestConstants;
import es.uca.webservices.testgen.autoseed.constants.TimeConstant;
import es.uca.webservices.testgen.autoseed.reader.variables.SpecReader;
import es.uca.webservices.testgen.autoseed.utils.ValidVariablesMap;

/**
 * Check Strategy Tests
 *
 * @author Valentín Liñeiro Barea
 */
public class CheckStrategyTest {

	/// Variables
	private List<IType> types;

	/// Constants
	private Map<Constant, Set<String>> consts;

	/// TypeChecker
	private SpecTypeChecker typeChecker;

	@Before
	public void setUp() throws Exception {
		types = new SpecReader(TestConstants.GENERIC_SPEC_PATH).getSimpleTypes();
		initialize();
		typeChecker = new SpecTypeChecker();
	}

	/**
	 * Initializes map of constants
	 * @return
	 */
	private void initialize() throws Exception {
		consts = new HashMap<Constant, Set<String>>();
		consts.put(new IntegerConstant("-1"), new HashSet<String>());
		consts.put(new IntegerConstant("1"), new HashSet<String>());
		consts.put(new IntegerConstant("10000"), new HashSet<String>());
		consts.put(new FloatConstant("-0.5"), new HashSet<String>());
		consts.put(new FloatConstant("0.5"), new HashSet<String>());
		consts.put(new FloatConstant("1.5"), new HashSet<String>());
		consts.put(new StringConstant("Papa"), new HashSet<String>());
		consts.put(new StringConstant("Popo"), new HashSet<String>());
		consts.put(new DateConstant("1989-08-20"), new HashSet<String>());
		consts.put(new DateConstant("2012-08-20"), new HashSet<String>());
		consts.put(new DateConstant("2014-08-20"), new HashSet<String>());
		consts.put(new DateTimeConstant("1989-01-01T00:00:00"), new HashSet<String>());
		consts.put(new DateTimeConstant("2012-01-01T00:00:00"), new HashSet<String>());
		consts.put(new DateTimeConstant("2014-01-01T00:00:00"), new HashSet<String>());
		consts.put(new TimeConstant("00:00:00"), new HashSet<String>());
		consts.put(new TimeConstant("15:00:00"), new HashSet<String>());
		consts.put(new TimeConstant("19:00:00"), new HashSet<String>());
		consts.put(new DurationConstant("PT0H0M1S"), new HashSet<String>());
		consts.put(new DurationConstant("PT2H0M0S"), new HashSet<String>());
		consts.put(new DurationConstant("PT15H0M0S"), new HashSet<String>());
	}

	/**
	 * Check Strategy Test
	 * @throws Exception
	 */
	@Test
	public void checkStrategyTest() throws Exception {
		ValidVariablesMap vars = typeChecker.getValidVariables(types, consts);
		assertThat("Number of different valid variables", vars.numberOfValidVariables(),
				is(equalTo(18)));
	}

}
