package es.uca.webservices.testgen.autoseed.algorithm;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.autoseed.constants.TestConstants;

/**
 * LoanApprovalDoc integration test
 *
 * @author Valentín Liñeiro Barea
 */
public class LoanApprovalDocTest extends AutomaticSeedingTest {

	/**
	 * Test Configuration
	 */
	@Before
	public void setUp() {
		config();
	}

	private void config() {
		final String TEST_PATH = this.getClass().getSimpleName().substring(0,
				this.getClass().getSimpleName().lastIndexOf('T'));
		BPEL_PATH = TestConstants.RESOURCES_PATH + "/" + TEST_PATH + "/" +
				TEST_PATH + ".bpel";
		SPEC_PATH = TestConstants.RESOURCES_PATH + "/" + TEST_PATH + "/" +
				TestConstants.SPEC_NAME;
		BPTS_PATH = TestConstants.RESOURCES_PATH + "/" + TEST_PATH + "/" +
				TEST_PATH + ".bpts";
	}

	@Override
	public void automaticSeedingTestAsserts(String[] lines) {
		assertThat("Assert 1", lines[0].contains("10000"), is(equalTo(true)));
		assertThat("Assert 2", lines[1].contains("true"), is(equalTo(true)));
		assertThat("Assert 3", lines[2].contains("high"), is(equalTo(true)));
	}

	@Override
	public void automaticSeedingTestWithOptimizationAsserts(String[] lines) {
		assertThat("Assert 1", lines[0].contains("10000"), is(equalTo(true)));
		assertThat("Assert 2", lines[1].contains("true"), is(equalTo(true)));
		assertThat("Assert 3", lines[2].contains("high"), is(equalTo(true)));
	}

	/**
	 * Test extra to set new seed
	 */
	@Test
	public void automaticSeedingWithExternalSeed() throws Exception {
		String seed = "seedseedseedseed";
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		AutomaticSeeding process = new AutomaticSeeding(BPEL_PATH, SPEC_PATH, "");
		processConfig(process);
		process.setSeed(seed);
        process.run(os);
        String output = os.toString();
        ByteArrayOutputStream os2 = new ByteArrayOutputStream();
		AutomaticSeeding process2 = new AutomaticSeeding(BPEL_PATH, SPEC_PATH, "");
		processConfig(process2);
		process2.setSeed(seed);
        process2.run(os2);
        String output2 = os2.toString();
        assertThat("Are equal", output.equals(output2), is(equalTo(true)));
	}

}
