package es.uca.webservices.testgen.autoseed;

import java.io.ByteArrayOutputStream;

/**
 * Class with some test utils
 * 
 * @author Valentín Liñeiro Barea
 */
public final class TestUtils {
	
	/**
	 * Convert output to proper String
	 * @param bOS
	 * @return
	 */
	public static String[] convertOutput(ByteArrayOutputStream bOS) {
		String saltoLinea = System.getProperty("line.separator");
		String output = bOS.toString().trim();
		output = output.replaceAll("\\r?\\n", " ").replaceAll("\\) #", "\\)" + saltoLinea + "#");
		return output.split("\\r?\\n");
	}
	
}
