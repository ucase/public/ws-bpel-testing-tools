package es.uca.webservices.testgen.autoseed.reader.constants;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.reader.constants.ConstReader;

/**
 * Generic ConstReader Test
 * 
 * @author Valentín Liñeiro Barea
 */
public abstract class GenericConstReaderTest {
	
	/// ConstReader
	protected ConstReader constReader;
	
	/**
	 * Number of Constants Found Test
	 * @throws Exception
	 */
	@Test
	public void numberOfConstantsFound() throws Exception {
		Map<Constant, Set<String>> consts = constReader.getConstants();
		numberOfConstantsFoundAsserts(consts);
	}

	/**
	 * Asserts for Number Of Constants Found Test
	 * @param consts 
	 */
	public abstract void numberOfConstantsFoundAsserts(Map<Constant, Set<String>> consts) 
			throws Exception;
	
	/**
	 * Shows constants found
	 */
	public void showConstantsFound(Map<Constant, Set<String>> consts) throws Exception {
		for(Entry<Constant, Set<String>> en: consts.entrySet()) {
			System.out.println(en.getKey().getValue() + " :");
			System.out.println("..............................");
			for(String var: en.getValue()) {
				System.out.println(var);
			}
			System.out.println("------------------------------");
		}
	}
	
}
