package es.uca.webservices.testgen.autoseed.cli;

import java.io.File;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.bpel.xml.XMLDocument;
import es.uca.webservices.testgen.autoseed.algorithm.AutomaticSeedingException;
import es.uca.webservices.testgen.autoseed.constants.TestConstants;
import es.uca.webservices.testgen.autoseed.report.ReportLevels;
import es.uca.webservices.vmutils.VMUtils;

/**
 * Command Tests
 *
 * @author Valentín Liñeiro Barea
 */
public class CommandTest {

	/**
	 * Test Configuration
	 */
	private static final String BPEL = TestConstants.RESOURCES_PATH +
			"LoanApprovalDoc/LoanApprovalDoc.bpel";
	private static final String SPEC = TestConstants.RESOURCES_PATH +
			"LoanApprovalDoc/data.spec";
	private static final String BPTS = TestConstants.RESOURCES_PATH +
			"LoanApprovalDoc/LoanApprovalDoc.bpts";

	private TestGeneratorAutoseedCommand command;

	@Before
	public void setUp() throws Exception {
		command = new TestGeneratorAutoseedCommand();
	}

	@After
	public void tearDown() throws Exception {
		command = null;
	}

	/**
	 * No arguments command test
	 * @throws Exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void noArgumentsTest() throws Exception {
		String[] args = {};
	    command.parseArgs(args);
	}

	/**
	 * Too many arguments command test
	 * @throws Exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void tooManyArgumentsTest() throws Exception {
	     String[] args = {BPEL, SPEC, "dummy"};
	     command.parseArgs(args);
	}

	/**
	 * Bad BPEL command test
	 * @throws Exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void badBPELTest() throws Exception {
	     String[] args = {"composition", SPEC};
	     command.parseArgs(args);
	}

	/**
	 * Bad spec command test
	 * @throws Exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void badSPECTest() throws Exception {
	     String[] args = {BPEL, "data"};
	     command.parseArgs(args);
	}

	/**
	 * File not found command test
	 * @throws Exception
	 */
	@Test(expected = AutomaticSeedingException.class)
	public void fileNotFoundTest() throws Exception {
	     String[] args = {BPEL, "dummy.spec"};
	     command.parseArgs(args);
	     command.run();
	}

	/**
	 * No exception test
	 * @throws Exception
	 */
	@Test
	public void noExceptionTest() throws Exception {
	     String[] args = {"-report-format", "none", BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	}

	/**
	 * Option help test
	 * @throws Exception
	 */
	@Test
	public void optionHelpTest() throws Exception {
	     String[] args = {"-help"};
	     command.parseArgs(args);
	     command.run();
	}

	/**
	 * Option optimization bpts not found test
	 * @throws Exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void optionOptimizationBadBPTSTest() throws Exception {
	     String[] args = {"-O", "dummy", BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	}

	/**
	 * Option optimization bpts not found test
	 * @throws Exception
	 */
	@Test(expected = AutomaticSeedingException.class)
	public void optionOptimizationBPTSNotFoundTest() throws Exception {
	     String[] args = {"-O", "dummy.bpts", BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	}

	/**
	 * Option optimization test
	 * @throws Exception
	 */
	@Test
	public void optionOptimizationTest() throws Exception {
	     String[] args = {"-O", BPTS, "-report-format", "none", BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	}

	/**
	 * Option output without optimization test
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@Test
	public void optionOutputWithoutOptimizationTest() throws Exception {
		 File output = File.createTempFile("data", ".vm");
	     String[] args = {"-output", output.getCanonicalPath(), "-report-format", "none", BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	     Velocity.init();
	     final Context dataCtx = VMUtils.loadDataFile(output);
	     output.delete();
	}

	/**
	 * Option output with optimization test
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@Test
	public void optionOutputWithOptimizationTest() throws Exception {
		 File output = File.createTempFile("data", ".vm");
	     String[] args = {"-O", BPTS, "-output", output.getCanonicalPath(), "-report-format", "none", BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	     Velocity.init();
	     final Context dataCtx = VMUtils.loadDataFile(output);
	     output.delete();
	}

	/**
	 * Option plain text report test
	 * @throws Exception
	 */
	@Test
	public void optionPlainTextReportTest() throws Exception {
	     String[] args = {"-report-format", "plain", BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	}

	/**
	 * Option xml report test
	 * @throws Exception
	 */
	@Test
	public void optionXMLReportTest() throws Exception {
	     String[] args = {"-report-format", "xml", BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	}

	/**
	 * Option advanced report test
	 * @throws Exception
	 */
	@Test
	public void optionAdvancedReportTest() throws Exception {
	     String[] args = {"-report-level", ReportLevels.ADVANCED.toString(), BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	}

	/**
	 * Option advanced report test
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@Test
	public void optionFullReportInFileTest() throws Exception {
		 File output = File.createTempFile("report", ".xml");
	     String[] args = {"-report-level", ReportLevels.FULL.toString(),
	    		 "-report-format", "xml",
	    		 "-report-output", output.getCanonicalPath(), BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	     XMLDocument doc = new XMLDocument(output);
	     output.delete();
	}

	/**
	 * Option count test cases test
	 * @throws Exception
	 */
	@Test
	public void optionCountTestCasesTest() throws Exception {
	     String[] args = {"-count", "-report-format", "none", BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	}

	/**
	 * Option count test cases test
	 * @throws Exception
	 */
	@Test
	public void optionCountTestCasesWithOptimizationTest() throws Exception {
	     String[] args = {"-count", "-report-format", "none", "-O", BPTS, BPEL, SPEC};
	     command.parseArgs(args);
	     command.run();
	}

	@Test
	public void optionSeedTest() throws Exception {
		String[] args = {"-seed", "seedseedseedseed", "-report-format", "none", BPEL, SPEC};
		command.parseArgs(args);
		command.run();
	}
}
