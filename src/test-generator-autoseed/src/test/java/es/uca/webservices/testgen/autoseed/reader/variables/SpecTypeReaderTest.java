package es.uca.webservices.testgen.autoseed.reader.variables;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.reader.variables.SpecReader;
import es.uca.webservices.testgen.autoseed.reader.variables.TypeReader;

public class SpecTypeReaderTest {
	
	private TypeReader reader;
	
	private String specPath = "src/test/resources/Spec/data.spec";
	
	@Before
	public void setUp() throws Exception {
		reader = new SpecReader(specPath);
	}
	
	@Test
	public void readSpecTypesTest() throws Exception {
		List<IType> types = reader.getTypes();
		assertThat("Number of variables", types.size(), is(equalTo(13)));
	}

}
