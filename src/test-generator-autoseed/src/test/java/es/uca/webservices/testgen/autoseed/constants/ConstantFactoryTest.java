package es.uca.webservices.testgen.autoseed.constants;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * Constant Factory Test
 * 
 * @author Valentín Liñeiro Barea
 */
public class ConstantFactoryTest {
	
	/// Constant Factory
	private IConstantFactory factory;

	@Before
	public void setUp() throws Exception {
		factory = ConstantFactory.getConstantFactory();
	}

	/**
	 * Constant Factory Test
	 * @throws Exception
	 */
	@Test
	public void constantFactoryTest() throws Exception {
		Constant intConst = factory.newConstant("1");
		assertTrue("Is Integer Constant", intConst instanceof IntegerConstant);
		Constant floatConst = factory.newConstant("0.5");
		assertTrue("Is Float Constant", floatConst instanceof FloatConstant);
		Constant strConst = factory.newConstant("'Papa'");
		assertTrue("Is String Constant", strConst instanceof StringConstant);
		Constant dateConst = factory.newConstant("'2012-08-20'");
		assertTrue("Is Date Constant", dateConst instanceof DateConstant);
		Constant dateTimeConst = factory.newConstant("'2012-01-01T00:00:00'");
		assertTrue("Is DateTime Constant", dateTimeConst instanceof DateTimeConstant);
		Constant timeConst = factory.newConstant("'15:00:00'");
		assertTrue("Is Time Constant", timeConst instanceof TimeConstant);
		Constant durConst = factory.newConstant("'PT2H0M0S'");
		assertTrue("Is Duration Constant", durConst instanceof DurationConstant);
	}

}
