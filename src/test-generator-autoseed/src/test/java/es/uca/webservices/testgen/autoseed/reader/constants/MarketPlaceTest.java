package es.uca.webservices.testgen.autoseed.reader.constants;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Map;
import java.util.Set;

import org.junit.Before;

import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.constants.TestConstants;
import es.uca.webservices.testgen.autoseed.reader.constants.BPELConstReader;
import es.uca.webservices.testgen.autoseed.source.BPELSource;

/**
 * ConstReader Tests for LoanApprovalDoc
 * 
 * @author Valentín Liñeiro Barea
 */
public class MarketPlaceTest extends GenericConstReaderTest {
	
	/**
	 * Test Configuration
	 */
	private final String TEST_PATH = this.getClass().getSimpleName().substring(0, 
			this.getClass().getSimpleName().lastIndexOf('T'));
	
	private final String BPEL_PATH = TestConstants.RESOURCES_PATH + "/" + TEST_PATH + "/" + 
			TEST_PATH + ".bpel";

	@Before
	public void setUp() throws Exception {
		constReader = new BPELConstReader(new BPELSource(BPEL_PATH));
	}

	@Override
	public void numberOfConstantsFoundAsserts(Map<Constant, Set<String>> consts) 
			throws Exception {
		assertThat("Number of constants", consts.size(), is(equalTo(2)));
	}

}
