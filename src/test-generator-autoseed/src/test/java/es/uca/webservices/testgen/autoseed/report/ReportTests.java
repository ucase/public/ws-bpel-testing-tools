package es.uca.webservices.testgen.autoseed.report;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.transform.TransformerException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import es.uca.webservices.bpel.xml.XMLDocument;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.autoseed.check.algorithm.SpecTypeChecker;
import es.uca.webservices.testgen.autoseed.constants.Constant;
import es.uca.webservices.testgen.autoseed.constants.IntegerConstant;
import es.uca.webservices.testgen.autoseed.constants.StringConstant;
import es.uca.webservices.testgen.autoseed.constants.TestConstants;
import es.uca.webservices.testgen.autoseed.reader.variables.SpecReader;

/**
 * Report tests
 *
 * @author Valentín Liñeiro Barea
 */
public class ReportTests {

	/**
	 * Test configuration
	 */
	private Report report;

	private IReportFormatterFactory factory = ConcreteFormatterFactory.getReportFormatterFactory();

	@Before
	public void setUp() throws Exception {
		report = new Report();
		report.setBpelPath("dummy.bpel");
		report.setSpecPath("dummy.spec");
		report.setBptsPath("dummy.bpts");
		Map<Constant, Set<String>> constants = getConstantMap();
		report.setConstants(constants);
		List<IType> variables = getVariables();
		report.setVariables(variables);
		Map<String, Set<String>> mapping = getMapping();
		report.setMapping(mapping);
		report.setValidVariables(new SpecTypeChecker(new DummyVariableMapper(mapping)).
				getValidVariables(variables, constants));
	}

	@After
	public void tearDown() throws Exception {
		report = null;
	}

	/**
	 * Gets constant map
	 * @return
	 */
	private Map<Constant, Set<String>> getConstantMap() {
		Map<Constant, Set<String>> consts = new HashMap<Constant, Set<String>>();
		IntegerConstant iC = new IntegerConstant("0");
		consts.put(iC, new HashSet<String>());
		consts.get(iC).add("$x");
		StringConstant sC = new StringConstant("Pepe");
		consts.put(sC, new HashSet<String>());
		consts.get(sC).add("$y");
		return consts;
	}

	/**
	 * Gets list of variables
	 * @return
	 */
	private List<IType> getVariables() throws Exception {
		return new SpecReader(TestConstants.GENERIC_SPEC_PATH).getSimpleTypes();
	}

	/**
	 * Gets mapping
	 * @return
	 */
	private Map<String, Set<String>> getMapping() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();
		map.put("var_1", new HashSet<String>());
		map.get("var_1").add("$x");
		map.put("var_3", new HashSet<String>());
		map.get("var_3").add("$y");
		return map;
	}

	/**
	 * Gets report output
	 * @param fmt
	 * @return
	 * @throws TransformerException
	 */
	private OutputStream getReportOutput(IReportFormatter fmt) throws TransformerException {
		OutputStream os = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(os);
		fmt.format(report, ps);
		return os;
	}

	/**
	 * Checks if XML given is well formed
	 * @param string
	 * @return
	 */
	private boolean isXMLWellFormed(String string) {
		boolean ok = true;
		try {
			new XMLDocument(string);
		}
		catch(Exception ex) {
			ok = false;
		}
		return ok;
	}

	/**
	 * Checks is YAML is well formed
	 * @param string
	 * @return
	 */
	private boolean isYAMLWellFormed(String string) {
		boolean ok = true;
		try {
			Yaml yaml = new Yaml();
			yaml.load(string);
		}
		catch(Exception ex) {
			ok = false;
		}
		return ok;
	}

	/**
	 * No Report test
	 */
	@Test
	public void NoneFormatReportTest() throws Exception {
		String format = "none";
		IReportFormatter fmt = factory.newFormatter(format, ReportLevels.BASIC);
		OutputStream os = getReportOutput(fmt);
		assertThat("Basic Output", os.toString(), is(equalTo("")));
		fmt = factory.newFormatter(format, ReportLevels.ADVANCED);
		os = getReportOutput(fmt);
		assertThat("Advanced Output", os.toString(), is(equalTo("")));
		fmt = factory.newFormatter(format, ReportLevels.FULL);
		os = getReportOutput(fmt);
		assertThat("Full Output", os.toString(), is(equalTo("")));
	}


	/**
	 * Plain Report test
	 */
	@Test
	public void PlainFormatReportTest() throws Exception {
		String format = "plain";
		IReportFormatter fmt = factory.newFormatter(format, ReportLevels.BASIC);
		OutputStream os = getReportOutput(fmt);
		assertThat("Basic Output", os.toString(), not(is(equalTo(""))));
		fmt = factory.newFormatter(format, ReportLevels.ADVANCED);
		os = getReportOutput(fmt);
		assertThat("Advanced Output", os.toString(), not(is(equalTo(""))));
		fmt = factory.newFormatter(format, ReportLevels.FULL);
		os = getReportOutput(fmt);
		assertThat("Full Output", os.toString(), not(is(equalTo(""))));
	}

	/**
	 * XML Report test
	 */
	@Test
	public void XMLFormatReportTest() throws Exception {
		String format = "xml";
		IReportFormatter fmt = factory.newFormatter(format, ReportLevels.BASIC);
		OutputStream os = getReportOutput(fmt);
		assertTrue("Basic Output", isXMLWellFormed(os.toString()));
		fmt = factory.newFormatter(format, ReportLevels.ADVANCED);
		os = getReportOutput(fmt);
		assertTrue("Advanced Output", isXMLWellFormed(os.toString()));
		fmt = factory.newFormatter(format, ReportLevels.FULL);
		os = getReportOutput(fmt);
		assertTrue("Full Output", isXMLWellFormed(os.toString()));
	}

	/**
	 * YAML Report test
	 */
	@Test
	public void YAMLFormatReportTest() throws Exception {
		String format = "yaml";
		IReportFormatter fmt = factory.newFormatter(format, ReportLevels.BASIC);
		OutputStream os = getReportOutput(fmt);
		assertTrue("Basic Output", isYAMLWellFormed(os.toString()));
		fmt = factory.newFormatter(format, ReportLevels.ADVANCED);
		os = getReportOutput(fmt);
		assertTrue("Advanced Output", isYAMLWellFormed(os.toString()));
		fmt = factory.newFormatter(format, ReportLevels.FULL);
		os = getReportOutput(fmt);
		assertTrue("Full Output", isYAMLWellFormed(os.toString()));
	}

}
