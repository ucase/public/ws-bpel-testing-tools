import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.GregorianCalendar;
import java.util.Date;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class EventGenerator {

	private final String [] types = {"Integer", "Float", "Long", "Boolean", "String", "Date", "Time"};
	
	public static void main(String args[]) throws Exception {
		
		String input = args[0];
		String type = args[1];
		String output = "";
		String path = input.substring(0, input.lastIndexOf("/") + 1);
		SAXBuilder builder = new SAXBuilder();
	    File xmlFile = new File(input);
	    
	    try
	    {
	        //To build a document from the xml
	        Document document = (Document) builder.build(xmlFile);
	 
	        // To get the root
	        Element rootNode = document.getRootElement();
	        
	        output = rootNode.getAttributeValue("name") + "." + type;
	        FileWriter values = new FileWriter(path + output);
	        
	        if (type.equals("json")){
	        	new EventGenerator().JsonFormatValues(values, document);
	        }
	        if (type.equals("xml")){
	        	new EventGenerator().XmlFormatValues(values, document);
	        }
	        if (type.equals("csv")){
	        	new EventGenerator().CsvFormatValues(values, document);
	        }
	        
	    }catch ( IOException io ) {
	        System.out.println( io.getMessage() );
	    }catch ( JDOMException jdomex ) {
	        System.out.println( jdomex.getMessage() );
	    }
	}

	/**
	 * Read the xml structure to generate a csv file. The csv file contains feeds that
	 * are generated according to the defined structure
	 * @param values is a file that will contain the generated feeds
	 * @param document is the xml that has the structure
	 * @throws IOException files exceptions
	 */
	private void CsvFormatValues(FileWriter values, Document document) throws IOException {
		
		BufferedWriter bw = new BufferedWriter(values);
		StringBuilder sb = new StringBuilder();
		
		List list = document.getRootElement().getChildren("block");
		
		for (int i = 0; i < list.size(); i++){
			Element blck = (Element) list.get(i);
			
			if (blck.getAttributeValue("repeat") != null){
				List <Element> fields = blck.getChildren(); //field or optionalfield
				
				// To print the head of the file
				
				for (int f = 0; f < fields.size(); f++){
					if (fields.get(f).getName().equals("optionalfields")){
						List <Element> optionals = fields.get(f).getChildren();
						for (int o = 0; o < optionals.size(); o ++){
							sb.append(optionals.get(o).getAttributeValue("name"));
							
							if (o != optionals.size() - 1){
								sb.append(",");
							}
						}
					} else {
						sb.append(fields.get(f).getAttributeValue("name"));

						if (f != fields.size() - 1){
							sb.append(",");
						}
					}
				}
				bw.write(sb.toString() + "\n");
				sb.setLength(0);

				int repetition = Integer.parseInt(blck.getAttributeValue("repeat"));
				
				for (int r = 0; r < repetition; r++){ //Number of values to repeat
					
					for (int e = 0; e < fields.size(); e++){
						Element field = (Element) fields.get(e);

						if (field.getName().equals("optionalfields")){
							List <Element> optional = field.getChildren();
							int size = optional.size();
							Random rand = new Random();
							String value = "";
							
							if (field.getAttributeValue("possibleempty") == null){
								size--;
							}
							
							int chosen = rand.nextInt(((size - 0) + 1) + 0);
							if (chosen != size){
								Element elementop = (Element) optional.get(chosen);
								String quotes = elementop.getAttributeValue("quotes");
								String type = elementop.getAttributeValue("type");
								
								if (!ExistType(type)){
									value = GenerateValueComplexType(elementop);
									for (int coma = 0; coma < chosen; coma++){
										sb.append(",");
									}
									sb.append(value);
								} else {
									value = GenerateValueSimpleType(type, elementop);
									if (quotes.equals("true")){
										for (int coma = 0; coma < chosen; coma++){
											sb.append(",");
										}
										sb.append("\"" + value + "\"");
									} else {
										sb.append(value);
									}
								}
							} else {
								;
							}
						} else {
							String quotes = field.getAttributeValue("quotes");
							String type = field.getAttributeValue("type");
							String value = "";
							
							if (!ExistType(type)){ //A non basic type
								
								value = GenerateValueComplexType(field);
								sb.append(value);
							} else {//A basic type
								
								value = GenerateValueSimpleType(type, field);
								
								if (quotes.equals("true")){
									sb.append("\"" + value + "\"");
								} else {
									sb.append(value);
								}
							}
						}
						if ((e < fields.size() - 1)){
							sb.append(",");
						}
					}
					bw.write(sb.toString());
					sb.setLength(0);
					bw.write("\n"); //The end of a field
				}
			}
		}
		bw.close();	
		
	}

	/**
	 * Read the xml structure to generate a xml file. The generated xml file contains feeds that
	 * are generated according to the defined structure
	 * @param values is a file that will contain the generated feeds
	 * @param document is the xml that has the structure
	 * @throws IOException files exceptions
	 */
	private void XmlFormatValues(FileWriter values, Document document) throws IOException {
		
		BufferedWriter bw = new BufferedWriter(values);
		StringBuilder sb = new StringBuilder();
		
		List list = document.getRootElement().getChildren("block");
		bw.write("<xml>\n"); //The beginning of a xml
		
		for (int i = 0; i < list.size(); i++){
			Element blck = (Element) list.get(i);
			String nameblck = blck.getAttributeValue("name");
			bw.write("<" + nameblck + ">\n"); 
			
			if (blck.getAttributeValue("value") != null){
				
				bw.write(blck.getAttributeValue("value"));
				bw.write("\n</" + nameblck + ">\n");
			}
			
			if (blck.getAttributeValue("repeat") != null){
				List <Element> fields = blck.getChildren(); //field or optionalfield
				int repetition = Integer.parseInt(blck.getAttributeValue("repeat"));
				
				for (int r = 0; r < repetition; r++){ //Number of values to repeat
					sb.append("<feed>\n"); //The beginning of a field
					
					for (int e = 0; e < fields.size(); e++){
						Element field = (Element) fields.get(e);

						if (field.getName().equals("optionalfields")){
							List <Element> optional = field.getChildren();
							int size = optional.size();
							Random rand = new Random();
							String value = "";
							
							if (field.getAttributeValue("possibleempty") == null){
								size--;
							}
							
							int chosen = rand.nextInt(((size - 0) + 1) + 0);
							if (chosen != size){
								Element elementop = (Element) optional.get(chosen);
								sb.append("<" + elementop.getAttributeValue("name") + " type=\"" + elementop.getAttributeValue("type") + "\">");
								String quotes = elementop.getAttributeValue("quotes");
								String type = elementop.getAttributeValue("type");
								
								if (!ExistType(type)){
									value = GenerateValueComplexType(elementop);
									sb.append(value);
								} else {
									value = GenerateValueSimpleType(type, elementop);
									if (quotes.equals("true")){
										sb.append("\"" + value + "\"");
									} else {
										sb.append(value);
									}
								}
							} else {
								;
							}
						} else {
							sb.append("<" + field.getAttributeValue("name") + " type=\"" + field.getAttributeValue("type") + "\">");
							String quotes = field.getAttributeValue("quotes");
							String type = field.getAttributeValue("type");
							String value = "";
							
							if (!ExistType(type)){ //A non basic type
								
								value = GenerateValueComplexType(field);
								sb.append(value);
							} else {//A basic type
								
								value = GenerateValueSimpleType(type, field);
								
								if (quotes.equals("true")){
									sb.append("\"" + value + "\"");
								} else {
									sb.append(value);
								}
							}
						}
						sb.append("</" + field.getAttributeValue("name") + ">\n");
					}
					bw.write(sb.toString());
					sb.setLength(0);
					bw.write("</feed>\n"); //The end of a field
				}
				bw.write("</feeds>\n");
			}			
		}
		bw.write("</xml>");
		bw.close();	
		
	}

	/**
	 * Read the xml structure to generate a json file. The json file contains feeds that
	 * are generated according to the defined structure
	 * @param values is a file that will contain the generated feeds
	 * @param document is the xml that has the structure
	 * @throws IOException files exceptions
	 */
	private void JsonFormatValues(FileWriter values, Document document) throws IOException {
		
		BufferedWriter bw = new BufferedWriter(values);
		StringBuilder sb = new StringBuilder();
		
		List list = document.getRootElement().getChildren("block");
		bw.write("{"); //The beginning of a json
		
		for (int i = 0; i < list.size(); i++){
			Element blck = (Element) list.get(i);
			String nameblck = blck.getAttributeValue("name");
			bw.write("\"" + nameblck + "\":"); 
			
			if (blck.getAttributeValue("value") != null){
				
				bw.write("{" + blck.getAttributeValue("value") + "}");
			}
			
			if (blck.getAttributeValue("repeat") != null){
				bw.write("[");
				List <Element> fields = blck.getChildren(); //field or optionalfield
				int repetition = Integer.parseInt(blck.getAttributeValue("repeat"));
				
				for (int r = 0; r < repetition; r++){ //Number of values to repeat
					sb.append("{"); //The beginning of a field
					
					for (int e = 0; e < fields.size(); e++){
						Element field = (Element) fields.get(e);

						if (field.getName().equals("optionalfields")){
							List <Element> optional = field.getChildren();
							int size = optional.size();
							Random rand = new Random();
							String value = "";
							
							if (field.getAttributeValue("possibleempty") == null){
								size--;
							}
							
							int chosen = rand.nextInt(((size - 0) + 1) + 0);
							if (chosen != size){
								Element elementop = (Element) optional.get(chosen);
								sb.append("\"" + elementop.getAttributeValue("name") + "\":");
								String quotes = elementop.getAttributeValue("quotes");
								String type = elementop.getAttributeValue("type");
								
								if (!ExistType(type)){
									value = GenerateValueComplexType(elementop);
									sb.append(value);
								} else {
									value = GenerateValueSimpleType(type, elementop);
									if (quotes.equals("true")){
										sb.append("\"" + value + "\"");
									} else {
										sb.append(value);
									}
								}
							} else {
								int sizelenth = sb.length();
								sb.deleteCharAt(sizelenth - 1);
							}
						} else {
							sb.append("\"" + field.getAttributeValue("name") + "\":");
							String quotes = field.getAttributeValue("quotes");
							String type = field.getAttributeValue("type");
							String value = "";
							
							if (!ExistType(type)){ //A non basic type
								
								value = GenerateValueComplexType(field);
								sb.append(value);
							} else {//A basic type
								
								value = GenerateValueSimpleType(type, field);
								
								if (quotes.equals("true")){
									sb.append("\"" + value + "\"");
								} else {
									sb.append(value);
								}
							}
						}
						if ((e < fields.size() - 1)){
							sb.append(",");
						}
					}
					bw.write(sb.toString());
					sb.setLength(0);
					bw.write("}"); //The end of a field
					if (r < repetition - 1){
						bw.write(",");
					}
				}					
				bw.write("]");
			}
			
			if (i < list.size() - 1){
				bw.write(",");
			}
		}
		bw.write("}");
		bw.close();	
	}
	
	/**
	 * The assigned type is in the IoT-EG. This function select the correct
	 * value generator according to the type. 
	 * @param type is the String to check
	 * @param field The element which the type @param type
	 * @return a value of the specific type according to the declared restriction and
	 * attributes of the Element field
	 */
	private String GenerateValueSimpleType(String type, Element field) {

		String value = "";
		
		switch(type){
		case "Integer":
			value = GenerateInteger(field);
			break;
		case "Float":
			value = GenerateFloat(field);
			break;
		case "Long":
			value = GenerateLong(field);
			break;
		case "String":
			value = GenerateString(field);
			break;
		case "Boolean":
			value = GenerateBoolean(field);
			break;
		case "Date":
			value = GenerateDate(field);
			break;
		case "Time":
			value = GenerateTime(field);
			break;
		default:
			break;
		}
		
		return value;
	}

	/**
	 * The assigned type is not in the IoT-EG. This function construct the complextype 
	 * (composed by simple types). 
	 * @param field The element which a complextype
	 * @return a value of the specific type according to the declared restriction and
	 * attributes of the Element field
	 */
	private String GenerateValueComplexType(Element field) {
		
		String quotes = field.getAttributeValue("quotes");
		String value = "";
		String finalvalue = "";
		String result = "";
		
		Element complextype = field.getChild("type");
		List <Element> simpletype = complextype.getChildren();
		
		if (field.getAttributeValue("get") != null){ // Default: get attribute doesn't exist
			Random rand = new Random();
			int max = simpletype.size() - 1;
			int chosen = rand.nextInt(((max - 0) + 1) + 0);
			Element simple = (Element) simpletype.get(chosen);
			String stype = simple.getAttributeValue("type");
			finalvalue = GenerateValueSimpleType(stype, simple);
		} else {
			for (int s = 0; s < simpletype.size(); s++){
				Element simple = (Element) simpletype.get(s);
				String stype = simple.getAttributeValue("type");
				value = GenerateValueSimpleType(stype, simple);
				finalvalue = finalvalue + value;
			}
		}
		
		if (quotes.equals("true")){
			result = "\"" + finalvalue + "\"";
		} else {
			result = finalvalue;
		}
		
		return result;
	}

	/**
	 * Determine if the type is a "basic" type include in the IoT-EG
	 * @param type is the String to check
	 * @return true or false
	 */
	private boolean ExistType(String type) {
		boolean exist = false;

		for (int i = 0; i < types.length; i++){
			if (type.equals(types[i])){
				exist = true;
			}
		}
		return exist;
	}
	
	/**
	 * Generate a value of Time type
	 * @param field is the Element in which the value will be assigned
	 * @return a value of the Time type according to the declared restriction and
	 * attributes of the Element field
	 */
	private String GenerateTime(Element field) {
		String result = "";
		
		if (field.getAttributeValue("value") != null){
			result = field.getAttributeValue("value");
		} else {
			if (field.getAttributeValue("mode") != null){
				java.util.Date randomDate = RandomUtil.getRandomDate(new java.util.Date(RandomUtil.getMinimumDate()), new java.util.Date(RandomUtil.getMaximumDate()), false);
				SimpleDateFormat sdf = new SimpleDateFormat(field.getAttributeValue("mode"));
				result = sdf.format(randomDate);
			}
		}
		
		return result;
	}

	/**
	 * Generate a value of Date type
	 * @param field is the Element in which the value will be assigned
	 * @return a value of the Date type according to the declared restriction and
	 * attributes of the Element field
	 */
	private String GenerateDate(Element field) {
		String result = "";
		
		if (field.getAttributeValue("value") != null){
			result = field.getAttributeValue("value");
		} else {
			if (field.getAttributeValue("mode") != null){
				java.util.Date dateFromDB = new java.util.Date(System.currentTimeMillis());
				Calendar calendarFromDB = Calendar.getInstance();

				calendarFromDB.setTime(dateFromDB);

				java.util.Date randomDate = RandomUtil.getRandomDate(new java.util.Date(RandomUtil.getMinimumDate()), new java.util.Date(RandomUtil.getMaximumDate()), false);
				SimpleDateFormat sdf = new SimpleDateFormat(field.getAttributeValue("mode"));
				result = sdf.format(randomDate);
			}
		}
		
		return result;
	}

	/**
	 * Generate a value of Boolean type
	 * @param field is the Element in which the value will be assigned
	 * @return a value of the Boolean type according to the declared restriction and
	 * attributes of the Element field
	 */
	private String GenerateBoolean(Element field) {
		String result = "";
		Random rand = new Random();
		boolean value;
		
		if (field.getAttributeValue("value") != null){
			result = field.getAttributeValue("value");
		} else {
			if (field.getAttributeValue("kind").equals("n")){
				value = rand.nextBoolean();
				if (value){
					result = "1";
				} else {
					result = "0";
				}
			} else {
				value = rand.nextBoolean();
				result = Boolean.toString(value);
			}
		}
		
		return result;
	}

	/**
	 * Generate a value of String type
	 * @param field is the Element in which the value will be assigned
	 * @return a value of the String type according to the declared restriction and
	 * attributes of the Element field
	 */
	private String GenerateString(Element field) {
		String result = "";
		
		if (field.getAttributeValue("value") != null){
			result = field.getAttributeValue("value");
		} else {
			if (field.getAttributeValue("long") != null){
				
				result = getRandString(Integer.parseInt(field.getAttributeValue("long")));
				
				if (field.getAttributeValue("kind") != null){
					if (field.getAttributeValue("kind").equals("low")){
						result = result.toLowerCase();
					}
				}
			} else {
					result = getRandString(10); // Default length
				
				if (field.getAttributeValue("kind") != null){
					if (field.getAttributeValue("kind").equals("low")){
						result = result.toLowerCase();
					}
				}	
			}
		}
		
		return result;
	}
	
	/**
	 * Generate a random String
	 * @param longt determines the length of the String
	 * @return a random String with a longt length
	 */
	private String getRandString(int longt) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < longt) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

	/**
	 * Generate a value of Float type
	 * @param field is the Element in which the value will be assigned
	 * @return a value of the Float type according to the declared restriction and
	 * attributes of the Element field
	 */
	private String GenerateFloat(Element field) {
		String result = "";
		Random rand = new Random();
		
		if (field.getAttributeValue("value") != null){
			result = field.getAttributeValue("value");
		} else {
			// Returns a pseudo-random number between min and max, inclusive
			if ((field.getAttributeValue("max") != null) && (field.getAttributeValue("min") != null)){
				float max = Float.parseFloat(field.getAttributeValue("max"));
				float min = Float.parseFloat(field.getAttributeValue("min"));
				result = Float.toString(rand.nextFloat() * (max - min) + min);
			} else {
				// Default values
				float max = (float) 0.0;
				float min = (float) 10.0;
				result = Float.toString(rand.nextFloat() * (max - min) + min);
			}
			
			if (field.getAttributeValue("long") != null){
				result = round(result, Integer.parseInt(field.getAttributeValue("long")));
			}
		}
		
		return result;
	}

	/**
	 * Round a float number
	 * @param result contains the float number
	 * @param decimalPlace determines the length 
	 * @return the float number rounded according to the decimalPlace value
	 */
	private String round(String result, int decimalPlace) {
		float d = Float.parseFloat(result);
	    BigDecimal bd = new BigDecimal(Float.toString(d));
	    bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
	    return bd.toString();
	}

	/**
	 * Generate a value of Integer type
	 * @param field is the Element in which the value will be assigned
	 * @return a value of the Integer type according to the declared restriction and
	 * attributes of the Element field
	 */
	private String GenerateInteger(Element field) {
		String result = "";
		Random rand = new Random();
		
		if (field.getAttributeValue("value") != null){
			result = field.getAttributeValue("value");
		} else {
			// Returns a pseudo-random number between min and max, inclusive
			if ((field.getAttributeValue("max") != null) && (field.getAttributeValue("min") != null)){
				int max = Integer.parseInt(field.getAttributeValue("max"));
				int min = Integer.parseInt(field.getAttributeValue("min"));
				result = Integer.toString(rand.nextInt(((max - min) + 1) + min));
			} else {
				// Default values
				int max = 0;
				int min = 9;
				result = Integer.toString(rand.nextInt(((max - min) + 1) + min));
			}
		}		
		
		return result;
	}

	/**
	 * Generate a value of Long type
	 * @param field is the Element in which the value will be assigned
	 * @return a value of the Long type according to the declared restriction and
	 * attributes of the Element field
	 */
	private String GenerateLong(Element field) {
		String result = "";
		Random rand = new Random();
		
		if (field.getAttributeValue("value") != null){
			result = field.getAttributeValue("value");
		} else {
			// Returns a pseudo-random number between min and max, inclusive
			if ((field.getAttributeValue("max") != null) && (field.getAttributeValue("min") != null)){
				long max = Long.parseLong(field.getAttributeValue("max"));
				long min = Long.parseLong(field.getAttributeValue("min"));
				result = Long.toString((long) (min + rand.nextDouble() * (max - min)));
			} else {
				// Default values
				long max = 0;
				long min = 9;
				result = Long.toString((long) (min + rand.nextDouble() * (max - min)));
			}
		}
		
		return result;
	}
}
