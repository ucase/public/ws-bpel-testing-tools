/*
 *  Copyright 2011-2013 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.api.types;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.ITypeVisitor;

/**
 * Class that represents the DateTime
 *
 */
public class TypeDateTime extends AbstractDates {

    private static final String TYPE_NAME = "DateTime";

	/**
     * Default constructor
     * @throws DatatypeConfigurationException 
     */
    public TypeDateTime() throws DatatypeConfigurationException {
    	super(TYPE_NAME);
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        this.setMaxValue(factory.newXMLGregorianCalendar(MAXYEAR, MAXMONTH, MAXDAY, MAXHOUR, MAXMINUTE, MAXSECOND, MAXMILLISECOND, DatatypeConstants.FIELD_UNDEFINED));
        this.setMinValue(factory.newXMLGregorianCalendar(MINYEAR, MINMONTH, MINDAY, MINHOUR, MINMINUTE, MINSECOND, MINMILLISECOND, DatatypeConstants.FIELD_UNDEFINED));
    }

    /**
     * Constructor that set minimum value and maximum value
     * @param minValue
     * @param maxValue 
     */
    public TypeDateTime(XMLGregorianCalendar minValue, XMLGregorianCalendar maxValue) {
    	super(TYPE_NAME);
        this.setMinValue(minValue);
        this.setMaxValue(maxValue);
    }

    @Override
    public Object accept(ITypeVisitor visitor) throws GenerationException {
        return visitor.visit(this);
    }
}
