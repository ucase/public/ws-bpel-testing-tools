/*
 *  Copyright 2011-2015 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es),
 *  Antonio García-Domínguez (antonio.garciadominguez@uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.api.types;

import java.math.BigDecimal;
import java.util.List;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.ITypeVisitor;

/**
 * Class that represents a type containing floating-point values.
 */
public class TypeFloat extends AbstractNumber<BigDecimal> {

    private static final String TYPE_NAME = "Float";
	private static final BigDecimal DEFAULT_MIN_VALUE = new BigDecimal("-100000000");
    private static final BigDecimal DEFAULT_MAX_VALUE = new BigDecimal("100000000");

    private int fractionDigits = -1;

    /**
     * Default constructor.
     */
    public TypeFloat() {
        this(null);
    }
    
    /**
     * Build a TypeFloat with a list of valid values.
     * @param listValues 
     */
    public TypeFloat(List<BigDecimal> listValues) {
        this(listValues, DEFAULT_MIN_VALUE, DEFAULT_MAX_VALUE);
    }

    /**
     * Constructor that sets the minimum value and the maximum value.
     * @param minValue
     * @param maxValue 
     */
    public TypeFloat(BigDecimal minValue, BigDecimal maxValue) {
        this(null, minValue, maxValue);
    }

    /**
     * Convenience version of {@link #TypeFloat(BigDecimal, BigDecimal)}.
     */
    public TypeFloat(double minValue, double maxValue) {
    	this(null, new BigDecimal(minValue), new BigDecimal(maxValue));
    }

    /**
     * Convenience version of {@link #TypeFloat(BigDecimal, BigDecimal)}.
     */
    public TypeFloat(String minValue, String maxValue) {
    	this(null, new BigDecimal(minValue), new BigDecimal(maxValue));
    }

    private TypeFloat(List<BigDecimal> allowedValues, BigDecimal minValue, BigDecimal maxValue) {
        super(allowedValues, TYPE_NAME);
        setMinValue(minValue);
        setMaxValue(maxValue);
        if (minValue != null && maxValue != null && minValue.compareTo(maxValue) > 0) {
        	throw new IllegalArgumentException("min value must be less than or equal than max value");
        }
    }

    @Override
    public Object accept(ITypeVisitor visitor) throws GenerationException {
        return visitor.visit(this);
    }

    /**
     * Return the fraction digits, if value is negative then no value assigned
     * @return the fractionDigits
     */
    public int getFractionDigits() {
        return fractionDigits;
    }

    /**
     * Set the fraction digits
     * @param fractionDigits the fractionDigits to set
     */
    public void setFractionDigits(int fractionDigits) {
        this.fractionDigits = fractionDigits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TypeFloat typeFloat = (TypeFloat) o;
        if (fractionDigits != typeFloat.fractionDigits) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + fractionDigits;
        return result;
    }
}
