package es.uca.webservices.testgen.api.types;

import java.util.ArrayList;
import java.util.List;

/**
 * Common superclass for all number types.
 *
 * @author Antonio Garcia-Dominguez
 */
public abstract class AbstractNumber<T> extends AbstractType {

    private List<T> allowedValues = new ArrayList<T>();
    private int totalDigits = 0;
    private T minValue, maxValue;

    public AbstractNumber(List<T> allowedValues, String typeName) {
    	super(typeName);
        if (allowedValues != null) {
            this.allowedValues = allowedValues;
        }
    }

    public List<T> getAllowedValues() {
        return allowedValues;
    }

    public void setAllowedValues(List<T> allowed) {
    	this.allowedValues = allowed;
    }
    
    /**
     * Return the totalDigits, if value equals 0 then no value assigned
     * @return the totalDigits
     */
    public int getTotalDigits() {
        return totalDigits;
    }

    /**
     * @param totalDigits the totalDigits to set
     */
    public void setTotalDigits(int totalDigits) {
        this.totalDigits = totalDigits;
    }

    public T getMinValue() {
        return minValue;
    }

    public void setMinValue(T minValue) {
        this.minValue = minValue;
    }

    public T getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(T maxValue) {
        this.maxValue = maxValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        @SuppressWarnings("rawtypes")
		AbstractNumber that = (AbstractNumber) o;

        if (totalDigits != that.totalDigits) return false;
        if (allowedValues != null ? !allowedValues.equals(that.allowedValues) : that.allowedValues != null)
            return false;
        if (maxValue != null ? !maxValue.equals(that.maxValue) : that.maxValue != null) return false;
        if (minValue != null ? !minValue.equals(that.minValue) : that.minValue != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = allowedValues != null ? allowedValues.hashCode() : 0;
        result = 31 * result + totalDigits;
        result = 31 * result + (minValue != null ? minValue.hashCode() : 0);
        result = 31 * result + (maxValue != null ? maxValue.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return getClass().getName() + " [allowedValues=" + allowedValues
				+ ", totalDigits=" + totalDigits + ", minValue=" + minValue
				+ ", maxValue=" + maxValue + "]";
	}


}
