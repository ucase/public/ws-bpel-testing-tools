/*
 *  Copyright 2011-2014
 *     Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es)
 *     Antonio García Domínguez (antonio.garciadominguez@uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.api.generators;


import java.util.Random;

/**
 * Interface for test case data generators.
 *
 * @author Miguel Ángel Pérez Montero, Antonio García Domínguez
 */
public interface IStrategy {
	/**
	 * Generates an array of values for the specified types, in the same order.
	 */
	Object[] generate(IType... abstractTypes) throws GenerationException;

	/**
	 * Configures the strategy with a specific set of options.
	 * 
	 * @throws IllegalArgumentException
	 *             The strategy does not accept options, or some option has an
	 *             invalid value.
	 */
	void init(Object... options) throws IllegalArgumentException;

}
