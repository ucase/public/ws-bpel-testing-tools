package es.uca.webservices.testgen.api.utils;

import java.util.List;

import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;

/**
 * Utility class with common tasks to be performed on types of the TestGenerator API.
 *
 * @author Antonio García-Domínguez
 */
public final class GeneratorUtils {

	private GeneratorUtils() {}

	/**
	 * Generates <code>n</code> tests using the provided generator and sends
	 * them to the formatter. Users of this class should then call
	 * {@link IFormatter#save(java.io.OutputStream)} to save the tests in the
	 * desired format, or call any other implementation-dependent method to
	 * access the generated tests directly.
	 * 
	 * @param n Number of tests to be generated.
	 * @param types Variables to be considered.
	 * @param fmt Formatter which should receive the generated test cases.
	 * @param tg Generator which should produce the test cases.
	 */
	public static void generate(int n, final List<IType> types, IFormatter fmt, IStrategy tg) throws GenerationException {
	    fmt.suiteStart();
	    for (int i = 0; i < n; ++i) {
	    	fmt.testStart();
	    	Object[] generated = tg.generate(types.toArray(new IType[0]));
	    	int j=0;
	    	for (IType t : types) {
	    		fmt.valueFor(t.getNameVariable(), generated[j]);
	    		++j;
	    	}
	    	fmt.testEnd();
	    }
	    fmt.suiteEnd();
	}
}
