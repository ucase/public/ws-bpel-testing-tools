/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.api.types;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.ITypeVisitor;

/**
 * Class that represents the Duration
 * 
 */
public class TypeDuration extends AbstractType {

    private static final String TYPE_NAME = "Duration";
	private Duration minValue;
    private Duration maxValue;

    /**
     * Default constructor
     * @throws DatatypeConfigurationException 
     */
    public TypeDuration() throws DatatypeConfigurationException {
    	super(TYPE_NAME);
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        this.minValue = factory.newDuration("P0Y");
        this.maxValue = factory.newDuration("P10Y");
    }

    /**
     * Contructor that set minimum value and maximum value
     * @param minValue
     * @param maxValue 
     */
    public TypeDuration(Duration minValue, Duration maxValue) {
    	super(TYPE_NAME);
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @Override
    public Object accept(ITypeVisitor visitor) throws GenerationException {
        return visitor.visit(this);
    }

    /**
     * @return the min
     */
    public Duration getMinValue() {
        return minValue;
    }

    /**
     * @param min the min to set
     */
    public void setMinValue(Duration min) {
        this.minValue = min;
    }

    /**
     * @return the maxValue
     */
    public Duration getMaxValue() {
        return maxValue;
    }

    /**
     * @param max the maxValue to set
     */
    public void setMaxValue(Duration max) {
        this.maxValue = max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypeDuration that = (TypeDuration) o;

        if (maxValue != null ? !maxValue.equals(that.maxValue) : that.maxValue != null) return false;
        if (minValue != null ? !minValue.equals(that.minValue) : that.minValue != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = minValue != null ? minValue.hashCode() : 0;
        result = 31 * result + (maxValue != null ? maxValue.hashCode() : 0);
        return result;
    }
}
