/*
 *  Copyright 2013 Antonio García-Domínguez (antonio.garciadominguez@uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.api.generators;

import es.uca.webservices.testgen.api.types.TypeDate;
import es.uca.webservices.testgen.api.types.TypeDateTime;
import es.uca.webservices.testgen.api.types.TypeDuration;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTime;
import es.uca.webservices.testgen.api.types.TypeTuple;

/**
 * Interface for a generic visitor of a tree of {@link IType} instances.
 *
 */
public interface ITypeVisitor {
	Object visit(IType type) throws GenerationException;
	
	/**
     * Generated a Date random of the typeDate the terms of the received
     * 
     * @param typeDate
     * @return
     */
	Object visit(TypeDate typeDate) throws GenerationException;

	/**
     * Generated a DateTime random of the typeDateTime the terms of the received
     * 
     * @param typeDateTime
     * @return
     */
	Object visit(TypeDateTime typeDateTime) throws GenerationException;

	/**
     * Generates a TypeDuration random of the typeDuration the terms of the received
     * @param typeDuration
     * @return
     * @throws GenerationException 
     */
	Object visit(TypeDuration typeDuration) throws GenerationException;
	
	/**
     * Generated a float random of the typeFloat terms of the received
     * 
     * @param typeFloat
     * @return
     */
	Object visit(TypeFloat typeFloat) throws GenerationException;
	
	/**
     * Generated a BigInteger random with the terms of the typeInt received
     * 
     * @param typeInt
     * @return
     */
	Object visit(TypeInt typeInt) throws GenerationException;
	
	/**
     * Generated a List<Object> random of the typeList the terms of the received
     * 
     * @param typeList
     * @return
     */
	Object visit(TypeList typeList) throws GenerationException;
	
	/**
     * Generated a String random of the typeString the terms of the received
     * 
     * @param typeString
     * @return
     */
	Object visit(TypeString typeString) throws GenerationException;

	/**
     * Generated a Time random of the typeTime the terms of the received
     * 
     * @param typeTime
     * @return
     */
	Object visit(TypeTime typeTime) throws GenerationException;
	
	/**
     * Generated a List<Object> random of the typeTuple the terms of the
     * received
     * 
     * @param typeTuple
     * @return
     */
	Object visit(TypeTuple typeTuple) throws GenerationException;
}
