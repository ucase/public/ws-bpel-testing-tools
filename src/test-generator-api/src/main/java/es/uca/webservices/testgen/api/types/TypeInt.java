/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.api.types;

import java.math.BigInteger;
import java.util.List;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.ITypeVisitor;


/**
 * Class that represents the integer
 *
 */
public class TypeInt extends AbstractNumber<BigInteger> {

    private static final String TYPE_NAME = "Int";
	public static final BigInteger DEFAULT_MIN_VALUE = new BigInteger("-100000000");
    public static final BigInteger DEFAULT_MAX_VALUE = new BigInteger("100000000");

    /**
     * Default constructor
     */
    public TypeInt() {
        this(null);
    }
    
    /**
     * Build a TypeInt with a list of valid values
     * @param listValues 
     */
    public TypeInt(List<BigInteger> listValues) {
        this(listValues, DEFAULT_MIN_VALUE, DEFAULT_MAX_VALUE);
    }

    /**
     * Constructor that sets the minimum value and the maximum value.
     * @param minValue
     * @param maxValue
     */
    public TypeInt(BigInteger minValue, BigInteger maxValue) {
        this(null, minValue, maxValue);
    }

    /**
     * Constructor that sets the minimum value and the maximum value.
     * @param minValue
     * @param maxValue 
     */
    public TypeInt(long minValue, long maxValue) {
        this(null, BigInteger.valueOf(minValue), BigInteger.valueOf(maxValue));
    }

    @Override
    public Object accept(ITypeVisitor visitor) throws GenerationException {
        return visitor.visit(this);
    }

    /**
     * Set the maximum value
     * @param maxValue the maxValue to set
     */
    public void setMaxValue(long maxValue) {
        setMaxValue(BigInteger.valueOf(maxValue));
    }

    /**
     * Set the minimum value
     * @param minValue the minValue to set
     */
    public void setMinValue(long minValue) {
        setMinValue(BigInteger.valueOf(minValue));
    }

    private TypeInt(List<BigInteger> allowedValues, BigInteger minValue, BigInteger maxValue) {
        super(allowedValues, TYPE_NAME);
        setMinValue(minValue);
        setMaxValue(maxValue);
        if (minValue != null && maxValue != null && minValue.compareTo(maxValue) > 0) {
        	throw new IllegalArgumentException("min value must be less than or equal than max value");
    }
    }

}
