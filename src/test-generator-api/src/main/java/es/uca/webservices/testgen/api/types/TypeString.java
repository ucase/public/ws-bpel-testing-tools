/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.api.types;

import java.util.ArrayList;
import java.util.List;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.ITypeVisitor;


/**
 * Class that represents String
 *
 */
public class TypeString extends AbstractType {

    private static final String TYPE_NAME = "String";
	private static final int DEFAULT_MAXLENGTH = 20;
    private List<String> listValues = new ArrayList<String>(); // store the list of allowed values
    private String pattern = ""; // store the regex
    private int maxLength = DEFAULT_MAXLENGTH;
    private int minLength = 0;

    /**
     * Default constructor
     */
    public TypeString() {
    	super(TYPE_NAME);
    }

    /**
     * Build a TypeString with a list of valid values
     * @param listValues 
     */
    public TypeString(List<String> listValues) {
    	super(TYPE_NAME);
        this.listValues = listValues;
    }

    /**
     * Build a TypeString with a regx
     * @param pattern 
     */
    public TypeString(String pattern) {
    	super(TYPE_NAME);
        this.pattern = pattern;
    }

    @Override
    public Object accept(ITypeVisitor visitor) throws GenerationException {
        return visitor.visit(this);
    }

    @Override
	public String toString() {
		return "TypeString [listValues=" + listValues + ", pattern=" + pattern
				+ ", maxLength=" + maxLength + ", minLength=" + minLength + "]";
	}

	/**
     * @return the listValues
     */
    @SuppressWarnings("rawtypes")
    public List getListValues() {
        return listValues;
    }

    /**
     * @return the pattern
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * checks if it contains pattern
     * @return 
     */
    public boolean isSetPattern() {
        return !pattern.equals("");
    }

    /**
     * Return the maximum lenght of the string
     * @return the maxLength
     */
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * Set the maximum lenght of the string
     * @param maxLength the maxLength to set
     */
    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    /**
     * Return the minimum lenght of the string
     * @return the minLength
     */
    public int getMinLength() {
        return minLength;
    }

    /**
     * Set the minimum lenght of the string
     * @param minLength the minLength to set
     */
    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypeString that = (TypeString) o;

        if (maxLength != that.maxLength) return false;
        if (minLength != that.minLength) return false;
        if (listValues != null ? !listValues.equals(that.listValues) : that.listValues != null) return false;
        if (pattern != null ? !pattern.equals(that.pattern) : that.pattern != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = listValues != null ? listValues.hashCode() : 0;
        result = 31 * result + (pattern != null ? pattern.hashCode() : 0);
        result = 31 * result + maxLength;
        result = 31 * result + minLength;
        return result;
    }
}
