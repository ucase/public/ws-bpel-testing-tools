/*
 *  Copyright 2011-2013 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es),
 *  Antonio García-Domínguez (antonio.garciadominguez@uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.api.types;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.generators.ITypeVisitor;


/**
 * Immutable list type.
 */
public class TypeList extends AbstractType {

    private static final String TYPE_NAME = "List";
	private static final int DEFAULT_MAXLENGTH = 100; // Maximum number of elements by default
    private static final int DEFAULT_MINLENGTH = 1;
	private IType type;
    private int minNumElement;
    private int maxNumElement;

    /**
     * Contructor TypeList with the specified IType and set minimum and maximum number of elements
     * @param type
     * @param minNumElement
     * @param maxNumElement 
     */
    public TypeList(IType type, int minNumElement, int maxNumElement) {
    	super(TYPE_NAME);
        this.type = type;
        this.minNumElement = minNumElement;
        this.maxNumElement = maxNumElement;
    }

    /**
     * Contructor TypeList with the specified IType and set minimum number of elements
     * @param type
     * @param minNumElement 
     */
    public TypeList(IType type, int minNumElement) {
    	this(type, minNumElement, DEFAULT_MAXLENGTH);
    }

    /**
     * Consructor TypeList with the specified IType
     * @param type 
     */
    public TypeList(IType type) {
    	this(type, DEFAULT_MINLENGTH, DEFAULT_MAXLENGTH);
    }

    /**
     * Return the IType
     * @return the type
     */
    public IType getType() {
        return type;
    }

    /**
     * Return the minimum number the elements
     * @return the minNumElement
     */
    public int getMinNumElement() {
        return minNumElement;
    }

    /**
     * Return the maximum number of the elements
     * @return the maxNumElemnet
     */
    public int getMaxNumElement() {
        return maxNumElement;
    }

    @Override
    public Object accept(ITypeVisitor visitor) throws GenerationException {
        return visitor.visit(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypeList typeList = (TypeList) o;

        if (maxNumElement != typeList.maxNumElement) return false;
        if (minNumElement != typeList.minNumElement) return false;
        if (type != null ? !type.equals(typeList.type) : typeList.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + minNumElement;
        result = 31 * result + maxNumElement;
        return result;
    }

	@Override
	public String toString() {
		return "TypeList [type=" + type + ", minNumElement=" + minNumElement + ", maxNumElement=" + maxNumElement + "]";
	}
}
