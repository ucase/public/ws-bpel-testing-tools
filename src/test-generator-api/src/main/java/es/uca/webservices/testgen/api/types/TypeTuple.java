/*
 *  Copyright 2011 Miguel Ángel Pérez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.api.types;

import java.util.Arrays;
import java.util.Collection;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.generators.ITypeVisitor;


/**
 * Class that represents n-tuple
 *
 */
public class TypeTuple extends AbstractType {

    private static final String TYPE_NAME = "Tuple";
	private IType[] types = null;

    /**
     * Constructor n-tuple with the specified types
     * @param types 
     */
    public TypeTuple(IType... types) {
    	super(TYPE_NAME);
        this.types = types;
    }

    public TypeTuple(Collection<IType> types) {
    	this(types.toArray(new IType[types.size()]));
    }

    /**
     * Return size of the n-tuple
     * @return 
     */
    public int size() {
        return types.length;
    }

    /**
     * Return the IType specified position 
     * @param index
     * @return 
     */
    public IType getIType(int index) {
        return types[index];
    }

    @Override
    public Object accept(ITypeVisitor visitor) throws GenerationException {
        return visitor.visit(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypeTuple typeTuple = (TypeTuple) o;

        if (!Arrays.equals(types, typeTuple.types)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return types != null ? Arrays.hashCode(types) : 0;
    }
}
