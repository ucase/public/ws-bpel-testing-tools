/*
 *  Copyright 2011 Miguel Ã�ngel PÃ©rez Montero (miguelangel.perezmontero@alum.uca.es).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.testgen.api.parsers;

import java.util.List;

import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;

/**
 * Interface for classes that will process the input files to the program
 */
public interface IParser {
    /**
     * Method that processes the file
     * @param args
     * @throws ParserException 
     */
    List<IType> parse(String... args) throws ParserException;

    /**
	 * Returns the strategy with name <code>name</code>. If <code>name</code> is
	 * <code>null</code> or the empty string, returns the default strategy for
	 * that parser.
     * @throws ParserException 
	 */
    IStrategy getStrategy(String name) throws ParserException;
}
