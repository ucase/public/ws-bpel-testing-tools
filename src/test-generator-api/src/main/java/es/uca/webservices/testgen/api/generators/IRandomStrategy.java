package es.uca.webservices.testgen.api.generators;

import java.util.Random;

public interface IRandomStrategy extends IStrategy {
	/**
	 * Changes the seed used for the existing pseudorandom number generator
	 * (PRNG).
	 * 
	 * @param seed
	 *            New seed to be used from now on.
	 */
	void setSeed(String seed);

	/**
	 * Returns the currently-used pseudorandom number generator.
	 */
	Random getPRNG();

	/**
	 * Changes the seed of the internal PRNG. The default PRNG uses 128-bit
	 * seeds (as it is based on the Mersenne Twister), but we may be using a
	 * different PRNG. In that case, the seed will be parsed as a long and
	 * passed to the standard {@link Random#setSeed(long)} method.
	 */
	void setPRNG(Random prng);
}
