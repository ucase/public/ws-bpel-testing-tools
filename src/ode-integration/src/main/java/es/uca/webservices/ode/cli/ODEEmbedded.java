package es.uca.webservices.ode.cli;

import java.io.File;
import java.io.IOException;
import java.net.BindException;
import java.util.Enumeration;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleState;
import org.apache.catalina.startup.Tomcat;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.WriterAppender;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.bpelunit.CustomizedRunner;

public class ODEEmbedded {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ODEEmbedded.class);

	private String fLoggingFilterName;
	private FileAppender fAppender;

	private int tomcatPort = CustomizedRunner.DEFAULT_ENGINE_PORT;
	private Tomcat tomcat;
	private String baseDir;
	private String hostName = "localhost";

	private File odeFile;

	public ODEEmbedded(File bDir, int port, String logLevel) throws Exception {
		if (logLevel.equals("none") || logLevel.equals("NONE")) {
			this.fLoggingFilterName = "off";
		} else {
			this.fLoggingFilterName = logLevel;
		}

		if (ODEEmbedded.isPortUsed(port)) {
			throw new Exception("Port " + port + " is already under use.");
		} else {
			if (bDir == null) {
				bDir = createTempDirectory();
			}
			baseDir = bDir.getAbsolutePath();
			configureLogging();

			tomcatPort = port;
			tomcat = new Tomcat();
			tomcat.setPort(tomcatPort);
			tomcat.setBaseDir(baseDir);
			tomcat.setHostname(hostName);
			long id = System.identityHashCode(tomcat);
			tomcat.getEngine().setName(tomcat.getEngine().getName() + "-" + id);
		}
	}

	public void startTomcat() throws LifecycleException {
		tomcat.start();
		LOGGER.info(String.format(
				"Starting Apache ODE with main directory %s on port %d",
				this.baseDir, getTomcatPort()));

	}

	public void awaitTomcat() {
		if (tomcat.getServer() != null
				&& tomcat.getServer().getState() == LifecycleState.STARTED) {
			tomcat.getServer().await();
		}
	}

	public void stopTomcat() throws LifecycleException {
		LOGGER.info("Stopping Apache ODE....");
		if (tomcat.getServer() != null
				&& tomcat.getServer().getState() != LifecycleState.DESTROYED) {
			if (tomcat.getServer().getState() != LifecycleState.STOPPED) {
				tomcat.stop();
			}
			tomcat.destroy();
		}
	}

	public LifecycleState getStateTomcat() {
		return tomcat.getServer().getState();
	}

	private File createTempDirectory() throws IOException {
		final File temp = File.createTempFile("temp", Long.toString(System.nanoTime()));

		if (!temp.delete()) {
			throw new IOException("Could not delete temp file: "
					+ temp.getAbsolutePath());
		}
		if (!temp.mkdir()) {
			throw new IOException("Could not create temp directory: "
					+ temp.getAbsolutePath());
		}
		return temp;
	}

	public int getTomcatPort() {
		return tomcatPort;
	}

	public void setTomcatPort(int tomcatPort) {
		this.tomcatPort = tomcatPort;
	}

	public String getBaseDir() {
		return baseDir;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public File getPathToWAR() {
		return odeFile;
	}

	public void setPathToWAR(File pathToODE) throws Exception {
		this.odeFile = pathToODE;
		final File dirWebapps = new File(baseDir, "webapps");
		FileUtils.copyFileToDirectory(odeFile, dirWebapps);

		final File oldName = new File(dirWebapps, odeFile.getName());
		final File newName = new File(dirWebapps, "ode.war");
		oldName.renameTo(newName);

		tomcat.addWebapp("/ode", "ode");
	}

	private static boolean isPortUsed(int port) throws IOException {
		try {
			final SelectChannelConnector conn = new SelectChannelConnector();
			conn.setPort(port);
			conn.open();
			conn.close();
			return false;
		} catch (BindException ex) {
			return true;
		}
	}

	private void configureLogging() throws IOException {
		if (fAppender != null) {
			return;
		}

		org.apache.log4j.Logger rootLogger = org.apache.log4j.Logger.getRootLogger();
		fAppender = new FileAppender(
			new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %-40c [%5p] %m%n"),
			this.baseDir + File.separator + "tomcat.log", false, false, 8192);
		rootLogger.addAppender(fAppender);

		final Enumeration<?> appenderEnum = rootLogger.getAllAppenders();
		while (appenderEnum.hasMoreElements()) {
			final Object elem = appenderEnum.nextElement();
			if (elem instanceof WriterAppender) {
				final WriterAppender appender = (WriterAppender)elem;
				appender.setThreshold(Level.ERROR);
			}
		}
	}

	public String getLoggingFilterName() {
		return fLoggingFilterName;
	}

	public void setLoggingFilterName(String fLoggingFilterName) {
		this.fLoggingFilterName = fLoggingFilterName;
	}

}
