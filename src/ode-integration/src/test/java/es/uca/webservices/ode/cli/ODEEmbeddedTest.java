package es.uca.webservices.ode.cli;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.apache.catalina.LifecycleState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.bpelunit.BPELUnitXMLResult;
import es.uca.webservices.bpel.bpelunit.XMLDumperRunner;
import es.uca.webservices.bpel.ode.ODEDeploymentArchivePackager;

public class ODEEmbeddedTest {
	private static final String RES_PREFIX = "src/test/resources/LoanApprovalRPCProcess/";
	private ODEEmbedded ode;

	@Before
	public void setUp() throws Exception {
		ode = new ODEEmbedded(null, 8080, "none");
		ode.setPathToWAR(new File("target/dependency/ode-axis2-war-1.3.8.war"));
		ode.startTomcat();
	}
	
	@After
	public void tearDown() throws Exception {
		if (ode != null) {
			ode.stopTomcat();
			deleteTempDir(new File(ode.getBaseDir()));
		}
	}	

	@Test
	public void testODE() throws Exception {
		final String baseDir = ode.getBaseDir();
		final File fBaseDir = new File(baseDir);

		assertTrue("The base directory should exist", fBaseDir.exists());
		assertEquals("Tomcat should be started", LifecycleState.STARTED, ode.getStateTomcat());
		assertTrue("Apache ODE should be started", new File(baseDir, "webapps/ode").exists());
	}

	@Test
	public void testBPELUnitWithRelativePath() throws Exception {
		XMLDumperRunner xmldr = new XMLDumperRunner(new File(RES_PREFIX, "loanApprovalProcess.bpts"));
		xmldr.setEnginePort(ode.getTomcatPort());

		// Note: if the archive location is a relative path, it is interpreted
		// from the folder of the .bpts file. Normally, passing absolute paths
		// is more predictable, but it is nice to test this as well.
		xmldr.setArchiveLocation(".");
		xmldr.setLoggingFilterName(ode.getLoggingFilterName());

		assertResultsExistAndAreSuccessful(xmldr);
	}

	@Test
	public void testBPELUnitWithAbsolutePath() throws Exception {
		final File fBPTS = new File(RES_PREFIX, "loanApprovalProcess.bpts");
		final XMLDumperRunner xmldr = new XMLDumperRunner(fBPTS);
		xmldr.setEnginePort(ode.getTomcatPort());
		xmldr.setArchiveLocation(fBPTS.getParentFile().getAbsolutePath());
		xmldr.setLoggingFilterName(ode.getLoggingFilterName());

		assertResultsExistAndAreSuccessful(xmldr);
	}

	@Test
	public void testBPELUnitWithZIP() throws Exception {
		final XMLDumperRunner xmldr = new XMLDumperRunner(new File(RES_PREFIX, "loanApprovalProcess.bpts"));

		final BPELProcessDefinition bpel = new BPELProcessDefinition(new File(RES_PREFIX + "/loanApprovalProcess.bpel"));
		final ODEDeploymentArchivePackager packager = new ODEDeploymentArchivePackager(bpel);
		packager.setODEPort(ode.getTomcatPort());

		final File fZip = File.createTempFile("archive", ".zip");
		try {
			packager.packODE(fZip);

			xmldr.setEnginePort(ode.getTomcatPort());
			xmldr.setArchiveLocation(fZip.getAbsolutePath());
			xmldr.setLoggingFilterName(ode.getLoggingFilterName());

			assertResultsExistAndAreSuccessful(xmldr);
		} finally {
			fZip.delete();
		}
	}

	private void assertResultsExistAndAreSuccessful(final XMLDumperRunner xmldr) throws Exception {
		final File fResults = new File(ode.getBaseDir(), "results.xml");
		xmldr.run(fResults);
		assertTrue(fResults.exists());
		final BPELUnitXMLResult results = new BPELUnitXMLResult(fResults);
		assertEquals("PASSED", results.getGlobalStatusCode());
	}
	
	private void deleteTempDir(File fTempDir){
		File[] files = fTempDir.listFiles();
		 
		for (int i=0; i<files.length; i++) {
			if (files[i].isDirectory())
				deleteTempDir(files[i]);
			files[i].delete();
		 }
		fTempDir.delete();
	}
}
