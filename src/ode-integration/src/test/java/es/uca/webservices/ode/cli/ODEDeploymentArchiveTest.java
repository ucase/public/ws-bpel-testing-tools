package es.uca.webservices.ode.cli;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.junit.Test;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.ode.ODEDeploymentArchivePackager;
import es.uca.webservices.bpel.xml.XMLDocument;

public class ODEDeploymentArchiveTest {
	private ODEDeploymentArchivePackager odeDeploy;
	
	public ODEDeploymentArchiveTest() throws Exception{
		final BPELProcessDefinition bpel = new BPELProcessDefinition(new File("src/test/resources/LoanApprovalRPCProcess/loanApprovalProcess.bpel"));
		odeDeploy = new ODEDeploymentArchivePackager(bpel);
	}
	
	@Test
	public void testDeployXML() throws Exception{
        XMLDocument deployXML = odeDeploy.generateDeploymentDescriptor();

        assertEquals("bpel:loanApprovalProcess", deployXML.evaluateExpression("/*:deploy/*:process/@name"));
		assertEquals("true", deployXML.evaluateExpression("//*:process/*:active/text()"));
		assertEquals("false", deployXML.evaluateExpression("//*:process/*:retired/text()"));
		assertEquals("all", deployXML.evaluateExpression("//*:process/*:process-events/@generate"));
        assertNotNull(deployXML.evaluateExpression("//*:process/*:invoke[@partnerLink='approver']"));
		assertNotNull(deployXML.evaluateExpression("//*:invoke[@partnerLink='assessor']"));
		assertNotNull(deployXML.evaluateExpression("//*:invoke[@partnerLink='customer']"));
	}
}