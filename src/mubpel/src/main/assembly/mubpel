#!/bin/bash

# BPEL mutation analysis tool launch script
# (C) 2008-2015 Antonio García Domínguez

# Alternative implementation of the "-f" option for readlink(1), since
# the Mac (BSD) version does not have it. It's not a perfect replacement
# but it should work for our case. It's been taken from:
#
# http://stackoverflow.com/questions/1055671/how-can-i-get-the-behavior-of-gnus-readlink-f-on-a-mac
#
readlink_f() {
    TARGET_FILE=$1

    cd $(dirname "$TARGET_FILE")
    TARGET_FILE=$(basename "$TARGET_FILE")

    # Iterate down a (possible) chain of symlinks
    while [ -L "$TARGET_FILE" ]
    do
        TARGET_FILE=$(readlink "$TARGET_FILE")
        cd $(dirname "$TARGET_FILE")
        TARGET_FILE=$(basename "$TARGET_FILE")
    done

    # Compute the canonicalized name by finding the physical path
    # for the directory we're in and appending the target file.
    PHYS_DIR=`pwd -P`
    RESULT=$PHYS_DIR/$TARGET_FILE
    echo $RESULT
}

java_version() {
    # Sets JAVA_VERSION, JAVA_MAJOR and JAVA_MINOR from "java -version"
    # For Java 1.7.0_80, JAVA_VERSION is "1.7.0", JAVA_MAJOR is 1, and JAVA_MINOR is 7
    export JAVA_VERSION=$("$JAVA_HOME/bin/java" -version 2>&1 | sed -rn 's/.*version "([0-9.]+)_.*/\1/p')
    export JAVA_MAJOR="${JAVA_VERSION%%.*}"
    JAVA_EXCEPT_PATCH="${JAVA_VERSION%.*}"
    export JAVA_MINOR="${JAVA_EXCEPT_PATCH/*./}"
}

# By default, the .jar and the script are distributed together

MUBPEL_DIR=$(dirname $(readlink -f "$0"))
JARFILE=$MUBPEL_DIR/${project.artifactId}.jar
WARFILE=$MUBPEL_DIR/ode.war

if [ -z "$JAVA_HOME" ]; then
  echo "Please set the JAVA_HOME environment variable to the path to your JVM"
  exit 1
fi

# Add -XX:PermSize if we're on Java 7 or before
java_version
if [[ "$JAVA_MAJOR" -le 1 && "$JAVA_MINOR" -le 7 ]]; then
    EXTRA_JVM_OPTS="$EXTRA_JVM_OPTS -XX:PermSize=128m"
fi

# Uncomment to get a heap dump when we run out of memory
#EXTRA_JVM_OPTS="$EXTRA_JVM_OPTS -XX:+HeapDumpOnOutOfMemoryError"

# Run the mutation analysis tool. Extra flags:
# - avoid JDK 1.6u18 JIT bug tripped by Saxon (see
#   http://trac.osgeo.org/geonetwork/ticket/301 ), while filtering the debugging
#   messages XX:CompileCommand kindly prints to stdout instead of stderr
# - increase maximum memory size
$JAVA_HOME/bin/java \
  -XX:CompileCommand=exclude,net/sf/saxon/event/ReceivingContentHandler.startElement \
  -Djavax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom=net.sf.saxon.xpath.XPathFactoryImpl \
  -Dmubpel.ode.war="$WARFILE" \
  -Xmx512m -Xss20m $EXTRA_JVM_OPTS -jar "$JARFILE" $@ \
  | egrep --line-buffered -v "^CompilerOracle|^### Excluding compile"
