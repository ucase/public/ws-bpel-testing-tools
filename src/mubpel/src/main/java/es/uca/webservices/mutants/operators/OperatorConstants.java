package es.uca.webservices.mutants.operators;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class which aggregates all the constants for every mutation operator.
 * Normally, these refer to the way the attribute values are understood. All
 * values are taken modulus their range, to alleviate the genetic algorithm from
 * having to learn the range of each operator's understood attribute values.
 *
 * @author Antonio García Domínguez
 * @version 1.0
 */
public final class OperatorConstants {

	private OperatorConstants() {}

    /**
     * EAA: XPath 1.0 numeric operators which can be used, in the same order as
     * in the XSLT stylesheet. During execution, the existing operator will be
     * removed from the replacement candidate list, so the actual indices will
     * vary.
     */
    public static final int      EAA_ATTRIBUTE_ADD = 1, EAA_ATTRIBUTE_SUB = 2,
            EAA_ATTRIBUTE_MUL = 3, EAA_ATTRIBUTE_FDIV = 4,
            EAA_ATTRIBUTE_MOD = 5;

    /** ECN attributes: indicate what action should be taken. */
    public static final int      ECN_ATTRIBUTE_INC = 1, ECN_ATTRIBUTE_DEC = 2,
            ECN_ATTRIBUTE_REPEATLAST = 3, ECN_ATTRIBUTE_REMOVELAST = 4;

    /**
     * ERR: these attributes indicate the operator which is to replace the
     * selected operand. It is selected from a list whose elements are in the
     * following order, and from which the existing operator is removed to avoid
     * generating the same program again.
     */
    public static final int      ERR_ATTRIBUTE_LT  = 1, ERR_ATTRIBUTE_GT = 2,
            ERR_ATTRIBUTE_LE = 3, ERR_ATTRIBUTE_GE = 4, ERR_ATTRIBUTE_EQ = 5,
            ERR_ATTRIBUTE_NE = 6;

    public static final String[] OPERATOR_NAMES    = { "ISV", "EAA", "EEU",
            "ERR", "ELL", "ECC", "ECN", "EMD", "EMF", "AFP", "ASF",
            "AIS", "AIE", "AWR", "AJC", "ASI", "APM", "APA", "XMF",
            "XMC", "XMT", "XTF", "XER", "XEE", "AEL", "EIU", "EIN", "EAP", 
            "EAN", "CFA", "CDE", "CCO", "CDC" };

    /**
     * EMD: indicate whether to halve the specified duration or set it to zero.
     **/
    public static final int EMD_ATTRIBUTE_HALF = 1, EMD_ATTRIBUTE_ZERO = 2;

    /* OPERATOR MAP */

	private static Map<String, Operator> operatorMap;

	/**
	 * Returns the map from all operator names to its Operator object.
	 * @return Map from operator name to {@link Operator} object. The
	 * elements in the map can be traversed with {@link Map#keySet()}
	 * in the same order as in {@link #OPERATOR_NAMES}.
	 * 
	 * This method is thread-safe.
	 */
	public static Map<String, Operator> getOperatorMap() {
		synchronized(OperatorConstants.class) {
			if (operatorMap == null) {
				OperatorFactory opFactory = new OperatorFactory();

				// Need a LinkedHashMap to extract entries in the same order they were
				// inserted. HashMap doesn't have that property, and TreeMap is slower.
				operatorMap = new LinkedHashMap<String, Operator>();

				for (String opName : OperatorConstants.OPERATOR_NAMES) {
					operatorMap.put(opName.toLowerCase(), opFactory.newOperator(opName));
				}
			}
			return operatorMap;
		}
	}

}
