package es.uca.webservices.mutants.subcommands;

/**
 * Transition implementation for the 'comparefull' subcommand, which is now deprecated.
 *
 * @author Antonio García-Domínguez
 */
public class CompareFullSubcommand extends CompareSubcommand {

	public CompareFullSubcommand() {
		super("comparefull",
				"This subcommand is now deprecated: please use 'compare --full' instead.\n"
				+ "Use 'compare -h' for more help on 'compare'.");
		setKeepGoing(true);
	}
}
