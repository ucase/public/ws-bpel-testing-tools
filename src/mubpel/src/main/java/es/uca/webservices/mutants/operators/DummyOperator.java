package es.uca.webservices.mutants.operators;

import org.w3c.dom.Node;

/**
 * Dummy operator which reports no operands and doesn't change the source
 * WS-BPEL process definition at all. Useful as a placeholder while all
 * operators are being implemented.
 * 
 * @author Antonio García Domínguez
 * @version 1.0
 */
public class DummyOperator implements Operator {

    private final String name;

    public DummyOperator(String name) {
        this.name = name;
    }

    public Node apply(Node source, int operandIndex, int attribute)
            throws Exception {
        return source;
    }

    public String getName() {
        return name;
    }

    public int getOperandCount(Node source) throws Exception {
        return 0;
    }

    public int getMaximumDistinctAttributeValue(Node source) throws Exception {
        return 1;
    }

}
