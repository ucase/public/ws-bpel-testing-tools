package es.uca.webservices.mutants.parallel;

import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.ThreadFactory;

/**
 * Thread factory which produces ProcessPipeThread objects over a group of
 * processes.
 */
class ProcessPipeThreadFactory implements ThreadFactory {

	private ThreadGroup fThreadGroup = new ThreadGroup("processPipeTF");
	private List<List<String>> fProcesses;
	private int currentPosition = 0;
	private final OutputStream fStderr;
	private boolean daemon = false;

	/**
	 * Creates a new factory.
	 * @param stderr 
	 * 
	 * @param processes
	 *            Group of processes to be looped over to produce as many as
	 *            <code>processes.length</code> {@link ProcessPipeThread}s.
	 */
	public ProcessPipeThreadFactory(OutputStream stderr, List<List<String>> processes) {
		fStderr = stderr;
		fProcesses = processes;
	}

	@Override
	public Thread newThread(Runnable r) {
		if (currentPosition < fProcesses.size()) {
			Thread t = new ProcessPipeThread(fThreadGroup, fProcesses.get(currentPosition++), r, fStderr);
			t.setName("ProcessPipeThread-" + currentPosition);
			t.setDaemon(daemon);
			return t;
		} else {
			return null;
		}
	}

	public boolean isDaemon() {
		return daemon;
	}

	public void setDaemon(boolean daemon) {
		this.daemon = daemon;
	}
}
