package es.uca.webservices.mutants.subcommands;

import java.io.File;
import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.Map;

import es.uca.webservices.bpel.deploy.ZippedDeploymentArchive;

import org.w3c.dom.Document;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.mutants.operators.Operator;
import es.uca.webservices.mutants.operators.OperatorConstants;

/**
 * <p>
 * Implements the 'analyze' subcommand.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */
public class AnalyzeSubcommand extends AbstractSubcommand {

	private static final String USAGE = "Produces a listing of the number of operands available for each operator\n"
			+ "in a specific WS-BPEL process definition. Each operator is listed in a\n"
			+ "separate line, where its name, number of operands and maximum value for\n"
			+ "the attribute field are separated by a single space.";

	public AnalyzeSubcommand() {
		super(1, 1, "analyze", "bpel|bpr|zip", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {
		final PrintStream os = getOutputStream();
		final Map<String, Pair<Integer, Integer>> analysisResults = analyze(getNonOptionArgs()
				.get(0));
		os.print(formatResult(analysisResults));
	}

	/**
	 * Convenience version of
	 * {@link #analyze(BPELProcessDefinition, Map, Operator)}. Useful when we
	 * don't need any progress information. Collects results from all available
	 * mutation operators into a map.
	 *
	 * @param inputFile
     *            Path to the <code>.bpel</code> or <code>.bpr</code> file with the process
     *            definition to be analyzed.
     */
	public Map<String, Pair<Integer, Integer>> analyze(String inputFile) throws Exception
    {
		BPELProcessDefinition bpelProc;
        if (inputFile.toLowerCase().endsWith(".bpr") || inputFile.toLowerCase().endsWith(".zip")) {
            bpelProc = new ZippedDeploymentArchive(new File(inputFile)).getMainBPEL();
        } else {
            bpelProc = new BPELProcessDefinition(new File(inputFile));
        }

		final Map<String, Pair<Integer, Integer>> results = createResultMap();
		for (Operator op : OperatorConstants.getOperatorMap().values()) {
			analyze(bpelProc, results, op);
		}
		return results;
	}

	/**
	 * Analyzes the process definition to check in how many locations the
	 * operator can be located, and computes the range of the attribute field
	 * for that operator.
	 *
	 * @param bpelProc
	 *            BPEL process definition to be analyzed.
	 * @param results
	 *            Result map to which the result of this analysis should be
	 *            saved, as a "operator name -> (number of locations, attribute range)"
	 *            key-value pair. If <code>null</code>, an appropriate empty map
	 *            will be created.
	 * @param op
	 *            Mutation operator for which the analysis should be performed.
	 * @return Result map after adding the key-value pair with the results of
	 *         this operator.
	 */
	public Map<String, Pair<Integer, Integer>> analyze(
			BPELProcessDefinition bpelProc,
			Map<String, Pair<Integer, Integer>> results,
			Operator op)
			throws Exception {
		if (results == null) {
			results = createResultMap();
		}

		final Document bpelDoc = bpelProc.getDocument();
		final int operandCount = op.getOperandCount(bpelDoc);
		final int maxAttrValue = op.getMaximumDistinctAttributeValue(bpelDoc);
		final Pair<Integer, Integer> value = new Pair<Integer, Integer>(
				operandCount, maxAttrValue);
		results.put(op.getName(), value);
		return results;
	}

	private Map<String, Pair<Integer, Integer>> createResultMap() {
		// It is very important that we use a LinkedHashMap: this way, the keys
		// will be traversed in the same order they were inserted.
		return new LinkedHashMap<String, Pair<Integer, Integer>>();
	}

	private String formatResult(Map<String, Pair<Integer, Integer>> analyze) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, Pair<Integer, Integer>> entry : analyze
				.entrySet()) {
			final String key = entry.getKey();
			final Pair<Integer, Integer> value = entry.getValue();
			sb.append(String.format("%s %d %d", key, value.getLeft(),
					value.getRight()));
			sb.append("\n");
		}
		return sb.toString();
	}

}
