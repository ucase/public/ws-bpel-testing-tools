package es.uca.webservices.mutants.operators;

import org.w3c.dom.Node;

public interface Operator {
    /**
     * Returns the name of the operator.
     *
     * @return Name of the operator.
     */
    String getName();

    /**
     * Calculates the number of valid operands in the document.
     *
     * @param source
     *            Document where the mutation operator should be applied.
     * @return Number of valid operands to be used in the document.
     */
    int getOperandCount(Node source) throws Exception;

    /**
     * Returns M in the range [1, M] of distinct (as in producing syntactically
     * different mutants) attribute integer values for some operand in the
     * document.
     */
    int getMaximumDistinctAttributeValue(Node source) throws Exception;

    /**
     * Applies the operator to a particular operand in the source document.
     *
     * @param source
     *            Document where the mutation operator should be applied.
     * @param operandIndex
     *            Position of the selected operand among all the operands.
     *            The first operand has position 1 (not 0).
     * @param attribute
     *            Additional parameter which fully determines the behavior of
     *            the operator. Its first distinct value should also start at
     *            1, and wrap around when the maximum value is reached. Values
     *            lesser than 1 (even negative) should also wrap, but in the
     *            reverse direction.
     * @return A new DOM XML Document with the resulting mutated document.
     */
    Node apply(Node source, int operandIndex, int attribute) throws Exception;
}
