package es.uca.webservices.mutants.parallel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generic thread which keeps a certain process alive and reads and writes lines
 * from its stdout and to its stdin without buffering.
 * 
 * @author Antonio García-Domínguez
 */
class ProcessPipeThread extends Thread {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessPipeThread.class);

	private static final class CopyStreamTask implements Runnable {
		private final OutputStream stderr;
		private final Process currentProcess;

		private CopyStreamTask(OutputStream stderr, Process currentProcess) {
			this.stderr = stderr;
			this.currentProcess = currentProcess;
		}

		public void run() {
			try {
				IOUtils.copy(currentProcess.getErrorStream(), stderr);
			} catch (IOException e) {
				LOGGER.error("Could not dump stderr for a subprocess", e);
			}
		}
	}
	
	// The full separator: Windows uses \r\n, Linux \n, Mac \r
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	// Only the last character: Windows and Linux use \n, Mac uses \r
	private static final char LINE_TERMINATOR_CHAR = LINE_SEPARATOR.charAt(LINE_SEPARATOR.length() - 1);

	private final List<String> fProcessArgs;
	private final OutputStream fStderr;

	private Process fProcess;
	private InputStream fProcessStdout;
	private PrintWriter fProcessStdin;

	private Thread fStderrDumpThread;

	/**
	 * Creates a new thread.
	 * 
	 * @param group
	 *            Thread group to which this thread should be added.
	 * @param args
	 *            Command which should be run to spawn the process as required.
	 * @param r
	 *            Runnable which should be executed by this thread.
	 * @param fStderr 
	 */
	public ProcessPipeThread(ThreadGroup group, List<String> args, Runnable r, OutputStream stderr) {
		super(group, r);
		fProcessArgs = args;
		fStderr = stderr;
	}

	public void run() {
		try {
			super.run();
		} finally {
			stopProcess();
		}
	}

	/**
	 * Reads an unbuffered line from the stdout of the watched process.
	 * 
	 * @return Next line produced by the process, without the trailing newline.
	 * @throws IOException
	 *             I/O error while reading the next line.
	 */
	public String readLineUnbuffered() throws IOException {
		startSubprocessIfNeeded();

		StringBuffer bufLine = new StringBuffer();
		int c = fProcessStdout.read();
		while (c != -1 && c != LINE_TERMINATOR_CHAR) {
			bufLine.append((char) c);
			c = fProcessStdout.read();
		}

		if (c == -1 && bufLine.length() == 0) {
			// The process has crashed: restart it
			stopProcess();
			return null;
		} else {
			return bufLine.toString();
		}
	}

	/**
	 * Writes a line to the stdin of the watched process. Flushes the stream
	 * immediately.
	 * 
	 * @param line
	 *            Line to be written.
	 * @throws IOException 
	 */
	public void writeLineUnbuffered(String line) throws IOException {
		startSubprocessIfNeeded();

		fProcessStdin.write(line + LINE_SEPARATOR);
		fProcessStdin.flush();
	}

	private synchronized void startSubprocessIfNeeded() throws IOException {
		if (fProcess == null) {
			fProcess = new ProcessBuilder(fProcessArgs).start();

			fProcessStdout = fProcess.getInputStream();
			fProcessStdin = new PrintWriter(new OutputStreamWriter(fProcess.getOutputStream()));

			fStderrDumpThread = new Thread(new CopyStreamTask(fStderr, fProcess));
			fStderrDumpThread.setName("CopySubStderrToMainStderr");
			fStderrDumpThread.setDaemon(isDaemon());
			fStderrDumpThread.start();
		}
	}

	private synchronized void stopProcess() {
		if (fProcess != null) {
			final NullOutputStream nullOutput = new NullOutputStream();
	
			try {
				// Close stdin and dump stdout and stderr
				fProcess.getOutputStream().close();
				IOUtils.copy(fProcess.getInputStream(), nullOutput);
				fStderrDumpThread.join();
	
				// Wait for the process to finish, and destroy it
				fProcess.waitFor();
				fProcess.destroy();
			} catch (IOException e) {
				LOGGER.debug("I/O exception", e);
			} catch (InterruptedException e) {
				LOGGER.debug("Interrupted wait to finish process", e);
			}
			
			fProcess = null;
		}
	}
}
