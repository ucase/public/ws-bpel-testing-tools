package es.uca.webservices.mutants.subcommands;

import java.io.File;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.activebpel.DeploymentArchivePackager;
import es.uca.webservices.bpel.bpelunit.CustomizedRunner;

/**
 * Packs up a WS-BPEL composition into an ActiveBPEL Business Process aRchive (.bpr)
 * file. Engine-dependent files are generated automatically and added to the .bpr.
 *
 * @author Antonio García Domínguez
 */
public class BPRSubcommand extends AbstractSubcommand {

	private int fMockupPort = DEFAULT_MOCKUP_PORT;
	private boolean fPreserveURLs = DEFAULT_PRESERVE_URLS;
	
    private static final String MOCKUP_PORT_OPTION = "mockup-port";
    private static final String PRESERVE_SERVICE_URLS_OPTION = "preserve-urls";

    private static final int DEFAULT_MOCKUP_PORT = CustomizedRunner.DEFAULT_BPELUNIT_PORT;
    private static final boolean DEFAULT_PRESERVE_URLS = false;

	public BPRSubcommand() {
		super(2, 2, "bpr", "bpel bpr", "Packs a WS-BPEL process into an ActiveBPEL .bpr file");
	}

	@Override
	protected void runCommand() throws Exception {
		final String bpel = getNonOptionArgs().get(0);
		final String bpr = getNonOptionArgs().get(1);
		
		final BPELProcessDefinition bpelProcess = new BPELProcessDefinition(new File(bpel));
		final DeploymentArchivePackager packager = new DeploymentArchivePackager(bpelProcess);
		packager.setBPELUnitPort(getMockupPort());
		packager.setPreserveServiceURLs(isPreserveURLs());
		packager.generateBPR(bpr);
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		
		if (options.has(MOCKUP_PORT_OPTION)) {
			setMockupPort((Integer)options.valueOf(MOCKUP_PORT_OPTION));
		}
		setPreserveURLs(options.has(PRESERVE_SERVICE_URLS_OPTION));
	}

	@Override
	protected OptionParser createOptionParser() {
		final OptionParser parser = super.createOptionParser();

		parser.accepts(MOCKUP_PORT_OPTION, "Sets the port on which BPELUnit will listen")
			.withRequiredArg().ofType(Integer.class).describedAs("P")
			.defaultsTo(DEFAULT_MOCKUP_PORT);
		parser.accepts(PRESERVE_SERVICE_URLS_OPTION, "Preserves the original URLs for the services, instead of replacing them with the URL to the BPELUnit mockup");
		
		return parser;
	}

	public int getMockupPort() {
		return fMockupPort;
	}

	public void setMockupPort(int fMockupPort) {
		this.fMockupPort = fMockupPort;
	}

	public boolean isPreserveURLs() {
		return fPreserveURLs;
	}

	public void setPreserveURLs(boolean fPreserveURLs) {
		this.fPreserveURLs = fPreserveURLs;
	}
	
}
