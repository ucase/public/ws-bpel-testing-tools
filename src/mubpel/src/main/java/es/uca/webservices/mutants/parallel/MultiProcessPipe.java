package es.uca.webservices.mutants.parallel;

import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.mutants.parallel.PriorityExecutor.Important;

/**
 * <p>
 * One-to-many pipe which splits each line in its input among several processes.
 * These processes produce a line for each line of input. The resulting lines
 * are in the same relative order as the input lines.
 * </p>
 * 
 * <p>
 * It can also concurrently dump the standard error to an InputStream, so the
 * subprocesses will not block once the buffers for their standard error streams
 * fill up.
 * </p>
 *
 * <p>
 * Using this class is simple:
 * <ol>
 * <li>Create an instance with the processes already running, pointing to where
 * their standard error output should be redirected.</li>
 * <li>Write each line of the input into the pipe with
 * {@link #writeLine(String)}. The pipe does not block during writing.</li>
 * <li>Tell the pipe we have no more input lines by closing the write side with
 * {@link #doneWriting()}.</li>
 * <li>Read all the output with {@link #readLine()}. The pipe blocks while
 * reading, and will return <code>null</code> once every line has been
 * processed.</li>
 * <li>Tell the pipe we have nothing else to read with {@link #doneReading()}.</li>
 * </ol>
 * </p>
 * 
 * <p>
 * If the number of input lines is not known in advance, you should use two
 * threads: one for writing and one for reading. You will still need to call
 * {@link #doneWriting()} and {@link #doneReading()} as needed.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */
public class MultiProcessPipe {

	public static final boolean DAEMON_THREADS = true;
	public static final boolean REGULAR_THREADS = false;

	private static final int LINE_RETRY_COUNT = 5;

	private static final class ProcessLineTask implements Callable<String>, Important {
		private final String inputLine;
		private final boolean isRetry;

		private ProcessLineTask(String inputLine, boolean isRetry) {
			this.inputLine = inputLine;
			this.isRetry = isRetry;
		}

		@Override
		public String call() throws Exception {
			ProcessPipeThread thisThread = (ProcessPipeThread) Thread.currentThread();
			thisThread.writeLineUnbuffered(inputLine);
			String outputLine = thisThread.readLineUnbuffered();
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Wrote %s, read %s from thread %s", inputLine, outputLine, thisThread.getName()));
			}
			return outputLine;
		}

		@Override
		public int getPriority() {
			return isRetry ? Thread.MAX_PRIORITY : Thread.NORM_PRIORITY;
		}
	}

	private static final int QUEUE_RECHECK_INTERVAL_MILLIS = 1000;
	private static final int DEFAULT_TERMINATION_TIMEOUT = 120;

	private static final Logger LOGGER = LoggerFactory.getLogger(MultiProcessPipe.class);

	private final ThreadPoolExecutor fThreadPool;
	private final BlockingDeque<Pair<String, Future<String>>> queuedValues = new LinkedBlockingDeque<Pair<String, Future<String>>>();

	private int fTerminationTimeout = DEFAULT_TERMINATION_TIMEOUT;
	private volatile boolean fShutdown = false;

	/**
	 * Creates a new multiprocess pipe. The pipe is initially empty.
	 * 
	 * @param stderr
	 *            If not <code>null</code>, the standard error streams of all
	 *            the subprocesses will be dumped to this stream.
	 * @param processes
	 *            Started processes which will repeatedly take a single line
	 *            from their stdin and produce another single line on their
	 *            stdout.
	 * @param daemon
	 *            Whether the threads started by this pipe should be daemon
	 *            threads that do not prevent the JVM for closing (
	 *            {@link #DAEMON_THREADS}) or regular threads (
	 *            {@link #REGULAR_THREADS}).
	 */
	public MultiProcessPipe(final OutputStream stderr, List<List<String>> processes, boolean daemon) {
		final int nProcesses = processes.size();
		final ProcessPipeThreadFactory threadFactory = new ProcessPipeThreadFactory(stderr, processes);
		threadFactory.setDaemon(daemon);
		fThreadPool = new PriorityExecutor(nProcesses, threadFactory);
	}

	/**
	 * Changes the amount of number of seconds to wait for after all jobs have
	 * been completed, to ensure the orderly termination of the pipe.
	 */
	public void setTerminationTimeout(int fTerminationTimeout) {
		this.fTerminationTimeout = fTerminationTimeout;
	}

	/**
	 * Returns the seconds which this pipe should wait for after all jobs have
	 * been completed, to ensure its orderly termination.
	 */
	public int getTerminationTimeout() {
		return fTerminationTimeout;
	}

	/**
	 * Writes a new line at the end of the pipe. This method is thread-safe, and
	 * never blocks.
	 * 
	 * @param inputLine
	 *            Line to be written to one of the subprocesses of this pipe.
	 */
	public Future<String> writeLine(String inputLine) {
		synchronized (queuedValues) {
			Future<String> future = fThreadPool.submit(new ProcessLineTask(inputLine, false));
			queuedValues.add(new Pair<String, Future<String>>(inputLine, future));
			queuedValues.notifyAll();
			LOGGER.debug("Queued " + inputLine);

			return future;
		}
	}

	/**
	 * Reads the next line produced by one of the subprocesses. The lines
	 * produced by the subprocesses are read in the same order their inputs were
	 * written, as if there was only a single subprocess inside this pipe. That
	 * is, as if this were a regular pipe.
	 * 
	 * This method blocks indefinitely until there is something to read, the
	 * thread is interrupted or the pipe is closed. This method is thread-safe.
	 * 
	 * @return The next line, or <code>null</code> if the pipe was closed and no
	 *         more pending results remain.
	 * @throws InterruptedException
	 *             The wait for the next line was interrupted.
	 * @throws ExecutionException
	 *             An exception was thrown while computing the next line.
	 */
	public String readLine() throws InterruptedException, ExecutionException {
		synchronized (queuedValues) {
			while (true) {
				if (!queuedValues.isEmpty()) {
					break;
				} else if (fShutdown) {
					return null;
				} else {
					// check back in a second
					queuedValues.wait(QUEUE_RECHECK_INTERVAL_MILLIS);
				}
			}

			final Pair<String, Future<String>> head = queuedValues.take();
			final String inputLine = head.getLeft();
			String result = head.getRight().get();
			for (int i = 0; result == null && i < LINE_RETRY_COUNT; ++i) {
				result = fThreadPool.submit(new ProcessLineTask(inputLine, true)).get();
			}
			return result;
		}
	}

	/**
	 * Closes the input side of the pipe, so no more lines are accepted. The
	 * lines that are already scheduled will be processed normally.
	 */
	public void doneWriting() throws InterruptedException {
		fShutdown = true;
	}

	/**
	 * <p>
	 * Closes the output side of the pipe. Ensures all the processes are cleanly
	 * shutdown. After waiting for a maximum of {@link #getTerminationTimeout()}
	 * seconds, the standard streams of each process are closed. The pipe waits
	 * for the normal termination of each process in order to destroy them.
	 * Exceptions during cleanup are only logged.
	 * </p>
	 *
	 * <p>
	 * This method is <emph>not</emph> thread-safe.
	 * </p>
	 */
	public void doneReading() throws InterruptedException {
		// Wait until all tasks are done, with a timeout
		fThreadPool.shutdown();
		fThreadPool.awaitTermination(getTerminationTimeout(), TimeUnit.SECONDS);
	}

}
