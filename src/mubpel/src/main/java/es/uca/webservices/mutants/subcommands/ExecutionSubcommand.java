package es.uca.webservices.mutants.subcommands;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.bpelunit.CustomizedRunner;
import es.uca.webservices.mutants.parallel.MultiProcessPipe;
import es.uca.webservices.mutants.subprocesses.ProcessUtils;
import es.uca.webservices.ode.cli.ODEEmbedded;

/**
 * <p>Base abstract class for subcommands requiring execution of a WS-BPEL process.</p>
 *
 * <p>This class can launch one or more engines under a root work directory. If only one engine is required,
 * the root work directory will be used to set up ActiveBPEL. Otherwise, each of the engines will be
 * started up in a subdirectory of the root work directory. The root work directory can be set with
 * {@link #setRootWorkDirectory(java.io.File)}.</p>
 * 
 * @author Antonio García-Domínguez
 * 
 */
public abstract class ExecutionSubcommand extends AbstractSubcommand {

	public static final String ODE_WAR_PATH_PROPERTY = "mubpel.ode.war";

	private static final String ABORT_AFTER_CT_OPTION = "abort-after-client-track";

	public static final String ENGINE_TYPE_ACTIVEBPEL = "activebpel";
    public static final String ENGINE_TYPE_ODE = "ode";
	public static final String ENGINE_TYPE_NONE = "none";

    // Default value when using --parallel without the optional arg
    public static final int DEFAULT_PARALLEL_THREAD_COUNT = Runtime.getRuntime().availableProcessors();

    static final String BPEL_LOGLEVEL_OPTION = "bpel-loglevel";
    static final String BPR_DIRECTORY_OPTION = "bpr-directory";
	static final String WORKDIR_OPTION = "work-directory";
	static final String MOCKUP_PORT_OPTION = "mockup-port";
	static final String ENGINE_PORT_OPTION = "engine-port";
    static final String ENGINE_TYPE_OPTION = "engine-type";
    static final String ODE_WAR_OPTION = "ode-war";
    static final String PARALLEL_OPTION = "parallel";
    static final String DECK_SIZE_OPTION = "deck-size";
    static final String PRESERVE_SERVICE_URLS_OPTION = "preserve-urls";
    static final String TEST_TIMEOUT_OPTION = "test-timeout";

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionSubcommand.class);

    private static final String DEFAULT_ENGINE_TYPE = "ode";
    private static final boolean DEFAULT_PRESERVE_SERVICE_URLS = false;

    private int fEnginePort = CustomizedRunner.DEFAULT_ENGINE_PORT;
	private int fMockupPort = CustomizedRunner.DEFAULT_BPELUNIT_PORT;
	private int fTestTimeout = CustomizedRunner.DEFAULT_TIMEOUT;
	private String fLogLevel = CustomizedRunner.NONE_LOGLEVEL, fEngineType = DEFAULT_ENGINE_TYPE;

	private File fRootWorkDirectory, pathToOdeWar;

    // If the work directory is a temporary directory, it should be deleted on cleanup.
    // If it was manually specified by the user, it should not: maybe the user wants to
    // use it later or check some files in it.
    private boolean fCleanupWorkDirectory = true;
    

	private File fBPRDir;
	
	private ODEEmbedded odeRunner;
	private File deployDir;

	private boolean fCleanupAfterRun = true, fPreserveServiceURLs = DEFAULT_PRESERVE_SERVICE_URLS, fAbortAfterClientTrack = false;
    private int fThreadCount = 1;
    private int fDeckSize = 0;

    public ExecutionSubcommand(int minArgumentCount, int maxArgumentCount,
			String name, String usage, String description) {
		super(minArgumentCount, maxArgumentCount, name, usage, description);
		if (System.getProperty(ODE_WAR_PATH_PROPERTY) != null) {
			this.pathToOdeWar = new File(System.getProperty(ODE_WAR_PATH_PROPERTY));
		}
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);

		this.fEnginePort = (Integer) options.valueOf(ENGINE_PORT_OPTION);
        this.fEngineType = (String) options.valueOf(ENGINE_TYPE_OPTION);
		this.fMockupPort = (Integer) options.valueOf(MOCKUP_PORT_OPTION);
		this.fLogLevel = (String) options.valueOf(BPEL_LOGLEVEL_OPTION);
		this.fPreserveServiceURLs = options.has(PRESERVE_SERVICE_URLS_OPTION);
		this.fAbortAfterClientTrack = options.has(ABORT_AFTER_CT_OPTION);
		this.fTestTimeout = (Integer) options.valueOf(TEST_TIMEOUT_OPTION);

		setBPRDir((File) options.valueOf(BPR_DIRECTORY_OPTION));
        setRootWorkDirectory((File) options.valueOf(WORKDIR_OPTION));
        if (options.has(PARALLEL_OPTION)) {
            setThreadCount((Integer) options.valueOf(PARALLEL_OPTION));
        }
        if (options.has(DECK_SIZE_OPTION)) {
            setDeckSize((Integer) options.valueOf(DECK_SIZE_OPTION));
        }
        if (options.has(ODE_WAR_OPTION)) {
        	setPathToOdeWar((File) options.valueOf(ODE_WAR_OPTION));
        }
    }

	@Override
	protected OptionParser createOptionParser() {
		OptionParser parser = super.createOptionParser();

		parser.accepts(ENGINE_PORT_OPTION,
				"Sets the port on which " +
				" will listen")
				.withRequiredArg().ofType(Integer.class)
				.defaultsTo(CustomizedRunner.DEFAULT_ENGINE_PORT)
				.describedAs("port");
		parser.accepts(MOCKUP_PORT_OPTION,
				"Sets the port on which BPELUnit will listen")
				.withRequiredArg().ofType(Integer.class)
				.defaultsTo(CustomizedRunner.DEFAULT_BPELUNIT_PORT)
				.describedAs("port");
		parser.accepts(WORKDIR_OPTION,
				"Sets the work directory for ActiveBPEL (default: new temporary directory)")
				.withRequiredArg().ofType(File.class).describedAs("directory");
		parser.accepts(BPR_DIRECTORY_OPTION,
				"Sets the BPR deployment directory for ActiveBPEL")
				.withRequiredArg().ofType(File.class).describedAs("directory");
		parser.accepts(BPEL_LOGLEVEL_OPTION,
				"Sets the logging level for ActiveBPEL: 'full' or 'none'")
				.withRequiredArg().defaultsTo(CustomizedRunner.NONE_LOGLEVEL)
				.describedAs("none|full");
        parser.accepts(ENGINE_TYPE_OPTION,
                "Kind of BPEL server that should be set up: 'none' will not bring up any servers")
                .withRequiredArg().defaultsTo(DEFAULT_ENGINE_TYPE)
                .describedAs("activebpel|ode|none");
        parser.accepts(ODE_WAR_OPTION, "Path to the ode.war file (if using Apache ODE)")
        		.withRequiredArg().ofType(File.class).describedAs("ode.war");

        parser.accepts(ABORT_AFTER_CT_OPTION,
        		"Abort pending activities after the client track finishes execution (useful for some compositions but not all: disabled by default)");
        parser.accepts(PRESERVE_SERVICE_URLS_OPTION,
        		"Forces the composition to use the original WS referenced in the WSDL files, instead of the BPELUnit mockups. Service URLs are always preserved when working with .bpr files.");
        parser.accepts(TEST_TIMEOUT_OPTION,
        		"Aborts a test case after the specified number of milliseconds")
        		.withRequiredArg().ofType(Integer.class).defaultsTo(CustomizedRunner.DEFAULT_TIMEOUT)
        		.describedAs("ms");

        parser.accepts(PARALLEL_OPTION,
                "Enables parallel execution in up to N multiple BPEL engines "
                        + "(default: #CPUs + 1, cannot use '-' as fourth argument)")
                .withOptionalArg().ofType(Integer.class).describedAs("N")
                .defaultsTo(DEFAULT_PARALLEL_THREAD_COUNT);
        parser.accepts(DECK_SIZE_OPTION,
                "When used with N > 0, jobs are shuffled in a temporary buffer with N entries,"
                        + "to avoid long sequences of long-running jobs (0 disables shuffling)")
                .withOptionalArg().ofType(Integer.class).describedAs("N");

        return parser;
	}

	/**
	 * Changes the port where ActiveBPEL will listen.
	 * 
	 * @param port
	 *            Port to be used by ActiveBPEL (by default,
	 *            {@link CustomizedRunner#DEFAULT_ENGINE_PORT}).
	 */
	public void setEnginePort(int port) {
		this.fEnginePort = port;
	}

	/**
	 * Returns the port where ActiveBPEL will listen.
	 */
	public int getEnginePort() {
		return fEnginePort;
	}

	/**
	 * Changes the root work directory. If <code>null</code>,
     * the field will be initialized during execution to a new
     * temporary directory, which will be cleaned up after the
     * command is executed.
	 *
	 * @param workdir
	 *            Root work directory to be used, <code>null</code> to use a temporary
	 *            directory.
	 */
	public void setRootWorkDirectory(File workdir) {
		this.fRootWorkDirectory = workdir;
        this.fCleanupWorkDirectory = (workdir == null);
	}

	/**
	 * Returns the root work directory. If <code>null</code>,
     * the field will be initialized during execution to a new
     * temporary directory, which will be cleaned up after the
     * command is executed.
	 */
	public File getRootWorkDirectory() {
		return fRootWorkDirectory;
	}

	/**
	 * Changes the port on which the BPELUnit mockups will listen.
	 * 
	 * @param port
	 *            Port on which BPELUnit will listen (by default,
	 *            {@link CustomizedRunner#DEFAULT_BPELUNIT_PORT}).
	 */
	public void setMockupPort(int port) {
		this.fMockupPort = port;
	}

	/**
	 * Returns the port on which the BPELUnit mockups will listen.
	 */
	public Integer getMockupPort() {
		return fMockupPort;
	}

	/**
	 * Returns the number of milliseconds that BPELUnit will wait for each test case before aborting it.
	 */
	public int getTestTimeout() {
		return fTestTimeout;
	}

	/**
	 * Changes the number of milliseconds that BPELUnit will wait for each test case before aborting it.
	 */
	public void setTestTimeout(int timeout) {
		this.fTestTimeout = timeout;
	}

	/**
	 * Changes the log level used by ActiveBPEL.
	 *
	 * @param level Log level to be used: normally {@link CustomizedRunner#NONE_LOGLEVEL}
	 *              or {@link CustomizedRunner#FULL_LOGLEVEL}.
	 */
	public void setActiveBPELLogLevel(String level) {
		this.fLogLevel = level;
	}

	/**
	 * Returns the log level to be used by ActiveBPEL.
	 */
	public String getActiveBPELLogLevel() {
		return fLogLevel;
	}

    public String getEngineType() {
        return fEngineType;
    }

    public void setEngineType(String fEngineType) {
        this.fEngineType = fEngineType;
    }

	/**
	 * Returns the directory that will contain the BPR files deployed to the
	 * ActiveBPEL engine (if used), or <code>null</code> if it hasn't been initialized yet.
	 */
	public File getBPRDir() {
		return fBPRDir;
	}

	/**
	 * Sets the directory that will contain the BPR files deployed to the
	 * ActiveBPEL engine (if used).
	 */
	public void setBPRDir(File fBprDir) {
		this.fBPRDir = fBprDir;
	}
	
	protected ODEEmbedded getODERunner() throws Exception {
		if (odeRunner != null || ENGINE_TYPE_NONE.equals(fEngineType)) {
            return odeRunner;
		}

		odeRunner = new ODEEmbedded(getRootWorkDirectory(), getEnginePort(), getActiveBPELLogLevel());
		LOGGER.debug("Initializing ODE engine on port {}", getEnginePort());
		LOGGER.debug("Initializing ODE engine on dir {}", odeRunner.getBaseDir());
		setDeployDir(new File(odeRunner.getBaseDir() + "/webapps"));

		LOGGER.debug("ODE war is located at {}", getPathToOdeWar());
		if (getPathToOdeWar() != null) {
			odeRunner.setPathToWAR(getPathToOdeWar());
		} else {
			throw new Exception("The path to the ODE war has not been set");
		}
		odeRunner.startTomcat();

		fRootWorkDirectory = getDeployDir().getParentFile();

		return odeRunner;
	}

	public File getPathToOdeWar() {
		return pathToOdeWar;
	}

	public void setPathToOdeWar(File pathToOdeWar) {
		this.pathToOdeWar = pathToOdeWar;
	}

	public File getDeployDir() {
		return deployDir;
	}

	public void setDeployDir(File deployDir) {
		this.deployDir = deployDir;
	}

	/**
	 * Returns true if the ActiveBPEL engine started by this subcommand should
	 * be cleaned up after executing the next batch of compositions.
	 */
	public boolean isCleanupAfterExecution() {
		return fCleanupAfterRun;
	}

	/**
	 * If <code>cleanup</code> is <code>true</code>, the ActiveBPEL engine
	 * started by this subcommand will be cleaned up after executing the next
	 * batch of compositions.
	 */
	public void setCleanupAfterExecution(boolean cleanup) {
		this.fCleanupAfterRun = cleanup;
	}
	
	/**
	 * Returns <code>true</code> if the BPEL composition should call the
	 * partner services using the addresses in the WSDL documents, instead
	 * of forcing the composition to invoke the mockups provided by BPELUnit.
	 * Otherwise, it returns <code>false</code>.
	 */
    public boolean isPreserveServiceURLs() {
		return fPreserveServiceURLs;
	}

    /**
     * If set to <code>true</code>, the BPEL composition will call the
     * partner services using the addresses in the WSDL documents. Otherwise,
     * the composition will always invoke the mockups provided by BPELUnit
     * (which is the default behaviour).
     */
	public void setPreserveServiceURLs(boolean preserveServiceURLs) {
		this.fPreserveServiceURLs = preserveServiceURLs;
	}

	/**
	 * Returns <code>true</code> if tests should abort pending activities once
	 * all activities in the client track have completed their execution.
	 * Otherwise, returns <code>false</code>. By default, tests do not abort
	 * pending activities.
	 */
	public boolean isAbortAfterClientTrack() {
		return fAbortAfterClientTrack;
	}

	/**
	 * If set to <code>true</code>, tests will abort pending activities once all
	 * activities in the client track have completed their execution. Otherwise,
	 * they will not. By default, tests do not abort pending activities.
	 */
	public void setAbortAfterClientTrack(boolean shouldAbort) {
		this.fAbortAfterClientTrack = shouldAbort;
	}

	/**
     * Sets the maximum number of parallel ActiveBPEL instances to run. If set
     * to 1, no parallelization is performed. For a good default value, you can
     * use {@link #DEFAULT_PARALLEL_THREAD_COUNT}.
     */
    public void setThreadCount(int threads) {
        this.fThreadCount = threads;
    }

    /**
     * Returns the maximum number of parallel ActiveBPEL instances to run.
     */
    public int getThreadCount() {
        return fThreadCount;
    }

    /**
     * Changes how many jobs are internally shuffled together in each iterator
     * of the parallel comparison. If set to 0, shuffling is disabled.
     */
    public void setDeckSize(int fDeckSize) {
        this.fDeckSize = fDeckSize;
    }

    /**
     * Returns how many jobs are internally shuffled together in each iterator
     * of the parallel comparison. If 0, shuffling is disabled.
     */
    public int getDeckSize() {
        return fDeckSize;
    }

    /**
     * Stops the engine, waits for its graceful shutdown and cleans up temporary directories.
     * This method is public, as some users of the class may want to preemptively stop the
     * BPEL engine before all the test cases are run.
     */
    public void cleanup() throws Exception {
        if (odeRunner != null) {
        	odeRunner.stopTomcat();
        	odeRunner.awaitTomcat();
            odeRunner = null;
        }
        if (fCleanupWorkDirectory && fRootWorkDirectory != null && fRootWorkDirectory.exists()) {
        	try {
        		FileUtils.deleteDirectory(fRootWorkDirectory);
        	} catch (Exception e) {
        		LOGGER.error("Could not delete the temporary work directory", e);
        	}
        }
    }

    /**
     * Splits a series of lines over several subprocesses, which produce a line
     * of output for each line of input. The split tries to keep all
     * subprocesses busy as long as possible, while keeping the output lines in
     * the same relative order as the input lines.
     *
     * <emph>Note 1:</emph> either <code>stdinLines</code> or <code>in</code>
     * must be null. Both cannot be not null at the same time.
     *
     * <emph>Note 2:</emph> the class which uses this method should define its
     * own <code>main</code> method, as the subprocesses will be running in
     * forked JVMs.
     *
     * @param stdinLines
     *            List of lines to be processed.
     * @param in
     *            Stream from which the lines to be processed should be read.
     * @param subprocessArgs
     *            Basic arguments to be passed to each subprocess. In addition,
     *            each subprocess will get the appropriate options (ports,
     *            directories, etc.) for setting up a different ActiveBPEL
     *            instance.
     */
    protected void parallelProcess(List<String> stdinLines, Reader in,
            List<String> subprocessArgs) throws IOException,
            InterruptedException, ExecutionException
    {
        // Create the commands required to start the compare subprocesses
        List<List<String>> processes = createSubprocessArgs(subprocessArgs);

        // Create the multiprocess pipe
        MultiProcessPipe pipe = new MultiProcessPipe(System.err, processes, MultiProcessPipe.DAEMON_THREADS);

        if (in != null) {
            parallelProcessInOrder(new LineIterator(in), pipe);
        } else if (getDeckSize() == 0) {
            parallelProcessInOrder(stdinLines.iterator(), pipe);
        } else {
            parallelProcessDecks(stdinLines, pipe, getDeckSize());
        }
    }

    /**
     * Processes a single line of output produced from a single line of input
     * sent to one of the subprocesses.
     *
     * @param line
     *            Output line to be processed.
     */
    protected abstract void parallelProcessOutputLine(String line);

    private List<List<String>> createSubprocessArgs(List<String> baseArgs) throws IOException {
        if (getRootWorkDirectory() == null) {
            fRootWorkDirectory = es.uca.webservices.bpel.io.util.FileUtils.createTemporaryDirectory("mubpel", ".dir");
        }

        List<List<String>> processes = new ArrayList<List<String>>(fThreadCount);
        for (int i = 0; i < fThreadCount; ++i) {
            final File leafWorkDir = new File(fRootWorkDirectory, "subengine" + i);

            List<String> args = new ArrayList<String>();
            args.add("--" + BPEL_LOGLEVEL_OPTION);
            args.add(getActiveBPELLogLevel());
            args.add("--" + ENGINE_PORT_OPTION);
            args.add(Integer.toString(getEnginePort() + i));
            args.add("--" + MOCKUP_PORT_OPTION);
            args.add(Integer.toString(getMockupPort() + i));
            args.add("--" + WORKDIR_OPTION);
            args.add(leafWorkDir.getAbsolutePath());
            args.add("--" + TEST_TIMEOUT_OPTION);
            args.add(Integer.toString(getTestTimeout()));
            if (isPreserveServiceURLs()) {
            	args.add("--" + PRESERVE_SERVICE_URLS_OPTION);
            }
            if (isAbortAfterClientTrack()) {
            	args.add("--" + ABORT_AFTER_CT_OPTION);
            }
            if (getLogFile() != null) {
                args.add("--" + LOGFILE_OPTION);
                args.add(getLogFile().getCanonicalPath() + "." + i);
                args.add("--" + LOGFILE_LEVEL_OPTION);
                args.add(getLogfileLogLevel().toString());
            }
            if (getPathToOdeWar() != null) {
            	args.add("--" + ODE_WAR_OPTION);
            	args.add(getPathToOdeWar().toString());
            }
            args.addAll(baseArgs);
            args.add("-");

            processes.add(ProcessUtils.callJavaClassArgs(getClass(), args));
        }
        return processes;
    }

    private void parallelProcessDecks(List<String> pathsToOtherBPEL,
            MultiProcessPipe pipe, final int deckSize)
            throws InterruptedException, ExecutionException {

        // Divide the job list into shuffled sublists of {@link #getDeckSize()}
        // elements and run them, while preserving the expected result ordering
        int startPos = 0;
        while (startPos < pathsToOtherBPEL.size()) {
            int nextPos = Math
                    .min(startPos + deckSize, pathsToOtherBPEL.size());
            parallelProcessDeck(pathsToOtherBPEL.subList(startPos, nextPos),
                    pipe);
            startPos = nextPos;
        }
        pipe.doneWriting();
        pipe.doneReading();
    }

    private void parallelProcessDeck(List<String> sublist, MultiProcessPipe pipe)
            throws InterruptedException, ExecutionException {

        // Create shuffled positions
        final List<Integer> positions = createPositionList(sublist.size());
        Collections.shuffle(positions);

        // Write the jobs into the pipe as shuffled
        for (int position : positions) {
            pipe.writeLine(sublist.get(position));
        }

        // Read the results into the job buffer
        final List<String> bufferedLines = createFilledList(null, sublist.size());
        for (int position : positions) {
            bufferedLines.set(position, pipe.readLine());
        }

        // Print the results while preserving the original order
        for (String line : bufferedLines) {
            parallelProcessOutputLine(line);
        }
    }

    private void parallelProcessInOrder(
            Iterator<String> itPathsToOtherBPEL, MultiProcessPipe pipe)
        throws InterruptedException, ExecutionException {

        // Since we don't know how many elements we are working with, we need to
        // perform reads and writes concurrently.

        final PipeWriteTask writeTask = new PipeWriteTask(pipe, itPathsToOtherBPEL);
        Thread writeThread = new Thread(writeTask);
        writeThread.setName("CompareWrite");
        writeThread.start();

        final PipeReadTask readTask = new PipeReadTask(pipe);
        Thread readThread = new Thread(readTask);
        readThread.setName("CompareRead");
        readThread.start();

        // Wait for all jobs to be scheduled
        LOGGER.info("Waiting for all jobs to be scheduled...");
        writeThread.join();
        pipe.doneWriting();

        // Wait for the pending jobs to complete and produce their results
        LOGGER.info("All jobs scheduled: waiting for completion...");
        readThread.join();
        LOGGER.info("All jobs completed: shutting down...");
        pipe.doneReading();
        LOGGER.debug("Shutdown complete");

        // Rethrow any pending exceptions in the reading thread
        if (readTask.getException() != null) {
            throw readTask.getException();
        }
    }

    private <T> List<T> createFilledList(final T defaultVal, int nElements) {
        final List<T> bufferedLines = new ArrayList<T>();
        for (int i = 0; i < nElements; ++i) {
            bufferedLines.add(defaultVal);
        }
        return bufferedLines;
    }

    private List<Integer> createPositionList(final int nElements) {
        final List<Integer> positions = new ArrayList<Integer>();
        for (int i = 0; i < nElements; ++i) {
            positions.add(i);
        }
        return positions;
    }

    private final class PipeWriteTask implements Runnable {
        private final MultiProcessPipe pipe;
        private final Iterator<String> itPathsToOtherBPEL;

        private PipeWriteTask(MultiProcessPipe pipe,
                Iterator<String> itPathsToOtherBPEL) {
            this.pipe = pipe;
            this.itPathsToOtherBPEL = itPathsToOtherBPEL;
        }

        public void run() {
            // Write all the paths to the pipe
            while (itPathsToOtherBPEL.hasNext()) {
                pipe.writeLine(itPathsToOtherBPEL.next());
            }
        }
    }

    private final class PipeReadTask implements Runnable {
        private final MultiProcessPipe pipe;

        private ExecutionException mException;

        private PipeReadTask(MultiProcessPipe pipe) {
            this.pipe = pipe;
        }

        public void run() {
            // Read all the results and print them
            String line;
            try {
                while ((line = pipe.readLine()) != null) {
                    parallelProcessOutputLine(line);
                }
            } catch (InterruptedException e) {
                // nothing to do
            } catch (ExecutionException e) {
                // save it so it can be rethrown later
                mException = e;
            }
        }

        private ExecutionException getException() {
            return mException;
        }
    }
}
