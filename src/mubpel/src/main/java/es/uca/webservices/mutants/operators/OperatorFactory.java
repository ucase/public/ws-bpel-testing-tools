package es.uca.webservices.mutants.operators;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.util.ClassLoaderURIResolver;
import es.uca.webservices.bpel.util.XSLTStylesheet;

/**
 * Generic factory which creates instances of all existing XSLT-backed
 * operators. Saves on having to write a Java class for each one. It also
 * concentrates all the constants into one place.
 *
 * @author Antonio García Domínguez
 * @version 1.1
 */
public class OperatorFactory {

    private static final String XSLT_PREFIX = "es/uca/webservices/mutants/operators";
    private static final Logger LOGGER = LoggerFactory.getLogger(OperatorFactory.class);

    /**
     * Creates a new instance of an operator from its name. Should ABC have a
     * corresponding abc.xsl XSLT stylesheet under XSLT_PREFIX (reachable from
     * this class' class loader), a XSLT-backed operator instance will be
     * created. Otherwise, a dummy operator will be created for the time being.
     *
     * @param name
     *            Name in uppercase of the operator.
     * @return XSLT backed operator if the corresponding XSLT stylesheet exists,
     *         a dummy operator which reports no operands and doesn't modify its
     *         input otherwise.
     */
    public Operator newOperator(String name) {
        final String xsltPath = XSLT_PREFIX + "/" + name.toLowerCase() + ".xsl";
        InputStream is = OperatorFactory.class.getClassLoader().getResourceAsStream(xsltPath);

        try {
        	if (is == null) {
        		// Try again with an absolute path (for OSGi)
        		is = OperatorFactory.class.getClassLoader().getResourceAsStream("/" + xsltPath);
        	}
            if (is != null && is.read() != -1) {
            	is.close();

                LOGGER.debug("Creating XSLT backed operator for {}", name);
                return createXSLTBackedOperator(name, xsltPath);
            }
            else {
                LOGGER.debug("Could not find XSLT stylesheet: using dummy operator for {}", name);
                return new DummyOperator(name);
            }
        }
        catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            return new DummyOperator(name);
        }
    }

	protected Operator createXSLTBackedOperator(final String name, final String xsltPath) {
		// We need to make sure that the operator .xsl files are loaded with the MuBPEL classloader:
		// in Eclipse, MuBPEL and xml-utils are in separate classloaders, and passing just the path
		// will not work.
		return new XSLTBackedOperator(name, new XSLTStylesheet(xsltPath,
				new ClassLoaderURIResolver(getClass().getClassLoader())));
	}
}
