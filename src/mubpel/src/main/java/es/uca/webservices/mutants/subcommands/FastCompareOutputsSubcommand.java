package es.uca.webservices.mutants.subcommands;

import java.io.File;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.namespace.QName;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import es.uca.webservices.bpel.bpelunit.FastBPELUnitXMLResultComparator;

/**
 * <p>
 * Implements the 'fastcompareout' subcommand.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */
public class FastCompareOutputsSubcommand extends AbstractSubcommand {

	private static final String IGNORE_TAGS_OPTION = "ignore-tags";
	private static final String USAGE =
		"Alternative version of 'compare' which uses a StAX parser instead of a DOM\n" +
	    "parser to improve performance and use much les smemory. However, difference\n" +
		"filtering is limited to single tags.";

	private Set<QName> fIgnoreTags = new HashSet<QName>();

	public FastCompareOutputsSubcommand() {
		super(2, 0, "fastcompareout", "xml xml1...", USAGE);
	}

	public Set<QName> getIgnoreTags() {
		return fIgnoreTags;
	}

	public void setIgnoreTags(Set<QName> ignoreTags) {
		this.fIgnoreTags = ignoreTags;
	}

	void setIgnoreTags(final String sIgnoreTags) {
		final String[] vsIgnoreTags = sIgnoreTags.split(",");
		fIgnoreTags.clear();
		for (String sIgnoreTag : vsIgnoreTags) {
			sIgnoreTag = sIgnoreTag.trim();
			if ("".equals(sIgnoreTag)) {
				continue;
			}
			if (sIgnoreTag.startsWith("{")) {
				int closingBrace = sIgnoreTag.lastIndexOf("}");
				if (closingBrace == -1) {
					throw new IllegalArgumentException("Could not find matching }");
				}
				final String nsURI = sIgnoreTag.substring(1, closingBrace);
				final String localPart = sIgnoreTag.substring(closingBrace + 1);
				fIgnoreTags.add(new QName(nsURI, localPart));
			}
			else {
				fIgnoreTags.add(new QName(sIgnoreTag));
			}
		}
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		if (options.has(IGNORE_TAGS_OPTION)) {
			final String sIgnoreTags = (String)options.valueOf(IGNORE_TAGS_OPTION);
			setIgnoreTags(sIgnoreTags);
		}
	}

	@Override
	protected OptionParser createOptionParser() {
		final OptionParser parser = super.createOptionParser();

		parser.accepts(
				IGNORE_TAGS_OPTION,
				"Ignores the specified set of tags, separated by commas and of the form 'localPart' or '{nsURI}localPart'")
			.withRequiredArg().ofType(String.class).describedAs("tags");

		return parser;
	}

	@Override
	protected void runCommand() throws Exception {
		List<String> nonOptionArgs = getNonOptionArgs();
		final String original = nonOptionArgs.get(0);
		PrintStream os = getOutputStream();

		FastBPELUnitXMLResultComparator cmp = new FastBPELUnitXMLResultComparator();
		cmp.ignoreTags(fIgnoreTags);
		for (String other : nonOptionArgs.subList(1, nonOptionArgs.size())) {
			os.println(
				String.format("%s-%s %s", original, other, formatResult(compareOutputs(cmp, original, other))));
		}
	}

	public Boolean[] compareOutputs(
		FastBPELUnitXMLResultComparator cmp, String pathToResultA, String pathToResultB)
			throws Exception {
		final File fileA = new File(pathToResultA);
		final File fileB = new File(pathToResultB);
		return cmp.compare(fileA, fileB);
	}

	public String formatResult(Boolean[] result) {
		StringBuilder sb = new StringBuilder();
		for (int counter = 0; counter < result.length; ++counter) {
			if (counter > 0) {
				sb.append(' ');
			}
			sb.append(result[counter] ? "0" : "1");
		}
		return sb.toString();
	}
}
