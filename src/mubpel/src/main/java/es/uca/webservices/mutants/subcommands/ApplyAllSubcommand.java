package es.uca.webservices.mutants.subcommands;

import java.io.File;
import java.io.FileOutputStream;

import es.uca.webservices.bpel.deploy.ZippedDeploymentArchive;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.mutants.operators.Operator;
import es.uca.webservices.mutants.operators.OperatorConstants;

/**
 * <p>
 * Implements the 'applyall' subcommand.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */
public class ApplyAllSubcommand extends AbstractSubcommand {

	private static final String USAGE =
		"Applies each operator to all of its operands, using the full range of\n" +
		"attribute values, and saves them using filenames following the format\n" +
		"mO-P-A.bpel, where O is the operator number (1-based), P is the operand\n" +
		"number (1-based) and A is the attribute value (1-based).\n\n" +
		"Please note that this code does not know about the specific constraints of\n" +
		"each mutation operator: it may generate several times the same mutant for\n" +
		"some operators if they are not properly defined.";

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplyAllSubcommand.class);

	public ApplyAllSubcommand() {
		super(1, 1, "applyall", "bpel|bpr|zip", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {
		applyAll(getNonOptionArgs().get(0));
	}

	public void applyAll(String inputPath) throws Exception {
		final String extension = FilenameUtils.getExtension(inputPath.toLowerCase());
        final boolean isZipped = "bpr".equals(extension) || "zip".equals(extension);

        ZippedDeploymentArchive archive = null;
		BPELProcessDefinition bpelProc;
        if (isZipped) {
            archive = new ZippedDeploymentArchive(new File(inputPath));
            bpelProc = archive.getMainBPEL();
        } else {
            bpelProc = new BPELProcessDefinition(new File(inputPath));
        }

		final Document source = bpelProc.getDocument();
		int iOperator = 1;
		for (Operator op : OperatorConstants.getOperatorMap().values()) {
			final int nOperandCount = op.getOperandCount(source);
			final int nMaxAttrVal = op.getMaximumDistinctAttributeValue(source);
			for (int iOperand = 1; iOperand <= nOperandCount; ++iOperand) {
				for (int iAttr = 1; iAttr <= nMaxAttrVal; ++iAttr) {
					LOGGER.info(String.format("Generating (%s=%d,%d,%d)", op.getName(), iOperator, iOperand, iAttr));
					final Document mutatedTree = (Document) op.apply(bpelProc.getDocument(), iOperand, iAttr);
					final BPELProcessDefinition mutatedBPELProcess = new BPELProcessDefinition(mutatedTree);

                    final String mutatedPrefix = String.format("m%02d-%02d-%02d", iOperator, iOperand, iAttr);
                    if (isZipped) {
                        archive.replaceMainBPEL(mutatedBPELProcess, new File(mutatedPrefix + "." + extension));
                    } else {
                        mutatedBPELProcess.dumpToStream(new FileOutputStream(new File(mutatedPrefix + ".bpel")));
                    }
				}
			}
			++iOperator;
		}

	}

}
