package es.uca.webservices.mutants.subcommands;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;
import es.uca.webservices.bpel.bpelunit.ExecutionResults;

/**
 * <p>
 * Implements the 'run' subcommand. When running a single composition, it will
 * dump the BPELUnit XML result file to stdo When running several compositions,
 * reading them from stdin or simply being forced, it will use the file list
 * output format.
 * </p>
 *
 * <p>
 * This output format consists of a series of lines with tab-separated fields.
 * The first field is always the absolute path to the BPELUnit XML results file,
 * and the rest are the absolute paths to the BPELUnit logs whose size is below
 * the acceptable limits, if defined.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */
public class RunSubcommand extends ExecutionSubcommand {

	private static final long BYTES_PER_MB = 1000000L;
	private static final Logger LOGGER = LoggerFactory.getLogger(RunSubcommand.class);
	private static final class LogFileFilter implements FilenameFilter {
		@Override
		public boolean accept(File dir, String name) {
			return name.endsWith(".log");
		}
	}

	private static final String PROCESS_LOGS_DIRNAME = "process-logs";
	static final String LISTFORMAT_OPTION = "file-list";
	static final String MAXTRACE_OPTION = "max-trace-megs";

	static final boolean DEFAULT_LIST_FORMAT = false;
	static final int DEFAULT_TRACE_SIZE = -1;

	private boolean fListFormat = DEFAULT_LIST_FORMAT;
	private int fMaxTraceSizeMB = DEFAULT_TRACE_SIZE;
	private int fLastLogID = -1;

	private static final String USAGE =
		"When running a single composition, runs the specified WS-BPEL process\n" +
		"definition against the provided BPELUnit test suite specification, \n" +
		"building the BPR as required to produce a BPELUnit XML result document,\n" +
		"which is dumped through stdout using UTF-8.\n" +
		"\n" +
		"When running several compositions or reading them from the standard input,\n" +
		"a list of lines is produced. Each line has 1+ fields: the first field is the\n" +
		"absolute path to the BPELUnit XML results file. Next, if logging has been\n" +
		"enabled, the paths to the ActiveBPEL execution logs which are under the\n" +
		"specified maximum size are listed. After that, wall times in nanoseconds\n" +
		"required by each test case are listed.\n" +
		"\n" +
		"Normally, no maximum execution log size is set. Otherwise, files which go\n" +
		"over the maximum size are deleted and are not reported through stdout.\n" +
		"\n" +
		"In order to read the paths to the BPEL compositions from the standard input,\n" +
		"use '-' after the path of the BPTS file.\n" +
		"\n" +
		"NOTE: the filelist output format can be forced even when running a\n" +
		"single composition, using the " + LISTFORMAT_OPTION + " option.\n";

	private BPELUnitSpecification fTestSpec;
	private List<List<String>> fResults = new ArrayList<List<String>>();
	public RunSubcommand() {
		super(2, 0, "run", "bpts (bpel1...|-)", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {
        List<String> nonOptionArgs = getNonOptionArgs();
        final String pathToBPTS = nonOptionArgs.get(0);
        final String pathToFirstProcess = nonOptionArgs.get(1);
        if ("-".equals(pathToFirstProcess)) {
            run(pathToBPTS, new InputStreamReader(System.in));
        } else {
            run(pathToBPTS, nonOptionArgs.subList(1, nonOptionArgs.size()));
        }
    }

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		fListFormat = options.has(LISTFORMAT_OPTION);
		fMaxTraceSizeMB = (Integer)options.valueOf(MAXTRACE_OPTION);
	}

	@Override
	protected OptionParser createOptionParser() {
		OptionParser parser = super.createOptionParser();
		parser.accepts(LISTFORMAT_OPTION,
				"Forces filelist format even when running a single composition");
		parser.accepts(MAXTRACE_OPTION,
				"Sets the maximum size for the logs listed in filelist format: " +
				"-1 disables this filtering")
			.withRequiredArg()
			.ofType(Integer.class)
			.defaultsTo(DEFAULT_TRACE_SIZE)
			.describedAs("megabytes");

		return parser;
	}

	@Override
	protected void parallelProcessOutputLine(String line) {
		List<String> parts = Arrays.asList(line.split("\\s"));
		fResults.add(parts);
	}

	/**
	 * Convenience wrapper method for {@link #run(String, List)}.
	 */
	public void run(String pathToBPTS, String... bpelFiles) throws Exception {
		run(pathToBPTS, Arrays.asList(bpelFiles));
	}

	/**
	 * Runs one or several BPEL processes and reports the results. If running a
	 * single process and the file list format is not enforced, it will dump the
	 * UTF-8 encoded BPELUnit XML result file through stdout. Otherwise, it will use the file
	 * list format.
	 *
	 * @param pathToBPTS
	 *            BPTS file to be run.
	 * @param bprOrBPELFiles
	 *            List of paths to the BPEL files which should be tested.
	 * @throws Exception
	 *             There was a problem while starting the engine or running the
	 *             compositions.
	 */
	public void run(String pathToBPTS, List<String> bprOrBPELFiles) throws Exception {
		if (getThreadCount() > 1) {
			parallelProcess(bprOrBPELFiles, null, Arrays.asList(new String[]{pathToBPTS}));
			return;
		}

		try {
			initializeExecution(pathToBPTS);
			if (bprOrBPELFiles.size() == 1 && !fListFormat) {
				final String bprOrBPEL = bprOrBPELFiles.get(0);
				final List<String> parts = runSingleCompositionInFileListFormat(fTestSpec, bprOrBPEL);
				RunSubcommand.dumpFileToStream(new File(parts.get(0)), getOutputStream(), "UTF-8");
			} else {
				for (String bprOrBPEL : bprOrBPELFiles) {
					List<String> parts = runSingleCompositionInFileListFormat(fTestSpec, bprOrBPEL);
					printLine(" ", parts);
				}
			}
		} finally {
            if (isCleanupAfterExecution()) {
			    cleanup();
            }
		}
	}

	/**
	 * Runs one or more BPEL processes against a BPTS. The absolute paths to the
	 * BPEL processes are read from each line in <code>in</code>.
	 *
	 * @param pathToBPTS
	 *            BPTS with the tests to be run.
	 * @param in
	 *            Reader from which the lines with the absolute paths to the
	 *            BPEL processes under test will be read.
	 */
	public void run(String pathToBPTS, Reader in) throws Exception {
		if (getThreadCount() > 1) {
			parallelProcess(null, in, Arrays.asList(new String[]{pathToBPTS}));
			return;
		}

		try {
			initializeExecution(pathToBPTS);
			BufferedReader reader = new BufferedReader(in);
			String bprOrBPEL;
			while ((bprOrBPEL = reader.readLine()) != null) {
				List<String> parts = runSingleCompositionInFileListFormat(fTestSpec, bprOrBPEL);
				printLine(" ", parts);
			}
		} finally {
            if (isCleanupAfterExecution()) {
			    cleanup();
            }
		}
	}

	/**
	 * Returns a list of all the results produced up to now by this subcommand.
	 * Each element is a list with the absolute path to the BPELUnit XML results
	 * file and the absolute paths to the log files, if logging has been
	 * enabled.
	 */
	public List<List<String>> getResults() {
		return fResults;
	}

	/* UTILITY FUNCTIONS */

	private void initializeExecution(String pathToBPTS) throws Exception {
		fTestSpec = new BPELUnitSpecification(new File(pathToBPTS));

		if (ExecutionSubcommand.ENGINE_TYPE_ODE.equals(getEngineType())){
			getODERunner();
			fTestSpec.setDeploymentDirectory(getDeployDir());
		} else {
			fTestSpec.setDeploymentDirectory(getBPRDir());
		}

		fTestSpec.setEnginePort(getEnginePort());
		fTestSpec.setBPELUnitPort(getMockupPort());
		fTestSpec.setPreserveURLs(isPreserveServiceURLs());
		fTestSpec.setAbortAfterClientTrack(isAbortAfterClientTrack());
		fTestSpec.setTestTimeout(getTestTimeout());
		fTestSpec.setLoggingLevelName(getActiveBPELLogLevel());
	}

	private List<String> runSingleCompositionInFileListFormat(
			BPELUnitSpecification spec, String bprOrBPEL)
			throws Exception
	{
		
		List<String> parts = new ArrayList<String>();

		final String extension = FilenameUtils.getExtension(bprOrBPEL.toLowerCase());
		ExecutionResults runResults;
        if ("bpr".equals(extension) || "zip".equals(extension)) {
            runResults = spec.run(new File(bprOrBPEL).getCanonicalPath());
        } else {
            runResults = spec.run(new BPELProcessDefinition(new File(bprOrBPEL)));
        }

		parts.add(runResults.getTestReportFile().getCanonicalPath());

		// Get the newest logs, produced by this composition
		File logDirectory = runResults.getLogsDirectory();
		getNewLogs(logDirectory, parts);

		// Add test execution times in nanoseconds
		for (long nanos : runResults.getTestWallNanos()) {
			parts.add(Long.toString(nanos));
		}

		// Save the parts for later queries
		fResults.add(parts);

		return parts;
	}

	private void getNewLogs(File logDirectory,  List<String> parts) throws IOException {
		if (!logDirectory.exists()) {
			return;
		}

		int maxNewLogID = fLastLogID;
		File[] logFiles = logDirectory.listFiles(new LogFileFilter());
		for (File logFile : logFiles) {
			final int newLogID = getLogID(logFile);
			maxNewLogID = Math.max(maxNewLogID, newLogID);
			if (newLogID > fLastLogID) {
				if (fMaxTraceSizeMB != -1
						&& logFile.length() > BYTES_PER_MB * fMaxTraceSizeMB) {
					logFile.delete();
					continue;
				}
				parts.add(logFile.getCanonicalPath());
			}
		}
		fLastLogID = maxNewLogID;
	}

	private int getLastLogNumber(File logDirectory) {
		if (!logDirectory.exists()) {
			return 0;
		}

		int result = 0;
		File[] logFiles = logDirectory.listFiles(new LogFileFilter());
		for (File logFile : logFiles) {
			result = Math.max(getLogID(logFile), result);
		}
		return result;
	}

	private int getLogID(File logFile) {
		final String nameWithoutExt = logFile.getName().split("\\.")[0];
		int logID = -1;
		try {
			logID = Integer.parseInt(nameWithoutExt);
		} catch (NumberFormatException ex) {}
		return logID;
	}

	private void printLine(final String separator, List<String> parts) {
		final PrintStream out = getOutputStream();
		out.print(parts.get(0));
		for (String s : parts.subList(1, parts.size())) {
			out.print(separator + s);
		}
		out.println();
	}

	private File getLogDirectory(File bprDir) throws IOException {
		if (bprDir != null) {
			return new File(bprDir.getCanonicalFile().getParentFile(), PROCESS_LOGS_DIRNAME);
		} else if (System.getenv("HOME") != null) {
			return new File(new File(System.getenv("HOME"), "AeBpelEngine"), PROCESS_LOGS_DIRNAME);
		}
		return null;
	}

	private static void dumpFileToStream(File f, PrintStream os, String fileEncoding) throws IOException {
		BufferedReader is = new BufferedReader(new InputStreamReader(new FileInputStream(f), fileEncoding));

		try {
			String line = null;
			while ((line = is.readLine()) != null) {
				os.println(line);
			}
		} finally {
			is.close();
		}
	}

	/**
	 * Main entry point from the command line.
	 *
	 * @param args
	 *            Arguments received from the command line.
	 * @throws Exception
	 *             There was a problem while running the command.
	 */
	public static void main(String[] args) throws Exception {
		ExecutionSubcommand cmd = new RunSubcommand();
		cmd.parseArgs(args);
		cmd.run();
	}

	/**
	 * Changes the maximum size (in MB) of the traces which will be listed when
	 * running two or more compositions or using the {@link #LISTFORMAT_OPTION}
	 * option.
	 */
	public void setTraceSize(int fTraceSize) {
		this.fMaxTraceSizeMB = fTraceSize;
	}

	/**
	 * Returns the maximum size (in MB) of the traces which will be listed when
	 * running two or more compositions or using the {@link #LISTFORMAT_OPTION}
	 * option.
	 */
	public int getTraceSize() {
		return fMaxTraceSizeMB;
	}

	/**
	 * If <code>true</code>, the file listing format will be used even if
	 * running a single composition. This setting will be ignored when running
	 * two or more compositions: the file listing format will always be used in
	 * that case.
	 */
	public void setListFormat(boolean listFormat) {
		this.fListFormat = listFormat;
	}

	/**
	 * Returns <code>true</code> if the file listing format will be used even
	 * when running a single composition.
	 */
	public boolean isListFormat() {
		return fListFormat;
	}

	
}
