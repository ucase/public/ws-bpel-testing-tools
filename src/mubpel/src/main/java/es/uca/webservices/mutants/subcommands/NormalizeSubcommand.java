package es.uca.webservices.mutants.subcommands;

import java.io.File;

import es.uca.webservices.bpel.xml.XMLDocument;

/**
 * <p>
 * Implements the 'normalize' subcommand.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */
public class NormalizeSubcommand extends AbstractSubcommand {

	private static final String USAGE =
		"Loads any XML file (a WS-BPEL process definition, for instance) and redumps\n" +
		"it as text. It's mostly useful to 'normalize' the original WS-BPEL process\n" +
		"definition to a canonical form, so we can use a regular diff tool to view\n" +
		"changes. I recommend tkdiff, as it's very easy to use and produces good\n" +
		"colored output.";

	public NormalizeSubcommand() {
		super(1, 1, "normalize", "bpel", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {
		normalize(getNonOptionArgs().get(0));
	}

	public void normalize(String pathToXML) throws Exception {
		XMLDocument doc = new XMLDocument(new File(pathToXML));
		doc.dumpToStream(getOutputStream());
	}

}
