package es.uca.webservices.mutants.subprocesses;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Convenience methods for creating subprocesses.
 */
public final class ProcessUtils {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ProcessUtils.class);

	private ProcessUtils() {
	}

	/**
	 * Invokes the main method of the specified Java class in a nested JVM, with
	 * the same classpath as the current one.
	 * 
	 * @param javaClass
	 *            Java class to be run.
	 * @param args
	 *            Arguments to be passed through the command line.
	 * @return Resulting process after building the command to be run and
	 *         starting it.
	 * @throws IOException
	 *             There was an I/O error while spawning the subprocess.
	 */
	public static Process callJavaClass(Class<?> javaClass, List<String> args)
			throws IOException {
		List<String> cmdParts = callJavaClassArgs(javaClass, args);
		ProcessBuilder builder = new ProcessBuilder(cmdParts);
		return builder.start();
	}
	
	/**
	 * Creates the required shell command to invoke the main method of the
	 * specified Java class in a nested JVM, with the same classpath as the
	 * current one.
	 * 
	 * @param javaClass
	 *            Java class to be run.
	 * @param args
	 *            Arguments to be passed through the command line (normally
	 *            through a {@link ProcessBuilder}.
	 * @return Resulting process after building the command to be run and
	 *         starting it.
	 */
	public static List<String> callJavaClassArgs(Class<?> javaClass, List<String> args) {
		final String javaPath = System.getProperty("java.home")
				+ File.separator + "bin" + File.separator + "java";
		final String javaClasspath = System.getProperty("java.class.path");
		final String javaClassname = javaClass.getCanonicalName();

		// Print debugging information: java exec, java classpath and name of
		// the main class
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("java executable: " + javaPath);
			LOGGER.debug("current classpath is:");
			String[] javaClasspathElements = javaClasspath
					.split(File.pathSeparator);
			for (String s : javaClasspathElements) {
				LOGGER.debug(" - " + s);
			}
			LOGGER.debug("java classname: " + javaClassname);
			LOGGER.debug("args:");
			for (String s : args) {
				LOGGER.debug("  " + s);
			}
		}

		List<String> cmdParts = new ArrayList<>();
		cmdParts.addAll(Arrays.asList(javaPath, "-cp", javaClasspath,
				//"-Xdebug", "-Xrunjdwp:transport=dt_socket,address=9000,server=y",
				"-Djavax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom=net.sf.saxon.xpath.XPathFactoryImpl",
				javaClassname));
		cmdParts.addAll(args);
		return cmdParts;
	}

	public static Process callJavaClass(Class<?> javaClass, String... args) throws IOException {
		return callJavaClass(javaClass, Arrays.asList(args));
	}

	/**
	 * Convenience method for {@link #callJavaClass(Class, List)}.
	 */
	public static List<String> callJavaClassArgs(Class<?> javaClass, String... args) {
		return callJavaClassArgs(javaClass, Arrays.asList(args));
	}
}
