package es.uca.webservices.mutants.subcommands;

import java.io.PrintStream;
import java.util.Map;

import es.uca.webservices.bpel.util.Pair;

/**
 * Subcommand which counts the number of mutants that can be produced.
 *
 * @author Antonio García-Domínguez
 */
public class CountMutantsSubcommand extends AbstractSubcommand {

	private static final String USAGE =
			"Prints the number of mutants that can be produced from the original\n"
			+ "program to the standard output stream.";

	public CountMutantsSubcommand() {
		super(1, 1, "count", "bpel|bpr|zip", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {
		final PrintStream os = getOutputStream();
		final String bpel = getNonOptionArgs().get(0);
		os.print(count(bpel));
	}

	public int count(String string) throws Exception {
		final Map<String, Pair<Integer, Integer>> results = new AnalyzeSubcommand().analyze(string);

		int count = 0;
		for (Map.Entry<String, Pair<Integer, Integer>> entry : results.entrySet()) {
			final Pair<Integer, Integer> pair = entry.getValue();
			count += pair.getLeft() * pair.getRight();
		}

		return count;
	}

}
