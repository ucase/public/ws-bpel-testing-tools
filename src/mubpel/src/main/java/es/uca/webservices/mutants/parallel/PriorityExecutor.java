package es.uca.webservices.mutants.parallel;

import java.util.Comparator;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of {@link ThreadPoolExecutor} that allows specifying
 * priorities in the tasks we submit. Mostly taken from <a href=
 * "http://binkley.blogspot.com.es/2009/04/jumping-work-queue-in-executor.html"
 * >this blog post</a>.
 *
 * @author Brian Oxley, Antonio García Domínguez
 */
public class PriorityExecutor extends ThreadPoolExecutor {

	private volatile int counter = 0;

	public PriorityExecutor(final int nProcesses, final ThreadFactory threadFactory) {
		super(nProcesses, nProcesses, 20, TimeUnit.SECONDS,
				new PriorityBlockingQueue<Runnable>(10,
						new PriorityTaskComparator()), threadFactory);
	}

	@Override
	protected <T> RunnableFuture<T> newTaskFor(final Callable<T> callable) {
		if (callable instanceof Important)
			return new PriorityTask<T>(((Important) callable).getPriority(), callable);
		else
			return new PriorityTask<T>(0, callable);
	}

	@Override
	protected <T> RunnableFuture<T> newTaskFor(final Runnable runnable,
			final T value) {
		if (runnable instanceof Important)
			return new PriorityTask<T>(((Important) runnable).getPriority(), runnable, value);
		else
			return new PriorityTask<T>(0, runnable, value);
	}

	public interface Important {
		int getPriority();
	}

	private final class PriorityTask<T> extends FutureTask<T> implements Comparable<PriorityTask<T>> {
		private final int priority, id;

		public PriorityTask(final int priority, final Callable<T> tCallable) {
			super(tCallable);
			this.priority = priority;
			this.id = ++counter;
		}

		public PriorityTask(final int priority, final Runnable runnable,
				final T result) {
			super(runnable, result);
			this.priority = priority;
			this.id = ++counter;
		}

		@Override
		public int compareTo(final PriorityTask<T> o) {
			return priority > o.priority ? -1 : (priority < o.priority ? 1 : (id < o.id ? -1 : (id > o.id ? 1 : 0)));
		}
	}

	private static class PriorityTaskComparator implements Comparator<Runnable> {
		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public int compare(final Runnable left, final Runnable right) {
			return ((PriorityTask) left).compareTo((PriorityTask) right);
		}
	}
}
