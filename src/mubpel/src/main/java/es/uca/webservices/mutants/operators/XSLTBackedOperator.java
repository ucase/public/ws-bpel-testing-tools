package es.uca.webservices.mutants.operators;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import es.uca.webservices.bpel.util.XSLTStylesheet;

public class XSLTBackedOperator implements Operator {

    private static final Logger LOGGER = LoggerFactory.getLogger(XSLTBackedOperator.class);
    private final String         name;
    private final XSLTStylesheet xsltStylesheet;

    public XSLTBackedOperator(String name, XSLTStylesheet xsltStylesheet) {
        this.name = name;
        this.xsltStylesheet = xsltStylesheet;
    }

    public Node apply(Node source, int operandIndex, int attribute)
            throws ParserConfigurationException, TransformerException,
            IOException {
        return runTransformation(source, "apply", operandIndex, attribute);
    }

    public int getOperandCount(Node source)
            throws ParserConfigurationException, TransformerException,
            IOException {
        final int count = transformIntoInteger(source, "count");
        LOGGER.debug("XSLT operator " + name + " found " + count + " operands");
        return count;
    }

    public String getName() {
        return name;
    }

    public int getMaximumDistinctAttributeValue(Node source) throws Exception {
        return transformIntoInteger(source, "maximumAttributeValue");
    }

    public XSLTStylesheet getXSLTStylesheet() {
      return xsltStylesheet;
    }

    private Node runTransformation(Node source, String action,
            Integer operandIndex, Integer attribute)
            throws ParserConfigurationException, TransformerException,
            IOException {

        DOMResult result = createEmptyResult();

        xsltStylesheet.setParameter("action", action);
        if (operandIndex != null) {
            xsltStylesheet.setParameter("operandIndex", operandIndex.intValue());
        }
        if (attribute != null) {
        	xsltStylesheet.setParameter("attribute", attribute.intValue());
        }

        // Use the classloader of this class to locate XSLT extension functions
        final ClassLoader oldLoader = Thread.currentThread().getContextClassLoader();
        try {
        	Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
        	xsltStylesheet.transform(new DOMSource(source), result);
        	return result.getNode();
        } finally {
        	Thread.currentThread().setContextClassLoader(oldLoader);
        }
    }

    private DOMResult createEmptyResult() throws ParserConfigurationException {
        DocumentBuilderFactory docBF = DocumentBuilderFactory.newInstance();
        docBF.setNamespaceAware(true);
        Document docResult = docBF.newDocumentBuilder().newDocument();
        return new DOMResult(docResult);
    }

    private int transformIntoInteger(Node source, final String mode)
            throws ParserConfigurationException, TransformerException, IOException {
        Node result = runTransformation(source, mode, null, null);

        assert result != null;
        assert result.getFirstChild() != null;
        Node countNode = result.getFirstChild().getFirstChild();
        assert countNode != null;
        assert countNode.getNodeType() == Node.TEXT_NODE;

        try {
            return Integer.parseInt(countNode.getNodeValue());
        }
        catch (NumberFormatException e) {
            return 0;
        }
    }
}
