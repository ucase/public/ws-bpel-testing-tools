package es.uca.webservices.mutants;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.mutants.subcommands.AnalyzeSubcommand;
import es.uca.webservices.mutants.subcommands.ApplyAllSubcommand;
import es.uca.webservices.mutants.subcommands.ApplySubcommand;
import es.uca.webservices.mutants.subcommands.BPRSubcommand;
import es.uca.webservices.mutants.subcommands.CompareFullSubcommand;
import es.uca.webservices.mutants.subcommands.CompareOutputsSubcommand;
import es.uca.webservices.mutants.subcommands.CompareSubcommand;
import es.uca.webservices.mutants.subcommands.CountMutantsSubcommand;
import es.uca.webservices.mutants.subcommands.FastCompareOutputsSubcommand;
import es.uca.webservices.mutants.subcommands.ISubcommand;
import es.uca.webservices.mutants.subcommands.NormalizeSubcommand;
import es.uca.webservices.mutants.subcommands.RunSubcommand;

/**
 * Command line runner for the mutation tool.
 * 
 * @author Antonio García Domínguez
 */
public final class CLIRunner {

	private static final int EXITCODE_EXECERROR = 4;
	private static final int EXITCODE_PARSEERROR = 3;
	private static final int EXITCODE_UNKNOWNSUBCMD = 2;
	private static final int EXITCODE_NOSUBCMD = 1;
	private static final Logger LOGGER = LoggerFactory.getLogger(CLIRunner.class);
	private static Properties properties;

	private CLIRunner() {}

	/**
	 * <p>
	 * Main method which selects the appropriate subcommand and runs it.
	 * </p>
	 * 
	 * <p>
	 * Errors and warnings are reported through the standard error output
	 * stream.
	 * </p>
	 * 
	 * @param args
	 *            Command-line arguments.
	 * @throws Exception
	 *             There was a problem while running the subcommand.
	 */
	public static void main(String[] args) {
		Map<String, ISubcommand> subcmdMap = getSubcommandMap();
		if (args.length < 1) {
			printUsage(subcmdMap);
			System.exit(EXITCODE_NOSUBCMD);
		}

		String subcmdName = args[0];
		if (!subcmdMap.containsKey(subcmdName)) {
			LOGGER.error("Unknown subcommand: " + subcmdName);
			printUsage(subcmdMap);
			System.exit(EXITCODE_UNKNOWNSUBCMD);
		}

		ISubcommand subcmd = subcmdMap.get(subcmdName);
		try {
			subcmd.parseArgs(
					Arrays.asList(args).subList(1, args.length)
						.toArray(new String[args.length-1]));
		} catch (IllegalArgumentException ex) {
			LOGGER.error("Error during argument parsing", ex);
			printUsage(subcmdMap);
			System.exit(EXITCODE_PARSEERROR);
		}

		try {
			subcmd.run();
		} catch (Exception e) {
			LOGGER.error("Error during command execution", e);
			System.exit(EXITCODE_EXECERROR);
		}
	}

	private static void printUsage(Map<String, ISubcommand> subcmdMap) {
		System.err.println("mubpel v" + getVersion() + "\n");
		System.err.println("Available subcommands:");
		for (ISubcommand subcmd : subcmdMap.values()) {
			System.err.println(" * " + subcmd.getName() + " " + subcmd.getUsage());
		}
		System.err.println("\nFor more help about a subcommand, use 'mubpel subcmd -h'");
	}

	private static String getVersion() {
		if (properties == null) {
			properties = new Properties();
			try {
				properties.load(CLIRunner.class.getResourceAsStream("/mubpel.properties"));
			} catch (IOException e) {
				LOGGER.error("Could not load mubpel.properties", e);
			}
		}
		return properties.getProperty("mubpel.version");
	}

	private static Map<String, ISubcommand> getSubcommandMap() {
		final ISubcommand[] subcommands = new ISubcommand[] {
				new AnalyzeSubcommand(), new ApplySubcommand(),
				new ApplyAllSubcommand(), new BPRSubcommand(),
				new CompareSubcommand(), new CompareFullSubcommand(),
				new CompareOutputsSubcommand(), new FastCompareOutputsSubcommand(),
				new CountMutantsSubcommand(),
				new NormalizeSubcommand(), new RunSubcommand(), };

		final Map<String, ISubcommand> commandMap = new LinkedHashMap<String, ISubcommand>();
		for (ISubcommand subcmd : subcommands) {
			commandMap.put(subcmd.getName(), subcmd);
		}
		return commandMap;
	}

}
