package es.uca.webservices.mutants.subcommands;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Document;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.deploy.ZippedDeploymentArchive;
import es.uca.webservices.mutants.operators.Operator;
import es.uca.webservices.mutants.operators.OperatorConstants;

/**
 * <p>
 * Implements the 'apply' subcommand.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */
public class ApplySubcommand extends AbstractSubcommand {

	private static final int APPLY_NUM_ARGS = 4;
	private static final String USAGE =
		"Applies the specified operator (1-based index or case-insensitive name) on\n" +
		"its n-th operand (1-based), using the extra attribute (1-based) to customize\n" +
		"its behaviour. The resulting WS-BPEL process definition is dumped to stdout.\n" +
		"If the selected operand does not exist, a warning will be reported to the\n" +
		"standard error output stream.";

	public ApplySubcommand() {
		super(APPLY_NUM_ARGS, APPLY_NUM_ARGS, "apply", "bpel|bpr|zip operator operandIndex attribute", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {
		final List<String> nonOptionArgs = getNonOptionArgs();
        final String inputFile = nonOptionArgs.get(0);
		final String extension = FilenameUtils.getExtension(inputFile.toLowerCase());
        final boolean isZipped = "bpr".equals(extension) || "zip".equals(extension);

        ZippedDeploymentArchive archive = null;
        BPELProcessDefinition inputDef;
        if (isZipped) {
            archive = new ZippedDeploymentArchive(new File(inputFile));
            inputDef = archive.getMainBPEL();
        } else {
            inputDef = new BPELProcessDefinition(new File(inputFile));
        }

        final BPELProcessDefinition def = apply(inputDef,
                nonOptionArgs.get(1).toLowerCase(),
                Integer.parseInt(nonOptionArgs.get(APPLY_NUM_ARGS - 2)),
                Integer.parseInt(nonOptionArgs.get(APPLY_NUM_ARGS - 1)));

        if (isZipped) {
            assert archive != null : "The BPR file should have been initialized at this point";

            final File fMutatedBPR = File.createTempFile("mutated", "." + extension);
            archive.replaceMainBPEL(def, fMutatedBPR);
            getOutputStream().print(fMutatedBPR.getAbsolutePath());
        } else {
            def.dumpToStream(getOutputStream());
        }
    }

    public BPELProcessDefinition apply(final BPELProcessDefinition originalDef, String operatorID,
                                       int operandIndex, int attribute) throws Exception {
        try {
			// First, try to interpret it as a number
			int operatorIndex = Integer.parseInt(operatorID);
			return apply(originalDef, operatorIndex, operandIndex, attribute);
		} catch (NumberFormatException ex) {
			// If that does not work, take it as the name of the operator
			if (OperatorConstants.getOperatorMap().containsKey(operatorID)) {
				Operator op = OperatorConstants.getOperatorMap().get(operatorID);
				return apply(originalDef, op, operandIndex, attribute);
			} else {
				throw new IllegalArgumentException(String.format(
						"Operator does not exist: %s", operatorID), ex);
			}
		}
	}

    public BPELProcessDefinition apply(final BPELProcessDefinition originalDef,
                                       int operatorIndex, int operandIndex, int attribute)
            throws Exception {
        final Map<String, Operator> operatorMap
			= OperatorConstants.getOperatorMap();

		if (operatorIndex >= 1 && operatorIndex <= operatorMap.size()) {
			// NOTE: operator indexes are 1-based, as provided from the CLI
			Operator op = getValueAt(operatorMap, operatorIndex - 1);
			return apply(originalDef, op, operandIndex, attribute);
		} else {
			throw new IllegalArgumentException(String.format(
					"Operator does not exist: valid range is [1,%d]",
					operatorMap.size()));
		}
	}

    public BPELProcessDefinition apply(BPELProcessDefinition originalBPELProcess, Operator op,
                                       int operandIndex, int attribute) throws Exception {
        Document mutatedTree = (Document) op.apply(
                originalBPELProcess.getDocument(), operandIndex, attribute);
        return new BPELProcessDefinition(mutatedTree);
    }

	/**
	 * Gets the n-th value in a map. This is only useful is the map preserves
	 * the order in which the elements were inserted, such as a LinkedHashMap.
	 */
	private static <X, Y> Y getValueAt(Map<X, Y> map, int operatorIndex) {
		Iterator<X> it = map.keySet().iterator();
		while (operatorIndex > 0) {
			it.next();
			--operatorIndex;
		}
		return map.get(it.next());
	}

}
