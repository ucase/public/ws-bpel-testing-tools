package es.uca.webservices.mutants.subcommands;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import net.bpelunit.framework.exception.ConfigurationException;
import net.bpelunit.framework.exception.DeploymentException;
import net.bpelunit.framework.exception.SpecificationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.bpelunit.AbortOnFirstDifferenceListener.AbortOnDiff;
import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;
import es.uca.webservices.bpel.bpelunit.BPELUnitXMLResult;
import es.uca.webservices.bpel.bpelunit.ExecutionResults;
import es.uca.webservices.bpel.ode.ODEDeploymentArchivePackager;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.bpel.xml.XMLDocument;

/**
 * Implements the 'compare' subcommand.
 * 
 * @author Antonio García-Domínguez
 */
public class CompareSubcommand extends ExecutionSubcommand {
	static final String TIMES_SEPARATOR = "T";

	private static final int COMPARE_NARGS_MIN = 4;
	private static final Logger LOGGER = LoggerFactory.getLogger(CompareSubcommand.class);

	static final String KEEP_GOING_OPTION = "keep-going";
	static final String COLUMN_HEADERS_OPTION = "column-headers";
	static final String IGNORE_XPATH_OPTION = "ignore-xpath";
    static final String IGNORE_XPATH_HELP =
    		"XPath expression for nodes which should be ignored in the comparison. "
    		+ "Please keep in mind that the namespace context used for the expression "
    		+ "only includes common prefixes such as bpel, wsdl, soap, soapenv or xsd. "
    		+ "You will need to use something like //element()[local-name(.)='x' and namespace-uri(.)='http://long/uri'] "
    		+ "if you'd like to remove 'x' elements in the 'http://long/uri' namespace.";

    static final String BPEL_FILE_COL_HEADER = "bpel_file";

	private static final String USAGE = "Compares the results obtained through running a series of WS-BPEL process\n"
			+ "definitions against the specified BPELUnit test specification with those\n"
			+ "produced by running the original composition (stored in a separate BPELUnit\n"
			+ "XML result file).\n"
			+ "\n"
			+ "For each process definition, a row with as many columns as test cases is\n"
			+ "produced. If the process definition could not be deployed, all cells are set\n"
			+ "to 2. Otherwise, all cells are set to 0, except for that of the first case\n"
			+ "which produced a different output from that in the original composition,\n"
			+ "which is set to 1. The test suite is aborted as soon as the first test case\n"
			+ "which produces a different output is found.\n"
			+ "\n"
			+ "If the fourth argument is -, filenames are read from standard input.\n"
			+ "\n"
			+ "Normally, it stops at the first test case with a different output from the\n"
			+ "original. If you want it to keep going, use --keep-going (-k).\n"
			+ "\n"
			+ "'--parallel' allows the user to run several compositions at the same time.\n"
			+ "When using --parallel, '--engine-port E' and '--mockup-port M' indicate the\n"
			+ "starting port for the engines and BPELUnit instances: the first subprocess\n"
			+ "will use E and M, the next one E+1 and M+1, and so on."
			+ "\n"
			+ "'--deck-size N' can be used to shuffle each N (NCPUs * 8 if N is not given)\n"
			+ "jobs, in order to avoid contiguous sequences of intensive jobs. Results will\n"
			+ "be printed in the same order as usual."
            + "\n"
            + ".bpel files will be converted into ActiveBPEL .bpr files. However, .bpr and\n"
            + ".zip files will be kept as is. Mixing different file types into the same\n"
            + "invocation may not work.\n"
            + "\n"
            + "If the file with the original results is modified while the command is running,\n"
            + "the command will try to reload it automatically."
            ;

	private BPELUnitSpecification fTestSpec;

	/**
	 * In-memory representation of the results from the original composition.
	 * This field is lazily initialized: use {@link #getOriginalResult()} to
	 * access it.
	 */
	private BPELUnitXMLResult fOriginalResult;
    private long lastOriginalResultLoadedTime;

	private boolean fKeepGoing = false;
	private List<Pair<String, Pair<int[], long[]>>> fResults
            = Collections.synchronizedList(new ArrayList<Pair<String, Pair<int[], long[]>>>());

	private File fOriginalResultFile;
	private boolean bPrintColumnHeaders = false;

	private String fIgnoreXPath;

	public CompareSubcommand() {
		this("compare", USAGE);
	}

	protected CompareSubcommand(String name, String usage) {
		super(COMPARE_NARGS_MIN, AbstractSubcommand.NARGS_UNLIMITED, name, "bpts bpel xml (bpel1...|-) | bpts bpr xml (bpr1...|-) | bpts zip xml (zip1...|-)", usage);
	}

	/**
	 * Sets whether to keep going after the first difference (<code>true</code>)
	 * or not (<code>false</code>).
	 */
	public void setKeepGoing(boolean fKeepGoing) {
		this.fKeepGoing = fKeepGoing;
	}

	/**
	 * Returns whether to keep going after the first difference (
	 * <code>true</code>) or not (<code>false</code>).
	 */
	public boolean isKeepGoing() {
		return fKeepGoing;
	}

	/**
	 * Returns if a line with the names of each column in MuBPEL's output
	 * should be printed before the first result (<code>true</code>) or not.
	 */
	public boolean isPrintColumnHeaders() {
		return bPrintColumnHeaders;
	}

	/**
	 * Sets if a line with the names of each column in MuBPEL's output should be
	 * printed before the first result (<code>true</code>) or not.
	 */
	public void setPrintColumnHeaders(boolean newValue) {
		this.bPrintColumnHeaders = newValue;
	}


	/**
	 * Returns the XPath expression that will be used to remove nodes that
	 * should be ignored for the sake of comparing the output of the mutants
	 * against that of the original mutant, if any. If no such expression has
	 * been set, returns <code>null</code>.
	 *
	 * @see #setIgnoreXPath(String)
	 */
	public String getIgnoreXPath() {
		return fIgnoreXPath;
	}

	/**
	 * Changes the XPath expression used to remove nodes that should be ignored.
	 * If <code>null</code>, disables this behavior.
	 * @see #getIgnoreXPath()
	 */
	public void setIgnoreXPath(String fIgnoreXPath) {
		this.fIgnoreXPath = fIgnoreXPath;
	}

	/**
	 * Convenience method for {@link #compare(String, String, String, List)}.
	 */
	public void compare(final String pathToBPTS,
			final String pathToOriginalBPEL, final String pathToOriginalResult,
			String... pathsToOtherBPELs) throws Exception {
		compare(pathToBPTS, pathToOriginalBPEL, pathToOriginalResult, Arrays
				.asList(pathsToOtherBPELs));
	}

	/**
	 * Compares the test results of a composition with several others and prints
	 * them to the stream returned by {@link #getOutputStream()} (normally
	 * <code>System.out</code>). The produced results can also be collected from
	 * {@link #getResults()}. If {@link ExecutionSubcommand#getThreadCount()}
	 * returns a value greater than 1, it will perform the comparisons in
	 * parallel.
	 * 
	 * @param pathToBPTS
	 *            Path to the BPELUnit test specification file.
	 * @param pathToOriginal
	 *            Path to the original BPEL composition.
	 * @param pathToOriginalResult
	 *            Path to the original BPEL composition test results.
	 * @param pathsToOtherBPELs
	 *            Paths to the other BPEL compositions.
	 */
	public void compare(final String pathToBPTS,
			final String pathToOriginal, final String pathToOriginalResult,
			List<String> pathsToOtherBPELs) throws Exception
	{
		initializeTestSpec(pathToBPTS);
		if (isPrintColumnHeaders()) {
			printColumnHeaders();
		}

		if (getThreadCount() > 1) {
			parallelCompare(pathToBPTS, pathToOriginal,
					pathToOriginalResult, pathsToOtherBPELs);
			return;
		}

		try {
            initializeComparison(pathToOriginalResult);

            BPELProcessDefinition originalDef = null;
            if (pathToOriginal.toLowerCase().endsWith(".bpel")) {
                originalDef = new BPELProcessDefinition(new File(pathToOriginal));
            }
			for (String pathToOther : pathsToOtherBPELs) {
				printResults(pathToOther, compare(originalDef, pathToOther));
			}
		} finally {
			if (isCleanupAfterExecution()) {
				cleanup();
			}
		}
	}

	/**
	 * Compares the test results of a composition with several others and prints
	 * them to the stream returned by {@link #getOutputStream()} (normally
	 * <code>System.out</code>). The produced results can also be collected from
	 * {@link #getResults()}. If {@link ExecutionSubcommand#getThreadCount()}
	 * returns a value greater than 1, it will perform the comparisons in
	 * parallel.
	 * 
	 * @param pathToBPTS
	 *            Path to the BPELUnit test specification file.
	 * @param pathToOriginal
	 *            Path to the original BPEL composition.
	 * @param pathToOriginalResult
	 *            Path to the original BPEL composition test results.
	 * @param in
	 *            Reader from which the paths to the other BPEL compositions
	 *            should be read.
	 */
    public void compare(final String pathToBPTS, final String pathToOriginal, final String pathToOriginalResult,
                        Reader in) throws Exception {
		initializeTestSpec(pathToBPTS);
		if (isPrintColumnHeaders()) {
			printColumnHeaders();
		}

		if (getThreadCount() > 1) {
			parallelCompare(pathToBPTS, pathToOriginal, pathToOriginalResult, in);
			return;
		}

		try {
            BPELProcessDefinition originalDef = null;
            if (pathToOriginal.toLowerCase().endsWith(".bpel")) {
			    originalDef = new BPELProcessDefinition(new File(pathToOriginal));
            }

			initializeComparison(pathToOriginalResult);
			BufferedReader reader = new BufferedReader(in);
			String line;
			while ((line = reader.readLine()) != null) {
				printResults(line, compare(originalDef, line));
			}
			reader.close();
		} finally {
			if (isCleanupAfterExecution()) {
				cleanup();
			}
		}
	}

	/**
	 * Returns the names of all the test cases to be run in the specified
	 * BPTS. The BPTS is cached in case it is reused in a later call of
	 * {@link #compare}.
	 */
	public List<String> getTestCaseNames(String pathToBPTS) throws Exception {
		initializeTestSpec(pathToBPTS);
		return fTestSpec.getTestCaseNames();
	}

	/**
	 * <p>
	 * Returns all the comparison results collected since this instance was
	 * created. The map is thread-safe for single operations. Anything longer
	 * than a single method call (or a call to the usual {@link #wait()} or
	 * {@link #notifyAll()} methods) should be wrapped in a synchronized block.
	 * For example:
	 * </p>
	 *
	 * <pre>
	 * List&lt;Pair&lt;String, int[]&gt;&gt; l = cmd.getResults();
	 * synchronized (l) {
	 * 	// your operations...
	 * }
	 * </pre>
	 *
	 * <p>Entries in the map are traversed in the same order that the results
	 * were produced.</p>
	 */
	public List<Pair<String, Pair<int[], long[]>>> getResults() {
		return fResults;
	}

	/**
	 * Returns the latest results produced by the original version of the composition.
	 */
	public BPELUnitXMLResult getOriginalResult()
		throws ParserConfigurationException, SAXException, IOException {
		if (fOriginalResult == null) {
            // First time we load it: there should not be any problems
            fOriginalResult = new BPELUnitXMLResult(fOriginalResultFile);
            lastOriginalResultLoadedTime = fOriginalResultFile.lastModified();
        }
        else if (lastOriginalResultLoadedTime < fOriginalResultFile.lastModified()) {
            try {
                // The file may not be fully written after the first time when we check it, so
                // if there's some problem we'll simply reuse the original results and then
                // try again on the next call.
                fOriginalResult = new BPELUnitXMLResult(fOriginalResultFile);
                lastOriginalResultLoadedTime = fOriginalResultFile.lastModified();
                LOGGER.info("Reloaded the results from the original program in {}", fOriginalResultFile.getPath());
            } catch (Exception ex) {
                LOGGER.warn("Could not reload the results from the original program: using the previous ones", ex);
            }
        }

        return fOriginalResult;
	}

	/* PROTECTED METHODS */

	@Override
	protected void parallelProcessOutputLine(String line) {
		LOGGER.debug("Processing line " + line);
		getOutputStream().println(line);
	
		try {
			// Each line has: .bpel + comparisons (1 per test) + separator + times (1 per test).
			// Therefore, in order to be valid, it should have 2 or more entries when split by spaces.
			// Unwanted lines may be mixed in with the rest of the output due to the JVM or some
			// logging framework.
			String[] lineParts = line.split(" ");
			if (lineParts.length < 2) {
				return;
			}
			final int nTests = (lineParts.length - 2) >> 1;
			final String bpel = lineParts[0];
	
			int[] comparisons = new int[nTests];
			for (int i = 0; i < comparisons.length; ++i) {
				comparisons[i] = Integer.valueOf(lineParts[1 + i]);
			}
	
			long[] nanos = new long[nTests];
			for (int i = 0; i < nanos.length; ++i) {
				nanos[i] = Long.valueOf(lineParts[1 + nTests + 1]);
			}
	
			addResult(bpel, comparisons, nanos);
		} catch (NumberFormatException ex) {
			// bad line: ignore
			LOGGER.error("Bad line: " + line + ", ignoring", ex);
		}
	}

	@Override
	protected void runCommand() throws Exception {
		List<String> args = this.getNonOptionArgs();

		final String pathToBPTS = args.get(0);
		final String pathToOriginalBPEL = args.get(1);
		final String pathToOriginalResult = args.get(2);
		final String firstBPEL = args.get(COMPARE_NARGS_MIN-1);
		List<String> pathsToOtherBPEL = args.subList(COMPARE_NARGS_MIN-1, args.size());

        if (getThreadCount() > 1) {
            if ("-".equals(firstBPEL)) {
                parallelCompare(pathToBPTS, pathToOriginalBPEL,
                        pathToOriginalResult, new InputStreamReader(System.in));
            } else {
                parallelCompare(pathToBPTS, pathToOriginalBPEL,
                        pathToOriginalResult, pathsToOtherBPEL);
            }
        } else if ("-".equals(firstBPEL)) {
            compare(pathToBPTS, pathToOriginalBPEL, pathToOriginalResult,
                    new InputStreamReader(System.in));
        } else {
            compare(pathToBPTS, pathToOriginalBPEL, pathToOriginalResult,
                    pathsToOtherBPEL);
        }
    }

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		if (options.has(KEEP_GOING_OPTION)) {
			// Users can only enable 'keep going' from the CLI:
			// they cannot disable it.
			setKeepGoing(true);
		}
		if (options.has(COLUMN_HEADERS_OPTION)) {
			setPrintColumnHeaders(true);
		}
		if (options.has(IGNORE_XPATH_OPTION)) {
			setIgnoreXPath((String)options.valueOf(IGNORE_XPATH_OPTION));
		}
	}

	@Override
	protected OptionParser createOptionParser() {
		OptionParser parser = super.createOptionParser();
		parser.accepts(KEEP_GOING_OPTION,
			"Continues execution after the first test case with different output");
		parser.accepts(COLUMN_HEADERS_OPTION,
			"Prints a line with the names of each column in the output");
		parser.accepts(IGNORE_XPATH_OPTION,
			IGNORE_XPATH_HELP)
			.withRequiredArg().ofType(String.class).describedAs("xpath");
		return parser;
	}

	/* ---- COMPARISON INIT/CLEANUP ---- */

	private void initializeComparison(final String pathToOriginalBPELResult) throws Exception {
		if (ExecutionSubcommand.ENGINE_TYPE_ODE.equals(getEngineType())){
			getODERunner();
			fTestSpec.setDeploymentDirectory(getDeployDir());
		}else{
			fTestSpec.setDeploymentDirectory(getBPRDir());
		}
		fOriginalResultFile = new File(pathToOriginalBPELResult);
	}

	private synchronized void initializeTestSpec(final String pathToBPTS)
			throws ParserConfigurationException, SAXException, IOException
	{
		if (fTestSpec == null || !fTestSpec.getFile().equals(new File(pathToBPTS))) {
			fTestSpec = new BPELUnitSpecification(new File(pathToBPTS));
		}
		fTestSpec.setBPELUnitPort(getMockupPort());
		fTestSpec.setEnginePort(getEnginePort());
		fTestSpec.setPreserveURLs(isPreserveServiceURLs());
		fTestSpec.setAbortAfterClientTrack(isAbortAfterClientTrack());
		fTestSpec.setTestTimeout(getTestTimeout());
		fTestSpec.setIgnoreXPath(getIgnoreXPath());
	}

	private void printColumnHeaders() throws ConfigurationException, SpecificationException
	{
		final PrintStream ps = getOutputStream();

		ps.print(BPEL_FILE_COL_HEADER);
		ps.print('\t');
		for (String testCase : fTestSpec.getTestCaseNames()) {
			ps.print(testCase);
			ps.print('\t');
		}
		ps.print(TIMES_SEPARATOR);
		ps.print('\t');
		for (String testCase : fTestSpec.getTestCaseNames()) {
			ps.print(TIMES_SEPARATOR);
			ps.print(testCase);
			ps.print('\t');
		}
		ps.println();
	}

	

	/* ---- PARALLEL COMPARISON ---- */

    public void parallelCompare(String pathToBPTS, String pathToOriginalBPEL,
                                String pathToOriginalResult, List<String> pathsToOtherBPEL) throws Exception {
        if (isPrintColumnHeaders()) {
			initializeTestSpec(pathToBPTS);
			printColumnHeaders();
		}
        try {
		    List<String> args = createSubprocessArgs(pathToBPTS, pathToOriginalBPEL, pathToOriginalResult);
		    parallelProcess(pathsToOtherBPEL, null, args);
        } finally {
            if (isCleanupAfterExecution()) {
                cleanup();
            }
        }
	}

    public void parallelCompare(String pathToBPTS, String pathToOriginalBPEL,
                                String pathToOriginalResult, Reader in) throws Exception {
        if (isPrintColumnHeaders()) {
			initializeTestSpec(pathToBPTS);
			printColumnHeaders();
		}
        try {
		    List<String> args = createSubprocessArgs(pathToBPTS, pathToOriginalBPEL, pathToOriginalResult);
		    parallelProcess(null, in, args);
        } finally {
            if (isCleanupAfterExecution()) {
                cleanup();
            }
        }
	}

	private List<String> createSubprocessArgs(String pathToBPTS,
			String pathToOriginalBPEL, String pathToOriginalResult) {
		List<String> args = new ArrayList<String>(Arrays.asList(pathToBPTS,
				pathToOriginalBPEL, pathToOriginalResult));
		if (this.isKeepGoing()) {
			args.add(0, "--" + KEEP_GOING_OPTION);
		}
		if (this.isPreserveServiceURLs()) {
			args.add(0, "--" + PRESERVE_SERVICE_URLS_OPTION);
		}
		if (this.getIgnoreXPath() != null) {
			args.add(0, "--" + IGNORE_XPATH_OPTION);
			args.add(1, this.getIgnoreXPath());
		}
		return args;
	}

	/* ---- SEQUENTIAL COMPARISON ---- */

	private Pair<int[], long[]> compare(BPELProcessDefinition originalDef, String pathToOther) throws Exception {
		Pair<int[], long[]> result = compare(isKeepGoing() ? AbortOnDiff.TEST : AbortOnDiff.SUITE, originalDef, pathToOther);
		addResult(pathToOther, result.getLeft(), result.getRight());
		return result;
	}

	private Pair<int[], long[]> compare(final AbortOnDiff abortType,
			final BPELProcessDefinition originalDef, String pathToOther)
			throws Exception
	{
		final BPELUnitXMLResult originalResult = getOriginalResult();
		final int testCaseCount = originalResult.getCompositionOutboundMessages().size();
		final int[] results = new int[testCaseCount];
		final long[] nanos = new long[testCaseCount];

		try {
            Pair<ExecutionResults, Set<Integer>> comparison;
			if (pathToOther.toLowerCase().endsWith(".bpel")) {
                final BPELProcessDefinition def = new BPELProcessDefinition(new File(pathToOther), originalDef);
               	comparison = fTestSpec.run(abortType, def, originalResult);
            } else {
                comparison = fTestSpec.run(abortType, new File(pathToOther).getCanonicalPath(), originalResult);
            }

            assert abortType == AbortOnDiff.TEST || comparison.getRight().size() <= 1 : "There should only be 1 different test case at most if aborting the entire suite";
            for (int pos : comparison.getRight()) {
            	results[pos] = 1;
            }

			final ExecutionResults testResults = comparison.getLeft();
			for (int i = 0; i < testResults.getTestWallNanos().size(); ++i) {
				nanos[i] = testResults.getTestWallNanos().get(i);
			}
		} catch (DeploymentException ex) {
			Arrays.fill(results, 2);
		}

		return new Pair<int[], long[]>(results, nanos);
	}

	/* ---- LIST UTILITIES ---- */

	private void addResult(String path, int[] comparisons, long[] nanos) {
		synchronized (fResults) {
			fResults.add(new Pair<String, Pair<int[], long[]>>(path, new Pair<int[], long[]>(comparisons, nanos)));
			fResults.notifyAll();
		}
	}

	private void printResults(String bpel, Pair<int[], long[]> pair) {
		PrintStream out = getOutputStream();

		final int[] comparisons = pair.getLeft();
		final long[] nanos = pair.getRight();
		if (comparisons.length > 0) {
			out.print(bpel + " " + comparisons[0]);
			for (int i = 1; i < comparisons.length; ++i) {
				out.print(" " + comparisons[i]);
			}
			out.print(" " + TIMES_SEPARATOR + " " + nanos[0]);
			for (int i = 1; i < nanos.length; ++i) {
				out.print(" " + nanos[i]);
			}
		}
		out.println();
	}

	/**
	 * Main method for parallel execution.
	 *
	 * @throws Exception
	 *             There was a problem while performing the comparison.
	 */
	public static void main(String[] args) throws Exception {
		ExecutionSubcommand subcmd = new CompareSubcommand();
		subcmd.parseArgs(args);
		subcmd.run();
	}
}
