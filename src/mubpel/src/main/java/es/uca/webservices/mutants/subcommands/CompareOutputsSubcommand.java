package es.uca.webservices.mutants.subcommands;

import java.io.File;
import java.io.PrintStream;
import java.util.List;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import es.uca.webservices.bpel.bpelunit.BPELUnitXMLResult;

/**
 * <p>
 * Implements the 'compareout' subcommand.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */
public class CompareOutputsSubcommand extends AbstractSubcommand {

	private static final String USAGE =
		"Compares the results obtained by a WS-BPEL composition on a test suite\n" +
		"against those of several others. Results are the same as those produced\n" +
		"by 'compare --full', but the BPTS test suite will not be executed in\n"
		+ "this case.";

	private String fIgnoreXPath;

	public CompareOutputsSubcommand() {
		super(2, 0, "compareout", "xml xml1...", USAGE);
	}

	public Object getIgnoreXPath() {
		return fIgnoreXPath;
	}

	public void setIgnoreXPath(String fIgnoreXPath) {
		this.fIgnoreXPath = fIgnoreXPath;
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		if (options.has(CompareSubcommand.IGNORE_XPATH_OPTION)) {
			this.fIgnoreXPath = (String)options.valueOf(CompareSubcommand.IGNORE_XPATH_OPTION);
		}
	}

	@Override
	protected OptionParser createOptionParser() {
		final OptionParser parser = super.createOptionParser();

		parser.accepts(CompareSubcommand.IGNORE_XPATH_OPTION, CompareSubcommand.IGNORE_XPATH_HELP)
			.withRequiredArg().ofType(String.class).describedAs("xpath");

		return parser;
	}

	@Override
	protected void runCommand() throws Exception {
		List<String> nonOptionArgs = getNonOptionArgs();
		final String original = nonOptionArgs.get(0);
		PrintStream os = getOutputStream();
		for (String other : nonOptionArgs.subList(1, nonOptionArgs.size())) {
			os.println(
				String.format("%s-%s %s", original, other, formatResult(compareOutputs(original, other))));
		}
	}

	public Boolean[] compareOutputs(String pathToResultA, String pathToResultB)
			throws Exception {
		final File fileA = new File(pathToResultA);
		final File fileB = new File(pathToResultB);
		final BPELUnitXMLResult resultA = new BPELUnitXMLResult(fileA);
		final BPELUnitXMLResult resultB = new BPELUnitXMLResult(fileB);
		return resultA.compareResultsTo(resultB, fIgnoreXPath);
	}

	public String formatResult(Boolean[] result) {
		StringBuilder sb = new StringBuilder();
		for (int counter = 0; counter < result.length; ++counter) {
			if (counter > 0) {
				sb.append(' ');
			}
			sb.append(result[counter] ? "0" : "1");
		}
		return sb.toString();
	}
}
