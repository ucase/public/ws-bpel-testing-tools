<?xml version="1.0" encoding="UTF-8" ?>
<!--
   ELL (Expressions: Logical to Logical) WS-BPEL mutation operator

   Switches back and forth between the 'and' and 'or' XPath
   logical operators. Does not use the attribute value at all.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/xpath-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <xsl:call-template name="generate-xpath-operands">
      <xsl:with-param name="selectedNodes" select="'uca:and | uca:or'"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="uca:and" mode="mutate-ast-node">
    <uca:or>
      <xsl:copy-of select="@*|node()"/>
    </uca:or>
  </xsl:template>

  <xsl:template match="uca:or" mode="mutate-ast-node">
    <uca:and>
      <xsl:copy-of select="@*|node()"/>
    </uca:and>
  </xsl:template>

</xsl:stylesheet>
