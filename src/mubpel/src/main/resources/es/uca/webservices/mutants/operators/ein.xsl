<?xml version="1.0" encoding="UTF-8" ?>
<!--
   EIN (Expressions: Insertion Unary Negation) WS-BPEL mutation operator
   
   Inserts the XPath negation function (not) in logic expressions. 
   
   Valentín Liñeiro Barea <valentin.lineirobarea@alum.uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/xpath-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:variable name="fixedMaximumAttributeValue" select="1"/>

  <xsl:template name="generate-operands">
    <xsl:call-template name="generate-xpath-operands">
      <xsl:with-param name="selectedNodes" 
      select="concat('
      uca:expression
      /
      ( uca:eq | uca:neq | uca:gt | uca:ge | uca:lt
      | uca:le | uca:and | uca:or | uca:variableReference
      | uca:functionCall/uca:qname[@localPart = ', $quot, 'not', $quot, ']/(
          following-sibling::node())
      )')"/>
      <xsl:with-param
          name="selectedBPELNodes"
          select="'//bpel:condition | //bpel:joinCondition |
                        //bpel:transitionCondition'"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="*" mode="mutate-ast-node">
  	<uca:functionCall>
  		<uca:qname localPart="not" prefix=""/>
  		<xsl:copy-of select="."/>
  	</uca:functionCall>
  </xsl:template>

</xsl:stylesheet>
