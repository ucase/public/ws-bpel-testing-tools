<?xml version="1.0" encoding="UTF-8"?>
<!--
   ASI (Activities: Switch Instructions) WS-BPEL mutation operator

   Switches places between activities in a sequence.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
   Modified by Juan Boubeta Puig <juan.boubeta@uca.es>
-->
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable">

  <xsl:import href="es/uca/webservices/mutants/operators/op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <xsl:for-each select="//bpel:sequence[count(*[uca:canBeRemoved(.)]) > 1]/bpel:*[uca:canBeRemoved(.)]">
      <xsl:variable name="currentNode" select="."/>
      <xsl:for-each select="uca:validSiblings($currentNode)">
        <xsl:sequence select="$currentNode"/>
        <xsl:element name="uca:replacementPos">
          <xsl:value-of select="position()"/>
        </xsl:element>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="*" mode="copy-tree">
    <xsl:param name="node"/>
    <xsl:param name="extra"/>
    <xsl:variable
        name="replacement"
        select="uca:validSiblings($node)[number($extra/text())]"/>

    <xsl:choose>
      <xsl:when test=". is $node/..">
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <xsl:for-each select="node()">
            <xsl:choose>
              <xsl:when test=". is $node">
                <xsl:copy-of select="$replacement"/>
              </xsl:when>
              <xsl:when test=". is $replacement">
                <xsl:copy-of select="$node"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="."/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </xsl:copy>
      </xsl:when>

      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()" mode="copy-tree">
            <xsl:with-param name="node" select="$node"/>
            <xsl:with-param name="extra" select="$extra"/>
          </xsl:apply-templates>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- UTILITY FUNCTIONS -->

  <xsl:function name="uca:validSiblings" as="node()*">
    <xsl:param name="elem" as="node()"/>
    <xsl:sequence
        select="$elem/following-sibling::bpel:*[uca:isActivity(.) and uca:canBeRemoved(.)]"/>
  </xsl:function>

</xsl:stylesheet>
