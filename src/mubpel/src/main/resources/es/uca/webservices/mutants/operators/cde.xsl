<?xml version="1.0" encoding="UTF-8" ?>
<!--
   CDE (Coverage: Decision) WS-BPEL mutation operator
   
   Applies the decision coverage criteria to a WS-BPEL composition.
   
   Valentín Liñeiro Barea <valentin.lineirobarea@alum.uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/xpath-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>
  
  <xsl:variable name="fixedMaximumAttributeValue" select="2"/>
  
  <!-- 
  		Selects all decisions that exist into 
   		the composition.
  -->

  <xsl:template name="generate-operands">
    <xsl:call-template name="generate-xpath-operands">
      <xsl:with-param name="selectedNodes" 
      select="concat('uca:expression/(uca:eq | uca:neq | uca:gt | uca:ge | uca:lt | 
      uca:le | uca:and | uca:or | uca:variableReference | 
      uca:functionCall/uca:qname[@localPart = ', $quot, 'not', 
      $quot, ']/(following-sibling::uca:eq | following-sibling::uca:neq | 
      following-sibling::uca:gt | following-sibling::uca:ge | 
      following-sibling::uca:lt | following-sibling::uca:le | 
      following-sibling::uca:and | following-sibling::uca:or))')"/>
      <xsl:with-param
          name="selectedBPELNodes"
          select="'//bpel:condition | //bpel:joinCondition |
                        //bpel:transitionCondition'"/>
    </xsl:call-template>
  </xsl:template>
  
  <!-- 
  	Uses the attribute variable to select when
   	mutate with true() or false().
  -->

  <xsl:template match="*" mode="mutate-ast-node">
    <xsl:choose>
	  <xsl:when test="$attribute = 1">
	    <uca:functionCall>
         <uca:qname localPart="true" prefix=""/>
      	</uca:functionCall>
  	  </xsl:when>
      <xsl:otherwise>
  	    <uca:functionCall>
         <uca:qname localPart="false" prefix=""/>
      	</uca:functionCall>
  	  </xsl:otherwise>
  	</xsl:choose>
  </xsl:template>

</xsl:stylesheet>
