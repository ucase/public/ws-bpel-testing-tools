﻿<?xml version="1.0" encoding="UTF-8"?>
<!--
   CFA WS-BPEL mutation operator
   
   Replaces an activity by an <exit> activity, in order to apply fault coverage.

   Valentín Liñeiro Barea <valentin.lineirobarea@alum.uca.es>
-->
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable">

  <xsl:import href="es/uca/webservices/mutants/operators/op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <!--
      Check activities we can replace safely.
  -->
  <xsl:template name="generate-operands">
  	<xsl:sequence select="//bpel:*[uca:canBeRemoved(.)]"/>
  </xsl:template>

  <!-- 
  	At the beginning, we replaced activities by a <throw> activity.
  	However, this solution required a fault handler, and a reply to the client.
  	We can't reply to the client in a generic way, so we use the <exit> activity,
  	and ActiveBPEL will reply to the client with a SOAP fault, the fault we need.
  	At this way, we avoid the fault handler, and we simplify the code of this
  	operator.
  -->
  
  <xsl:template match="*" mode="mutate-node">
  	<bpel:exit/>
  </xsl:template>

</xsl:stylesheet>
