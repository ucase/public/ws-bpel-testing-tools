<?xml version="1.0" encoding="UTF-8" ?>
<!--
   EMF (Expressions: Message Deadline) WS-BPEL mutation operator

   Halves or sets to zero a deadline expression, such as those
   used in the <until> elements.

   Juan Boubeta Puig <juan.boubeta@uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:saxon="http://saxon.sf.net/"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/xpath-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:variable name="fixedMaximumAttributeValue" select="1"/>

  <xsl:template name="generate-operands">
    <xsl:call-template name="generate-xpath-operands">
      <xsl:with-param
          name="selectedNodes"
          select="concat('uca:stringConstant[matches(.,',
                  $quot,
                  '-?
                  [0-9]{4}-[0-9]{2}-[0-9]{2}',
                  $quot,
                  ',',
                  $quot, 'x', $quot,
                  ')]')"/>
      <xsl:with-param
          name="selectedBPELNodes"
          select="'//bpel:until'"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="*" mode="mutate-ast-node">

    <xsl:variable
        name="finalAttribute"
        select="abs(($attribute - 1) mod $fixedMaximumAttributeValue)"/>

    <xsl:choose>
      <xsl:when test="$finalAttribute = 0">
        <xsl:copy>
          <xsl:value-of select="concat($quot, fn:adjust-date-to-timezone(fn:current-date(), ()), $quot)"/>
        </xsl:copy>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
