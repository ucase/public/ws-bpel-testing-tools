<?xml version="1.0" encoding="UTF-8" ?>
<!--
   XTF (eXceptions) WS-BPEL mutation operator

   Replaces the fault thrown by a throw activity.

   Juan Boubeta Puig <juan.boubeta@uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <xsl:for-each select="//bpel:throw">

      <xsl:variable name="currentNode" select="."/>

      <!-- If the current throw faultName can be replaced by at least one other faultName, then
           list it as an operand. -->
      <xsl:for-each select="@faultName">
        <xsl:for-each select="uca:listFaultNames(., $currentNode)">
          <xsl:sequence select="$currentNode"/>
          <xsl:element name="uca:faultName">
            <xsl:value-of select="."/>
          </xsl:element> 
        </xsl:for-each>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>


  <!-- Mutates faultName attributes by changing their value -->
  <xsl:template match="*" mode="mutate-node">
    <xsl:param name="extra"/>

    <xsl:copy>
      <xsl:copy-of select="@*[local-name() != 'faultName' and namespace-uri() = '']"/>
      <xsl:attribute name="faultName">
        <xsl:value-of select="$extra/text()"/>
      </xsl:attribute>
      <xsl:copy-of select="node()"/>
    </xsl:copy>
  </xsl:template> 


  <xsl:template match="bpel:throw" mode="collect-faultNames">
    <xsl:sequence select="@faultName"/>
  </xsl:template>

  <xsl:template match="bpel:scope" mode="collect-faultNames"/>

  <xsl:template match="*" mode="collect-faultNames">
    <xsl:apply-templates mode="collect-faultNames"/>
  </xsl:template>

  <xsl:template match="text()" mode="collect-faultNames"/>


  <!-- UTILITY FUNCTIONS -->

  <!-- Lists faultNames which can replace a fault. These faultNames
       are selected from the same scope the original fault was
       defined. -->
  <xsl:function name="uca:listFaultNames" as="xs:string*">
    <xsl:param name="faultName" as="xs:string"/>
    <xsl:param name="currentNode"/>

    <xsl:variable
        name="AllFaultNames"
        select="fn:distinct-values(uca:listAllFaultNames($currentNode))"/>

    <xsl:sequence select="fn:remove($AllFaultNames, fn:index-of($AllFaultNames, $faultName))"/>

  </xsl:function>


  <xsl:function name="uca:listAllFaultNames" as="xs:string*">
    <xsl:param name="currentNode"/>

    <xsl:apply-templates select="$currentNode/(ancestor::bpel:process|ancestor::bpel:scope)[last()]/*" mode="collect-faultNames"/>

  </xsl:function>

</xsl:stylesheet>
