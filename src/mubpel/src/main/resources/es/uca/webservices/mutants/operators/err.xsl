<?xml version="1.0" encoding="UTF-8" ?>
<!--
   ERR (Expressions: Relational to Relational) WS-BPEL mutation operator

   Exchanges a relational operator with another relational operator in
   an XPath expression.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/xpath-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:variable name="fixedMaximumAttributeValue" select="5"/>

  <xsl:template name="generate-operands">
    <xsl:call-template name="generate-xpath-operands">
      <xsl:with-param name="selectedNodes" select="'uca:eq | uca:neq | uca:lt | uca:le | uca:gt | uca:ge'"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="*" mode="mutate-ast-node">
    <xsl:variable name="current" select="."/>
    <xsl:variable name="replacements" as="xs:string*">
      <xsl:sequence select="('lt','gt','le','ge','eq','neq')"/>
    </xsl:variable>

    <xsl:element name="uca:{$replacements[not(. = local-name($current))][1 + abs(($attribute - 1) mod 5)]}">
      <xsl:copy-of select="@*|node()"/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
