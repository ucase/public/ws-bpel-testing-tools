<?xml version="1.0" encoding="UTF-8"?>
<!--
   APM (Activities: Pick Message) WS-BPEL mutation operator

   Removes an <onMessage> element from a <pick> activity, if there
   is more than one.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable">

  <xsl:import href="es/uca/webservices/mutants/operators/delete-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <xsl:sequence select="//bpel:pick[count(bpel:onMessage) &gt; 1]/bpel:onMessage"/>
  </xsl:template>

</xsl:stylesheet>