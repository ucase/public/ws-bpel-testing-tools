<?xml version="1.0" encoding="UTF-8" ?>
<!--
   EMD (Expressions: Message Duration) WS-BPEL mutation operator

   Halves or sets to zero a duration expression, such as those
   used in the <for> and <repeatEvery> elements.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/xpath-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:variable name="fixedMaximumAttributeValue" select="2"/>

  <xsl:template name="generate-operands">
    <xsl:call-template name="generate-xpath-operands">
      <xsl:with-param
          name="selectedNodes"
          select="concat('uca:stringConstant[matches(.,',
                  $quot,
                  '-?
                  P([0-9]+Y)?([0-9]+M)?([0-9]+D)?
                  T?([0-9]+H)?([0-9]+M)?([.0-9]+S)?',
                  $quot,
                  ',',
                  $quot, 'x', $quot,
                  ')]')"/>
      <xsl:with-param
          name="selectedBPELNodes"
          select="'//bpel:for | //bpel:repeatEvery'"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="*" mode="mutate-ast-node">
    <xsl:variable
        name="millis"
        select="f:durationToMillis(.)"/>
    <xsl:variable
        name="finalAttribute"
        select="abs(($attribute - 1) mod $fixedMaximumAttributeValue)"/>

    <xsl:choose>
      <xsl:when test="$finalAttribute = 0">
        <xsl:copy>
          <xsl:value-of select="concat($quot, f:millisToDuration($millis div 2), $quot)"/>
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:value-of select="concat($quot, f:millisToDuration(0), $quot)"/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
