<?xml version="1.0" encoding="UTF-8" ?>
<!--
   ECN (Expressions: Change Number) WS-BPEL mutation operator

   Changes an XPath number constant with another, by doing one of the following:

   - incrementing by 1,
   - decrementing by 1,
   - repeating the last digit (if 0, replace by 10), or
   - removing the last digit (if 0, replace by -10; if 1, replace by -1 instead).

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/xpath-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:variable name="fixedMaximumAttributeValue" select="4"/>

  <xsl:template name="generate-operands">
    <xsl:call-template name="generate-xpath-operands">
      <xsl:with-param name="selectedNodes" select="'uca:numberConstant'"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="*" mode="mutate-ast-node">
    <xsl:variable name="finalAttr" select="abs(($attribute - 1) mod 4)"/>
    <xsl:variable name="n" select="number(text())"/>

    <!--
        To avoid repeated mutants, some cases are handled
        specially. Right now, these are the possible results depending
        on the value of the operand and the attribute (0-3):

        n = 0: (1,-1,10,-10)
        n = 1: (2, 0,11, -1)
        otherwise: (n+1, n-1, repeat last digit, remove last digit)
    -->
    <xsl:copy>
      <xsl:choose>
        <xsl:when test="$finalAttr = 0">
          <xsl:value-of select="$n + 1"/>
        </xsl:when>
        <xsl:when test="$finalAttr = 1">
          <xsl:value-of select="$n - 1"/>
        </xsl:when>
        <xsl:when test="$finalAttr = 2">
          <xsl:choose>
            <xsl:when test="$n = 0">10</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number($n * 10 + $n mod 10, '0.00')"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$n = 0">-10</xsl:when>
            <xsl:when test="$n = 1">-1</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number($n idiv 10, '0.00')"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
