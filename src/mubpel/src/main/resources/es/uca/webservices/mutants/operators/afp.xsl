<?xml version="1.0" encoding="UTF-8"?>
<!--
   AFP (Activities: For Each, Parallel attribute) WS-BPEL mutation operator

   Changes the parallel attribute in <forEach> activities from "no" to "yes".
   We assume that changing from "yes" to "no" would only impact the process'
   performance, and not its results.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable">

  <xsl:import href="es/uca/webservices/mutants/operators/op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <xsl:sequence select="//bpel:forEach[@parallel='no']"/>
  </xsl:template>

  <xsl:template match="bpel:forEach" mode="mutate-node">
    <xsl:element name="forEach" namespace="http://docs.oasis-open.org/wsbpel/2.0/process/executable">
      <xsl:copy-of select="@*[local-name(.) != 'parallel']"/>
      <xsl:attribute name="parallel">yes</xsl:attribute>
      <xsl:copy-of select="node()"/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>