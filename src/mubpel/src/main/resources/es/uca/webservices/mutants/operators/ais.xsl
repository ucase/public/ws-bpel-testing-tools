<?xml version="1.0" encoding="UTF-8"?>
<!--
   AIS (Activities: Isolated Scopes) WS-BPEL mutation operator

   Changes the isolated attribute in <scope> activities from "yes" to "no".
   This could make the process fail if there were data dependencies between
   the results of each iteration.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable">

  <xsl:import href="es/uca/webservices/mutants/operators/op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <xsl:sequence select="//bpel:scope[@isolated='yes']"/>
  </xsl:template>

  <xsl:template match="bpel:scope" mode="mutate-node">
    <xsl:element name="scope" namespace="http://docs.oasis-open.org/wsbpel/2.0/process/executable">
      <xsl:copy-of select="@*[local-name(.) != 'isolated']"/>
      <xsl:attribute name="isolated">no</xsl:attribute>
      <xsl:copy-of select="node()"/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>