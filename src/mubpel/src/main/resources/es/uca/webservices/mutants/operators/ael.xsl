<?xml version="1.0" encoding="UTF-8"?>
<!--
   AEL (Activity: Eliminate) WS-BPEL mutation operator

   Removes an activity from the WS-BPEL process definition. Currently, <receive>
   activities with createInstance="yes" are skipped, as removing them might
   produce mutants which do not satisfy SA00056 (per the WS-BPEL 2.0 standard).
   <empty> activities are also skipped: removing them would only produce
   equivalent mutants.

   All other activities can be removed. If an activity is the only member of an
   structured activity, it will be replaced by an <empty> activity instead.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable">

  <xsl:import href="es/uca/webservices/mutants/operators/delete-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <!--
      Checking the whole document tree for activities which can be
      removed might be slow, but it's very simple to implement. If we
      required more performance in the future, we could use a pruned
      search which only descended through the structured activities,
      using a recursive function.
  -->
  <xsl:template name="generate-operands">
    <xsl:sequence select="//bpel:*[uca:canBeRemoved(.)]"/>
  </xsl:template>

  <!-- Replace activities which are the only child of their structured
       activity by <empty>, instead of just removing them. -->
  <xsl:template match="*[uca:countSiblingActivities(.) = 0]" mode="mutate-node">
    <bpel:empty/>
  </xsl:template>


  <!-- UTILITY FUNCTIONS -->

  <!-- Returns the number of siblings which are WS-BPEL activities -->
  <xsl:function name="uca:countSiblingActivities" as="xs:integer">
    <xsl:param name="elem" as="node()"/>
    <xsl:sequence select="count($elem/../bpel:*[uca:isActivity(.) and not(. is $elem)])"/>
  </xsl:function>

</xsl:stylesheet>
