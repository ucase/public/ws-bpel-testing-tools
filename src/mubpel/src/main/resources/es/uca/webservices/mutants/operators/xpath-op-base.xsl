<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    xmlns:saxon="http://saxon.sf.net/">

  <xsl:import href="es/uca/webservices/mutants/operators/op-base.xsl"/>
  <xsl:variable name="XPATH_URI" select="'urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0'"/>

  <xsl:template match="*" mode="mutate-node">
    <xsl:param name="ast"/>
    <xsl:param name="extra"/>

    <xsl:variable name="finalAST">
      <xsl:apply-templates select="$ast" mode="mutate-ast">
        <xsl:with-param name="bpelNode" select="."/>
        <xsl:with-param name="extra" select="$extra"/>
      </xsl:apply-templates>
    </xsl:variable>
    <xsl:variable name="finalExpr" select="conv:xml2XPath($finalAST)"/>

    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:value-of select="$finalExpr"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="@*|node()" mode="mutate-ast">
    <xsl:param name="extra"/>
    <xsl:param name="bpelNode"/>

    <xsl:choose>
      <xsl:when test=". is $extra[1]">
        <xsl:apply-templates select="." mode="mutate-ast-node">
          <xsl:with-param name="bpelNode" select="$bpelNode"/>
          <xsl:with-param name="extra" select="$extra"/>
        </xsl:apply-templates>
      </xsl:when>

      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()" mode="mutate-ast">
            <xsl:with-param name="bpelNode" select="$bpelNode"/>
            <xsl:with-param name="extra" select="$extra"/>
          </xsl:apply-templates>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="generate-xpath-operands">
    <xsl:param name="selectedNodes"/>
    <xsl:param name="selectedBPELNodes"
               select="'//bpel:condition | //bpel:joinCondition |
                        //bpel:transitionCondition |
                        //bpel:assign/bpel:copy/(bpel:from | bpel:to |
                                                 bpel:from/bpel:query|
                                                 bpel:to/bpel:query) |
                        //bpel:for | //bpel:repeatEvery | //bpel:until'"/>

    <xsl:for-each select="saxon:evaluate($selectedBPELNodes)">

      <xsl:variable name="currentNode" select="."/>
      <xsl:variable name="expr" select="normalize-space(uca:concat-seq(text()))"/>

      <xsl:if test="$expr != ''">
        <xsl:variable name="xpathTree" select="conv:xpath2XML($expr)"/>

        <!-- Each relational operator in the XPath expression
             constitutes its own operand -->
        <xsl:for-each select="saxon:evaluate(concat('$p1//(', $selectedNodes, ')'), $xpathTree)">
          <xsl:sequence select="$currentNode"/>
          <xsl:sequence select="$xpathTree/uca:expression"/>
          <xsl:sequence select="."/>
        </xsl:for-each>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
