<?xml version="1.0" encoding="UTF-8"?>
<!--
   Base stylesheet for all operators that delete activities or elements from
   the WS-BPEL process definition. Includes the required logic to delete the
   selected operand and the links entering or leaving the selected
   operand.

   Stylesheets which inherit from this one only need to define the
   generate-operands named template.

   Author: Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    xmlns:saxon="http://saxon.sf.net/">

  <xsl:import href="es/uca/webservices/mutants/operators/op-base.xsl"/>
  <xsl:key name="link-names" match="bpel:link" use="@name"/>

  <xsl:template name="collect-ignored-elems">
    <xsl:param name="node"/>

    <xsl:for-each select="$node//(bpel:source|bpel:target)">
      <xsl:choose>
        <xsl:when test="exists(key('link-names', @linkName, $node))">
          <!-- The link is defined inside the operand: don't do
               anything.  Since it doesn't matter *where* inside the
               operand it is, we can use a key to check this. The
               amortized running time for checking a key should be
               shorter than traversing the tree all the time. -->
        </xsl:when>
        <xsl:otherwise>
          <!-- The link is defined outside the operand. We need to
               collect it and the endpoints of the <link> so they are
               not copied back in the mutant. As names only need to be
               unique inside a <flow> (SA00064) and can be reused in
               different <flow>s, we cannot use a <key>: we will have
               to traverse the tree to find them. -->

          <xsl:variable
              name="linkName"
              select="@linkName"/>
          <xsl:variable
              name="linkNode" as="node()"
              select="($node/preceding::bpel:links/bpel:link[@name=$linkName])[last()]"/>
          <xsl:variable
              name="otherEndpoint" as="node()">
            <xsl:choose>
              <xsl:when test="self::bpel:source">
                <xsl:sequence
                    select="$linkNode/following::bpel:target[@linkName=$linkName][1]"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:sequence
                    select="$linkNode/following::bpel:source[@linkName=$linkName][1]"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>

          <xsl:sequence select="$linkNode"/>
          <xsl:sequence select="."/>
          <xsl:sequence select="$otherEndpoint"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <!-- Default mutation rule: delete -->
  <xsl:template match="*" mode="mutate-node"/>

</xsl:stylesheet>
