<?xml version="1.0" encoding="UTF-8" ?>
<!--
   EIU (Expressions: Insertion Unary Minus) WS-BPEL mutation operator
   
   Inserts the XPath unary minus operator in aritmethic expressions.   

   Valentín Liñeiro-Barea <valentin.lineirobarea@alum.uca.es>
   Antonio García-Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/xpath-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:variable name="fixedMaximumAttributeValue" select="1"/>
  
  <!-- 
  	We select all arithmetic expressions, including number constants and function
  	calls to numeric functions. We skip over zero constants or invocations of number(0).
   -->
  <xsl:template name="generate-operands">
    <xsl:call-template name="generate-xpath-operands">
      <xsl:with-param name="selectedNodes" 
      select="concat('
        (uca:expression | uca:eq | uca:neq | uca:lt | uca:le | uca:gt | uca:ge)
        /
        (uca:add | uca:sub | uca:div | uca:mul | uca:mod | uca:neg
         | uca:numberConstant[string(.) != ', $quot, '0.0', $quot, ']
         | uca:functionCall[
             (uca:qname[@localPart = ', $quot, 'number', $quot, ']
              and count(uca:numberConstant[string(.) = ', $quot, '0.0', $quot, ']) = 0)
             or uca:qname[@localPart = ', $quot, 'sum', $quot,']
             or uca:qname[@localPart = ', $quot, 'floor', $quot,']
             or uca:qname[@localPart = ', $quot, 'ceiling', $quot,']
             or uca:qname[@localPart = ', $quot, 'round', $quot,']
          ]
        )')"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="*" mode="mutate-ast-node">
    <xsl:element name="uca:neg">
      <xsl:copy-of select="."/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
