<?xml version="1.0" encoding="UTF-8" ?>
<!--
   EAN (Expressions: Absolute value, Negative) WS-BPEL mutation operator
   
   Replaces a subexpression by its negative absolute value.
   
   This operator computes its operands in the same way as EIU, so it is based on it.
   However, the mutation itself is different.  

   Valentín Liñeiro-Barea <valentin.lineirobarea@alum.uca.es>
   Antonio García-Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/eiu.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <!--
  	ActiveBPEL 1.4 doesn't support XPath 2.0, so we can't use
  	the abs() function. We simulate it through this:
  		-abs(x) = -(x * ((x >= 0) - (x < 0)))
   -->
  <xsl:template match="*" mode="mutate-ast-node">
  	<uca:neg>
  		<uca:mul>
  			<xsl:copy-of select="."/>
  			<uca:sub>
  				<uca:ge>
  					<xsl:copy-of select="."/>
  					<uca:numberConstant>0.0</uca:numberConstant>
  				</uca:ge>
  				<uca:lt>
  					<xsl:copy-of select="."/>
  					<uca:numberConstant>0.0</uca:numberConstant>
  				</uca:lt>
  			</uca:sub>
  		</uca:mul>
  	</uca:neg>
  </xsl:template>

</xsl:stylesheet>
