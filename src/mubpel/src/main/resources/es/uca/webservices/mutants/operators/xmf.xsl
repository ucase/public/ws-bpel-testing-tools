<?xml version="1.0" encoding="UTF-8"?>
<!--
   XMF (eXceptions: Missed Fault Handler) WS-BPEL mutation operator

   Removes a <catch> or <catchAll> handler in a WS-BPEL fault handler.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
   Modified by Juan Boubeta Puig <juan.boubeta@uca.es>
-->
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable">

  <xsl:import href="es/uca/webservices/mutants/operators/delete-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <xsl:for-each select="//bpel:faultHandlers">
      <xsl:choose>
        <xsl:when test="count(bpel:catch|bpel:catchAll) > 1">
          <xsl:sequence select="bpel:catch|bpel:catchAll"/>
        </xsl:when>
        <xsl:when test="count(bpel:catch|bpel:catchAll) = 1">
          <xsl:sequence select="."/>
        </xsl:when>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
