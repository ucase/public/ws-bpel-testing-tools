<?xml version="1.0" encoding="UTF-8"?>
<!--
   XSLT file which models an invalid mutation operator

   This operator is intentionally designed to generate invalid
   mutants, that is, mutants which cannot be deployed in an
   WS-BPEL conforming engine.

   It purposefully modifies that the WS-BPEL process definition
   to violate the SA-00004 static analysis requirement, per the
   WS-BPEL 2.0 standard:

   "If any referenced queryLanguage or expressionLanguage is
   unsupported by the WS-BPEL processor then the processor
   MUST reject the submitted WS-BPEL process definition."

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable">

  <xsl:import href="es/uca/webservices/mutants/operators/op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <xsl:sequence select="//bpel:process"/>
  </xsl:template>

  <xsl:template match="bpel:process" mode="mutate-node">
    <xsl:element name="process"
                 namespace="http://docs.oasis-open.org/wsbpel/2.0/process/executable">
      <xsl:copy-of select="@*[not(local-name(.) = 'expressionLanguage')]"/>
      <xsl:attribute name="expressionLanguage">http://www.example.com/FakeLanguage</xsl:attribute>

      <xsl:copy-of select="node()"/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>