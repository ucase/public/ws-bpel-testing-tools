<?xml version="1.0" encoding="UTF-8" ?>
<!--
   EAA (Expressions: Arithmetic to Arithmetic) WS-BPEL mutation operator

   Changes an XPath arithmetic operator with another. Candidates are
   selected from a predefined list containing addition, subtraction,
   multiplication, division and modulus, from which the existing
   operator is removed. This list is indexed using the zero-based
   attribute.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/xpath-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:variable name="fixedMaximumAttributeValue" select="4"/>

  <xsl:template name="generate-operands">
    <xsl:call-template name="generate-xpath-operands">
      <xsl:with-param name="selectedNodes" select="'uca:add | uca:sub | uca:div | uca:mul | uca:mod'"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="*" mode="mutate-ast-node">
    <xsl:variable name="current" select="."/>
    <xsl:variable name="replacements" as="xs:string*">
      <xsl:sequence select="('add','sub','mul','div','mod')"/>
    </xsl:variable>

    <xsl:element name="uca:{$replacements[not(. = local-name($current))][1+abs(($attribute - 1) mod 4)]}">
      <xsl:copy-of select="@*|node()"/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
