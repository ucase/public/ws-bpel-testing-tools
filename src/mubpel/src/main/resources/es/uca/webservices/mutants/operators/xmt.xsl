<?xml version="1.0" encoding="UTF-8"?>
<!--
   XMT (eXceptions: reMove Termination) WS-BPEL mutation operator

   Removes a termination handler from a scope.

   Juan Boubeta Puig <juan.boubeta@uca.es>
-->
<xsl:stylesheet
    version="2.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable">

  <xsl:import href="es/uca/webservices/mutants/operators/delete-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <xsl:for-each select="//bpel:scope/bpel:terminationHandler">
      <xsl:sequence select="."/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="*" mode="mutate-node"/>

</xsl:stylesheet>
