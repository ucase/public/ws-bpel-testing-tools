<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <!-- Most mutation operators do not use the attribute field at all,
       so any value is the same to them. -->
  <xsl:variable name="fixedMaximumAttributeValue" select="1"/>

  <!-- Useful for when we need to include a literal quote somewhere,
       as there is no way to escape quotes in XPath -->
  <xsl:variable name="quot"><xsl:text>'</xsl:text></xsl:variable>

  <!-- Starting template -->
  <xsl:template match="/">
    <xsl:variable name="operands" as="node()*">
      <xsl:call-template name="generate-operands"/>
    </xsl:variable>
    <xsl:variable name="operandCount" select="count($operands[self::bpel:*])"/>

    <xsl:choose>
      <xsl:when test="$action = 'count'">
        <uca:operandCount>
          <xsl:value-of select="$operandCount"/>
        </uca:operandCount>
      </xsl:when>

      <xsl:when test="$action = 'maximumAttributeValue'">
        <uca:maximumDistinctAttributeValue>
          <xsl:call-template name="maximum-attribute-value"/>
        </uca:maximumDistinctAttributeValue>
      </xsl:when>

      <xsl:when test="$action = 'debugOperandList'">
        <uca:operandList>
          <xsl:for-each select="$operands">
            <uca:element name="{name(.)}">
              <xsl:choose>
                <xsl:when test="self::uca:*">
                  <uca:ast>
                    <xsl:copy-of select="."/>
                  </uca:ast>
                  <uca:expr>
                    <xsl:value-of select="conv:xml2XPath(.)"/>
                  </uca:expr>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:copy-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </uca:element>
          </xsl:for-each>
        </uca:operandList>
      </xsl:when>

      <!-- Useful for debugging the operands to be actually used -->
      <xsl:when test="$action='debugChosenOperands'">
        <uca:chosenOperands>
          <xsl:for-each select="(1 to count($operands[self::bpel:*]))">
            <uca:chosenOperand position="{.}">
              <xsl:for-each select="uca:nthOperand(., $operands)">
                <uca:argument>
                  <xsl:copy-of select="."/>
                  <xsl:if test="self::uca:*">
                    <uca:expr>
                      <xsl:value-of select="conv:xml2XPath(.)"/>
                    </uca:expr>
                  </xsl:if>
                </uca:argument>
              </xsl:for-each>
            </uca:chosenOperand>
          </xsl:for-each>
        </uca:chosenOperands>
      </xsl:when>

      <xsl:when test="$action = 'apply'">
        <xsl:choose>
          <!-- Produce a warning and act as a noop if there are no
               operands for the operator that the user wants to apply -->
          <xsl:when test="$operandCount &lt;= 0">
            <xsl:message>There are no operands where this operator can be applied. The resulting process definition will be the same as the original.
            </xsl:message>
            <xsl:copy-of select="/"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:variable
                name="selectedOperandIndex"
                select="1 + abs(($operandIndex - 1) mod $operandCount)"/>
            <xsl:variable
                name="selectedOperand"
                select="uca:nthOperand($selectedOperandIndex, $operands)"/>
            <xsl:variable
                name="BPELNode"
                select="$selectedOperand[self::bpel:*][1]"/>
            <xsl:variable
                name="ast"
                select="$selectedOperand[self::uca:expression][1]"/>
            <xsl:variable
                name="extra"
                select="$selectedOperand[position() &gt; 1][not(self::uca:expression)]"/>

            <!-- These elements should *not* be copied over. See
                 delete-op-base for an example. -->
            <xsl:variable name="ignoredElems" as="node()*">
              <xsl:call-template name="collect-ignored-elems">
                <xsl:with-param name="node" select="$BPELNode"/>
              </xsl:call-template>
            </xsl:variable>

            <xsl:apply-templates mode="copy-tree">
              <xsl:with-param name="node" select="$BPELNode"/>
              <xsl:with-param name="ast" select="$ast"/>
              <xsl:with-param name="extra" select="$extra"/>
              <xsl:with-param name="ignoredElems" select="$ignoredElems"/>
            </xsl:apply-templates>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:otherwise>
        <xsl:message
            terminate="yes"
            select="concat('Error: unknown action ', $action)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- By default, no elements are ignored during copying -->
  <xsl:template name="collect-ignored-elems">
    <xsl:param name="node"/>
  </xsl:template>

  <xsl:template match="@*|node()" mode="copy-tree">
    <xsl:param name="node"/>
    <xsl:param name="ast"/>
    <xsl:param name="extra"/>
    <xsl:param name="ignoredElems" as="node()*"/>

    <xsl:choose>
      <xsl:when test=". is $node">
        <xsl:apply-templates select="." mode="mutate-node">
          <xsl:with-param name="ast" select="$ast"/>
          <xsl:with-param name="extra" select="$extra"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:when test="some $a in $ignoredElems satisfies . is $a"/>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()" mode="copy-tree">
            <xsl:with-param name="node" select="$node"/>
            <xsl:with-param name="ast" select="$ast"/>
            <xsl:with-param name="extra" select="$extra"/>
            <xsl:with-param name="ignoredElems" select="$ignoredElems"/>
          </xsl:apply-templates>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Each template which reuses this function only needs to define
       the value of maximumAttributeValue accordingly, if different
       from 1. If it is not a fixed value, then a completely new
       function will need to be defined, so it overrides this one. -->
  <xsl:template name="maximum-attribute-value">
    <xsl:value-of select="$fixedMaximumAttributeValue"/>
  </xsl:template>


  <!-- UTILITY FUNCTIONS -->

  <xsl:function name="uca:concat-seq" as="xs:string">
    <xsl:param name="seq"/>
    <xsl:variable name="result">
      <xsl:for-each select="$seq"><xsl:value-of select="."/></xsl:for-each>
    </xsl:variable>

    <xsl:value-of select="$result"/>
  </xsl:function>

  <!-- Select the nth operand from the operand list: we need to define
       our own function, since we have to use a "flat" list to avoid
       creating copies of the existing nodes, and thus losing the
       ability to exactly point to a node in a XPath AST, for
       instance. The problem with this approach (and the reason for
       this function) is that each operand can have 1-3 parts (the
       last 2 are optional), and several operands can share the same
       BPEL activity node, so it is rather hard to select an operand
       reliably using regular indices.

       uca:nthOperand($n, $l) returns the subsequence of $l that
       corresponds to the entries of the n-th operand. At the very
       least, we will have as the first element the WS-BPEL activity
       node. Optionally, and in the same order, we will find later two
       more elements, with the XPath AST and its exact node to be
       mutated.

       If no such operand exists, the function will return the empty
       sequence.
  -->
  <xsl:function name="uca:nthOperand" as="node()*">
    <xsl:param name="n" as="xs:integer"/>
    <xsl:param name="operandList" as="node()*"/>

    <!-- Divide the operand list into head (the first operand in the
         list) and tail (the rest) -->

    <xsl:variable name="activityRemoved"
                  select="remove($operandList, 1)"/>
    <xsl:variable name="nextActivity"
                  select="$activityRemoved[self::bpel:*][1]"/>
    <xsl:variable name="nextActivityOffset">
      <xsl:choose>
        <xsl:when test="exists($nextActivity)">
          <xsl:value-of select="uca:index-of-node($activityRemoved, $nextActivity)[1]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="count($activityRemoved) + 1"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="head" as="node()*">
      <xsl:sequence select="$operandList[1]"/>
      <xsl:for-each select="$activityRemoved[position() &lt; $nextActivityOffset]">
        <xsl:sequence select="."/>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="tail" as="node()*">
      <xsl:for-each select="$activityRemoved[position() &gt;= $nextActivityOffset]">
        <xsl:sequence select="."/>
      </xsl:for-each>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$n &lt;= 1">
        <xsl:sequence select="$head"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="uca:nthOperand($n - 1, $tail)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <!-- Workaround for the builtin index-of XPath function in Saxon,
       which does not seem to work properly -->
  <xsl:function name="uca:index-of-node" as="xs:integer*">
    <xsl:param name="sequence" as="node()*"/>
    <xsl:param name="srch" as="node()"/>
    <xsl:for-each select="$sequence">
      <xsl:if test=". is $srch">
        <xsl:sequence select="position()"/>
      </xsl:if>
    </xsl:for-each>
  </xsl:function>

  <!-- Checks whether a node is a WS-BPEL activity -->
  <xsl:function name="uca:isActivity" as="xs:boolean">
    <xsl:param name="elem" as="node()"/>

    <!-- Names taken from the WS-BPEL 2.0 XML Schema -->
    <xsl:variable
        name="activityNames"
        select="('assign', 'compensate', 'compensateScope', 'empty', 'exit',
                'extensionActivity', 'flow', 'forEach', 'if', 'invoke',
                'pick', 'receive', 'repeatUntil', 'reply', 'rethrow', 'scope',
                'scope', 'sequence', 'throw', 'validate', 'wait', 'while')"/>
    <xsl:sequence
        select="exists($elem/self::bpel:*[exists(index-of($activityNames, local-name($elem)))])"/>
  </xsl:function>

  <!-- Filter out activities which cannot be safely replaced -->
  <xsl:function name="uca:canBeRemoved" as="xs:boolean">
    <xsl:param name="elem" as="node()"/>
    <xsl:sequence
        select="uca:isActivity($elem)
                and local-name($elem) ne 'empty'
                and not(exists($elem/../self::bpel:process))
                and not(exists($elem/self::bpel:receive[@createInstance='yes']))"/>
  </xsl:function>

</xsl:stylesheet>
