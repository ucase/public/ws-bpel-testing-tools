<?xml version="1.0" encoding="UTF-8" ?>
<!--
   ISV (Identifier: Switch Variables) WS-BPEL mutation operator

   Exchanges a variable with another variable of an equivalent type
   from the same scope. Currently, type equivalence is performed
   by name.

   Antonio García Domínguez <antonio.garciadominguez@uca.es>
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:f="java:es.uca.webservices.bpel.util.FuncionesXPath"
    xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
    xmlns:uca="http://www.uca.es/xpath/2007/11"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    version="2.0">

  <xsl:import href="es/uca/webservices/mutants/operators/xpath-op-base.xsl"/>

  <xsl:param name="action" as="xs:string"/>
  <xsl:param name="operandIndex" as="xs:integer"/>
  <xsl:param name="attribute" as="xs:integer"/>

  <xsl:template name="generate-operands">
    <!-- Interleave XPath expressions and variable attributes in the
         sequence, following document order -->
    <xsl:for-each select="//bpel:condition | //bpel:joinCondition | //bpel:transitionCondition | //bpel:assign/bpel:copy/(bpel:from | bpel:to | bpel:from/bpel:query | bpel:to/bpel:query) | *[@variable or @inputVariable or @outputVariable]">

      <xsl:variable name="currentNode" select="."/>

      <!-- If the current element includes some variable attribute,
           and it can be replaced by at least one other variable, then
           list it as an operand. -->
      <xsl:for-each select="(@variable | @inputVariable | @outputVariable)[count(uca:listVariablesWithEquivalentType(., $currentNode)) &gt; 0]">
        <xsl:variable name="attributeNode" select="."/>
        <xsl:for-each select="uca:listVariablesWithEquivalentType(., $currentNode)">
          <xsl:sequence select="$currentNode"/>
          <xsl:element name="uca:replacementInfo">
            <xsl:element name="uca:attributeName">
              <xsl:value-of select="name($attributeNode)"/>
            </xsl:element>
            <xsl:element name="uca:newValue">
              <xsl:value-of select="."/>
            </xsl:element>
          </xsl:element>
        </xsl:for-each>
      </xsl:for-each>

      <!-- If the element did not include variable attributes, then it
           must have been selected because it could contain an XPath
           expression. Every occurence of a variable which can be
           replaced by another should be counted as a separate
           operand. -->
      <xsl:if test="not(@variable or @inputVariable or @outputVariable)">
        <xsl:variable name="expr" select="normalize-space(uca:concat-seq(text()))"/>
        <xsl:if test="$expr != ''">
          <xsl:variable name="xpathTree" select="conv:xpath2XML($expr)"/>

          <!-- The variable could be in the form 'M.P', where M is the
               message variable and P its part, according to the WSDL
               declarations. We will strip away '.P' using the
               built-in tokenize() function, and look whether there
               are any variables which can replace M. -->
          <xsl:for-each select="$xpathTree//uca:variableReference[count(uca:listVariablesWithEquivalentType(tokenize(uca:qname/@localPart, '\.')[1], $currentNode)) &gt; 0]">
            <xsl:variable name="xpathNode" select="."/>
            <xsl:for-each select="uca:listVariablesWithEquivalentType(tokenize(uca:qname/@localPart, '\.')[1], $currentNode)">
              <xsl:sequence select="$currentNode"/>
              <xsl:sequence select="$xpathTree/uca:expression"/>
              <xsl:sequence select="$xpathNode"/>
              <xsl:element name="uca:newValue">
                <xsl:value-of select="."/>
              </xsl:element>
            </xsl:for-each>
          </xsl:for-each>
        </xsl:if>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <!-- Reuse xpath-op-base.xsl for mutating XPath expressions -->
  <xsl:template match="*[not(@variable or @outputVariable or @inputVariable)]" mode="mutate-node">
    <xsl:param name="ast"/>
    <xsl:param name="extra"/>

    <xsl:apply-imports>
      <xsl:with-param name="ast" select="$ast"/>
      <xsl:with-param name="extra" select="$extra"/>
    </xsl:apply-imports>
  </xsl:template>

  <!-- Mutates variable attributes by changing their value -->
  <xsl:template match="*" mode="mutate-node">
    <xsl:param name="ast"/>
    <xsl:param name="extra"/>

    <xsl:copy>
      <xsl:copy-of select="@*[name(.) != string($extra/*[1])]"/>
      <xsl:attribute name="{string($extra/*[1])}">
        <xsl:value-of
            select="$extra/*[2]"/>
      </xsl:attribute>
      <xsl:copy-of select="*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="*" mode="mutate-ast-node">
    <xsl:param name="bpelNode"/>
    <xsl:param name="extra"/>

    <xsl:variable name="variableTokens"
                  select="tokenize(uca:qname/@localPart, '\.')"/>

    <!-- Keep the .part token, if any -->
    <xsl:variable name="messagePart" as="xs:string">
      <xsl:choose>
        <xsl:when test="count($variableTokens) = 2">
          <xsl:value-of select="concat('.', $variableTokens[2])"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:element name="uca:variableReference">
      <xsl:element name="uca:qname">
        <xsl:attribute name="localPart">
          <xsl:value-of select="concat(normalize-space($extra[2]/text()), $messagePart)"/>
        </xsl:attribute>
      </xsl:element>
    </xsl:element>
  </xsl:template>


  <!-- UTILITY FUNCTIONS -->

  <!-- Lists variables which can replace a variable. These variables
       are selected from the same scope the original variable was
       defined. -->
  <xsl:function name="uca:listVariablesWithEquivalentType" as="xs:string*">
    <xsl:param name="variableName" as="xs:string"/>
    <xsl:param name="currentNode"/>

    <!-- Go to the innermost scope from the current node where this variable is defined -->
    <xsl:for-each select="$currentNode/(ancestor-or-self::bpel:process|ancestor-or-self::bpel:scope)[bpel:variables/bpel:variable[@name=$variableName]][1]/bpel:variables">

      <xsl:variable name="variableNode"
                    select="bpel:variable[@name=$variableName]"/>

      <!-- Match types by name and kind (messageType with messageType and so on) -->
      <xsl:for-each select="bpel:variable[@name!=$variableName][not($variableNode/@messageType) or @messageType = $variableNode/@messageType][not($variableNode/@type) or @type = $variableNode/@type][not($variableNode/@element) or @element = $variableNode/@element]">

        <!-- Add an element to the sequence being generated with the variable name -->
        <xsl:sequence select="@name"/>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:function>
</xsl:stylesheet>
