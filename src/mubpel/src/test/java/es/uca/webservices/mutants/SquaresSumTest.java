package es.uca.webservices.mutants;

import org.junit.Test;

public class SquaresSumTest extends BPELMutationTestCase {

    private static final String TEST_SUITE = "squaresSum/squaresSum.bpts";

    public SquaresSumTest() {
        super("squaresSum/squaresSum.bpel");
    }

    /* AIS TESTS */

    @Test
    public void aisNumberOfOperands() throws Exception {
        assertOperandCount(1, getOp("AIS"));
    }

    @Test
    public void aisMutateMainLoopBodyScope() throws Exception {
        this.applyAndAssertEqualsXPath(getOp("AIS"), 1, 0,
                                       "//bpel:scope[1]/@isolated", "no");
    }

    @Test
    public void aisMutatedMainLoopBodyScopeIsDifferent() throws Exception {
        this.assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("AIS"), 1, 0);
    }

    /* AJC TESTS */

    @Test
    public void ajcOperandCount() throws Exception {
        assertOperandCount(0, getOp("AJC"));
    }

    /* EEU TESTS */

    @Test
    public void eeuOperandCount() throws Exception {
        assertOperandCount(0, getOp("EEU"));
    }
    
    /* EIU TESTS */
    
    @Test
    public void eiuOperandCount() throws Exception {
        assertOperandCount(4, getOp("EIU"));
    }
    
    @Test
    public void eiuAddSquare() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EIU"),
                                  2,
                                  1,
                                  "//bpel:assign[@name='SquareElement']/bpel:copy/bpel:from/text()",
                                  "-($i * $i)");
    }
    
    @Test
    public void eiuMutatesAddSquare() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EIU"), 2, 1);
    }
    
    /* EIN TESTS */
    
    @Test
    public void einOperandCount() throws Exception {
        assertOperandCount(0, getOp("EIN"));
    }
    
    /* EAP TESTS */
    
    @Test
    public void eapOperandCount() throws Exception {
        assertOperandCount(4, getOp("EAP"));
    }
    
    @Test
    public void eapAddIncrease() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EAP"),
                                  3,
                                  1,
                                  "//bpel:assign[@name='IncreaseSum']/bpel:copy/bpel:from/text()",
                                  "(($currentResult + $output.response/ns0:sum) * " +
                                  "((($currentResult + $output.response/ns0:sum) >= 0.0) - " +
                                  "(($currentResult + $output.response/ns0:sum) < 0.0)))");
    }
    
    @Test
    public void eapMutatesAddIncrease() throws Exception {
    	assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("EAP"), 2, 1);
    }
    
    /* EAN TESTS */
    
    @Test
    public void eanOperandCount() throws Exception {
        assertOperandCount(4, getOp("EAN"));
    }
    
    @Test
    public void eanAddResult() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EAN"),
                                  4,
                                  1,
                                  "//bpel:assign[@name='AddResult']/bpel:copy[1]/bpel:from/text()",
                                  "-(($counter + 1.0) * ((($counter + 1.0) >= 0.0) - (($counter + 1.0) < 0.0)))");
    }
    
    @Test
    public void eapMutatesAddResult() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EAN"), 4, 1);
    }
    
    /* CFA TESTS */
    
    @Test
    public void cfaOperandCount() throws Exception {
        assertOperandCount(8, getOp("CFA"));
    }
    
    @Test
    public void cfaChangeReply() throws Exception {
    	applyAndAssertEqualsXPath(
                getOp("CFA"),
                8,
                1,
                "count(//bpel:forEach[@name='GenerateResults']/following-sibling::bpel:exit)",
                "1");
    }
    
    @Test
    public void cfaMutatesChangeReplyIsInvalid() throws Exception {
    	assertMutantIsInvalid(TEST_SUITE, getOp("CFA"), 8, 1);
    }
    
    /* CDE TESTS */
    
    @Test
    public void cdeOperandCount() throws Exception {
        assertOperandCount(0, getOp("CDE"));
    }
    
    /* CCO TESTS */
    
    @Test
    public void ccoOperandCount() throws Exception {
        assertOperandCount(0, getOp("CCO"));
    }
    
    /* CDC TESTS */
    
    @Test
    public void cdcOperandCount() throws Exception {
        assertOperandCount(0, getOp("CDC"));
    }
    
}
