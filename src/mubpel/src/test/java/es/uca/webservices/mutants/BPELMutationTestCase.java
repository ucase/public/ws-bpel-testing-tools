package es.uca.webservices.mutants;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import net.bpelunit.framework.exception.DeploymentException;
import net.bpelunit.framework.model.test.report.ArtefactStatus.StatusCode;

import org.activebpel.rt.jetty.AeJettyForker;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.InvalidProcessException;
import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;
import es.uca.webservices.bpel.bpelunit.BPELUnitXMLResult;
import es.uca.webservices.bpel.bpelunit.CustomizedRunner;
import es.uca.webservices.bpel.bpelunit.ExecutionResults;
import es.uca.webservices.bpel.bpelunit.IncompatibleResultsException;
import es.uca.webservices.bpel.bpelunit.MissingResultsException;
import es.uca.webservices.bpel.bpelunit.AbortOnFirstDifferenceListener.AbortOnDiff;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.bpel.xml.XMLDocument;
import es.uca.webservices.mutants.operators.Operator;
import es.uca.webservices.mutants.operators.OperatorFactory;
import es.uca.webservices.mutants.operators.TestOperatorFactory;
import es.uca.webservices.mutants.operators.TestXSLTStylesheet;
import es.uca.webservices.mutants.operators.XSLTBackedOperator;

public abstract class BPELMutationTestCase {

    private static final String     MUTATEDBPEL_FILENAME = "__mutated__.bpel";
    private BPELProcessDefinition bpelProcess;
    private Document              bpelDocument;
    final private OperatorFactory   opFactory = new TestOperatorFactory();
    final private Map<String, Operator> operators = new HashMap<String, Operator>();
    final private String            pathToBPELProcessDefinition;

    // AeJettyForker for starting and stopping ActiveBPEL when necessary
    private static AeJettyForker fRunner;

	private static final Map<String, BPELUnitXMLResult> originalProcessResults
      = new HashMap<String, BPELUnitXMLResult>();

    public BPELMutationTestCase(String pathToBPELProcessDefinition) {
        this.pathToBPELProcessDefinition = pathToBPELProcessDefinition;
    }

    @BeforeClass
    public static void classSetUp() throws Exception {
    	// We'll use a different temporary folder each time, to ensure each
    	// test class is isolated from the rest
    	fRunner = new AeJettyForker(null, CustomizedRunner.DEFAULT_ENGINE_PORT - 1, "none");
    	fRunner.start();
    }

    @AfterClass
    public static void classTearDown() throws Exception {
    	fRunner.stop();
    	fRunner.join();
    }

    @Before
    public void setUp() throws ParserConfigurationException, SAXException,
            IOException, XPathExpressionException, InvalidProcessException {
        final URL resource = getClass().getResource("/" + pathToBPELProcessDefinition);
        assertNotNull("The BPEL process " + pathToBPELProcessDefinition + " should exist", resource);
        bpelProcess = new BPELProcessDefinition(new File(resource.getFile().replace("%20", " ")));
        bpelDocument = bpelProcess.getDocument();
    }

    protected Operator getOp(final String name) {
        if (!operators.containsKey(name)) {
            operators.put(name, opFactory.newOperator(name));
        }
        return operators.get(name);
    }

    protected void assertNoMessagesReported(Operator op) {
      TestXSLTStylesheet testStylesheet = getTestStylesheetFromOperator(op);
      assertEquals("Operator should not report any errors", 0, testStylesheet.getErrors().size());
      assertEquals("Operator should not report any fatal errors", 0, testStylesheet.getFatalErrors().size());
      assertEquals("Operator should not report any warnings", 0, testStylesheet.getWarnings().size());
    }

    protected void assertWarningCount(Operator op, final int count) {
      TestXSLTStylesheet testStylesheet = getTestStylesheetFromOperator(op);
      assertEquals(count, testStylesheet.getWarnings().size());
    }

    protected void applyAndAssertEqualsXPath(Operator op, int operand,
            int attribute, String xpathExpr, String expectedValue)
            throws Exception {
        applyAndAssertEqualsXPath(op, operand, attribute, xpathExpr,
                                  expectedValue, null);
    }

    protected void applyAndAssertEqualsXPath(Operator op, int operand,
            int attribute, String xpathExpr, String expectedValue, String msg)
            throws Exception {
        Document result = applyOperator(op, operand, attribute);
        assertNoMessagesReported(op);
        assertEqualsXPathBPEL(result, xpathExpr, expectedValue, msg);
    }

    protected Document applyOperator(Operator op, int operand, int attribute)
            throws Exception {
        return (Document) op.apply(bpelDocument, operand, attribute);
    }

    protected void assertMaximumDistinctAttributeValueIs(Operator op, int i)
            throws Exception {
        assertEquals(i, op.getMaximumDistinctAttributeValue(bpelDocument));
    }

    protected BPELUnitXMLResult getOriginalProcessResults(
            BPELUnitSpecification testSpec) throws Exception {

        // Cache results from the same BPTS for faster test execution
        final String canonicalSpecPath = testSpec.getFile().getCanonicalPath();
        if (originalProcessResults.containsKey(canonicalSpecPath)) {
            return originalProcessResults.get(canonicalSpecPath);
        }

        final ExecutionResults results = testSpec.run(bpelProcess);
        assertValidTestTimes(results);

		BPELUnitXMLResult originalResult = new BPELUnitXMLResult(results.getTestReportFile());
        assertEquals(
                "Original WS-BPEL process should always pass all tests",
                StatusCode.PASSED.name(), originalResult.getGlobalStatusCode());
        originalProcessResults.put(canonicalSpecPath, originalResult);
        return originalResult;
    }

	protected BPELUnitXMLResult getMutantProcessResults(
            BPELUnitSpecification testSpec, Operator op, int operand,
            int attribute) throws Exception {
        BPELProcessDefinition defMutated = generateMutant(op, operand, attribute);

        // Run it using the specified test suite
        try {
            final ExecutionResults results = testSpec.run(defMutated);
            assertValidTestTimes(results);

            final int testCaseCount = results.getTestCaseCount();
            final File xmlResultFile = results.getTestReportFile();
            final BPELUnitXMLResult mutatedResult = new BPELUnitXMLResult(xmlResultFile);

            assertValidProcessStatusCode(
                "Mutated WS-BPEL process should either fail or pass",
                mutatedResult);
            assertEquals(
                "The number of replies should be equal to the number of test cases",
                testCaseCount,
                mutatedResult.getCompositionOutboundMessages().size());

            return mutatedResult;
        }
        finally {
            defMutated.getFile().delete();
        }
    }

    private BPELProcessDefinition generateMutant(Operator op, int operand, int attribute)
            throws Exception, TransformerException, FileNotFoundException {
        Document mutatedDoc = applyOperator(op, operand, attribute);
        File fMutated = new File(MUTATEDBPEL_FILENAME);
        new XMLDocument(mutatedDoc).dumpToStream(new FileOutputStream(fMutated));
        return new BPELProcessDefinition(fMutated, bpelProcess);
    }

    private void assertValidProcessStatusCode(String msg,
            BPELUnitXMLResult result) throws XPathExpressionException {
        String status = result.getGlobalStatusCode();
        assertTrue(msg + ": got '" + status + "'",
                StatusCode.PASSED.name().equals(status)
                || StatusCode.FAILED.name().equals(status)
                || StatusCode.ERROR.name().equals(status));
    }

    private void assertValidTestTimes(final ExecutionResults results) {
		assertNotNull("Test times should have been initialized", results.getTestWallNanos());
	    assertEquals("There should be as many test times as executed test cases", results.getTestCasesRun(), results.getTestWallNanos().size());
		for (long time : results.getTestWallNanos()) {
			assertTrue("Test times should be positive nonzero numbers", time > 0);
		}
	}

	protected void assertSuccessfulMutationIsEquivalent(String testSuitePath,
            Operator op, int operand, int attribute) throws Exception {
        assertMutationEquivalence(testSuitePath, op, operand, attribute, true, true);
        assertNoMessagesReported(op);
    }

    protected void assertSuccessfulMutationIsDifferent(String testSuitePath, Operator op,
            int operand, int attribute) throws Exception {
        assertMutationEquivalence(testSuitePath, op, operand, attribute, false, true);
        assertNoMessagesReported(op);
    }

    protected void assertMutantIsInvalid(String testSuitePath, Operator op,
            int operand, int attribute) throws Exception {
        assertMutationEquivalence(testSuitePath, op, operand, attribute, false, false);
        assertNoMessagesReported(op);
    }

    protected int getAttributeOffsetIgnoringCurrent(int current, int desired) {
        assert current != desired;
        return current < desired ? desired - 1 : desired;
    }

    /**
     * Asserts that the mutation operator <code>op</code> returns a list with
     * <code>nOperands</code> operands after analyzing the current WS-BPEL
     * document.
     *
     * @param nOperands
     *            Expected number of operands.
     * @param op
     *            Mutation operator to be used for analysis.
     * @throws Exception
     *             Error while performing the analysis.
     */
    protected void assertOperandCount(final int nOperands, final Operator op)
            throws Exception {
        assertEquals(nOperands, op.getOperandCount(bpelDocument));
    }

    protected void assertMutationEquivalence(String testSuitePath, Operator op,
            int operand, int attribute, boolean shouldBeEqual, boolean mutantShouldBeValid) throws Exception {
        BPELUnitSpecification testSpec = getTestSpecification(testSuitePath);
        BPELUnitXMLResult originalResult = getOriginalProcessResults(testSpec);

        try {
            final BPELProcessDefinition fMutated = generateMutant(op, operand, attribute);
            final Pair<ExecutionResults, Set<Integer>> results = testSpec.run(AbortOnDiff.SUITE, fMutated, originalResult);
            assertValidTestTimes(results.getLeft());

			final Set<Integer> differentTestCasePos = results.getRight();
            assertEquals("Comparison results should match expectations", shouldBeEqual, differentTestCasePos.isEmpty());
            if (!mutantShouldBeValid) {
                fail("Mutant was expected to be invalid and is valid");
            }
        } catch (DeploymentException ex) {
            if (mutantShouldBeValid) {
                fail("Mutant was expected to be valid and is invalid: " + ex.getLocalizedMessage());
            }
        } finally {
        	originalResult.getFile().delete();
        }
    }

	protected BPELUnitSpecification getTestSpecification(String testSuitePath)
			throws ParserConfigurationException, SAXException, IOException {
		BPELUnitSpecification testSpec
            = new BPELUnitSpecification(
                new File(getClass().getResource("/" + testSuitePath).getFile().replace("%20", " ")));
        testSpec.setDeploymentDirectory(fRunner.getBPRDirectory());
        testSpec.setEnginePort(fRunner.getPort());

        // Use a different port than default (7777), for testing
        testSpec.setBPELUnitPort(CustomizedRunner.DEFAULT_BPELUNIT_PORT+1);

		return testSpec;
	}

    private void assertEqualsXPathBPEL(Document doc, String expr,
            String expected, String msg) throws XPathExpressionException,
            TransformerException, InvalidProcessException {
        BPELProcessDefinition processDefinition = new BPELProcessDefinition(doc);
        String obtained = processDefinition.evaluateExpression(expr).trim();
        if (msg != null) {
            assertEquals(msg, expected, obtained);
        }
        else {
            assertEquals(expected, obtained);
        }
    }

  private TestXSLTStylesheet getTestStylesheetFromOperator(Operator op) {
    try {
      XSLTBackedOperator xsltOp
          = (XSLTBackedOperator)op;
      return (TestXSLTStylesheet)xsltOp.getXSLTStylesheet();
    } catch (ClassCastException ex) {
      fail("Operator received is a dummy operator or is not configured to collect messages");
      throw ex;
    }
  }

	protected void compareAllResults(BPELUnitSpecification testSpecification,
			BPELUnitXMLResult resOriginal, Operator op, int operand,
			int attribute, boolean bMustBeSame) throws Exception,
			XPathExpressionException, IncompatibleResultsException,
			MissingResultsException {
		BPELUnitXMLResult resMutant = getMutantProcessResults(
				testSpecification, op, operand, attribute);
		final boolean assertionPassed = resOriginal.hasSameResultsAs(resMutant, null) == bMustBeSame;
		resMutant.getFile().delete();

		assertTrue(String.format(
			"Original and mutant (%s,%d,%d) %s have the same results in every test",
			op.getName(), operand, attribute, bMustBeSame ? "must"	: "must not"),
			assertionPassed);
	}

}
