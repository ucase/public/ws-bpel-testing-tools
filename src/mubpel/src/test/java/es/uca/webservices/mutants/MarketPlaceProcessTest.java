package es.uca.webservices.mutants;

import org.junit.Test;

public class MarketPlaceProcessTest extends BPELMutationTestCase {

    public MarketPlaceProcessTest() {
        super("marketPlace/marketplace.bpel");
    }

    /* ACI TESTS */

    @Test
    public void aciOperandCount() throws Exception {
        assertOperandCount(0, getOp("ACI"));
    }

    /* AFP TESTS */

    @Test
    public void afpOperandCount() throws Exception {
        assertOperandCount(0, getOp("AFP"));
    }

    /* AIS TESTS */

    @Test
    public void aisNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("AIS"));
    }

    /* AJC TESTS */

    @Test
    public void ajcOperandCount() throws Exception {
        assertOperandCount(0, getOp("AJC"));
    }

    /* EEU TESTS */

    @Test
    public void eeuOperandCount() throws Exception {
        assertOperandCount(0, getOp("EEU"));
    }
}
