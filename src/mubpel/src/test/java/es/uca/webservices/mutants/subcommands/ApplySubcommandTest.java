package es.uca.webservices.mutants.subcommands;

import es.uca.webservices.bpel.deploy.ZippedDeploymentArchive;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Tests for the <code>apply</code> subcommand.
 */
public class ApplySubcommandTest {

    @Test
    public void bpelTest() throws Exception {
        final ApplySubcommand cmd = new ApplySubcommand();
        cmd.setOutputStream(new ByteArrayOutputStream());
        cmd.parseArgs("src/test/resources/loanRPC/loanRPC.bpel", "ERR", "1", "1");
        cmd.runCommand();
    }

    @Test
    public void bprTest() throws Exception {
        final ApplySubcommand cmd = new ApplySubcommand();
        final ByteArrayOutputStream bOS = new ByteArrayOutputStream();
        cmd.setOutputStream(bOS);
        cmd.parseArgs("src/test/resources/simplebpel.bpr", "CFA", "1", "1");
        cmd.runCommand();

        final File result = new File(bOS.toString());
        final ZippedDeploymentArchive bpr = new ZippedDeploymentArchive(result);
        assertEquals("simplebpel", bpr.getMainBPEL().getName().getLocalPart());
        result.delete();
    }

}
