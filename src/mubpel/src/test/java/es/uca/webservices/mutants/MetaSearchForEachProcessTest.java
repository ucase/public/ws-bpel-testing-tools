package es.uca.webservices.mutants;

import org.junit.Test;

public class MetaSearchForEachProcessTest extends BPELMutationTestCase {

    final private static String TEST_SUITE = "metaSearch/MetaSearchTest.bpts";

    public MetaSearchForEachProcessTest() {
        super("metaSearch/MetaSearchBPEL2_forEach.bpel");
    }

    /* AFP TESTS */

    @Test
    public void afpOperandCount() throws Exception {
        assertOperandCount(2, getOp("AFP"));
    }

    @Test
    public void afpMutateFirstForEach() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("AFP"),
                                  1,
                                  0,
                                  "//bpel:forEach[@name='InterleaveResults']/@parallel",
                                  "yes");
        applyAndAssertEqualsXPath(
                                  getOp("AFP"),
                                  1,
                                  0,
                                  "//bpel:forEach[@name='ScanResultsForMatch']/@parallel",
                                  "no");
    }

    @Test
    public void afpMutateSecondForEach() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("AFP"),
                                  2,
                                  0,
                                  "//bpel:forEach[@name='InterleaveResults']/@parallel",
                                  "no");
        applyAndAssertEqualsXPath(
                                  getOp("AFP"),
                                  2,
                                  0,
                                  "//bpel:forEach[@name='ScanResultsForMatch']/@parallel",
                                  "yes");
    }

    @Test
    public void afpParallelizedFirstForEachShouldNotWork() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("AFP"), 1, 0);
    }

    @Test
    public void afpParallelizedSecondForEachShouldWork() throws Exception {
        assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("AFP"), 2, 0);
    }

    /* AIS TESTS */

    @Test
    public void aisNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("AIS"));
    }

    /* AJC TESTS */

    @Test
    public void ajcOperandCount() throws Exception {
        assertOperandCount(0, getOp("AJC"));
    }

    /* EEU TESTS */

    @Test
    public void eeuOperandCount() throws Exception {
        assertOperandCount(2, getOp("EEU"));
    }

    @Test
    public void eeuMutateForEachFinalCounterFirstCalculation() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EEU"),
                                  1,
                                  0,
                                  "//bpel:if[@name='GoogleHasMoreResults']//bpel:from[1]",
                                  "(1.0 + (2.0 * $maxGoogle))");
    }

    @Test
    public void eeuWrongMSNFinalCounterCalculationMutantIsEquivalent()
            throws Exception {
        assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("EEU"), 2, 0);
    }
}
