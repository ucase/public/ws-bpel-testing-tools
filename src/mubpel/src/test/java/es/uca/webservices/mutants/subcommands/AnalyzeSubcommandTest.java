package es.uca.webservices.mutants.subcommands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.mutants.operators.OperatorConstants;

/**
 * Tests for the "analyze" subcommand.
 */
public class AnalyzeSubcommandTest {

	@Test
	public void smokeTest() throws Exception {
		AnalyzeSubcommand cmd = new AnalyzeSubcommand();
		Map<String, Pair<Integer, Integer>> results
			= cmd.analyze("src/test/resources/loanRPC/loanRPC.bpel");
        checkAnalysisConsistency(results);
	}

    @Test
    public void bprTest() throws Exception {
        final AnalyzeSubcommand cmd = new AnalyzeSubcommand();
        final Map<String, Pair<Integer, Integer>> results = cmd.analyze("src/test/resources/simplebpel.bpr");
        checkAnalysisConsistency(results);
    }

    private void checkAnalysisConsistency(Map<String, Pair<Integer, Integer>> results) {
        final List<String> lOpNames = Arrays.asList(OperatorConstants.OPERATOR_NAMES);
        assertEquals(lOpNames.size(), results.size());
        for (Entry<String, Pair<Integer, Integer>> e : results.entrySet()) {
            assertTrue(lOpNames.contains(e.getKey()));
            assertTrue(e.getValue().getLeft() >= 0);
            assertTrue(e.getValue().getRight() >= 1);
        }
    }
}
