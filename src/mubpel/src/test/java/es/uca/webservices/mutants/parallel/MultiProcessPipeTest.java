package es.uca.webservices.mutants.parallel;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.junit.Test;

import es.uca.webservices.mutants.subprocesses.ProcessUtils;

/**
 * Tests for the MultiProcessPipe class.
 */
public class MultiProcessPipeTest {

	@Test
	public void singleProcessEmptyInput() throws Exception {
		assertPipeWorks(1, 0, 0);
	}

	@Test
	public void singleProcessSingleLine() throws Exception {
		assertPipeWorks(1, 1, 0);
	}

	@Test
	public void singleProcessLongInput() throws Exception {
		assertPipeWorks(1, 2000, 0);
	}

	@Test
	public void twoProcessesEmptyInput() throws Exception {
		assertPipeWorks(2, 0, 0);
	}

	@Test
	public void twoProcessesSingleLine() throws Exception {
		assertPipeWorks(2, 1, 0);
	}

	@Test
	public void twoProcessesLongInput() throws Exception {
		assertPipeWorks(2, 2000, 0);
	}

	@Test
	public void eightProcessesEmptyInput() throws Exception {
		assertPipeWorks(8, 0, 0);
	}

	@Test
	public void eightProcessesSingleLine() throws Exception {
		assertPipeWorks(8, 1, 0);
	}

	@Test
	public void eightProcessesLongInput() throws Exception {
		assertPipeWorks(8, 2000, 0);
	}

	@Test
	public void eightProcessesLongInputRandomDelay() throws Exception {
		assertPipeWorks(8, 1000, 50);
	}

	private void assertPipeWorks(final int nProcesses, final int nLines, int maxDelayMillis)
		throws IOException, InterruptedException, ExecutionException
	{
		// Create the pipe
		List<List<String>> args = new ArrayList<List<String>>(nProcesses);
		for (int i = 0; i < nProcesses; ++i) {
			args.add(ProcessUtils.callJavaClassArgs(getClass(), "" + (i + 1)
					* 5));
		}
		MultiProcessPipe mp = new MultiProcessPipe(System.err, args, MultiProcessPipe.REGULAR_THREADS);

		// Use a *really* short termination timeout, to detect a bug
		// in the class regarding proper closing of each side of the pipe.
		mp.setTerminationTimeout(1);

		// Write from 0 to lines - 1
		for (int i = 0; i < nLines; ++i) {
			mp.writeLine(Integer.toString(i));
		}
		// Tell the pipe that we are done writing
		mp.doneWriting();

		// Read all available lines, checking their order and number
		int iLineCount = 0;
		String line;
		while ((line = mp.readLine()) != null) {
			assertEquals("processed " + Integer.toString(iLineCount++), line);
		}
		assertEquals(nLines, iLineCount);
		mp.doneReading();
	}

	/**
	 * Simple program which maps each line 'X' with 'processed X', to test this
	 * multiprocess pipe. Random delays can be introduced if the first argument
	 * is an integer greater than 0. The random seed is always the same.
	 *
	 * @throws IOException
	 *             I/O error when printing a new line.
	 * @throws InterruptedException
	 *            The process was interrupted while processing a line.
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		int maxDelay = Integer.valueOf(args[0]);
		Random rndGenerator = new Random(0);

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		while ((line = reader.readLine()) != null) {
			System.out.println("processed " + line);
			if (maxDelay > 0) {
				Thread.sleep(rndGenerator.nextInt(maxDelay));
			}
		}
	}
}
