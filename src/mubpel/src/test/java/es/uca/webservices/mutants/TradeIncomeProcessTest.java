package es.uca.webservices.mutants;

import org.junit.Test;

public class TradeIncomeProcessTest extends BPELMutationTestCase {

    private static final String TEST_SUITE = "tradeIncome/tradeIncome-templates.bpts";

    public TradeIncomeProcessTest() {
        super("tradeIncome/tradeIncome.bpel");
    }

    @Test
    public void isv_1_1_1_isDifferent() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ISV"), 1, 1);
    }
}
