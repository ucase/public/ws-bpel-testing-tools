package es.uca.webservices.mutants;


import org.junit.Test;

public class LoanApprovalProcessCoverageTest extends BPELMutationTestCase {
	
	private static final String TEST_SUITE = "loanCoverage/LoanApprovalProcess.bpts";

	public LoanApprovalProcessCoverageTest() {
		super("loanCoverage/LoanApprovalProcess.bpel");
	}
		
	/* EIU TESTS */
    
    @Test
    public void eiuOperandCount() throws Exception {
    	assertOperandCount(7, getOp("EIU"));
    }
    
    @Test
    public void eiuChangeIf1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EIU"),
                                  2,
                                  1,
                                  "//bpel:if[@name='If1']/bpel:condition/text()",
                                  "((number(string($processInput.input/ns0:amount)) <= -10000.0) and " +
                                  "(number(string($processInput.input/ns0:amount)) >= 1000.0))");
    }
    
    @Test
    public void eiuMutatesIf1Condition() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EIU"), 2, 1);
    }
    
    /* EIN TESTS */
    
    @Test
    public void einOperandCount() throws Exception {
        assertOperandCount(4, getOp("EIN"));
    }
    
    @Test
    public void einChangeIf1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EIN"),
                                  2,
                                  1,
                                  "//bpel:if[@name='If2']/bpel:condition/text()",
                                  "not((string($assessorOutput.output/ns0:risk) = 'high'))");
    }
    
    @Test
    public void einMutatesIf1Condition() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EIN"), 2, 1);
    }
    
    /* EAP TESTS */
    
    @Test
    public void eapOperandCount() throws Exception {
    	assertOperandCount(7, getOp("EAP"));
    }
    
    @Test
    public void eapChangeIf1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EAP"),
                                  4,
                                  1,
                                  "//bpel:if[@name='If1']/bpel:condition/text()",
                                  "((number(string($processInput.input/ns0:amount)) <= 10000.0) and " +
                                  "(number(string($processInput.input/ns0:amount)) >= (1000.0 * " +
                                  "((1000.0 >= 0.0) - (1000.0 < 0.0)))))");
    }
    
    @Test
    public void eapMutatesIf1Condition() throws Exception {
    	assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("EAP"), 4, 1);
    }
    
    /* EAN TESTS */
    
    @Test
    public void eanOperandCount() throws Exception {
    	assertOperandCount(7, getOp("EAN"));
    }
    
    @Test
    public void eanChangeIf1() throws Exception {
    	applyAndAssertEqualsXPath(
                getOp("EAN"),
                4,
                1,
                "//bpel:if[@name='If1']/bpel:condition/text()",
                "((number(string($processInput.input/ns0:amount)) <= 10000.0) and " +
                "(number(string($processInput.input/ns0:amount)) >= -(1000.0 * " +
                "((1000.0 >= 0.0) - (1000.0 < 0.0)))))");
    }
    
    @Test
    public void eanMutatesIf1Condition() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EAN"), 4, 1);
    }
    
    /* CFA TESTS */
    
    @Test
    public void cfaOperandCount() throws Exception {
    	assertOperandCount(28, getOp("CFA"));
    }
    
    @Test
    public void cfaChangeSmallAmount() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CFA"),
                                  3,
                                  1,
                                  "count(//bpel:sequence[@name='SmallAmount']) = 0 and " +
                                  "count(//bpel:if[@name='If1']/bpel:exit) = 1",
                                  "true");
    }
    
    @Test
    public void cfaMutatesSequenceSmallAmount() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CFA"), 3, 1);
    }
    
    /* CDE TESTS */
    
    @Test
    public void cdeOperandCount() throws Exception {
    	assertOperandCount(4, getOp("CDE"));
    }
    
    @Test
    public void cdeAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("CDE"), 2);
    }
    
    @Test
    public void cdeChangeIf2False() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDE"),
                                  2,
                                  2,
                                  "//bpel:if[@name='If2']/bpel:condition/text()",
                                  "false()");
    }
    
    @Test
    public void cdeMutatesIf2False() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDE"), 2, 2);
    }
    
    /* CCO TESTS */
    
    @Test
    public void ccoOperandCount() throws Exception {
    	assertOperandCount(5, getOp("CCO"));
    }
    
    @Test
    public void ccoAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("CCO"), 2);
    }
    
    @Test
    public void ccoChangeIf2False() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CCO"),
                                  2,
                                  2,
                                  "//bpel:if[@name='If1']/bpel:condition/text()",
                                  "((number(string($processInput.input/ns0:amount)) <= 10000.0) and false())");
    }
    
    @Test
    public void ccoMutatesIf2False() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CCO"), 2, 2);
    }
    
    /* CDC TESTS */
    
    @Test
    public void cdcOperandCount() throws Exception {
    	assertOperandCount(6, getOp("CDC"));
    }
    
    @Test
    public void cdcAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("CDC"), 2);
    }
    
    @Test
    public void cdcChangeIf2False() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDC"),
                                  2,
                                  1,
                                  "//bpel:if[@name='If1']/bpel:condition/text()",
                                  "(true() and (number(string($processInput.input/ns0:amount)) >= 1000.0))");
    }
    
    @Test
    public void cdcMutatesIf2False() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDC"), 2, 1);
    }
    
	
}
