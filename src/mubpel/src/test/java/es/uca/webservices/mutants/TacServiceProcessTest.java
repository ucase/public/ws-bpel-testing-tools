package es.uca.webservices.mutants;

import org.junit.Test;

public class TacServiceProcessTest extends BPELMutationTestCase {

    private static final String TEST_SUITE = "tacService/tacService.bpts";

    public TacServiceProcessTest() {
        super("tacService/tacService.bpel");
    }

    /* EEU TESTS */

    @Test
    public void eeuOperandCount() throws Exception {
        assertOperandCount(1, getOp("EEU"));
    }

    @Test
    public void eeuMutateCounterDecrement() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EEU"),
                                  1,
                                  0,
                                  "//bpel:assign[@name='Assign1']/bpel:copy[2]/bpel:from",
                                  "(1.0 + $counter)");
    }

    @Test
    public void eeuMutantWithCounterIncrementIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EEU"), 1, 0);
    }

    /* AEL TESTS */

    @Test
    public void aelOperandCount() throws Exception {
        assertOperandCount(4, getOp("AEL"));
    }

    @Test
    public void aelDeleteInitialization() throws Exception {
        applyAndAssertEqualsXPath(getOp("AEL"), 1, 0,
                        "count(//bpel:sequence/bpel:assign)",
                        "0");
        applyAndAssertEqualsXPath(getOp("AEL"), 1, 0,
                        "count(//bpel:while)",
                        "1");
        applyAndAssertEqualsXPath(getOp("AEL"), 1, 0,
                        "count(//bpel:while/bpel:assign)",
                        "1");
    }

    @Test
    public void aelDeleteInitializationIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("AEL"), 1, 0);
    }

    @Test
    public void aelRemoveWhileLoopBody() throws Exception {
        applyAndAssertEqualsXPath(getOp("AEL"), 3, 0,
                        "count(//bpel:while/bpel:assign)", "0");
        applyAndAssertEqualsXPath(getOp("AEL"), 3, 0,
                        "count(//bpel:while/bpel:empty)", "1");
    }

    @Test
    public void aelRemoveWhileLoopBodyIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("AEL"), 3, 0);
    }
    
    /* EIU TESTS */
    
    @Test
    public void eiuOperandCount() throws Exception {
    	assertOperandCount(2, getOp("EIU"));
    }
    
    @Test
    public void eiuChangeAssign1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EIU"),
                                  2,
                                  1,
                                  "//bpel:assign[@name='Assign1']/bpel:copy[2]/bpel:from/text()",
                                  "-(-1.0 + $counter)");
    }
    
    @Test
    public void eiuMutatesAsign1() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EIU"), 2, 1);
    }
    
    /* EIN TESTS */
    
    @Test
    public void einOperandCount() throws Exception {
    	assertOperandCount(1, getOp("EIN"));
    }
    
    @Test
    public void einChangeWhile1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EIN"),
                                  1,
                                  1,
                                  "//bpel:while[@name='While1']/bpel:condition/text()",
                                  "not(($counter >= 1.0))");
    }
    
    @Test
    public void einMutatesWhile1() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EIN"), 1, 1);
    }
    
    /* EAP TESTS */
    
    @Test
    public void eapOperandCount() throws Exception {
    	assertOperandCount(2, getOp("EAP"));
    }
    
    @Test
    public void eapChangeAssign1() throws Exception {
    	applyAndAssertEqualsXPath(
    							  getOp("EAP"),
    							  2,
    							  1,
    							  "//bpel:assign[@name='Assign1']/bpel:copy[2]/bpel:from/text()",
                				  "((-1.0 + $counter) * (((-1.0 + $counter) >= 0.0) - ((-1.0 + $counter) < 0.0)))");
    }
    
    @Test
    public void eapMutatesAssign1() throws Exception {
    	assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("EAP"), 2, 1);
    }
    
    /* EAN TESTS */
    
    @Test
    public void eanOperandCount() throws Exception {
    	assertOperandCount(2, getOp("EAN"));
    }
    
    @Test
    public void eanChangeAssign1() throws Exception {
    	applyAndAssertEqualsXPath(
    							  getOp("EAN"),
    							  2,
    							  1,
    							  "//bpel:assign[@name='Assign1']/bpel:copy[2]/bpel:from/text()",
                				  "-((-1.0 + $counter) * (((-1.0 + $counter) >= 0.0) - ((-1.0 + $counter) < 0.0)))");
    }
    
    @Test
    public void eanMutatesAssign1() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EAN"), 2, 1);
    }
    
    /* CFA TESTS */
    
    @Test
    public void cfaOperandCount() throws Exception {
    	assertOperandCount(4, getOp("CFA"));
    }
    
    @Test
    public void cfaChangeInitializeCounter() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CFA"),
                                  1,
                                  1,
                                  "count(//bpel:assign[@name='InitializeCounter']) = 0 and " +
                                  "count(/bpel:process/bpel:sequence/bpel:exit) = 1",
                                  "true");
    }
    
    @Test
    public void cfaMutatesAssign1() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CFA"), 1, 1);
    }
    
    /* CDE TESTS */
    
    @Test
    public void cdeOperandCount() throws Exception {
    	assertOperandCount(1, getOp("CDE"));
    }
    
    @Test
    public void cdeChangeWhile1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDE"),
                                  1,
                                  1,
                                  "//bpel:while[@name='While1']/bpel:condition/text()",
                                  "true()");
    }
    
    @Test
    public void cdeMutatesWhile1() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDE"), 1, 1);
    }
    
    /* CCO TESTS */
    
    @Test
    public void ccoOperandCount() throws Exception {
    	assertOperandCount(1, getOp("CCO"));
    }
    
    @Test
    public void ccoChangeWhile1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CCO"),
                                  1,
                                  2,
                                  "//bpel:while[@name='While1']/bpel:condition/text()",
                                  "false()");
    }
    
    @Test
    public void ccoMutatesWhile1() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CCO"), 1, 1);
    }
    
    /* CDC TESTS */
    
    @Test
    public void cdcOperandCount() throws Exception {
    	assertOperandCount(1, getOp("CDC"));
    }
    
    @Test
    public void cdcChangeWhile1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDC"),
                                  1,
                                  1,
                                  "//bpel:while[@name='While1']/bpel:condition/text()",
                                  "true()");
    }
    
    @Test
    public void cdcMutatesWhile1() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDC"), 1, 1);
    }
}
