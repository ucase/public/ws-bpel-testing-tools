package es.uca.webservices.mutants.subcommands;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

import javax.xml.namespace.QName;

import org.junit.Test;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.mutants.CLIRunner;

/**
 * Test for the 'normalize' subcommand, which also serves as a smoke test
 * for the command-line launcher.
 * 
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class NormalizeSubcommandTest {

	private static final String PROCESS_NAME = "loanApprovalProcess";
	private static final String NORMALIZED_BPEL = "src/test/resources/loanRPC/loanRPC.bpel";

	@Test
	public void normalizeSingleBPEL() throws Exception {
		// Run "normalize (bpel)", overriding current stdout
		PrintStream oldOut = System.out;
		try {
			final File fBPEL = new File("output.bpel");
			fBPEL.deleteOnExit();
			FileOutputStream fOS = new FileOutputStream(fBPEL);
			final PrintStream newOut = new PrintStream(fOS);
			System.setOut(newOut);
			CLIRunner.main(new String[] { "normalize", NORMALIZED_BPEL });
			newOut.close();

			// Standard output should be a valid BPEL definition
			BPELProcessDefinition bpelProc = new BPELProcessDefinition(fBPEL);
			QName bpelName = bpelProc.getName();
			assertEquals(PROCESS_NAME, bpelName.getLocalPart());
		} finally {
			System.setOut(oldOut);
		}
	}
}
