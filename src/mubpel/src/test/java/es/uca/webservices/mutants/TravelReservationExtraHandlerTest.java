package es.uca.webservices.mutants;

import org.junit.Test;

import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;
import es.uca.webservices.bpel.bpelunit.BPELUnitXMLResult;

public class TravelReservationExtraHandlerTest extends BPELMutationTestCase {

	private static final String TEST_SUITE = "travelReservationServiceExtraHandler/travelTestExtended.bpts";

	public TravelReservationExtraHandlerTest() {
		super("travelReservationServiceExtraHandler/TravelReservationService.bpel");
	}

	@Test
	public void sameResultsWhenRepeatingMutant() throws Exception {
		for (int i = 0; i < 3; ++i) {
			assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("AEL"), 57, 1);
		}
	}

	@Test
	public void sameResultsWhenAlternatingMutant() throws Exception {
		final BPELUnitSpecification testSpecification = getTestSpecification(TEST_SUITE);
		BPELUnitXMLResult resOriginal = getOriginalProcessResults(testSpecification);
		try {
			compareAllResults(testSpecification, resOriginal, getOp("AEL"), 57, 1, true);
			compareAllResults(testSpecification, resOriginal, getOp("AEL"), 56, 1, false);
			compareAllResults(testSpecification, resOriginal, getOp("AEL"), 57, 1, true);
		} finally {
			resOriginal.getFile().delete();
		}
	}

}
