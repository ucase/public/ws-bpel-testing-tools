package es.uca.webservices.mutants.operators;

import es.uca.webservices.mutants.operators.Operator;
import es.uca.webservices.mutants.operators.OperatorFactory;
import es.uca.webservices.mutants.operators.XSLTBackedOperator;

/**
 * Mutation operator factory specialized to produce test-enabled versions
 * of the mutation operators.
 *
 *  @author Antonio García Domínguez
 *  @version 1.0
 */
public class TestOperatorFactory extends OperatorFactory {

  @Override
  protected Operator createXSLTBackedOperator(final String name,
                                              final String xsltPath) {
    return new XSLTBackedOperator(name, new TestXSLTStylesheet(xsltPath));
  }

}
