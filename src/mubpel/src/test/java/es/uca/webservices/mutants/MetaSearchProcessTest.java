package es.uca.webservices.mutants;

import static junit.framework.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;
import es.uca.webservices.bpel.bpelunit.BPELUnitXMLResult;
import es.uca.webservices.mutants.operators.Operator;
import es.uca.webservices.mutants.operators.OperatorConstants;

public class MetaSearchProcessTest extends BPELMutationTestCase {

    private static final String TEST_SUITE = "metaSearch/MetaSearchTest.bpts";

    public MetaSearchProcessTest() {
        super("metaSearch/MetaSearchBPEL2.bpel");
    }

    /* Sanity tests */

    @Test
    public void alwaysSameResultWithSameMutant() throws Exception {
        BPELUnitSpecification testSpec = getTestSpecification(TEST_SUITE);
        BPELUnitXMLResult originalResult = getOriginalProcessResults(testSpec);

        final Operator OP = getOp("ISV");
        final int OPERAND = 19;
        final int ATTR = 3;
        final int SAME_MUTANT_TRIES = 10;

        BPELUnitXMLResult firstReply = getMutantProcessResults(testSpec, OP,
                                                               OPERAND, ATTR);
        Boolean[] firstResults = firstReply.compareResultsTo(originalResult, null);
        firstReply.getFile().delete();

        for (int i = 2; i <= SAME_MUTANT_TRIES; ++i) {
            BPELUnitXMLResult reply = getMutantProcessResults(testSpec, OP,
                                                              OPERAND, ATTR);
            Boolean[] results = reply.compareResultsTo(originalResult, null);
            reply.getFile().delete();

            assertTrue(String
                    .format("Invocation %d of the mutant should yield "
                            + "the same results as the first one", i), Arrays
                    .equals(firstResults, results));
        }
        
        originalResult.getFile().delete();
    }

    /* ECN TESTS */

    @Test
    public void ecnMultiplyZeroConstantShouldBeTenInstead() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ECN"),
                                  3,
                                  OperatorConstants.ECN_ATTRIBUTE_REPEATLAST,
                                  "//bpel:assign[@name='InitializeMSNValues']/bpel:copy[7]/bpel:from/text()",
                                  "number(10)");
    }

    @Test
    public void ecnDivideZeroConstantShouldBeMinusTenInstead()
            throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ECN"),
                                  3,
                                  OperatorConstants.ECN_ATTRIBUTE_REMOVELAST,
                                  "//bpel:assign[@name='InitializeMSNValues']/bpel:copy[7]/bpel:from/text()",
                                  "number(-10)");
    }

    @Test
    public void ecnDivideZeroConstantShouldBeValid()
            throws Exception {
        this.assertSuccessfulMutationIsDifferent(TEST_SUITE,
          getOp("ECN"), 3, OperatorConstants.ECN_ATTRIBUTE_REMOVELAST);
    }

    @Test
    public void ecnDivideOneConstantShouldBeMinusOneInstead() throws Exception {
       applyAndAssertEqualsXPath(
                getOp("ECN"),
                5,
                OperatorConstants.ECN_ATTRIBUTE_REMOVELAST,
                "//bpel:assign[@name='CounterToOne']/bpel:copy[1]/bpel:from/text()",
                "number(-1)");
    }

    @Test
    public void ecnDivideOneConstantShouldBeValid() throws Exception {
       this.assertSuccessfulMutationIsDifferent(TEST_SUITE,
           getOp("ECN"), 5, OperatorConstants.ECN_ATTRIBUTE_REMOVELAST);
    }

    /* EAA TESTS */

    @Test
    public void eaaNumberOfOperands() throws Exception {
        assertOperandCount(6, getOp("EAA"));
    }

    @Test
    public void eaaAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("EAA"), 4);
    }

    @Test
    public void eaaMutateDiv2CounterToDivision() throws Exception {
        eaaMutateDiv2Counter(OperatorConstants.EAA_ATTRIBUTE_FDIV, "div");
    }

    @Test
    public void eaaMutateDiv2CounterToAddition() throws Exception {
        eaaMutateDiv2Counter(OperatorConstants.EAA_ATTRIBUTE_ADD, "+");
    }

    @Test
    public void eaaMutateDiv2CounterToUnderRange() throws Exception {
        eaaMutateDiv2CounterWithManualAttribute(
                                                OperatorConstants.EAA_ATTRIBUTE_ADD - 1,
                                                "-");
    }

    private void eaaMutateDiv2Counter(final int operator_id,
            final String operator_string) throws Exception {
        eaaMutateDiv2CounterWithManualAttribute(
                                                getAttributeOffsetIgnoringCurrent(
                                                                      OperatorConstants.EAA_ATTRIBUTE_MOD,
                                                                      operator_id),
                                                operator_string);
    }

    private void eaaMutateDiv2CounterWithManualAttribute(final int attribute,
            final String operator_string) throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EAA"),
                                  1,
                                  attribute,
                                  "//bpel:if[@name='Switch_5']/bpel:condition/text()",
                                  "(($div2counter " + operator_string
                                          + " 2.0) = 0.0)");
    }

    @Test
    public void eaaMutateCounterIncrementToMultiplication() throws Exception {
        eaaMutateCounterIncrement(OperatorConstants.EAA_ATTRIBUTE_MUL, "*");
    }

    @Test
    public void eaaMutateCounterIncrementToSubtraction() throws Exception {
        eaaMutateCounterIncrement(OperatorConstants.EAA_ATTRIBUTE_SUB, "-");
    }

    @Test
    public void eaaMutateCounterIncrementToModulus() throws Exception {
        eaaMutateCounterIncrement(OperatorConstants.EAA_ATTRIBUTE_MOD, "mod");
    }

    @Test
    public void eaaMutateCounterIncrementToOverRange() throws Exception {
        eaaMutateCounterIncrementWithManualAttribute(
                                                     getAttributeOffsetIgnoringCurrent(
                                                                           OperatorConstants.EAA_ATTRIBUTE_ADD,
                                                                           OperatorConstants.EAA_ATTRIBUTE_MOD) + 1,
                                                     "-");
    }

    private void eaaMutateCounterIncrement(final int operator_id,
            final String operator_string) throws Exception {
        eaaMutateCounterIncrementWithManualAttribute(
                                                     getAttributeOffsetIgnoringCurrent(
                                                                           OperatorConstants.EAA_ATTRIBUTE_ADD,
                                                                           operator_id),
                                                     operator_string);
    }

    private void eaaMutateCounterIncrementWithManualAttribute(
            final int attribute, final String operator_string) throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EAA"),
                                  2,
                                  attribute,
                                  "//bpel:assign[@name='IncreaseCounterEverySecondTime']/bpel:copy/bpel:from/text()",
                                  "($counter " + operator_string + " 1.0)");
    }

    @Test
    public void eaaMutateDiv2CounterDivisionShouldStandOut() throws Exception {
        assertSuccessfulMutationIsDifferent(
                                  TEST_SUITE,
                                  getOp("EAA"),
                                  1,
                                  getAttributeOffsetIgnoringCurrent(
                                                        OperatorConstants.EAA_ATTRIBUTE_MOD,
                                                        OperatorConstants.EAA_ATTRIBUTE_FDIV));
    }

    @Test
    public void eaaMutateCounterIncrementShouldStandOut() throws Exception {
        assertSuccessfulMutationIsDifferent(
                                  TEST_SUITE,
                                  getOp("EAA"),
                                  2,
                                  getAttributeOffsetIgnoringCurrent(
                                                        OperatorConstants.EAA_ATTRIBUTE_ADD,
                                                        OperatorConstants.EAA_ATTRIBUTE_SUB));
    }

    /* ELL TESTS */

    @Test
    public void ellNumberOfOperands() throws Exception {
        assertOperandCount(4, getOp("ELL"));
    }

    @Test
    public void ellAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ELL"), 1);
    }

    @Test
    public void ellMutateDoneConditionToAnd() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ELL"),
                                  2,
                                  1,
                                  "//bpel:if[@name='IsDoneAtAll']/bpel:condition/text()",
                                  "((boolean($done_google) = true()) and (boolean($done_msn) = true()))");
    }

    @Test
    public void ellMutateResultCollectionConditionToOr() throws Exception {
        // Looking at the source code, it would look as if the original "and"
        // (which should be turned into an "or" by ELL) would be the 4th
        // operand,
        // as it is to the right of an "or" operator, but in reality it comes
        // before it in the corresponding XPath AST, and thus it's really the
        // 3rd
        // operand.
        applyAndAssertEqualsXPath(
                                  getOp("ELL"),
                                  3,
                                  1,
                                  "//bpel:while[@name='While_1']/bpel:condition/text()",
                                  "((($counter <= $maxGoogle) or ($counter <= $maxMSN)) or not(($added >= $inputVariable.payload/client:maxResults)))");
    }

    @Test
    public void ellMutateDoneConditionToAndShouldStandOut() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ELL"), 2, 1);
    }

    @Test
    public void ellMutateResultCollectionConditionShouldStandOut()
            throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ELL"), 4, 1);
    }

    /* ASF TESTS */

    @Test
    public void asfNumberOfOperands() throws Exception {
        assertOperandCount(11, getOp("ASF"));
    }

    /* AIE TESTS */

    @Test
    public void aieNumberOfOperands() throws Exception {
        assertOperandCount(12, getOp("AIE"));
    }

    /* AWR TESTS */

    @Test
    public void awrNumberOfOperands() throws Exception {
        assertOperandCount(2, getOp("AWR"));
    }

    @Test
    public void awrAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("AWR"), 1);
    }

    @Test
    public void awrSwitchMainWhileToRepeatUntil() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("AWR"),
                                  1,
                                  1,
                                  "count(//bpel:sequence[@name='Sequence_5'][count(bpel:while) = 0 and count(bpel:repeatUntil) = 1])",
                                  "1");
    }

    @Test
    public void awrMutatedDuplicateWhileShouldStandOut() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("AWR"), 2, 1);
    }

    /* ASI TESTS */

    @Test
    public void asiNumberOfOperands() throws Exception {
        assertOperandCount(42, getOp("ASI"));
    }

    /* XMF TESTS */

    @Test
    public void xmfNumberOfOperands() throws Exception {
        assertOperandCount(3, getOp("XMF"));
    }

    @Test
    public void xmfAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("XMF"), 1);
    }

    /* ECC TESTS */

    @Test
    public void eccNumberOfOperands() throws Exception {
        assertOperandCount(42, getOp("ECC"));
    }

    /* ISV TESTS */

    @Test
    public void isvNumberOfOperands() throws Exception {
        assertOperandCount(135, getOp("ISV"));
    }

    @Test
    public void isvDoNotInitializeDoneMSN() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ISV"),
                                  3,
                                  1,
                                  "//bpel:assign[@name='MSNNotDone']/bpel:copy/bpel:to/@variable",
                                  "done_google");
    }

    @Test
    public void isvDoNotInitializeDoneMSNAttributeOverRange() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ISV"),
                                  3,
                                  2,
                                  "//bpel:assign[@name='MSNNotDone']/bpel:copy/bpel:to/@variable",
                                  "done_google");
    }

    @Test
    public void isvDoNotInitializeDoneMSNAttributeUnderRange() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ISV"),
                                  3,
                                  -1,
                                  "//bpel:assign[@name='MSNNotDone']/bpel:copy/bpel:to/@variable",
                                  "done_google");
    }

    @Test
    public void isvDoNotInitializeDoneMSNShouldStandOut() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ISV"), 3, 1);
    }

    @Test
    public void isvChangeDoneMSNIsDoneAtall() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ISV"),
                                  6,
                                  1,
                                  "//bpel:if[@name='IsDoneAtAll']/bpel:condition/text()",
                                  "((boolean($done_google) = true()) or (boolean($done_google) = true()))");
    }

    @Test
    public void isvChangeMod2LoopVariable() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ISV"),
                                  65,
                                  1,
                                  "//bpel:if[@name='Switch_5']/bpel:condition/text()",
                                  "(($counter mod 2.0) = 0.0)");
    }

    @Test
    public void isvChangeMod2LoopVariableShouldStandOut() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ISV"), 23, 1);
    }

    /* ERR TESTS */

    @Test
    public void errChangeDoneGoogleEqualTrueToLessThan() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ERR"),
                                  3,
                                  1,
                                  "//bpel:if[@name='IsDoneAtAll']/bpel:condition[1]/text()",
                                  "((boolean($done_google) < true()) or (boolean($done_msn) = true()))");
    }

    @Test
    public void errChangeDoneMSNEqualTrueToLessThan() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ERR"),
                                  4,
                                  1,
                                  "//bpel:if[@name='IsDoneAtAll']/bpel:condition[1]/text()",
                                  "((boolean($done_google) = true()) or (boolean($done_msn) < true()))");
    }

    /* ACI TESTS */

    @Test
    public void aciOperandCount() throws Exception {
        // Only one activity has the createInstance attribute set to "yes":
        // changing it to "no" would produce a stillborn mutant, so we report
        // 0 operands instead
        assertOperandCount(0, getOp("ACI"));
    }

    /* AFP TESTS */

    @Test
    public void afpOperandCount() throws Exception {
        assertOperandCount(0, getOp("AFP"));
    }

    /* AIS TESTS */

    @Test
    public void aisNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("AIS"));
    }

    /* AJC TESTS */

    @Test
    public void ajcOperandCount() throws Exception {
        assertOperandCount(0, getOp("AJC"));
    }

    /* EEU TESTS */

    @Test
    public void eeuOperandCount() throws Exception {
        assertOperandCount(0, getOp("EEU"));
    }
    
    /* EIU TESTS */
    
    @Test
    public void eiuOperandCount() throws Exception {
    	assertOperandCount(9, getOp("EIU"));
    }
    
    @Test
    public void eiuAttributeRange() throws Exception {
    	assertMaximumDistinctAttributeValueIs(getOp("EIU"), 1);
    }
    
    @Test
    public void eiuChangeIfSwitch5() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EIU"),
                                  13,
                                  1,
                                  "//bpel:if[@name='Switch_5']/bpel:condition/text()",
                                  "(-($div2counter mod 2.0) = 0.0)");
    }
    
    @Test
    public void eiuMutatesGoogleMax() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EIU"), 2, 1);
    }
    
    /* EIN TESTS */
    
    @Test
    public void einOperandCount() throws Exception {
    	assertOperandCount(14, getOp("EIN"));
    }
    
    @Test
    public void einAttributeRange() throws Exception {
    	assertMaximumDistinctAttributeValueIs(getOp("EIN"), 1);
    }
    
    @Test
    public void einChangeWhile_1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EIN"),
                                  5,
                                  1,
                                  "//bpel:while[@name='While_1']/bpel:condition/text()",
                                  "not(((($counter <= $maxGoogle) or ($counter <= $maxMSN)) and " +
                                  "not(($added >= $inputVariable.payload/client:maxResults))))");
    }
    
    @Test
    public void einMutatesWhile_1() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EIN"), 5, 1);
    }
    
    /* EAP TESTS */
    
    @Test
    public void eapOperandCount() throws Exception {
    	assertOperandCount(9, getOp("EAP"));
    }
    
    @Test
    public void eapAttributeRange() throws Exception {
    	assertMaximumDistinctAttributeValueIs(getOp("EAP"), 1);
    }
    
    @Test
    public void eapChangeAssignIncreaseCounterEverySecondTime() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EAP"),
                                  5,
                                  1,
                                  "//bpel:assign[@name='IncreaseCounterEverySecondTime']/bpel:copy/bpel:from/text()",
                                  "(($counter + 1.0) * ((($counter + 1.0) >= 0.0) - (($counter + 1.0) < 0.0)))");
    }
    
    @Test
    public void eapMutatesAssignIncreaseCounterEverySecondTime() throws Exception {
    	assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("EAP"), 2, 1);
    }
    
    /* EAN TESTS */
    
    @Test
    public void eanOperandCount() throws Exception {
    	assertOperandCount(9, getOp("EAN"));
    }
    
    @Test
    public void eanAttributeRange() throws Exception {
    	assertMaximumDistinctAttributeValueIs(getOp("EAN"), 1);
    }
    
    @Test
    public void eanChangeAssignIncreaseCounter() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EAN"),
                                  7,
                                  1,
                                  "//bpel:assign[@name='IncreaseCounter']/bpel:copy/bpel:from/text()",
                                  "-(($currentResultNumber + 1.0) * ((($currentResultNumber + 1.0) >= 0.0) - " +
                                  "(($currentResultNumber + 1.0) < 0.0)))");
    }
    
    @Test
    public void eanMutatesAssignIncreaseCounter() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EAN"), 7, 1);
    }
    
    /* CFA TESTS */
    
    @Test
    public void cfaOperandCount() throws Exception {
    	assertOperandCount(73, getOp("CFA"));
    }
    
    @Test
    public void cfaAttributeRange() throws Exception {
    	assertMaximumDistinctAttributeValueIs(getOp("CFA"), 1);
    }
    
    @Test
    public void cfaChangeCancelledAsignSeller() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CFA"),
                                  40,
                                  1,
                                  "count(//bpel:if[@name='Switch_3']) = 0 and " +
                                  "count(//bpel:if[@name='Switch_5']/bpel:exit) = 1",
                                  "true");
    }
    
    @Test
    public void cfaMutatesCancelledAsignSeller() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CFA"), 40, 1);
    }
    
    /* CDE TESTS */
    
    @Test
    public void cdeOperandCount() throws Exception {
    	assertOperandCount(14, getOp("CDE"));
    }
    
    @Test
    public void cdeAttributeRange() throws Exception {
    	assertMaximumDistinctAttributeValueIs(getOp("CDE"), 2);
    }
    
    @Test
    public void cdeChangeIfSwitch2() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDE"),
                                  3,
                                  1,
                                  "//bpel:if[@name='Switch_2']/bpel:condition/text()",
                                  "true()");
    }
    
    @Test
    public void cdeMutatesIfSwitch2() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDE"), 3, 1);
    }
    
    /* CCO TESTS */
    
    @Test
    public void ccoOperandCount() throws Exception {
    	assertOperandCount(18, getOp("CCO"));
    }
    
    @Test
    public void ccoAttributeRange() throws Exception {
    	assertMaximumDistinctAttributeValueIs(getOp("CCO"), 2);
    }
    
    @Test
    public void ccoChangeIfSwitch_1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CCO"),
                                  6,
                                  2,
                                  "//bpel:if[@name='Switch_1']/bpel:condition/text()",
                                  "false()");
    }
    
    @Test
    public void ccoMutatesIfSwitch_1() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CCO"), 6, 2);
    }
    
    /* CDC TESTS */
    
    @Test
    public void cdcOperandCount() throws Exception {
    	assertOperandCount(21, getOp("CDC"));
    }
    
    @Test
    public void cdcAttributeRange() throws Exception {
    	assertMaximumDistinctAttributeValueIs(getOp("CDC"), 2);
    }
    
    @Test
    public void cdcChangeIfSwitch_1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDC"),
                                  8,
                                  1,
                                  "//bpel:if[@name='Switch_1']/bpel:condition/text()",
                                  "true()");
    }
    
    @Test
    public void cdcMutatesIfSwitch_1() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDC"), 8, 1);
    }
}
