package es.uca.webservices.mutants;

import org.junit.Test;

import es.uca.webservices.mutants.operators.OperatorConstants;

public class MarketPlaceFlowDoubleQuoteProcessTest extends BPELMutationTestCase {

    private final static String TEST_SUITE = "marketPlace/marketplace_flow.bpts";

    public MarketPlaceFlowDoubleQuoteProcessTest() {
		super("marketPlace/marketplace_flow_doublequotes.bpel");
	}

    /* EMD TESTS */

    @Test
    public void emdOperandCount() throws Exception {
        assertOperandCount(1, getOp("EMD"));
    }

    @Test
    public void emdHalveWaitBetweenMessages() throws Exception {
        applyAndAssertEqualsXPath(getOp("EMD"), 1,
                                  OperatorConstants.EMD_ATTRIBUTE_HALF,
                                  "//bpel:for", "'P0Y0M0DT0H0M2.500S'");
    }

    @Test
    public void emdRemoveWaitBetweenMessages() throws Exception {
        applyAndAssertEqualsXPath(getOp("EMD"), 1,
                                  OperatorConstants.EMD_ATTRIBUTE_ZERO,
                                  "//bpel:for", "'P0Y0M0DT0H0M0.000S'");
    }

    @Test
    public void emdNoWaitMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EMD"), 1,
                                  OperatorConstants.EMD_ATTRIBUTE_ZERO);
    }

    @Test
    public void emdHalfWaitMutantIsEquivalent() throws Exception {
        assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("EMD"), 1,
                                   OperatorConstants.EMD_ATTRIBUTE_HALF);
    }
	
}
