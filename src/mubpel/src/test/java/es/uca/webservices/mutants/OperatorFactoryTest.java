package es.uca.webservices.mutants;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import es.uca.webservices.mutants.operators.DummyOperator;
import es.uca.webservices.mutants.operators.Operator;
import es.uca.webservices.mutants.operators.OperatorFactory;
import es.uca.webservices.mutants.operators.XSLTBackedOperator;

/**
 * Simple tests for an operator factory.
 * 
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class OperatorFactoryTest {

	private static final String EXISTING_OPERATOR = "asi";

	@Test
	public void existingOperatorIsXSLTBacked() {
		final Operator op = new OperatorFactory().newOperator(EXISTING_OPERATOR);
		assertEquals(EXISTING_OPERATOR, op.getName());
		assertTrue("Existing operator should be XSLT-backed", op instanceof XSLTBackedOperator);
	}

	@Test
	public void missingOperatorIsDummy() throws Exception {
		final Operator op = new OperatorFactory().newOperator("missing");
		assertEquals("missing", op.getName());
		assertEquals(0, op.getOperandCount(null));
		assertEquals(1, op.getMaximumDistinctAttributeValue(null));
		assertNull(op.apply(null, 1, 1));
		assertTrue("Missing operator should be a dummy", op instanceof DummyOperator);
	}
}
