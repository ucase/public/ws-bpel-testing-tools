package es.uca.webservices.mutants.subcommands;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests for the "count" subcommand.
 *
 * @author Antonio García-Domínguez
 */
public class CountMutantsSubcommandTest {

	@Test
	public void loanRPC() throws Exception {
		final CountMutantsSubcommand cmd = new CountMutantsSubcommand();
		final int count = cmd.count("src/test/resources/loanRPC/loanRPC.bpel");
		assertEquals(55, count);
	}

}
