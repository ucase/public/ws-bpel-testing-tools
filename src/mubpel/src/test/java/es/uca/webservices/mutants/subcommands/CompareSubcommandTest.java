package es.uca.webservices.mutants.subcommands;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.bpelunit.CustomizedRunner;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.mutants.subprocesses.ProcessUtils;
import es.uca.webservices.ode.cli.ODEEmbedded;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.Assert;
import org.activebpel.rt.jetty.AeJettyForker;
import org.junit.After;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.*;

/**
 * Tests for the 'compare' subcommand. These tests depend on the TacService
 * composition and its .bpel.
 * 
 * @author Antonio García-Domínguez
 */
public class CompareSubcommandTest {

	private static final Logger logger = LoggerFactory.getLogger(CompareSubcommandTest.class);
	private static final String BPELODE = "src/test/resources/TacService_ODE/tacService.bpel";
	private static final String BPTSODE = "src/test/resources/TacService_ODE/tacService.bpts";
	private static final String BPEL = "src/test/resources/tacService/tacService.bpel";
	private static final String BPTS = "src/test/resources/tacService/tacService.bpts";

	private String engine = ExecutionSubcommand.ENGINE_TYPE_ODE;
	private File fOriginalResult;
	private File fM1, fM2, fM3, fM4;

	private AeJettyForker fServerActiveBPEL;
	private ODEEmbedded fServerODE;

	// Expected results for --keep-going
	private static final int[] expected_O_O = new int[] { 0, 0, 0, 0 };
	private static final int[] expected_O_M1 = new int[] { 1, 0, 0, 0 };
	private static final int[] expected_O_M1_K = new int[] { 1, 1, 1, 1 };
	private static final int[] expected_O_M2 = new int[] { 1, 0, 0, 0 };
	private static final int[] expected_O_M2_K = new int[] { 1, 1, 1, 1 };
	private static final int[] expected_O_M3_ODE = new int[] { 1, 0, 0, 0 };
	private static final int[] expected_O_M3_K_ODE = new int[] { 1, 1, 1, 1 };
	private static final int[] expected_O_M3_ActiveBPEL = new int[] { 0, 1, 0, 0 };
	private static final int[] expected_O_M3_K_ActiveBPEL = new int[] { 0, 1, 1, 1 };
	private static final int[] expected_O_M4 = new int[] { 1, 0, 0, 0 };
	private static final int[] expected_O_M4_K = new int[] { 1, 1, 1, 1 };

	@Test 
	public void compareOriginalAgainstItselfActiveBPEL()
			throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ACTIVEBPEL);
		assertComparisonResultsEqual(
			"A composition is equal to itself with compare", BPEL,	expected_O_O, expected_O_O);
	}
	
	@Test
	public void compareOriginalAgainstM1ActiveBPEL() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ACTIVEBPEL);
		assertComparisonResultsEqual(
			"Original and M1 should produce the expected results with compare",
			fM1.getCanonicalPath(), expected_O_M1_K, expected_O_M1);
	}

	@Test
	public void compareOriginalAgainstM1WithFullActiveBPEL() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ACTIVEBPEL);
		assertComparisonResultsEqualWithCompareFull(
			"Original and M1 should produce the expected results with comparefull",
			fM1.getCanonicalPath(), expected_O_M1_K);
	}
	
	@Test
	public void compareOriginalAgainstM3ActiveBPEL() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ACTIVEBPEL);
		assertComparisonResultsEqual(
			"Original and M3 should produce the expected results with compare",
			fM3.getCanonicalPath(), expected_O_M3_K_ActiveBPEL, expected_O_M3_ActiveBPEL);
	}
	
	@Test
	public void compareOriginalAgainstItselfShouldReportAllEqual() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		assertComparisonResultsEqual(
			"A composition is equal to itself with compare", BPELODE,	expected_O_O, expected_O_O);
	}

	@Test
	public void compareOriginalAgainstItselfShouldReportAllEqualWithFull() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		assertComparisonResultsEqualWithCompareFull(
			"A composition is equal to itself with comparefull", BPELODE, expected_O_O);
	}

	@Test
	public void compareOriginalAgainstM1() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		assertComparisonResultsEqual(
			"Original and M1 should produce the expected results with compare",
			fM1.getCanonicalPath(), expected_O_M1_K, expected_O_M1);
	}

	@Test
	public void compareOriginalAgainstM1WithFull() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		assertComparisonResultsEqualWithCompareFull(
			"Original and M1 should produce the expected results with comparefull",
			fM1.getCanonicalPath(), expected_O_M1_K);
	}

	/**
	 * Tests that comparing a composition with a mutant produces the expected
	 * results. The filename is received through a stream, and the server is
	 * brought up in a different port from the suite-wide server.
	 */
	@Test
	public void compareParallelFromStream() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		final String sPortNumber = Integer.toString(fServerODE.getTomcatPort() + 1);

		Process process = ProcessUtils.callJavaClass(CompareSubcommand.class,
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, RunSubcommandTest.PATH_DEPENDENCY_ODE,
				"--" + ExecutionSubcommand.PARALLEL_OPTION,
				"--" + ExecutionSubcommand.ENGINE_PORT_OPTION, sPortNumber,
				"--" + CompareSubcommand.KEEP_GOING_OPTION, BPTSODE, BPELODE,
				fOriginalResult.getCanonicalPath(), "-");

		// Write the path to the mutants into the subprocess' stdin
		PrintStream ps = new PrintStream(process.getOutputStream());
		ps.println(fM1.getCanonicalPath());
		ps.println(fM2.getCanonicalPath());
		ps.close();

		// Check stderr for results
		dumpStderr(process);
		int[][] results = dumpStreamAsResults(process.getInputStream(),
				fM1.getCanonicalPath(), fM2.getCanonicalPath());
		process.destroy();

		assertArrayEquals("Original and M1 should produce the expected results "
				+ "(keep going)", expected_O_M1_K, results[0]);
		assertArrayEquals("Original and M2 should produce the expected results "
				+ "(keep going)", expected_O_M2_K, results[1]);
	}

	/**
	 * Tests that performing the comparison of O with (O,M1,M2,M2,M1,O) in
	 * parallel with the default number of engines (NCPU + 1) and job deck size
	 * (8*NCPU) works and produces the correct results.
	 */
	@Test
	public void compareParallelUntilFirstDifference() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		final List<String> extraOptions = Arrays.asList(
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, RunSubcommandTest.PATH_DEPENDENCY_ODE,
				"--" + CompareSubcommand.PARALLEL_OPTION);
		assertParallelComparisonWorks(
				CompareSubcommand.DEFAULT_PARALLEL_THREAD_COUNT, extraOptions, false);
	}

	/**
	 * Tests that performing the comparison of O with (O,M1,M2,M2,M1,O) in
	 * parallel with 2 engines (#CPU + 1), disabling job shuffling and running
	 * all tests cases works and produces the correct results.
	 */
	@Test
	public void compareParallelKeepGoingNoShuffling() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		assertParallelComparisonWorksWithDeckSize(0);
	}

	/**
	 * Same test as {@link #compareParallelKeepGoingNoShuffling()}, except we
	 * now use a deck size of exactly 1. It's not really shuffling, and it's not
	 * really parallelizing anything: just checking if the code handles the
	 * corner cases well.
	 */
	@Test
	public void compareParallelKeepGoingDummyShuffling() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		assertParallelComparisonWorksWithDeckSize(1);
	}

	/**
	 * Same test as {@link #compareParallelKeepGoingNoShuffling()}, but we now
	 * use a deck size of 2. This is to ensure everything works if the job list
	 * size is a multiple of a deck size other than 1.
	 */
	@Test
	public void compareParallelKeepGoingShufflingTwo() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		assertParallelComparisonWorksWithDeckSize(2);
	}

	/**
	 * Same test as {@link #compareParallelKeepGoingShufflingTwo()}, but we use
	 * a deck size of 4. This ensures everything works even if the job list size
	 * is *not* a multiple of the deck size.
	 */
	@Test
	public void compareParallelKeepGoingShufflingFour() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		assertParallelComparisonWorksWithDeckSize(3);
	}

	/**
	 * Tests that the correct column headers are printed when using the
	 * --column-headers option.
	 * @throws Exception 
	 */
	@Test
	public void printColumnHeadersSequential() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		final Process process = ProcessUtils.callJavaClass(CompareSubcommand.class,
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, RunSubcommandTest.PATH_DEPENDENCY_ODE,
				"--" + ExecutionSubcommand.ENGINE_PORT_OPTION,
				Integer.toString(fServerODE.getTomcatPort() + 1),
				"--" + CompareSubcommand.COLUMN_HEADERS_OPTION,
				BPTSODE, BPELODE,	fOriginalResult.getCanonicalPath(), fM1.getCanonicalPath());

		testColumnHeaders(process);
	}

	/**
	 * Tests that the correct column headers are printed when using the
	 * --column-headers option, reading file names from stdin.
	 * @throws Exception 
	 */
	@Test
	public void printColumnHeadersSequentialFromStdin() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		final Process process = ProcessUtils.callJavaClass(CompareSubcommand.class,
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, RunSubcommandTest.PATH_DEPENDENCY_ODE,
				"--" + ExecutionSubcommand.ENGINE_PORT_OPTION,
				Integer.toString(fServerODE.getTomcatPort() + 1),
				"--" + CompareSubcommand.COLUMN_HEADERS_OPTION,
				BPTSODE, BPELODE,	fOriginalResult.getCanonicalPath(), "-");
		
		final PrintStream ps = new PrintStream(process.getOutputStream());
		ps.println(fM1.getCanonicalPath());
		ps.close();
		testColumnHeaders(process);
	}

	/**
	 * Tests that the correct column headers are printed when using the
	 * --parallel and --column-headers options.
	 * @throws Exception 
	 */
	@Test
	public void printColumnHeadersParallel() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		final Process process = ProcessUtils.callJavaClass(CompareSubcommand.class,
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, RunSubcommandTest.PATH_DEPENDENCY_ODE,
				"--" + ExecutionSubcommand.PARALLEL_OPTION,
				"--" + ExecutionSubcommand.ENGINE_PORT_OPTION,
				Integer.toString(fServerODE.getTomcatPort() + 1),
				"--" + CompareSubcommand.COLUMN_HEADERS_OPTION,
				BPTSODE, BPELODE,	fOriginalResult.getCanonicalPath(), fM1.getCanonicalPath());

		testColumnHeaders(process);
	}

	/**
	 * Tests that the correct column headers are printed when using the
	 * --column-headers option, reading file names from stdin.
	 * @throws Exception 
	 */
	@Test
	public void printColumnHeadersParallelFromStdin() throws Exception {
		startEngine(ExecutionSubcommand.ENGINE_TYPE_ODE);
		final Process process = ProcessUtils.callJavaClass(CompareSubcommand.class,
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, RunSubcommandTest.PATH_DEPENDENCY_ODE,
				"--" + ExecutionSubcommand.PARALLEL_OPTION,
				"--" + ExecutionSubcommand.ENGINE_PORT_OPTION,
				Integer.toString(fServerODE.getTomcatPort() + 1),
				"--" + CompareSubcommand.COLUMN_HEADERS_OPTION,
				BPTSODE, BPELODE,	fOriginalResult.getCanonicalPath(), "-");
		
		final PrintStream ps = new PrintStream(process.getOutputStream());
		ps.println(fM1.getCanonicalPath());
		ps.close();
		testColumnHeaders(process);
	}
	
	/* START ENGINE */
	private void startEngine(String engine) throws Exception {
		this.engine = engine;

		if (ExecutionSubcommand.ENGINE_TYPE_ODE.equals(engine)) {
			fServerODE = new ODEEmbedded(null, CustomizedRunner.DEFAULT_ENGINE_PORT, "none");
		}else{
			fServerActiveBPEL = new AeJettyForker(null, CustomizedRunner.DEFAULT_ENGINE_PORT + 1, "none");
			fServerActiveBPEL.start();
		}

		generateOriginalOutput();
		fM1 = generateMutant("asi", 1, 1);
		fM2 = generateMutant("ael", 1, 1);
		fM3 = generateMutant("ael", 2, 1);
		fM4 = generateMutant("cfa", 1, 1);
		if (logger.isDebugEnabled()) {
			logger.debug("fM1 is in " + fM1.getCanonicalPath());
			logger.debug("fM2 is in " + fM2.getCanonicalPath());
			logger.debug("fM3 is in " + fM3.getCanonicalPath());
			logger.debug("fM4 is in " + fM4.getCanonicalPath());
		}
	}

	@After
	public void tearDown() throws Exception {
		if (fServerODE != null) {
			fServerODE.stopTomcat();
		} else if (fServerActiveBPEL != null) {
			fServerActiveBPEL.stop();
		}

		fM1.delete();
		fM2.delete();
		fM3.delete();
		fM4.delete();
		fOriginalResult.delete();
	}

	/* PARALLELIZED COMPARISON WITH CUSTOM DECK SIZE */

	private void assertParallelComparisonWorksWithDeckSize(final int deckSize) throws Exception {
		final List<String> extraOptions = Arrays
				.asList("--" + ExecutionSubcommand.ODE_WAR_OPTION, RunSubcommandTest.PATH_DEPENDENCY_ODE,
						"--" + CompareSubcommand.PARALLEL_OPTION, "2", "--"
						+ ExecutionSubcommand.DECK_SIZE_OPTION, Integer
						.toString(deckSize), "--"
						+ CompareSubcommand.KEEP_GOING_OPTION);
		assertParallelComparisonWorks(2, extraOptions, true);
	}

	/* PARALLELIZED COMPARISON */

	private void assertParallelComparisonWorks(final int expectedThreadCount,
			final List<String> extraOptions, final boolean keepGoingResults)
			throws Exception {
		final String pOriginalResult = fOriginalResult.getCanonicalPath();
		final String pM1 = fM1.getCanonicalPath();
		final String pM2 = fM2.getCanonicalPath();
		final String pM3 = fM3.getCanonicalPath();
		final String pM4 = fM4.getCanonicalPath();
		final int portNumber = fServerODE.getTomcatPort() + 1;
		final String sPortNumber = Integer.toString(portNumber);

		final List<String> args = new ArrayList<>(Arrays.asList("--"
				+ ExecutionSubcommand.ENGINE_PORT_OPTION, sPortNumber, BPTSODE,
				BPELODE, pOriginalResult, BPELODE, pM1, pM2, pM3, pM4));
		List<int[]> expected;
		if (keepGoingResults) {
			expected = Arrays.asList(
				expected_O_O, expected_O_M1_K, expected_O_M2_K, expected_O_M3_K_ODE, expected_O_M4_K);
		} else {
			expected = Arrays.asList(
				expected_O_O, expected_O_M1, expected_O_M2, expected_O_M3_ODE, expected_O_M4);
		}

		CompareSubcommand cmd = new CompareSubcommand();
		args.addAll(0, extraOptions);
		cmd.parseArgs(args.toArray(new String[] {}));
		assertEquals(expectedThreadCount, cmd.getThreadCount());
		cmd.run();

		final List<Pair<String, Pair<int[], long[]>>> results = cmd.getResults();
		assertEquals(
			"The number of results should match the number of expected results",
			expected.size(), results.size());

		for (int i = 0; i < expected.size(); ++i) {
			final String expectedValues = Arrays.toString(expected.get(i));
			final String obtainedValues = Arrays.toString(results.get(i).getRight().getLeft());
			assertEquals(String.format("expected[%d] == results[%d]", i, i),
					expectedValues, obtainedValues);
		}
	}

	/* COMPARISON IN SUBPROCESSES */

	private void dumpStderr(Process process) throws IOException {
		String line;
		BufferedReader errReader = new BufferedReader(
				new InputStreamReader(process.getErrorStream()));
		while ((line = errReader.readLine()) != null) {
			System.err.println(line);
		}
		process.getErrorStream().close();
	}

	private int[][] dumpStreamAsResults(InputStream inputStream, String... expectedPaths)
			throws IOException {
		// Dump the whole stream to a byte array
		ByteArrayOutputStream bOS = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int count;
		while ((count = inputStream.read(buf)) != -1) {
			bOS.write(buf, 0, count);
		}

		// Decode the byte array and split it into lines
		String[] lines = bOS.toString().split(
				System.getProperty("line.separator"));

		List<int[]> results = new ArrayList<>();
		int iExpected = 0;
		for (String line : lines) {
			try {
				String[] parts = addResultsLineTo(line, results);
				assertEquals(
						"Each line lists the .bpel file with which the original is compared",
						expectedPaths[iExpected++], parts[0]);
			} catch (NumberFormatException ex) {
				// skip bad lines (such as those introduced by Cobertura's instrumentation)
			}
		}
		assertEquals("There is the expected number of results", expectedPaths.length, iExpected);

		return results.toArray(new int[][]{});
	}

	private String[] addResultsLineTo(final String line, List<int[]> results) {
		String[] parts = line.split(" ");
		assert parts.length >= 2 : "The line '" + line + "' should have at least two parts but did not";
		
		final int nTestCases = (parts.length - 2) >> 1;
		int[] result = new int[nTestCases];
		for (int j = 0; j < result.length; ++j) {
			result[j] = Integer.parseInt(parts[1 + j]);
		}
		results.add(result);
		return parts;
	}

	/* PAIRWISE COMPARISON */

	private void assertComparisonResultsEqual(String msg,
			String pathToOtherBPEL, int[] keepGoingResult,
			int[] untilFirstDiffResult) throws Exception {

		CompareSubcommand cmd = new CompareSubcommand();
		
		if (engine.equals(ExecutionSubcommand.ENGINE_TYPE_ODE)){
			cmd.setPathToOdeWar(new File(RunSubcommandTest.PATH_DEPENDENCY_ODE));
			cmd.setRootWorkDirectory(new File (fServerODE.getBaseDir()));
			cmd.setEnginePort(fServerODE.getTomcatPort());
		}else{
			cmd.setEngineType("activebpel");
			cmd.setBPRDir(fServerActiveBPEL.getBPRDirectory());
			cmd.setRootWorkDirectory(fServerActiveBPEL.getMainDirectory());
			cmd.setEnginePort(fServerActiveBPEL.getPort());	
		}
		final String pathToOriginalResult = fOriginalResult.getCanonicalPath();

		// First, use --keep-going, and then try again without --keep-going
		cmd.getResults().clear();
		cmd.setKeepGoing(true);
		if (engine.equals(ExecutionSubcommand.ENGINE_TYPE_ODE)){
			cmd.compare(BPTSODE, BPELODE, pathToOriginalResult, pathToOtherBPEL);
		}else{
			cmd.compare(BPTS, BPEL, pathToOriginalResult, pathToOtherBPEL);
		}
		
		cmd.setKeepGoing(false);
		if (engine.equals(ExecutionSubcommand.ENGINE_TYPE_ODE)){
			cmd.compare(BPTSODE, BPELODE, pathToOriginalResult, pathToOtherBPEL);
		}else{
			cmd.compare(BPTS, BPEL, pathToOriginalResult, pathToOtherBPEL);
		}

		assertArrayEquals(msg + "(keep going)", keepGoingResult, cmd.getResults().get(0).getRight().getLeft());
		assertArrayEquals(msg + "(stop at first diff)", untilFirstDiffResult, cmd.getResults().get(1).getRight().getLeft());
	}

	private void assertComparisonResultsEqualWithCompareFull(String msg,
			String pathToOtherBPEL, int[] keepGoingResult) throws Exception
	{
		final String pathToOriginalResult = fOriginalResult.getCanonicalPath();

		CompareFullSubcommand cmdfullCLI = new CompareFullSubcommand();
		
		// Call it as if from the CLI
		if (engine.equals(ExecutionSubcommand.ENGINE_TYPE_ODE)){
			cmdfullCLI.parseArgs(
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, RunSubcommandTest.PATH_DEPENDENCY_ODE,
				"--" + ExecutionSubcommand.WORKDIR_OPTION, fServerODE.getBaseDir(),
				"--" + ExecutionSubcommand.ENGINE_PORT_OPTION, Integer.toString(fServerODE.getTomcatPort()),
				BPTSODE, BPELODE, pathToOriginalResult, pathToOtherBPEL);
		}else{
			cmdfullCLI.parseArgs(
					"--" + ExecutionSubcommand.ENGINE_TYPE_OPTION, "activebpel",
					"--" + ExecutionSubcommand.WORKDIR_OPTION, fServerActiveBPEL.getMainDirectory().toString(),
					"--" + ExecutionSubcommand.ENGINE_PORT_OPTION, Integer.toString(fServerActiveBPEL.getPort()),
					BPTS, BPEL, pathToOriginalResult, pathToOtherBPEL);
			cmdfullCLI.setBPRDir(fServerActiveBPEL.getBPRDirectory());
			cmdfullCLI.run();
		}
		cmdfullCLI.run();
		assertArrayEquals(msg + " (comparefull, CLI)", keepGoingResult, cmdfullCLI.getResults().get(0).getRight().getLeft());
	}

	/* TEST SETUP */

	private void generateOriginalOutput() throws Exception {
		fOriginalResult = File.createTempFile("comparetest_o", ".xml");

		RunSubcommand cmd = new RunSubcommand();
		if (engine.equals(ExecutionSubcommand.ENGINE_TYPE_ODE)){
			cmd.setEnginePort(fServerODE.getTomcatPort());
			cmd.setRootWorkDirectory(new File(fServerODE.getBaseDir()));
			cmd.setOutputStream(new FileOutputStream(fOriginalResult));
			cmd.setPathToOdeWar(new File(RunSubcommandTest.PATH_DEPENDENCY_ODE));
			cmd.run(BPTSODE, BPELODE);
		}else{
			cmd.setEngineType("activebpel");
			cmd.setBPRDir(fServerActiveBPEL.getBPRDirectory());
			cmd.setEnginePort(fServerActiveBPEL.getPort());
			cmd.setRootWorkDirectory(fServerActiveBPEL.getMainDirectory());
			cmd.setOutputStream(new FileOutputStream(fOriginalResult));
			cmd.run(BPTS, BPEL);
		}
	}

	private File generateMutant(String operator, int operand, int attr) throws Exception {
		File file = File.createTempFile("comparetest", ".bpel");
		ApplySubcommand apply = new ApplySubcommand();
		BPELProcessDefinition doc;
		if (engine.equals(ExecutionSubcommand.ENGINE_TYPE_ODE)){
			doc = apply.apply(new BPELProcessDefinition(new File(BPELODE)), operator, operand, attr);
		}else{
			doc = apply.apply(new BPELProcessDefinition(new File(BPEL)), operator, operand, attr);
		}
		doc.dumpToStream(new FileOutputStream(file));
		return file;
	}

	private void testColumnHeaders(final Process process) throws IOException {
		process.getOutputStream().close();
		dumpStderr(process);
	
		final BufferedReader bR = new BufferedReader(new InputStreamReader(process.getInputStream()));
		final List<int[]> lResults = new ArrayList<>();
		String sColumnHeaders = null;
		for (String line = bR.readLine(); line != null; line = bR.readLine()) {
			if (line.startsWith(CompareSubcommand.BPEL_FILE_COL_HEADER)) {
				if (sColumnHeaders != null) {
					Assert.fail("There should be exactly one column headers line");
				}
				else {
					sColumnHeaders = line;
				}
			}
			else {
				try {
					assertNotNull("The column headers line should come before the first result", sColumnHeaders);
					addResultsLineTo(line, lResults);
				} catch (NumberFormatException ex) {
					// ignore extra output lines added by coverage instrumentation
				}
			}
		}
		assertEquals(1, lResults.size());
		assertArrayEquals(expected_O_M1, lResults.get(0));
	
		final String[] columnHeaders = sColumnHeaders.split("\t");
		assertArrayEquals(new String[] {
			CompareSubcommand.BPEL_FILE_COL_HEADER,
			"Test Case EmptyInput",
			"Test Case OneLine",
			"Test Case TwoLines",
			"Test Case ThreeLines",
			CompareSubcommand.TIMES_SEPARATOR,
			CompareSubcommand.TIMES_SEPARATOR + "Test Case EmptyInput",
			CompareSubcommand.TIMES_SEPARATOR + "Test Case OneLine",
			CompareSubcommand.TIMES_SEPARATOR + "Test Case TwoLines",
			CompareSubcommand.TIMES_SEPARATOR + "Test Case ThreeLines",
		}, columnHeaders);
	}

}
