package es.uca.webservices.mutants;

import static org.junit.Assert.assertArrayEquals;

import java.io.File;

import org.junit.Test;

import es.uca.webservices.bpel.bpelunit.BPELUnitXMLResult;

public class MessageComparisonTest {

	@Test
	public void ignoreSOAPHeadersForComparison() throws Exception {
		final BPELUnitXMLResult resOriginal = new BPELUnitXMLResult(new File("src/test/resources/compareWithHeaders/original.xml"));
		final BPELUnitXMLResult resOther = new BPELUnitXMLResult(new File("src/test/resources/compareWithHeaders/other.xml"));
		
		assertArrayEquals(new Boolean[] {true, true, true}, resOriginal.compareResultsTo(resOther, null));
	}

	@Test
	public void ignoreXPathForComparison() throws Exception {
		final BPELUnitXMLResult resOriginal = new BPELUnitXMLResult(new File(
			"src/test/resources/compareWithHeaders/original.xml"
		));
		final BPELUnitXMLResult resOther = new BPELUnitXMLResult(new File(
			"src/test/resources/compareWithHeaders/ignoreXPath.xml"
		));

		assertArrayEquals(
			new Boolean[] {false, true, true},
			resOriginal.compareResultsTo(resOther, null)
		);
		assertArrayEquals(
			new Boolean[] {false, true, true},
			resOriginal.compareResultsTo(resOther, "//element()[local-name(.)='notused']")
		);
		assertArrayEquals(
			new Boolean[] {true, true, true},
			resOriginal.compareResultsTo(resOther, "//element()[local-name(.)='amount']")
		);
		assertArrayEquals(
			new Boolean[] {true, true, true},
			resOriginal.compareResultsTo(resOther, "//element()[local-name(.)='check' and namespace-uri(.)='http://j2ee.netbeans.org/wsdl/loanServicePT']/element()[local-name(.)='amount']")
		);
	}
}
