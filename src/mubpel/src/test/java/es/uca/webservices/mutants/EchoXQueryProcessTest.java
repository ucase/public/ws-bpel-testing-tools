package es.uca.webservices.mutants;

import org.junit.Test;

/**
 * Tests for the XQuery-based echo example,
 * @author Antonio Garcia-Dominguez
 */
public class EchoXQueryProcessTest extends BPELMutationTestCase {

    public EchoXQueryProcessTest() {
        super("echoXQuery/bpel/simplebpel.bpel");
    }

    @Test
    public void eeuDoesNotApplyToXQuery() throws Exception {
        assertOperandCount(0, getOp("EEU"));
    }

    @Test
    public void isvDoesNotVisitXQueryExpressions() throws Exception {
        assertOperandCount(0, getOp("ISV"));
    }

    @Test
    public void asfOperands() throws Exception {
        assertOperandCount(1, getOp("ASF"));
    }

    @Test
    public void asiOperands() throws Exception {
        assertOperandCount(1, getOp("ASI"));
    }

    @Test
    public void aelOperands() throws Exception {
        assertOperandCount(4, getOp("AEL"));
    }

    @Test
    public void cfaOperands() throws Exception {
        assertOperandCount(4, getOp("CFA"));
    }
}
