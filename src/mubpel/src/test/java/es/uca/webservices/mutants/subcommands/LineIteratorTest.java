package es.uca.webservices.mutants.subcommands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.StringReader;
import java.util.NoSuchElementException;

import org.junit.Test;

/**
 * Tests for the LineIterator class.
 * 
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class LineIteratorTest {

	@Test(expected = NoSuchElementException.class)
	public void emptyString() throws IOException {
		LineIterator it = new LineIterator(new StringReader(""));
		assertFalse(it.hasNext());
		it.next();
	}

	@Test
	public void oneLine() throws IOException {
		LineIterator it = new LineIterator(new StringReader("hi"));
		assertTrue(it.hasNext());
		assertEquals("hi", it.next());
		assertFalse(it.hasNext());		
	}

	@Test
	public void twoLines() throws IOException {
		LineIterator it = new LineIterator(new StringReader("hi\nho"));
		assertTrue(it.hasNext());
		assertEquals("hi", it.next());
		assertTrue(it.hasNext());
		assertEquals("ho", it.next());
		assertFalse(it.hasNext());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void cannotRemove() throws IOException {
		LineIterator it = new LineIterator(new StringReader(""));
		it.remove();
	}
}
