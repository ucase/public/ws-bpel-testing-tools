package es.uca.webservices.mutants.parallel;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.junit.Test;

import es.uca.webservices.mutants.subprocesses.ProcessUtils;

/**
 * Tests {@link MultiProcessPipe} for processes that suddenly close and require restarting.
 */
public class MultiProcessPipeFailedProcessTest {

	@Test
	public void test() throws Exception {
		assertPipeWorks(8, 200);
	}

	public static void main(String[] args) throws Exception {
		final int maxLines = Integer.valueOf(args[0]);
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		for (int i = 0; i < maxLines; ++i) {
			String line;
			if ((line = reader.readLine()) != null) {
				System.out.println("processed " + line);
				Thread.sleep(50);
			}
		}
	}

	private void assertPipeWorks(final int nProcesses, final int nLines)
			throws IOException, InterruptedException, ExecutionException
	{
		// Create the pipe
		List<List<String>> args = new ArrayList<List<String>>(nProcesses);
		for (int i = 0; i < nProcesses; ++i) {
			args.add(ProcessUtils.callJavaClassArgs(getClass(), "" + (i + 1) * 5));
		}
		MultiProcessPipe mp = new MultiProcessPipe(System.err, args, MultiProcessPipe.REGULAR_THREADS);

		// Write from 0 to lines - 1
		for (int i = 0; i < nLines; ++i) {
			mp.writeLine(Integer.toString(i));
		}
		// Tell the pipe that we are done writing
		mp.doneWriting();

		// Read all available lines, checking their order and number
		int iLineCount = 0;
		String line;
		while ((line = mp.readLine()) != null) {
			assertEquals("processed " + Integer.toString(iLineCount++), line);
		}
		assertEquals(nLines, iLineCount);
		mp.doneReading();
	}

}
