package es.uca.webservices.mutants;

import org.junit.Test;

import es.uca.webservices.mutants.operators.OperatorConstants;

public class LoanStructuredProcessTest extends BPELMutationTestCase {

    private static final String TEST_SUITE = "loanStructured/ServicioPrestamosTest.bpts";

    public LoanStructuredProcessTest() {
        super("loanStructured/loanStructuredNonRPC.bpel");
    }

    @Test
    public void eaaNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("EAA"));
    }

    @Test
    public void ellNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("ELL"));
    }

    @Test
    public void errNumberOfOperands() throws Exception {
        assertOperandCount(2, getOp("ERR"));
    }

    @Test
    public void ecnNumberOfOperands() throws Exception {
        assertOperandCount(1, getOp("ECN"));
    }

    @Test
    public void ecnIncrementLoanThreshold() throws Exception {
        ecnAssertLoanThresholdMutationEquals(
                                             OperatorConstants.ECN_ATTRIBUTE_INC,
                                             "10001");
    }

    @Test
    public void ecnDecrementLoanThreshold() throws Exception {
        ecnAssertLoanThresholdMutationEquals(
                                             OperatorConstants.ECN_ATTRIBUTE_DEC,
                                             "9999");
    }

    @Test
    public void ecnRepeatLastNumberLoanThreshold() throws Exception {
        ecnAssertLoanThresholdMutationEquals(
                                             OperatorConstants.ECN_ATTRIBUTE_REPEATLAST,
                                             "100000.00");
    }

    @Test
    public void ecnDeleteLastNumberLoanThreshold() throws Exception {
        ecnAssertLoanThresholdMutationEquals(
                                             OperatorConstants.ECN_ATTRIBUTE_REMOVELAST,
                                             "1000.00");
    }

    @Test
    public void ecnAttributeUnderRange() throws Exception {
        ecnAssertLoanThresholdMutationEquals(
                                             OperatorConstants.ECN_ATTRIBUTE_INC - 1,
                                             "9999");
    }

    @Test
    public void ecnAttributeOverRange() throws Exception {
        ecnAssertLoanThresholdMutationEquals(
                                             OperatorConstants.ECN_ATTRIBUTE_REMOVELAST + 1,
                                             "10001");
    }

    private void ecnAssertLoanThresholdMutationEquals(final int attribute,
            final String expr) throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ECN"),
                                  1,
                                  attribute,
                                  "//bpel:if[@name='If1']/bpel:condition/text()",
                                  "(number(string($processInput.input/ns0:amount)) < "
                                          + expr + ")");
    }

    /* ASF TESTS */

    @Test
    public void asfNumberOfOperands() throws Exception {
        assertOperandCount(3, getOp("ASF"));
    }

    @Test
    public void asfAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ASF"), 1);
    }

    @Test
    public void asfMutateSmallAmountHighRiskSequenceToFlow() throws Exception {
        applyAndAssertEqualsXPath(getOp("ASF"), 2, 0,
                                  "//bpel:if[@name='If2']/bpel:flow/@name",
                                  "SmallAmountHighRisk");
    }

    @Test
    public void asfMutatedSmallAmountHighRiskSequenceShouldStandOut()
            throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ASF"), 3, 1);
    }

    /* AIE TESTS */

    @Test
    public void aieNumberOfOperands() throws Exception {
        assertOperandCount(2, getOp("AIE"));
    }

    @Test
    public void aieAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("AIE"), 1);
    }

    @Test
    public void aieMutateRemoveLargeAmountElseBranch() throws Exception {
        applyAndAssertEqualsXPath(getOp("AIE"), 2, 0,
                                  "count(//bpel:if[@name='If1']/bpel:else)",
                                  "0");
    }

    @Test
    public void aieMutatedSmallAmountHighRiskBranchRemovedShouldStandOut()
            throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("AIE"), 1, 1);
    }

    /* AWR TESTS */

    @Test
    public void awrNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("AWR"));
    }

    /* ASI TESTS */

    @Test
    public void asiNumberOfOperands() throws Exception {
        assertOperandCount(12, getOp("ASI"));
    }

    /* This test is designed to ensure that the new comparison operator which
     * considers all outbound messages from the WS-BPEL composition works. The
     * old comparison operator wouldn't notice that we were asking for a loan
     * of 0 monetary units, as it could only rely on the output presented to
     * the client, and not on the messages sent to the external services. */
    @Test
    public void asiSwappedAssignInvokeShouldStandOut() throws Exception {
        this.assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ASI"), 3, 1);
    }

    /* XMF TESTS */

    @Test
    public void xmfNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("XMF"));
    }

    /* ECC TESTS */

    @Test
    public void eccNumberOfOperands() throws Exception {
        assertOperandCount(13, getOp("ECC"));
    }

    @Test
    public void eccAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ECC"), 1);
    }

    @Test
    public void eccChangeLoanThresholdComparisonPathOpToAllDescendants()
            throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ECC"),
                                  1,
                                  1,
                                  "//bpel:if[@name='If1']/bpel:condition/text()",
                                  "(number(string($processInput.input//ns0:amount)) < 10000.0)");
    }

    // TODO: write // -> / test as well (no working examples right now)

    @Test
    public void eccMutatedLoanThresholdPathShouldNotStandOut() throws Exception {
        assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("ECC"), 2, 1);
    }

    /* ISV TESTS */

    @Test
    public void isvNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("ISV"));
    }

    @Test
    public void isvAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ISV"), 1);
    }

    /* ACI TESTS */

    @Test
    public void aciOperandCount() throws Exception {
        // Only one activity has the createInstance attribute set to "yes":
        // changing it to "no" would produce a stillborn mutant, so we report
        // 0 operands instead
        assertOperandCount(0, getOp("ACI"));
    }

    /* AFP TESTS */

    @Test
    public void afpOperandCount() throws Exception {
        assertOperandCount(0, getOp("AFP"));
    }

    /* AIS TESTS */

    @Test
    public void aisNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("AIS"));
    }

    /* AJC TESTS */

    @Test
    public void ajcOperandCount() throws Exception {
        assertOperandCount(0, getOp("AJC"));
    }

    /* EEU TESTS */

    @Test
    public void eeuOperandCount() throws Exception {
        assertOperandCount(0, getOp("EEU"));
    }

    /* EIU TESTS */
    
    @Test
    public void eiuOperandCount() throws Exception {
    	assertOperandCount(2, getOp("EIU"));
    }
    
    @Test
    public void eiuChangeIf1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EIU"),
                                  2,
                                  1,
                                  "//bpel:if[@name='If1']/bpel:condition/text()",
                                  "(number(string($processInput.input/ns0:amount)) < -10000.0)");
    }
    
    @Test
    public void eiuMutatesIf1Condition() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EIU"), 2, 1);
    }
    
    /* EIN TESTS */
    
    @Test
    public void einOperandCount() throws Exception {
        assertOperandCount(2, getOp("EIN"));
    }
    
    @Test
    public void einChangeIf1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EIN"),
                                  1,
                                  1,
                                  "//bpel:if[@name='If1']/bpel:condition/text()",
                                  "not((number(string($processInput.input/ns0:amount)) < 10000.0))");
    }
    
    @Test
    public void einMutatesIf1Condition() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EIN"), 1, 1);
    }
    
    /* EAP TESTS */
    
    @Test
    public void eapOperandCount() throws Exception {
    	assertOperandCount(2, getOp("EAP"));
    }
    
    @Test
    public void eapChangeIf1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EAP"),
                                  2,
                                  1,
                                  "//bpel:if[@name='If1']/bpel:condition/text()",
                                  "(number(string($processInput.input/ns0:amount)) < (10000.0 * ((10000.0 >= 0.0) - (10000.0 < 0.0))))");
    }
    
    @Test
    public void eapMutatesIf1Condition() throws Exception {
    	assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("EAP"), 2, 1);
    }
    
    /* EAN TESTS */
    
    @Test
    public void eanOperandCount() throws Exception {
    	assertOperandCount(2, getOp("EAN"));
    }
    
    @Test
    public void eanChangeIf1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EAN"),
                                  2,
                                  1,
                                  "//bpel:if[@name='If1']/bpel:condition/text()",
                                  "(number(string($processInput.input/ns0:amount)) < -(10000.0 * ((10000.0 >= 0.0) - (10000.0 < 0.0))))");
    }
    
    @Test
    public void eanMutatesIf1Condition() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EAN"), 2, 1);
    }
    
    /* CFA TESTS */
    
    @Test
    public void cfaOperandCount() throws Exception {
    	assertOperandCount(17, getOp("CFA"));
    }
    
    @Test
    public void cfaChangeSmallAmount() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CFA"),
                                  3,
                                  1,
                                  "count(//bpel:sequence[@name='SmallAmount']) = 0 and " +
                                  "count(//bpel:if[@name='If1']/bpel:exit) = 1",
                                  "true");
    }
    
    @Test
    public void cfaMutatesSequenceSmallAmount() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CFA"), 3, 1);
    }
    
    /* CDE TESTS */
    
    @Test
    public void cdeOperandCount() throws Exception {
    	assertOperandCount(2, getOp("CDE"));
    }
    
    @Test
    public void cdeAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("CDE"), 2);
    }
    
    @Test
    public void cdeChangeIf2False() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDE"),
                                  2,
                                  2,
                                  "//bpel:if[@name='If2']/bpel:condition/text()",
                                  "false()");
    }
    
    @Test
    public void cdeMutatesIf2False() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDE"), 2, 2);
    }
    
    /* CCO TESTS */
    
    @Test
    public void ccoOperandCount() throws Exception {
    	assertOperandCount(2, getOp("CCO"));
    }
    
    @Test
    public void ccoAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("CCO"), 2);
    }
    
    @Test
    public void ccoChangeIf2False() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CCO"),
                                  2,
                                  2,
                                  "//bpel:if[@name='If2']/bpel:condition/text()",
                                  "false()");
    }
    
    @Test
    public void ccoMutatesIf2False() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CCO"), 2, 2);
    }
    
    //In this composition, all decisions have only one condition, so 
    //CDE, CCO and CDC mutations are the same.
    
    /* CDC TESTS */
    
    @Test
    public void cdcOperandCount() throws Exception {
    	assertOperandCount(2, getOp("CDC"));
    }
    
    @Test
    public void cdcAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("CDC"), 2);
    }
    
    @Test
    public void cdcChangeIf2False() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDC"),
                                  2,
                                  2,
                                  "//bpel:if[@name='If2']/bpel:condition/text()",
                                  "false()");
    }
    
    @Test
    public void cdcMutatesIf2False() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDC"), 2, 2);
    }
    
}
