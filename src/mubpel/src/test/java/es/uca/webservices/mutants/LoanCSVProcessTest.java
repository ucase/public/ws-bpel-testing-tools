package es.uca.webservices.mutants;


import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;
import es.uca.webservices.bpel.bpelunit.BPELUnitXMLResult;

public class LoanCSVProcessTest extends BPELMutationTestCase {

	private static final String TEST_SUITE = "loanCSV/LoanApprovalProcess-CSV.bpts";

	public LoanCSVProcessTest() {
		super("loanCSV/LoanApprovalProcess.bpel");
	}

	@Test
	public void lessThanIsEqualWithCompare() throws Exception {
		assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("ERR"), 1, 1);
	}

	@Test
	public void lessThanIsEqualWithCompareKeepGoing() throws ParserConfigurationException, SAXException, IOException, Exception {
		final BPELUnitSpecification testSpecification = getTestSpecification(TEST_SUITE);
		BPELUnitXMLResult resOriginal = getOriginalProcessResults(testSpecification);
		try {
			compareAllResults(testSpecification, resOriginal, getOp("ERR"), 1, 1, true);
		} finally {
			resOriginal.getFile().delete();
		}
	}
	
}
