package es.uca.webservices.mutants;

import org.junit.Test;

import es.uca.webservices.mutants.operators.OperatorConstants;

public class LoanRPCProcessTest extends BPELMutationTestCase {

    private static final String TEST_SUITE = "loanRPC/loanRPC.bpts";

    public LoanRPCProcessTest() {
        super("loanRPC/loanRPC.bpel");
    }

    /* INVALID MUTANT DETECTION TESTS */

    @Test
    public void invalidMutantsAreDetected() throws Exception {
        assertMutantIsInvalid(TEST_SUITE, getOp("generate_invalid_mutant"), 1, 1);
    }

    /* ERR TESTS */

    @Test
    public void errOperandCount() throws Exception {
        assertOperandCount(2, getOp("ERR"));
    }

    @Test
    public void errAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ERR"), 5);
    }

    @Test
    public void errChangeLoanAmountThresholdComparisonToGreaterEqual()
            throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ERR"),
                                  1,
                                  getAttributeOffsetIgnoringCurrent(
                                                        OperatorConstants.ERR_ATTRIBUTE_LE,
                                                        OperatorConstants.ERR_ATTRIBUTE_GE),
                                  "//bpel:if[@name='IfSmallAmount']/bpel:condition/text()",
                                  "($request.amount >= 10000.0)");
    }

    @Test
    public void errChangeLoanAmountThresholdComparisonToGreaterThan()
            throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ERR"),
                                  1,
                                  getAttributeOffsetIgnoringCurrent(
                                                        OperatorConstants.ERR_ATTRIBUTE_LE,
                                                        OperatorConstants.ERR_ATTRIBUTE_GT),
                                  "//bpel:if[@name='IfSmallAmount']/bpel:condition/text()",
                                  "($request.amount > 10000.0)");
    }

    @Test
    public void errChangeLoanAmountThresholdComparisonToLessThan()
            throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ERR"),
                                  1,
                                  getAttributeOffsetIgnoringCurrent(
                                                        OperatorConstants.ERR_ATTRIBUTE_LE,
                                                        OperatorConstants.ERR_ATTRIBUTE_LT),
                                  "//bpel:if[@name='IfSmallAmount']/bpel:condition/text()",
                                  "($request.amount < 10000.0)");
    }

    @Test
    public void errChangeRiskComparisonToInequality() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ERR"),
                                  2,
                                  getAttributeOffsetIgnoringCurrent(
                                                        OperatorConstants.ERR_ATTRIBUTE_EQ,
                                                        OperatorConstants.ERR_ATTRIBUTE_NE),
                                  "//bpel:if[@name='IfLowRisk']/bpel:condition/text()",
                                  "($risk.level != 'low')");
    }

    @Test
    public void errChangeRiskComparisonToAttributeUnderRange() throws Exception {
        // Since (a idiv b) * b + (a mod b) = a must hold in this case:
        // (-1 idiv 6) * 6 + (-1 mod 6) = -1 mod 6 = -1
        // 1 + abs(-1 mod 6) yields 2, which is
        // OperatorConstants.ERR_ATTRIBUTE_GT
        applyAndAssertEqualsXPath(
                                  getOp("ERR"),
                                  2,
                                  OperatorConstants.ERR_ATTRIBUTE_LT - 1,
                                  "//bpel:if[@name='IfLowRisk']/bpel:condition/text()",
                                  "($risk.level > 'low')");
    }

    @Test
    public void errChangeRiskComparisonToAttributeOverRange() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ERR"),
                                  2,
                                  getAttributeOffsetIgnoringCurrent(
                                                        OperatorConstants.ERR_ATTRIBUTE_EQ,
                                                        OperatorConstants.ERR_ATTRIBUTE_NE) + 1,
                                  "//bpel:if[@name='IfLowRisk']/bpel:condition/text()",
                                  "($risk.level < 'low')");
    }

    @Test
    public void errChangeRiskComparisonToLessEqual() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ERR"),
                                  2,
                                  getAttributeOffsetIgnoringCurrent(
                                                        OperatorConstants.ERR_ATTRIBUTE_EQ,
                                                        OperatorConstants.ERR_ATTRIBUTE_LE),
                                  "//bpel:if[@name='IfLowRisk']/bpel:condition/text()",
                                  "($risk.level <= 'low')");
    }

    @Test
    public void errMutatedToGreaterThanThresholdComparisonShouldStandOut()
            throws Exception {
        assertSuccessfulMutationIsDifferent(
                                  TEST_SUITE,
                                  getOp("ERR"),
                                  1,
                                  getAttributeOffsetIgnoringCurrent(
                                                        OperatorConstants.ERR_ATTRIBUTE_LE,
                                                        OperatorConstants.ERR_ATTRIBUTE_GT));
    }

    @Test
    public void errMutatedToNotEqualRiskComparisonShouldStandOut()
            throws Exception {
        assertSuccessfulMutationIsDifferent(
                                  TEST_SUITE,
                                  getOp("ERR"),
                                  2,
                                  getAttributeOffsetIgnoringCurrent(
                                                        OperatorConstants.ERR_ATTRIBUTE_EQ,
                                                        OperatorConstants.ERR_ATTRIBUTE_NE));
    }

    @Test
    public void errMutatedToStrictLessThanThresholdComparisonShouldNotStandOut()
            throws Exception {
        assertSuccessfulMutationIsEquivalent(
                                   TEST_SUITE,
                                   getOp("ERR"),
                                   1,
                                   getAttributeOffsetIgnoringCurrent(
                                                         OperatorConstants.ERR_ATTRIBUTE_LE,
                                                         OperatorConstants.ERR_ATTRIBUTE_LT));
    }

    @Test
    public void ecnNumberOfOperands() throws Exception {
        assertOperandCount(1, getOp("ECN"));
    }

    @Test
    public void ecnAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ECN"), 4);
    }

    @Test
    public void ecnAttributeChangeQuantityOperandsAndAttributeValuesWrapFromOne()
            throws Exception {
        final String xpathExpr = "//bpel:condition[1]/text()";
        final String expected = "($request.amount <= 9999)";

        // 1 is the starting position for attribute and operand indices,
        // so the indices 0 and 2 are semantically equivalent both for
        // attributes and perands
        applyAndAssertEqualsXPath(getOp("ECN"), 0, 0, xpathExpr, expected);
        applyAndAssertEqualsXPath(getOp("ECN"), 0, 2, xpathExpr, expected);
    }

    @Test
    public void ecnMutatedRepeatNumberLoanThresholdShouldStandOut()
            throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ECN"), 1,
                                  OperatorConstants.ECN_ATTRIBUTE_REPEATLAST);
    }

    @Test
    public void ecnMutatedRemoveNumberLoanThresholdShouldStandOut()
            throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ECN"), 1,
                                  OperatorConstants.ECN_ATTRIBUTE_REMOVELAST);
    }

    @Test
    public void ecnMutatedDecrementLoanThresholdShouldNotStandOut()
            throws Exception {
        assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("ECN"), 1,
                                   OperatorConstants.ECN_ATTRIBUTE_DEC);
    }

    @Test
    public void ecnMutatedIncrementLoanThresholdShouldNotStandOut()
            throws Exception {
        assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("ECN"), 1,
                                   OperatorConstants.ECN_ATTRIBUTE_INC);
    }

    /* EAA TESTS */

    @Test
    public void eaaNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("EAA"));
    }

    /* ELL TESTS */

    @Test
    public void ellNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("ELL"));
    }

    /* ASF TESTS */

    @Test
    public void asfNumberOfOperands() throws Exception {
        assertOperandCount(1, getOp("ASF"));
    }

    /* AIE TESTS */

    @Test
    public void aieNumberOfOperands() throws Exception {
        assertOperandCount(2, getOp("AIE"));
    }

    /* AWR TESTS */

    @Test
    public void awrNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("AWR"));
    }

    /* ASI TESTS */

    @Test
    public void asiNumberOfOperands() throws Exception {
        assertOperandCount(2, getOp("ASI"));
    }

    @Test
    public void asiAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ASI"), 1);
    }

    @Test
    public void asiChangeOrderInvokeAssignAssessor() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("ASI"),
                                  2,
                                  1,
                                  "count(//bpel:sequence[@name='SmallAmount'])",
                                  "1",
                                  "When swapping, ancestor elements should not be repeated");
        applyAndAssertEqualsXPath(
                                  getOp("ASI"),
                                  2,
                                  1,
                                  "count(//bpel:sequence[@name='SmallAmount']/*)",
                                  "2",
                                  "When swapping, the swapped elements should not be repeated");
        applyAndAssertEqualsXPath(
                                  getOp("ASI"),
                                  2,
                                  1,
                                  "count(//bpel:sequence[@name='SmallAmount'][*[1][self::bpel:if] and *[2][self::bpel:invoke]])",
                                  "1",
                                  "After swapping, the swapped elements should be in reverse order");
    }

    @Test
    public void asiMutatedAssesorInvocationShouldStandOut() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ASI"), 3, 1);
    }

    /* XMF TESTS */

    @Test
    public void xmfNumberOfOperands() throws Exception {
        assertOperandCount(1, getOp("XMF"));
    }

    /* ECC TESTS */

    @Test
    public void eccNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("ECC"));
    }

    /* ISV TESTS */

    @Test
    public void isvNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("ISV"));
    }

    @Test
    public void isvAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ISV"), 1);
    }

    /* ACI TESTS */

    @Test
    public void aciOperandCount() throws Exception {
        // Only one activity has the createInstance attribute set to "yes":
        // changing it to "no" would produce a stillborn mutant, so we report
        // 0 operands instead
        assertOperandCount(0, getOp("ACI"));
    }

    /* AFP TESTS */

    @Test
    public void afpOperandCount() throws Exception {
        assertOperandCount(0, getOp("AFP"));
    }

    /* AIS TESTS */

    @Test
    public void aisNumberOfOperands() throws Exception {
        assertOperandCount(0, getOp("AIS"));
    }

    @Test
    public void aisMutationWithNoOperandsProducesWarning() throws Exception {
      // Applying any mutation operator for which there are no operands
      // should produce a warning.
      applyOperator(getOp("AIS"), 0, 0);
      assertWarningCount(getOp("AIS"), 1);
    }

    @Test
    public void aisMutationShouldBeNoop() throws Exception {
      // Applying any mutation operator for which there are no operands
      // should produce an exact copy of the original process definition.
      assertMutationEquivalence(TEST_SUITE, getOp("AIS"), 0, 0, true, true);
      assertWarningCount(getOp("AIS"), 1);
    }

    /* AEL TESTS */

    @Test
    public void aelFaultHandlerShouldBeRemoved() throws Exception {
        applyAndAssertEqualsXPath(getOp("AEL"), 1, 0,
                "count(//bpel:faultHandlers[1]/bpel:catch/bpel:reply)", "0");
    }

    @Test
    public void aelFaultHandlerRemovalShouldBeDetected() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("AEL"), 1, 0);
    }

}
