package es.uca.webservices.mutants.operators;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;

import es.uca.webservices.bpel.util.XSLTStylesheet;

/**
 * XSLT stylesheet which collects all errors, fatal errors and warnings into
 * lists and lets the user query them. Errors and fatal errors are rethrown
 * as TransformerExceptions. Only the errors, fatal errors and warnings from
 * the last transformation are stored; their lists are cleared upon each
 * transformation.
 *
 * @see javax.xml.transform.TransformerException
 * @author Antonio García Domínguez
 * @version 1.0
 */
public class TestXSLTStylesheet
    extends XSLTStylesheet
    implements ErrorListener {

  private List<TransformerException> lErrors
    = new ArrayList<TransformerException>();
  private List<TransformerException> lFatalErrors
    = new ArrayList<TransformerException>();
  private List<TransformerException> lWarnings
    = new ArrayList<TransformerException>();

  public TestXSLTStylesheet(String pathToStylesheet) {
    super(pathToStylesheet);
  }

  @Override
  protected void prepareTransformer(Transformer t) {
    lErrors.clear();
    lFatalErrors.clear();
    lWarnings.clear();
    t.setErrorListener(this);
  }

  public void error(TransformerException e) throws TransformerException {
    lErrors.add(e);
    throw e;
  }

  public void fatalError(TransformerException e) throws TransformerException{
    lFatalErrors.add(e);
    throw e;
  }

  public void warning(TransformerException e) throws TransformerException {
    lWarnings.add(e);
  }

  /**
   * Accessor to the error list.
   * @return List with all errors thrown from the XSLT stylesheet.
   */
  public List<TransformerException> getErrors() {
    return lErrors;
  }

  /**
   * Accessor to the fatal error list.
   * @return List with all fatal errors thrown from the XSLT stylesheet.
   */
  public List<TransformerException> getFatalErrors() {
    return lFatalErrors;
  }

  /**
   * Accessor to the warning list.
   * @return List with all warnings thrown from the XSLT stylesheet.
   */
  public List<TransformerException> getWarnings() {
    return lWarnings;
  }

}
