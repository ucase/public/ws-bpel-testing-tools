package es.uca.webservices.mutants;

import org.junit.Test;

import es.uca.webservices.mutants.operators.OperatorConstants;

public class MarketPlaceFlowProcessTest extends BPELMutationTestCase {

    private final static String TEST_SUITE = "marketPlace/marketplace_flow.bpts";

    public MarketPlaceFlowProcessTest() {
        super("marketPlace/marketplace_flow.bpel");
    }

    /* AJC TESTS */

    @Test
    public void ajcOperandCount() throws Exception {
        assertOperandCount(1, getOp("AJC"));
    }

    @Test
    public void ajcRemoveJoinConditionFromNegotiation() throws Exception {
        applyAndAssertEqualsXPath(getOp("AJC"), 1, 0, "count(//bpel:joinCondition)",
                                  "0");
    }

    @Test
    public void ajcMutantWithoutJoinConditionIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("AJC"), 1, 0);
    }

    /* APA TESTS */

    @Test
    public void apaOperandCount() throws Exception {
        assertOperandCount(1, getOp("APA"));
    }

    @Test
    public void apaDisabledSecondPartyTimeoutMutantIsDifferent()
            throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("APA"), 1, 0);
    }

    /* APM AND BROKEN LINK DELETION TESTS */

    @Test
    public void apmOperandCount() throws Exception {
        assertOperandCount(4, getOp("APM"));
    }

    @Test
    public void apmMutateFirstMessageOnFirstPick() throws Exception {
        applyAndAssertEqualsXPath(getOp("APM"), 1, 0,
                                  "count(//bpel:pick[1]/bpel:onMessage)", "1");
        applyAndAssertEqualsXPath(
                                  getOp("APM"),
                                  1,
                                  0,
                                  "//bpel:pick[1]/bpel:onMessage[1]/@partnerLink",
                                  "buyer");

        // The links started from inside the delete <onMessage> should also be
        // deleted, so the mutant can be successfully deployed
        applyAndAssertEqualsXPath(getOp("APM"), 1, 0,
            "count(//bpel:link[@name='firstSeller1' or @name='firstSeller2'])",
            "0");
        applyAndAssertEqualsXPath(getOp("APM"), 1, 0,
                "count(//bpel:source[@linkName='firstSeller1' or @linkName='firstSeller2'])",
                "0");
        applyAndAssertEqualsXPath(getOp("APM"), 1, 0,
                "count(//bpel:target[@linkName='firstSeller1' or @linkName='firstSeller2'])",
                "0");
    }

    @Test
    public void apmMutateSecondMessageOnSecondPick() throws Exception {
        applyAndAssertEqualsXPath(getOp("APM"), 4, 0,
                                  "count(//bpel:pick[2]/bpel:onMessage)", "1");
        applyAndAssertEqualsXPath(
                                  getOp("APM"),
                                  4,
                                  0,
                                  "//bpel:pick[2]/bpel:onMessage[1]/@partnerLink",
                                  "seller");

        // The links started from inside the delete <onMessage> should also be
        // deleted, so the mutant can be successfully deployed
        applyAndAssertEqualsXPath(getOp("APM"), 4, 0,
                "count(//bpel:link[@name='secondBuyer'])", "0");
        applyAndAssertEqualsXPath(getOp("APM"), 4, 0,
                "count(//bpel:source[@linkName='secondBuyer'])", "0");
        applyAndAssertEqualsXPath(getOp("APM"), 4, 0,
                "count(//bpel:target[@linkName='secondBuyer'])", "0");
    }

    @Test
    public void apmIgnoreSellerIfFirstMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("APM"), 2, 0);
    }

    @Test
    public void apmIgnoreBuyerIfSecondMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("APM"), 3, 0);
    }

    /* EEU TESTS */

    @Test
    public void eeuOperandCount() throws Exception {
        assertOperandCount(0, getOp("EEU"));
    }

    /* EMD TESTS */

    @Test
    public void emdOperandCount() throws Exception {
        assertOperandCount(1, getOp("EMD"));
    }

    @Test
    public void emdHalveWaitBetweenMessages() throws Exception {
        applyAndAssertEqualsXPath(getOp("EMD"), 1,
                                  OperatorConstants.EMD_ATTRIBUTE_HALF,
                                  "//bpel:for", "'P0Y0M0DT0H0M2.500S'");
    }

    @Test
    public void emdRemoveWaitBetweenMessages() throws Exception {
        applyAndAssertEqualsXPath(getOp("EMD"), 1,
                                  OperatorConstants.EMD_ATTRIBUTE_ZERO,
                                  "//bpel:for", "'P0Y0M0DT0H0M0.000S'");
    }

    @Test
    public void emdNoWaitMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EMD"), 1,
                                  OperatorConstants.EMD_ATTRIBUTE_ZERO);
    }

    @Test
    public void emdHalfWaitMutantIsEquivalent() throws Exception {
        assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("EMD"), 1,
                                   OperatorConstants.EMD_ATTRIBUTE_HALF);
    }

    /* XMC TESTS */

    @Test
    public void xmcOperandCount() throws Exception {
        assertOperandCount(2, getOp("XMC"));
    }

    @Test
    public void xmcRemoveFirstCompensationHandler() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("XMC"),
                                  1,
                                  0,
                                  "count(//bpel:scope[@name='FirstMessageFromSeller']/bpel:compensationHandler)",
                                  "0");
        applyAndAssertEqualsXPath(
                                  getOp("XMC"),
                                  1,
                                  0,
                                  "count(//bpel:scope[@name='FirstMessageFromBuyer']/bpel:compensationHandler)",
                                  "1");
    }

    @Test
    public void xmcMutantWithoutSecondCompensationHandlerIsDifferent()
            throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("XMC"), 2, 0);
    }
    
    /* EIU TESTS */
    
    @Test
    public void eiuOperandCount() throws Exception {
    	assertOperandCount(0, getOp("EIU"));
    }
    
    /* EIN TESTS */
    
    @Test
    public void einOperandCount() throws Exception {
        assertOperandCount(2, getOp("EIN"));
    }
    
    @Test
    public void einChangesIf1() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EIN"),
                                  2,
                                  1,
                                  "//bpel:if[@name='MarketplaceSwitch']/bpel:condition/text()",
                                  "not(($sellerInfo.askingPrice <= $buyerInfo.offer))");
    }
    
    @Test
    public void einMutatesIf1Condition() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EIN"), 2, 1);
    }
    
    /* EAP TESTS */
    
    @Test
    public void eapOperandCount() throws Exception {
    	assertOperandCount(0, getOp("EAP"));
    }
    
    /* EAN TESTS */
    
    @Test
    public void eanOperandCount() throws Exception {
    	assertOperandCount(0, getOp("EAN"));
    }
    
    /* CFA TESTS */
    
    @Test
    public void cfaOperandCount() throws Exception {
    	assertOperandCount(16, getOp("CFA"));
    }
    
    @Test
    public void cfaChangeCancelledAsignSeller() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CFA"),
                                  5,
                                  1,
                                  "count(//bpel:assign[@name='CancelledAssignSeller']) = 0 and " +
                                  "count(//bpel:scope[@name='FirstMessageFromSeller']/bpel:compensationHandler" +
                                  "/bpel:sequence/bpel:exit) = 1",
                                  "true");
    }
    
    @Test
    public void cfaMutatesAsignSeller() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CFA"), 5, 1);
    }
    
    /* CDE TESTS */
    
    @Test
    public void cdeOperandCount() throws Exception {
    	assertOperandCount(2, getOp("CDE"));
    }
    
    @Test
    public void cdeChangeJoinFailure() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDE"),
                                  1,
                                  1,
                                  "//bpel:if[@name='MarketplaceSwitch']/bpel:targets/bpel:joinCondition/text()",
                                  "true()");
    }
    
    @Test
    public void cdeMutatesJoinFailure() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDE"), 1, 1);
    }
    
    /* CCO TESTS */
    
    @Test
    public void ccoOperandCount() throws Exception {
    	assertOperandCount(5, getOp("CCO"));
    }
    
    @Test
    public void ccoChangeJoinFailure() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CCO"),
                                  2,
                                  2,
                                  "//bpel:if[@name='MarketplaceSwitch']/bpel:targets/bpel:joinCondition/text()",
                                  "(($firstSeller2 and false()) or ($firstBuyer2 and $secondSeller))");
    }
    
    @Test
    public void ccoMutatesJoinFailure() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CCO"), 2, 2);
    }
    
    
    /* CDC TESTS */
    
    @Test
    public void cdcOperandCount() throws Exception {
    	assertOperandCount(6, getOp("CDC"));
    }
    
    @Test
    public void cdcChangeJoinFailure() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDC"),
                                  4,
                                  2,
                                  "//bpel:if[@name='MarketplaceSwitch']/bpel:targets/bpel:joinCondition/text()",
                                  "(($firstSeller2 and $secondBuyer) or (false() and $secondSeller))");
    }
    
    @Test
    public void cdcMutatesJoinFailure() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDC"), 4, 2);
    }
    
}
