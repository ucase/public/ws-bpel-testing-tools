package es.uca.webservices.mutants.subcommands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.xml.namespace.QName;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests for the {@link FastCompareOutputsSubcommand} class.
 *
 * @author Antonio García-Domínguez
 */
public class FastCompareOutputsSubcommandTest {

	FastCompareOutputsSubcommand cmd;
	
	@Before
	public void setUp() {
		cmd = new FastCompareOutputsSubcommand();
	}

	@Test
	public void emptyIgnored() {
		cmd.setIgnoreTags("");
		assertEquals(0, cmd.getIgnoreTags().size());
	}

	@Test
	public void singleOnlyLocal() {
		cmd.setIgnoreTags("a");
		assertEquals(1, cmd.getIgnoreTags().size());
		assertTrue(cmd.getIgnoreTags().contains(new QName("a")));
	}

	@Test
	public void singleOnlyNS() {
		cmd.setIgnoreTags("{http://example.com}a");
		assertEquals(1, cmd.getIgnoreTags().size());
		assertTrue(cmd.getIgnoreTags().contains(new QName("http://example.com", "a")));
	}

	@Test
	public void twoOnlyLocal() {
		cmd.setIgnoreTags("a,b");
		assertEquals(2, cmd.getIgnoreTags().size());
		assertTrue(cmd.getIgnoreTags().contains(new QName("a")));
		assertTrue(cmd.getIgnoreTags().contains(new QName("b")));
	}

	@Test
	public void twoOnlyNS() {
		cmd.setIgnoreTags("{foo.com}a,{https://www.uca.es}b");
		assertEquals(2, cmd.getIgnoreTags().size());
		assertTrue(cmd.getIgnoreTags().contains(new QName("foo.com", "a")));
		assertTrue(cmd.getIgnoreTags().contains(new QName("https://www.uca.es", "b")));
	}

	@Test
	public void twoMixed() {
		cmd.setIgnoreTags("{foo.com}a,b");
		assertEquals(2, cmd.getIgnoreTags().size());
		assertTrue(cmd.getIgnoreTags().contains(new QName("foo.com", "a")));
		assertTrue(cmd.getIgnoreTags().contains(new QName("b")));
	}
}
