package es.uca.webservices.mutants;

import org.junit.Test;

import es.uca.webservices.mutants.operators.OperatorConstants;

public class TravelReservationServiceProcessTest extends BPELMutationTestCase {

    private static final String TEST_SUITE = "travelReservationService/travelTest.bpts";

    public TravelReservationServiceProcessTest() {
        super(
              "travelReservationService/TravelReservationService.bpel");
    }

    /* ISV TESTS */

    @Test
    public void isvOperandCount() throws Exception {
        assertOperandCount(0, getOp("ISV"));
    }

    @Test
    public void isvAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ISV"), 1);
    }


    /* EAA TESTS */

    @Test
    public void eaaOperandCount() throws Exception {
        assertOperandCount(0, getOp("EAA"));
    }

    @Test
    public void eaaAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("EAA"), 4);
    }


    /* EEU TESTS */

    @Test
    public void eeuOperandCount() throws Exception {
        assertOperandCount(0, getOp("EEU"));
    }

    @Test
    public void eeuAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("EEU"), 1);
    }


    /* ERR TESTS */

    @Test
    public void errOperandCount() throws Exception {
        assertOperandCount(6, getOp("ERR"));
    }

    @Test
    public void errAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ERR"), 5);
    }

    @Test
    public void errMutateHasAirlineCondition() throws Exception {
        applyAndAssertEqualsXPath(getOp("ERR"), 1, 1,
            "(//bpel:if)[1]/bpel:condition/text()",
            "(count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems/ota:Item/ota:Air) < 0.0)");
        applyAndAssertEqualsXPath(getOp("ERR"), 1, 1,
            "(//bpel:if)[1]/bpel:condition/text()",
            "(count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems/ota:Item/ota:Air) < 0.0)");
        applyAndAssertEqualsXPath(getOp("ERR"), 1, 1,
            "(//bpel:if)[2]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:CustomerInfos/ota:CustomerInfo/ota:Customer/ota:PersonName) = 0");
        applyAndAssertEqualsXPath(getOp("ERR"), 1, 1,
            "(//bpel:if)[3]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:CustomerInfos/ota:CustomerInfo/ota:Customer/ota:Email) = 0");
        applyAndAssertEqualsXPath(getOp("ERR"), 1, 1,
            "(//bpel:if)[4]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:TravelCost/ota:FormOfPayment) = 0");
        applyAndAssertEqualsXPath(getOp("ERR"), 1, 1,
            "(//bpel:if)[5]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems//ota:Vehicle) > 0");
        applyAndAssertEqualsXPath(getOp("ERR"), 1, 1,
            "(//bpel:if)[6]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems/ota:Item/ota:Hotel) > 0");
    }

    @Test
    public void errMutateHasVehicleCondition() throws Exception {
        applyAndAssertEqualsXPath(getOp("ERR"), 5, 2,
            "(//bpel:if)[1]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems/ota:Item/ota:Air) > 0");
        applyAndAssertEqualsXPath(getOp("ERR"), 5, 2,
            "(//bpel:if)[2]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:CustomerInfos/ota:CustomerInfo/ota:Customer/ota:PersonName) = 0");
        applyAndAssertEqualsXPath(getOp("ERR"), 5, 2,
            "(//bpel:if)[3]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:CustomerInfos/ota:CustomerInfo/ota:Customer/ota:Email) = 0");
        applyAndAssertEqualsXPath(getOp("ERR"), 5, 2,
            "(//bpel:if)[4]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:TravelCost/ota:FormOfPayment) = 0");
        applyAndAssertEqualsXPath(getOp("ERR"), 5, 2,
            "(//bpel:if)[5]/bpel:condition/text()",
            "(count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems//ota:Vehicle) <= 0.0)");
        applyAndAssertEqualsXPath(getOp("ERR"), 5, 2,
            "(//bpel:if)[6]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems/ota:Item/ota:Hotel) > 0");
    }

    @Test
    public void errHasAirlineConditionMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ERR"), 1, 1);
    }


    /* ELL TESTS */

    @Test
    public void ellOperandCount() throws Exception {
        assertOperandCount(0, getOp("ELL"));
    }

    @Test
    public void ellAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ELL"), 1);
    }


    /* ECC TESTS */

    @Test
    public void eccOperandCount() throws Exception {
        assertOperandCount(24, getOp("ECC"));
    }

    @Test
    public void eccAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ECC"), 1);
    }

    @Test
    public void eccMutateHasAirlineCondition() throws Exception {
        applyAndAssertEqualsXPath(getOp("ECC"), 15, 1,
            "(//bpel:if)[1]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems/ota:Item/ota:Air) > 0");
        applyAndAssertEqualsXPath(getOp("ECC"), 15, 1,
            "(//bpel:if)[2]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:CustomerInfos/ota:CustomerInfo/ota:Customer/ota:PersonName) = 0");
        applyAndAssertEqualsXPath(getOp("ECC"), 15, 1,
            "(//bpel:if)[3]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:CustomerInfos/ota:CustomerInfo/ota:Customer/ota:Email) = 0");
        applyAndAssertEqualsXPath(getOp("ECC"), 15, 1,
            "(//bpel:if)[4]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:TravelCost/ota:FormOfPayment) = 0");
        applyAndAssertEqualsXPath(getOp("ECC"), 15, 1,
            "(//bpel:if)[5]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems//ota:Vehicle) > 0");
        applyAndAssertEqualsXPath(getOp("ECC"), 15, 1,
            "(//bpel:if)[6]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems/ota:Item/ota:Hotel) > 0");
    }

    @Test
    public void eccHasAirlineConditionMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ECC"), 16, 1);
    }


    /* ECN TESTS */

    @Test
    public void ecnOperandCount() throws Exception {
        assertOperandCount(6, getOp("ECN"));
    }

    @Test
    public void ecnAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ECN"), 4);
    }

    @Test
    public void ecnMutateAirlineFormOfPaymentCondition() throws Exception {
        applyAndAssertEqualsXPath(getOp("ECN"), 4, 1,
            "(//bpel:if)[1]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems/ota:Item/ota:Air) > 0");
        applyAndAssertEqualsXPath(getOp("ECN"), 4, 1,
            "(//bpel:if)[2]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:CustomerInfos/ota:CustomerInfo/ota:Customer/ota:PersonName) = 0");
        applyAndAssertEqualsXPath(getOp("ECN"), 4, 1,
            "(//bpel:if)[3]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:CustomerInfos/ota:CustomerInfo/ota:Customer/ota:Email) = 0");
        applyAndAssertEqualsXPath(getOp("ECN"), 4, 1,
            "(//bpel:if)[4]/bpel:condition/text()",
            "(count($ItineraryIn.itinerary/ota:TravelCost/ota:FormOfPayment) = 1)");
        applyAndAssertEqualsXPath(getOp("ECN"), 4, 1,
            "(//bpel:if)[5]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems//ota:Vehicle) > 0");
        applyAndAssertEqualsXPath(getOp("ECN"), 4, 1,
            "(//bpel:if)[6]/bpel:condition/text()",
            "count($ItineraryIn.itinerary/ota:ItineraryInfo/ota:ReservationItems/ota:Item/ota:Hotel) > 0");
    }

    @Test
    public void ecnAirlineFormOfPaymentConditionMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ECN"), 4, 1);
    }


    /* EMD TESTS */

    @Test
    public void emdOperandCount() throws Exception {
        assertOperandCount(4, getOp("EMD"));
    }

    @Test
    public void emdAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("EMD"), 2);
    }

    @Test
    public void emdHalveFirstOnAlarmDuration() throws Exception {
        applyAndAssertEqualsXPath(getOp("EMD"), 2,
                                  OperatorConstants.EMD_ATTRIBUTE_HALF,
                                  "(//bpel:for)[1]", "'PT0H0M2S'");
        applyAndAssertEqualsXPath(getOp("EMD"), 2,
                                  OperatorConstants.EMD_ATTRIBUTE_HALF,
                                  "(//bpel:for)[2]", "'P0Y0M0DT0H0M5.000S'");
    }

    @Test
    public void emdSetFirstOnAlarmDurationToZero() throws Exception {
        applyAndAssertEqualsXPath(getOp("EMD"), 2,
                                  OperatorConstants.EMD_ATTRIBUTE_ZERO,
                                  "(//bpel:for)[1]", "'PT0H0M2S'");
        applyAndAssertEqualsXPath(getOp("EMD"), 2,
                                  OperatorConstants.EMD_ATTRIBUTE_ZERO,
                                  "(//bpel:for)[2]", "'P0Y0M0DT0H0M0.000S'");
    }

    @Test
    public void emdSetFirstAlarmToZeroMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EMD"), 2,
                                  OperatorConstants.EMD_ATTRIBUTE_ZERO);
    }


    /* EMF TESTS */

    /* In this case, it is not possible to ckeck mutants */
    @Test
    public void emfOperandCount() throws Exception {
        assertOperandCount(0, getOp("EMF"));
    }

    @Test
    public void emfAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("EMF"), 1);
    }


    /* ACI TESTS */

    @Test
    public void aciOperandCount() throws Exception {
        assertOperandCount(0, getOp("ACI"));
    }

    @Test
    public void aciAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ACI"), 1);
    }


    /* AFP TESTS */

    @Test
    public void afpOperandCount() throws Exception {
        assertOperandCount(0, getOp("AFP"));
    }

    @Test
    public void afpAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("AFP"), 1);
    }


    /* ASF TESTS */

    @Test
    public void asfOperandCount() throws Exception {
        assertOperandCount(12, getOp("ASF"));
    }

    @Test
    public void asfAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("ASF"), 1);
    }

    @Test
    public void asfMutateMainSequenceToFlow() throws Exception {
        applyAndAssertEqualsXPath(getOp("ASF"), 3, 0,
                "/bpel:process/bpel:faultHandlers/bpel:catchAll/bpel:sequence/@name",
                "faultHandlersSequence");
        applyAndAssertEqualsXPath(getOp("ASF"), 3, 0,
                "count(//bpel:flow/bpel:assign[@name = 'AirlineCustomerPersonNameHandleError'])",
                "1");
    }

    @Test
    public void asfMainSequenceMutantIsDifferent() throws Exception {
    	assertSuccessfulMutationIsEquivalent(TEST_SUITE, getOp("ASF"), 3, 0);
        //assertMutantIsInvalid(TEST_SUITE, getOp("ASF"), 3, 0);
    }


    /* AIS TESTS */

    @Test
    public void aisOperandCount() throws Exception {
        assertOperandCount(0, getOp("AIS"));
    }

    @Test
    public void aisAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("AIS"), 1);
    }


    /* AIE TESTS */

    @Test
    public void aieOperandCount() throws Exception {
        assertOperandCount(0, getOp("AIE"));
    }

    @Test
    public void aieAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("AIE"), 1);
    }


    /* AWR TESTS */

    @Test
    public void awrOperandCount() throws Exception {
        assertOperandCount(0, getOp("AWR"));
    }

    @Test
    public void awrAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("AWR"), 1);
    }


    /* AJC TESTS */

    @Test
    public void ajcOperandCount() throws Exception {
        assertOperandCount(0, getOp("AJC"));
    }

    @Test
    public void ajcAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("AJC"), 1);
    }


    /* ASI TESTS */

    @Test
    public void asiOperandCount() throws Exception {
        assertOperandCount(33, getOp("ASI"));
    }

    @Test
    public void asiMutateMainSequence() throws Exception {
        applyAndAssertEqualsXPath(getOp("ASI"), 5, 1,
            "/bpel:process/bpel:sequence/*[1]/@name",
            "ReceiveItinerary");
        applyAndAssertEqualsXPath(getOp("ASI"), 5, 1,
            "/bpel:process/bpel:sequence/*[2]/@name",
            "HasAirline");
        applyAndAssertEqualsXPath(getOp("ASI"), 5, 1,
            "/bpel:process/bpel:sequence/*[3]/@name",
            "CopyItineraryIn");
    }

    @Test
    public void asiMainSequenceMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("ASI"), 7, 1);
    }


    /* APM TESTS */

    @Test
    public void apmOperandCount() throws Exception {
        assertOperandCount(0, getOp("APM"));
    }

    @Test
    public void apmAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("APM"), 1);
    }


    /* APA TESTS */

    @Test
    public void apaOperandCount() throws Exception {
        assertOperandCount(3, getOp("APA"));
    }

    @Test
    public void apaAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("APA"), 1);
    }

    @Test
    public void apaMutateAirlineCancelAlarm() throws Exception {
        applyAndAssertEqualsXPath(getOp("APA"), 1, 0,
                                  "count(//bpel:if[1]//bpel:onAlarm)", "0");
        applyAndAssertEqualsXPath(getOp("APA"), 1, 0,
                                  "count(//bpel:if[2]//bpel:onAlarm)", "1");
        applyAndAssertEqualsXPath(getOp("APA"), 1, 0,
                                  "count(//bpel:if[3]//bpel:onAlarm)", "1");
    }

    @Test
    public void apaMutateVehicleCancelAlarm() throws Exception {
        applyAndAssertEqualsXPath(getOp("APA"), 3, 0,
                                  "count(//bpel:if[1]//bpel:onAlarm)", "1");
        applyAndAssertEqualsXPath(getOp("APA"), 3, 0,
                                  "count(//bpel:if[2]//bpel:onAlarm)", "1");
        applyAndAssertEqualsXPath(getOp("APA"), 3, 0,
                                  "count(//bpel:if[3]//bpel:onAlarm)", "0");
    }

    @Test
    public void apaNoAirlineCancelMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("APA"), 1, 0);
    }

    @Test
    public void apaNoHotelCancelMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("APA"), 3, 0);
    }


    /* XMF TESTS */

    @Test
    public void xmfOperandCount() throws Exception {
        assertOperandCount(5, getOp("XMF"));
    }

    @Test
    public void xmfAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("XMF"), 1);
    }

    @Test
    public void xmfMutateProcessFaultHandlersCatchAll() throws Exception {
        applyAndAssertEqualsXPath(getOp("XMF"), 1, 0,
                                  "count(/bpel:process/bpel:faultHandlers)", "0");
        applyAndAssertEqualsXPath(getOp("XMF"), 1, 0,
                                  "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catch)", "3");
        applyAndAssertEqualsXPath(getOp("XMF"), 1, 0,
                                  "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catchAll)", "1");
    }

    @Test
    public void xmfMutateAirlineScopeFaultHandlersCatch() throws Exception {
        applyAndAssertEqualsXPath(getOp("XMF"), 2, 0,
                                  "count(/bpel:process/bpel:faultHandlers)", "1");
        applyAndAssertEqualsXPath(getOp("XMF"), 2, 0,
                                  "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catch)", "2");
        applyAndAssertEqualsXPath(getOp("XMF"), 2, 0,
                                  "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catchAll)", "1");
    }

    @Test
    public void xmfMutateAirlineScopeFaultHandlersCatchAll() throws Exception {
        applyAndAssertEqualsXPath(getOp("XMF"), 5, 0,
                                  "count(/bpel:process/bpel:faultHandlers)", "1");
        applyAndAssertEqualsXPath(getOp("XMF"), 5, 0,
                                  "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catch)", "3");
        applyAndAssertEqualsXPath(getOp("XMF"), 5, 0,
                                  "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catchAll)", "0");
    }

    @Test
    public void xmfNoProcessFaultHandlersCatchAllMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("XMF"), 1, 0);
    }

    @Test
    public void xmfNoAirlineScopeFaultHandlersCatchMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("XMF"), 2, 0);
    }

    /* XMC TESTS */

    @Test
    public void xmcOperandCount() throws Exception {
        assertOperandCount(0, getOp("XMC"));
    }

    @Test
    public void xmcAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("XMC"), 1);
    }


    /* XMT TESTS */

    @Test
    public void xmtOperandCount() throws Exception {
        assertOperandCount(1, getOp("XMT"));
    }

    @Test
    public void xmtAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("XMT"), 1);
    }

    @Test
    public void xmtMutate() throws Exception {
        applyAndAssertEqualsXPath(getOp("XMT"), 1, 0,
                                  "count(//bpel:terminationHandler)", "0");
    }

    @Test
    public void xmtMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("XMT"), 1, 0);
    }


    /* XTF TESTS */

    @Test
    public void xtfOperandCount() throws Exception {
        assertOperandCount(6, getOp("XTF"));
    }

    @Test
    public void xtfAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("XTF"), 1);
    }

    @Test
    public void xtfMutateCustomerPersonNameFaultName() throws Exception {
        applyAndAssertEqualsXPath(getOp("XTF"), 1, 1,
                       "(//bpel:throw)[1]/@faultName", "AirlineCustomerEmailDoesNotExist");
        applyAndAssertEqualsXPath(getOp("XTF"), 1, 1,
                       "(//bpel:throw)[2]/@faultName", "AirlineCustomerEmailDoesNotExist");
        applyAndAssertEqualsXPath(getOp("XTF"), 1, 1,
                       "(//bpel:throw)[3]/@faultName", "AirlineFormOfPaymentDoesNotExist");
    }

    @Test
    public void xtfCustomerPersonNameFaultNameMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("XTF"), 1, 1);
    }

    /* XER TESTS */

    @Test
    public void xerOperandCount() throws Exception {
        assertOperandCount(4, getOp("XER"));
    }

    @Test
    public void xerAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("XER"), 1);
    }

    @Test
    public void xerMutateAirlineFormOfPaymentDoesNotExistRethrow() throws Exception {
        applyAndAssertEqualsXPath(getOp("XER"), 1, 0,
                                  "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catch[@faultName='AirlineCustomerPersonNameDoesNotExist']//bpel:rethrow)", "0");
        applyAndAssertEqualsXPath(getOp("XER"), 1, 0,
                "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catch[@faultName='AirlineCustomerEmailDoesNotExist']//bpel:rethrow)", "1");
        applyAndAssertEqualsXPath(getOp("XER"), 1, 0,
                "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catch[@faultName='AirlineFormOfPaymentDoesNotExist']//bpel:rethrow)", "1");
        applyAndAssertEqualsXPath(getOp("XER"), 1, 0,
                                  "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catchAll//bpel:rethrow)", "1");
    }

    @Test
    public void xerMutateAirlineRethrow() throws Exception {
        applyAndAssertEqualsXPath(getOp("XER"), 4, 0,
                "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catch[@faultName='AirlineCustomerPersonNameDoesNotExist']//bpel:rethrow)", "1");
        applyAndAssertEqualsXPath(getOp("XER"), 4, 0,
                "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catch[@faultName='AirlineCustomerEmailDoesNotExist']//bpel:rethrow)", "1");
        applyAndAssertEqualsXPath(getOp("XER"), 4, 0,
                "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catch[@faultName='AirlineFormOfPaymentDoesNotExist']//bpel:rethrow)", "1");
        applyAndAssertEqualsXPath(getOp("XER"), 4, 0,
                "count(//bpel:scope[@name='AirlineScope']/bpel:faultHandlers/bpel:catchAll//bpel:rethrow)", "0");
    }

    @Test
    public void xerNoAirlineFormOfPaymentDoesNotExistRethrowMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("XER"), 1, 0);
    }


    /* XEE TESTS */

    @Test
    public void xeeOperandCount() throws Exception {
        assertOperandCount(1, getOp("XEE"));
    }

    @Test
    public void xeeAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("XEE"), 1);
    }

    @Test
    public void xeeMutateAirlineEvent() throws Exception {
        applyAndAssertEqualsXPath(getOp("XEE"), 1, 0,
                                  "count(/bpel:process/bpel:eventHandlers)", "0");
    }

    @Test
    public void xeeNoAirlineEventMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("XEE"), 1, 0);
    }


    /* AEL TESTS */

    @Test
    public void aelOperandCount() throws Exception {
        assertOperandCount(58, getOp("AEL"));
    }

    @Test
    public void aelAttributeRange() throws Exception {
        assertMaximumDistinctAttributeValueIs(getOp("AEL"), 1);
    }

    @Test
    public void aelMutateFaultHandlersSequence() throws Exception {
        applyAndAssertEqualsXPath(getOp("AEL"), 1, 0,
                "count(/bpel:process/bpel:faultHandlers/bpel:catchAll/bpel:sequence)", "0");
        applyAndAssertEqualsXPath(getOp("AEL"), 1, 0,
                "count(/bpel:process/bpel:faultHandlers/bpel:catchAll/bpel:empty)", "1");
    }

    @Test
    public void aelMutateFaultHandlersCompensate() throws Exception {
        applyAndAssertEqualsXPath(getOp("AEL"), 2, 0,
                "count(/bpel:process/bpel:faultHandlers/bpel:catchAll/bpel:sequence/bpel:compensate)", "0");
        applyAndAssertEqualsXPath(getOp("AEL"), 2, 0,
                "count(/bpel:process/bpel:faultHandlers/bpel:catchAll/bpel:sequence/bpel:assign)", "1");
        applyAndAssertEqualsXPath(getOp("AEL"), 2, 0,
                "count(/bpel:process/bpel:faultHandlers/bpel:catchAll/bpel:sequence/bpel:reply)", "1");
    }

    @Test
    public void aelNoFaultHandlersSequenceMutantIsDifferent() throws Exception {
        assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("AEL"), 1, 0);
    }
    
    /* EIU TESTS */
    
    @Test
    public void eiuOperandCount() throws Exception {
    	assertOperandCount(0, getOp("EIU"));
    }
    
    /* EIN TESTS */
    
    @Test
    public void einOperandCount() throws Exception {
    	assertOperandCount(6, getOp("EIN"));
    }
    
    @Test
    public void einChangeIfCustomerEmailDoesNotExist() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("EIN"),
                                  3,
                                  1,
                                  "//bpel:if[@name='CustomerEmailDoesNotExist']/bpel:condition/text()",
                                  "not((count($ItineraryIn.itinerary/ota:CustomerInfos/" +
                                  "ota:CustomerInfo/ota:Customer/ota:Email) = 0.0))");
    }
    
    @Test
    public void einMutatesIfCustomerEmailDoesNotExist() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("EIN"), 3, 1);
    }
    
    /* EAP TESTS */
    
    @Test
    public void eapOperandCount() throws Exception {
    	assertOperandCount(0, getOp("EAP"));
    }
    
    /* EAN TESTS */
    
    @Test
    public void eanOperandCount() throws Exception {
    	assertOperandCount(0, getOp("EAN"));
    }
    
    /* CFA TESTS */
    
    @Test
    public void cfaOperandCount() throws Exception {
    	assertOperandCount(58, getOp("CFA"));
    }
    
    @Test
    public void cfaChangeThrowAirlineCustomerPersonNameDoesNotExist() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CFA"),
                                  30,
                                  1,
                                  "count(//bpel:throw[@name='AirlineCustomerPersonNameDoesNotExist']) = 0 and " +
                                  "count(//bpel:if[@name='CustomerPersonNameDoesNotExist']/bpel:exit) = 1",
                                  "true");
    }
    
    @Test
    public void cfaMutatesChangeThrowAirlineCustomerPersonNameDoesNotExist() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CFA"), 30, 1);
    }
    
    /* CDE TESTS */
    
    @Test
    public void cdeOperandCount() throws Exception {
    	assertOperandCount(6, getOp("CDE"));
    }
    
    @Test
    public void cdeChangeIfHasVehicle() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDE"),
                                  5,
                                  2,
                                  "//bpel:if[@name='HasVehicle']/bpel:condition/text()",
                                  "false()");
    }
    
    @Test
    public void cdeMutatesIfHasVehicle() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDE"), 5, 2);
    }
    
    /* CCO TESTS */
    
    @Test
    public void ccoOperandCount() throws Exception {
    	assertOperandCount(6, getOp("CCO"));
    }
    
    @Test
    public void ccoChangeIfHasHotel() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CCO"),
                                  6,
                                  1,
                                  "//bpel:if[@name='HasHotel']/bpel:condition/text()",
                                  "true()");
    }
    
    @Test
    public void ccoMutatesIfHasHotel() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDE"), 6, 1);
    }
    
    /* CDC TESTS */
    
    @Test
    public void cdcOperandCount() throws Exception {
    	assertOperandCount(6, getOp("CDC"));
    }
    
    @Test
    public void cdcChangeIfHasAirline() throws Exception {
        applyAndAssertEqualsXPath(
                                  getOp("CDC"),
                                  1,
                                  1,
                                  "//bpel:if[@name='HasAirline']/bpel:condition/text()",
                                  "true()");
    }
    
    @Test
    public void cdcMutatesIfHasAirline() throws Exception {
    	assertSuccessfulMutationIsDifferent(TEST_SUITE, getOp("CDC"), 1, 1);
    }
}
