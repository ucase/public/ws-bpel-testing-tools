package es.uca.webservices.mutants.subcommands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import net.bpelunit.framework.model.test.report.ArtefactStatus.StatusCode;

import org.activebpel.rt.jetty.AeJettyForker;
import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.bpelunit.BPELUnitXMLResult;
import es.uca.webservices.bpel.bpelunit.CustomizedRunner;
import es.uca.webservices.bpel.io.util.FileUtils;
import es.uca.webservices.mutants.subprocesses.ProcessUtils;

/**
 * Tests for the 'run' subcommand.
 */
public class RunSubcommandTest {

	static final String PATH_DEPENDENCY_ODE = "target/dependency/ode-axis2-war-1.3.8.war";

	private static final String BPEL = "src/test/resources/tacService/tacService.bpel";
	private static final String BPTS = "src/test/resources/tacService/tacService.bpts";
	private static final String BPELODE = "src/test/resources/LoanApprovalRPC_ODE/loanApprovalProcess.bpel";
	private static final String BPTSODE = "src/test/resources/LoanApprovalRPC_ODE/loanApprovalProcess.bpts";
	
	private File fTmpStdout, fTmpStderr;

	private AeJettyForker fRunner = null;
	@Before
	public void setUp() throws FileNotFoundException {
		fTmpStdout = new File("tmp.xml");
		fTmpStderr = new File("tmp.err.xml");
	}

	@After
	public void tearDown() {
		fTmpStdout.delete();
		fTmpStderr.delete();

		try {
			int i = 0;
			while (isPortUsed(8080) && i < 3) {
				System.err.println("Waiting 1s for port 8080 to be available");
				Thread.sleep(1_000);
				i++;
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Tests that 'run bpts bpel' with no other options works and doesn't hang.
	 * This assumes that no ActiveBPEL instance is currently running at the
	 * default port.
	 */
	@Test
	public void defaultOptionsWork() throws Exception {
		assertInvocationWithSingleCompositionWorks(
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, PATH_DEPENDENCY_ODE,
				BPTSODE, BPELODE);
	}

	@Test
	public void usingExistingActiveBPELWorks() throws Exception {
		fRunner = new AeJettyForker(null, 8080, CustomizedRunner.NONE_LOGLEVEL);
		fRunner.start();
		
		RunSubcommand cmd = new RunSubcommand();
		cmd.setEngineType("activebpel");
		cmd.setBPRDir(fRunner.getBPRDirectory());
		FileOutputStream fos = new FileOutputStream(fTmpStdout); 
		cmd.setOutputStream(fos);
		cmd.run(BPTS, BPEL);
		assertFileIsPassedBPELUnitResult(fTmpStdout);
		
		fRunner.stop();
	}

	/**
	 * Tests that '--engine-port' option is properly interpreted. To do so, it
	 * monopolizes port 8080 (the default) so we make sure that ActiveBPEL is
	 * using some other port.
	 */
	@Test
	public void changingEnginePortWorks() throws Exception {
		final int port = CustomizedRunner.DEFAULT_ENGINE_PORT;
		assertCmdWorksWithPortTaken(port, new String[] {
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, PATH_DEPENDENCY_ODE,
				"--" + ExecutionSubcommand.ENGINE_PORT_OPTION,
				Integer.toString(port + 1), BPTSODE, BPELODE});
	}

	/**
	 * Tests that '--mockup-port' is properly interpreted.
	 */
	@Test
	public void changingMockupPortWorks() throws Exception {
		final int port = CustomizedRunner.DEFAULT_BPELUNIT_PORT;
		assertCmdWorksWithPortTaken(port, new String[] {
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, PATH_DEPENDENCY_ODE,
				"--" + ExecutionSubcommand.MOCKUP_PORT_OPTION,
				Integer.toString(port + 1), BPTSODE, BPELODE });
	}

	/**
	 * Tests that '--work-directory' and '--logging-level' work, when loglevel
	 * is NONE.
	 */
	@Test
	public void changingWorkDirectoryWorksWithoutLogging() throws Exception {
		assertChangingWorkDirectoryWorks(CustomizedRunner.NONE_LOGLEVEL);
	}

	/**
	 * Tests that '--work-directory' and '--logging-level' work, when loglevel
	 * is FULL.
	 */
	@Test
	public void changingWorkDirectoryWorksWithLogging() throws Exception {
		assertChangingWorkDirectoryWorks(CustomizedRunner.FULL_LOGLEVEL);
	}

	/**
	 * Tests that forcing the filelist format for running a single composition works.
	 */
	@Test
	public void runningSingleProcessWithFileListFormatWorks() throws Exception {
        // Don't let the RunCommand clean up the log files immediately - we need to check some of its output
        File tmpWorkdir = FileUtils.createTemporaryDirectory("mubpeltest", ".dir");
        try {
            assertInvocationWithMultipleCompositionArgumentsWorks(5, 5, 1,
            		"--" + ExecutionSubcommand.ODE_WAR_OPTION, PATH_DEPENDENCY_ODE,
            		"--" + RunSubcommand.BPEL_LOGLEVEL_OPTION, CustomizedRunner.FULL_LOGLEVEL,
                    "--" + RunSubcommand.LISTFORMAT_OPTION,
                    "--" + RunSubcommand.WORKDIR_OPTION, tmpWorkdir.getAbsolutePath(),
                    BPTSODE, BPELODE);
        } finally {
            org.apache.commons.io.FileUtils.deleteDirectory(tmpWorkdir);
        }
	}

	/**
	 * Tests that passing multiple BPEL processes through the command line
	 * produces the expected list of lines with locations of XML results and execution
	 * logs, separated by whitespace.
	 */
	@Test
	public void runningMultipleProcessesInSequenceWorks() throws Exception {
        File tmpWorkdir = FileUtils.createTemporaryDirectory("mubpeltest", ".dir");
        try {
            assertInvocationWithMultipleCompositionArgumentsWorks(5, 5, 2,
            		"--" + ExecutionSubcommand.ODE_WAR_OPTION, PATH_DEPENDENCY_ODE,
            		"--" + RunSubcommand.BPEL_LOGLEVEL_OPTION, CustomizedRunner.FULL_LOGLEVEL,
                    "--" + ExecutionSubcommand.WORKDIR_OPTION,
                    tmpWorkdir.getAbsolutePath(), BPTSODE, BPELODE, BPELODE);
        } finally {
            org.apache.commons.io.FileUtils.deleteDirectory(tmpWorkdir);
        }
	}

	/**
	 * Tests that passing multiple BPEL processes through the command line with
	 * logging disabled only produces the locations of the XML results.
	 */
	@Test
	public void runningMultipleProcessesInSequenceWithoutLoggingProducesOnlyXMLListing() throws Exception {
        assertInvocationWithMultipleCompositionArgumentsWorks(0, 5, 2,
        		"--" + ExecutionSubcommand.ODE_WAR_OPTION, PATH_DEPENDENCY_ODE,
                "--" + ExecutionSubcommand.BPEL_LOGLEVEL_OPTION, CustomizedRunner.NONE_LOGLEVEL,
                BPTSODE, BPELODE, BPELODE);
    }

	/**
	 * Tests that passing multiple BPEL processes through the command line with
	 * maximum size set to 0 bytes only produces the locations of the XML
	 * results.
	 */
	@Test
	public void runningMultipleProcessesInSequenceWithZeroMaxSizeProducesOnlyXMLListing()
			throws Exception {
		assertInvocationWithMultipleCompositionArgumentsWorks(0, 5, 2,
				new String[] { "--" + ExecutionSubcommand.ODE_WAR_OPTION, PATH_DEPENDENCY_ODE,
						"--" + RunSubcommand.MAXTRACE_OPTION, "0", BPTSODE, BPELODE, BPELODE });
	}

	/**
	 * Tests that passing a single BPEL process through stdin produces the
	 * expected list of locations of XML results and execution logs.
	 */
	@Test
	public void passingSingleProcessThroughStdinWorks() throws Exception {
		assertInvocationThroughStdinWorks(BPELODE);
	}

	/**
	 * Tests that passing several BPEL processes through stdin produces the
	 * expected list of locations of XML results and execution logs.
	 */
	@Test
	public void passingMultipleProcessesThroughStdinWorks() throws Exception {
		assertInvocationThroughStdinWorks(BPELODE, BPELODE);
	}

	/**
	 * Tests that running several times a composition in parallel works
	 * properly, when passing the composition through the command line.
	 */
	@Test
	public void parallelRunThroughCommandLineArgsWorks() throws Exception {
        RunSubcommand cmd = new RunSubcommand();
		cmd.parseArgs(
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, PATH_DEPENDENCY_ODE,
				"--" + RunSubcommand.BPEL_LOGLEVEL_OPTION, CustomizedRunner.FULL_LOGLEVEL,
				"--" + ExecutionSubcommand.PARALLEL_OPTION,
				"--", BPTSODE, BPELODE, BPELODE);
        cmd.setCleanupAfterExecution(false);
		cmd.run();
		assertFileListOutputIsCorrect(cmd);
        cmd.cleanup();
	}

	/**
	 * Tests that running several times a composition in parallel works
	 * properly, when passing the composition through stdin.
	 */
	@Test
	public void parallelRunThroughStreamWorks() throws Exception {
		RunSubcommand cmd = new RunSubcommand();
		cmd.setPathToOdeWar(new File(PATH_DEPENDENCY_ODE));
		cmd.setActiveBPELLogLevel(CustomizedRunner.FULL_LOGLEVEL);
		cmd.setThreadCount(2);
		StringReader reader = new StringReader(BPELODE + "\n" + BPELODE);
		cmd.run(BPTSODE, reader);
		assertFileListOutputIsCorrect(cmd);
	}

	private void assertFileListOutputIsCorrect(RunSubcommand cmd)
			throws ParserConfigurationException, SAXException, IOException,
			XPathExpressionException {
		List<List<String>> results = cmd.getResults();
		for (List<String> lineParts : results) {
			assertFileListOutputLineIsCorrect(5, 5, lineParts);
		}
	}

	private void assertInvocationThroughStdinWorks(String... inputLines)
			throws IOException, InterruptedException,
			ParserConfigurationException, SAXException,
			XPathExpressionException
    {
        File tmpWorkdir = FileUtils.createTemporaryDirectory("mubpeltest", ".dir");
        try {
            // Start the process and dump stderr to a file
            final Process p = ProcessUtils.callJavaClass(RunSubcommand.class,
            		"--" + ExecutionSubcommand.ODE_WAR_OPTION, PATH_DEPENDENCY_ODE,
                       "--" + RunSubcommand.BPEL_LOGLEVEL_OPTION, CustomizedRunner.FULL_LOGLEVEL,
                    "--" + ExecutionSubcommand.WORKDIR_OPTION,
                    tmpWorkdir.getAbsolutePath(), BPTSODE, "-");
            Thread t = this.startStderrDumpThread(p.getErrorStream());

            // Print the BPEL file to be run through the process' stdin
            PrintStream in = new PrintStream(p.getOutputStream());
            for (String inputLine : inputLines) {
                in.println(inputLine);
            }
            in.close();

            // Read the file list output from the process' stdout and check it
            List<String> lines = collectStdoutLinesFromInvocation(p, t);
            assertFileListOutputIsCorrect(lines, 5, 5, inputLines.length);
        } finally {
            org.apache.commons.io.FileUtils.deleteDirectory(tmpWorkdir);
        }
    }

	/* PRIVATE METHODS */

	private void assertFileListOutputIsCorrect(final List<String> lines,
			final int expectedLogs, final int expectedTimes,
			final int expectedLines) throws IOException, InterruptedException,
			ParserConfigurationException, SAXException,
			XPathExpressionException {
		assertEquals("There should be as many output lines as compositions", expectedLines, lines.size());
		for (String line : lines) {
			assertFileListOutputLineIsCorrect(
					expectedLogs,
					expectedTimes,
					Arrays.asList(line.split("\\s")));
		}
	}

	private void assertFileListOutputLineIsCorrect(final int expectedLogs, final int expectedTimes, List<String> parts)
			throws ParserConfigurationException, SAXException, IOException,
			XPathExpressionException
	{
		// There should be the path to the BPELUnit results, the paths
		// to the .log files produced by ActiveBPEL, and the test times
		// in nanoseconds
		assertEquals(1 + expectedLogs + expectedTimes, parts.size());

		// First part should be the path to a BPELUnit XML result file
		assertFileIsPassedBPELUnitResult(new File(parts.get(0)));

		// Next should be the .log files produced by ActiveBPEL
		if (expectedLogs > 0) {
			for (String logPath : parts.subList(1, expectedLogs)) {
				assertTrue(logPath.endsWith(".log"));
				assertTrue(new File(logPath).canRead());
			}
		}

		// Finally, test times in nanoseconds
		if (expectedTimes > 0) {
			for (String time : parts.subList(expectedLogs + 1, parts.size())) {
				assertTrue(Long.parseLong(time) >= 0);
			}
		}
	}

	private List<String> collectStdoutLinesFromInvocation(Process p, Thread t)
			throws IOException, InterruptedException {
		List<String> lines = new ArrayList<String>();
		BufferedReader bR = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while ((line = bR.readLine()) != null) {
			if (!lineIsFromCobertura(line)) {
				lines.add(line);
			}
		}

		assertEquals(0, p.waitFor());
		t.join();
		return lines;
	}

	private void assertInvocationWithMultipleCompositionArgumentsWorks(
			final int expectedLogs, final int expectedTimes,
			final int expectedLines, String... args) throws Exception {
		final Process p = ProcessUtils.callJavaClass(RunSubcommand.class, args);
		Thread t = this.startStderrDumpThread(p.getErrorStream());
		List<String> lines = collectStdoutLinesFromInvocation(p, t);
		assertFileListOutputIsCorrect(lines, expectedLogs, expectedTimes, expectedLines);
	}

	private void assertInvocationWithSingleCompositionWorks(String... args)
			throws IOException, InterruptedException,
			ParserConfigurationException, SAXException,
			XPathExpressionException {
		
		// Start the process
		final Process proc = ProcessUtils.callJavaClass(RunSubcommand.class, args);

		// Close stdin and dump stderr and stdout to files
		proc.getOutputStream().close();
		Thread t = startStderrDumpThread(proc.getErrorStream());
		dumpStreamToFile(proc.getInputStream(), fTmpStdout, "UTF-8");
		assertEquals(0, proc.waitFor());
		t.join();

		// Ensure the dump is a valid BPELUnit XML result, and that the tests pass
		assertFileIsPassedBPELUnitResult(fTmpStdout);
	}

	private void assertFileIsPassedBPELUnitResult(final File resultFile)
			throws ParserConfigurationException, SAXException, IOException,
			XPathExpressionException {
		BPELUnitXMLResult xmlResult = new BPELUnitXMLResult(resultFile);
		assertEquals(StatusCode.PASSED.toString(), xmlResult.getGlobalStatusCode());
	}

	private Thread startStderrDumpThread(final InputStream errorStream) {
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					IOUtils.copy(errorStream, System.err);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
		return t;
	}

	private void dumpStreamToFile(InputStream is, File file, String encoding)
			throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is, encoding));
		FileWriter writer = new FileWriter(file);

		String line;
		while ((line = reader.readLine()) != null) {
			if (lineIsFromCobertura(line)) {
				// We need to filter away Cobertura's messages, so it doesn't break the tests
				continue;
			}
			writer.write(line);
			writer.write('\n');
		}
		reader.close();
		writer.close();
	}

	private boolean lineIsFromCobertura(String line) {
		return (line.startsWith("Flushing") || line.startsWith("Cobertura"));
	}

	private File assertChangingWorkDirectoryWorks(String logLevel)
			throws IOException, Exception, ParserConfigurationException,
			SAXException, XPathExpressionException {
		File tmpDir = FileUtils.createTemporaryDirectory("test", "dir");
		assertInvocationWithSingleCompositionWorks(
				"--" + ExecutionSubcommand.ODE_WAR_OPTION, PATH_DEPENDENCY_ODE,
				"--" + ExecutionSubcommand.WORKDIR_OPTION,
				tmpDir.getCanonicalPath(),
				"--" + ExecutionSubcommand.BPEL_LOGLEVEL_OPTION,
				logLevel,
				BPTSODE, BPELODE);
		assertTrue(new File(tmpDir, "tomcat.log").exists());
		return tmpDir;
	}

	private void assertCmdWorksWithPortTaken(final int port, final String[] args)
			throws IOException, Exception, ParserConfigurationException,
			SAXException, XPathExpressionException {
		final ServerSocketChannel serverChannel = ServerSocketChannel.open();
		serverChannel.configureBlocking(false);
		serverChannel.socket().bind(new InetSocketAddress(port));

		try {
			assertInvocationWithSingleCompositionWorks(args);
		} finally {
			serverChannel.close();
		}
	}
	
	private static boolean isPortUsed(int port) throws IOException {
		try {
			final SelectChannelConnector conn = new SelectChannelConnector();
			conn.setPort(port);
			conn.open();
			conn.close();
			return false;
		} catch (BindException ex) {
			return true;
		}
	}
}
