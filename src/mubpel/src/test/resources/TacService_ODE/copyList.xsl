<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform" 
xmlns:p="http://xml.netbeans.org/schema/tacService">
  <template match="p:tacRequest">
    <p:tacResponse>
	<apply-templates/>
    </p:tacResponse>
  </template>
  <template match="p:lines">
    <p:lines>
      <for-each select="p:line">
      <sort select="position()" data-type="number" order="descending"/>
	<p:line><value-of select="." /></p:line>
      </for-each>
    </p:lines>
  </template>
</stylesheet>