MuBPEL changelog
================

Staged for next version
-----------------------

Nothing yet: stay tuned!

Version 1.3.0 (2013/04/09)
--------------------------

* _Added_: added "--column-headers" option to "compare".
* _Added_: added "--console-level" option.
* _Added_: limited support for ODE (no embedded server for now, but .bpts files using the ODE deployer should work).
* _Added_: new subcommand "bpr" for creating the ActiveBPEL ".bpr" archive from a ".bpel" file. This ".bpr" assumes that the user will only be calling the BPELUnit mockups and not real services.
* _Added_: new subcommand "count" for reporting how many mutants can be produced from a ".bpel" or ".bpr" file.
* _Change_: by default, test cases now stop as soon as the first different message is found, instead of going through all the messages in the test. Test suites still work the same as usual, depending on whether the "-k" option is set (either explicitly or through "comparefull") or not.
* _Change_: create an OSGi-compatible version.
* _Change_: fix "analyze" for Windows systems.
* _Change_: fix EMD when using double quotes.
* _Change_: handle ".bpr" ActiveBPEL deployment archives in addition to raw ".bpel" processes. This affects "apply", "applyall", "compare", "comparefull" and "run".
* _Change_: handle non-XPath query languages more gracefully.
* _Change_: make EAN, EAP and EIU ignore zeros to avoid producing equivalent mutants.
* _Change_: rename "--loglevel" option to "--logfile-level".
* _Change_: rename program from "mutationtool" to "MuBPEL" (program) and "mubpel" (launcher script).
* _Change_: simulation records now record BPELUnit test case names.
* _Change_: switch to bpel-packager 1.2.1 and ActiveBPEL 4.1-uca12, which includes a fix for JAXEN-227, can limit CPU usage by process and fixes the ActiveBPEL management page in the embedded ActiveBPEL server.
* _Change_: the logfile has INFO as a minimum logging level threshold, and the console has FATAL.
* _Change_: update launch script to make it work on Macs (BSD readlink does not support the "-f" option from GNU readlink). Advanced users may want to replace their readlink with greadlink.

Version 1.2.1 (2011/06/02)
------------------------

* _Change_: print exact build timestamp in the help message.
* _Fix_: fix regression in "mubpel comparefull", which was working like "mubpel compare" instead of like "mubpel compare -k".

Version 1.2 (2011/03/02)
-----------------------

* _Change_: mubpel has gotten a big facelift and can now print detailed help for each subcommand. Each subcommand can now also take options, in addition to their usual arguments.
* _Change_: "mubpel run" and "mubpel compare" can now be parallelized and bring up embedded ActiveBPEL instances.
* _Fix_: fixed bug in "mubpel compare" which wrongly reported some surviving mutants as dead.

Version 1.1 (2010/06/16)
------------------------

* _Added_: implemented EMF, XEE, XER, XMT and XTF.
* _Change_: removed XRF, as it produced too many stillborn mutants.
* _Change_: renamed CEA to AEL, according to the latest publications.
* _Change_: refined ECN not to produce repeated mutants when the constant to be mutated is 0 or 1.
* _Change_: ASI, ISV and XTF no longer use the attribute field. Every possible change is encoded as a separate operand, to avoid generating repeated mutants when the effective attribute range for an operand is smaller than the maximum attribute range.
* _Change_: refined ASI to exchange non-adjacent activities.
* _Change_: refined XMF to remove the <faultHandlers> element when removing the last remaining <faultHandler>, to avoid producing a stillborn mutant.
* _Change_: speed up "mubpel compare". The old comprehensive comparison is available in "mubpel comparefull".
* _Change_: do not take prefixes and namespace nodes when comparing SOAP messages.
* _Change_: depend on BPELUnit to correctly and quickly report invalid compositions, instead of detecting them after invocation.
* _Change_: do not reuse types.xml.
* _Change_: use 64-bit integers for the fields of each individual, as the least common multiples in LoanApprovalExtended overflowed the 32-bit fields which were being used.
* _Fix_: messages from asynchronous send/receive and receive/send activities were not being used for comparison.
* _Fix_: fix off-by-one error in the operator number for 'mubpel apply' and ensure all arguments start counting on 1 (the operand argument was starting on 0 by mistake).

Version 1.0 (2010/04/12)
------------------------

* First stable release.
