package es.uca.webservices.xpath.parser;

public @SuppressWarnings("all")
class ASTOnlyTestNamespaceNode extends SimpleNodeWithShallowCopy {
    private String prefix;

    public ASTOnlyTestNamespaceNode(int id) {
        super(id);
    }

    public ASTOnlyTestNamespaceNode(XPathParser p, int id) {
        super(p, id);
    }

    /** Accept the visitor. **/
    public Object jjtAccept(XPathParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }

    /**
     * @param prefix
     *            the prefix to set
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }

    public String toString() {
        return String.format("AnyNamespaceNode (prefix: '%s')", getPrefix());
    }

    public ASTOnlyTestNamespaceNode shallowCopy() {
        ASTOnlyTestNamespaceNode copy
            = (ASTOnlyTestNamespaceNode)super.shallowCopy();
        copy.setPrefix(this.getPrefix());
        return copy;
    }

}
