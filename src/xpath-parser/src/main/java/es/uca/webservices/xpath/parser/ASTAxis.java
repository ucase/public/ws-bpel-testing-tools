package es.uca.webservices.xpath.parser;

public @SuppressWarnings("all")
class ASTAxis extends SimpleNodeWithShallowCopy {
    private String axis;

    public ASTAxis(int id) {
        super(id);
    }

    public ASTAxis(XPathParser p, int id) {
        super(p, id);
    }

    /** Accept the visitor. * */
    public Object jjtAccept(XPathParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }

    /**
     * @param axis
     *            the axis to set
     */
    public void setAxis(String axis) {
        this.axis = axis;
    }

    /**
     * @return the axis
     */
    public String getAxis() {
        return axis;
    }

    public String toString () {
        return String.format("Axis: %s", getAxis());
    }

    public ASTAxis shallowCopy() {
        ASTAxis copy = (ASTAxis)super.shallowCopy();
        copy.setAxis(this.getAxis());
        return copy;
    }

}
