package es.uca.webservices.xpath.parser;

public @SuppressWarnings("all")
class ASTQName extends SimpleNodeWithShallowCopy {
    private String localPart, prefix;

    public ASTQName(int id) {
        super(id);
    }

    public ASTQName(XPathParser p, int id) {
        super(p, id);
    }

    /** Accept the visitor. * */
    public Object jjtAccept(XPathParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }

    /**
     * @param localPart
     *            the localPart to set
     */
    public void setLocalPart(String localPart) {
        this.localPart = localPart;
    }

    /**
     * @return the localPart
     */
    public String getLocalPart() {
        return localPart;
    }

    /**
     * @param prefix
     *            the prefix to set
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }

    public String toString() {
        return String.format("Name: %s:%s",
                             (getPrefix().length() > 0
                              ? getPrefix() : "{none}"),
                             getLocalPart());
    }

    public ASTQName shallowCopy() {
        ASTQName copy = (ASTQName)super.shallowCopy();
        copy.setLocalPart(this.getLocalPart());
        copy.setPrefix(this.getPrefix());
        return copy;
    }
}
