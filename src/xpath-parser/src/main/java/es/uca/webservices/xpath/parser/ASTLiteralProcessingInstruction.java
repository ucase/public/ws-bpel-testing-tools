package es.uca.webservices.xpath.parser;

public @SuppressWarnings("all")
class ASTLiteralProcessingInstruction extends SimpleNodeWithShallowCopy {
    private String instruction;

    public ASTLiteralProcessingInstruction(int id) {
        super(id);
    }

    public ASTLiteralProcessingInstruction(XPathParser p, int id) {
        super(p, id);
    }

    /** Accept the visitor. * */
    public Object jjtAccept(XPathParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }

    /**
     * @param instruction
     *            the instruction to set
     */
    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    /**
     * @return the instruction
     */
    public String getInstruction() {
        return instruction;
    }

    public String toString() {
        return String.format("Literal processing instruction: %s",
                             getInstruction());
    }

    public SimpleNode shallowCopy() {
        ASTLiteralProcessingInstruction copy
            = (ASTLiteralProcessingInstruction)super.shallowCopy();
        copy.setInstruction(this.getInstruction());
        return copy;
    }

}
