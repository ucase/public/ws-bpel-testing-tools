package es.uca.webservices.xpath.parser;

public @SuppressWarnings("all")
class ASTStringConstant extends SimpleNodeWithShallowCopy {
    private String value;

    public ASTStringConstant(int id) {
        super(id);
    }

    public ASTStringConstant(XPathParser p, int id) {
        super(p, id);
    }

    /** Accept the visitor. * */
    public Object jjtAccept(XPathParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }

    /**
     * Returns the string literal contained in this node.
     *
     * @return String literal contained in this node.
     */
    public String getValue() {
        return value;
    }

    /**
     * Changes the string literal contained in this node. Precondition: the
     * string literal includes the original quotes.
     *
     * @param newValue
     *            New value for this node's string literal.
     */
    public void setValue(String newValue) {
        value = newValue;
    }

    public String toString() {
        return String.format("String literal: %s", getValue());
    }

    public ASTStringConstant shallowCopy() {
        ASTStringConstant copy = (ASTStringConstant)super.shallowCopy();
        copy.setValue(this.getValue());
        return copy;
    }

}
