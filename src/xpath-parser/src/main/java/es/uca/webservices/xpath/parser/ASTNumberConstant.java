package es.uca.webservices.xpath.parser;

public @SuppressWarnings("all")
class ASTNumberConstant extends SimpleNodeWithShallowCopy {
    private float value;

    public ASTNumberConstant(int id) {
        super(id);
    }

    public ASTNumberConstant(XPathParser p, int id) {
        super(p, id);
    }

    /** Accept the visitor. * */
    public Object jjtAccept(XPathParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }

    /**
     * Changes the float constant contained in this node.
     *
     * @param value
     *            the value to set
     */
    public void setValue(float value) {
        this.value = value;
    }

    /**
     * Returns the float constant contained in this node.
     *
     * @return the value
     */
    public float getValue() {
        return value;
    }

    public String toString() {
        return String.format("Number constant: %g", getValue());
    }

    public ASTNumberConstant shallowCopy() {
        ASTNumberConstant copy = (ASTNumberConstant)super.shallowCopy();
        copy.setValue(this.getValue());
        return copy;
    }
}
