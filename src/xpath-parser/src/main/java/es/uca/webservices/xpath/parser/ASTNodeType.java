package es.uca.webservices.xpath.parser;

public @SuppressWarnings("all")
class ASTNodeType extends SimpleNodeWithShallowCopy {
    private String type;

    public ASTNodeType(int id) {
        super(id);
    }

    public ASTNodeType(XPathParser p, int id) {
        super(p, id);
    }

    /** Accept the visitor. * */
    public Object jjtAccept(XPathParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    public String toString() {
        return String.format("Node type test: %s", getType());
    }

    public ASTNodeType shallowCopy() {
        ASTNodeType copy = (ASTNodeType)super.shallowCopy();
        copy.setType(this.getType());
        return copy;
    }
}
