package es.uca.webservices.xpath.parser;

public @SuppressWarnings("all")
class SimpleNodeWithShallowCopy extends SimpleNode {

    public SimpleNodeWithShallowCopy(int i) {
        super(i);
    }

    public SimpleNodeWithShallowCopy(XPathParser p, int i) {
        super(p, i);
    }

    /** Creates a shallow copy (same id, same class, no children, no parent) */
    public SimpleNode shallowCopy() {
        try {
            SimpleNode copy = (SimpleNode) super.clone();
            copy.children   = null;
            copy.parent     = null;
            copy.parser     = null;
            return copy;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
