package es.uca.webservices.xpath.parser;

import java.io.BufferedReader;
import java.io.Reader;
import java.io.StringReader;

import static org.junit.Assert.fail;
import org.junit.Test;

/** 
 * Checks whether the parser accepts and rejects the right inputs.
 * Doesn't check whether the parsing tree is actually right.
 * 
 * @author Antonio García Domínguez
 * @version 1.0
 * 
 * */
public class XPathParserAcceptTest {
	
	static final private String[] vExpressionsAccept = {
		"/",
		"ancestor::div",
		"ancestor-or-self::div",
		"attribute::*",
		"attribute::name",
		"child::*",
		"child::chapter[1]",
		"child::chapter[title]",
		"child::chapter[child::title]",
		"child::chapter[child::title='Introduction']",
		"child::chapter/descendant::para",
		"child::*/child::para",
		"/child::doc/child::chapter[position()=5]/child::section[position()=2]",
		"child::node()",
		"child::para",
		"child::para[attribute::type=\"warning\"]",
		"child::para[attribute::type='warning'][position()=5]",
		"child::para[position()=1]",
		"child::para[position()>1]",
		"child::para[position()=5][attribute::type=\"warning\"]",
		"child::para[position()=last()]",
		"child::para[position()=last()-1]",
		"child::*[self::chapter]",
		"child::text()",
		"/descendant::figure[position()=42]",
		"/descendant::olist/child::item",
		"descendant-or-self::para",
		"/descendant::para",
		"descendant::para",
		"following-sibling::chapter[position()=1]",
		"preceding-sibling::chapter[position()=1]",
		"self::para",
		"*",
		".",
		"@*",
		"*",
		"ancestor::author[parent::book][1]",
		"ancestor::book[1]",
		"ancestor::book[author][1]",
		"author",
		"author[1]",
		"author[* = \"Bob\"]",
		"author[degree and award]",
		"author[degree and not(publication)]",
		"author[degree][award]",
		"author[(degree or award) and publication]",
		"author[first-name][3]",
		"author[first-name = \"Bob\"]",
		"author[last-name[1]]",
		"author[last-name = \"Bob\"]",
		"author[last-name = \"Bob\" and first-name = \"Joe\"]",
		"author[last-name = \"Bob\" and ../price &gt; 50]",
		"author[last-name [position()=1]]",
		"author[. = \"Matthew Bob\"]",
		"author[not(degree or award) and publication]",
		"author[not(last-name = \"Bob\")]",
		"book",
		"book[author/degree]",
		"book[/bookstore/@specialty=@style]",
		"book[excerpt]",
		"book[last()]",
		"book[position() &lt;= 3]",
		"bookstore",
		"book[@style]",
		"degree[@from != \"Harvard\"]",
		"degree[position() &lt; 3]",
		"first",
		"first.name",
		"@my:*",
		"my:*",
		"my:book",
		"p",
		"price",
		"price[@intl = \"Canada\"]",
		"*[@specialty]",
		"@style",
		"x",
		"x[1]",
		"y",
		"processing-instruction()",
		"processing-instruction('foo')",
	};
	
	final private static String[] vExpressionsReject = {
		"dummy::node",
		"child::*:foo",
		"::foo",
		"processing-instruction(3)",
	};

        @Test
	public void testAcceptedExpressions() {
		for (String s : vExpressionsAccept) {
			Reader is = new BufferedReader(new StringReader(s));
			XPathParser parser = new XPathParser(is);
			try {
				parser.Start();
			} catch (ParseException e) {
				e.printStackTrace();
				fail(String.format("Input expression '%s' was rejected though it was valid", s));
			}			
		}
	}

        @Test
	public void testRejectedExpressions() {
		for (String s : vExpressionsReject) {
			Reader is = new BufferedReader(new StringReader(s));
			XPathParser parser = new XPathParser(is);
			try {
				parser.Start();
				fail(String.format("Input expression '%s' was accepted though it wasn't valid", s));
			} catch (ParseException e) {								
			}			
		}		
	}
}
