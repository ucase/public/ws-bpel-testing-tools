package es.uca.takuan.coverage.loaders;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.util.List;

import org.junit.Test;

/**
 * Tests checking that all the possible paths are detected.
 * @author Antonio García-Domínguez
 */
public class BpelLoaderTest {

	@Test
	public void marketplace() throws Exception {
		final List<List<String>> paths = getPaths("marketplace");
		assertEquals(2, paths.size());
	}

	@Test
	public void metasearch() throws Exception {
		final List<List<String>> paths = getPaths("metasearch");
		assertEquals(3, paths.size());
	}

	private List<List<String>> getPaths(final String compositionName) throws Exception {
		final BpelLoader loader = new BpelLoader();
		final InputStream bpelStream = getClass().getResourceAsStream("/" + compositionName + ".bpel");
		final List<List<String>> paths = loader.load(bpelStream);
		return paths;
	}

}
