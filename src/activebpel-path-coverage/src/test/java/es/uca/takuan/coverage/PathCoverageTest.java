package es.uca.takuan.coverage;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 * Integration tests, making sure everything still works.
 */
public class PathCoverageTest {

	private static final File TEST_RESOURCES_DIR = new File("src/test/resources");
	private PathCoverage coverage;
	private File executedPathsFile;
	private File outputFile;
	private File possiblePathsFile;
	private static DocumentBuilder DOC_BUILDER;

	@BeforeClass
	public static void setupSuite() throws Exception {
		DOC_BUILDER = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	}

	@Before
	public void setup() throws Exception {
		executedPathsFile = File.createTempFile("executedpaths", ".log");
		outputFile = File.createTempFile("output", ".log");
		possiblePathsFile = File.createTempFile("possiblepaths", ".log");

		coverage = new PathCoverage();
		coverage.setBPELFile(getTestResource("marketplace.bpel"));
		coverage.setExecutedPathsFile(executedPathsFile);
		coverage.setOutputFile(outputFile);
		coverage.setPossiblePathsFile(possiblePathsFile);
	}

	@Test
	public void plain() throws Exception {
		coverage.setPlain(true);
		coverage.generateCoverage(Arrays.asList(
			getTestResource("traces/1.log"), getTestResource("traces/2.log")));
	}

	@Test
	public void xml() throws Exception {
		coverage.generateCoverage(Arrays.asList(
			getTestResource("traces2/1.log"), getTestResource("traces2/3.log")));

		assertIsXML(executedPathsFile);
		assertIsXML(outputFile);
		assertIsXML(possiblePathsFile);
	}

	/**
	 * Checks that the contents of a file constitute a well-formed XML document.
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	private void assertIsXML(File xmlFile)
			throws ParserConfigurationException, SAXException, IOException
	{
		DOC_BUILDER.parse(xmlFile);		
	}

	private File getTestResource(final String path) {
		return new File(TEST_RESOURCES_DIR, path);
	}
}
