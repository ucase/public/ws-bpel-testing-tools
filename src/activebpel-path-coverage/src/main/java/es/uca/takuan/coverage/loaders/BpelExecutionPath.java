package es.uca.takuan.coverage.loaders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class is the one who generates the execution paths
 * @author Alejandro Álvarez Ayllón, Antonio García Domínguez
 */
class BpelExecutionPath extends Thread {
	private static final Logger LOGGER = LoggerFactory.getLogger(BpelExecutionPath.class);
    private static List<String>      recursiveNodes = initializeRecursiveNodes();

    private List<String>       currentExecutionPath;
    private List<List<String>> generalList;
    private List<Thread>       alternateThreads;
    private Stack<NodeHistory>       baseStack;
    private Document                 document;

    /**
     * Initializes the list of nodes that must be inspected
     * in deep
     * @return
     */
    private static List<String> initializeRecursiveNodes()
    {
    	return Arrays.asList("sequence", "flow", "while", "for", "assign", "else");
    }

    /**
     * Constructor
     * @param d     The root document
     * @param cN    The root node
     * @param pL    Where to put the list of executed sentences when this ends
     */
    public BpelExecutionPath(Document d, Node cN, List<List<String>> pL)
    {
        document             = d;
        currentExecutionPath = new ArrayList<String>();
        generalList          = pL;
        alternateThreads     = new ArrayList<Thread>();
        baseStack            = new Stack<NodeHistory>();
        
        // Add the first node (this is a constructor from scratch)
        baseStack.push(new NodeHistory(null, cN));
        currentExecutionPath.add("/process");
    }

    /**
     * Private constructor, for alternative path threads
     * @param d     The root document
     * @param st    The stack of the execution simulation
     * @param cP    The history of executed sentences
     * @param pL    Where to put the list of executed sentences when this ends
     */
    private BpelExecutionPath(Document d, Stack<NodeHistory> st, List<String> cP, List<List<String>> pL)
    {
        document             = d;
        baseStack            = st;
        currentExecutionPath = cP;
        generalList          = pL;
        alternateThreads     = new ArrayList<Thread>();
    }


    /**
     * Main thread method
     */
    @Override
    public void run()
    {
        // Process children
        processNodeList();

        // Add to the general list
        generalList.add(currentExecutionPath);

        // Wait children threads
		try {
			for (Thread th : alternateThreads) {
				th.join();
			}
		} catch (InterruptedException ex) {
			LOGGER.error("Interrupted wait", ex);
		}
    }

    /**
     * Create a copy that can be used separately
     * @param baseStack
     * @return
     */
    private Stack<NodeHistory> cloneStack(Stack<NodeHistory> baseStack) {
        // We only need to duplicate the nodes in the stack, and
        // their direct parents
        
        // We clone the stack
        Stack<NodeHistory> newStack = new Stack<NodeHistory>();

        // For each element
        for(NodeHistory nh : baseStack) {
            // Clone its parent
            NodeHistory newNode, parent;

            newNode   = nh.clone();
            parent    = nh.getParent();
            if(parent != null)
                newNode.setParent(parent.clone());

            newStack.push(newNode);
        }

        return newStack;
    }

    /**
     * Gets a child node with the given localname
     * @param root  The parent node
     * @param local The local name
     */
    private Node getNode(Node root, String local)
    {
        NodeList children = root.getChildNodes();
        for(int i = 0; i < children.getLength(); ++i) {
            Node child = children.item(i);
            if(child.getNodeType() == Node.ELEMENT_NODE && child.getLocalName().equals(local))
                return child;
        }
        return null;
    }

    /**
     * Main method for the execution simulation
     */
    private void processNodeList()
    {
        while(!baseStack.isEmpty())
        {
            NodeHistory nodeRef;

            // We get the top node
            nodeRef = baseStack.pop();

            // Add sibling, except if it is an else
            // (They will be added in a special way)
            Node sibling = nodeRef.getNextSibling();
            if(sibling != null &&
               !(sibling.getNodeType() == Node.ELEMENT_NODE && sibling.getLocalName().equals("else")))
                baseStack.push(new NodeHistory(nodeRef.getParent(), sibling));


            // We work over this node
            if(nodeRef.getNode().getNodeType() == Node.ELEMENT_NODE) {
                
                // If it is a if
                if(nodeRef.getLocalName().equals("if")) {
                    /** We need both if and else nodes **/
                    Node branch;
                    /**
                     * First, work with else, because we need a clear copy of
                     * the current execution path. If we do the else before the if,
                     * we will have duplicated if sentences
                     */
                    branch = getNode(nodeRef.getNode(), "else");

                    // Duplicate the current execution path
                    final List<String> alternateWay = new ArrayList<String>();
                    alternateWay.addAll(currentExecutionPath);

                    // Add manually the if
                    alternateWay.add(nodeRef.getXPath());

                    /* If there is no explicit else, add the "virtual else"
                     * and add the duplicated list to the general list
                     */
                    if(branch == null) {
                        alternateWay.add(new NodeHistory(nodeRef, branch).getXPath());
                        generalList.add(alternateWay);
                    } else {
                        // Duplicate the current stack
                        Stack<NodeHistory> alternateStack = cloneStack(baseStack);
                        // Add the else node to the stack
                        alternateStack.push(new NodeHistory(nodeRef.clone(), branch));
                        // Explicit else. Throw a new thread to handle it with a copy of the list
                        Thread alternate = new BpelExecutionPath(document, alternateStack, alternateWay, generalList);
                        alternate.start();
                        alternateThreads.add(alternate);
                    }

                    // Work with the IF
                    // We want the child after the condition node
                    branch = getNode(nodeRef.getNode(), "condition");
                    branch = branch.getNextSibling();

                    // Add the if
                    currentExecutionPath.add(nodeRef.getXPath());

                    // Create the virtual if-condition node
                    NodeHistory virtualIfCond = new NodeHistory(nodeRef,
                                                                document.createElementNS(BpelLoader.BPEL_NAMESPACE,
                                                                                         "if-condition"));
                    currentExecutionPath.add(virtualIfCond.getXPath());
                    
                    // Process children
                    baseStack.push(new NodeHistory(virtualIfCond, branch));
                }
                // Ignore the condition nodes, the rest, as usual
                else if(!nodeRef.getLocalName().equals("condition")) {
                    currentExecutionPath.add(nodeRef.getXPath());

                    /* Special cases: recursives
                     * Sequence, Flow, ...
                     */
                    if(recursiveNodes.contains(nodeRef.getLocalName())) {
                        // Add the first child to the stack
                        baseStack.push(new NodeHistory(nodeRef, nodeRef.getFirstChild()));
                    }
                }
            }
        }
    }
}