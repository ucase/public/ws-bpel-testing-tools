package es.uca.takuan.coverage;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

/**
 * Class which incrementally collects information about the paths that were and
 * were not executed.
 * 
 * and the possible execution paths
 * 
 * @author Alejandro Álvarez Ayllón
 */
class CoverageComparer {

	private Map<List<String>, Integer> timesExecuted;

	public CoverageComparer(List<List<String>> possibleExecutionPaths) {
		this.timesExecuted = new IdentityHashMap<List<String>, Integer>();
		for (List<String> path : possibleExecutionPaths) {
			timesExecuted.put(path, 0);
		}
	}

	/**
	 * Collects information from each execution trace.
	 * 
	 * @param executedPaths
	 *            The executed paths.
	 */
	public void collectFromTrace(List<String> sentencesExecuted) {
		/**
		 * NOTA: No es el sistema más eficiente. Utilizar un árbol para los logs
		 * cargados optimizaría el consumo de memoria y de tiempo, pero esa
		 * optimización no es primordial ahora, que se trata de conseguir algo
		 * que funcione
		 */
		for (Map.Entry<List<String>, Integer> entry : timesExecuted.entrySet()) {
			final List<String> key = entry.getKey();
			final Integer val = entry.getValue();
			if (key.equals(sentencesExecuted)) {
				timesExecuted.put(key, val + 1);
				break;
			}
		}
	}

	public List<List<String>> getPathsNotExecuted() {
		List<List<String>> notExecuted = new ArrayList<List<String>>();
		for (Map.Entry<List<String>, Integer> entry : timesExecuted.entrySet()) {
			final List<String> key = entry.getKey();
			final Integer val = entry.getValue();
			if (val == 0) {
				notExecuted.add(key);
			}
		}

		return notExecuted;
	}
}
