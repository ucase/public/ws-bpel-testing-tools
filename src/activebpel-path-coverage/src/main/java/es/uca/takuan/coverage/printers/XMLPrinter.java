package es.uca.takuan.coverage.printers;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * This class implements the XML Printer
 * 
 * @author Alejandro Álvarez Ayllón
 */
public class XMLPrinter implements Printer {

	private PrintStream out;
	private int uniqId = 0;

	@Override
	public void setPrintStream(PrintStream o) {
		out = o;
	}

	@Override
	public void printHeader() {
		out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		out.println("<cov:document xmlns:cov=\"http://www.uca.es/2009/10/coverage\">");
	}

	@Override
	public void printStatistics(Map<String, String> statistics) {
		Set<Entry<String, String>> entries = statistics.entrySet();

		out.println("<cov:statistics>");
		for (Entry<String, String> i : entries) {
			out.println("\t<cov:value label=\"" + i.getKey() + "\">"
					+ i.getValue() + "</cov:value>");
		}
		out.println("</cov:statistics>");
	}

	@Override
	public void printExecutionPath(List<String> path) {
		if (uniqId == 0) {
			out.println("<cov:paths>");
		}

		uniqId++;

		// Print info about this path
		out.println("\t<cov:path id=\"" + Integer.toString(uniqId)
				+ "\" length=\"" + Integer.toString(path.size()) + "\">");

		// Print sentences
		for (String s : path) {
			out.println("\t\t<cov:sentence>" + s + "</cov:sentence>");
		}

		// Close
		out.println("\t</cov:path>");
	}

	@Override
	public void printFooter() {
		out.println("</cov:paths>");
		out.println("</cov:document>");
		uniqId = 0;
	}
}
