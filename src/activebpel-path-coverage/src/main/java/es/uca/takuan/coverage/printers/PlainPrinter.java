package es.uca.takuan.coverage.printers;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Prints in plaintext format.
 * @author Alejandro Álvarez Ayllón
 */
public class PlainPrinter implements Printer {

    private PrintStream out;

    private static final String SEPARATOR = "==============================\n";

	private int uniqId = 0;

	@Override
    public void setPrintStream(PrintStream o)
    {
        out = o;
    }

	@Override
    public void printStatistics(Map<String, String> statistics)
    {
        Set<Entry<String, String>> entries = statistics.entrySet();

        for(Entry<String, String> i : entries) {
            out.println(i.getKey() + ": " + i.getValue());
        }
        // White line
        out.println();
    }

	@Override
    public void printExecutionPath(List<String> path)
    {
		uniqId++;

		// Print info about this path
		out.println(SEPARATOR);
		out.println("Execution path no." + Integer.toString(uniqId));
		out.println(Integer.toString(path.size()) + " sentences");
		out.println();

		// Print sentences
		for (String sentence : path) {
			out.println(sentence);
		}
    }

    @Override
    public void printFooter()
    {
		uniqId = 0;
    }

	@Override
	public void printHeader() {
		// do nothing
	}

}
