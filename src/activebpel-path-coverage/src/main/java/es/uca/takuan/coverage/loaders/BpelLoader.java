package es.uca.takuan.coverage.loaders;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * This class loads a BPEL and obtains all the possible
 * execution paths
 * @author Alejandro Álvarez Ayllón
 */
public class BpelLoader {

    public static final String BPEL_NAMESPACE = "http://docs.oasis-open.org/wsbpel/2.0/process/executable";

    /**
     * Loads a BPEL file and generates the list of possible execution paths
     * @param bpelStream  The source of the BPEL file
     * @return            A list of possible execution paths
     * @throws org.xml.sax.SAXException         An error with the parser occurred
     * @throws java.io.IOException              The input stream can not be readed
     * @throws org.w3c.dom.DOMException         The BPEL document is malformed or incorrect
     * @throws java.lang.InterruptedException   There have been an error with some of the threads
     * @throws ParserConfigurationException     The parser hasn't been properly configured.
     * @throws XPathExpressionException         The XPath expression for finding the root sequences could not be evaluated.
     */
    public List<List<String>> load(InputStream bpelStream) throws SAXException, IOException, DOMException, InterruptedException, ParserConfigurationException, XPathExpressionException
    {
        // Parse the document: use standard JAXP API to avoid dependency on a
        // specific XML parser (OpenJDK does not use Xerces)
        final Document document = parse(bpelStream);
        final Element root = document.getDocumentElement();

        // Check the BPEL
        if(!root.getNamespaceURI().equals(BPEL_NAMESPACE))
            throw new DOMException(DOMException.NAMESPACE_ERR, "Expected root namespace: " + BPEL_NAMESPACE);
        else if(!root.getLocalName().equals("process"))
            throw new DOMException(DOMException.NOT_SUPPORTED_ERR, "Unknown root element: process expected");

        // Get main sequences
        final XPath xpath = XPathFactory.newInstance().newXPath();
        final NodeList rootSequences = (NodeList)xpath.evaluate(
        	"//*[local-name(.) = 'sequence' and count(ancestor::*[local-name(.) = 'sequence']) = 0]",
        	root, XPathConstants.NODESET);
        if(rootSequences.getLength() == 0) {
            throw new DOMException(DOMException.NOT_FOUND_ERR, "Not found main sequence element");
        }

        // Create main list
        List<List<String>> possiblePaths = Collections.synchronizedList(new ArrayList<List<String>>());

        for (int i = 0; i < rootSequences.getLength(); ++i) {
        	final Node mainSequence = rootSequences.item(i);
            Thread mainThread = new BpelExecutionPath(document, mainSequence, possiblePaths);
            mainThread.run();
            mainThread.join();
        }

        // Return
        return possiblePaths;
    }

	private Document parse(InputStream bpelStream)
			throws ParserConfigurationException, SAXException, IOException {
		final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        final DocumentBuilder db = dbf.newDocumentBuilder();
        return db.parse(new InputSource(bpelStream));
	}
}
