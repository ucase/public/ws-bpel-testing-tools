package es.uca.takuan.coverage.printers;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;

/**
 * This interface is implemented by clases that print the
 * execution path list
 * @author Alejandro Álvarez Ayllón
 */
public interface Printer
{

    /**
     * Set the print stream
     * @param o
     */
    void setPrintStream(PrintStream o);

    /**
     * Header of the output
     */
    void printHeader();

    /**
     * Print a list of key-value pairs as statistics
     * @param statistics
     */
    void printStatistics(Map<String, String> statistics);

    /**
     * Prints an execution path
     * @param epl The executed path.
     */
    void printExecutionPath(List<String> path);

    /**
     * Closes the document
     */
    void printFooter();
}
