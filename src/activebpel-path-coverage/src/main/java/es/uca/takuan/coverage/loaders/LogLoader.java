package es.uca.takuan.coverage.loaders;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * This interface is implemented by classes that are able
 * to load a Log file and generate the path of the execution
 * as a list of XPath expressions
 * @author Alejandro Álvarez Ayllón
 */
public interface LogLoader
{
    List<String> load(InputStream in) throws IOException;
}
