package es.uca.takuan.coverage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import es.uca.takuan.coverage.loaders.ActiveBpelLogLoader;
import es.uca.takuan.coverage.loaders.BpelLoader;
import es.uca.takuan.coverage.loaders.LogLoader;
import es.uca.takuan.coverage.printers.PlainPrinter;
import es.uca.takuan.coverage.printers.Printer;
import es.uca.takuan.coverage.printers.XMLPrinter;

/**
 * Computes path coverage of a set of test traces over a WS-BPEL 2.0 composition.
 *
 * @author Alejandro Álvarez-Ayllón (original version), Antonio García-Domínguez (refactor + SLF4J + JOptSimple + performance)
 */
public class PathCoverage {

	private static final String EXECUTED_PATHS_OPTION = "executedPaths";
	private static final String HELP_OPTION = "help";
	private static final String OUTPUT_OPTION = "output";
	private static final String PLAIN_OPTION = "plain";
	private static final String POSSIBLE_PATHS_OPTION = "possiblePaths";
	private static final String VERBOSE_OPTION = "verbose";
	private static final Logger LOGGER = LoggerFactory.getLogger(PathCoverage.class);

	private File bpelFile, executedPathsFile, outputFile, possiblePathsFile;
	private Printer outputPrinter = new XMLPrinter();

	/**
	 * Main entrance point from the command line.
	 * 
	 * @param args
	 *            Arguments of the program
	 */
	public static void main(String[] args) throws Exception {
		final OptionParser parser = createOptionParser();
		final OptionSet options = parser.parse(args);
		final List<String> notOptions = options.nonOptionArguments();

		if (notOptions.size() < 2) {
			LOGGER.error("Not enough arguments: you must specify at least the BPEL composition, and one execution log");
			parser.printHelpOn(System.err);
			return;
		}
		else if (options.has(HELP_OPTION)) {
			parser.printHelpOn(System.err);
			return;
		}

		PathCoverage coverage = new PathCoverage();
		if (options.has(POSSIBLE_PATHS_OPTION)) {
			coverage.setPossiblePathsFile((File)options.valueOf(POSSIBLE_PATHS_OPTION));
		}
		if (options.has(EXECUTED_PATHS_OPTION)) {
			coverage.setExecutedPathsFile((File)options.valueOf(EXECUTED_PATHS_OPTION));
		}
		if (options.has(OUTPUT_OPTION)) {
			coverage.setOutputFile((File)options.valueOf(OUTPUT_OPTION));
		}
		if (options.has(PLAIN_OPTION)) {
			coverage.setPlain(true);
		}
		if (options.has(VERBOSE_OPTION)) {
			// NOTE: this is the *only* place in the code where we should depend directly on Log4J.
			// The rest of the code should only use SLF4J, to keep it clean.
			org.apache.log4j.Logger.getRootLogger().setLevel(Level.DEBUG);
		}

		final File bpelFile = new File(notOptions.get(0));
		coverage.setBPELFile(bpelFile);

		final List<File> logFiles = new ArrayList<File>(notOptions.size() - 1);
		for (String logFilePath : notOptions.subList(1, notOptions.size())) {
			logFiles.add(new File(logFilePath));
		}

		try {
			coverage.generateCoverage(logFiles);
		} catch (Exception ex) {
			LOGGER.error("Unrecoverable exception while computing path coverage: {}", ex);
			throw ex;
		}
	}

	public void generateCoverage(List<File> logFiles) throws SAXException, IOException, InterruptedException, ParserConfigurationException, XPathExpressionException {
		/*
		 * Steps:
		 * 
		 * 1. Load the BPEL file and generate a list with every possible path
		 * through the process. Each path contains a list of XPath paths with
		 * every branch. We can ignore instructions.
		 * 
		 * 2. For every trace, produce the corresponding list of XPath paths.
		 * 
		 * 3. Try to find every possible path in the above lists.
		 * 
		 * 4. Select those paths which have not been run, and list them in the
		 * coverage report.
		 */

		List<List<String>> possiblePaths = loadPossiblePaths();
		if (possiblePathsFile != null) {
			writePossiblePaths(possiblePaths);
		}

		final LogLoader logLoader = new ActiveBpelLogLoader();
		final CoverageComparer comparer = new CoverageComparer(possiblePaths);

		// We can't possibly hold *every* trace in memory: what if we had 2000+ test cases?
		// To save memory, we will collect results incrementally over each execution trace.
		if (executedPathsFile != null) {
			LOGGER.debug("Writing executed paths to {}...", executedPathsFile);
			outputPrinter.setPrintStream(new PrintStream(new FileOutputStream(executedPathsFile)));
			outputPrinter.printHeader();
		}
		for (File f : logFiles) {
			LOGGER.debug("Loading log {}...", f);
			final List<String> path = logLoader.load(new FileInputStream(f));
			LOGGER.debug("Finished loading {}", f);

			if (executedPathsFile != null) {
				outputPrinter.printExecutionPath(path);
			}

			LOGGER.debug("Comparing possible execution paths and {}... ", f);
			comparer.collectFromTrace(path);
			LOGGER.debug("Finished comparing possible execution paths and {}", f);
		}
		if (executedPathsFile != null) {
			outputPrinter.printFooter();
			LOGGER.debug("Finished writing executed paths to {}", executedPathsFile);
		}

		writeCoverageReport(comparer.getPathsNotExecuted());
	}

	public File getBPELFile() {
		return bpelFile;
	}

	public void setBPELFile(File bpelFile) {
		this.bpelFile = bpelFile;
	}

	public boolean getPlain() {
		return outputPrinter instanceof PlainPrinter;
	}

	public void setPlain(boolean isPlain) {
		if (isPlain) {
			outputPrinter = new PlainPrinter();
		}
		else {
			outputPrinter = new XMLPrinter();
		}
	}

	public File getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(File outputFile) {
		this.outputFile = outputFile;
	}

	public File getExecutedPathsFile() {
		return executedPathsFile;
	}

	public void setExecutedPathsFile(File executedPaths) {
		this.executedPathsFile = executedPaths;
	}

	public File getPossiblePathsFile() {
		return possiblePathsFile;
	}

	public void setPossiblePathsFile(File possiblePaths) {
		this.possiblePathsFile = possiblePaths;
	}

	private static OptionParser createOptionParser() {
		final OptionParser parser = new OptionParser();
		parser.accepts(POSSIBLE_PATHS_OPTION,
				"Prints all the possible execution paths in the specified file")
				.withRequiredArg().ofType(File.class);
		parser.accepts(EXECUTED_PATHS_OPTION,
				"Prints all the executed paths obtained from the logs in the specified file")
				.withRequiredArg().ofType(File.class);
		parser.accepts(OUTPUT_OPTION,
				"Prints final results to the specified file, instead of stdout")
				.withRequiredArg().ofType(File.class);
		parser.accepts(PLAIN_OPTION, "Prints all the outputs as plain text");
		parser.accepts(VERBOSE_OPTION, "Prints verbose debugging information");
		parser.accepts(HELP_OPTION, "Shows help on using this program");
		return parser;
	}

	private void writeCoverageReport(List<List<String>> neverExecuted)
			throws FileNotFoundException {
		PrintStream out = System.err;
		if (outputFile != null) {
			out = new PrintStream(new FileOutputStream(outputFile));
			LOGGER.info("Coverage report written to {}", outputFile);
		}
	
		final Map<String, String> stats = new HashMap<String, String>();
		stats.put("Number of not executed paths", Integer.toString(neverExecuted.size()));
	
		outputPrinter.setPrintStream(out);
		outputPrinter.printHeader();
		outputPrinter.printStatistics(stats);
		printPaths(neverExecuted);
		outputPrinter.printFooter();
	}

	private void writePossiblePaths(final List<List<String>> possiblePaths) throws FileNotFoundException {
		LOGGER.debug("Writing possible execution paths to {}...", possiblePathsFile);
		outputPrinter.setPrintStream(new PrintStream(new FileOutputStream(possiblePathsFile)));

		// Get some statistics
		int maxExec = 0, minExec = Integer.MAX_VALUE;
		for (List<String> sentences : possiblePaths) {
			final int nSentences = sentences.size();
			maxExec = Math.max(nSentences, maxExec);
			minExec = Math.min(nSentences, minExec);
		}

		// General statistics
		Map<String, String> stats = new HashMap<String, String>();
		stats.put("Possible execution paths", Integer.toString(possiblePaths.size()));
		stats.put("Maximum length of an execution path: ", Integer.toString(maxExec));
		stats.put("Minimum length of an execution path: ", Integer.toString(minExec));

		// Write
		outputPrinter.printHeader();
		outputPrinter.printStatistics(stats);
		printPaths(possiblePaths);
		outputPrinter.printFooter();

		LOGGER.debug("Finished writing possible execution paths to {}", possiblePathsFile);
	}

	private List<List<String>> loadPossiblePaths() throws SAXException,
			IOException, InterruptedException, ParserConfigurationException,
			XPathExpressionException {
		LOGGER.debug("Loading BPEL file {}...", bpelFile);
		List<List<String>> possiblePaths = new BpelLoader().load(new FileInputStream(bpelFile));
		LOGGER.debug("Loaded BPEL file {} successfully", bpelFile);
		return possiblePaths;
	}

	private void printPaths(final List<List<String>> paths) {
		for (List<String> path : paths) {
			outputPrinter.printExecutionPath(path);
		}
	}
}
