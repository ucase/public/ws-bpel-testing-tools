package es.uca.takuan.coverage.loaders;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * This class is used to handle the nodes that are processed: XPath and Node at
 * the same time. The class should be final, so we don't have to make
 * {@link #clone()} throw a {@link CloneNotSupportedException}.
 * 
 * @author Alejandro Álvarez Ayllón, Antonio García Domínguez
 */
final class NodeHistory implements Cloneable {
    private String               xPath;
    private Node                 node;
    private NodeHistory          parent;
    private Map<String, Integer> childUniqueID;


    /**
     * Private constructor
     */
    private NodeHistory() {
    }

    /** Constructor
     * @param p The parent of this node
     * @param n The node to be handled
     */
    public NodeHistory(NodeHistory p, Node n)
    {
        node = n;

        // Initialize
        parent = p;
        if(p != null) {
            xPath = p.xPath + "/" + (n == null ? "[virtual else]" : n.getLocalName());
        } else {
            xPath = "/process/" + (n == null ? "[virtual else]" : n.getLocalName());
        }

        childUniqueID = new HashMap<String, Integer>();

        // Add identifier if necessary
        String id = null;
        if(parent != null)
            id = parent.getUniqueID(n);

        // Look for name attribute, if present
        if (node != null) {
			NamedNodeMap attrList = node.getAttributes();
			if (attrList != null) {
				Node nameAttr = attrList.getNamedItem("name");
				if (nameAttr != null)
					id = "@name='" + nameAttr.getNodeValue() + "'";
			}
        }

        // Concat
        if(id != null && !id.isEmpty())
            xPath = xPath + "[" + id + "]";
    }

    /**
     * Returns the XPath-like expression of this node
     * @return  A String with the XPath-like expression
     */
    public String getXPath()
    {
        return xPath;
    }

    /**
     * Returns the node
     * @return  The associated node
     */
    public Node getNode()
    {
        return node;
    }

    /**
     * Returns the first child of the associated node
     * @return The first child node, or null if there is no one
     */
    public Node getFirstChild()
    {
        return node.getFirstChild();
    }

    /**
     * Returns the next sibling in the XML tree
     * @return A Node with the next sibling, or null if there is no one
     */
    public Node getNextSibling()
    {
        return node.getNextSibling();
    }

    /**
     * Returns the local name of the node
     * @return A String with the localname
     */
    public String getLocalName()
    {
        return node.getLocalName();
    }

    /**
     * Returns the parent of this node
     * @return
     */
    public NodeHistory getParent()
    {
        return parent;
    }

    /**
     * Sets the parent of this node
     * @param clone
     */
    void setParent(NodeHistory p)
    {
        parent = p;
    }

    /**
     * Generates a unique numeric ID within its siblings
     * @param n The node for wich the ID is going to be generated
     * @return A String with the UniqueID, or null if it is not necessary (i.e. first node in a list)
     */
    private String getUniqueID(Node n)
    {
        Integer lastID;

        // If n is not an element, ignore
        if(n == null || n.getNodeType() != Node.ELEMENT_NODE)
            return null;

        // Recover
        lastID = childUniqueID.get(n.getLocalName());

        // Not defined?
        if(lastID == null) {
            // Copy nodes are special. They begin in 0
            // and always the ID is shown
            if(n.getLocalName().equals("copy")) {
                lastID = 0;
                childUniqueID.put("copy", lastID);
                return lastID.toString();
            } else {
                // Create and save, but not return (first one)
                lastID = 1;
                childUniqueID.put(n.getLocalName(), lastID);
                return null;
            }
        } else {
            // Increment, save and return
            ++lastID;
            childUniqueID.put(n.getLocalName(), lastID);
            return lastID.toString();
        }
    }

    /**
     * Clones this node
     * @return
     */
    @Override
    public NodeHistory clone()
    {
        NodeHistory clon = new NodeHistory();

        clon.childUniqueID = new HashMap<String, Integer>(childUniqueID);
        clon.node   = node;
        clon.parent = parent;
        clon.xPath  = xPath;

        return clon;
    }
}