package es.uca.webservices.xpath;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.activebpel.rt.bpel.AeBusinessProcessException;
import org.activebpel.rt.bpel.IAeProcessInfoEvent;
import org.activebpel.rt.bpel.function.AeFunctionCallException;
import org.activebpel.rt.bpel.function.IAeFunctionExecutionContext;
import org.activebpel.rt.bpel.impl.AeAbstractBpelObject;
import org.activebpel.rt.bpel.impl.IAeBusinessProcessEngineInternal;
import org.activebpel.rt.bpel.impl.IAeBusinessProcessInternal;

/**
 * This class is used to obtain the scope of a variable
 * 
 * @author Antonio García Domínguez
 * @version 1.0
 */
public class ScopeFunction extends AbstractFunction {

	private static final Logger LOGGER = Logger.getLogger(ScopeFunction.class
			.getCanonicalName());

	/**
	 * Prints in the register the scope of a variable
	 * 
	 * @param contexto
	 *            The execution context
	 * @param argumentos
	 *            The parameters sended
	 * @return The value of the variable
	 * @throws AeFunctionCallException
	 *             The parameters are not correct
	 */
	public Object call(IAeFunctionExecutionContext contexto,
			@SuppressWarnings("rawtypes") List argumentos)
			throws AeFunctionCallException {

		if (argumentos.size() != 2) {
			throw new AeFunctionCallException("Expected 2 arguments, got "
					+ argumentos.size());
		} else if (!(argumentos.get(0) instanceof Long)) {
			throw new AeFunctionCallException(
					"Expected Integer as first argument, got "
							+ argumentos.get(0).getClass().getName());
		} else if (!(argumentos.get(1) instanceof String)) {
			throw new AeFunctionCallException(
					"Expected String as second argument, got "
							+ argumentos.get(1).getClass().getName());
		}

		final AeAbstractBpelObject objetoBPEL = contexto
				.getAbstractBpelObject();
		final IAeBusinessProcessInternal proceso = objetoBPEL.getProcess();
		final IAeBusinessProcessEngineInternal motor = proceso.getEngine();

		final long nivelProfundidad = ((Long) argumentos.get(0)).intValue();
		final String sDelim = String.format("@%d@", nivelProfundidad);
		final String exprXPath = (String) argumentos.get(1);

		// Hemos de sustituir el delimitador por el apóstrofe
		final String exprXPathSinEscape = exprXPath.replaceAll(sDelim, "'");

		printMessage(proceso, motor, " : ---- INICIO AMBITO ----");
		Object resPrueba;
		try {
			resPrueba = objetoBPEL.executeExpression(new StringXPathExpression(exprXPathSinEscape, contexto));
		} catch (AeBusinessProcessException e) {
			LOGGER.severe(e.getLocalizedMessage());
			return null;
		} finally {
			printMessage(proceso, motor, " : ---- FIN AMBITO ----");
		}

		// Asumimos que el resultado null se refiere a un nodeset vacío: esto
		// daba problemas en expresiones como count(uca:ambito(x,y)), donde y
		// carecia de elementos o sencillamente no existía.
		return (resPrueba != null) ? resPrueba : new ArrayList<Object>();
	}

	/**
	 * Prints a message in the register
	 * 
	 * @param proceso
	 *            BPEL process reference
	 * @param motor
	 *            Engine
	 * @param s
	 *            Message string
	 */
	protected void printMessage(final IAeBusinessProcessInternal proceso,
			final IAeBusinessProcessEngineInternal motor, final String s) {
		motor.fireEvaluationEvent(proceso.getProcessId(), "",
				IAeProcessInfoEvent.GENERIC_INFO_EVENT, "", s);
	}
}
