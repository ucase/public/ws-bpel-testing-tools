/**
 * 
 */
package es.uca.webservices.xpath;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;

import org.activebpel.rt.bpel.IAeProcessInfoEvent;
import org.activebpel.rt.bpel.function.AeFunctionCallException;
import org.activebpel.rt.bpel.function.IAeFunctionExecutionContext;
import org.activebpel.rt.bpel.impl.AeAbstractBpelObject;
import org.activebpel.rt.bpel.impl.IAeBusinessProcessEngineInternal;
import org.activebpel.rt.bpel.impl.IAeBusinessProcessInternal;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class is used to inspectionate the value of a variable
 * @author Antonio García Domínguez
 * @version 1.0
 */
class InspectFunction extends AbstractFunction {
	protected static final String NONSENSICAL_VALUE = "@N@nonsensical@N@";

	/**
	 * Prints in the register the equivalence between the received expression
	 * (escaping escapes and subcalls to this function) and its results; and
	 * returns the result as it it. It uses a quotes escape by level scheme,
	 * replacing the quotes in the level 0 with:
	 * 
	 * @0@, for level 1
	 * @1@, and so on
	 * 
	 * @param contexto
	 *            The execution context
	 * @param args
	 *            Received arguments
	 * @throws AeFunctionCallException
	 *             The parameters are not valid
	 */
	public Object call(final IAeFunctionExecutionContext contexto,
			@SuppressWarnings("rawtypes") final List args)
		throws AeFunctionCallException
	{
		if (args.size() != 2) {
			throw new AeFunctionCallException(
					"Expected 2 arguments, got" + args.size());
		} else if (!(args.get(0) instanceof Long)) {
			throw new AeFunctionCallException(
					"Expected Integer as first argument, got "
							+ args.get(0).getClass().getName());
		} else if (!(args.get(1) instanceof String)) {
			throw new AeFunctionCallException(
					"Expected String as second argument, got "
							+ args.get(1).getClass().getName());
		}

		final AeAbstractBpelObject bpelObject = contexto.getAbstractBpelObject();
		final IAeBusinessProcessInternal process = bpelObject.getProcess();
		final IAeBusinessProcessEngineInternal engine = process.getEngine();

		final long depthLevel = ((Long) args.get(0)).intValue();
		final String sDelim = String.format("@%d@", depthLevel);
		final String exprXPath = (String) args.get(1);

		// Hemos de sustituir el delimitador por el apóstrofe
		final String exprXPathUnescaped = exprXPath.replaceAll(sDelim, "'");

		Object result;
		try {
			result = bpelObject.executeExpression(new StringXPathExpression(exprXPathUnescaped, contexto));
		} catch (Exception e) {
			// Normalmente, cuando estamos aquí, sabemos que la fórmula es
			// sintácticamente correcta,
			// ya que hemos instrumentado a través de un análisis
			// sintáctico, así que hay dos posibilidades:
			// o no se ha podido resolver alguna función (fallo del
			// diseñador original), o hay una variable
			// no inicializada, y deberíamos informar de que se trata de un
			// valor "nonsensical" a Daikon.

			// NOTA: no se pueden inspeccionar variables con mensajes WSDL
			// así como así.
			// Hay que inspeccionar sus partes, realmente.
			result = NONSENSICAL_VALUE;
		}

		// Retiramos las subllamadas para mostrar una ruta limpia al
		// usuario
		String tmpInspectedPath = exprXPath;
		{
			long nivelActual = depthLevel;
			while (tmpInspectedPath.indexOf("@" + nivelActual + "@") != -1) {
				tmpInspectedPath = tmpInspectedPath
						.replaceAll(
								String
										.format(
												"uca:inspeccionar\\([^,]*,\\s*@%d@(.*?)@%d@\\s*\\)",
												nivelActual, nivelActual),
								"$1");
				++nivelActual;
			}
		}
		// Las secuencias @N@ restantes no se hallan dentro de una
		// llamada: sustituimos por '
		final String inspectedPath = tmpInspectedPath.replaceAll("@[0-9]+@", "'");

		String sResult = "";
		if (result instanceof Element) {
			// Recorreremos el árbol y mostraremos únicamente los
			// valores escalares en las hojas
			final String exprXPathElemento = inspectedPath
					+ "/"
					+ computePrefixedName(bpelObject, (Element) result)
					+ "[1]";
			inspectTree(bpelObject, process, engine,
					exprXPathElemento, (Element) result);
		} else {
			// Convertimos el resultado a cadena
			sResult = result.toString();
			logPathValue(process, engine, inspectedPath, sResult);
		}

		if (NONSENSICAL_VALUE.equals(result)) {
			/*
			 * See org.activebpel.rt.bpel.impl.activity.assign.AeCopyOperationBase.execute
			 */
			return null;
		} else {
			return result;
		}
	}

	/**
	 * <p>
	 * Visit the tree in a prefix order. Namely:
	 * </p>
	 * <ol>
	 * <li>We give the textual content (removing trailing spaces) as path =
	 * value of direct descendants.</li>
	 * <li>We enunciate all the attributes as path/@attribute = value.</li>
	 * <li>We enunciate all children as path/child[position], recursively.</li>
	 * </ol>
	 */
	protected void inspectTree(AeAbstractBpelObject objetoBPEL,
			IAeBusinessProcessInternal proceso,
			IAeBusinessProcessEngineInternal motor, String ruta,
			Element elem) {

		// Atributos (excepto los de tipo xmlns:..., que son declaraciones
		// de nombres de espacios)
		NamedNodeMap nAtribs = elem.getAttributes();
		for (int iAtributo = 0; iAtributo < nAtribs.getLength(); ++iAtributo) {
			final Node nAtrib = nAtribs.item(iAtributo);

			final String atribNSURI = nAtrib.getNamespaceURI();
			if (XMLConstants.XML_NS_URI.equals(atribNSURI)
				|| XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI.equals(atribNSURI)
				|| XMLConstants.XMLNS_ATTRIBUTE_NS_URI.equals(atribNSURI))
			{
				// Skip over the xml:*, xmlns:* and xsi:* attributes
				continue;
			}

			final String nbAtr = computePrefixedName(objetoBPEL, nAtrib);
			final String rutaAtr = String.format("%s/@%s", ruta, nbAtr);
			final String valorAtr = nAtrib.getNodeValue();
			logPathValue(proceso, motor, rutaAtr, valorAtr);
		}

		// Nodos elemento o texto hijos
		NodeList nlHijos = elem.getChildNodes();
		Map<String, Integer> recuentoHijos = new HashMap<String, Integer>();
		String contenidoTextual = "";
		for (int iHijo = 0; iHijo < nlHijos.getLength(); ++iHijo) {
			final Node nHijo = nlHijos.item(iHijo);
			if (nHijo.getNodeType() == Node.ELEMENT_NODE) {
				final String nombre = computePrefixedName(objetoBPEL, nHijo);

				// Establece la posición del elemento respecto de sus
				// hermanos del mismo tipo
				int numRepeticion;
				if (recuentoHijos.containsKey(nombre)) {
					numRepeticion = recuentoHijos.get(nombre) + 1;
				} else {
					numRepeticion = 1;
				}
				recuentoHijos.put(nombre, numRepeticion);

				final String rutaElem = String.format("%s/%s[%d]", ruta,
						nombre, numRepeticion);
				inspectTree(objetoBPEL, proceso, motor, rutaElem,
						(Element) nHijo);
			} else if (nHijo.getNodeType() == Node.TEXT_NODE) {
				contenidoTextual = contenidoTextual + nHijo.getNodeValue();
			}
		}

		// Contenido textual de los descendientes directos
		contenidoTextual = contenidoTextual.trim();
		if (contenidoTextual.length() > 0) {
			logPathValue(proceso, motor, ruta, contenidoTextual);
		}
	}

	/**
	 * Prints a message into the log.
	 * 
	 * @param process
	 *            Process reference
	 * @param engine
	 *            BPEL engine
	 * @param path
	 *            Path
	 * @param value
	 *            Value
	 */
	protected void logPathValue(final IAeBusinessProcessInternal process,
			final IAeBusinessProcessEngineInternal engine,
			final String path, String value) {
		engine.fireEvaluationEvent(process.getProcessId(), "",
				IAeProcessInfoEvent.GENERIC_INFO_EVENT, "",
				String.format(" : INSPECTION(%s) = %s", path, value));
	}

}