/**
 *
 */
package es.uca.webservices.xpath;

import java.util.List;
import java.util.logging.Logger;

import javax.xml.namespace.QName;


import org.activebpel.rt.bpel.function.AeFunctionCallException;
import org.activebpel.rt.bpel.function.IAeFunctionExecutionContext;
import org.activebpel.rt.bpel.impl.AeAbstractBpelObject;
import org.w3c.dom.Node;

/**
 * This class is used to enclose a property access into a scope.
 * 
 * @author Alejandro Álvarez-Ayllón, Antonio García-Domínguez
 * @version 1.1
 */
public final class PropertyScopeFunction extends ScopeFunction {

	private static final Logger LOGGER
		= Logger.getLogger(PropertyScopeFunction.class.getCanonicalName());

	/**
	 * Prints in the execution log the scope of the property.
	 * 
	 * @param context
	 *            The execution context
	 * @param args
	 *            The passed arguments
	 * @return The value of the property
	 * @throws AeFunctionCallException
	 *             The parameters are invalid
	 */
	@Override
	public Object call(IAeFunctionExecutionContext context, @SuppressWarnings("rawtypes") List args)
			throws AeFunctionCallException {
		final AeAbstractBpelObject bpelObject = context.getAbstractBpelObject();

		// Check argument count and types
		if (args.size() != 2) {
			throw new AeFunctionCallException("Expected 2 arguments, got " + args.size());
		}
		if (!(args.get(0) instanceof String)) {
			throw new AeFunctionCallException(
					"Expected String as first argument, got "
							+ args.get(0).getClass().getName());
		}
		if (!(args.get(1) instanceof String)) {
			throw new AeFunctionCallException(
					"Expected String as second argument, got "
							+ args.get(0).getClass().getName());
		}

		// We will use the usual bpel:getVariableProperty function to compute its value
		final String variable = ((String) args.get(0));
		final String property = ((String) args.get(1));
		final QName propertyQName = mapStringToQName(context, property);
		final String xpathExpr = "bpel:getVariableProperty('" + variable + "','" + property + "')";

		try {
			ensurePropertyIsCached(context, variable, propertyQName);
			Object resPrueba = bpelObject.executeExpression(new StringXPathExpression(xpathExpr, context));
			if (!(resPrueba instanceof Node)) {
				return resPrueba;
			}
			Node node = (Node) resPrueba;

			// If it only has one child, return the first text node of that child as its value
			if (node.getChildNodes().getLength() == 1 && node.getFirstChild().getNodeType() == Node.TEXT_NODE) {
				printMessage(bpelObject.getProcess(),
						bpelObject.getProcess().getEngine(),
						" : Ruta accedida: $" + variable + "~" + propertyQName.getLocalPart());
				return node.getFirstChild().getNodeValue();
			} else {
				// Otherwise, return the node as is
				return node;
			}
		} catch (Exception e) {
			LOGGER.severe(e.getLocalizedMessage());
			return null;
		}
	}
}
