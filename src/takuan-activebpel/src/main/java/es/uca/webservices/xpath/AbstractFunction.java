package es.uca.webservices.xpath;

import java.util.Set;
import java.util.logging.Logger;

import javax.xml.namespace.QName;

import org.activebpel.rt.AeException;
import org.activebpel.rt.bpel.AeBusinessProcessException;
import org.activebpel.rt.bpel.IAeExpressionLanguageFactory;
import org.activebpel.rt.bpel.def.AeBaseDef;
import org.activebpel.rt.bpel.def.AeProcessDef;
import org.activebpel.rt.bpel.def.activity.support.AeFromDef;
import org.activebpel.rt.bpel.def.visitors.AeWSBPELDefVisitorFactory;
import org.activebpel.rt.bpel.def.visitors.AeWSBPELInlinePropertyAliasVisitor;
import org.activebpel.rt.bpel.function.IAeFunction;
import org.activebpel.rt.bpel.function.IAeFunctionExecutionContext;
import org.activebpel.rt.bpel.impl.AeAbstractBpelObject;
import org.activebpel.rt.bpel.impl.IAeBusinessProcessInternal;
import org.activebpel.rt.bpel.server.IAeProcessDeployment;
import org.activebpel.rt.bpel.server.engine.AeEngineFactory;
import org.w3c.dom.Node;

public abstract class AbstractFunction implements IAeFunction {

	private static final Logger LOGGER = Logger
			.getLogger(AbstractFunction.class.getCanonicalName());

	/**
	 * Resolves the name of the node using the BPEL prefixes
	 * 
	 * @param objetoBPEL
	 *            BPEL reference
	 * @param nodo
	 *            Node to be resolved
	 * @return A String with the resolved name (prefix:local)
	 */
	protected static String computePrefixedName(AeAbstractBpelObject objetoBPEL, Node nodo) {
		// Hay que resolver el prefijo usando el contexto del proceso BPEL,
		// o no coincidirán los prefijos debidamente (se usarían los de la
		// especificación de los casos de uso de BPELUnit y no los del
		// proceso BPEL)
		final String localPart = nodo.getLocalName();
		final String nsURI = nodo.getNamespaceURI();

		if (nsURI != null) {
			@SuppressWarnings("unchecked")
			final Set<String> prefixes = (Set<String>) objetoBPEL.resolveNamespaceToPrefixes(nsURI);

			if (!prefixes.isEmpty()) {
				// First try with the prefixes in the WS-BPEL composition
				return prefixes.iterator().next() + ":" + localPart;
			} else if (nodo.getPrefix() != null) {
				// Otherwise, use the prefix used in the node as-is
				return nodo.getPrefix() + ":" + localPart;
			} else {
				LOGGER.fine(String
						.format("Could not find a valid prefix for '%s': using just the local part '%s'",
								nsURI, localPart));
			}
		}

		return localPart;
	}

	/**
	 * Converts a string of the form "prefix:localPart" into a proper QName. Assumes that
	 * the prefix used is known by the function context's namespace context.
	 */
	protected QName mapStringToQName(IAeFunctionExecutionContext context, String propertyName) {
		final String[] parts = propertyName.split(":", 2);
		QName propQName;
		if (parts.length == 1) {
			propQName = new QName(parts[0]);
		}
		else {
			// It might be useful to preserve the original prefix
			propQName = new QName(context.getNamespaceContext().resolvePrefixToNamespace(parts[0]), parts[1], parts[0]);
		}
		return propQName;
	}

	protected void ensurePropertyIsCached(IAeFunctionExecutionContext context, String variable, QName property) throws AeBusinessProcessException, AeException {
				/**
				 * We need to ensure that the property is cached by ActiveBPEL before
				 * evaluating the expression. Sometimes, after instrumenting, the static
				 * analysis regularly performed by ActiveBPEL on accessed properties
				 * will stop working. This is usually the case for properties which are
				 * not used in any correlation sets, among others.
				 * 
				 * We can't fix this by adding fake correlation sets: they would need to
				 * be *really* used in a receive or invoke activity, it seems. The only
				 * other way to fix this would be to instrument the code while
				 * preserving the ability to statically analyze which properties are
				 * going to be accessed, but this is unfeasible at the moment.
				 * 
				 * Instead, every time we access a property through this extension
				 * function, we will generate a virtual &lt;from&gt; definition that
				 * uses that property and give it to an instance of the same kind of
				 * visitor that is normally used for static analysis. This visitor will
				 * update the internal property cache of the process accordingly.
				 */
				AeWSBPELInlinePropertyAliasVisitor visitor = getPropertyVisitor(context);
				visitVirtualFromDef(visitor, context.getDef(), variable, property);
	}

	private void visitVirtualFromDef(AeWSBPELInlinePropertyAliasVisitor visitor, AeBaseDef parentDef, String variable,
			QName propQName) {
				visitor.setProcessDef(getProcessDef(parentDef));

				AeFromDef virtualDef = new AeFromDef();
				virtualDef.setParent(parentDef);
				virtualDef.setVariable(variable);
				virtualDef.setProperty(propQName);
				visitor.visit(virtualDef);
	}

	private AeProcessDef getProcessDef(AeBaseDef parentDef) {
		while (!(parentDef instanceof AeProcessDef)) {
			parentDef = parentDef.getParent();
		}
		return (AeProcessDef)parentDef;
	}

	private AeWSBPELInlinePropertyAliasVisitor getPropertyVisitor(IAeFunctionExecutionContext context)
			throws AeBusinessProcessException, AeException {
				IAeBusinessProcessInternal process = context.getAbstractBpelObject().getProcess();
				IAeProcessDeployment deployment
					= AeEngineFactory.getDeploymentProvider().findCurrentDeployment(process.getName());
				IAeExpressionLanguageFactory exprFactory
					= process.getExpressionLanguageFactory();
				AeWSBPELInlinePropertyAliasVisitor visitor
					= (AeWSBPELInlinePropertyAliasVisitor)AeWSBPELDefVisitorFactory.getInstance(process.getBPELNamespace()).createPropertyAliasInlineVisitor(deployment, exprFactory);
				return visitor;
	}

}