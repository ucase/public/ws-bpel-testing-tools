package es.uca.webservices.xpath;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.activebpel.rt.bpel.AeBusinessProcessException;
import org.activebpel.rt.bpel.IAeProcessInfoEvent;
import org.activebpel.rt.bpel.function.AeFunctionCallException;
import org.activebpel.rt.bpel.function.IAeFunctionExecutionContext;
import org.activebpel.rt.bpel.impl.AeAbstractBpelObject;
import org.activebpel.rt.bpel.impl.IAeBusinessProcessEngineInternal;
import org.activebpel.rt.bpel.impl.IAeBusinessProcessInternal;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class is used to print the paths of the variables
 * @author aalvarez
 */
public final class PrintPathsFunction extends AbstractFunction {

	// Heavily simplified regex for finding WS-BPEL variables
	// TODO: implement the regex in http://www.w3.org/TR/REC-xml/#NT-NameStartChar
	private static final Pattern PATTERN_VARIABLE = Pattern.compile("\\$(?:\\w|\\.)+");

	private static final Logger LOGGER
		= Logger.getLogger(PrintPathsFunction.class.getCanonicalName());

	public Object call(IAeFunctionExecutionContext contexto, @SuppressWarnings("rawtypes") List argumentos) throws AeFunctionCallException {

		if (argumentos.size() != 2) {
			throw new AeFunctionCallException("Expected 2 arguments, got "
					+ argumentos.size());
		} else if (!(argumentos.get(0) instanceof Long)) {
			throw new AeFunctionCallException(
					"Expected Integer as first argument, got "
							+ argumentos.get(0).getClass().getName());
		} else if (!(argumentos.get(1) instanceof String)) {
			throw new AeFunctionCallException(
					"Expected String as second argument, got "
							+ argumentos.get(1).getClass().getName());
		}

		final AeAbstractBpelObject objetoBPEL = contexto
				.getAbstractBpelObject();
		final IAeBusinessProcessInternal proceso = objetoBPEL.getProcess();
		final IAeBusinessProcessEngineInternal motor = proceso.getEngine();

		final long nivelProfundidad = ((Long) argumentos.get(0)).intValue();
		final String sDelim = String.format("@%d@", nivelProfundidad);
		final String exprXPath = (String) argumentos.get(1);

		// Hemos de sustituir el delimitador por el apóstrofe
		final String exprXPathSinEscape = exprXPath.replaceAll(sDelim, "'");

		// Evaluamos la expresión
		Object resPrueba;
		try {
			resPrueba = objetoBPEL.executeExpression(new StringXPathExpression(exprXPathSinEscape, contexto));
		} catch (AeBusinessProcessException e) {
			LOGGER.severe(e.getLocalizedMessage());
			return null;
		}

		String sRutaCompleta;
		if (resPrueba instanceof Node) {
			// Ruta completa a la que se ha accedido
			final Matcher varMatcher = PATTERN_VARIABLE.matcher(exprXPathSinEscape);
			if (varMatcher.find()) {
				// Esta es la ruta dentro del árbol XML de la parte del mensaje
				// o de la variable que nos interesa, pero no incluye a la variable
				final String sRutaXML = getPath(objetoBPEL, (Node) resPrueba);

				// Esta es la parte anterior a la ruta dentro del árbol XML anterior
				final String sPrefijo = varMatcher.group();

				sRutaCompleta = sPrefijo + sRutaXML;
			}
			else {
				sRutaCompleta = exprXPathSinEscape + getPath(objetoBPEL, (Node) resPrueba);
			}
		} 
		else {
			sRutaCompleta = exprXPathSinEscape;
		}

		if (resPrueba != null) {
			logPath(proceso, motor, " : Ruta accedida: " + sRutaCompleta);			
		}
		else {
			// Asumimos que el resultado null se refiere al nodeset vacío. Esto permite
			// que expresiones como count(uca:imprimirRutas(...)) funcionen debidamente.
			resPrueba = new ArrayList<Object>();
		}
		
		return resPrueba;
	}

	private String getPath(AeAbstractBpelObject bpelObject, Node node) {
		final String sName = computePrefixedName(bpelObject, node);

		if (node instanceof Document || node instanceof DocumentFragment) {
			return "";
		} else if (node instanceof Attr) {
			final Node nCont = ((Attr) node).getOwnerElement();
			return String.format("%s/@%s", getPath(bpelObject, nCont), sName);
		} else {
			final Node nPadre = node.getParentNode();
			int posicion = 1;

			// Obtenemos posicion entre los hermanos con el mismo nombre
			final NodeList nlHijosPadre = nPadre.getChildNodes();
			for (int i = 0; i < nlHijosPadre.getLength(); ++i) {
				final Node nHijo = nlHijosPadre.item(i);
				if (nHijo.getNodeName().equals(sName)) {
					if (nHijo.isSameNode(node)) {
						break;
					} else {
						posicion++;
					}
				}
			}

			return String.format("%s/%s[%d]", getPath(bpelObject, nPadre), sName,
					posicion);
		}
	}
	
	private void logPath(final IAeBusinessProcessInternal processObject,
			final IAeBusinessProcessEngineInternal engine, final String msg) {
		engine.fireEvaluationEvent(processObject.getProcessId(), "",
				IAeProcessInfoEvent.GENERIC_INFO_EVENT, "", msg);
	}
}
