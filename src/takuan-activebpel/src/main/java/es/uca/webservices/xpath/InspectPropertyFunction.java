package es.uca.webservices.xpath;

import java.util.List;
import java.util.logging.Logger;

import javax.xml.namespace.QName;

import org.activebpel.rt.AeException;
import org.activebpel.rt.bpel.function.AeFunctionCallException;
import org.activebpel.rt.bpel.function.IAeFunctionExecutionContext;
import org.activebpel.rt.bpel.impl.AeAbstractBpelObject;
import org.w3c.dom.Element;

/**
 * This class is used to inspect BPEL variable properties
 * 
 * @author Alejandro Álvarez-Ayllón, Antonio García-Domínguez
 * @version 1.1
 */
public final class InspectPropertyFunction extends InspectFunction {

	private static final Logger LOGGER = Logger.getLogger(InspectPropertyFunction.class.getCanonicalName());
	private static final String PROP_PREFIX = "inspectproperty";

	/**
	 * Prints the value of the property in the BPEL execution log
	 * 
	 * @param context
	 *            The call context
	 * @param args
	 *            The parameters
	 * @return The value of the property
	 * @throws AeFunctionCallException
	 *             The parameters are not valid
	 */
	@Override
	public Object call(IAeFunctionExecutionContext context, @SuppressWarnings("rawtypes") List args)
			throws AeFunctionCallException
	{
		final AeAbstractBpelObject assignActivity = context.getAbstractBpelObject();

		// Check number and types of parameters
		if (args.size() != 3) {
			throw new AeFunctionCallException("Expected 3 arguments, got " + args.size());
		}
		if (!(args.get(0) instanceof String)) {
			throw new AeFunctionCallException("Expected String as first argument, got " + args.get(0).getClass().getName());
		}
		if (!(args.get(1) instanceof String)) {
			throw new AeFunctionCallException("Expected String as second argument, got " + args.get(1).getClass().getName());
		}
		if (!(args.get(2) instanceof String)) {
			throw new AeFunctionCallException("Expected String as second argument, got " + args.get(2).getClass().getName());
		}

		// We will build an XPath query using the predefined bpel:getVariableProperty function
		final String variable = ((String) args.get(0));
		final String propertyNamespace = ((String) args.get(1));
		final String propertyLocalPart = ((String) args.get(2));

		// We need to make up a new QName on the spot, as we can't rely on any prefix
		final String propertyName = PROP_PREFIX + ":" + propertyLocalPart;
		assignActivity.getDefinition().addNamespace(PROP_PREFIX, propertyNamespace);
		final String xpathExpr = "bpel:getVariableProperty('" + variable + "','" + propertyName + "')";

		// Query the property and log the result
		try {
			ensurePropertyIsCached(context, variable, new QName(propertyNamespace, propertyLocalPart));
			Object result = assignActivity.executeExpression(new StringXPathExpression(xpathExpr, context));
			if (result == null) {
				result = NONSENSICAL_VALUE;
				LOGGER.warning("Property is null: " + variable + "~" + propertyLocalPart);
			}

			// Dump the result to the ActiveBPEL execution log
			String loggedVariable = "$" + variable + "~" + propertyLocalPart;
			if (result instanceof Element) {
				inspectTree(assignActivity,
						assignActivity.getProcess(),
						assignActivity.getProcess().getEngine(),
						loggedVariable,
						(Element) result);
			} else {
				logPathValue(assignActivity.getProcess(),
						assignActivity.getProcess().getEngine(),
						loggedVariable,
						result.toString());
			}

			if (NONSENSICAL_VALUE.equals(result)) {
				return null;
			}
			else {
				return result;
			}
		} catch (AeException e) {
			LOGGER.severe(e.getLocalizedMessage());
			return null;
		}
	}
}
