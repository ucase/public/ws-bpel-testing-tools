package es.uca.webservices.xpath;

import java.util.HashMap;
import java.util.Map;

import org.activebpel.rt.bpel.function.AeUnresolvableException;
import org.activebpel.rt.bpel.function.IAeFunction;
import org.activebpel.rt.bpel.function.IAeFunctionContext;

/**
 * Context for the functions used to monitor BPEL processes.
 * 
 * @author Antonio Garcia-Dominguez
 * @version 1.1
 */
public class ContextoFunciones implements IAeFunctionContext {

	static final String XML_NAMESPACE = "http://www.w3.org/2000/xmlns/";

	private static final String SCOPEPROP_FNAME = "ambitoPropiedad";
	private static final String INSPECTPROP_FNAME = "inspeccionarPropiedad";
	private static final String PRINTPATHS_FNAME = "imprimirRutas";
	private static final String SCOPE_FNAME = "ambito";
	private static final String INSPECT_FNAME = "inspeccionar";

	private final Map<String, IAeFunction> name2Function = new HashMap<String, IAeFunction>();

	public ContextoFunciones() {
		name2Function.put(INSPECT_FNAME, new InspectFunction());
		name2Function.put(INSPECTPROP_FNAME, new InspectPropertyFunction());
		name2Function.put(SCOPE_FNAME, new ScopeFunction());
		name2Function.put(SCOPEPROP_FNAME, new PropertyScopeFunction());
		name2Function.put(PRINTPATHS_FNAME, new PrintPathsFunction());
	}

	/**
	 * Get the object that implements the desired function.
	 * 
	 * @param name
	 *            Function name
	 * @throws AeUnresolvableException
	 *             The function is not defined
	 * 
	 * @see org.activebpel.rt.bpel.function.IAeFunctionContext#getFunction(java.lang.String)
	 */
	public IAeFunction getFunction(String name) throws AeUnresolvableException {
		if (name2Function.containsKey(name)) {
			return name2Function.get(name);
		} else {
			throw new AeUnresolvableException(String.format(
					"Function %s is not available in this context", name));
		}
	}

}
