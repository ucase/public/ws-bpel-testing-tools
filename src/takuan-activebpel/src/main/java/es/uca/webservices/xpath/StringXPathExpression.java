package es.uca.webservices.xpath;

import org.activebpel.rt.bpel.def.AeBaseDef;
import org.activebpel.rt.bpel.def.IAeBPELConstants;
import org.activebpel.rt.bpel.def.IAeExpressionDef;
import org.activebpel.rt.bpel.def.visitors.IAeDefVisitor;
import org.activebpel.rt.bpel.function.IAeFunctionExecutionContext;

/**
 * @author Antonio García Domínguez
 */
final class StringXPathExpression extends AeBaseDef implements IAeExpressionDef {
	private final String expresionXPath;

	private final String namespaceBPEL;

	StringXPathExpression(String exprXPathSinEscape, IAeFunctionExecutionContext contexto) {
		this.expresionXPath = exprXPathSinEscape;
		this.namespaceBPEL = contexto.getBpelNamespace();
		setParent(contexto.getDef());
	}

	public String getBpelNamespace() {
		return namespaceBPEL;
	}

	public String getExpression() {
		return expresionXPath;
	}

	public String getExpressionLanguage() {
		if (getBpelNamespace().equals(IAeBPELConstants.BPWS_NAMESPACE_URI)) {
			return IAeBPELConstants.BPWS_XPATH_EXPR_LANGUAGE_URI;
		} else {
			return IAeBPELConstants.WSBPEL_EXPR_LANGUAGE_URI;
		}
	}

	public void setExpression(String aExpression) {
		// no-op
	}

	public void setExpressionLanguage(String aLanguageURI) {
		// no-op
	}

	@Override
	public void accept(IAeDefVisitor aVisitor) {
		// no-op
	}
}