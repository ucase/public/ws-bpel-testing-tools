package es.uca.webservices.gamera.exec;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.exec.bpel.BPELExecutor;

public class AbstractBPELExecutorExecutionTest {

	//private static final String BPEL_BASENAME = "loanRPC.bpel";
	//private static final String BPTS_BASENAME = "loanRPC.bpts";
	//private static final String BPEL_RES_DIR = "loanRPC";
	private static final String BPEL_BASENAME = "loanApprovalProcess.bpel";
	private static final String BPTS_BASENAME = "loanApprovalProcess.bpts";
	private static final String BPEL_RES_DIR = "LoanApprovalRPC_ODE";
	private static final Logger logger = LoggerFactory.getLogger(BPELExecutorKeepGoingTest.class);

	protected static final GAIndividual ERR_1_1 = new GAIndividual(4, 1, 1);
	protected static final GAIndividual ERR_1_5 = new GAIndividual(4, 1, 5);
	protected static final ComparisonResults EXPECTED_RESULTS_ERR_1_1 = new ComparisonResults(
			ERR_1_1, new ComparisonResult[] { ComparisonResult.SAME_OUTPUT,
					ComparisonResult.SAME_OUTPUT, ComparisonResult.SAME_OUTPUT,
					ComparisonResult.SAME_OUTPUT, ComparisonResult.SAME_OUTPUT});
					//ComparisonResult.SAME_OUTPUT });
	protected static final ComparisonResults EXPECTED_RESULTS_ERR_1_5 = new ComparisonResults(
			ERR_1_5, new ComparisonResult[] { ComparisonResult.SAME_OUTPUT,
					ComparisonResult.SAME_OUTPUT, ComparisonResult.SAME_OUTPUT,
					ComparisonResult.DIFFERENT_OUTPUT,
					ComparisonResult.DIFFERENT_OUTPUT});
					//ComparisonResult.SAME_OUTPUT });
	protected static final ComparisonResults EXPECTED_RESULTS_ERR_1_5_STOP_FIRST_DIFF = new ComparisonResults(
			ERR_1_5,
			new ComparisonResult[] { ComparisonResult.SAME_OUTPUT,
					ComparisonResult.SAME_OUTPUT, ComparisonResult.SAME_OUTPUT,
					ComparisonResult.DIFFERENT_OUTPUT,
					ComparisonResult.SAME_OUTPUT}); 
					//ComparisonResult.SAME_OUTPUT });

	protected static BPELExecutor mExecutor;
	protected static File mTmpResourceDir;

	/**
	 * Extracts the resources required for these tests into a temporary folder,
	 * and prepares the BPELExecutor.
	 */
	@BeforeClass
	public static void setUp() throws IOException, PreparationException {
		mTmpResourceDir = File.createTempFile("res", "dir");
		mTmpResourceDir.delete();
		mTmpResourceDir.mkdir();
	
		String[] sResources = new String[] { BPTS_BASENAME, BPEL_BASENAME,
				"ApprovalService.wsdl", "AssessorService.wsdl",
				"LoanService.wsdl", "loanServicePT.wsdl" };
		for (String sResource : sResources) {
			extractResource(BPEL_RES_DIR, sResource);
		}
	
		mExecutor = new BPELExecutor();
		mExecutor.setTestSuite(mTmpResourceDir.getCanonicalPath()
				+ File.separator + BPTS_BASENAME);
		mExecutor.setOriginalProgram(mTmpResourceDir.getCanonicalPath()
				+ File.separator + BPEL_BASENAME);
		mExecutor.setPathToOdeWar("../mubpel/target/dependency/ode-axis2-war-1.3.8.war");
	}

	/**
	 * Deletes the extracted resources and cleans up the executor.
	 */
	@AfterClass
	public static void tearDown() throws IOException, PreparationException {
		mExecutor.cleanup();
		FileUtils.deleteDirectory(mTmpResourceDir);
	}

	protected void assertComparisonProducesExpectedResults(GAIndividual[] fIndividuals, final ComparisonResults[] expectedRow) throws ComparisonException {
		ComparisonResults[] res = mExecutor.compare(null, fIndividuals);
		assertNotNull(res);
		assertTrue("Actual results " + Arrays.toString(res)	+
				" should match expectations " + Arrays.toString(expectedRow),
				Arrays.deepEquals(expectedRow, res));
		for (ComparisonResults r : res) {
			assertNotNull(r.getTotalNanos());
			assertEquals(r.getRow().length, r.getTestWallNanos().length);
			for (BigInteger nanos : r.getTestWallNanos()) {
				assertTrue(nanos.compareTo(BigInteger.ZERO) >= 0);
			}
		}
	}

	private static void extractResource(final String resourceDir, final String resourceFile) throws IOException, FileNotFoundException {
		final String resourcePath = "/" + resourceDir + "/" + resourceFile;
		final InputStream resourceStream = BPELExecutorKeepGoingTest.class.getResourceAsStream(resourcePath);
		final File resourceFileObj = new File(mTmpResourceDir, resourceFile);
		final FileOutputStream fileStream = new FileOutputStream(resourceFileObj);
		IOUtils.copy(resourceStream, fileStream);
		logger.debug("Extracted resource " + resourcePath + " to " + resourceFileObj.getCanonicalPath());
	}

}
