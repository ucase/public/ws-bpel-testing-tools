package es.uca.webservices.gamera.exec;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.matchers.JUnitMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionProcessor;
import es.uca.webservices.gamera.exec.bpel.BPELExecutor;

/**
 * Tests for the BPELExecutor {@link Option} annotations.
 *
 * @author Antonio García-Domínguez
 */
public class BPELExecutorAnnotationsTest {

	private static final Class<BPELExecutor> klazz = BPELExecutor.class;

	private static Method methodSetOutputFile;
	private static Method methodSetTestSuite;
	private static Method methodSetOriginalProgram;
	private static Method methodSetDataFile;
	private static Method[] expectedOptionMethods;

	@BeforeClass
	public static void prepareSuite() throws Exception {
		methodSetOutputFile = klazz.getMethod("setOutputFile", String.class);
		methodSetOriginalProgram = klazz.getMethod("setOriginalProgram", String.class);
		methodSetTestSuite = klazz.getMethod("setTestSuite", String.class);
		methodSetDataFile = klazz.getMethod("setDataFile", String.class);
		expectedOptionMethods = new Method[]{
				methodSetDataFile, methodSetOutputFile, methodSetOriginalProgram, methodSetTestSuite
		};
	}

	@Before
	public void setUp() {
		// Clear up caches to isolate tests from each other
		ResourceBundle.clearCache();
	}

	@Test
	public void allOptionsAreListed() throws Exception {
		final Map<Method, Option> results = OptionProcessor.listOptions(klazz);
		assertThat(results.size(), is(equalTo(expectedOptionMethods.length)));
		for (Method expectedMethod : expectedOptionMethods) {
			assertTrue(
				expectedMethod.getName() + " must be reported as an option",
				results.containsKey(expectedMethod));
		}
	}

	@Test
	public void spanishShortDescription() throws Exception {
		assertEquals("BPEL original",
			OptionProcessor.getShortDescription(
				methodSetOriginalProgram,
				new Locale("es")));
	}

	@Test
	public void englishShortDescription() throws Exception {
		Locale.setDefault(Locale.UK);
		assertEquals("Original BPEL",
			OptionProcessor.getShortDescription(methodSetOriginalProgram));
	}

	@Test
	public void spanishLongDescription() throws Exception {
		Locale.setDefault(new Locale("es"));
		final String longDescription = OptionProcessor.getLongDescription(methodSetOutputFile);
		assertThat(longDescription, containsString("Ruta"));
	}

	@Test
	public void englishLongDescription() throws Exception {
		Locale.setDefault(Locale.UK);
		final String longDescription = OptionProcessor.getLongDescription(methodSetTestSuite);
		assertThat(longDescription, containsString("Path"));
	}
}
