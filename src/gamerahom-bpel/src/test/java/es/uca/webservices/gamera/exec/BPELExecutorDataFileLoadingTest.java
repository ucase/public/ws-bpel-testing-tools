package es.uca.webservices.gamera.exec;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.exec.bpel.AbstractBPELExecutor;
import es.uca.webservices.gamera.exec.bpel.BPELExecutor;

/**
 * Tests checking that the data file loading functionality works fine
 * in the BPELExecutor, both as a configuration option and as a method
 * to be used during the execution of the .
 *
 * @author Antonio García-Domínguez
 */
public class BPELExecutorDataFileLoadingTest extends AbstractBPELExecutorExecutionTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(BPELExecutorDataFileLoadingTest.class);
	private static final String RESOURCE_PREFIX = "src/test/resources/ODE/";
	private static final String DATAFILE_BASED_BPTS = "loanRPC-vm.bpts";
	private static File tmpDataFileBPTS;

	@BeforeClass
	public static void setupSuite() throws Exception {
		final File datafileBPTS = new File(RESOURCE_PREFIX, DATAFILE_BASED_BPTS);
		tmpDataFileBPTS = new File(mTmpResourceDir, DATAFILE_BASED_BPTS);
		copy(datafileBPTS, tmpDataFileBPTS);

		mExecutor.setTestSuite(tmpDataFileBPTS.getCanonicalPath());
		mExecutor.loadDataFile(new File(RESOURCE_PREFIX, "three-tests.vm"));
		mExecutor.validate();
		mExecutor.setComparisonThreads(1);
		mExecutor.prepare();
	}

	@AfterClass
	public static void teardownSuite() throws Exception {
		if (tmpDataFileBPTS != null) {
			tmpDataFileBPTS.delete();
			tmpDataFileBPTS = null;
		}
	}

	@Test(expected=InvalidConfigurationException.class)
	public void cannotLoadDataFileIfUnusedInBpts() throws Exception {
		AbstractBPELExecutor executor = new BPELExecutor();
		executor.setOriginalProgram(RESOURCE_PREFIX + "fake.bpel");
		executor.setDataFile(RESOURCE_PREFIX + "fake.vm");
		executor.setTestSuite(RESOURCE_PREFIX + "without-data-path.bpts");
		executor.validate();
	}

	@Test
	public void loadDataFileWorksWithProperBpts() throws Exception {
		final File srcFile = new File(RESOURCE_PREFIX, "fake.vm");
		final File dstFile = new File(RESOURCE_PREFIX, "data.src");

		AbstractBPELExecutor executor = new BPELExecutor();
		executor.setOriginalProgram(RESOURCE_PREFIX + "loanRPC.bpel");
		executor.setDataFile(RESOURCE_PREFIX + "fake.vm");
		executor.setTestSuite(RESOURCE_PREFIX + "with-data-path.bpts");
		executor.validate();

		assertTrue("GAmera should have copied the data file to the path expected by the BPTS",
				FileUtils.contentEquals(srcFile, dstFile));
	}

	@Test
	public void dataFileCanBeReloadedAfterPrepare() throws Exception {
		LOGGER.info("Running with three tests");
		ComparisonResults[] threeResults = mExecutor.compare(null, ERR_1_1);
		assertEquals(1, threeResults.length);
		assertEquals(3, threeResults[0].getRow().length);

		LOGGER.info("Running with four tests");
		mExecutor.loadDataFile(new File(RESOURCE_PREFIX, "four-tests.vm"));
		ComparisonResults[] fourResults = mExecutor.compare(null, ERR_1_1);
		assertEquals(1, fourResults.length);
		assertEquals(4, fourResults[0].getRow().length);
	}

	private static void copy(final File source, final File target)
			throws FileNotFoundException, IOException {
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis = new FileInputStream(source);
			fos = new FileOutputStream(target);
			IOUtils.copy(fis, fos);
		} finally {
			if (fis != null) {
				fis.close();
			}
			if (fos != null) {
				fos.close();
			}
		}
	}
}