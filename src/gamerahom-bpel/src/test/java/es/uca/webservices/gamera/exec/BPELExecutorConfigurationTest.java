package es.uca.webservices.gamera.exec;

import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.exec.bpel.AbstractBPELExecutor;
import es.uca.webservices.gamera.exec.bpel.BPELExecutor;

/**
 * Configuration loading tests for the {@link BPELExecutor} class.
 * 
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class BPELExecutorConfigurationTest {

    @Test(expected=InvalidConfigurationException.class)
    public void missingTestSuiteIsInvalid() throws InvalidConfigurationException {
        AbstractBPELExecutor exec = new BPELExecutor();
        exec.validate();
    }

    @Test
    public void withTestSuiteIsValid() throws InvalidConfigurationException {
        getValidExecutor().validate();
    }

    @Test(expected=InvalidConfigurationException.class)
    public void missingOutputFileIsInvalid() throws InvalidConfigurationException {
        BPELExecutor exec = getValidExecutor();
        exec.setOutputFile("");
        exec.validate();
    }

    @Test(expected=InvalidConfigurationException.class)
    public void missingOriginalProgramIsInvalid() throws InvalidConfigurationException {
       AbstractBPELExecutor exec = getValidExecutor();
       exec.setOriginalProgram("");
       exec.validate();
    }

    private BPELExecutor getValidExecutor() {
        BPELExecutor exec = new BPELExecutor();
        exec.setTestSuite("src/test/resources/loanRPC-vm.bpts");
        exec.setOriginalProgram("src/test/resources/loanRPC.bpel");
        return exec;
    }

}
