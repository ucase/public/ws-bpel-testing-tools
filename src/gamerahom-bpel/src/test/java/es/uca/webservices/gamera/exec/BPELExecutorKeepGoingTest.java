package es.uca.webservices.gamera.exec;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.math.BigInteger;

import org.junit.BeforeClass;
import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.exec.GAProgressMonitor;
import es.uca.webservices.gamera.exec.bpel.BPELExecutor;
import es.uca.webservices.mutants.operators.OperatorConstants;

/**
 * Execution tests for the {@link BPELExecutor} class, with the default setting
 * of going on after the first difference.
 * 
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class BPELExecutorKeepGoingTest extends AbstractBPELExecutorExecutionTest {

	@BeforeClass
	public static void setupSuite() throws Exception {
		// We assume that the base class' @BeforeClass method is run first
		mExecutor.prepare();
	}

	@Test
	public void analyzeWorks() throws Exception {
		AnalysisResults results = mExecutor.analyze(null);
		assertNotNull(results);
		assertEquals(
				"Analysis results should show information about all available operators",
				OperatorConstants.OPERATOR_NAMES.length, results
						.getFieldRanges().length);
		assertEquals(
				"Analysis results should show information about all available operators",
				OperatorConstants.OPERATOR_NAMES.length, results
						.getLocationCounts().length);
		assertEquals("Fourth line should be 'ERR 2 5'", BigInteger.valueOf(2), results.getLocationCounts()[3]);
		assertEquals("Fourth line should be 'ERR 2 5'", BigInteger.valueOf(5), results.getFieldRanges()[3]);
	}

	@Test
	public void analyzeIsMonitored() throws Exception {
		// Prepare mock progress monitor
		final GAProgressMonitor mockMonitor = mock(GAProgressMonitor.class);
		when(mockMonitor.isCancelled()).thenReturn(false);

		// Perform the analysis
		mExecutor.analyze(mockMonitor);

		// Verify that the monitor has been correctly used
		verify(mockMonitor).start(OperatorConstants.OPERATOR_NAMES.length);
		verify(mockMonitor, times(OperatorConstants.OPERATOR_NAMES.length)).status(anyString());
		verify(mockMonitor, times(OperatorConstants.OPERATOR_NAMES.length)).done(1);
	}

	@Test
	public void analyzeCanBeCancelled() throws Exception {
		// The mock should act as if the user cancelled the operation after the third operator
		final GAProgressMonitor mockMonitor = mock(GAProgressMonitor.class);
		when(mockMonitor.isCancelled())
			.thenReturn(false)
			.thenReturn(false)
			.thenReturn(true);

		// Perform the analysis
		final AnalysisResults results = mExecutor.analyze(mockMonitor);
		verify(mockMonitor, times(3)).isCancelled();
		assertThat(results.getFieldRanges().length, is(equalTo(3)));
		assertThat(results.getLocationCounts().length, is(equalTo(3)));
		assertThat(results.getOperatorNames().length, is(equalTo(3)));
	}

	@Test
	public void generateWorks() throws Exception {
		File fMutant = mExecutor.generate(ERR_1_1);
		assertTrue("Generated mutant should be readable", fMutant.canRead());
		assertTrue("Generated mutant should be a file", fMutant.isFile());
		assertTrue("Generated mutant should not be empty", fMutant.length() > 0);
		assertTrue("Generated mutant filename should end in .bpel", fMutant
				.getName().endsWith(".bpel"));
	}

	@Test
	public void compareOneByOneWorks() throws Exception {
		assertComparisonProducesExpectedResults(
			new GAIndividual[]{ ERR_1_5 },
			new ComparisonResults[]{
					EXPECTED_RESULTS_ERR_1_5,
			});

		assertComparisonProducesExpectedResults(
			new GAIndividual[]{ ERR_1_1 },
			new ComparisonResults[]{
					EXPECTED_RESULTS_ERR_1_1,
			});
	}

	@Test
	public void compareTwoAtATimeWorks() throws Exception {
		assertComparisonProducesExpectedResults(
			new GAIndividual[]{
					ERR_1_5,
					ERR_1_1
			},
			new ComparisonResults[]{
					EXPECTED_RESULTS_ERR_1_5,
					EXPECTED_RESULTS_ERR_1_1
			});
	}

	@Test
	public void compareIsMonitored() throws Exception {
		final GAProgressMonitor monitor = mock(GAProgressMonitor.class);
		when(monitor.isCancelled()).thenReturn(false);

		mExecutor.compare(monitor, ERR_1_1, ERR_1_1, ERR_1_1, ERR_1_1, ERR_1_1);

		verify(monitor).start(5);
		verify(monitor, atLeastOnce()).done(1);
		verify(monitor, atLeastOnce()).status(anyString());
	}

	@Test
	public void compareCanBeCancelled() throws Exception {
		// Cancel after the third mutant
		final GAProgressMonitor monitor = mock(GAProgressMonitor.class);
		when(monitor.isCancelled()).thenReturn(false).thenReturn(true);

		mExecutor.compare(monitor, ERR_1_1, ERR_1_1, ERR_1_1, ERR_1_1, ERR_1_1);
		verify(monitor, times(4)).isCancelled();

		// Regular mutation should still work after cancelling a task in the middle
		compareTwoAtATimeWorks();
	}

}
