package es.uca.webservices.gamera.exec;

import org.junit.BeforeClass;
import org.junit.Test;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.exec.bpel.BPELExecutor;

/**
 * Tests for the {@link BPELExecutor} class, stopping after the first difference.
 */
public class BPELExecutorStopAtFirstDiffTest extends AbstractBPELExecutorExecutionTest {

	@BeforeClass
	public static void setupSuite() throws Exception {
		// We assume that the base class' @BeforeClass method is run first
		mExecutor.setStopAtFirstDiff(true);
		mExecutor.prepare();
	}

	@Test
	public void compareStopAtFirstDiffIsHonored() throws Exception {
		assertComparisonProducesExpectedResults(
			new GAIndividual[]{
					ERR_1_5,
					ERR_1_1
			},
			new ComparisonResults[]{
					EXPECTED_RESULTS_ERR_1_5_STOP_FIRST_DIFF,
					EXPECTED_RESULTS_ERR_1_1
			});
	}

}
