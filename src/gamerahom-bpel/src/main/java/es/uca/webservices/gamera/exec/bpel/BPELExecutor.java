package es.uca.webservices.gamera.exec.bpel;

import static es.uca.webservices.bpel.bpelunit.BPELUnitSpecification.BPELUNIT_SPEC_NS_PREFIX;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import es.uca.webservices.mutants.subcommands.*;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;
import es.uca.webservices.bpel.bpelunit.CustomizedRunner;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionType;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.DummyProgressMonitor;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.GAProgressMonitor;
import es.uca.webservices.gamera.api.exec.GAStoppableAfterFirstDiffExecutor;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.mutants.subcommands.CompareSubcommand;
import es.uca.webservices.mutants.subcommands.RunSubcommand;
import es.uca.webservices.mutants.operators.Operator;
import es.uca.webservices.mutants.operators.OperatorConstants;

/**
 * <p>Executor for Web Service compositions written in WS-BPEL 2.0.</p>
 *
 * <p>The base port used for the ActiveBPEL engine can be customized using the {@link #DEFAULT_BASE_BPEL_PORT_ENVVAR}
 * environment variable. This is useful when running tests that indirectly use this class, like most of the
 * GAmeraHOM projects. Direct users will want to use the {@link #setBaseBPELPort} method instead, though.</p>
 *
 * <p>The {@link #DEFAULT_BPELUNIT_PORT_ENVVAR} can also be used to modify the port at which BPELUnit mockups will
 * listen, in the same way.</p>
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
public class BPELExecutor extends AbstractBPELExecutor implements GAStoppableAfterFirstDiffExecutor {

	private static final int MAGIC_NUMBER_HASH1 = 1231;
	private static final int MAGIC_NUMBER_HASH2 = 1237;
	private static final Logger LOGGER = LoggerFactory.getLogger(BPELExecutor.class);
	private static final String ODE_WAR_FILENAME = "ode.war";
	private static final int MAGIC_NUMBER_HASHCODE = 31;
	
	private final class BackgroundFeedCompareTask implements Runnable {
		private PrintWriter mWriter;
		private BlockingQueue<GAIndividual> mQueue;

		public BackgroundFeedCompareTask(PrintWriter pathWriter, BlockingQueue<GAIndividual> mutantQueue) {
			mWriter = pathWriter;
			mQueue = mutantQueue;
		}

		@Override
		public void run() {
			try {
				while (true) {
					final GAIndividual individual = mQueue.take();
					LOGGER.debug("Took {}", individual);
					mWriter.println(generate(individual));
					mWriter.flush();
				}
			} catch (InterruptedException e) {
				LOGGER.debug("Write to pipe thread was interrupted", e);
			} catch (GenerationException e) {
				LOGGER.error("There was an error while generating one of the individuals", e);
			} finally {
				mWriter.close();
			}
		}
	}

	private final class BackgroundCompareTask implements Runnable {
		private final PipedReader pipePathReader;

		private BackgroundCompareTask(PipedReader pipePathReader) {
			this.pipePathReader = pipePathReader;
		}

		@Override
		public void run() {
			try {
				mCompareCmd.compare(
					getTestSuiteFile().getCanonicalPath(),
					getOriginalProgramFile().getCanonicalPath(),
					outputFile.getCanonicalPath(),
					pipePathReader);
			} catch (Exception e) {
				LOGGER.error("Error in the background comparison thread", e);
			}
		}
	}

    // Useful for Jenkins jobs
    public static final String DEFAULT_BASE_BPEL_PORT_ENVVAR = "BPEL_BASE_PORT";
    public static final String DEFAULT_BPELUNIT_PORT_ENVVAR = "BPELUNIT_PORT";

    public BPELExecutor() {
        setDefaultBaseBPELPort();
        setDefaultBPELUnitPort();
        setDefaultPathToOdeWar();
    }

    // Options

    private File outputFile = new File("original.out");
    
    private File pathToOdeWar = null;

    private boolean stopAtFirstDiff = false;
    private int baseBpelPort = CustomizedRunner.DEFAULT_ENGINE_PORT;
    private int bpelunitPort = CustomizedRunner.DEFAULT_BPELUNIT_PORT;
    // Data file copy state
	// Should we copy the data file to the path in the .bpts at the next comparison, and rerun the original program?
	private boolean dataFileCopyPending = false;
    // Cached locations for the test data to be loaded from the .bpts
	private List<File> dataFileCopyTargets;

    // Background "mubpel compare" threads
	private CompareSubcommand mCompareCmd;
    private Thread mCompareThread, mFeedCompareThread;

	/**
	 * Number of comparisons that can be performed in parallel. By default, it is set to 1, as
	 * we the current implementation for parallelism uses subprocesses and these can't be launched
	 * from the Eclipse OSGi bundles. Values higher than 1 should work in any regular JVM.
	 */
	private int nComparisonThreads = 1;
    // "run" command for the original results
	private final RunSubcommand mRunCmd = new RunSubcommand();
	// Thread-safe queue
	private BlockingQueue<GAIndividual> mMutantQueue;

	@Option(type=OptionType.PATH_SAVE, fileExtensions={"xml"})
	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile != null ? new File(outputFile) : null;
	}

	public String getOutputFile() {
		return outputFile != null ? outputFile.getPath() : null;
	}
	
	@Option(type=OptionType.PATH_LOAD, fileExtensions={"bpts"})
	public void setTestSuite(String testSuite) {
		super.setTestSuite(testSuite);

		this.dataFileCopyTargets = null;
		if (getDataFileRaw() != null) {
			this.dataFileCopyPending = true;
		}
	}

	@Override
	public void loadDataFile(File f) {
		super.loadDataFile(f);
		if (getDataFileRaw() != null) {
			dataFileCopyPending = true;
		}
	}

	public int getComparisonThreads() {
		return nComparisonThreads;
	}

	public void setComparisonThreads(int threads) {
		this.nComparisonThreads = threads;
	}

    @Override
    public boolean getStopAtFirstDiff() {
        return stopAtFirstDiff;
    }

    @Override
    public void setStopAtFirstDiff(boolean stopAtFirstDiff) {
        this.stopAtFirstDiff = stopAtFirstDiff;
    }

    public int getBaseBPELPort() {
        return baseBpelPort;
    }

    public void setBaseBPELPort(int port) {
        this.baseBpelPort = port;
    }

    private void setDefaultBaseBPELPort() {
        final String sDefaultPort = System.getenv(DEFAULT_BASE_BPEL_PORT_ENVVAR);
        if (sDefaultPort != null) {
            try {
                final int port = Integer.valueOf(sDefaultPort);
                if (port > 0) {
                    LOGGER.debug("Default base port set through the " + DEFAULT_BASE_BPEL_PORT_ENVVAR + " environment variable: " + port);
                    setBaseBPELPort(port);
                }
            } catch (NumberFormatException ex) {
                LOGGER.error(
                        "Environment variable " + DEFAULT_BASE_BPEL_PORT_ENVVAR + " uses an invalid default base port, '{}'",
                        sDefaultPort, ex);
            }
        }
    }

    public int getBPELUnitPort() {
        return bpelunitPort;
    }

    public void setBPELUnitPort(int port) {
        this.bpelunitPort = port;
    }

    private void setDefaultBPELUnitPort() {
        final String sBPELUnitPort = System.getenv(DEFAULT_BPELUNIT_PORT_ENVVAR);
        if (sBPELUnitPort != null) {
            try {
                final int port = Integer.valueOf(sBPELUnitPort);
                if (port > 0) {
                    LOGGER.debug("Default BPELUnit port set through the " + DEFAULT_BPELUNIT_PORT_ENVVAR + " environment variable: " + port);
                    setBPELUnitPort(port);
                }
            } catch (NumberFormatException ex) {
                LOGGER.error("Environment variable " + DEFAULT_BPELUNIT_PORT_ENVVAR + " uses an invalid default port, '{}'", sBPELUnitPort, ex);
            }
        }
    }

    @Override
	public void cleanup() throws PreparationException {
		// Did we call prepare() before?
		if (mFeedCompareThread != null) {
			LOGGER.debug("Cleaning up {}", this);
			mFeedCompareThread.interrupt();
			// the compare thread will finish automatically, once the
			// mFeedCompareThread closes the input side of the pipe
			try {
				mFeedCompareThread.join();
				mCompareThread.join();
			} catch (Exception e) {
				throw new PreparationException(e);
			}
		}
        if (mRunCmd != null) {
            try {
                mRunCmd.cleanup();
            } catch (Exception e) {
                throw new PreparationException(e);
            }
        }
    }

	@Override
	public ComparisonResults[] compare(GAProgressMonitor monitor, GAIndividual... individuals)
			throws ComparisonException {
		try {
			if (monitor == null) {
				monitor = new DummyProgressMonitor();
			}
			if (dataFileCopyPending) {
				runOriginalProgram();
			}

			// Clear the results obtained in the previous batch
			final List<Pair<String, Pair<int[], long[]>>> bkgResults = mCompareCmd.getResults();
			final List<ComparisonResults> lComparisons = new ArrayList<ComparisonResults>();
			synchronized (bkgResults) {
				bkgResults.clear();
				LOGGER.debug("Comparing {} individuals", individuals.length);
				monitor.start(individuals.length);

				// Write all the paths into the pipe, so they can be run in parallel
				for (GAIndividual individual : individuals) {
					mMutantQueue.put(individual);
				}

				// Wait until all results are ready, but check periodically if both
				// producer and consumer threads are still alive
				int prev = 0;
				while (!monitor.isCancelled() && mCompareThread.isAlive() && mFeedCompareThread.isAlive() && bkgResults.size() < individuals.length) {
					bkgResults.wait(1000);
					for (int iRes = prev; iRes < bkgResults.size(); ++iRes) {
						LOGGER.debug("Done with individual #{}: {}", iRes, individuals[iRes]);
						monitor.status(individuals[iRes].individualFieldsToString());
						monitor.done(1);
					}
					prev = bkgResults.size();
				}
				if (!monitor.isCancelled() && bkgResults.size() < individuals.length) {
					LOGGER.error("Failed to compare all {} individuals: only compared {}", individuals.length, bkgResults.size());
				}
				convertMatrixIntoResults(bkgResults, lComparisons);

				LOGGER.debug("Successfully compared {} individuals", bkgResults.size());
			}

			if (monitor.isCancelled()) {
				cancel();
			}

			return lComparisons.toArray(new ComparisonResults[lComparisons.size()]);
		} catch (Exception ex) {
			throw new ComparisonException(ex);
		}
	}

	@Override
	public void prepare() throws PreparationException {
		try {
            runOriginalProgram();
            startBackgroundCompareThread();
        } catch (Exception e) {
			throw new PreparationException(e);
		}
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		super.validate();
		try {
			if (getOutputFile() == null) {
				throw new InvalidConfigurationException("BPEL executor needs the path to the output file");
			}

			outputFile.createNewFile();
			if (!outputFile.canWrite()) {
				throw new InvalidConfigurationException("Cannot write to the output file " + getOutputFile());
			}

			copyDataFile();
		} catch (IOException e) {
			throw new InvalidConfigurationException("Error while creating the output file", e);
		} catch (XPathExpressionException e) {
			throw new InvalidConfigurationException("Could not extract expected data file path from BPTS", e);
		} catch (ParserConfigurationException e) {
			throw new InvalidConfigurationException("Could not extract expected data file path from BPTS", e);
		} catch (SAXException e) {
			throw new InvalidConfigurationException("Could not extract expected data file path from BPTS", e);
		}
	}

	@Override
	public List<String> getTestCaseNames() throws AnalysisException {
		try {
			return mCompareCmd.getTestCaseNames(getTestSuite());
		} catch (Exception e) {
			throw new AnalysisException(e);
		}
	}

    private void cancel() throws IOException,
			InterruptedException, PreparationException, InvalidConfigurationException {
	
		// The fastest way to cancel everything is to interrupt all processing and restart ActiveBPEL
		mMutantQueue.clear();
		cleanup();
		validate();
		prepare();
	}

	private void copyDataFile() throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, InvalidConfigurationException {
		final File dataFile = getDataFileRaw();
		dataFileCopyPending = false;
		if (dataFile == null) {
            LOGGER.info("There was no data file to copy: returning without doing anything");
			return;
		}

		if (dataFileCopyTargets == null) {
			BPELUnitSpecification bpts = new BPELUnitSpecification(getTestSuiteFile());
			final NodeList srcAttrs = (NodeList)bpts.evaluateExpression(
					"//" + BPELUNIT_SPEC_NS_PREFIX + ":dataSource/@src",
					XPathConstants.NODESET);
			dataFileCopyTargets = new ArrayList<File>(srcAttrs.getLength());
			for (int i = 0; i < srcAttrs.getLength(); ++i) {
				final Attr attr = (Attr)srcAttrs.item(i);
				File target = new File(attr.getValue());
				if (!target.isAbsolute()) {
					target = new File(getTestSuiteFile().getParentFile(), attr.getValue());
				}
				dataFileCopyTargets.add(target);
			}
		}

		if (dataFile != null && dataFileCopyTargets.isEmpty()) {
			throw new InvalidConfigurationException("Cannot load a data file if the BPTS doesn't use any");
		}
		for (File target : dataFileCopyTargets) {
			if (!target.equals(dataFile)) {
				target.delete();
				FileUtils.copyFile(dataFile, target);
                LOGGER.info(String.format("Copied the '%s' datafile to target destination '%s'", dataFile, target));
			}
		}
	}

	private void runOriginalProgram() throws Exception {
		if (dataFileCopyPending) {
			copyDataFile();
		}
		if (getPathToOdeWar() != null)
			mRunCmd.setPathToOdeWar(getPathToOdeWar());
		
		mRunCmd.setOutputStream(new FileOutputStream(getOutputFile()));
		mRunCmd.setActiveBPELLogLevel("none");
		mRunCmd.setCleanupAfterExecution(false);
        mRunCmd.setEnginePort(baseBpelPort);
        mRunCmd.setMockupPort(bpelunitPort);
		mRunCmd.run(getTestSuiteFile().getCanonicalPath(), getOriginalProgramFile().getCanonicalPath());
		LOGGER.info("Original output is available at " + getOutputFile());
	}

	private void startBackgroundCompareThread() throws Exception {
		// First, we need two prepare two pipes to communicate the threads in both directions
		final PipedWriter pipePathWriter = new PipedWriter();
		final PipedReader pipePathReader = new PipedReader(pipePathWriter);

		/*
		 * PipedReader requires that all writes are performed from the same
		 * thread. We need one thread to keep on writing to the PipedReader from
		 * a thread-safe blocking queue.
		 */
		final PrintWriter pathWriter = new PrintWriter(pipePathWriter);
		mMutantQueue = new LinkedBlockingQueue<GAIndividual>();
		mFeedCompareThread = new Thread(new BackgroundFeedCompareTask(pathWriter, mMutantQueue));
		mFeedCompareThread.setDaemon(true);
		mFeedCompareThread.start();

		/*
		 * The other side of the pipe feeds the 'mubpel compare' command. We
		 * need to avoid using the default port, since we will be running the
		 * original composition concurrently during preparation.
		 */
		mCompareCmd = new CompareSubcommand();
		
		if(getPathToOdeWar() != null)
			mCompareCmd.setPathToOdeWar(getPathToOdeWar());
		
		mCompareCmd.setKeepGoing(!stopAtFirstDiff);
		mCompareCmd.setCleanupAfterExecution(true);
        mCompareCmd.setMockupPort(bpelunitPort);
		//if (nComparisonThreads > 1) {
        if (nComparisonThreads > 0) {        	
			mCompareCmd.setEnginePort(baseBpelPort + 1);
			mCompareCmd.setThreadCount(nComparisonThreads);
		} else {
            mCompareCmd.setEnginePort(baseBpelPort);
			mCompareCmd.setBPRDir(mRunCmd.getBPRDir());
		}
		mCompareThread = new Thread(new BackgroundCompareTask(pipePathReader));
		mCompareThread.setName("BackgroundCompare");
		// The background comparison thread should not prevent the JVM from
		// exiting if something goes wrong
		mCompareThread.setDaemon(true);
		mCompareThread.start();
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BPELExecutor other = (BPELExecutor) obj;
		if (baseBpelPort != other.baseBpelPort)
			return false;
		if (bpelunitPort != other.bpelunitPort)
			return false;
		if (nComparisonThreads != other.nComparisonThreads)
			return false;
		if (outputFile == null) {
			if (other.outputFile != null)
				return false;
		} else if (!outputFile.equals(other.outputFile))
			return false;
		if (stopAtFirstDiff != other.stopAtFirstDiff)
			return false;
		return true;
	}

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + baseBpelPort;
		result = prime * result + bpelunitPort;
		result = prime * result + nComparisonThreads;
		result = prime * result
				+ ((outputFile == null) ? 0 : outputFile.hashCode());
		result = prime * result + (stopAtFirstDiff ? MAGIC_NUMBER_HASH1 : MAGIC_NUMBER_HASH2);
		return result;
	}

    
    @Override
	public String toString() {
		return "BPELExecutor [outputFile=" + outputFile + ", stopAtFirstDiff="
				+ stopAtFirstDiff + ", baseBpelPort=" + baseBpelPort
				+ ", bpelunitPort=" + bpelunitPort + ", testSuiteFile="
				+ getTestSuiteFile() + ", originalProgramFile="
				+ getOriginalProgramFile() + ", dataFile=" + getDataFileRaw()
				+ "]";
	}

    /**
	 * Returns the path to the WAR distribution of Apache ODE. The default value
	 * is <code>(Rodan installation directory)/ode.war</code>, where the
	 * installation directory is obtained from the
	 * {@link GAExecutor#INSTALL_DIR_SYS_PROP} system property.
	 */
    public File getPathToOdeWar() {
		return pathToOdeWar;
	}

	/**
	 * Changes the path to the ODE WAR file.
	 */
	public void setPathToOdeWar(String pathToOdeWar) { this.pathToOdeWar = new File(pathToOdeWar); }

	private void setDefaultPathToOdeWar() {
		final String installDir = System.getProperty(GAExecutor.INSTALL_DIR_SYS_PROP);
	    if (installDir != null) {
               setPathToOdeWar(new File(installDir, ODE_WAR_FILENAME).getPath());
	    }
	}
}
