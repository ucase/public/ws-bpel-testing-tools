package es.uca.webservices.gamera.exec.bpel;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionType;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.DummyProgressMonitor;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.GAProgressMonitor;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.mutants.operators.Operator;
import es.uca.webservices.mutants.operators.OperatorConstants;
import es.uca.webservices.mutants.subcommands.AnalyzeSubcommand;
import es.uca.webservices.mutants.subcommands.ApplySubcommand;

/**
 * Abstract superclass for all MuBPEL-based executors.
 *
 * @author Antonio García-Domínguez
 */
public abstract class AbstractBPELExecutor implements GAExecutor {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractBPELExecutor.class);

	// From mutant basename (as it may have been run in a remote machine) to GA individual
	private final Map<String, GAIndividual> mapBasenameToIndividual = new HashMap<String, GAIndividual>();

	// From GA individual to locally generated file
	private final Map<GAIndividual, File> mapIndividualToFile = new HashMap<GAIndividual, File>();

	private File originalProgram, testSuite, dataFile;
	
	private String mutationDirectory=null;
	
	@Option(type=OptionType.PATH_LOAD, fileExtensions={"bpts"})
	public void setTestSuite(String testSuite) {
		this.testSuite = testSuite != null ? new File(testSuite) : null;
	}

	public String getTestSuite() {
		return testSuite != null ? testSuite.getPath() : null;
	}

	public File getTestSuiteFile() {
		return testSuite;
	}

	@Option(type=OptionType.PATH_LOAD, fileExtensions={"bpel"})
	public void setOriginalProgram(String originalProgram) {
		this.originalProgram = originalProgram != null ? new File(originalProgram) : null;
	}

	public String getOriginalProgram() {
		return originalProgram != null ? originalProgram.getPath() : null;
	}

	@Override
	public File getOriginalProgramFile() {
		return originalProgram;
	}

	@Option(type=OptionType.PATH_LOAD, fileExtensions={"vm"}, optional=true)
	public void setDataFile(String path) {
		loadDataFile(path != null ? new File(path) : null);
	}

	@Override
	public File getMutantFile(GAIndividual individual) throws UnsupportedOperationException, GenerationException {
		return generate(individual);
	}

	/**
	 * JavaBean-style counterpart to {@link #setDataFile(String)}, to be used by SnakeYAML.
	 */
	public String getDataFile() {
		return dataFile == null ? null : dataFile.getPath();
	}

	public File getDataFileRaw() {
		return dataFile;
	}

	@Override
	public void loadDataFile(File dataFile) throws UnsupportedOperationException {
		this.dataFile = dataFile;
	}
	
	protected void setMutationDirectory(String path){
		mutationDirectory=path;
	}

	@Override
	public AnalysisResults analyze(GAProgressMonitor monitor) throws AnalysisException {
		try {
			if (monitor == null) {
				monitor = new DummyProgressMonitor();
			}
		
			BPELProcessDefinition bpelDef = new BPELProcessDefinition(originalProgram);
			final Collection<Operator> operators = OperatorConstants.getOperatorMap().values();
			Map<String, Pair<Integer, Integer>> rawResults = null;
		
			AnalyzeSubcommand cmd = new AnalyzeSubcommand();
			monitor.start(operators.size());
			for (Operator op : operators) {
				monitor.status(op.getName());
				rawResults = cmd.analyze(bpelDef, rawResults, op);
				monitor.done(1);
				if (monitor.isCancelled()) {
					break;
				}
			}
		
			final int nOperators = rawResults.size();
			String[] opNames = new String[nOperators];
			BigInteger[] opCounts = new BigInteger[nOperators];
			BigInteger[] attrRanges = new BigInteger[nOperators];
		
			int iOperator = 0;
			for (Map.Entry<String, Pair<Integer, Integer>> entry : rawResults.entrySet()) {
				opNames[iOperator] = entry.getKey();
				opCounts[iOperator] = BigInteger.valueOf(entry.getValue().getLeft());
				attrRanges[iOperator] = BigInteger.valueOf(entry.getValue().getRight());
				iOperator += 1;
			}
		
			return new AnalysisResults(opNames, opCounts, attrRanges);
		} catch (Exception e) {
			throw new AnalysisException(e);
		}
	}

	public File generate(GAIndividual individual) throws GenerationException {
		if (mapIndividualToFile.containsKey(individual)) {
			final File cachedFile = mapIndividualToFile.get(individual);
			LOGGER.debug("Using cached file {} for individual {}", cachedFile, individual);
			return cachedFile;
		}
	
		try {
			File mutatedFile = originalProgram;
			for (int iOrder = 1; iOrder <= individual.getOrder(); ++iOrder) {
				final String mutatedPath = mutatedFile.getCanonicalPath();
	            final BigInteger[] currentMutation = individual.getMutant(iOrder);
				final int operator = currentMutation[0].intValue();
				final int location = currentMutation[1].intValue();
				final int attribute = currentMutation[2].intValue();
	
				final ApplySubcommand cmd = new ApplySubcommand();
	            final BPELProcessDefinition prevDef = new BPELProcessDefinition(new File(mutatedPath));
	            final BPELProcessDefinition mutatedDocument = cmd.apply(prevDef, operator, location, attribute);
				final String dstFilename = String.format("%s-m%02d-%02d-%02d-",	mutatedFile.getName(), operator, location, attribute);
				if(mutationDirectory==null){
					mutatedFile = File.createTempFile(dstFilename, ".bpel");
				}else{
					mutatedFile= new File(mutationDirectory+dstFilename+".bpel");
				}
				mutatedDocument.dumpToStream(new FileOutputStream(mutatedFile));
			}
	
			mapIndividualToFile.put(individual, mutatedFile);
			mapBasenameToIndividual.put(mutatedFile.getName(), individual);
			return mutatedFile;
		} catch (Exception e) {
			throw new GenerationException(e);
		}
	}

	protected void convertMatrixIntoResults(
			final List<Pair<String, Pair<int[], long[]>>> cmpMatrix,
			final List<ComparisonResults> cmpResults) throws ComparisonException
	{
		for (final Pair<String, Pair<int[], long[]>> result : cmpMatrix) {
			final String bpelPath = result.getLeft();
			final int[] bkgComparisons = result.getRight().getLeft();
			final long[] bkgNanos = result.getRight().getRight();

			final String basename = new File(bpelPath).getName();
			final ComparisonResults comparisonResults = convertLineIntoResults(basename, bkgComparisons, bkgNanos);

			cmpResults.add(comparisonResults);
		}
	}

	protected ComparisonResults convertLineIntoResults(final String basename, final int[] cmps, final long[] nanos) throws ComparisonException {
		final GAIndividual individual = mapBasenameToIndividual.get(basename);
		if (individual == null) {
			throw new ComparisonException("Could not find individual with basename " + basename);
		}

		final ComparisonResult[] results = new ComparisonResult[cmps.length];
		for (int i = 0; i < cmps.length; ++i) {
			results[i] = ComparisonResult.getObject(cmps[i]);
		}
		final ComparisonResults comparisonResults = new ComparisonResults(individual, results);

		final BigInteger[] bigNanos = new BigInteger[nanos.length];
		for (int i = 0; i < bigNanos.length; ++i) {
			bigNanos[i] = BigInteger.valueOf(nanos[i]);
		}
		comparisonResults.setTestWallNanos(bigNanos);

		return comparisonResults;
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (testSuite == null) {
			throw new InvalidConfigurationException("The path to the BPELUnit test suite must be specified");
		}			
		if (originalProgram == null) {
			throw new InvalidConfigurationException("The path to the original program must be specified");
		}

		if (!testSuite.canRead()) {
			throw new InvalidConfigurationException("The file with the BPELUnit test suite at " + testSuite + " is not readable");
		}
		try {
			new BPELUnitSpecification(testSuite);
		} catch (Exception e) {
			throw new InvalidConfigurationException("Failed to load the BPELUnit test suite at " + testSuite);
		}

		if (!originalProgram.canRead()) {
			throw new InvalidConfigurationException("The file with the original program at " + originalProgram + " is not readable");
		}
		try {
			new BPELProcessDefinition(originalProgram);
		} catch (Exception e) {
			throw new InvalidConfigurationException("Failed to load the BPEL process definition " + originalProgram);
		}

		if (dataFile != null && !dataFile.canRead()) {
			throw new InvalidConfigurationException("Cannot read the data file at " + dataFile);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataFile == null) ? 0 : dataFile.hashCode());
		result = prime
				* result
				+ ((mapBasenameToIndividual == null) ? 0 : mapBasenameToIndividual
						.hashCode());
		result = prime
				* result
				+ ((mapIndividualToFile == null) ? 0 : mapIndividualToFile
						.hashCode());
		result = prime * result
				+ ((originalProgram == null) ? 0 : originalProgram.hashCode());
		result = prime * result
				+ ((testSuite == null) ? 0 : testSuite.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractBPELExecutor other = (AbstractBPELExecutor) obj;
		if (dataFile == null) {
			if (other.dataFile != null)
				return false;
		} else if (!dataFile.equals(other.dataFile))
			return false;
		if (mapBasenameToIndividual == null) {
			if (other.mapBasenameToIndividual != null)
				return false;
		} else if (!mapBasenameToIndividual.equals(other.mapBasenameToIndividual))
			return false;
		if (mapIndividualToFile == null) {
			if (other.mapIndividualToFile != null)
				return false;
		} else if (!mapIndividualToFile.equals(other.mapIndividualToFile))
			return false;
		if (originalProgram == null) {
			if (other.originalProgram != null)
				return false;
		} else if (!originalProgram.equals(other.originalProgram))
			return false;
		if (testSuite == null) {
			if (other.testSuite != null)
				return false;
		} else if (!testSuite.equals(other.testSuite))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AbstractBPELExecutor [mapFileToIndividual="
				+ mapBasenameToIndividual + ", mapIndividualToFile="
				+ mapIndividualToFile + ", originalProgram=" + originalProgram
				+ ", testSuite=" + testSuite + ", dataFile=" + dataFile + "]";
	}

	@Override
	public List<String> getMutationOperatorNames()
			throws UnsupportedOperationException {
		return Arrays.asList(OperatorConstants.OPERATOR_NAMES);
	}
	
}