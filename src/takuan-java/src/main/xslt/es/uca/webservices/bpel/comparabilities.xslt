<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform"
	xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
	xmlns:uca="http://www.uca.es/xpath/2007/11">

	<!-- PLANTILLAS CON NOMBRE -->
	<xsl:template name="instrumentarParaComparabilidad">
		<xsl:param name="expr" />
		
		<xsl:variable name="arbolInstr">
			<xsl:apply-templates
				select="conv:xpath2XML(normalize-space(string($expr)))"
				mode="ins-comp">
				<xsl:with-param name="nivel" select="1" />
			</xsl:apply-templates>
		</xsl:variable>
		
		<xsl:variable name="arbolFinal">
			<xsl:call-template name="instalarFuncionAmbito">
				<xsl:with-param name="arbolInstr" select="$arbolInstr" />
				<xsl:with-param name="nivel" select="0" />
			</xsl:call-template>
		</xsl:variable>

		<xsl:value-of select="conv:xml2XPath($arbolFinal)" />
	</xsl:template>

	<!-- Plantilla para instalacion de funcion ambito -->
	<xsl:template name="instalarFuncionAmbito">
		<xsl:param name="arbolInstr" />
		<xsl:param name="nivel" />

		<xsl:variable name="exprEscapada">
			<xsl:variable name="xpathString">
				<xsl:value-of select="conv:xml2XPath($arbolInstr)" />
			</xsl:variable>
			
			<xsl:variable name="xpathExpr">
				<xsl:choose>
					<xsl:when test="contains($xpathString, 'uca:ambitoPropiedad(')">
						<xsl:variable name="postCall">
							<xsl:value-of select="substring-after(substring-after($xpathString, 'uca:ambitoPropiedad'), ')')"/>
						</xsl:variable>
						<xsl:variable name="preCall">
							<xsl:value-of select="substring-before($xpathString, 'uca:ambitoPropiedad(')"/>
						</xsl:variable>
						
						<xsl:variable name="args">
							<xsl:value-of select="substring-before(substring-after($xpathString, 'uca:ambitoPropiedad('),')')"/>
						</xsl:variable>
						<xsl:variable name="newCall">
							<xsl:value-of select="concat('uca:ambitoPropiedad(', replace($args, $apos, concat('@', $nivel, '@')), ')')"/>
						</xsl:variable>
						
						<xsl:value-of select="concat($preCall, $newCall, $postCall)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$xpathString"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:value-of select="replace($xpathExpr, $apos, concat('@', $nivel, '@'))"/>
		</xsl:variable>

		<xsl:element name="uca:expression">
			<xsl:element name="uca:functionCall">
				<xsl:element name="uca:qname">
					<xsl:attribute name="prefix">uca</xsl:attribute>
					<xsl:attribute name="localPart">ambito</xsl:attribute>
				</xsl:element>
				<xsl:element name="uca:numberConstant">
					<xsl:value-of select="$nivel" />
				</xsl:element>
				<xsl:element name="uca:stringConstant">
					<xsl:text>'</xsl:text>
					<xsl:value-of select="$exprEscapada" />
					<xsl:text>'</xsl:text>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!--  Plantilla para instalacion de funcion imprimirRutas -->
	<xsl:template name="instalarFuncionImprimirRutas">
		<xsl:param name="arbolInstr" />
		<xsl:param name="nivel" />
		
		<xsl:element name="uca:expression">
			<xsl:element name="uca:functionCall">
				<xsl:element name="uca:qname">
					<xsl:attribute name="prefix">uca</xsl:attribute>
					<xsl:attribute name="localPart">imprimirRutas</xsl:attribute>
				</xsl:element>
				<xsl:element name="uca:numberConstant">
					<xsl:value-of select="$nivel" />
				</xsl:element>
				<xsl:element name="uca:stringConstant">
					<xsl:value-of select="concat($apos, replace(conv:xml2XPath($arbolInstr), $apos, concat('@', $nivel, '@')), $apos)"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- INSTRUMENTACION -->

	<!-- No toda funcion constituye un ámbito aparte: las funciones de
		conversión de tipos no hacen otra cosa que reflejar el mismo
		valor de otra manera. Las funciones que realizan tareas como
		negación, recuento de elementos, suma o redondeo 
		se podrían considerar como operadores primitivos, por lo que 
		tampoco deberían ser considerados como ámbitos aparte.		
	-->
	<xsl:template
		match="uca:functionCall[uca:qname/@prefix != '' or (uca:qname/@localPart != 'string' and uca:qname/@localPart != 'boolean' and uca:qname/@localPart != 'not' and uca:qname/@localPart != 'number' and uca:qname/@localPart != 'sum' and uca:qname/@localPart != 'floor' and uca:qname/@localPart != 'ceiling' and uca:qname/@localPart != 'round' and uca:qname/@localPart != 'count')]"
		mode="ins-comp">
		<xsl:param name="nivel" />
		
		<xsl:choose>
			<xsl:when test="uca:qname/@prefix = 'uca' and uca:qname/@localPart = 'ambitoPropiedad'">
				<!-- Copiar, sus parámetros no necesitan ser procesados -->
				<xsl:copy-of select="."/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*" mode="ins-comp" />
					<xsl:apply-templates select="*[1]" mode="ins-comp" />
	
					<xsl:for-each select="*[position()>1]">
						<xsl:variable name="subarbolInstr">
							<xsl:apply-templates mode="ins-comp" select=".">
								<xsl:with-param name="nivel" select="$nivel+1" />
							</xsl:apply-templates>
						</xsl:variable>
	
						<xsl:call-template name="instalarFuncionAmbito">
							<xsl:with-param name="arbolInstr"
								select="$subarbolInstr" />
							<xsl:with-param name="nivel" select="$nivel" />
						</xsl:call-template>
					</xsl:for-each>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Esto refleja el hecho de que en las expresiones (a = b
		and c = d) y (a = b or c = d), a y b por un lado y c y d
		por otro son comparables entre sí, pero no a con c o d ni
		b con c o d, o viceversa. -->
	<xsl:template match="uca:or|uca:and|uca:predicate"
		mode="ins-comp">
		<xsl:param name="nivel" />

		<xsl:copy>
			<xsl:apply-templates select="@*" mode="ins-comp" />

			<xsl:for-each select="*">
				<xsl:variable name="subarbolInstr">
					<xsl:apply-templates mode="ins-comp" select=".">
						<xsl:with-param name="nivel" select="$nivel+1" />
					</xsl:apply-templates>
				</xsl:variable>

				<xsl:call-template name="instalarFuncionAmbito">
					<xsl:with-param name="arbolInstr"
						select="$subarbolInstr" />
					<xsl:with-param name="nivel" select="$nivel" />
				</xsl:call-template>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>

	<!--  Uno de estos nodos siempre delimita el uso de una variable en su nivel más externo,
		gracias a la limitación impuesta por BPEL en la forma de las expresiones XPath (no
		se admiten rutas, teniendo que empezar toda expresión por una referencia a variable,
		constante, literal o llamada a función. Sólo nos interesan este tipo de expresiones:
		
		$a.b   -> variableReference
		$a.b/c -> children(variableReference, c)
		$a.b//c -> alldesc(variableReference, c)
		$a.b[c] -> predicate(variableReference, c)
		
		Queremos evitar instrumentar expresiones como f(x,y)/z, ya que no comienzan por una variable.
	-->
	<xsl:template
		match="*[(self::uca:predicate or self::uca:children or self::uca:alldesc or self::uca:variableReference) and not(parent::uca:predicate or parent::uca:children or parent::uca:alldesc or parent::uca:variableReference) and not(descendant-or-self::uca:children/uca:functionCall or descendant-or-self::uca:alldesc/uca:functionCall or descendant-or-self::uca:predicate/uca:functionCall)]"
		mode="ins-comp">
		<xsl:param name="nivel" />

		<xsl:variable name="subarbol">
			<xsl:copy>
				<xsl:apply-templates mode="ins-comp">
					<xsl:with-param name="nivel" select="$nivel+1" />
				</xsl:apply-templates>
			</xsl:copy>
		</xsl:variable>

		<xsl:call-template name="instalarFuncionImprimirRutas">
			<xsl:with-param name="arbolInstr" select="$subarbol" />
			<xsl:with-param name="nivel" select="$nivel" />
		</xsl:call-template>
	</xsl:template>

	<!-- Regla identidad para la mayoría de los elementos -->
	<xsl:template match="node()|@*" mode="ins-comp">
		<xsl:param name="nivel" />
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" mode="ins-comp">
				<xsl:with-param name="nivel" select="$nivel" />
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
