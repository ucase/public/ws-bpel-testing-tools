<?xml version="1.0" encoding="UTF-8"?>
<!-- 

  XSLT stylesheet which instruments a WS-BPEL 2.0 process definition to be used
  with Takuan. It performs the following tasks:

  - finds the instrumentable program points
  - finds the instrumentable variables in those program points
  - inspects those dummy variables at the beginning and end of each program point
  - adds the dummy variables required to inspect the dummy variables
  - instruments the XPath expressions used to produce comparability scopes

  The instrumentable program points are basically any bpel:sequence activity. The
  stylesheet can also wrap existing standalone activities inside if/else/elseif
  activities and loops into sequences.

  Instrumentable variables include regular variables, and the properties of
  all variables.

  In order to limit the program points that can be instrumented, the user can
  set certain attributes on the bpel:process element or on the program point
  itself. Setting uca:instrumentSequencesByDefault to "no" will allow the user
  to manually select exactly which sequences and if/else/elseif/loops are to
  be instrumented, by setting uca:instrument to "yes" on them. The user can
  also set uca:maxInstrumentationDepth on the bpel:process node to a proper
  number of levels, pruning the instrumentation to a certain depth of the DOM
  tree of the WS-BPEL process definition. Another option is to instrument all
  sequences by default, and discard specific ones  by setting uca:instrument
  to "no" on them.

  Likewise, instrumentable properties can also be limited. To do this, the user
  needs to set uca:instrumentVariablesByDefault to "no" on the bpel:process
  element, and then pick the variables to be instrumented by setting uca:instrument
  to "yes" on them. Another option is to instrument all varialbes by default, and
  discard specific ones by setting uca:instrument to "no" on them.

  Copyright (C) 2008-2011 Antonio García-Domínguez, Alejandro Álvarez-Ayllón
-->
<xsl:stylesheet version="2.0" 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:uca="http://www.uca.es/xpath/2007/11"
		xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
		xmlns:xsd="http://www.w3.org/2001/XMLSchema"
		xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
		xmlns:vprop="http://docs.oasis-open.org/wsbpel/2.0/varprop"
		xmlns:bpelvar="java:es.uca.webservices.bpel.BPELVariable"
		xmlns:bpelproc="java:es.uca.webservices.bpel.BPELProcessDefinition"
		xmlns:conv="java:es.uca.webservices.xpath.ConversorXMLXPath"
		xmlns:map="java:java.util.Map"
		xmlns:prop="java:es.uca.webservices.bpel.BPELProperty"
		xmlns:propalias="java:es.uca.webservices.bpel.wsdl.BPELPropertyAlias"
		xmlns:wsdlcat="java:es.uca.webservices.bpel.wsdl.WSDLCatalog"
		xmlns:wsdlmsg="java:javax.wsdl.Message"
		exclude-result-prefixes="bpelvar conv map prop propalias wsdlcat wsdlmsg"
		>

  <xsl:import href="es/uca/webservices/bpel/comparabilities.xslt"/>

  <!-- Parsed WS-BPEL process definition -->
  <xsl:param name="bpelProcessDef"/>

  <!-- WSDL/XSD catalog from the WS-BPEL process definition -->
  <xsl:variable name="wsdlCatalog" select="bpelproc:getWSDLCatalog($bpelProcessDef)"/>

  <!-- Namespace for our XPath extension functions -->
  <xsl:variable name="uca" select="'http://www.uca.es/xpath/2007/11'"/>

  <!-- We need to use this variable to use the single quote ' in expressions: there is no way to escape it in XPath -->
  <xsl:variable name="apos">
    <xsl:text>'</xsl:text>
  </xsl:variable>

  <!-- SEQUENCE INSTRUMENTATION -->

  <!-- We need to preserve the first and last elements (normally, bpel:receive and bpel:send activities, respectively) -->
	<xsl:template match="bpel:process/bpel:sequence[bpelproc:shouldInstrumentElement($bpelProcessDef, .)]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates select="*[1]" />

			<xsl:call-template name="inspect-variables">
				<xsl:with-param name="nombrePunto" select="'ENTER'" />
			</xsl:call-template>
			<xsl:apply-templates select="*[position()>1 and position() &lt; last()]" />
			<xsl:call-template name="inspect-variables">
				<xsl:with-param name="nombrePunto" select="'EXIT'" />
			</xsl:call-template>

			<xsl:apply-templates select="*[last()]" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="bpel:sequence[not(parent::bpel:process)][bpelproc:shouldInstrumentElement($bpelProcessDef, .)]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />

			<xsl:call-template name="inspect-variables">
				<xsl:with-param name="nombrePunto" select="'ENTER'" />
			</xsl:call-template>
			<xsl:apply-templates />
			<xsl:call-template name="inspect-variables">
				<xsl:with-param name="nombrePunto" select="'EXIT'" />
			</xsl:call-template>
		</xsl:copy>
	</xsl:template>

  <!-- Create sequences for standalone activities inside loops or if/else activities -->
	<xsl:template match="*[self::bpel:if or self::bpel:elseif or self::bpel:else][not(bpel:sequence) and not(bpel:empty)][bpelproc:shouldInstrumentElement($bpelProcessDef, .)]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates select="bpel:condition|bpel:targets|bpel:sources" />

			<bpel:sequence>
				<xsl:call-template name="inspect-variables">
					<xsl:with-param name="nombrePunto" select="'ENTER'" />
				</xsl:call-template>
				<xsl:apply-templates
					select="*[not(self::bpel:condition or self::bpel:targets or self::bpel:sources or self::bpel:else or self::bpel:elseif)]" />
				<xsl:call-template name="inspect-variables">
					<xsl:with-param name="nombrePunto" select="'EXIT'" />
				</xsl:call-template>
			</bpel:sequence>
			<xsl:apply-templates select="bpel:else|bpel:elseif" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="bpel:repeatUntil[not(bpel:sequence) and not(bpel:empty)][bpelproc:shouldInstrumentElement($bpelProcessDef, .)]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates select="bpel:targets|bpel:sources" />
			<bpel:sequence>
				<xsl:call-template name="inspect-variables">
					<xsl:with-param name="nombrePunto" select="'ENTER'" />
				</xsl:call-template>
				<xsl:apply-templates
					select="*[not(self::bpel:condition or self::bpel:targets or self::bpel:sources)]" />
				<xsl:call-template name="inspect-variables">
					<xsl:with-param name="nombrePunto" select="'EXIT'" />
				</xsl:call-template>
			</bpel:sequence>
			<xsl:apply-templates select="bpel:condition" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="bpel:scope[not(bpel:sequence) and not(bpel:empty)][bpelproc:shouldInstrumentElement($bpelProcessDef, .)]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates
				select="bpel:targets|bpel:sources|bpel:variables|bpel:partnerLinks|bpel:messageExchanges|bpel:correlationSets|bpel:eventHandlers|bpel:faultHandlers|bpel:compensationHandler|bpel:terminationHandler" />
			<bpel:sequence>
				<xsl:call-template name="inspect-variables">
					<xsl:with-param name="nombrePunto" select="'ENTER'" />
				</xsl:call-template>
				<xsl:apply-templates
					select="*[not(self::bpel:targets or self::bpel:sources or self::bpel:variables or self::bpel:partnerLinks or self::bpel:messageExchanges or self::bpel:correlationSets or self::bpel:eventHandlers or self::bpel:faultHandlers or self::bpel:compensationHandler or self::bpel:terminationHandler)]" />
				<xsl:call-template name="inspect-variables">
					<xsl:with-param name="nombrePunto" select="'EXIT'" />
				</xsl:call-template>
			</bpel:sequence>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="*[self::bpel:catch or self::bpel:catchAll or self::bpel:compensationHandler or self::bpel:terminationHandler][not(bpel:sequence)][not(bpel:empty)][bpelproc:shouldInstrumentElement($bpelProcessDef, .)]">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<bpel:sequence>
				<xsl:call-template name="inspect-variables">
					<xsl:with-param name="nombrePunto" select="'ENTER'" />
				</xsl:call-template>
				<xsl:apply-templates />
				<xsl:call-template name="inspect-variables">
					<xsl:with-param name="nombrePunto" select="'EXIT'" />
				</xsl:call-template>
			</bpel:sequence>
		</xsl:copy>
	</xsl:template>

  <!-- VARIABLE INSPECTIONS -->

  <!-- Starting point for creating the dummy variables used in inspections -->
  <xsl:template match="bpel:variables">
  	<xsl:copy>
  		<xsl:apply-templates/>
  	</xsl:copy>
  </xsl:template>

  <!-- Create duplicates of the variables to be used as destinations for the copies -->
  <xsl:template match="bpel:variable">
    <!-- Two xsl:copy elements copy the original variable and create a duplicate, along 
         with its namespace nodes, which will contain the dummy variable. -->
    <xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
    <xsl:copy>
      <xsl:attribute name="name"><xsl:value-of select="concat('dummy_', @name)"/></xsl:attribute>
      <xsl:attribute name="uca:instrument">no</xsl:attribute>
      <xsl:apply-templates select="@*[name(.) != 'name' and name(.) != 'uca:instrument']|node()"/>
    </xsl:copy>

    <!-- Variables for the properties of the current variable -->
    <xsl:variable name="var" select="bpelvar:new(.)"/>
    <xsl:for-each select="wsdlcat:getVariablePropertyAliasesAsXML($wsdlCatalog, $var)//alias">
      <xsl:element name="bpel:variable">
        <xsl:attribute name="name"><xsl:value-of select="@dummyVarName"/></xsl:attribute>
		<xsl:attribute name="uca:instrument">no</xsl:attribute>
		<xsl:copy-of select="namespace::* | @type | @element"/>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>

  <!-- Inspect the available variables by copying them to dummy variables of the same type. -->
  <xsl:template name="inspect-variables">
    <xsl:param name="nombrePunto"/>

	<xsl:variable name="vars" as="node()*" select="ancestor::*[self::bpel:process or self::bpel:scope]/bpel:variables/bpel:variable[bpelproc:shouldInstrumentVariable($bpelProcessDef, .)]"/>

    <xsl:comment>
      <xsl:text>start inspections</xsl:text>
    </xsl:comment>
    <xsl:if test="count($vars) &gt; 0">
      <bpel:assign>
		  <xsl:attribute name="uca:punto">
			  <xsl:value-of select="$nombrePunto"/>
		  </xsl:attribute>
	 	  <xsl:apply-templates select="$vars" mode="inspect"/>
       </bpel:assign>
    </xsl:if>
    <xsl:comment>
      <xsl:text>end inspections</xsl:text>
    </xsl:comment>
  </xsl:template>
  
  <xsl:template match="bpel:variable[@messageType]" mode="inspect">
    <xsl:variable name="varName" select="@name"/>
    <xsl:variable name="var" select="bpelvar:new(.)"/>
    <xsl:variable name="msgType" select="wsdlcat:getMessageType($wsdlCatalog, bpelvar:getMessageType($var))"/>
    <xsl:variable name="parts" as="xsd:string*" select="map:keySet(wsdlmsg:getParts($msgType))"/>

	<xsl:for-each select="$parts">
		<xsl:variable name="expr" select="replace(concat('$', $varName, '.', .), $apos, '@0@')"/>
		<bpel:copy ignoreMissingFromData="yes">
			<bpel:from>
				<xsl:value-of select="concat('uca:inspeccionar(0, ', $apos, $expr, $apos, ')')" />
			</bpel:from>
			<bpel:to>
				<xsl:value-of select="concat('$dummy_', $varName, '.', .)" />
			</bpel:to>
		</bpel:copy>
	</xsl:for-each>

    <xsl:apply-templates select="." mode="inspect-properties"/>
  </xsl:template>

  <xsl:template match="bpel:variable[not(@messageType)]" mode="inspect">
	<xsl:variable name="expr" select="replace(concat('$', @name), $apos, '@0@')"/>

	<bpel:copy ignoreMissingFromData="yes">
		<bpel:from><xsl:value-of select="concat('uca:inspeccionar(0, ', $apos, $expr, $apos, ')')" /></bpel:from>
		<bpel:to><xsl:value-of select="concat('$dummy_', @name)" /></bpel:to>
	</bpel:copy>

    <xsl:apply-templates select="." mode="inspect-properties"/>
  </xsl:template>

  <xsl:template match="bpel:variable" mode="inspect-properties">
    <xsl:variable name="var" select="bpelvar:new(.)"/>

    <xsl:for-each select="wsdlcat:getVariablePropertyAliasesAsXML($wsdlCatalog, $var)//alias">
      <bpel:copy ignoreMissingFromData="yes">
        <bpel:from>
          <xsl:value-of select="concat('uca:inspeccionarPropiedad(', $apos, @varName, $apos, ', ', $apos, @nameNS, $apos, ', ', $apos, @name, $apos, ')')"/>
        </bpel:from>
		<bpel:to><xsl:value-of select="concat('$', @dummyVarName)"/></bpel:to>
	  </bpel:copy>
    </xsl:for-each>
  </xsl:template>

  <!-- ADD COMPARABLE EXPRESSION SCOPES -->

  <xsl:template match="bpel:condition|bpel:copy/bpel:from[not(bpel:literal) and not(@property) and not(@partnerLink)]|bpel:startCounterValue|bpel:finalCounterValue|bpel:branches">
    <xsl:copy>
      <xsl:apply-templates select="@*[local-name(.) != 'part' and local-name(.) != 'variable']"/>
      
      <!-- Expresión a instrumentar -->
	<xsl:variable name="expresion">
		<xsl:choose>
			<xsl:when test="@variable">
				<xsl:value-of select="concat('$', @variable)" />
				<xsl:if test="@part">
					<xsl:value-of select="concat('.', @part)" />
				</xsl:if>
				<xsl:if test="text()">
					<xsl:value-of select="concat('/', normalize-space(string(.)))" />
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="normalized">
					<xsl:value-of select="normalize-space(string(.))" />
				</xsl:variable>
				<xsl:value-of
					select="replace($normalized, 'bpel:getVariableProperty', 'uca:ambitoPropiedad')" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
      
      <!-- Instrumentamos usando la plantilla de comparabilidad -->
	<xsl:call-template name="instrumentarParaComparabilidad">
		<xsl:with-param name="expr" select="$expresion" />
	</xsl:call-template>   
    </xsl:copy>
  </xsl:template>
  
  <!-- Copy from property -->
  <xsl:template match="bpel:copy/bpel:from[not(bpel:literal) and @property]">
	  <bpel:from>
		  <xsl:value-of select="concat('uca:ambitoPropiedad(', $apos, @variable, $apos, ',', $apos, @property, $apos, ')')"/>
	  </bpel:from>
  </xsl:template>

  <!-- DEFAULT RULE -->

	<!-- Copy as-is. When copying the bpel:process element, we will register
		the UCA WS-BPEL extension, which is required in order to use our XPath extension
		functions. -->

  <xsl:template match="@*|node()">
    <xsl:copy>
      <!-- Primero los atributos, y luego los elementos -->
      <xsl:apply-templates select="@*"/>

      <xsl:choose>
        <xsl:when test="self::bpel:process">
          <xsl:attribute name="uca:dummy">dummy</xsl:attribute>
          <!-- Creamos una lista de extensiones si no la hay -->
          <xsl:if test="not(bpel:extensions)">
            <bpel:extensions>
              <bpel:extension mustUnderstand="no" namespace="{$uca}"/>
            </bpel:extensions>
          </xsl:if>
        </xsl:when>
        <xsl:when test="self::bpel:extensions">
          <!-- Añadimos la extensión de la UCA a la lista si no existe -->
          <xsl:if test="not(bpel:extension[@namespace = $uca])">
            <bpel:extension mustUnderstand="no" namespace="{$uca}"/>
          </xsl:if>
        </xsl:when>
      </xsl:choose>

      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
