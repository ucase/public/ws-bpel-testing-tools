package es.uca.webservices.bpel.daikon.decls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents a Daikon program point. Every program point has a name (ending in
 * ::ENTER or ::EXITN, where N is a number indicating it is the N-th exit
 * point), and a list of variable declarations.
 * 
 * @author Antonio García-Domínguez
 */
public class ProgramPoint {

	private String name;
	private List<Variable> variables = new ArrayList<Variable>();
	private ProgramPointPart part;

	/**
	 * Creates a new program point withe the specified name. The program point
	 * does not declare any variables yet: use
	 * {@link #addDeclaration(Variable)} for that.
	 * 
	 * @param name
	 *            Name of the program point.
	 * @param part 
	 */
	public ProgramPoint(String name, ProgramPointPart part) {
		this.name = name;
		this.part = part;
	}

	/**
	 * Returns the full name of this program point, including the entry/exit point.
	 */
	public String getName() {
		return name + (part == ProgramPointPart.ENTER ? ":::ENTER" : ":::EXIT1");
	}

	/**
	 * Adds the declaration for the variable at the end of the list of
	 * declarations.
	 */
	public void addDeclaration(Variable variable) {
		this.variables.add(variable);
	}

	/**
	 * Returns an unmodifiable list with the declarations for this program
	 * point. To add more declarations, please use
	 * {@link #addDeclaration(Variable)}.
	 */
	public List<Variable> getDeclarations() {
		return Collections.unmodifiableList(variables);
	}

	public ProgramPointPart getPart() {
		return part;
	}

	public void setPart(ProgramPointPart part) {
		this.part = part;
	}

	@Override
	public String toString() {
		return String.format("DaikonProgramPoint [name=%s, variables=%s]", name, variables);
	}
}
