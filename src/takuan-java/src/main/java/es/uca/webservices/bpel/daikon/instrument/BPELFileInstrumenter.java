package es.uca.webservices.bpel.daikon.instrument;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.util.XSLTStylesheet;
import es.uca.webservices.xpath.ConversorXMLXPath;
import es.uca.webservices.xpath.parser.XPathParser;

/**
 * This class instruments the BPEL file using the XSLT stylesheet
 * 
 * @author Antonio García-Domínguez, Alejandro Álvarez-Ayllón
 * @version 1.2
 */
public class BPELFileInstrumenter {

	private static final String RUTA_HOJAXSLT = "es/uca/webservices/bpel/inspect.xslt";
	private static final Logger LOGGER = LoggerFactory.getLogger(BPELFileInstrumenter.class);

	private XSLTStylesheet xslt;
	private BPELProcessDefinition bpelProcessDef;

	/**
	 * Main method. This is the entry point if this class is run as a program.
	 * 
	 * @param args
	 *            The arguments passed to the application
	 * @throws FileNotFoundException
	 *             The file was not found
	 * @throws TransformerException
	 *             An error occurred while trying to transform the file
	 */
	public static void main(String[] args) throws FileNotFoundException, TransformerException {

		if (args.length != 2) {
			System.err.println("Usage: <path to source BPEL file> <path to output BPEL file>");
			System.exit(1);
		}

		final String sOrigen = args[0], sResultado = args[1];

		try {
			final BPELProcessDefinition def = new BPELProcessDefinition(new File(sOrigen));
			BPELFileInstrumenter instr = new BPELFileInstrumenter(def);
			instr.instrument(new File(sResultado));
			LOGGER.info("Process instrumented succesfully");
		} catch (Exception e) {
			LOGGER.error("Errors during instrumentation", e);
		}
	}

	public BPELFileInstrumenter(BPELProcessDefinition def) {
		this.bpelProcessDef = def;

		xslt = new XSLTStylesheet(RUTA_HOJAXSLT);
		xslt.setParameter("bpelProcessDef", bpelProcessDef);
	}

	/**
	 * Instruments the received WS-BPEL process definition for its use in Takuan. The
	 * instrumented WS-BPEL process definition is stored in a temporary file.
	 */
	public BPELProcessDefinition instrument() throws BPELInstrumentationException
	{
		try {
			return instrument(File.createTempFile("instrumented", ".bpel"));
		} catch (IOException ex) {
			throw new BPELInstrumentationException(ex);
		}
	}

	/**
	 * Instruments the received WS-BPEL process definition for its use in Takuan.
	 */
	public BPELProcessDefinition instrument(final File fDestination)
			throws BPELInstrumentationException
	{
		XPathParser.class.getClass();
		ConversorXMLXPath.class.getClass();

		try {
			xslt.transform(bpelProcessDef.getDocument(), fDestination);
			return new BPELProcessDefinition(fDestination, bpelProcessDef);
		} catch (Exception ex) {
			throw new BPELInstrumentationException(ex);
		}
	}

}
