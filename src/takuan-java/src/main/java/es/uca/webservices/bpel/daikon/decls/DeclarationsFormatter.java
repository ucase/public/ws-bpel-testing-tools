package es.uca.webservices.bpel.daikon.decls;

import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.util.List;

/**
 * Tests for converting Daikon program point and declaration objects into the actual .decls format.
 *
 * @author Antonio García-Domínguez
 */
public class DeclarationsFormatter {

	private static final String FIRST_SEPARATOR = " # ";
	private static final String REST_SEPARATOR = ", ";

	public void format(Variable decl, Writer sw) throws IOException {
		sw.write(decl.getVariableName());
		sw.write("\n");
		sw.write(decl.getOriginalType());

		String separator = FIRST_SEPARATOR;
		if (decl.getMinimumValue() != null) {
			sw.write(separator);
			sw.write("minvalue=");
			sw.write(decl.getMinimumValue());
			separator = REST_SEPARATOR;
		}
		if (decl.getMaximumValue() != null) {
			sw.write(separator);
			sw.write("maxvalue=");
			sw.write(decl.getMaximumValue());
			separator = REST_SEPARATOR;
		}
		if (decl.getValuesAsString() != null) {
			sw.write(separator);
			sw.write("validvalues=");
			sw.write(decl.getValuesAsString());
			separator = REST_SEPARATOR;
		}
		if (!decl.getMinLengthComponents().isEmpty()) {
			sw.write(separator);
			sw.write("minlength=");
			appendLengthComponents(sw, decl.getMinLengthComponents());
			separator = REST_SEPARATOR;
		}
		if (!decl.getMaxLengthComponents().isEmpty()) {
			sw.write(separator);
			sw.write("maxlength=");
			appendLengthComponents(sw, decl.getMaxLengthComponents());
			separator = REST_SEPARATOR;
		}

		sw.write("\n");
		sw.write(decl.getJavaType());
		sw.write("\n");
		sw.write(String.valueOf(decl.getComparability()));
	}

	public void format(ProgramPoint pp, Writer sw) throws IOException {
		sw.write("DECLARE\n");
		sw.write(pp.getName());
		sw.write("\n");

		String varSeparator = "";
		for (Variable decl : pp.getDeclarations()) {
			sw.write(varSeparator);
			format(decl, sw);
			varSeparator = "\n";
		}
	}

	private void appendLengthComponents(Writer sw, final List<BigInteger> cmps) throws IOException {
		String nSep = "[";
		for (BigInteger bi : cmps) {
			sw.write(nSep);
			sw.write(bi != null ? bi.toString() : "*");
			nSep = " ";
		}
		sw.write("]");
	}

}
