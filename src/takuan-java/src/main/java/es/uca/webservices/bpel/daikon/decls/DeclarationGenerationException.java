package es.uca.webservices.bpel.daikon.decls;

/**
 * Represents an exception produced while generating Daikon declarations.
 * @author Antonio García-Domínguez
 */
public class DeclarationGenerationException extends Exception {

	private static final long serialVersionUID = 1L;

	public DeclarationGenerationException(String message) {
		super(message);
	}

	public DeclarationGenerationException(Throwable cause) {
		super(cause);
	}

	public DeclarationGenerationException(String message, Throwable cause) {
		super(message, cause);
	}
}
