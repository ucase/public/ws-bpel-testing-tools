package es.uca.webservices.bpel.daikon.instrument;

/**
 * Exception for errors during the instrumentation of a WS-BPEL process
 * definition for Takuan.
 * 
 * @author Antonio García-Domínguez
 */
public class BPELInstrumentationException extends Exception {

	private static final long serialVersionUID = 1L;

	public BPELInstrumentationException(String message) {
		super(message);
	}

	public BPELInstrumentationException(Throwable cause) {
		super(cause);
	}

	public BPELInstrumentationException(String message, Throwable cause) {
		super(message, cause);
	}
}
