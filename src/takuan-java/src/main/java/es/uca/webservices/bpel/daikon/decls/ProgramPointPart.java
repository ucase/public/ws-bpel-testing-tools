package es.uca.webservices.bpel.daikon.decls;

/**
 * Lists the possible parts in a program point that we can be in. For WS-BPEL, we only have two:
 * the enter and exit points.
 */
public enum ProgramPointPart {
	ENTER, EXIT
}
