package es.uca.webservices.bpel.daikon.decls;

import java.io.Writer;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.wsdl.Message;
import javax.wsdl.Part;
import javax.xml.xpath.XPathExpressionException;

import org.apache.xmlbeans.SchemaAttributeModel;
import org.apache.xmlbeans.SchemaField;
import org.apache.xmlbeans.SchemaGlobalElement;
import org.apache.xmlbeans.SchemaLocalAttribute;
import org.apache.xmlbeans.SchemaParticle;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlAnySimpleType;
import org.apache.xmlbeans.impl.schema.SchemaParticleImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import serviceAnalyzer.messageCatalog.TypeGA;
import serviceAnalyzer.messageCatalog.TypeTypedef;
import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.BPELVariable;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.bpel.xsd.XMLSchemaCatalog;
import es.uca.webservices.wsdl.ast.VariableTypes;

/**
 * Generates and formats Daikon program points from a WS-BPEL process
 * definition.
 * 
 * @author Antonio García-Domínguez
 */
public class DeclarationsGenerator {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeclarationsGenerator.class);

	/**
	 * Generates declarations for every program point in <code>def</code>
	 * and prints them to <code>writer</code> using <code>fmt</code> as
	 * formatter.
	 */
	public void generateAndPrint(BPELProcessDefinition def, DeclarationsFormatter fmt, Writer writer)
				throws DeclarationGenerationException
	{
		try {
			final List<Element> sequences = def.getInstrumentableSequences();
			boolean bFirst = true;
			for (Element sequence : sequences) {
				// Since we have the same variables in the enter and exit points,
				// we can do the whole analysis just once.
				final ProgramPoint pp = generate(def, sequence, ProgramPointPart.ENTER);

				if (bFirst) {
					bFirst = false;
				} else {
					writer.write("\n\n");
				}

				fmt.format(pp, writer);
				writer.write("\n\n");
				pp.setPart(ProgramPointPart.EXIT);
				fmt.format(pp, writer);
			}
		} catch (Exception e) {
			throw new DeclarationGenerationException(e);
		}
	}

	public List<ProgramPoint> generate(BPELProcessDefinition def) throws DeclarationGenerationException {
		try {
			return generate(def, def.getInstrumentableSequences());
		} catch (XPathExpressionException e) {
			throw new DeclarationGenerationException("Could not list the instrumentable program points", e);
		}
	}

	public List<ProgramPoint> generate(BPELProcessDefinition def,	List<Element> instrumentableSequences) throws DeclarationGenerationException
	{
		try {
			final List<ProgramPoint> pps = new ArrayList<ProgramPoint>();
			for (Element seq : instrumentableSequences) {
				pps.add(generate(def, seq, ProgramPointPart.ENTER));
				pps.add(generate(def, seq, ProgramPointPart.EXIT));
			}
			return pps;
		} catch (Exception ex) {
			throw new DeclarationGenerationException(ex);
		}
	}

	public ProgramPoint generate(BPELProcessDefinition def, Element seq, ProgramPointPart part)
			throws XPathExpressionException, DeclarationGenerationException
	{
		final String name = computeSequenceLabel(def, seq, part);
		final ProgramPoint pp = new ProgramPoint(name, part);
		for (Variable decl : processProgramPoint(def, seq)) {
			pp.addDeclaration(decl);
		}
		return pp;
	}

	/**
	 * Generates a list of Daikon variable declarations from a WSDL message
	 * type.
	 */
	public List<Variable> generate(XMLSchemaCatalog catalog, String prefix, Message msg)
			throws DeclarationGenerationException
	{
		final List<Variable> decls = new ArrayList<Variable>();
		processMessage(catalog, prefix, msg, decls);
		return decls;
	}

	/**
	 * Generates a list of Daikon declaration from an XML Schema element.
	 */
	public List<Variable> generate(String prefix, SchemaGlobalElement globalElement)
			throws DeclarationGenerationException
	{
		final List<Variable> decls = new ArrayList<Variable>();
		processParticle(prefix, (SchemaParticle)globalElement, decls);
		return decls;
	}

	/**
	 * Generates a list of Daikon declarations from an XML Schema type.
	 */
	public List<Variable> generate(String prefix, SchemaType type)
			throws DeclarationGenerationException
	{
		final List<Variable> decls = new ArrayList<Variable>();
		processType(prefix, type, decls);
		return decls;
	}

	private List<Variable> processProgramPoint(BPELProcessDefinition bpel, Element programPoint)
			throws DeclarationGenerationException
	{
		try {
			final List<BPELVariable> variables = bpel.getProgramPointVariables(programPoint);
			final List<Variable> decls = new ArrayList<Variable>();

			for (BPELVariable variable : variables) {
				List<Variable> varDecls;
				if (variable.getType() != null) {
					final SchemaType type = bpel.getXMLSchemaCatalog().getType(variable.getType());
					varDecls = processVariableWithType(type, variable);
				}
				else if (variable.getElement() != null) {
					final SchemaGlobalElement element = bpel.getXMLSchemaCatalog().getElement(variable.getElement());
					varDecls = processVariableWithElement(element, variable);
				}
				else if (variable.getMessageType() != null) {
					final Message messageType = bpel.getWSDLCatalog().getMessageType(variable.getMessageType());
					varDecls = processVariableWithMessageType(bpel.getXMLSchemaCatalog(), messageType, variable);
				}
				else {
					throw new DeclarationGenerationException(
						"Variable " + variable.getName() + " doesn't have any of type/element/messageType set");
				}

				decls.addAll(varDecls);
			}

			return decls;
		}
		catch (DeclarationGenerationException dex) {
			throw dex;
		}
		catch (Exception ex) {
			throw new DeclarationGenerationException(ex);
		}
	}

	private List<Variable> processVariableWithMessageType(final XMLSchemaCatalog catalog, final Message message, BPELVariable variable)
			throws DeclarationGenerationException
	{
		if (message == null) {
			throw new DeclarationGenerationException(
				"Could not find message type " + variable.getMessageType());
		}
		return generate(catalog, variable.getName(), message);
	}

	private List<Variable> processVariableWithElement(final SchemaGlobalElement elementType, BPELVariable variable)
			throws DeclarationGenerationException
	{
		if (elementType == null) {
			throw new DeclarationGenerationException(
				"Could not find element type " + variable.getElement());
		}
		return generate(variable.getName(), elementType);
	}

	private List<Variable> processVariableWithType(final SchemaType type, BPELVariable variable)
			throws DeclarationGenerationException
	{
		if (type == null) {
			throw new DeclarationGenerationException(
				"Could not find type " + variable.getType());
		}
		return generate(variable.getName(), type);
	}

	@SuppressWarnings("unchecked")
	private void processMessage(XMLSchemaCatalog catalog, String prefix, Message msg, final List<Variable> decls)
			throws DeclarationGenerationException
	{
		for (Part part : (Collection<Part>)msg.getParts().values()) {
			final String partName = concatenate(prefix, part.getName());
			if (part.getElementName() != null) {
				final SchemaGlobalElement elem = catalog.getElement(part.getElementName());
				if (elem == null) {
					throw new DeclarationGenerationException("Could not find element " + part.getElementName());
				}
				processParticle(partName, (SchemaParticle)elem, decls);
			}
			else if (part.getTypeName() != null) {
				final SchemaType type = catalog.getType(part.getTypeName());
				if (type == null) {
					throw new DeclarationGenerationException("Could not find type " + part.getTypeName());
				}
				if (type.getContentType() == SchemaType.ELEMENT_CONTENT) {
					/*
					 * Add RPC style wrapper elements for each part.
					 * 
					 * We assume that the WSDL is WS-I BP 1.1 compliant, so
					 * wsdl:part/@type is only used with the rpc/literal style,
					 * and wsdl:part/@element with the document/literal style.
					 */
					final int originalSize = decls.size();
					processType(concatenate(partName, part.getName() + "[]"), type, decls);
					for (int i = originalSize; i < decls.size(); ++i) {
						final Variable decl = decls.get(i);
						decl.addMinLengthComponent(BigInteger.ONE);
						decl.addMaxLengthComponent(BigInteger.ONE);
						decl.setJavaType(decl.getJavaType() + "[]");
						decl.setOriginalType(decl.getOriginalType() + "[]");
					}
				}
				else {
					// Simple content does not require a wrapper element
					processType(partName, type, decls);
				}
			}
			else {
				throw new DeclarationGenerationException("Neither element nor type were set for the message " + msg.getQName());
			}
		}
	}

	private void processType(String prefix, SchemaType type, List<Variable> decls) throws DeclarationGenerationException {
		if (type.isNoType()) {
			throw new DeclarationGenerationException("noType is not supported");
		}
		else if (type.isSimpleType()) {
			decls.add(processSimpleType(prefix, type));
		}
		else {
			processComplexType(prefix, type, decls);
		}
	}

	private void processComplexType(String prefix, SchemaType type, List<Variable> decls) throws DeclarationGenerationException {
		if (type.getContentType() == SchemaType.SIMPLE_CONTENT) {
			decls.add(processSimpleType(prefix, type));
		}

		final SchemaParticle contentModel = type.getContentModel();
		if (contentModel != null) {
			processParticle(prefix, contentModel, decls);
		}
	}

	private void processParticle(String prefix, SchemaParticle particle, List<Variable> decls) throws DeclarationGenerationException {
		if (particle.getParticleType() == SchemaParticle.WILDCARD) {
			LOGGER.debug("Schema particle " + particle.getName() + " has wildcard contents: ignoring");
			return;
		}

		final int originalSize = decls.size();
		if (particle.getParticleType() == SchemaParticle.ELEMENT) {
			processElementParticle(prefix, particle, decls);
		} else {
			// choice/sequence/all: process recursively and apply
			// min/maxOccurs to the declarations for the element.
			for (SchemaParticle child : particle.getParticleChildren()) {
				processParticle(prefix, child, decls);
			}

			final BigInteger minOccurs = computeMinOccurs(particle);
			final BigInteger maxOccurs = computeMaxOccurs(particle);
			for (int i = originalSize; i < decls.size(); ++i) {
				final Variable childDecl = decls.get(i);
				childDecl.multiplyFirstMinComponentBy(minOccurs);
				childDecl.multiplyFirstMaxComponentBy(maxOccurs);
			}
		}
	}

	private void processElementParticle(String prefix, SchemaParticle contentModel, final List<Variable> decls) throws DeclarationGenerationException {
		final SchemaType type    = contentModel.getType();
		final String elementName = concatenate(prefix, contentModel.getName().getLocalPart() + "[]");
		final int originalSize = decls.size();

		processType(elementName, type, decls);
		final SchemaAttributeModel attrModel = type.getAttributeModel();
		if (attrModel != null) {
			for (SchemaLocalAttribute attr : attrModel.getAttributes()) {
				processAttribute(elementName, attr, decls);
			}
		}

		applyContainerRestrictions((SchemaField)contentModel,
			decls, originalSize,
			computeMinOccurs(contentModel), computeMaxOccurs(contentModel));
	}

	private void processAttribute(final String prefix,
			SchemaLocalAttribute attr, final List<Variable> decls)
			throws DeclarationGenerationException
	{
		final int originalSize = decls.size();
		processType(
				concatenate(prefix, "@" + attr.getName().getLocalPart() + "[]"),
				attr.getType(), decls);

		applyContainerRestrictions((SchemaField)attr,
				decls, originalSize,
				attr.getMinOccurs(), attr.getMaxOccurs());
	}

	private Variable processSimpleType(String prefix, SchemaType type)
			throws DeclarationGenerationException {
		switch (type.getSimpleVariety()) {
		case SchemaType.ATOMIC:
			return processSimpleAtomicType(prefix, type);
		case SchemaType.LIST:
			return processSimpleListType(prefix, type);
		case SchemaType.UNION:
			return processSimpleAtomicType(prefix, type.getUnionCommonBaseType());
		default:
			throw new DeclarationGenerationException(
					"Unsupported simple variety: " + type.getSimpleVariety());
		}
	}

	private Variable processSimpleAtomicType(String prefix,
			SchemaType type) throws DeclarationGenerationException {
		final String originalType = getTypeName(type);
		final String javaType = mapToJavaType(type);
		final Variable decl = new Variable(prefix,
				originalType, javaType);

		applyBuiltinRestrictions(decl, type);
		applyAtomicRestrictions(decl, type);
		return decl;
	}

	private Variable processSimpleListType(String prefix,
			SchemaType type) throws DeclarationGenerationException {
		final Variable decl = processSimpleAtomicType(prefix, type.getListItemType());
		decl.setVariableName(decl.getVariableName() + "~elems[]");
		decl.setOriginalType(decl.getOriginalType() + "[]");
		decl.setJavaType(decl.getJavaType() + "[]");
		applyListRestrictions(decl, type);
		return decl;
	}

	private BigInteger computeMaxOccurs(SchemaParticle particle) {
		BigInteger maxOccurs = particle.getMaxOccurs();
		if (maxOccurs == null) {
			if (((SchemaParticleImpl)particle).getIntMaxOccurs() != Integer.MAX_VALUE) {
				// maxOccurs is not set: use default value
				maxOccurs = BigInteger.ONE;
			}
		}
		return maxOccurs;
	}

	private BigInteger computeMinOccurs(SchemaParticle particle) {
		// If this particle is part of a choice, its contents may not appear at all.
		// For all other cases, we can be sure all its contents will appear (even though
		// the order may change), so we can use minOccurs as is.
		BigInteger minOccurs = particle.getMinOccurs();
		if (particle.getParticleType() == SchemaParticle.CHOICE) {
			minOccurs = BigInteger.ZERO;
		}
		else if (minOccurs == null) {
			minOccurs = BigInteger.ONE;
		}
		return minOccurs;
	}

	private String computeSequenceLabel(BPELProcessDefinition def,
			Element seq, ProgramPointPart part)
			throws XPathExpressionException {
		final StringBuilder sb = new StringBuilder(def.getName().getLocalPart());
		sb.append('.');

		// We need to add each path element in reverse, and then revert
		// the entire part. Prepending everything into the StringBuilder
		// would incur an O(n²) cost, instead of a O(n) one.
		StringBuilder sbPath = new StringBuilder();
		for (Node n = seq; n instanceof Element; n = n.getParentNode()) {
			final Element e = (Element) n;

			StringBuilder sbPathElem = new StringBuilder();
			sbPathElem.append('_');
			if (e.hasAttribute("name") && !"process".equals(e.getLocalName())) {
				// ActiveBPEL never uses the process name for the execution
				// logs,
				// but it uses the names of any other nested activity. The Perl
				// parser expects declarations to follow the same style.
				sbPathElem.append(e.getAttribute("name"));
			} else {
				sbPathElem.append(e.getLocalName());
				sbPathElem.append(computeSiblingPosition(e));
			}
			sbPath.append(sbPathElem.reverse());
		}
		sb.append(sbPath.reverse());

		return sb.toString();
	}

	private int computeSiblingPosition(Element e) {
		final String originalNS   = e.getNamespaceURI() + "";
		final String originalName = e.getLocalName();

		int position = 1;
		for (Node sib = e.getPreviousSibling(); sib != null; sib = sib.getPreviousSibling()) {
			if (!(sib instanceof Element)) {
				continue;
			}

			final Element sibElem = (Element)sib;
			final String sibNS    = sibElem.getNamespaceURI() + "";
			final String sibName  = sibElem.getLocalName();
			if (originalNS.equals(sibNS) && originalName.equals(sibName)) {
				position++;
			}
		}

		return position;
	}

	private String concatenate(String parent, String child) {
		if (parent.length() > 0) {
			return parent + "." + child;
		} else {
			return child;
		}
	}

	private String getTypeName(final SchemaType type) {
		if (type.getName() != null) {
			return type.getName().toString();
		}
		else if (type.getBaseType() != null) {
			return getTypeName(type.getBaseType());
		}
		else {
			return "anonymous";
		}
	}

	private String mapToJavaType(final SchemaType type) throws DeclarationGenerationException {
		switch (type.getBuiltinTypeCode()) {
		case SchemaType.BTC_UNSIGNED_LONG:
			LOGGER.warn("Type #{} has 'unsigned long' type, which does not exist in Java. This might result in some loss of precision", type);
		case SchemaType.BTC_UNSIGNED_INT:
			// Note: we need to promote XML Schema's 'unsigned int' to Java's 'long' to be able to represent all valid values
		case SchemaType.BTC_LONG:
			return "long";
		case SchemaType.BTC_UNSIGNED_SHORT:
			// Note: we need to promote XML Schema's 'unsigned short' to Java's 'int' to be able to represent all valid values
		case SchemaType.BTC_INTEGER:
		case SchemaType.BTC_INT:
		case SchemaType.BTC_NON_NEGATIVE_INTEGER:
		case SchemaType.BTC_NEGATIVE_INTEGER:
		case SchemaType.BTC_NON_POSITIVE_INTEGER:
		case SchemaType.BTC_POSITIVE_INTEGER:
			return "int";
		case SchemaType.BTC_UNSIGNED_BYTE:
			// Note: we need to promote XML Schema's 'unsigned byte' to Java's 'short' to be able to represent all values
		case SchemaType.BTC_SHORT:
			return "short";
		case SchemaType.BTC_BYTE:
			return "byte";
		case SchemaType.BTC_BOOLEAN:
			return "boolean";
		case SchemaType.BTC_DECIMAL:
		case SchemaType.BTC_DOUBLE:
			return "double";
		case SchemaType.BTC_FLOAT:
			return "float";
		case SchemaType.BTC_NOT_BUILTIN:
			return mapToJavaType(type.getBaseType());
		default:
			return "java.lang.String";
		}
	}

	private void applyBuiltinRestrictions(Variable decl, SchemaType type) {
		if (type.getBaseType() != null) {
			applyBuiltinRestrictions(decl, type.getBaseType());
		}

		switch (type.getBuiltinTypeCode()) {
		case SchemaType.BTC_BOOLEAN:
			decl.setBounds("0", "1");
			decl.setValues(new String[]{"0", "1"});
			break;
		case SchemaType.BTC_NON_POSITIVE_INTEGER:
			decl.setBounds(null, "0");
			break;
		case SchemaType.BTC_NEGATIVE_INTEGER:
			decl.setBounds(null, "-1");
			break;
		case SchemaType.BTC_LONG:
			decl.setBounds("-9223372036854775808", "9223372036854775807");
			break;
		case SchemaType.BTC_INT:
			decl.setBounds("-2147483648", "2147483647");
			break;
		case SchemaType.BTC_SHORT:
			decl.setBounds("-32768", "32767");
			break;
		case SchemaType.BTC_BYTE:
			decl.setBounds("-32768", "32767");
			break;
		case SchemaType.BTC_NON_NEGATIVE_INTEGER:
			decl.setBounds("0", null);
			break;
		case SchemaType.BTC_UNSIGNED_LONG:
			decl.setBounds("0", null);
			LOGGER.warn("Original schema type of {} is an unsigned long, but " +
					"the maximum value won't fit in the Integer Daikon uses: " +
					"maxvalue was not set", decl.getVariableName());
			break;
		case SchemaType.BTC_UNSIGNED_INT:
			decl.setBounds("0", null);
			LOGGER.warn("Original schema type of {} is an unsigned int, but " +
					"the maximum value won't fit in the Integer Daikon uses: " +
					"maxvalue was not set", decl.getVariableName());
			break;
		case SchemaType.BTC_UNSIGNED_SHORT:
			decl.setBounds("0", "65535");
			break;
		case SchemaType.BTC_UNSIGNED_BYTE:
			decl.setBounds("0", "255");
			break;
		case SchemaType.BTC_POSITIVE_INTEGER:
			decl.setBounds("1", null);
			break;
		}
	}

	private void applyContainerRestrictions(SchemaField container,
			final List<Variable> decls, final int startingPosition,
			final BigInteger minOccurs, final BigInteger maxOccurs)
	{
		// Add "[]" to the types of the generated values, min/max occurs and fixed values
		for (int i = startingPosition; i < decls.size(); ++i) {
			final Variable decl = decls.get(i);
			decl.setJavaType(decl.getJavaType() + "[]");
			decl.setOriginalType(decl.getOriginalType() + "[]");
			decl.addMinLengthComponent(minOccurs);
			decl.addMaxLengthComponent(maxOccurs);
			if (container.isFixed()) {
				decl.setFixedValue(container.getDefaultText());
			}
		}
	}

	private void applyListRestrictions(Variable decl, SchemaType type) {
		TypeTypedef catalogTypedef = TypeTypedef.Factory.newInstance();
		VariableTypes.setFacetRestrictions(type, catalogTypedef);

		BigInteger minLength = BigInteger.ZERO;
		BigInteger maxLength = null;
		if (catalogTypedef.isSetMin()) {
			minLength = new BigInteger(catalogTypedef.getMin());
		}
		if (catalogTypedef.isSetMax()) {
			maxLength = new BigInteger(catalogTypedef.getMax());
		}

		decl.addMinLengthComponent(minLength);
		decl.addMaxLengthComponent(maxLength);
	}

	private void applyAtomicRestrictions(final Variable decl, final SchemaType type) {
		final Pair<TypeGA.Enum, TypeTypedef> info = VariableTypes.primitiveType2CatalogType(type);
		final TypeTypedef catalogTypedef = info.getRight();
		if (catalogTypedef == null) {
			return;
		}

		if (catalogTypedef.isSetMin()) {
			decl.setMinimumValue(catalogTypedef.getMin());
		}
		if (catalogTypedef.isSetMax()) {
			try {
				BigInteger bi = new BigInteger(catalogTypedef.getMax());
				if (bi.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) <= 0) {
					// Can fit into Daikon's Integer values: use it as maximum value
					decl.setMaximumValue(catalogTypedef.getMax());
				}
			} catch (NumberFormatException ex) {
				// It's not a number, so we don't have to worry about Daikon using an Integer
				decl.setMaximumValue(catalogTypedef.getMax());
			}
		}

		// We can't use ServiceAnalyzer for the enumerated values, as the way it models
		// boolean values conflicts with the way we need to handle enumerations.
		XmlAnySimpleType[] enumValues = type.getEnumerationValues();
		if (enumValues != null) {
			String[] values = new String[enumValues.length];
			for (int i = 0; i < enumValues.length; ++i) {
				values[i] = enumValues[i].getStringValue();
			}
			decl.setValues(values);
		}
	}
}
