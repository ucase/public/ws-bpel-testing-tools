package es.uca.webservices.bpel.daikon.decls;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * Java bean for a Daikon variable generation inside a program point. The
 * variable can have some optional XML Schema restrictions that should be
 * suppressed in our patched version of Daikon.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */
public class Variable {

	private String name, originalType, javaType;
	private String minValue, maxValue, fixedValue;
	private List<BigInteger> minLengthComponents = new LinkedList<BigInteger>();
	private List<BigInteger> maxLengthComponents = new LinkedList<BigInteger>();
	private String[] values;

	/**
	 * Creates a new declaration.
	 * 
	 * @param name
	 *            Name of the variable.
	 * @param originalType
	 *            Type of the variable in the original type system (e.g. XML
	 *            Schema), without the extra restrictions.
	 * @param javaType
	 *            Java type to which the original type has been mapped.
	 */
	public Variable(String name, String originalType, String javaType) {
		this.name = name;
		this.originalType = originalType;
		this.javaType = javaType;
	}

	/**
	 * Returns the name of the variable.
	 */
	public String getVariableName() {
		return name;
	}

	/**
	 * Changes the name of the variable.
	 */
	public void setVariableName(String name) {
		this.name = name;
	}

	/**
	 * Returns the type of the variable in the original type system.
	 */
	public String getOriginalType() {
		return originalType;
	}

	/**
	 * Changes the type of the variable in the original type system.
	 */
	public void setOriginalType(String type) {
		originalType = type;
	}

	/**
	 * Returns the type of the variable mapped to the Java type system.
	 */
	public String getJavaType() {
		return javaType;
	}

	/**
	 * Changes the type of the variable mapped to the Java type system.
	 */
	public void setJavaType(String type) {
		this.javaType = type;
	}

	/**
	 * Returns the Daikon comparability index of the variable.
	 * 
	 * This method always returns 0 for the element type and the indices, as the
	 * comparability indices are computed in the Takuan Perl scripts.
	 */
	public String getComparability() {
		final int dimensions = countSubstring("[]", javaType);
		return "0" + repeat("[0]", dimensions);
	}

	/**
	 * Returns the minimum value for this declaration.
	 * 
	 * @return Minimum value for this declaration, or <code>null</code> if not
	 *         set.
	 */
	public String getMinimumValue() {
		return minValue;
	}

	/**
	 * Changes the minimum value for this declaration.
	 * 
	 * @param min
	 *            New minimum value for this declaration, or <code>null</code>
	 *            if it is to be unset.
	 */
	public void setMinimumValue(String min) {
		minValue = min;
	}

	/**
	 * Returns the maximum value for this declaration.
	 * 
	 * @return Maximum value for this declaration, or <code>null</code> if not
	 *         set.
	 */
	public String getMaximumValue() {
		return maxValue;
	}

	/**
	 * Changes the maximum value for this declaration.
	 * 
	 * @param max
	 *            New maximum value for this declaration, or <code>null</code>
	 *            if it is to be unset.
	 * 
	 */
	public void setMaximumValue(String max) {
		maxValue = max;
	}

	/**
	 * Returns the fixed value for this declaration.
	 */
	public String getFixedValue() {
		return fixedValue;
	}

	/**
	 * Changes the fixed value for this declaration.
	 * 
	 * @param value
	 *            New fixed value, or <code>null</code> if it is to be unset.
	 */
	public void setFixedValue(String value) {
		minValue = maxValue = fixedValue = value;
	}

	/**
	 * Convenience method for setting the minimum and/or maximum values.
	 * 
	 * @param newmin
	 *            New minimum value to be used, or <code>null</code> if it
	 *            should not be set.
	 * @param newmax
	 *            New maximum value to be used, or <code>null</code> if it
	 *            should not be set.
	 */
	public void setBounds(String min, String max) {
		if (min != null) {
			minValue = min;
		}
		if (max != null) {
			maxValue = max;
		}
	}

	public void setValues(String[] values) {
		this.values = values.clone();
	}

	public String getValuesAsString() {
		if (values == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder("[");
		for (String value : values) {
			if (sb.length() > 1) {
				sb.append(' ');
			}
			sb.append('"');
			sb.append(value);
			sb.append('"');
		}
		sb.append(']');
		return sb.toString();
	}

	/**
	 * Adds another component at the beginning of the minimum length list.
	 */
	public void addMinLengthComponent(BigInteger minOccurs) {
		minLengthComponents.add(0, minOccurs);
	}

	/**
	 * Multiplies the first length component of this declaration by
	 * <code>factor</code>, replacing it.
	 */
	public void multiplyFirstMinComponentBy(BigInteger factor) {
		final BigInteger firstComponent = minLengthComponents.remove(0);
		minLengthComponents.add(0, firstComponent.multiply(factor));
	}

	/**
	 * Returns the list of minimum length components. These are the values of
	 * the minOccurs attribute for all the elements in the path of the variable,
	 * from the outermost to the innermost one. The list might be empty.
	 */
	public List<BigInteger> getMinLengthComponents() {
		return minLengthComponents;
	}

	/**
	 * Adds another component at the beginning of the maximum length list.
	 * 
	 * @param maxOccurs
	 *            Value of the maxOccurs or maxLength facets: not null if equal
	 *            to a certain number, or <code>null</code> if unbounded.
	 */
	public void addMaxLengthComponent(BigInteger maxOccurs) {
		maxLengthComponents.add(0, maxOccurs);
	}

	/**
	 * Multiplies the first length component of this declaration by
	 * <code>factor</code>, replacing it. Takes into account unbounded values.
	 *
	 * @param factor
	 *            Multiplication factor. Unbounded values are represented by
	 *            <code>null</code>.
	 */
	public void multiplyFirstMaxComponentBy(BigInteger factor) {
		final BigInteger firstComponent = maxLengthComponents.remove(0);
		maxLengthComponents.add(0,
			firstComponent == null || factor == null
			? null
			: factor.multiply(firstComponent));
	}

	/**
	 * Returns the list of maximum length components. These are the values of
	 * the maxOccurs attribute for all the elements in the path of the variable,
	 * from the outermost to the innermost one. The list might be empty.
	 */
	public List<BigInteger> getMaxLengthComponents() {
		return maxLengthComponents;
	}

	/**
	 * Returns the elements of {@link #getMinLengthComponents()} as a string, separated by
	 * commas. If the list is empty, returns <code>null</code> instead.
	 */
	public String getMinLengthAsString() {
		return joinLengthComponents(minLengthComponents);
	}

	/**
	 * Returns the elements of {@link #getMaxLengthComponents()} as a string, separated by
	 * commas. If the list is empty, returns <code>null</code> instead.
	 */
	public String getMaxLengthAsString() {
		return joinLengthComponents(maxLengthComponents);
	}

	@Override
	public String toString() {
		return String.format("DaikonDeclaration [name=%s, originalType=%s, javaType=%s, minOccurs=%s, maxOccurs=%s]",
				name, originalType, javaType, getMinLengthComponents(), getMaxLengthComponents());
	}

	private int countSubstring(String substring, String text) {
		int count = 0;
		for (
			int index = text.indexOf(substring, 0);
			index != -1;
			++count, index = text.indexOf(substring, index + 1)
		);
		return count;
	}

	private String joinLengthComponents(List<?> elements) {
		final char separator = ',';
		StringBuilder sb = new StringBuilder();
		for (Object o : elements) {
			if (sb.length() > 0) {
				sb.append(separator);
			}
			// Null values represent be "unbounded" lengths
			sb.append(o != null ? o.toString() : '*');
		}
		return sb.toString();
	}

	private String repeat(String s, int n) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < n; ++i) {
			sb.append(s);
		}
		return sb.toString();
	}
}
