package es.uca.webservices.bpel.daikon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.bpel.daikon.decls.Variable;

/**
 * Tests for the minLength/maxLength multiplications used in {@link Variable}.
 *
 * @author Antonio García-Domínguez
 */
public class LengthMultiplicationsTest {

	private Variable decl;

	@Before
	public void setup() {
		decl = new Variable("a", "b", "c");
	}

	@Test
	public void twoScalars() {
		decl.addMinLengthComponent(bi(1));
		decl.addMaxLengthComponent(bi(3));
		decl.multiplyFirstMinComponentBy(bi(2));
		decl.multiplyFirstMaxComponentBy(bi(3));

		assertEquals(bi(1*2), decl.getMinLengthComponents().get(0));
		assertEquals(bi(3*3), decl.getMaxLengthComponents().get(0));
	}

	@Test
	public void minScalarByZero() {
		decl.addMinLengthComponent(BigInteger.ONE);
		decl.multiplyFirstMinComponentBy(BigInteger.ZERO);
		assertEquals(BigInteger.ZERO, decl.getMinLengthComponents().get(0));
	}

	@Test
	public void minZeroByScalar() {
		decl.addMinLengthComponent(BigInteger.ZERO);
		decl.multiplyFirstMinComponentBy(bi(2));
		assertEquals(BigInteger.ZERO, decl.getMinLengthComponents().get(0));
	}

	@Test
	public void maxScalarByUnbounded() {
		decl.addMaxLengthComponent(bi(2));
		decl.multiplyFirstMaxComponentBy(null);
		assertNull(decl.getMaxLengthComponents().get(0));
	}

	@Test
	public void maxUnboundedByScalar() {
		decl.addMaxLengthComponent(null);
		decl.multiplyFirstMaxComponentBy(bi(2));
		assertNull(decl.getMaxLengthComponents().get(0));
	}

	@Test
	public void maxUnboundedByUnbounded() {
		decl.addMaxLengthComponent(null);
		decl.multiplyFirstMaxComponentBy(null);
		assertNull(decl.getMaxLengthComponents().get(0));
	}

	private BigInteger bi(final int val) {
		return BigInteger.valueOf(val);
	}
}
