package es.uca.webservices.bpel.daikon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;

import es.uca.webservices.bpel.BPELConstants;
import es.uca.webservices.bpel.daikon.decls.Variable;

/**
 * Check that the new Java-based .decls generator works with XML Schema simple types.
 *
 * @author Antonio García-Domínguez
 */
public class SimpleTypesTest extends AbstractDeclarationGeneratorTestCase {

	private static final String SIMPLETYPES_NS_URI = "http://www.example.org/simpleTypes";

	public SimpleTypesTest() {
		super("/decls/simpleTypes.xsd", SIMPLETYPES_NS_URI);
	}

	@Test
	public void xsdInt() throws Exception {
		final Variable decl = generateSingleDeclarationForElement("regularInteger");
		assertEquals("regularInteger[]", decl.getVariableName());
		assertEquals("{" + XSD_NS_URI + "}int[]", decl.getOriginalType());
		assertEquals("int[]", decl.getJavaType());
		assertEquals("0[0]", decl.getComparability());
		assertEquals("-2147483648", decl.getMinimumValue());
		assertEquals("2147483647", decl.getMaximumValue());
		assertEquals("1", decl.getMinLengthAsString());
		assertEquals("1", decl.getMaxLengthAsString());
	}

	@Test
	public void anySimpleTypeIsAString() throws Exception {
		final List<Variable> decls = generateDeclarationsForElement("anythingGoes");
		assertEquals(1, decls.size());
		final Variable decl = decls.get(0);
		assertIsAnySimpleType(decl);
	}

	public void anyTypeIgnored() throws Exception {
		final List<Variable> decls = generateDeclarationsForElement("forgotType");
		assertEquals(0, decls.size());
	}

	@Test
	public void unsignedByteWithMinAndMax() throws Exception {
		final Variable decl =  generateSingleDeclarationForElement("diceRoll");
		assertEquals("diceRoll[]", decl.getVariableName());
		assertEquals("{" + SIMPLETYPES_NS_URI + "}diceRollType[]", decl.getOriginalType());
		assertEquals("short[]", decl.getJavaType());
		assertEquals("0", decl.getMinimumValue()); // built-in restriction (it's an unsigned byte)
		assertEquals("5", decl.getMaximumValue()); // custom restriction from the XSD
	}

	@Test
	public void stringEnumeration() throws Exception {
		final Variable decl = generateSingleDeclarationForElement("color");
		assertEquals("color[]", decl.getVariableName());
		assertEquals("{" + SIMPLETYPES_NS_URI + "}colorType[]", decl.getOriginalType());
		assertEquals("java.lang.String[]", decl.getJavaType());
		assertEquals("[\"Red\" \"Green\" \"Blue\"]", decl.getValuesAsString());
	}

	@Test
	public void unboundedList() throws Exception {
		final Variable decl = generateSingleDeclarationForElement("listOfBigNumbers");
		assertEquals("listOfBigNumbers[]~elems[]", decl.getVariableName());
		assertEquals("{" + XSD_NS_URI + "}unsignedLong[][]", decl.getOriginalType());
		assertEquals("long[][]", decl.getJavaType());
		assertEquals("0", decl.getMinimumValue());

		// We need to remove the maximum value in this case, as it's too big for
		// the Integer objects that Daikon uses
		assertNull(decl.getMaximumValue());

		assertEquals("1,0", decl.getMinLengthAsString());
		assertEquals("1,*", decl.getMaxLengthAsString());
	}

	@Test
	public void nonEmptyList() throws Exception {
		final Variable decl = generateSingleDeclarationForElement("listShortsNonEmpty");
		assertEquals("1,1", decl.getMinLengthAsString());
		assertEquals("1,*", decl.getMaxLengthAsString());
	}

	@Test
	public void smallList() throws Exception {
		final Variable decl = generateSingleDeclarationForElement("listShortsMini");
		assertEquals("1,0",  decl.getMinLengthAsString());
		assertEquals("1,10", decl.getMaxLengthAsString());
	}

	@Test
	public void nonEmptySmallList() throws Exception {
		final Variable decl = generateSingleDeclarationForElement("listShortsMiniNonEmpty");
		assertEquals("1,1", decl.getMinLengthAsString());
		assertEquals("1,10", decl.getMaxLengthAsString());
	}

	@Test
	public void booleanElement() throws Exception {
		final Variable decl = generateSingleDeclarationForElement("isTrue");
		assertEquals("boolean[]", decl.getJavaType());
		assertEquals("0", decl.getMinimumValue());
		assertEquals("1", decl.getMaximumValue());
		assertEquals("[\"0\" \"1\"]", decl.getValuesAsString());
	}

	@Test
	public void fixedValue() throws Exception {
		final Variable decl = generateSingleDeclarationForElement("answerToLifeTheUniverseAndEverything");
		assertEquals("42", decl.getMinimumValue());
		assertEquals("42", decl.getMaximumValue());
		assertEquals("42", decl.getFixedValue());
	}

	@Test
	public void union() throws Exception {
		final List<Variable> decls = generateDeclarationsForElement("stringOrNumber");
		assertEquals(1, decls.size());

		final Variable decl = decls.get(0);
		assertEquals("stringOrNumber[]", decl.getVariableName());
		assertIsAnySimpleType(decl);
	}

	@Test
	public void date() throws Exception {
		final List<Variable> decls = generateDeclarationsForElement("birthdate");
		assertEquals(1, decls.size());
		final Variable decl = decls.get(0);
		assertEquals("java.lang.String[]", decl.getJavaType());
	}

	private void assertIsAnySimpleType(final Variable decl) {
		assertEquals("{" + BPELConstants.XSD_NAMESPACE + "}anySimpleType[]", decl.getOriginalType());
		assertEquals("java.lang.String[]", decl.getJavaType());
	}
}
