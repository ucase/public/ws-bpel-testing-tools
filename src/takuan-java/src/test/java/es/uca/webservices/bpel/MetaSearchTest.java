package es.uca.webservices.bpel;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.Test;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.daikon.instrument.BPELInstrumentationException;

/**
 * Tests if the MetaSearch composition is correctly instrumented. After
 * starting work on making the TravelReservationService work, this one
 * stopped working.
 *
 * @author Antonio García Domínguez
 */
public class MetaSearchTest extends CompositionTestBase {

	public MetaSearchTest() throws Exception {
		super("metaSearch");
	}

	@Test
	public void programPointsDepth1() throws Exception {
		assertProgramPointsAtDepthThreshold(1, 2);
	}

	@Test
	public void programPointsDepth2() throws Exception {
		assertProgramPointsAtDepthThreshold(2, 6);
	}

	@Test
	public void programPointsDepth3() throws Exception {
		assertProgramPointsAtDepthThreshold(3, 11);
	}

	@Test
	public void programPointsDepth4() throws Exception {
		assertProgramPointsAtDepthThreshold(4, 16);
	}

	@Test
	public void programPointsDepth5() throws Exception {
		assertProgramPointsAtDepthThreshold(5, 23);
	}

	@Test
	public void programPointsDepth6() throws Exception {
		assertProgramPointsAtDepthThreshold(6, 26);
	}

	@Test
	public void programPointsDepth7() throws Exception {
		assertProgramPointsAtDepthThreshold(7, 29);
	}

	@Test
	public void programPointsDepth8() throws Exception {
		assertProgramPointsAtDepthThreshold(8, 32);
	}

	@Test
	public void programPointsDepth9() throws Exception {
		assertProgramPointsAtDepthThreshold(9, 32);
	}

	@Test
	public void programPointsDepth0() throws Exception {
		assertProgramPointsAtDepthThreshold(0, 32);
	}

	@Test
	public void programPointsUnset() throws Exception {
		assertEquals(32, bpelProcess.getInstrumentableElements("*").size());
		assertEquals(32, getInstrumentedBPEL().getInstrumentableSequences().size());
	}

	private void assertProgramPointsAtDepthThreshold(final int depth,
			final int nProgramPoints) throws XPathExpressionException,
			BPELInstrumentationException, ParserConfigurationException,
			SAXException, IOException, InvalidProcessException {
		bpelProcess.setInstrumentSequencesDepthThreshold(depth);
		final List<Element> instrumentableElements = bpelProcess.getInstrumentableElements("*");
		assertEquals(nProgramPoints, instrumentableElements.size());

		final BPELProcessDefinition instrumentedBPEL = getInstrumentedBPEL();
		assertEquals(nProgramPoints, instrumentedBPEL.getInstrumentableSequences().size());
	}
}
