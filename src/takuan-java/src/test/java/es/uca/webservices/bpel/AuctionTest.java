package es.uca.webservices.bpel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

import javax.xml.namespace.QName;

import org.junit.Test;
import org.w3c.dom.Element;

import es.uca.webservices.bpel.daikon.decls.DeclarationsFormatter;
import es.uca.webservices.bpel.daikon.decls.DeclarationsGenerator;

/**
 * Tests for the sample Auction composition. This composition uses endpoint
 * assignments, which confused the instrumenter.
 * 
 * @author Antonio García-Domínguez
 */
public class AuctionTest extends CompositionTestBase {

	private static final String AUCTION_WSDL_TARGET_NS = "http://example.com/auction/wsdl/auctionService/";

	public AuctionTest() throws Exception {
		super("auction");
	}

	@Test
	public void correctNumberOfProgramPoints() throws Exception {
		assertEquals(3, getInstrumentedBPEL().getInstrumentableSequences().size());
	}

	@Test
	public void variablesInFirstProgramPoint() throws Exception {
		final BPELProcessDefinition def = getInstrumentedBPEL();
		final List<Element> instrumentedPP = def.getInstrumentableSequences();
		final List<BPELVariable> variables = def.getProgramPointVariables(instrumentedPP.get(0));
		assertEquals(10, variables.size());

		final BPELVariable firstVariable = variables.get(0);
		assertEquals("sellerData", firstVariable.getName());
		assertEquals(
				new QName(AUCTION_WSDL_TARGET_NS, "sellerData"),
				firstVariable.getMessageType());
	}

	@Test
	public void doNotInstrumentProgramVariablesByDefaultIsHonored() throws Exception {
		final BPELProcessDefinition def = getInstrumentedBPEL();
		def.setInstrumentVariablesByDefault(false);
		final List<Element> instrumentedPP = def.getInstrumentableSequences();
		final List<BPELVariable> variables = def.getProgramPointVariables(instrumentedPP.get(0));
		assertEquals(1, variables.size());
	}

	@Test
	public void programPointsAreWellSeparated() throws Exception {
		final BPELProcessDefinition def = getInstrumentedBPEL();
		final File fDecls = File.createTempFile("shipping", ".decls");
		new DeclarationsGenerator().generateAndPrint(def,
			new DeclarationsFormatter(), new FileWriter(fDecls));

		final BufferedReader reader = new BufferedReader(new FileReader(fDecls));
		boolean previousIsBlank = true; // DECLARE on the first line of the file is valid
		String line;
		while ((line = reader.readLine()) != null) {
			if (line.contains("DECLARE")) {
				assertEquals("DECLARE should always be on its own in a single line", "DECLARE", line);
				assertTrue("Declarations should be at the beginning of the file or preceded by a blank line", previousIsBlank);
			}
			previousIsBlank = line.isEmpty();
		}
	}

}
