package es.uca.webservices.bpel.daikon;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;

import org.junit.Test;

import es.uca.webservices.bpel.daikon.decls.DeclarationsFormatter;
import es.uca.webservices.bpel.daikon.decls.ProgramPoint;
import es.uca.webservices.bpel.daikon.decls.ProgramPointPart;
import es.uca.webservices.bpel.daikon.decls.Variable;

/**
 * Tests for the textual formatting of Daikon declarations.
 *
 * @author Antonio García-Domínguez
 */
public class FormattingTest {

	@Test
	public void singleSimple() throws IOException {
		assertFormattedAs(
			new Variable("a", "xsd:int", "int"),
			"a\nxsd:int\nint\n0");
	}

	@Test
	public void singleMin() throws IOException {
		final Variable decl = new Variable("a", "xsd:int", "int");
		decl.setMinimumValue(1 + "");
		assertFormattedAs(
				decl,
				"a\n" +
				"xsd:int # minvalue=1\n" +
				"int\n" +
				"0"
		);
	}

	@Test
	public void singleMax() throws IOException {
		final Variable decl = new Variable("a", "xsd:int", "int");
		decl.setMaximumValue(2 + "");
		assertFormattedAs(
				decl,
				"a\n" +
				"xsd:int # maxvalue=2\n" +
				"int\n" +
				"0"
		);
	}

	@Test
	public void singleMaxMin() throws IOException {
		final Variable decl = new Variable("a", "xsd:int", "int");
		decl.setMinimumValue(1 + "");
		decl.setMaximumValue(2 + "");
		assertFormattedAs(
				decl,
				"a\n" +
				"xsd:int # minvalue=1, maxvalue=2\n" +
				"int\n" +
				"0"
		);
	}

	@Test
	public void singleValues() throws IOException {
		final Variable decl = new Variable("a", "xsd:int", "int");
		decl.setValues(new String[]{"0", "1"});
		assertFormattedAs(
				decl,
				"a\n" +
				"xsd:int # validvalues=[\"0\" \"1\"]\n" +
				"int\n" +
				"0"
		);
	}

	@Test
	public void singleFixed() throws IOException {
		final Variable decl = new Variable("a", "xsd:int", "int");
		decl.setFixedValue("42");
		assertFormattedAs(
				decl,
				"a\n" +
				"xsd:int # minvalue=42, maxvalue=42\n" +
				"int\n" +
				"0"
		);
	}

	@Test
	public void singleOneLengthComponent() throws IOException {
		final Variable decl = new Variable("a", "xsd:int", "int");
		decl.addMinLengthComponent(BigInteger.ZERO);
		decl.addMaxLengthComponent(BigInteger.ONE);
		assertFormattedAs(
				decl,
				"a\n" +
				"xsd:int # minlength=[0], maxlength=[1]\n" +
				"int\n" +
				"0"
		);
	}

	@Test
	public void singleTwoLengthComponents() throws IOException {
		final Variable decl = new Variable("a", "xsd:int", "int");
		decl.addMinLengthComponent(BigInteger.ONE);
		decl.addMaxLengthComponent(BigInteger.ONE);

		decl.addMinLengthComponent(BigInteger.ZERO);
		decl.addMaxLengthComponent(null);
		assertFormattedAs(
				decl,
				"a\n" +
				"xsd:int # minlength=[0 1], maxlength=[* 1]\n" +
				"int\n" +
				"0"
		);
	}

	@Test
	public void emptyProgramPoint() throws IOException {
		final ProgramPoint pp = new ProgramPoint("this.ismyprogram.point", ProgramPointPart.ENTER);
		assertFormattedAs(pp, "DECLARE\n" + pp.getName() + "\n");
	}

	@Test
	public void oneVariableProgramPoint() throws IOException {
		final ProgramPoint pp = new ProgramPoint("this.ismyprogram.point", ProgramPointPart.ENTER);
		pp.addDeclaration(new Variable("a", "xsd:int", "int"));
		assertFormattedAs(
				pp,
				"DECLARE\n" + pp.getName() + "\n" +
				"a\nxsd:int\nint\n0"
				);
	}

	@Test
	public void twoVariablesProgramPoint() throws IOException {
		final ProgramPoint pp = new ProgramPoint("this.ismyprogram.point", ProgramPointPart.ENTER);
		pp.addDeclaration(new Variable("a", "xsd:int", "int"));
		pp.addDeclaration(new Variable("b", "xsd:double", "double"));
		assertFormattedAs(
				pp,
				"DECLARE\n" + pp.getName() + "\n" +
				"a\nxsd:int\nint\n0\n" +
				"b\nxsd:double\ndouble\n0"
				);
	}

	private void assertFormattedAs(final ProgramPoint pp,
			final String expected) throws IOException {
		final DeclarationsFormatter fmt = new DeclarationsFormatter();
		final StringWriter sw = new StringWriter();
		fmt.format(pp, sw);
		assertEquals(expected, sw.toString());
	}

	private void assertFormattedAs(
			final Variable decl, final String expected) throws IOException {
		final DeclarationsFormatter fmt = new DeclarationsFormatter();
		final StringWriter sw = new StringWriter();
		fmt.format(decl, sw);
		assertEquals(expected, sw.toString());
	}
}
