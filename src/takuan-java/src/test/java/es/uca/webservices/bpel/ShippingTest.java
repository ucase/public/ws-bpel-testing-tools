package es.uca.webservices.bpel;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.w3c.dom.Element;

import es.uca.webservices.bpel.daikon.decls.DeclarationsGenerator;
import es.uca.webservices.bpel.daikon.decls.ProgramPoint;


public class ShippingTest extends CompositionTestBase {

	public ShippingTest() throws Exception {
		super("shipping");
	}

	@Test
	public void dummyVariablesForProperties() throws Exception {
		BPELProcessDefinition def = getInstrumentedBPEL();
		final String requestItemsCountType
			= def.evaluateExpression("//bpel:variable[@name='dummy_shipRequest_itemsCount']/@type");
		assertEquals("t:itemCountType", requestItemsCountType);

		final String noticeItemsCountType
			= def.evaluateExpression("//bpel:variable[@name='dummy_shipNotice_itemsCount']/@type");
		assertEquals("t:itemCountType", noticeItemsCountType);
	}

	@Test
	public void detectProgramPoints() throws Exception {
		assertEquals(4, getInstrumentedBPEL().getInstrumentableSequences().size());
	}

	@Test
	public void doNotInstrumentProgramPointsByDefaultIsHonored() throws Exception {
		bpelProcess.setInstrumentSequencesByDefault(false);
		assertEquals(0, bpelProcess.getInstrumentableSequences().size());
	}

	@Test
	public void propertiesBecomeVirtualVariables() throws Exception {
		final BPELProcessDefinition def = getInstrumentedBPEL();
		final List<Element> insPP = def.getInstrumentableSequences();
		final List<BPELVariable> vars = def.getProgramPointVariables(insPP.get(0));
		assertEquals(9, vars.size());

		assertEquals("shipRequest~itemsCount", vars.get(3).getName());
	}

	@Test
	public void generateDeclsForAllPoints() throws Exception {
		final BPELProcessDefinition def = getInstrumentedBPEL();
		final List<ProgramPoint> pp = new DeclarationsGenerator().generate(def);

		assertEquals(8, pp.size());
		assertEquals("shippingService._process1_sequence1:::ENTER", pp.get(0).getName());
		assertEquals("shippingService._process1_sequence1:::EXIT1", pp.get(1).getName());
		assertEquals(14, pp.get(0).getDeclarations().size());
		assertEquals(14, pp.get(1).getDeclarations().size());
		assertEquals("shipRequest.shipOrder.shipOrder[].ShipOrderRequestHeader[].shipOrderID[]",
				pp.get(0).getDeclarations().get(0).getVariableName());

		assertEquals("shippingService._process1_sequence1_CheckIfComplete_ShipComplete:::ENTER", pp.get(2).getName());
		assertEquals("shippingService._process1_sequence1_CheckIfComplete_ShipComplete:::EXIT1", pp.get(3).getName());
		assertEquals("shippingService._process1_sequence1_CheckIfComplete_else1_sequence1:::ENTER", pp.get(4).getName());
		assertEquals("shippingService._process1_sequence1_CheckIfComplete_else1_sequence1:::EXIT1", pp.get(5).getName());
	}

}
