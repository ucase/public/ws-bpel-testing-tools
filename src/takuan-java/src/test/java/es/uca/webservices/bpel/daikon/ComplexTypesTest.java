package es.uca.webservices.bpel.daikon;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import es.uca.webservices.bpel.daikon.decls.Variable;

/**
 * Checks that the new Java-based .decls generator works with complex types.
 *
 * @author Antonio García-Domínguez
 */
public class ComplexTypesTest extends AbstractDeclarationGeneratorTestCase {

	private static final String TARGET_NS_URI = "http://www.example.org/complexTypes";

	public ComplexTypesTest() {
		super("/decls/complexTypes.xsd", TARGET_NS_URI);
	}

	@Test
	public void basicSequence() throws Exception {
		final List<Variable> decls = generateDeclarationsForElement("location");
		assertEquals(2, decls.size());
		assertIsASimpleTypedElement(decls.get(0), "location[].longitude[]", "double[][]", "double[][]", "1,1");
		assertIsASimpleTypedElement(decls.get(1), "location[].latitude[]", "double[][]", "double[][]", "1,1");
	}

	@Test
	public void simpleContentWithAttribute() throws Exception {
		final List<Variable> decls = generateDeclarationsForElement("contributor");
		assertEquals(2, decls.size());

		final Variable simpleContentDecl = decls.get(0);
		assertEquals("contributor[]", simpleContentDecl.getVariableName());
		assertEquals("java.lang.String[]", simpleContentDecl.getJavaType());
		assertEquals("1", simpleContentDecl.getMinLengthAsString());
		assertEquals("1", simpleContentDecl.getMaxLengthAsString());

		final Variable attributeDecl = decls.get(1);
		assertEquals("contributor[].@type[]", attributeDecl.getVariableName());
		assertEquals("java.lang.String[][]", attributeDecl.getJavaType());
		assertEquals("[\"editor\" \"author\" \"translator\"]", attributeDecl.getValuesAsString());
		assertEquals("1,0", attributeDecl.getMinLengthAsString());
		assertEquals("1,1", attributeDecl.getMaxLengthAsString());
	}

	@Test
	public void all() throws Exception {
		final List<Variable> decls = generateDeclarationsForElement("unorderedLocation");
		assertEquals(2, decls.size());
		assertIsASimpleTypedElement(decls.get(0), "unorderedLocation[].longitude[]", "float[][]", "float[][]", "1,1");
		assertIsASimpleTypedElement(decls.get(1), "unorderedLocation[].latitude[]", "float[][]", "float[][]", "1,1");
	}

	@Test
	public void choiceWithUnboundedRepetitionsAndRequiredAttribute() throws Exception {
		final List<Variable> decls = generateDeclarationsForElement("color");
		assertEquals(5, decls.size());

		// The '0' in the middle represents the choice: if the particle
		// containing the element isn't chosen, it will not appear at all.
		// This is different from all other cases, in which the minOccurs
		// attribute is used as-is. This is important in order to know the
		// minimum total number of values that the list can contain.
		assertIsASimpleTypedElement(decls.get(0), "color[].red[]", "unsignedByte[][]", "short[][]", "1,0", "1,1");
		assertIsASimpleTypedElement(decls.get(1), "color[].green[]", "unsignedByte[][]", "short[][]", "1,0", "1,1");
		assertIsASimpleTypedElement(decls.get(2), "color[].blue[]", "unsignedByte[][]", "short[][]", "1,0", "1,1");
		assertIsASimpleTypedElement(decls.get(3), "color[].component[]", "unsignedByte[][]", "short[][]", "1,0", "1,*");
		assertIsASimpleTypedElement(decls.get(4), "color[].component[].@type[]", "string[][][]", "java.lang.String[][][]", "1,0,1", "1,*,1");
	}

	private void assertIsASimpleTypedElement(
			final Variable decl, final String name,
			final String xsdType, final String javaType,
			final String length)
	{
		assertIsASimpleTypedElement(decl, name, xsdType, javaType, length, length);
	}

	private void assertIsASimpleTypedElement(
			final Variable decl, final String name,
			final String xsdType, final String javaType,
			final String minLength, final String maxLength)
	{
		assertEquals(name, decl.getVariableName());
		assertEquals("{" + XSD_NS_URI + "}" + xsdType, decl.getOriginalType());
		assertEquals(javaType, decl.getJavaType());
		assertEquals(minLength, decl.getMinLengthAsString());
		assertEquals(maxLength, decl.getMaxLengthAsString());
	}
}
