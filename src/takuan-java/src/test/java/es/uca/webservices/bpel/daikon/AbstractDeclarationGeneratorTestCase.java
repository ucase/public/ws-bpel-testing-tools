package es.uca.webservices.bpel.daikon;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.SchemaGlobalElement;
import org.junit.Before;

import es.uca.webservices.bpel.daikon.decls.DeclarationGenerationException;
import es.uca.webservices.bpel.daikon.decls.DeclarationsGenerator;
import es.uca.webservices.bpel.daikon.decls.Variable;
import es.uca.webservices.bpel.xsd.XMLSchemaCatalog;
import es.uca.webservices.wsdl.analyzer.MissingSchemaComponentException;
import es.uca.webservices.wsdl.analyzer.SchemaReadingException;

/**
 * Abstract base class for all test cases about XML Schema types in the declaration generator.
 *
 * @author Antonio García-Domínguez
 */
public abstract class AbstractDeclarationGeneratorTestCase {

	protected static final String XSD_NS_URI = "http://www.w3.org/2001/XMLSchema";
	private XMLSchemaCatalog xsdCatalog;
	protected String pathToXSD, targetNamespaceURI;

	public AbstractDeclarationGeneratorTestCase(String pathToXSD, String targetNamespaceURI) {
		this.pathToXSD = pathToXSD;
		this.targetNamespaceURI = targetNamespaceURI;
	}

	@Before
	public void setUp() throws SchemaReadingException {
		xsdCatalog = new XMLSchemaCatalog();
		xsdCatalog.addXMLSchema(SimpleTypesTest.class.getResourceAsStream(pathToXSD));
	}

	protected Variable generateSingleDeclarationForElement(
			final String elementName) throws MissingSchemaComponentException,
			DeclarationGenerationException {
		final List<Variable> decls = generateDeclarationsForElement(elementName);
		assertEquals(1, decls.size());
		return decls.get(0);
	}

	protected List<Variable> generateDeclarationsForElement(
			final String elementName) throws MissingSchemaComponentException,
			DeclarationGenerationException {
		final SchemaGlobalElement type = xsdCatalog.getElement(new QName(targetNamespaceURI, elementName));
		if (type == null) {
			throw new MissingSchemaComponentException("Could not find element " + elementName);
		}

		final DeclarationsGenerator generator = new DeclarationsGenerator();
		final List<Variable> decls = generator.generate("", type);
		return decls;
	}

}