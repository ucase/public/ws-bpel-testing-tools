package es.uca.webservices.bpel.daikon;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;

import javax.wsdl.Message;
import javax.xml.namespace.QName;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.bpel.daikon.decls.DeclarationGenerationException;
import es.uca.webservices.bpel.daikon.decls.DeclarationsGenerator;
import es.uca.webservices.bpel.daikon.decls.Variable;
import es.uca.webservices.bpel.wsdl.WSDLCatalog;
import es.uca.webservices.bpel.xsd.XMLSchemaCatalog;
import es.uca.webservices.wsdl.analyzer.MissingSchemaComponentException;

/**
 * Test cases for generating Daikon declarations from WSDL message types.
 *
 * @author Antonio García-Domínguez
 */
public class MessageTypesTest {

	private static final String MSG_TYPES_NS_URI = "http://www.example.org/messageTypes/";
	private WSDLCatalog wsdlCatalog;
	private XMLSchemaCatalog xsdCatalog;
	private DeclarationsGenerator declGenerator;

	@Before
	public void setUp() throws Exception {
		// Note: we're reusing some of the test resources from bpel-packager
		declGenerator = new DeclarationsGenerator();
		final File wsdlFile = new File(
				MessageTypesTest.class.getResource(
						"/decls/messageTypes.wsdl").getPath());

		wsdlCatalog = new WSDLCatalog();
		wsdlCatalog.addWSDL(null, wsdlFile);

		xsdCatalog = new XMLSchemaCatalog();
		xsdCatalog.addWSDL(null, wsdlFile);
	}

	@Test
	public void typeMessageComplexContent() throws Exception {
		final List<Variable> decls = getDeclsForMessage("TypeMessage");
		assertEquals(2, decls.size());

		final Variable quantityDecl = decls.get(0);
		assertEquals("input.input[].quantity[]", quantityDecl.getVariableName());
		assertEquals("double[][]", quantityDecl.getJavaType());
		assertEquals("0[0][0]", quantityDecl.getComparability());
		assertEquals("1,1", quantityDecl.getMinLengthAsString());
		assertEquals("1,1", quantityDecl.getMaxLengthAsString());

		final Variable currencyDecl = decls.get(1);
		assertEquals("input.input[].currency[]", currencyDecl.getVariableName());
		assertEquals("java.lang.String[][]", currencyDecl.getJavaType());
	}

	@Test
	public void typeMessageSimpleContent() throws Exception {
		final List<Variable> decls = getDeclsForMessage("ColorMessage");
		assertEquals(3, decls.size());

		assertEquals("red", decls.get(0).getVariableName());
		assertEquals("short", decls.get(0).getJavaType());
		assertEquals("0", decls.get(0).getComparability());

		assertEquals("blue", decls.get(1).getVariableName());
		assertEquals("short", decls.get(1).getJavaType());
		assertEquals("0", decls.get(1).getComparability());

		assertEquals("green", decls.get(2).getVariableName());
		assertEquals("short", decls.get(2).getJavaType());
		assertEquals("0", decls.get(2).getComparability());
	}

	@Test
	public void elementMessage() throws Exception {
		final List<Variable> decls = getDeclsForMessage("ElementMessage");
		assertEquals(1, decls.size());
		final Variable colorDecl = decls.get(0);

		assertEquals("input.color[]", colorDecl.getVariableName());
		assertEquals("java.lang.String[]", colorDecl.getJavaType());
		assertEquals("0[0]", colorDecl.getComparability());
		assertEquals("[\"Red\" \"Green\" \"Blue\"]", colorDecl.getValuesAsString());
		assertEquals("1", colorDecl.getMinLengthAsString());
		assertEquals("1", colorDecl.getMaxLengthAsString());
	}

	private List<Variable> getDeclsForMessage(final String name)
			throws MissingSchemaComponentException,
			DeclarationGenerationException 
	{
		final Message messageType = wsdlCatalog.getMessageType(new QName(MSG_TYPES_NS_URI, name));
		return declGenerator.generate(xsdCatalog, "", messageType);
	}
}
