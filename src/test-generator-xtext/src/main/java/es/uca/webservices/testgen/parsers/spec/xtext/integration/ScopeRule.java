package es.uca.webservices.testgen.parsers.spec.xtext.integration;

import java.util.List;

import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;

public class ScopeRule {
	/* 
	 * Default strategy, which should not accept any options other than the PRNG and/or seed
	 * and be able to generate any type. This is useful to avoid creating many instances of
	 * the same strategy, saving memory in the process.
	 */
	private List<IType> types;
	private IStrategy strategy;

	public ScopeRule(List<IType> types, IStrategy strategy) {
		this.types = types;
		this.strategy = strategy;
	}

	public void addType(IType type) {
		types.add(type);
	}

	public List<IType> getTypes() {
		return types;
	}

	public void setTypes(List<IType> types) {
		this.types = types;
	}

	public IStrategy getStrategy() {
		return strategy;
	}

	public void setStrategy(IStrategy strategy) {
		this.strategy = strategy;
	}
}
