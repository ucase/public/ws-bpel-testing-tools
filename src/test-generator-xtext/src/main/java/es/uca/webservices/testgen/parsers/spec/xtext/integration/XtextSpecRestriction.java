package es.uca.webservices.testgen.parsers.spec.xtext.integration;


import java.util.ArrayList;
import java.util.List;

import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Restrictions;

/**
 * Class that represents the rule restriction of the grammar TestSpec.g
 */
public class XtextSpecRestriction {
	
	private String primitiveTypeToken;
    private Restrictions restrictions;

    // Names of the typedefs, if any, from most to least specific
    private List<String> typedefNames;

    /**
     * Constructor a XtextSpecRestriction with a token that represent a type
     * @param type 
     */
    public XtextSpecRestriction(String type) {
        primitiveTypeToken = type;
        typedefNames = new ArrayList<String>();
    }

    /**
     * Copy constructor.
     */
    public XtextSpecRestriction(XtextSpecRestriction superTypedef) {
    	this(superTypedef.getPrimitiveTypeToken());
    	addRestriction(superTypedef.getRestriction());
    	typedefNames = new ArrayList<String>(superTypedef.getTypedefNames());
	}

	/**
     * @return the type
     */
    public String getPrimitiveTypeToken() {
        return primitiveTypeToken;
    }

    /**
     * Associates the specified key with the specified value and add in restrictions
     * @param key
     * @param value 
     */
	public void addRestriction(Restrictions value) {
		if (this.restrictions != null) {
			this.restrictions.getRestriction().addAll(value.getRestriction());
		} else {
			this.restrictions = value;
		}
	}

	/**
     * @return the restriction
     */
    public Restrictions getRestriction() {
        return restrictions;
    }

    /**
	 * Returns a list with the names of the typedefs involved in this set of
	 * restrictions, from most to least specific. Does not include the primitve
	 * type itself (use {@link #getPrimitiveTypeToken()} for that instead.
	 */
	public List<String> getTypedefNames() {
		return typedefNames;
	}

	/**
	 * Adds a typedef to the list of typedefs, as the most specific typedef yet.
	 */
	public void addTypedefName(String typedef) {
		typedefNames.add(0, typedef);
	}
    
}
