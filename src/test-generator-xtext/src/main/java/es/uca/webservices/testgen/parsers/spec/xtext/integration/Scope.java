package es.uca.webservices.testgen.parsers.spec.xtext.integration;

import es.uca.webservices.testgen.api.generators.IRandomStrategy;
import es.uca.webservices.testgen.api.generators.IStrategy;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeTuple;

public class Scope {

	private List<IType> types = new ArrayList<IType>();// root of tree

	/*
	 * Map from rules to leaves.
	 */
	private Map<IType, ScopeRule> assignments = new IdentityHashMap<IType, ScopeRule>();

	/*
	 * Scope of the element list.
	 */
	private Map<IType, Scope> nestedScopes = new IdentityHashMap<IType, Scope>();

	public Scope() {
		// default constructor: nothing to do!
	}

	public Scope(List<IType> types) {
		this.assignTypes(types);
	}

	public List<IType> getTypes() {
		return types;
	}

	public void setTypes(List<IType> types) {
		this.assignTypes(types);
	}

	protected void assignTypes(List<IType> types) {
		this.types = types;
		for (IType itype : types) {
			if (itype instanceof TypeList) {
				// if there's a list, it's a new scope
				final List<IType> listTypeNestedScope = new ArrayList<IType>();
				listTypeNestedScope.add(((TypeList) itype).getType());
				nestedScopes.put(itype, new Scope(listTypeNestedScope));
			} else if (itype instanceof TypeTuple) {
				// lists inside a tuple are also new scopes
				searchNestedScopeInTuple((TypeTuple) itype);
			}
		}
	}

	private void searchNestedScopeInTuple(TypeTuple tuple) {
		for (int i = 0; i < tuple.size(); ++i) {
			if (tuple.getIType(i) instanceof TypeList) {
				final List<IType> listTypeNestedScope = new ArrayList<IType>();
				listTypeNestedScope.add(((TypeList) tuple.getIType(i)).getType());
				nestedScopes.put(tuple.getIType(i), new Scope(listTypeNestedScope));
			} else if (tuple.getIType(i) instanceof TypeTuple) {
				searchNestedScopeInTuple((TypeTuple) tuple.getIType(i));
			}
		}
	}

	public Map<IType, ScopeRule> getAssignments() {
		return assignments;
	}

	public void setAssignments(Map<IType, ScopeRule> assigments) {
		this.assignments = assigments;
	}

	public Map<IType, Scope> getNestedScopes() {
		return nestedScopes;
	}

	public void setPRNG(Random prng) {
		for (ScopeRule rule : this.assignments.values()) {
			IStrategy strat = rule.getStrategy();
			if (strat instanceof IRandomStrategy) {
				((IRandomStrategy) strat).setPRNG(prng);
			}
		}
		for (Scope nestedScope : this.nestedScopes.values()) {
			nestedScope.setPRNG(prng);
		}
	}

}
