package es.uca.webservices.testgen.parsers.spec.xtext.integration;

import es.uca.webservices.testgen.strategies.random.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;

import com.google.inject.Injector;

import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.parsers.IParser;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.api.types.AbstractDates;
import es.uca.webservices.testgen.api.types.AbstractType;
import es.uca.webservices.testgen.api.types.TypeDate;
import es.uca.webservices.testgen.api.types.TypeDateTime;
import es.uca.webservices.testgen.api.types.TypeDuration;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTime;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.parsers.spec.xtext.TestSpecStandaloneSetupGenerated;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Entry;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Model;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Path;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Restriction;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.RestrictionValue;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Restrictions;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Rule;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Strategy;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Typedef;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Variable;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.VariableList;

public class XtextSpecParser implements IParser {
	
	private String specPath;

	private static final String TOTALDIGITS = "totalDigits";
	private static final String FRACTIONDIGITS = "fractionDigits";
	private static final String MAX = "max";
	private static final String MIN = "min";
	private static final String ELEMENT = "element";
	private static final String VALUES = "values";
	private static final String PATTERN = "pattern";

	private static DatatypeFactory dataTypeFactory;

	private Map<String, XtextSpecRestriction> restrictionsByTypedef = new HashMap<String, XtextSpecRestriction>();
	private List<IType> types = new ArrayList<IType>();
	private Map<String, Strategy> strategies = new HashMap<String, Strategy>();

	public XtextSpecParser() {
		// for SnakeYAML
	}

	public XtextSpecParser(String path) {
		this.specPath = path;
	}

	public String getSpec() {
		return specPath;
	}

	public void setSpec(String specPath) {
		this.specPath = specPath;
	}

	public List<IType> getTypes() {
		return types;
	}

	@Override
	public List<IType> parse(String... args) throws ParserException {
		final Resource resource = getResource();

		if (resource.getContents().isEmpty()) {
			return types;
		} else {
			readTypes(resource);
		}

		return types;
	}

	/**
	 * Validates and returns the EMF {@link Resource} underlying the TestSpec
	 * specification.
	 */
	public Resource getResource() throws ParserException {
		final Injector injector = new TestSpecStandaloneSetupGenerated().createInjectorAndDoEMFRegistration();
		final XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL,	Boolean.TRUE);
		final Resource resource = resourceSet.getResource(URI.createURI(specPath), true);

		final IResourceValidator validator = injector
				.getInstance(IResourceValidator.class);
		List<Issue> list = validator.validate(resource, CheckMode.ALL,
				CancelIndicator.NullImpl);
		if (!list.isEmpty()) {
			final StringBuilder sb = new StringBuilder(
					"There are validation issues in '" + specPath + "':");
			for (Issue issue : list) {
				sb.append("\n - ");
				sb.append(issue);
			}
			throw new ParserException(sb.toString());
		}
		return resource;
	}

	@Override
	public IStrategy getStrategy(String name) throws ParserException {
		if (name == null || "".equals(name)){
			try {
				return getStrategyByName(Constants.DEFAULT_STRATEGY);
			} catch (Exception e) {
				throw new ParserException(e); 
			}
		}

		final Strategy strategy = strategies.get(name);
		if (strategy == null) {
			throw new ParserException(
				String.format("Unknown strategy '%s': known strategies are %s", name, strategies.keySet()));
		}
 
		final List<IType> typesScope = new ArrayList<IType>();
		for (IType type : types) {
			if (type instanceof TypeTuple) {
				for (int i = 0; i < ((TypeTuple) type).size(); ++i) {
					typesScope.add(((TypeTuple) type).getIType(i));
				}
			}
			typesScope.add(type);
		}

		final Scope scope = fillScope(new Scope(typesScope), strategy);
		return new CompositeStrategy(scope);
	}

	private Scope fillScope(Scope scope, Strategy strategy) throws ParserException {
		for (IType typeScope : scope.getTypes()) {
			addDefaultSizeStrategyIfMissing(scope, typeScope);
		}

		for (Rule rule : strategy.getRules()) {
			final String strategyName = rule.getStrategy().getNameStrategy();
			IStrategy st = createStrategy(strategyName, rule);

			final List<IType> listItypeOfScopeRule = createListItypeOfScopeRule(rule);
			final ScopeRule scopeRule = new ScopeRule(listItypeOfScopeRule, st);

			for (Path path : rule.getPaths()) {
				IType itype = searchITypeWithVariableName(path.getVariable().getName());
				Scope scp = getScopeFor(path.getIndices(), itype, scope);
				for (IType typeScope : scp.getTypes()) {
					addDefaultSizeStrategyIfMissing(scp, typeScope);
				}

				if (!path.getIndices().isEmpty()) {
					isCompositeIType(itype);
					itype = getAtomicIType(itype, path.getIndices());
				}
				scp.getAssignments().put(itype, scopeRule);
			}
		}
		return scope;
	}

	/**
	 * Uses the default strategy for the size of a list, if not specified.
	 */
	private void addDefaultSizeStrategyIfMissing(Scope scope, IType typeScope) {
		if (typeScope instanceof TypeList) {
			// Whether they have a rule for it or not, lists must have some
			// strategy for its size.
			if (!scope.getAssignments().containsKey(typeScope)) {
				List<IType> listTypeForScopeRuleList = new ArrayList<IType>();
				listTypeForScopeRuleList.add(typeScope);
				ScopeRule scopeRuleForList = new ScopeRule(listTypeForScopeRuleList, new UniformRandomStrategy());
				scope.getAssignments().put(typeScope, scopeRuleForList);
			}
		}
	}

	private Scope getScopeFor(List<String> path, IType itype, Scope scope) {
		if(!path.isEmpty()){
			if(itype instanceof TypeList){
				final Scope scopeNested = scope.getNestedScopes().get(itype);
				final IType itypeList = ((TypeList)itype).getType();
				return getScopeFor(path.subList(1, path.size()),itypeList,scopeNested);
			}
			if(itype instanceof TypeTuple){
				final IType itypeTuple = ((TypeTuple)itype).getIType(Integer.parseInt(path.get(0))-1);
				return getScopeFor(path.subList(1, path.size()),itypeTuple,scope);
			}
			for(IType type : scope.getTypes()){
				if(type==itype){
					return scope;
				}
			}
		}
		return scope;
	}

	private List<IType> createListItypeOfScopeRule(Rule rule) throws ParserException {
		final List<IType> listItypeOfScopeRule = new ArrayList<IType>();
		for(Path path: rule.getPaths()){
			IType itype = searchITypeWithVariableName(path.getVariable().getName());
			itype = getAtomicIType(itype,path.getIndices());
			listItypeOfScopeRule.add(itype);
		}
		return listItypeOfScopeRule;
	}

	private IType getAtomicIType(IType itype, List<String> indices) throws ParserException {
		if(!indices.isEmpty()){
			isCompositeIType(itype);
			final int ind = Integer.parseInt(indices.get(0));
			if(itype instanceof TypeList){
				
				if(ind!=1){//Indices List must be 1
					throw new ParserException("Indice List must be '1', and was " + indices.get(0));
				}
				return getAtomicIType(((TypeList)itype).getType(),indices.subList(1, indices.size()));
			}
			else if(itype instanceof TypeTuple){
				return getAtomicITypeInTypeTuple(itype,indices);
			}
		}
		return itype;
	}

	private IType getAtomicITypeInTypeTuple(IType itype, List<String> list) throws ParserException {
		isCompositeIType(itype);
		if(list.size()>1){
			IType element = ((TypeTuple)itype).getIType(Integer.parseInt(list.get(0))-1);
			return getAtomicITypeInTypeTuple(element,list.subList(1, list.size()));
		} else{
			if(itype instanceof TypeTuple){
				return ((TypeTuple)itype).getIType(Integer.parseInt(list.get(0))-1);//In spec the indices start in 1 and in java indices start in 0
			} else{
				if(Integer.parseInt(list.get(0))!=1){
					throw new ParserException("Indice List must be '1', ans was " + list.get(0));
				}
				return ((TypeList)itype).getType();
			}
		}
		
	}

	private IType searchITypeWithVariableName(String variableName) {
		IType itype=null;
		for(IType type : types){
			if(variableName.equals(type.getNameVariable())){
				itype=type;
				break;
			}
		}
		return itype;
	}

	private IStrategy createStrategy(String strategyName, Rule rule) throws ParserException {
		IStrategy st;
		try {
			st = getStrategyByName(strategyName);
			if(rule.getStrategy().getListParameter() != null){
				st.init(rule.getStrategy().getListParameter().toArray());
			}
		} catch (Exception e) {
			throw new ParserException(e);
		};
		return st;
	}

	private void isCompositeIType(IType itype) throws ParserException {
		if(!(itype instanceof TypeList || itype instanceof TypeTuple)){
			throw new ParserException("To use the operator '[NUM]', the type must be a List or Tuple and it was " 
					+ itype.getClass().getName());
		}
	}

	private void readTypes(final Resource resource) throws ParserException {
		final Model model = (Model) resource.getContents().get(0);		
		for (Entry entry : model.getEntries()) {
			if (entry instanceof Typedef) {
				final Typedef typedef = (Typedef) entry;
				putTypedef(typedef);
			} else if (entry instanceof VariableList) {
				final VariableList decl = (VariableList) entry;
				for(Variable v : decl.getVars()) {
					try {
						final IType at = createAbstractType(v);
						at.setNameVariable(v.getName());
						types.add(at);
					} catch (DatatypeConfigurationException e) {
						throw new ParserException(e);
					}
				}
			} else if (entry instanceof Strategy) {
				final Strategy strategy = (Strategy)entry;
				strategies.put(strategy.getName(), strategy);
			}
		}
	}
	private IStrategy getStrategyByName(final String strategyName) throws IllegalAccessException, InstantiationException {
		return Constants.RANDOM_STRATEGIES.get(strategyName).newInstance();
	}

	private XtextSpecRestriction putTypedef(Typedef typedef) {
		final String nameTypedef = typedef.getName();
		if (restrictionsByTypedef.containsKey(nameTypedef)) {
			return restrictionsByTypedef.get(nameTypedef);
		}

		XtextSpecRestriction restrictions;
		if (typedef.getTypedef() != null) {
			// Typedef based on another typedef: inherit its restrictions and type chain
			final XtextSpecRestriction superTypedef = putTypedef(typedef.getTypedef());
			restrictions = new XtextSpecRestriction(superTypedef);
		}
		else {
			// Typedef based on a primitive value
			restrictions = new XtextSpecRestriction(typedef.getPrimitive().name());
		}
		restrictions.addRestriction(typedef.getRestrictions());
		restrictions.addTypedefName(typedef.getName());
		
		restrictionsByTypedef.put(nameTypedef, restrictions);
		return restrictions;
	}

	private IType createAbstractType(Variable decl)
			throws DatatypeConfigurationException, ParserException {
		VariableList vL=(VariableList)decl.eContainer();
		if (vL.getTypedef() != null) {
			return createUserType(vL.getTypedef().getName());
		} else {
			return createBasicType(vL.getPrimitive().getLiteral());
		}
	}

	private IType createUserType(String nameUser) throws ParserException, DatatypeConfigurationException {
		final XtextSpecRestriction restriction = this.restrictionsByTypedef.get(nameUser);
		if (restriction == null) {
			return createBasicType(nameUser);
		}
		try {
			final String name = restriction.getPrimitiveTypeToken().toUpperCase();
			AbstractType type;
			if (name.equals("INT")) {
				type = createTypeInt(nameUser);
			} else if (name.equals("FLOAT")) {
				type = createTypeFloat(nameUser);
			} else if (name.equals("STRING")) {
				type = createTypeString(nameUser);
			} else if (name.equals("DATE")) {
				type = createTypeDate(nameUser);
			} else if (name.equals("DATETIME")) {
				type = createTypeDateTime(nameUser);
			} else if (name.equals("TIME")) {
				type = createTypeTime(nameUser);
			} else if (name.equals("DURATION")) {
				type = createTypeDuration(nameUser);
			} else if (name.equals("LIST")){
				type = createTypeList(nameUser);
			} else if (name.equals("TUPLE")) {
				type = createTypeTuple(nameUser);
			} else {
				throw new ParserException("Unknown primitive type token " + name);
			}

			final List<String> typedefNames = restriction.getTypedefNames();
			for (int i = typedefNames.size() - 1; i >= 0; i--) {
				type.addTypedefName(typedefNames.get(i));
			}

			return type;
		} catch (DatatypeConfigurationException ex) {
			throw new ParserException(ex);
		}
	}

	/**
	 * Create basic type (TypeInt, TypeFloat, TypeString, TypeDuration,
	 * TypeDate...)
	 * 
	 * @param name
	 * @return
	 * @throws ParserException
	 */
	private IType createBasicType(String name)
			throws DatatypeConfigurationException {
		final String nameUpperCase=name.toUpperCase();
		if (nameUpperCase.equals("INT")) {
			return new TypeInt();
		} else if (nameUpperCase.equals("FLOAT")) {
			return new TypeFloat();
		} else if (nameUpperCase.equals("STRING")) {
			return new TypeString();
		} else if (nameUpperCase.equals("DATE")) {
			return new TypeDate();
		} else if (nameUpperCase.equals("DATETIME")) {
			return new TypeDateTime();
		} else{// "DURATION"
			return new TypeDuration();
		}
	}

	/**
	 * Create TypeInt with restriction
	 * 
	 * @param nameUserType
	 * @return
	 * @throws ParserException
	 */
	private TypeInt createTypeInt(String nameUserType) {
		TypeInt typeInt = null;
		XtextSpecRestriction xtextSpecRestriction = restrictionsByTypedef.get(nameUserType);
		Restrictions restriction = xtextSpecRestriction.getRestriction();
		typeInt = new TypeInt();
		for (Restriction r : restriction.getRestriction()) {
			if (VALUES.equals(r.getName())) {
				List<BigInteger> values = new ArrayList<BigInteger>();
				for (RestrictionValue rv : r.getValue().getList()) {
					values.add(new BigInteger(rv.getInt()));
				}
				typeInt = new TypeInt(values);
			} else {
				if (MAX.equals(r.getName())) {
					final BigInteger max = new BigInteger(r.getValue().getInt());
					typeInt.setMaxValue(max);
				}
				if (MIN.equals(r.getName())) {
					final BigInteger min = new BigInteger(r.getValue().getInt());
					typeInt.setMinValue(min);
				}
				if (TOTALDIGITS.equals(r.getName())) {
					typeInt.setTotalDigits(Integer.parseInt(r.getValue().getInt()));
				}
			}
		}
		return typeInt;
	}

	/**
	 * Create TypeDFloat with restriction
	 * 
	 * @param nameUserType
	 * @return
	 */
	private TypeFloat createTypeFloat(String nameUserType) {
		TypeFloat typeFloat = null;
		XtextSpecRestriction xtextSpecRestriction = restrictionsByTypedef.get(nameUserType);
		Restrictions restriction = xtextSpecRestriction.getRestriction();
		typeFloat = new TypeFloat();
		for (Restriction r : restriction.getRestriction()) {
			if (VALUES.equals(r.getName())) {
				List<BigDecimal> values = new ArrayList<BigDecimal>();
				for (RestrictionValue rv : r.getValue().getList()) {
					values.add(new BigDecimal(String.valueOf(rv.getFloat())));
				}
				typeFloat = new TypeFloat(values);
			} else {
				if (MAX.equals(r.getName())) {
					String maxString;
					if(r.getValue().getFloat() != null){
						maxString = r.getValue().getFloat();
					}else{
						maxString = r.getValue().getInt();
					}
					final BigDecimal max = new BigDecimal(maxString);
					typeFloat.setMaxValue(max);
				}
				if (MIN.equals(r.getName())) {
					String minString;
					if(r.getValue().getFloat() != null){
						minString = r.getValue().getFloat();
					}else{
						minString = r.getValue().getInt();
					}
					final BigDecimal min = new BigDecimal(minString);
					typeFloat.setMinValue(min);
				}
				if (TOTALDIGITS.equals(r.getName())) {
					typeFloat.setTotalDigits(Integer.parseInt(r.getValue().getInt()));
				}
				if (FRACTIONDIGITS.equals(r.getName())) {
					typeFloat.setFractionDigits(Integer.parseInt(r.getValue().getInt()));
				}
			}
		}
		return typeFloat;
	}

	/**
	 * Create TypeString with restriction
	 * 
	 * @param nameUserType
	 * @return
	 */
	private TypeString createTypeString(String nameUserType) {
		XtextSpecRestriction xtextSpecRestriction = restrictionsByTypedef.get(nameUserType);
		Restrictions restriction = xtextSpecRestriction.getRestriction();
		TypeString typeString = new TypeString();
		for (Restriction r : restriction.getRestriction()) {
			if (VALUES.equals(r.getName())) {
				List<String> values = new ArrayList<String>();
				for (RestrictionValue rv : r.getValue().getList()) {
					values.add(rv.getString().replace("\"", ""));
				}
				typeString = new TypeString(values);
			} else if (PATTERN.equals(r.getName())) {
				typeString = new TypeString(r.getValue().getString());
			} else {
				if(MIN.equals(r.getName())){
					typeString.setMinLength(Integer.parseInt(r.getValue().getInt()));
				}else if(MAX.equals(r.getName())){
					typeString.setMaxLength(Integer.parseInt(r.getValue().getInt()));
				}
			}
		}
		return typeString;
	}

	private TypeDuration createTypeDuration(String nameUserType)
			throws ParserException, DatatypeConfigurationException {
		TypeDuration typeDuration = new TypeDuration();
		final DatatypeFactory factory = getDatatypeFactory();
		Duration duration = null;
		XtextSpecRestriction xtextSpecRestriction = restrictionsByTypedef.get(nameUserType);
		Restrictions restriction = xtextSpecRestriction.getRestriction();
		for (Restriction r : restriction.getRestriction()) {
			if (MAX.equals(r.getName())) {
				duration = factory.newDuration(r.getValue().getString()
						.replace("\"", ""));
				typeDuration.setMaxValue(duration);
			}
			if (MIN.equals(r.getName())) {
				duration = factory.newDuration(r.getValue().getString()
						.replace("\"", ""));
				typeDuration.setMinValue(duration);
			}
		}
		return typeDuration;
	}

	private TypeDateTime createTypeDateTime(String nameUserType)
			throws ParserException, DatatypeConfigurationException {
		XtextSpecRestriction xtextSpecRestriction = restrictionsByTypedef.get(nameUserType);
		Restrictions restriction = xtextSpecRestriction.getRestriction();
		return (TypeDateTime) initializeFromXMLGregorianCalendar(
				new TypeDateTime(), restriction);
	}

	private TypeTime createTypeTime(String nameUserType) throws ParserException,
			DatatypeConfigurationException {
		XtextSpecRestriction xtextSpecRestriction = restrictionsByTypedef.get(nameUserType);
		Restrictions restriction = xtextSpecRestriction.getRestriction();
		return (TypeTime) initializeFromXMLGregorianCalendar(new TypeTime(),
				restriction);
	}

	private TypeDate createTypeDate(String nameUserType) throws ParserException,
			DatatypeConfigurationException {
		XtextSpecRestriction xtextSpecRestriction = restrictionsByTypedef.get(nameUserType);
		Restrictions restriction = xtextSpecRestriction.getRestriction();
		return (TypeDate) initializeFromXMLGregorianCalendar(new TypeDate(),
				restriction);
	}

	/**
	 * Create AbstractDate the typeDate with restriction
	 * 
	 * @param typeDate
	 * @param restriction
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private AbstractDates initializeFromXMLGregorianCalendar(
			AbstractDates typeDate, Restrictions restriction)
			throws DatatypeConfigurationException {
		final DatatypeFactory factory = getDatatypeFactory();
		XMLGregorianCalendar calendar = null;
		for (Restriction r : restriction.getRestriction()) {
			if (MAX.equals(r.getName())) {
				calendar = factory.newXMLGregorianCalendar(r.getValue()
						.getString().replace("\"", ""));
				typeDate.setMaxValue(calendar);
			}
			if (MIN.equals(r.getName())) {
				calendar = factory.newXMLGregorianCalendar(r.getValue()
						.getString().replace("\"", ""));
				typeDate.setMinValue(calendar);
			}
		}
		return typeDate;
	}

	/**
	 * Instance a DatatypeFactory
	 * 
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private synchronized DatatypeFactory getDatatypeFactory()
			throws DatatypeConfigurationException {
		if (dataTypeFactory == null) {
			dataTypeFactory = DatatypeFactory.newInstance();
		}
		return dataTypeFactory;
	}

	/**
	 * Create TypeTuple with restriction
	 * 
	 * @param nameUserType
	 * @return
	 * @throws ParserException
	 * @throws DatatypeConfigurationException 
	 */

	private TypeTuple createTypeTuple(String nameUserType)
			throws ParserException, DatatypeConfigurationException {
		final XtextSpecRestriction xtextSpecRestriction = restrictionsByTypedef.get(nameUserType);
		final Restrictions restriction = xtextSpecRestriction.getRestriction();
		final Restriction restrict = getElementRestriction(restriction);
		final EList<RestrictionValue> elems = restrict.getValue().getList();
		final int sizeList = elems.size();
		IType[] ntuple = new IType[sizeList];
		for (int j = 0; j < sizeList; ++j) {
			final RestrictionValue elem = elems.get(j);

			String type;
			if (elem.getTypedef() == null) {
				type = elem.getPrimitive().getLiteral();
			} else {
				type = elem.getTypedef().getName();
			}
			ntuple[j] = createUserType(type);
		}
		
		return new TypeTuple(ntuple);
	}

	/**
	 * Create TypeList with restriction
	 * 
	 * @param nameUserType
	 * @return
	 * @throws ParserException
	 * @throws DatatypeConfigurationException
	 */
	private TypeList createTypeList(String nameUserType)
			throws ParserException, DatatypeConfigurationException {
		final XtextSpecRestriction xtextSpecRestriction = restrictionsByTypedef.get(nameUserType);
		final Restrictions restriction = xtextSpecRestriction.getRestriction();

		// There are two ways to write a list with an element of type TElem:
		//
		//   typedef list (element=TElem) TElemList;   -> 'element' restriction has 'typedef' value
		//   typedef list (element={TElem}) TElemList; -> 'element' restriction has 'list' value
		//
		// Both should be acceptable.
		RestrictionValue elementValue = getElementRestriction(restriction).getValue();
		final EList<RestrictionValue> listValue = elementValue.getList();
		if (!listValue.isEmpty()) {
			// If we use a 'list' value, use the first value in the list
			if (listValue.size() != 1) {
				throw new ParserException(
						"'element' property in list typedefs must have exactly 1 element, but it has "
								+ listValue.size());
			}
			elementValue = listValue.get(0);
		}

		String type;
		if (elementValue.getTypedef() == null) {
			type = elementValue.getPrimitive().getLiteral();
		}
		else {
			type = elementValue.getTypedef().getName();
		}
	
		TypeList tList = new TypeList(createUserType(type));
		int min = tList.getMinNumElement();
		int max = tList.getMaxNumElement();
		for (Restriction r : restriction.getRestriction()) {
			if (MIN.equals(r.getName())) {
				min = Integer.parseInt(r.getValue().getInt());
			}
			if (MAX.equals(r.getName())) {
				max = Integer.parseInt(r.getValue().getInt());
			}
		}
		tList = new TypeList(createUserType(type), min, max);
		return tList;
	}

	private Restriction getElementRestriction(final Restrictions restriction)
			throws ParserException {
		final EList<Restriction> listRest = restriction.getRestriction();
		Restriction restrict1 = null;
		for (Restriction r : listRest) {
			if (ELEMENT.equals(r.getName())) {
				restrict1 = r;
				break;
			}
		}
		if (restrict1 == null) {
			throw new ParserException(
					"tuple" + " typedef does not contain the 'element' property");
		}
		return restrict1;
	}
}
