package es.uca.webservices.testgen.parsers.spec.xtext.integration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IRandomStrategy;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.strategies.random.AbstractRandomStrategy;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;
import es.uca.webservices.testgen.strategies.random.util.RandomProxy;

/**
 * A strategy which can delegates on other strategies for specific variables or parts of them.
 * This strategy overrides all the PRNGs in the other strategies, making sure that they share
 * the same PRNG and that they cannot change the seed on their own.
 */
public class CompositeStrategy extends AbstractRandomStrategy {
	final static Logger LOGGER = LoggerFactory.getLogger(CompositeStrategy.class);

	/**
	 * Default strategy used to generate list sizes and so on when nothing has
	 * been specified. Shares the PRNG with this CompositeStrategy.
	 */
	private final UniformRandomStrategy defaultStrategy;

	private final Scope scope;
	private final Map<IType, Object> valuesForIType = new IdentityHashMap<IType, Object>();

	public CompositeStrategy(Scope scope) {
		this.scope = scope;
		this.defaultStrategy = new UniformRandomStrategy();
		 
		final RandomProxy proxy = new RandomProxy(this);
		defaultStrategy.setPRNG(proxy);
		setScopePRNG(scope, proxy);
	}

	/**
	 * Generation with strategies
	 * @param abstractTypes
	 * @return values generated
	 * @throws GenerationException
	 */
	public synchronized Object[] generate(IType... abstractTypes) throws GenerationException {
		// Step 1: create map from the name of the abstract type name to the IType object
		Map<String, IType> mapAbstractTypes = createMapAbstractTypes(abstractTypes);

		// Step 2: go through rules and find the matching ITypes, collecting
		// them while removing them from the above map
		fillValuesForIType(mapAbstractTypes, scope);
		if (!mapAbstractTypes.isEmpty()) {
			// The remaining elements do not have a strategy defined for them
			generateElementWithoutStrategy(mapAbstractTypes);
		}

		// Create the array to be returned
		final Object[] values = new Object[abstractTypes.length];
		for (int i = 0; i < abstractTypes.length; ++i) {
			values[i] = getCorrectValue(abstractTypes[i]);
		}
		valuesForIType.clear();
		return values;
	}


	/**
	 * Propagate a specific PRNG through all the rules with randomized strategies
	 * in the scope. Used in the constructor to enforce all strategies to use the
	 * same PRNG through a proxy.
	 */
	private void setScopePRNG(Scope scope, Random prng) {
		for (ScopeRule rule : scope.getAssignments().values()) {
			final IStrategy strategy = rule.getStrategy();
			if (strategy instanceof IRandomStrategy) {
				((IRandomStrategy)strategy).setPRNG(prng);
			}
		}
	
		for (Scope nestedScope : scope.getNestedScopes().values()) {
			setScopePRNG(nestedScope, prng);
		}
	}

	// Goes through all the nested scopes and creates the values for the assignments
	private Map<String, IType> fillValuesForIType(Map<String, IType> mapAbstractTypes, Scope scope) throws GenerationException {
		mapAbstractTypes = fillValuesForITypeHelp(mapAbstractTypes, scope);
		for(IType t : scope.getNestedScopes().keySet()) {
			fillValuesForIType(mapAbstractTypes, scope.getNestedScopes().get(t));
		}
		return mapAbstractTypes;
	}

	/**
	 * 
	 * @param mapAbstractTypes
	 * @param scope
	 * @return IType without value generated
	 * @throws GenerationException
	 */
	private Map<String, IType> fillValuesForITypeHelp(Map<String, IType> mapAbstractTypes, Scope scope)
			throws GenerationException {
		for (IType itype: scope.getAssignments().keySet()){
			if(!valuesForIType.containsKey(itype)){
				IStrategy st = scope.getAssignments().get(itype).getStrategy();

				List <IType> typesScopeRule = scope.getAssignments().get(itype).getTypes();
				for(IType t: typesScopeRule){
					if(t.getNameVariable() != null){
						mapAbstractTypes.remove(t.getNameVariable());
					}
				}
				Object[] valuesGenerated = new Object[1];
				if(itype instanceof TypeList && st instanceof UniformRandomStrategy){//In the list only must be generated the length, UniformStrategy generated all the list
					valuesGenerated[0] = ((UniformRandomStrategy)st).random(((TypeList)itype).getMinNumElement(), ((TypeList)itype).getMaxNumElement());//     st.generate(typesScopeRule.toArray(new IType[0]));
				} else{
					valuesGenerated = st.generate(typesScopeRule.toArray(new IType[0]));
				}
				
				if(itype instanceof TypeList){//If it is a list then there are generated special form	
					//Search length of the list in valuesGenerated
					int posLengthList = searchITypeInList(itype, typesScopeRule);			
					int lengthList = Integer.parseInt(valuesGenerated[posLengthList].toString());
					
					valuesGenerated[posLengthList] = getValueForList(lengthList,(TypeList)itype, scope.getNestedScopes().get(itype));//cambio el valor numerico por la lista bien generada
				}			
				
				for(int j=0; j< valuesGenerated.length; ++j){
					IType t = scope.getAssignments().get(itype).getTypes().get(j);
					if(!valuesForIType.containsKey(t)){
						if(valuesGenerated[j] instanceof Object[]){
							valuesForIType.put(t, ((Object[])valuesGenerated[j])[0]); 
						}else{
							valuesForIType.put(t, valuesGenerated[j]);
						}
						
					}
				}
			}
		}
		
		return mapAbstractTypes;
	}


	private Object getValueForList(int lengthList, TypeList typeList, Scope scopeNested) throws GenerationException {
		List<Object> valuesList = new ArrayList<Object>(lengthList);
		for(int i=0;i<lengthList; ++i){
			ScopeRule rule = scopeNested.getAssignments().get(typeList.getType());
			if(typeList.getType() instanceof TypeList){				
				Object[] lengthListNestedObjects = rule.getStrategy().generate(typeList.getType());
				int lengthListNested = Integer.parseInt(lengthListNestedObjects[0].toString());
				valuesList.add(getValueForList(lengthListNested,(TypeList) typeList.getType(), scopeNested.getNestedScopes().get(typeList.getType())));
			} else if(typeList.getType() instanceof TypeTuple){
				valuesList.add(getValueForTuple((TypeTuple)typeList.getType(),scopeNested));
			}else{
				valuesList.add(rule.getStrategy().generate(typeList.getType())[0]);
			}
		}
		return valuesList;
	}


	private Object getValueForTuple(TypeTuple typeTuple, Scope scopeNested) throws GenerationException {
		final Map<IType, Object> valuesForTuple = new IdentityHashMap<IType, Object>();
		List<Object> valuesTuple = new ArrayList<Object>(typeTuple.size());
		for(int i=0; i< typeTuple.size();++i){
			ScopeRule rule = scopeNested.getAssignments().get(typeTuple.getIType(i));
			List <IType> typesScopeRule= new ArrayList<IType>();
			IStrategy strategy;
			if( rule != null){ // HACK: this is done because the tuple list produces null
				typesScopeRule = rule.getTypes();
				strategy = rule.getStrategy();
			}else{
				for(IType itype: scopeNested.getTypes()){
					if(itype instanceof TypeTuple){
						for(int j=0; j<((TypeTuple)itype).size();++j){
							typesScopeRule.add(((TypeTuple)itype).getIType(j));
						}
					}else{
						typesScopeRule.add(itype);
					}
				}
				strategy = defaultStrategy;
			}

			if(!valuesForTuple.containsKey(typeTuple.getIType(i))){
				if(typeTuple.getIType(i) instanceof TypeList){				
					Object[] lengthListNestedObjects = strategy.generate(typesScopeRule.toArray(new IType[0]));
					int lengthListNested = Integer.parseInt(lengthListNestedObjects[0].toString());
					valuesTuple.add(getValueForList(lengthListNested,(TypeList) typeTuple.getIType(i), scopeNested.getNestedScopes().get(typeTuple.getIType(i))));
					for(int j=0;j<typesScopeRule.size();++j){
						valuesForTuple.put(typesScopeRule.get(j), lengthListNestedObjects[j]);
					}
				} else if(typeTuple.getIType(i) instanceof TypeTuple){
					valuesTuple.add(getValueForTuple((TypeTuple)typeTuple.getIType(i),scopeNested)); 
				}else{
					Object[] valuesGenerated = strategy.generate(typesScopeRule.toArray(new IType[0]));
					for(int j=0;j<typesScopeRule.size();++j){
						valuesForTuple.put(typesScopeRule.get(j), valuesGenerated[j]);
					}
					valuesTuple.add(valuesForTuple.get(typeTuple.getIType(i)));
				}
			}
			else{
				valuesTuple.add(valuesForTuple.get(typeTuple.getIType(i)));
			}
		}
		return valuesTuple;
	}


	private Object getCorrectValue(IType iType) throws GenerationException {
		if (valuesForIType.containsKey(iType)) {
			return valuesForIType.get(iType);
		} else if (iType instanceof TypeList) {
			return createListObject((TypeList) iType);
		} else if (iType instanceof TypeTuple) {
			return createTupleObject((TypeTuple) iType);
		} else {
			return defaultStrategy.generate(iType);
		}
	}

	private Object createListObject(TypeList list) throws GenerationException {
		if (valuesForIType.containsKey(list)) {
			return valuesForIType.get(list);
		} else {
			final int lengthList = defaultStrategy.random(list.getMinNumElement(), list.getMaxNumElement());
			final List<Object> valuesList = new ArrayList<Object>(lengthList);
			for (int i = 0; i < lengthList; ++i) {
				valuesList.set(i, getCorrectValue(list.getType()));
			}
			valuesForIType.put(list, valuesList);
			return valuesList;
		}
	} 


	private Object createTupleObject(TypeTuple tuple) throws GenerationException {
		if (valuesForIType.containsKey(tuple)){
			return valuesForIType.get(tuple);
		}

		final Object[] valuesTuple = new Object[tuple.size()];
		for(int k=0;k<tuple.size();++k){
			valuesTuple[k]=getCorrectValue(tuple.getIType(k));
		}
		valuesForIType.put(tuple, valuesTuple);

		return valuesTuple;
	}


	private void generateElementWithoutStrategy(Map<String, IType> mapAbstractTypes) throws GenerationException {
		for (String key : mapAbstractTypes.keySet()) {
			IType type = mapAbstractTypes.get(key);
			if (type instanceof TypeList) {
				valuesForIType.put(type, ((Object[]) defaultStrategy.generate(type))[0]);
			} else if (type instanceof TypeTuple) {
				Object[] valuesTuple = new Object[((TypeTuple) type).size()];
				for (int i = 0; i < ((TypeTuple) type).size(); ++i) {
					if (valuesForIType.containsKey(((TypeTuple) type).getIType(i))) {
						valuesTuple[i] = valuesForIType.get(((TypeTuple) type).getIType(i));
					} else {
						valuesForIType.put(type, defaultStrategy
								.generate(((TypeTuple) type).getIType(i)));
					}
				}
				valuesForIType.put(type, valuesTuple);
			} else {
				valuesForIType.put(type, ((Object[]) defaultStrategy.generate(type))[0]);
			}
		}
	}

	private Map<String, IType> createMapAbstractTypes(IType... abstractTypes) {
		Map<String, IType> mapAbstractTypes = new HashMap<String, IType>();
		for (IType iType : abstractTypes) {
			mapAbstractTypes.put(iType.getNameVariable(), iType);
		}
		return mapAbstractTypes;
	}

	private int searchITypeInList(IType itype, List<IType> typesScopeRule) {
		int posLengthList=0;
		while(typesScopeRule.get(posLengthList)!= itype){
			++posLengthList;
		}
		return posLengthList;
	}

	@Override
	public void init(Object... options) throws IllegalArgumentException {

	}

	@Override
	public void setPRNG(Random prng) {
		scope.setPRNG(prng);
	}

}
