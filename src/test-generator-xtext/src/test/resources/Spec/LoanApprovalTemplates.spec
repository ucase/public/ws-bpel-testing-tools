typedef int (min=1000, max=200000) Req;
typedef string (values={"true", "false", "silent", "smart"}) ApReply;
typedef int (min=0, max=120000) ApLimit;
typedef string (values={"low", "high", "silent", "smart"}) AsReply;
typedef int (min=0, max=5000) AsLimit;
typedef string (values={"true", "false"}) Accept;
Req req_amount;
ApReply ap_reply;
ApLimit ap_limit;
AsReply as_reply;
AsLimit as_limit;
Accept accepted;