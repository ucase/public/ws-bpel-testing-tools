typedef tuple (element={float, int}) tupla;
typedef list (element={tupla}) listaTupla;

listaTupla l;

strategy listaGaussian{
	//l.size : Gaussian(3, 1)
    l[1][1], l[1][2]: Gaussian(3, 1)
}
