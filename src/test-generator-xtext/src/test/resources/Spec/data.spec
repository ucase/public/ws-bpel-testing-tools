/*
Copyright (C) 2012 Álvaro Cortijo García

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

typedef date (min="1990-01-01", max="2012-12-31") DateIncome;
typedef string (pattern="[A-Z][a-z]{0,10}") Nombre;
typedef float (min = 0, max = 5000) TotalCost;
typedef int (min = 0, max = 1000) Quantity;
typedef tuple(element={Nombre,TotalCost,Quantity}) Cost;
typedef list (min=1,max=6,element={Cost}) Costs;
typedef float (min = 0, max = 50000) Total;
typedef tuple (element={Nombre,Total,Costs}) Section;
typedef list (min=1,max=5,element={Section}) Sections;

typedef float (min = 0, max = 30000) TotalRank;

typedef float (min = -30000, max = 6000) Taxation;

typedef int (min = 0, max = 2) Marketing;

typedef string (values={"TV","radio","leaflets"}) AdvertisingMedia;
typedef float (min = 200, max = 5000) AdvertisingCost;
typedef tuple (element={AdvertisingMedia, AdvertisingCost}) AdvertisignTuple;
typedef list (min=1,max=3,element={AdvertisignTuple}) Advertisign;

typedef int (min = 3, max = 40) OffersPercent;
typedef tuple (element={Nombre,OffersPercent}) Offer;
typedef list (min=6,max=6,element={Offer}) Offers;

typedef int (min = -20, max = 6) Fault;

DateIncome fecha;
Sections sectionsEstablishment1;
Sections sectionsEstablishment2;
TotalRank totalRank;
Taxation taxation;
Marketing marketing;
Advertisign advertisign;
Offers offers;
Fault fault;

