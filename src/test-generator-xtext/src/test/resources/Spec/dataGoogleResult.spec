/* GOOGLE */

typedef string(pattern="[a-z]{1,3}[.](com|org)") Domain;
typedef tuple(element={Domain}) GoogleResult;
typedef list(min=0,max=30,element={GoogleResult}) GoogleResults;

GoogleResults google;
