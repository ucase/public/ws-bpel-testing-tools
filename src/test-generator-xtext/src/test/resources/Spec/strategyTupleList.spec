typedef tuple (element={float, Lista}) Tupla;
typedef list (element={int}) Lista;

Tupla t;
int a, b;

strategy listaGaussian{
    t[1]: Gaussian(3, 1)
    t[2][1]: Gaussian(3, 1)
    a,b : Poisson()
}
