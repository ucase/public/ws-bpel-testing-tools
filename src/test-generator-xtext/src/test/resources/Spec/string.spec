typedef string (pattern="[0-9]{7}[a-zA-Z]") dni;
typedef string (pattern="[0-9A-Z]{1,3}(\\.[A-Z]{3}(\\.X){0,1}){0,1}") TAttr_Type;
typedef string (pattern="\u0200\u0205*") THex;
typedef string (pattern="\07\077\0777") TOct;
typedef string (min=1, max=32) TAttr_Instance;

dni myDNI;
TAttr_Type type;
THex hex;
TOct oct;
TAttr_Instance tAttr_Instance;
