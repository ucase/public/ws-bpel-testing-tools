package es.uca.webservices.testgen.parsers.xtextspec;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.parsers.spec.xtext.integration.CompositeStrategy;
import es.uca.webservices.testgen.parsers.spec.xtext.integration.Scope;
import es.uca.webservices.testgen.parsers.spec.xtext.integration.ScopeRule;


public class CompositeStrategyTest {

	@Test
	public void compositeStrategyListOfTuple() throws Exception {

		final TypeFloat tFloat = new TypeFloat();
		final TypeInt tInt = new TypeInt();
		final TypeTuple tTuple = new TypeTuple(tFloat, tInt);
		final TypeList tList = new TypeList(tTuple);
		tList.setNameVariable("tupleList");
		
		final List<IType> listOfScopeRule = new ArrayList<IType>();
		listOfScopeRule.add(tFloat);
		listOfScopeRule.add(tInt);
		final IStrategy mockElement = mock(IStrategy.class);
		final ScopeRule ruleElement = new ScopeRule(listOfScopeRule, mockElement);
		final Scope nestedScope = new Scope(Arrays.asList((IType)tTuple));
		nestedScope.getAssignments().put(tFloat, ruleElement);
		nestedScope.getAssignments().put(tInt, ruleElement);
		
		final IStrategy mockSize = mock(IStrategy.class);
		final List<IType> listOfScopeRuleMain = new ArrayList<IType>();
		listOfScopeRuleMain.add(tList);
		final ScopeRule ruleSize = new ScopeRule(listOfScopeRuleMain,mockSize);
		Scope scope = new Scope(Arrays.asList((IType)tList));
		scope.getAssignments().put(tList,ruleSize);
		scope.getNestedScopes().put(tList, nestedScope);
		
		doReturn(new Object[]{5}).when(mockSize).generate(tList);
		doReturn(new Object[]{13}).when(mockElement).generate(tFloat);
		doReturn(new Object[]{13}).when(mockElement).generate(tInt);
		doReturn(new Object[]{10,15}).when(mockElement).generate(listOfScopeRule.toArray(new IType[0]));
		
		final CompositeStrategy cs = new CompositeStrategy(scope);
		cs.generate(tList);
		
		verify(mockSize, times(1)).generate(tList);
		verify(mockElement, times(6)).generate(listOfScopeRule.toArray(new IType[0])); // 5 from the list and 1 more for the assignment
	}
	

	@Test
	public void compositeStrategyTupleList() throws Exception {

		final TypeInt tInt = new TypeInt();
		final TypeList tList = new TypeList(tInt);
		final TypeFloat tFloat = new TypeFloat();
		final TypeTuple tTuple = new TypeTuple(tFloat, tList);
		tTuple.setNameVariable("t");
		final TypeInt tInta = new TypeInt();
		tInta.setNameVariable("a");
		final TypeInt tIntb = new TypeInt();
		tIntb.setNameVariable("b");
		
		final IStrategy mockElement = mock(IStrategy.class);
		final ScopeRule ruleElement = new ScopeRule(Arrays.asList((IType)tInt), mockElement);
		List<IType> types = new ArrayList<IType>();
		types.add(tInt);
		final Scope nestedScope = new Scope(types);
		nestedScope.getAssignments().put(tInt, ruleElement);

		final ScopeRule rule = new ScopeRule(Arrays.asList((IType)tFloat),mockElement);
		List<IType> type = new ArrayList<IType>();
		type.add(tTuple);
		type.add(tList);
		type.add(tFloat);
		type.add(tInta);
		type.add(tIntb);
		Scope scope = new Scope(type);
		scope.getAssignments().put(tFloat,rule);
		final IStrategy mockSize = mock(IStrategy.class);
		final ScopeRule ruleSizeList = new ScopeRule(Arrays.asList((IType)tList),mockSize);
		scope.getAssignments().put(tList,ruleSizeList);
		
		final IStrategy mockPoisson = mock(IStrategy.class);
		final ScopeRule ruletIntab = new ScopeRule(Arrays.asList((IType)tInta, (IType)tIntb),mockPoisson);
		scope.getAssignments().put(tInta,ruletIntab);
		scope.getAssignments().put(tIntb,ruletIntab);
		
		scope.getNestedScopes().put(tList, nestedScope);
		
		doReturn(new Object[]{10}).when(mockPoisson).generate(tInta);
		doReturn(new Object[]{20}).when(mockPoisson).generate(tIntb);
		doReturn(new Object[]{10, 20}).when(mockPoisson).generate(new IType[]{tInta, tIntb});
		doReturn(new Object[]{13.3}).when(mockElement).generate(tFloat);
		doReturn(new Object[]{13}).when(mockElement).generate(tInt);
		doReturn(new Object[]{5}).when(mockSize).generate(tList);
		
		final CompositeStrategy cs = new CompositeStrategy(scope);
		cs.generate(tTuple,tInta,tIntb);
		
		verify(mockPoisson, times(1)).generate(tInta,tIntb);
		verify(mockElement, atLeastOnce()).generate(tFloat);
		verify(mockElement, atLeast(5)).generate(tInt);
	}
	
	@Test
	public void compositeStrategyListOfList() throws Exception {

		final TypeInt tInt = new TypeInt();
		final TypeList tList = new TypeList(tInt);
		final TypeList tListL = new TypeList(tList);
		tListL.setNameVariable("l");
		
		final IStrategy mockSizetList = mock(IStrategy.class);
		final ScopeRule ruleSizetList = new ScopeRule(Arrays.asList((IType)tList), mockSizetList);
		final Scope nestedScope = new Scope(Arrays.asList((IType)tList));
		nestedScope.getAssignments().put(tList, ruleSizetList);
		final Scope nestednestedScope = new Scope(Arrays.asList((IType)tInt));
		final IStrategy mocktInt = mock(IStrategy.class);
		final ScopeRule ruletInt = new ScopeRule(Arrays.asList((IType)tInt), mocktInt);
		nestednestedScope.getAssignments().put(tInt, ruletInt);
		nestedScope.getNestedScopes().put(tList, nestednestedScope);

		Scope scope = new Scope(Arrays.asList((IType)tListL));
		final IStrategy mocktListL = mock(IStrategy.class);
		final ScopeRule ruleSizeListL = new ScopeRule(Arrays.asList((IType)tListL),mocktListL);
		scope.getAssignments().put(tListL,ruleSizeListL);
		scope.getNestedScopes().put(tListL, nestedScope);
		
		doReturn(new Object[]{10}).when(mocktInt).generate(tInt);
		doReturn(new Object[]{5}).when(mockSizetList).generate(tList);
		doReturn(new Object[]{3}).when(mocktListL).generate(tListL);
		
		final CompositeStrategy cs = new CompositeStrategy(scope);
		cs.generate(tListL);
		
		verify(mocktInt, atLeast(15)).generate(tInt);
		verify(mockSizetList, times(4)).generate(tList); // 3 for the size of the list and another one for the assignment
		verify(mocktListL, times(1)).generate(tListL);
	}

}
