package es.uca.webservices.testgen.parsers.xtextspec;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import es.uca.webservices.testgen.api.generators.IRandomStrategy;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Test;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.api.types.TypeDate;
import es.uca.webservices.testgen.api.types.TypeDateTime;
import es.uca.webservices.testgen.api.types.TypeDuration;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTime;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.parsers.spec.xtext.integration.CompositeStrategy;
import es.uca.webservices.testgen.parsers.spec.xtext.integration.XtextSpecParser;

public class XtextSpecParserTest {
	private static final String SPEC_PREFIX = "src/test/resources/Spec/";
	
	private void assertParsedTypesEqualTo(
            final String path, List<IType> expectedTypes)
            throws IOException, ParserException {
        final XtextSpecParser parser = parse(path);
        assertThat(parser.getTypes(), is(equalTo(expectedTypes)));
    }

	private XtextSpecParser parse(final String path) throws ParserException {
        final XtextSpecParser parser = new XtextSpecParser(SPEC_PREFIX + path);
        parser.parse((String)null);
		return parser;
    }
	
    @Test
    public void booleanSpec() throws Exception {
        List<IType> expectedTypes = new ArrayList<IType>();
        List<String> values = new ArrayList<String>();
        values.add("true");
        values.add("false");
        TypeString tString = new TypeString(values);
        expectedTypes.add(tString);

        assertParsedTypesEqualTo("boolean.spec", expectedTypes);
    }

    @Test
    public void integerValues() throws Exception {
        List<IType> expectedTypes = new ArrayList<IType>();
        BigInteger min = new BigInteger("1");
        BigInteger max = new BigInteger("6");
        List<BigInteger> values = new ArrayList<BigInteger>();
        values.add(min);
        values.add(max);
        TypeInt tInt = new TypeInt(values);
        expectedTypes.add(tInt);

        assertParsedTypesEqualTo("intValues.spec", expectedTypes);
    }
    
    @Test
    public void integer() throws Exception {
        List<IType> expectedTypes = new ArrayList<IType>();
        BigInteger min = new BigInteger("1");
        BigInteger max = new BigInteger("6");
        TypeInt tInt = new TypeInt(min, max);
        expectedTypes.add(tInt);

        assertParsedTypesEqualTo("int.spec", expectedTypes);
    }
    
    @Test
    public void negativeInt() throws Exception {
        List<IType> expectedTypes = new ArrayList<IType>();
        BigInteger min = new BigInteger("-50");
        BigInteger max = new BigInteger("-10");
        TypeInt tInt = new TypeInt(min, max);
        expectedTypes.add(tInt);

        assertParsedTypesEqualTo("negative.spec", expectedTypes);
    }

    @Test
    public void integer2() throws Exception {
        List<IType> expectedTypes = new ArrayList<IType>();
        BigInteger min = new BigInteger("0");
        BigInteger max = new BigInteger("100");
        TypeInt tInt = new TypeInt(min, max);
        expectedTypes.add(tInt);

        assertParsedTypesEqualTo("int2.spec", expectedTypes);
    }

    @Test
    public void list() throws Exception {
        List<IType> expectedTypes = new ArrayList<IType>();
        TypeList ListaNoVaciaInt = new TypeList(new TypeInt(), 1);
        expectedTypes.add(ListaNoVaciaInt);

        assertParsedTypesEqualTo("list.spec", expectedTypes);
    }

    @Test
    public void listTypedef() throws Exception {
    	final IType tL = new TypeList(new TypeInt(BigInteger.ZERO, TypeInt.DEFAULT_MAX_VALUE));
    	assertParsedTypesEqualTo("list-typedef.spec", Arrays.asList(tL));
    }

    @Test
    public void tuple() throws Exception {
        List<IType> expectedTypes = new ArrayList<IType>();
        BigInteger min = new BigInteger("1");
        BigInteger max = new BigInteger("6");
        TypeInt CaraDado = new TypeInt(min, max);
        TypeString tString = new TypeString();
        TypeTuple ResultadoTirada = new TypeTuple(tString, CaraDado);
        expectedTypes.add(ResultadoTirada);
        expectedTypes.add(tString);

        assertParsedTypesEqualTo("tuple.spec", expectedTypes);
    }

    @Test
    public void numberFloat() throws Exception {
        TypeFloat tFloat = new TypeFloat();
        tFloat.setFractionDigits(3);
        tFloat.setMinValue(new BigDecimal("3.14"));
        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(tFloat);

        assertParsedTypesEqualTo("float.spec", expectedTypes);
    }
    
    @Test
    public void negativeFloat() throws Exception {
        TypeFloat tFloat = new TypeFloat();
        tFloat.setMinValue(new BigDecimal("-50.3"));
        tFloat.setMaxValue(new BigDecimal("-10.8"));
        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(tFloat);

        assertParsedTypesEqualTo("negativeFloat.spec", expectedTypes);
    }
    
    @Test
	public void rangeIntFloat() throws Exception {    
	    TypeFloat tFloat = new TypeFloat();
	    tFloat.setMinValue(new BigDecimal("0"));
	    tFloat.setMaxValue(new BigDecimal("30000"));
	    List<IType> expectedTypes = new ArrayList<IType>();
	    expectedTypes.add(tFloat);
	
	    assertParsedTypesEqualTo("floatWithInt.spec", expectedTypes);
	}

	@Test
    public void floatValues() throws Exception {
        List<IType> expectedTypes = new ArrayList<IType>();
        BigDecimal min = new BigDecimal("1.5");
        BigDecimal max = new BigDecimal("6.8");
        List<BigDecimal> values = new ArrayList<BigDecimal>();
        values.add(min);
        values.add(max);
        TypeFloat tFloat = new TypeFloat(values);
        expectedTypes.add(tFloat);

        assertParsedTypesEqualTo("floatValues.spec", expectedTypes);
    }

    @Test
    public void stringPalcttern() throws Exception {
        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(new TypeString("[0-9]{7}[a-zA-Z]"));
        expectedTypes.add(new TypeString("[0-9A-Z]{1,3}(\\.[A-Z]{3}(\\.X){0,1}){0,1}"));
        expectedTypes.add(new TypeString("\u0200\u0205*"));
        expectedTypes.add(new TypeString("\u0007\u003f\u01ff"));
        final TypeString tString = new TypeString();
        tString.setMaxLength(32);
        tString.setMinLength(1);
        expectedTypes.add(tString);

        assertParsedTypesEqualTo("string.spec", expectedTypes);
    }

    @Test
    public void time() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar time1 = factory.newXMLGregorianCalendarTime(0, 0, 0, DatatypeConstants.FIELD_UNDEFINED);
        XMLGregorianCalendar time2 = factory.newXMLGregorianCalendarTime(12, 59, 59, DatatypeConstants.FIELD_UNDEFINED);
        TypeTime tTime = new TypeTime(time1, time2);
        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(tTime);

        assertParsedTypesEqualTo("time.spec", expectedTypes);
    }

    @Test
    public void date() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar date1 = factory.newXMLGregorianCalendarDate(2011, 1, 1, DatatypeConstants.FIELD_UNDEFINED);
        XMLGregorianCalendar date2 = factory.newXMLGregorianCalendarDate(2011, 1, 31, DatatypeConstants.FIELD_UNDEFINED);
        TypeDate tDate = new TypeDate(date1, date2);
        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(tDate);

        assertParsedTypesEqualTo("date.spec", expectedTypes);
    }

    @Test
    public void dateTime() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        XMLGregorianCalendar dateTime1 = factory.newXMLGregorianCalendar("2011-01-01T00:00:00");
        XMLGregorianCalendar dateTime2 = factory.newXMLGregorianCalendar("2011-01-31T23:59:59");
        TypeDateTime tDateTime = new TypeDateTime(dateTime1, dateTime2);
        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(tDateTime);

        assertParsedTypesEqualTo("dateTime.spec", expectedTypes);
    }
    
    @Test
    public void listDateTime() throws Exception {
        TypeList tList = new TypeList(new TypeDateTime(),0,1);
        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(tList);

        assertParsedTypesEqualTo("listDateTime.spec", expectedTypes);
    }
    
    @Test
    public void duration() throws Exception {
        final DatatypeFactory factory = DatatypeFactory.newInstance();
        Duration duration1 = factory.newDuration("P11Y1M1D");
        Duration duration2 = factory.newDuration("P11Y1M31D");
        TypeDuration tDuration = new TypeDuration(duration1, duration2);
        List<IType> expectedTypes = new ArrayList<IType>();
        expectedTypes.add(tDuration);

        assertParsedTypesEqualTo("duration.spec", expectedTypes);
    }

    @Test
    public void reverseOrder() throws Exception {
        List<IType> expectedTypes = new ArrayList<IType>();
        BigInteger min = new BigInteger("0");
        BigInteger max = new BigInteger("100");
        TypeInt tInt = new TypeInt(min, max);
        tInt.setTotalDigits(3);
        expectedTypes.add(tInt);

        assertParsedTypesEqualTo("reverseOrder.spec", expectedTypes);
    }    

    @Test
    public void empty() throws Exception {
        final List<IType> expected = new ArrayList<IType>();
        assertParsedTypesEqualTo("empty.spec", expected);
    }

    @Test(expected = ParserException.class)
    public void exceptionTypedef() throws Exception {
        parse("exceptionTypedef.spec");
    }

    @Test(expected = ParserException.class)
    public void exceptionDeclaration() throws Exception {
        parse("exceptionDeclaration.spec");
    }
    
    @Test
    public void noException() throws Exception {
        parse("data.spec");
    }
    
    @Test
    public void noExceptionWithStrategy() throws Exception {
        final XtextSpecParser parser = parse("strategy.spec");
        IRandomStrategy strategy = (IRandomStrategy) parser.getStrategy("UnDadoTrucado");
        strategy.setPRNG(new Random());
        assert(strategy instanceof CompositeStrategy);
        Object[] generated = strategy.generate(parser.getTypes().toArray(new IType[0]));
        System.out.println(generated);
    }

	@Test
	public void noExceptionWithStrategyParameters() throws Exception {
		final XtextSpecParser parser = parse("strategyWithParameter.spec");
        IRandomStrategy strategy = (IRandomStrategy) parser.getStrategy("listaGaussian");
		strategy.setPRNG(new Random());
		assert (strategy instanceof CompositeStrategy);
		Object[] generated = strategy.generate(parser.getTypes().toArray(new IType[0]));

		assertTrue (generated[0] instanceof List);// list of element generated
        List<List<?>> lList = (List<List<?>>) generated[0];

		assertTrue (lList.get(0) instanceof List);// tuple
        for (List<?> l : lList) {
            assertTrue(l.get(0) instanceof BigDecimal);
            assertTrue(l.get(1) instanceof BigInteger);
		}
	}

	@Test
    public void noExceptionStrategyTupleList() throws Exception {
    	// Use a cheap PRNG for unit tests
        final XtextSpecParser parser = parse("strategyTupleList.spec");
        IRandomStrategy strategy = (IRandomStrategy) parser.getStrategy("listaGaussian");
        strategy.setPRNG(new Random());

        assert(strategy instanceof CompositeStrategy);
        Object[] generated = strategy.generate(parser.getTypes().toArray(new IType[0]));

        assertTrue(((Object[])generated[0])instanceof Object[]);//tuple
        Object[] tuple = (Object[]) generated[0];

        assertTrue(tuple[0] instanceof BigDecimal);//float
        assertTrue(tuple[1] instanceof List);//list
        List<BigInteger> lTuple = (List<BigInteger>) tuple[1];
        assertTrue(lTuple.get(0) instanceof BigInteger);//list
    }
    
    @Test
    public void noExceptionStrategyListOfList() throws Exception {
    	// Use a cheap PRNG for unit tests
        final XtextSpecParser parser = parse("strategyListOfList.spec");
        IRandomStrategy strategy = (IRandomStrategy) parser.getStrategy("listaGaussian");
        strategy.setPRNG(new Random());

        assert(strategy instanceof CompositeStrategy);
        Object[] generated = strategy.generate(parser.getTypes().toArray(new IType[0]));
        assertTrue(generated[0] instanceof List);//list

        final List<List<BigInteger>> lList = (List<List<BigInteger>>) generated[0];
        assertTrue(lList.get(0) instanceof List);
		assertTrue(lList.get(0).get(0) instanceof BigInteger);//int
    }

    @Test
    public void specGoogleResult() throws Exception {
        List<IType> expectedTypes = new ArrayList<IType>();
        final TypeString tStringDomain = new TypeString("[a-z]{1,3}[.](com|org)");
        final TypeTuple tTupleGoogleResult = new TypeTuple(tStringDomain);
        final TypeList tListGoogleResults = new TypeList(tTupleGoogleResult,0,30);
        
        expectedTypes.add(tListGoogleResults);

        assertParsedTypesEqualTo("dataGoogleResult.spec", expectedTypes);
    }

    @Test
    public void typedefNamesArePreserved() throws Exception {
    	final XtextSpecParser parser = parse("LoanApprovalTemplates.spec");
    	final IType typeReq = parser.getTypes().get(0);
    	assertEquals(Arrays.asList("Req", "Int"), typeReq.getNamesTypes());
	}
}
