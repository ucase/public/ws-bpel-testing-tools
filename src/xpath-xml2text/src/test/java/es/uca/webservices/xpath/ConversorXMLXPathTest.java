package es.uca.webservices.xpath;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.beans.Visibility;
import java.io.FileNotFoundException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import es.uca.webservices.xpath.parser.ASTExpression;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Tests for the XPath-XML-XPath roundtrip transformation chain.
 *
 * @author Antonio García-Domínguez
 */
public class ConversorXMLXPathTest {

	@Test
	public void variable() throws Exception {
		assertRoundtripPreservesXPath("$x");
	}

	@Test
	public void variablePart() throws Exception {
		assertRoundtripPreservesXPath("$x.y");
	}

	@Test
	public void implicitChildAxesQualified() throws Exception {
		assertRoundtripPreservesXPath("x:y");
	}

	@Test
	public void implicitChildAxesUnqualified() throws Exception {
		assertRoundtripPreservesXPath("y");
	}

	@Test
	public void childAxesQualified() throws Exception {
		assertRoundtripPreservesXPath("child::x:y");
	}

	@Test
	public void childAxesUnqualified() throws Exception {
		assertRoundtripPreservesXPath("child::y");
	}

	@Test
	public void attributeAxesQualified() throws Exception {
		assertRoundtripPreservesXPath("@x:y");
	}

	@Test
	public void attributeAxesUnqualified() throws Exception {
		assertRoundtripPreservesXPath("@y");
	}

	@Test
	public void tradeIngress() throws Exception {
		assertRoundtripPreservesXPath("$EstablishmentOperationIn2.earnings/section[$counter]/@name");
	}

    @Test
    public void badExpressionProducesEmptyDocument() throws Exception {
        final Document doc = ConversorXMLXPath.safeXPath2DOM("i am error");
        final Element elem = doc.getDocumentElement();
        assertEquals("expression", elem.getLocalName());
        assertEquals(XPathXMLGeneratorVisitor.UCA_NAMESPACE, elem.getNamespaceURI());
        assertEquals(0, elem.getChildNodes().getLength());
    }

	private void assertRoundtripPreservesXPath(final String expr)
			throws ParserConfigurationException, FileNotFoundException,
			TransformerException {
		assertEquals(expr, ConversorXMLXPath.roundtrip(expr));
	}
}
