<?xml version="1.0" encoding="UTF-8"?>
<!-- 
	
	Hoja de conversión de AST XPath XML a cadena.
	Antonio García Domínguez
	$Id$
	
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:uca="http://www.uca.es/xpath/2007/11">

	<xsl:output      method="text" />
	<xsl:strip-space elements="*" />
	
	<!-- Necesaria, pues hemos desactivado la regla por defecto -->
	<xsl:template match="uca:expression">
	  <xsl:apply-templates/>
	</xsl:template>

	<!-- Reglas para operadores unarios -->
	<xsl:template match="uca:neg">
		<xsl:text>-</xsl:text>
		<xsl:apply-templates select="*[1]" />
	</xsl:template>

	<!-- Regla para llamadas a función -->
	<xsl:template match="uca:functionCall">
		<xsl:apply-templates select="*[1]" />
		<xsl:text>(</xsl:text>
		<xsl:apply-templates select="*[2]" />
		<xsl:for-each select="*[position()>2]">
			<xsl:text>, </xsl:text>
			<xsl:apply-templates select="." />
		</xsl:for-each>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<!-- Regla para los operadores binarios normales -->
	<xsl:template
		match="uca:or|uca:and|uca:eq|uca:neq|uca:lt|uca:gt|uca:le|uca:ge|uca:add|uca:sub|uca:mul|uca:div|uca:mod|uca:union">
		<xsl:text>(</xsl:text>
		<xsl:apply-templates select="*[1]" />
		<xsl:choose>
			<xsl:when test="local-name(.)='or'">
				<xsl:text> or </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='and'">
				<xsl:text> and </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='eq'">
				<xsl:text> = </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='neq'">
				<xsl:text> != </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='lt'">
				<xsl:text> &lt; </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='gt'">
				<xsl:text> &gt; </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='le'">
				<xsl:text> &lt;= </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='ge'">
				<xsl:text> &gt;= </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='add'">
				<xsl:text> + </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='sub'">
				<xsl:text> - </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='mul'">
				<xsl:text> * </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='div'">
				<xsl:text> div </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='mod'">
				<xsl:text> mod </xsl:text>
			</xsl:when>
			<xsl:when test="local-name(.)='union'">
				<xsl:text> | </xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:apply-templates select="*[2]" />
		<xsl:text>)</xsl:text>
	</xsl:template>

	<!-- Reglas para operadores de camino -->
	<xsl:template match="uca:children">
		<xsl:apply-templates select="*[1]" />
		<xsl:text>/</xsl:text>
		<xsl:apply-templates select="*[2]" />
	</xsl:template>
	<xsl:template match="uca:alldesc">
		<xsl:apply-templates select="*[1]" />
		<xsl:text>//</xsl:text>
		<xsl:apply-templates select="*[2]" />
	</xsl:template>

	<xsl:template match="uca:axisSelector[*[1]/@name = 'attribute']">
		<xsl:text>@</xsl:text>
		<xsl:apply-templates select="*[2]" />
	</xsl:template>
	<xsl:template match="uca:axisSelector[*[1]/@name != 'attribute']">
		<xsl:value-of select="*[1]/@name" />
		<xsl:text>::</xsl:text>
		<xsl:apply-templates select="*[2]" />
	</xsl:template>

	<xsl:template match="uca:root">
		<xsl:if test="not(../uca:alldesc | ../uca:children)">
			<xsl:text>/</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="uca:currentNode">
		<xsl:text>.</xsl:text>
	</xsl:template>
	<xsl:template match="uca:parentNode">
		<xsl:text>..</xsl:text>
	</xsl:template>

	<!-- Reglas para predicados de nodo -->
	<xsl:template match="uca:literalProcessingInstruction">
		<xsl:value-of
			select="concat('processing-instruction(', @instruction, ')')" />
	</xsl:template>
	<xsl:template match="uca:nodeType">
		<xsl:value-of select="concat(@type, '()')" />
	</xsl:template>
	<xsl:template match="uca:onlyTestNamespace">
		<xsl:value-of select="concat(@prefix, ':*')" />
	</xsl:template>
	<xsl:template match="uca:onlyTestNamespace">
		<xsl:value-of select="concat(@prefix, ':*')" />
	</xsl:template>
	<xsl:template match="uca:anyNode">
		<xsl:text>*</xsl:text>
	</xsl:template>
	<xsl:template match="uca:predicate">
		<xsl:apply-templates select="*[1]" />
		<xsl:text>[</xsl:text>
		<xsl:apply-templates select="*[2]" />
		<xsl:text>]</xsl:text>
	</xsl:template>

	<!-- Reglas para valores atómicos -->
	<xsl:template match="uca:variableReference">
		<xsl:text>$</xsl:text>
		<xsl:apply-templates select="*[1]" />
	</xsl:template>
	<xsl:template match="uca:stringConstant|uca:numberConstant">
		<xsl:value-of select="string(.)" />
	</xsl:template>
	<xsl:template match="uca:qname">
		<xsl:choose>
			<xsl:when
				test="string-length(normalize-space(@prefix))>0">
				<xsl:value-of select="concat(@prefix, ':', @localPart)" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="@localPart" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- Desactivamos la regla por defecto -->	
	<xsl:template match="*"/>
	
</xsl:stylesheet>
