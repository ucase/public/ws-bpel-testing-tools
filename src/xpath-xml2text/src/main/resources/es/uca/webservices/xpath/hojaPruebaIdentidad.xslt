<?xml version="1.0" encoding="UTF-8"?>
<!--
    Hoja de mutación de expresiones XPath
    Antonio García Domínguez
    $Id$
    
    Esta hoja recibe un árbol XML de la forma <expression>(expresión en forma de cadena)</expression> y
    devuelve otra cadena con la expresión XPath mutada.
-->
<xsl:stylesheet version="2.0" 
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:uca="http://www.uca.es/xpath/2007/11" 
                xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform"
                extension-element-prefixes="conv"
                exclude-result-prefixes="conv"
                xmlns:conv="xalan://es.uca.webservices.xpath.ConversorXMLXPath">
                
   <xsl:output method="text"/>
   <xsl:strip-space elements="*"/>
   
   <xsl:template match="/">     
     <xsl:variable name="arbolInstr">
       <xsl:apply-templates select="conv:xpath2XML(normalize-space(string(expression)))" mode="xpath"/>
     </xsl:variable>
     <xsl:value-of select="conv:xml2XPath($arbolInstr)"/>
   </xsl:template>
   
   <!-- Regla identidad para la mayoría de los elementos -->
   <xsl:template match="node()|@*" mode="xpath">   
     <xsl:copy>
       <xsl:apply-templates select="node()|@*" mode="xpath"/>
     </xsl:copy>
   </xsl:template>
  
</xsl:stylesheet>
