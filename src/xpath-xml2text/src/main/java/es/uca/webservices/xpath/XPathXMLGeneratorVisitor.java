package es.uca.webservices.xpath;

import es.uca.webservices.xpath.parser.ASTAdd;
import es.uca.webservices.xpath.parser.ASTAllDescendants;
import es.uca.webservices.xpath.parser.ASTAnd;
import es.uca.webservices.xpath.parser.ASTAnyNode;
import es.uca.webservices.xpath.parser.ASTAxis;
import es.uca.webservices.xpath.parser.ASTAxisSelector;
import es.uca.webservices.xpath.parser.ASTChildren;
import es.uca.webservices.xpath.parser.ASTCurrentNode;
import es.uca.webservices.xpath.parser.ASTDiv;
import es.uca.webservices.xpath.parser.ASTEqual;
import es.uca.webservices.xpath.parser.ASTExpression;
import es.uca.webservices.xpath.parser.ASTFunctionCall;
import es.uca.webservices.xpath.parser.ASTGreaterEqual;
import es.uca.webservices.xpath.parser.ASTGreaterThan;
import es.uca.webservices.xpath.parser.ASTLessEqual;
import es.uca.webservices.xpath.parser.ASTLessThan;
import es.uca.webservices.xpath.parser.ASTLiteralProcessingInstruction;
import es.uca.webservices.xpath.parser.ASTMod;
import es.uca.webservices.xpath.parser.ASTMult;
import es.uca.webservices.xpath.parser.ASTNeg;
import es.uca.webservices.xpath.parser.ASTNodeType;
import es.uca.webservices.xpath.parser.ASTNotEqual;
import es.uca.webservices.xpath.parser.ASTNumberConstant;
import es.uca.webservices.xpath.parser.ASTOnlyTestNamespaceNode;
import es.uca.webservices.xpath.parser.ASTOr;
import es.uca.webservices.xpath.parser.ASTParentNode;
import es.uca.webservices.xpath.parser.ASTPredicate;
import es.uca.webservices.xpath.parser.ASTQName;
import es.uca.webservices.xpath.parser.ASTRoot;
import es.uca.webservices.xpath.parser.ASTStringConstant;
import es.uca.webservices.xpath.parser.ASTSubtract;
import es.uca.webservices.xpath.parser.ASTUnion;
import es.uca.webservices.xpath.parser.ASTVariableReference;
import es.uca.webservices.xpath.parser.ParseException;
import es.uca.webservices.xpath.parser.SimpleNode;
import es.uca.webservices.xpath.parser.XPathParser;
import es.uca.webservices.xpath.parser.XPathParserVisitor;

import org.w3c.dom.*;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.*;

/**
 * This class generates a XML tree from the AST of a XPath expression.
 * This way, a XPath expression can be converted into a XML tree, modify it using
 * XSLT, and then return it to the string form with other XSLT sheet.
 *
 * The usual form of using this class is:
 * 1. Create an instance
 * 2. Invoke visit() over the root node of the AST, with null as user data. This root
 *     node will be always of type ASTExpression, due to the grammas, and it will be
 *     the only one of that type.
 * 3. Recover the document builded this way with getDocument()
 * 
 * @author Antonio García Domínguez
 * @version 1.0
 */
public class XPathXMLGeneratorVisitor implements XPathParserVisitor {
	/** Document to be built */
	private Document doc;

	/** Internal counter used to generate unique IDs for each node */
	private int contador = 0;

	/** Namespace for all elements */
	public static final String UCA_NAMESPACE = "http://www.uca.es/xpath/2007/11";

	/**
         * Testing code: it takes a string, analyzes it, passes it to XML and print the XML tree
	 * 
	 * @throws IOException                                    I/O Error
	 * @throws ParserConfigurationException    An error occurred while configuring the XML analyzer
	 */
	public static void main(String[] args) throws IOException, ParseException,
			ParserConfigurationException {
		BufferedReader is = new BufferedReader(new InputStreamReader(System.in));

		try {
			String linea = "";
			do {
				System.out
						.print("Introduce una expresión XPath y pulsa Intro (línea vacía termina): ");
				linea = is.readLine();
				if (linea.length() == 0) {
					continue;
				}

				try {
					XPathParser parser = new XPathParser(
							new StringReader(linea));
					ASTExpression expr = parser.Start();
					XPathXMLGeneratorVisitor v = new XPathXMLGeneratorVisitor();
					v.visit(expr, null);
					Document doc = v.getDocument();
					volcarXML(doc, System.out);

				} catch (ParseException ex) {
					System.err
							.println("No es una expresión XPath válida: fallo al analizar");
				}
			} while (linea.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

        /**
         * Constructor
         * @throws ParserConfigurationException
         */
	public XPathXMLGeneratorVisitor() throws ParserConfigurationException {
		final DocumentBuilderFactory docB = DocumentBuilderFactory
				.newInstance();
		docB.setNamespaceAware(true);
		doc = docB.newDocumentBuilder().newDocument();
	}

        /**
         * Gets the resulting document
         * @return  The resulting document
         */
	public Document getDocument() {
		return doc;
	}

        /**
         * This call usually will not be called. It does nothing.
         * @param node
         * @param data
         * @return
         */
	public Element visit(SimpleNode node, Object data) {
		// Aquí no tiene sentido que hagamos nada: de todas formas,
		// este método normalmente no será nunca llamado.
		return null;
	}

        /**
         * Visits the node passed as an argument
         * @param node      The node to ve visited
         * @param data       User data
         * @return                A new element associated with this node
         */
	public Element visit(ASTExpression node, Object data) {
		/*
		 * Éste es el único elemento que realmente tiene efectos secundarios:
		 * los demás se limitan a hacer un recorrido postfijo del árbol e ir
		 * creando un árbol cada vez más completo.
		 */
		Element eExpression = opPorDefecto(node, "expression");

		/**
		 * A partir de ahora ya podremos obtener el documento completo con
		 * getDocument().
		 */
		doc.appendChild(eExpression);

		return eExpression;
	}

        /**
         *  Visits a OR node
         * @param node
         * @param data
         * @return
         */
	public Element visit(ASTOr node, Object data) {
		return opPorDefecto(node, "or");
	}

        /**
         * Visits a AND node
         * @param node
         * @param data
         * @return
         */
	public Element visit(ASTAnd node, Object data) {
		return opPorDefecto(node, "and");
	}

        /**
         * Visits a EQUAL node
         * @param node
         * @param data
         * @return
         */
	public Element visit(ASTEqual node, Object data) {
		return opPorDefecto(node, "eq");
	}

        /**
         * Visits a NOTEQUAL node
         * @param node
         * @param data
         * @return
         */
	public Element visit(ASTNotEqual node, Object data) {
		return opPorDefecto(node, "neq");
	}

        /**
         * Visits a LESSTHAN node
         * @param node
         * @param data
         * @return
         */
	public Element visit(ASTLessThan node, Object data) {
		return opPorDefecto(node, "lt");
	}

        /**
         * Visits a GREATERTHAN node
         * @param node
         * @param data
         * @return
         */
	public Element visit(ASTGreaterThan node, Object data) {
		return opPorDefecto(node, "gt");
	}

        /**
         * Visits a LESSEQUAL node
         * @param node
         * @param data
         * @return
         */
	public Element visit(ASTLessEqual node, Object data) {
		return opPorDefecto(node, "le");
	}

        /**
         * Visits a GREATEREQUAL node
         * @param node
         * @param data
         * @return
         */
	public Element visit(ASTGreaterEqual node, Object data) {
		return opPorDefecto(node, "ge");
	}

	public Element visit(ASTAdd node, Object data) {
		return opPorDefecto(node, "add");
	}

	public Element visit(ASTSubtract node, Object data) {
		return opPorDefecto(node, "sub");
	}

	public Element visit(ASTMult node, Object data) {
		return opPorDefecto(node, "mul");
	}

	public Element visit(ASTDiv node, Object data) {
		return opPorDefecto(node, "div");
	}

	public Element visit(ASTMod node, Object data) {
		return opPorDefecto(node, "mod");
	}

	public Element visit(ASTNeg node, Object data) {
		return opPorDefecto(node, "neg");
	}

	public Element visit(ASTUnion node, Object data) {
		return opPorDefecto(node, "union");
	}

	public Element visit(ASTChildren node, Object data) {
		return opPorDefecto(node, "children");
	}

	public Element visit(ASTAllDescendants node, Object data) {
		return opPorDefecto(node, "alldesc");
	}

	public Element visit(ASTRoot node, Object data) {
		return opPorDefecto(node, "root");
	}

	public Element visit(ASTAxisSelector node, Object data) {
		return opPorDefecto(node, "axisSelector");
	}

	public Element visit(ASTPredicate node, Object data) {
		return opPorDefecto(node, "predicate");
	}

	public Element visit(ASTAxis node, Object data) {
		Element eAxis = crearElemento("axis");
		eAxis.setAttribute("name", node.getAxis());
		agregarHijos(node, eAxis);
		return eAxis;
	}

	public Element visit(ASTCurrentNode node, Object data) {
		return opPorDefecto(node, "currentNode");
	}

	public Element visit(ASTParentNode node, Object data) {
		return opPorDefecto(node, "parentNode");
	}

	public Element visit(ASTLiteralProcessingInstruction node, Object data) {
		Element eLPI = crearElemento("literalProcessingInstruction");
		eLPI.setAttribute("instruction", node.getInstruction());
		agregarHijos(node, eLPI);
		return eLPI;
	}

	public Element visit(ASTNodeType node, Object data) {
		Element eNT = crearElemento("nodeType");
		eNT.setAttribute("type", node.getType());
		agregarHijos(node, eNT);
		return eNT;
	}

	public Element visit(ASTOnlyTestNamespaceNode node, Object data) {
		Element e = crearElemento("onlyTestNamespace");
		e.setAttribute("prefix", node.getPrefix());
		agregarHijos(node, e);
		return e;
	}

	public Element visit(ASTAnyNode node, Object data) {
		return opPorDefecto(node, "anyNode");
	}

	public Element visit(ASTVariableReference node, Object data) {
		return opPorDefecto(node, "variableReference");
	}

	public Element visit(ASTStringConstant node, Object data) {
		Element e = crearElemento("stringConstant");
		e.setTextContent(node.getValue());
		return e;
	}

	public Element visit(ASTNumberConstant node, Object data) {
		Element e = crearElemento("numberConstant");
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("##########0.0##", dfs);
		e.setTextContent(df.format(node.getValue()));
		return e;
	}

	public Element visit(ASTFunctionCall node, Object data) {
		return opPorDefecto(node, "functionCall");
	}

	public Element visit(ASTQName node, Object data) {
		Element e = crearElemento("qname");
		e.setAttribute("prefix", node.getPrefix());
		e.setAttribute("localPart", node.getLocalPart());
		return e;
	}

        /**
         * Creates a new element with the UCA namespace, the specified localpart and an incrementing ID
         * @param localPart The localpart of the element
         * @return
         */
	private Element crearElemento(String localPart) {
		Element e = doc.createElementNS(UCA_NAMESPACE, "uca:" + localPart);
		e.setAttribute("id", String.format("%d", contador++));
		return e;
	}

        /**
         * Adds the children of the node to the element
         * @param node                  The node whose children will be added
         * @param eExpression     The element where the nodes will be appended
         */
	private void agregarHijos(final SimpleNode node, final Element eExpression) {
		for (int i = 0; i < node.jjtGetNumChildren(); ++i) {
			Element eHijo = (Element) node.jjtGetChild(i).jjtAccept(this, null);
			eExpression.appendChild(eHijo);
		}
	}

        /**
         * Creates a new node with the name "localPart" and adds the node children as children
         * @param node              The node to be added as a child
         * @param localPart       The name of the element
         * @return
         */
	private Element opPorDefecto(SimpleNode node, String localPart) {
		Element e = crearElemento(localPart);
		agregarHijos(node, e);
		return e;
	}

	/**
         * Dumps a DOM tree into a file using the default XSLT transformation
	 * 
	 * @throws TransformerFactoryConfigurationError        An error occurred while configuring the transformer
	 * @throws TransformerException                                      An error occurred while serializing the XML tree
	 * @throws IOException                                                        I/O error
	 */
	private static void volcarXML(Document doc, OutputStream os)
			throws TransformerFactoryConfigurationError, TransformerException,
			IOException {
		DOMSource origen = new DOMSource(doc);
		StreamResult destino = new StreamResult(os);

		Transformer serializador = TransformerFactory.newInstance()
				.newTransformer();
		serializador.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		serializador.setOutputProperty(OutputKeys.INDENT, "yes");
		serializador.transform(origen, destino);
	}

}
