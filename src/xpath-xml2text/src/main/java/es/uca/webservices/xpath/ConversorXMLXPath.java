package es.uca.webservices.xpath;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.AugmentedSource;
import net.sf.saxon.Controller;
import net.sf.saxon.TransformerFactoryImpl;
import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.om.NodeInfo;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.Whitespace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import es.uca.webservices.xpath.parser.ASTExpression;
import es.uca.webservices.xpath.parser.ParseException;
import es.uca.webservices.xpath.parser.XPathParser;

/**
 * This class exposes the XML&lt;-&gt;XPath conversion functions to be used from XPath 2.0 and XSLT 2.0
 * and from Java.
 * 
 * @author Antonio García Domínguez
 * @version 1.2
 */
public class ConversorXMLXPath {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConversorXMLXPath.class);
	private static final String PATH_XSLT = "es/uca/webservices/xpath/hojaXML2XPath.xslt";
	private static final String PATH_ROUNDTRIP_XSLT = "es/uca/webservices/xpath/hojaPruebaIdentidad.xslt";

	private static Templates xml2XPathTranslet = null, testTranslet = null;
	private static TransformerFactoryImpl xml2XPathFactory = null, testFactory = null;

	/**
	 * Test code. It converts a string into XML and then the way back,
	 * everything in the same XSLT sheet.
	 * 
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		final BufferedReader is = new BufferedReader(new InputStreamReader(
				System.in));
		String line = "";
		do {
			System.out.print("Enter an XPath expression, or end with an empty line: ");
			line = is.readLine();
			if (line.length() == 0)
				continue;

			try {
				// First, dump to an XML file
				Source s = xpath2XML(null, line);
				System.out.println("Intermediate XML tree:");
				dumpXML(s, System.out);

				// Ahora el resultado de cadena->XML->cadena
				System.out.println("Roundtrip result: " + roundtrip(line));

			} catch (Exception e) {
				e.printStackTrace();
			}
		} while (line.length() > 0);
	}

	static String roundtrip(String sXPathExpression) throws ParserConfigurationException, FileNotFoundException, TransformerException {
		final Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		final Element eExpression = doc.createElement("expression");
		eExpression.setTextContent(sXPathExpression);
		doc.appendChild(eExpression);
		final DOMSource source = new DOMSource(doc);

		final StringWriter sw = new StringWriter();
		final StreamResult result = new StreamResult(sw);
		final Transformer t = initTestTranslet().newTransformer();
		t.transform(((Controller) t).getConfiguration().buildDocument(source), result);

		return sw.toString();
	}

	/**
	 * Converts a XPath string into a XML tree representing its AST. This is a
	 * version of {@link #xpath2DOM(String)} prepared to be called from the
	 * XPath 2.0 engine inside Saxon-B 9.1.
	 * 
	 * @param sXPath
	 *            XPath expression
	 * @return The resulting DOM Tree, or an empty document if parsing failed.
	 * @throws ParserConfigurationException
	 *             An error occurred when the analyzer was being configured
	 * @throws TransformerException
	 *             An error occurred during the generation of the XML Tree
	 */
	public static Source xpath2XML(XPathContext context, final String sXPath)
			throws ParserConfigurationException, TransformerException {
        final Document document = safeXPath2DOM(sXPath);
        final Source source = new DOMSource(document.getDocumentElement());
        if (context != null) {
            // Adaptamos el resultado a la Configuration de la hoja que lo recibe
            return context.getController().getConfiguration().buildDocument(source);
        } else {
            return source;
        }
    }

    /**
     * Safer version of {@link #xpath2DOM(String)} that returns an empty document if it fails
     * to be parsed as an XPath expression.
     */
    public static Document safeXPath2DOM(String sXPath) throws ParserConfigurationException {
        try {
    		return xpath2DOM(sXPath);
        } catch (ParseException ex) {
            LOGGER.warn("Could not convert '{}' from XPath 1.0 to XML: returning an empty document", sXPath, ex);
            final XPathXMLGeneratorVisitor v = new XPathXMLGeneratorVisitor();
            v.visit(new ASTExpression(1), null);
            return v.getDocument();
        }
    }

    /**
	 * Parses an XPath expression into an AST, represented by a DOM tree.
	 * 
	 * @param sXPath
	 *            String with the XPath expression to be parsed.
	 * @throws ParseException
	 *             An error occurred while analyzing the input
	 * @throws ParserConfigurationException
	 *             An error occurred when the analyzer was being configured
	 */
	public static Document xpath2DOM(final String sXPath)
			throws ParseException, ParserConfigurationException {
		XPathParser parser = new XPathParser(new StringReader(sXPath));
		ASTExpression expr = parser.Start();
		XPathXMLGeneratorVisitor v = new XPathXMLGeneratorVisitor();
		v.visit(expr, null);
		final Document document = v.getDocument();
		return document;
	}

	/**
	 * Converts a XML tree with the AST into a XPath expression
	 * 
	 * @param source
	 *            Root node of the XML tree (document element)
	 * @return A string that represents the specified AST
	 * @throws FileNotFoundException
	 *             The XSLT sheet could not be founded. This is an internal
	 *             error.
	 * @throws TransformerException
	 *             An error occurred while applying the XSLT sheet
	 * @throws ParserConfigurationException
	 *             An error occurred while copying the original tree
	 */
	public static String xml2XPath(NodeInfo source)
			throws FileNotFoundException, TransformerException,
			ParserConfigurationException {

		final StringWriter swExpression = new StringWriter();
		StreamResult result = new StreamResult(swExpression);
		final Transformer t = initTransletXML2XPath().newTransformer();

		// Adapt the argument to our stylesheet's Configuration
		AugmentedSource as = AugmentedSource.makeAugmentedSource(source);
		as.setStripSpace(Whitespace.ALL);
		source = ((Controller) t).getConfiguration().buildDocument(as);
		t.transform(source, result);

		return swExpression.toString();
	}

	/**
	 * Initializes the translet used to convert the XML into XPath
	 * 
	 * @return
	 * @throws TransformerConfigurationException
	 * @throws XPathException
	 */
	private static Templates initTransletXML2XPath()
			throws TransformerConfigurationException, XPathException {

		if (xml2XPathTranslet != null) {
			return xml2XPathTranslet;
		}

		final InputStream streamXSLT = new BufferedInputStream(
				ConversorXMLXPath.class.getClassLoader().getResourceAsStream(
						PATH_XSLT));
		xml2XPathFactory = (TransformerFactoryImpl) TransformerFactory.newInstance(
				"net.sf.saxon.TransformerFactoryImpl",
				Thread.currentThread().getContextClassLoader());
		xml2XPathTranslet = xml2XPathFactory.newTemplates(new StreamSource(streamXSLT));
		return xml2XPathTranslet;
	}

	/**
	 * Initializes the translet used for testing
	 * 
	 * @return
	 * @throws FileNotFoundException
	 * @throws TransformerConfigurationException
	 * @throws XPathException
	 */
	private static Templates initTestTranslet() throws FileNotFoundException,
			TransformerConfigurationException, XPathException {
		if (testTranslet != null) {
			return testTranslet;
		}

		final InputStream streamXSLT = new BufferedInputStream(
				ConversorXMLXPath.class.getClassLoader().getResourceAsStream(
						PATH_ROUNDTRIP_XSLT));
		testFactory = (TransformerFactoryImpl) TransformerFactory.newInstance();
		testTranslet = testFactory.newTemplates(new StreamSource(streamXSLT));
		return testTranslet;
	}

	/**
	 * Dumps a DOM tree into a file using the default XSLT transformation
	 * 
	 * @throws TransformerFactoryConfigurationError
	 *             An error occurred while configuring the transformer.
	 * @throws TransformerException
	 *             An error occurred while serializing the XML tree
	 * @throws IOException
	 *             I/O error
	 * @throws ParserConfigurationException
	 */
	private static void dumpXML(Source origen, OutputStream os)
			throws TransformerFactoryConfigurationError, TransformerException,
			IOException, ParserConfigurationException {

		StreamResult dest = new StreamResult(os);
		Transformer serializer = TransformerFactory.newInstance().newTransformer();
		serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		serializer.setOutputProperty(OutputKeys.INDENT, "yes");
		serializer.transform(origen, dest);
	}
}
