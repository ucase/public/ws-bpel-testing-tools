#!/usr/bin/perl
#
# Maven integration for takuan-perl
# Copyright (C) 2011 Antonio García-Domínguez
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

use strict;
use warnings;
use English;
use Config;
use lib 'lib';

use Daikon qw($VERSION);

# Configuration ################################################################

my $tgzPath  = "Takuan-$VERSION.tar.gz";
my $perlPath = "takuan-perl.pl";
my $parPath  = "takuan.par";
my $execPath = "takuan-perl";
if ($OSNAME =~ /^(?:MSWin|cygwin|OS2)/) {
    $execPath .= ".exe";
}

my $deployFileGoal = "org.apache.maven.plugins:maven-deploy-plugin:2.7:deploy-file";
my $repositoryId  = "neptuno-snapshots";
my $repositoryURL = "https://neptuno.uca.es/nexus/content/repositories/snapshots";
my $versionSuffix = "-SNAPSHOT";

# Note: all parameters provided to this script are forwarded to 'mvn'

# Utility functions ############################################################

sub runcmd {
  my @args = @_;
  print "Running '" . (join " ", @args) . "'...\n";
  return system(@args) == 0;
}

# Main body ####################################################################

# Clean previous builds
runcmd("make", "clean");
unlink glob "*.tar.gz";
unlink glob "*.par";
unlink $parPath;
unlink $tgzPath;

# Build, test and generate the source distribution
runcmd("perl", "Makefile.PL") or die "Could not generate the Makefile";
runcmd("make")         or die "Could not build takuan-perl";
runcmd("make", "test") or die "A test failed in takuan-perl";
runcmd("make", "dist") or die "Could not build the source distribution for takuan-perl";
die "Cannot find source distribution" unless -r $tgzPath;

# Generate the par-based and executable distributions
my @ppArgs = ("pp", "-r",
	      "-I", "blib/lib",
	      "script/takuan-perl");
runcmd(@ppArgs, "-p", "-o", $parPath);
runcmd(@ppArgs, "-P", "-o", $perlPath);
runcmd(@ppArgs, "-o", $execPath);

# Deploy the generated files to Nexus
@ARGV = ("mvn") unless @ARGV;
my @deployArgs = (@ARGV, $deployFileGoal,
		  "-DgroupId=es.uca.webservices",
		  "-DartifactId=takuan-perl",
		  "-Dversion=$VERSION$versionSuffix",
		  "-DrepositoryId=$repositoryId",
		  "-Durl=$repositoryURL",
		  "-Dpackaging=par",
		  "-Dfile=$parPath",
		  "-Dclassifiers=sources,dist,dist",
		  "-Dtypes=tgz,pl,exe-$Config{archname}",
		  "-Dfiles=$tgzPath,$perlPath,$execPath"
		  );

runcmd(@deployArgs) or die "Could not deploy the artifacts";
