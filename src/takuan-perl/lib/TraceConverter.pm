#
# Converter from raw ActiveBPEL execution traces into Daikon variable trace files.
#
# Copyright (C) 2008-2011 Antonio García-Domínguez
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

package TraceConverter;

use strict;
use warnings;
use Exporter "import";
our @EXPORT_OK = qw(serialize convertirTraza imprimirPunto printConvertedTrace);

use Carp;

use Daikon qw(C_TJAVA rutaAsigAPunto cargarDecls loadActiveBPELLog);
use Text::Balanced qw(extract_bracketed);

# Convenience subroutine to encapsulate the old convertidorTrazas.pl script
sub printConvertedTrace {
  my ($rutaDecls, $rutaTraza)       = @_;
  my ($rhInfoTipos, $nombreProceso) = cargarDecls($rutaDecls);
  my $rvLineas   = loadActiveBPELLog($rutaTraza);
  my @lineas_ser = serialize(@$rvLineas);
  convertirTraza(join("\n", @lineas_ser) . "\n", $nombreProceso, $rhInfoTipos);
}

sub _careful_push {
  my ($rhMap, $key, $entry) = @_;

  if (exists $rhMap->{$key}) {
    push @{$rhMap->{$key}}, $entry;
  }
  else {
    confess "$key is not present in the map";
  }
}

sub _find_parent {
  my ($rhLinesByPath, $path) = @_;

  # Look up the longest prefix which exists in rhLinesByPath and
  # use it as parent path. We will always have a match, as we
  # have the empty path in rhLinesByPath.
  my $parentPath = $path;
  while (!exists $rhLinesByPath->{$parentPath}) {
    $parentPath = substr $parentPath, 0, rindex($parentPath, "/");
  }

  return $parentPath;
}

sub serialize
{
    my @input_lines = @_;

    my $rvLineTree    = [];
    my $rhLinesByPath = {"" => $rvLineTree};
    my $currentPath   = "";

    # Traverse the available lines: there are basically three cases:
    #
    # 1. The line starts a new activity ("Executing [...]").
    #
    # 2. The line does not start a new activity, but it has a path (a
    # "[/...]" substring).
    #
    # 3. The line does not start a new activity, and it does not have
    # a path: use the last path used for insertion.

    for my $line (@input_lines) {
      # Extract the first bracketed expression in the line which
      # contains a path. Note: we need to extract from a *copy*, not
      # the original line.
      my $lineCopy = $line;
      my $path = undef;
      while ($path = extract_bracketed($lineCopy, '[', "[^[]*")) {
	# All paths start with a slash
	last if (substr $path, 0, 2) eq "[/";
      }

      # Remove brackets (unless no bracketed expression was found)
      $path = substr $path, 1, -1 if $path;

      if (defined $path && length $path > 0) {
        my $parentPath = _find_parent $rhLinesByPath, $path;

        if (index($line, "Executing") != -1) {
          # Starting a new activity
          my $rvNewNode = [$line];
          ${rhLinesByPath}->{$path} = $rvNewNode;
          _careful_push $rhLinesByPath, $parentPath, $rvNewNode;

          $currentPath = $path;
        }
        else {
          # add the line to it
          _careful_push $rhLinesByPath, $parentPath, $line;

          $currentPath = $parentPath;
        }

        if (index($line, "Completed") != -1 && exists $rhLinesByPath->{$path}) {
          # If we're done with this path, remove it to make space for its next occurrence
          delete $rhLinesByPath->{$path};
          $currentPath = _find_parent $rhLinesByPath, $path;
        }
      }
      else {
        # No path: just use the same path we used last time
        _careful_push $rhLinesByPath, $currentPath, $line;
      }
    }

    return _flatten($rvLineTree);
}

sub _flatten
{
  return map { (ref $_ ne 'ARRAY') ? $_ : _flatten(@$_) } @_;
}

sub convertirTraza
{
    my $contenido     = shift;
    my $nombreProceso = shift;
    my $rhInfoTipos   = shift;

    while ($contenido =~ m#(?-xs:^Executing \[(.*/assign(?:\[[0-9]+\])?)\]$)
                          \s+
                          (.*?)
                          (?-xs:^Completed normally \[\1\]$)
                          | (?-xs:^Completed with fault: .* :  \[/.+sequence[^/]*\] \[f\]$)#xgsm)
    {
	my ($ruta, $traza) = ($1, $2||"");
	if (!defined $ruta) {
	    # Completed with fault: skip
	    next;
	}

	# Filtramos las inspecciones y retiramos aquellas asignaciones
	# que no provengan de nuestra instrumentacion
	my @lineasTraza    = split /\r?\n/, $traza;
	@lineasTraza = grep /INSPECTION/, @lineasTraza;
	next if (defined($ruta) && scalar @lineasTraza == 0);

	# Ahora construimos un hash con las asignaciones
	my %inspecciones = map {
	    /INSPECTION\((.*)\)\s+=\s+(.*)\s+\[\]/;
	    $1 => $2;
	} @lineasTraza;

	# Imprimimos este punto de la ejecución en cuestion
	imprimirPunto($nombreProceso, $ruta, \%inspecciones, $rhInfoTipos);
    }
}

sub imprimirPunto
{
    my $processName   = shift;
    my $path          = shift;
    my $rhInspections = shift;
    my $rhTypeInfo    = shift;
    my $outFHandle    = shift || *STDOUT;

    my $point = rutaAsigAPunto($processName, $path);
    my $rhPoint = $rhTypeInfo->{$point} or die "Could not find program point $point (path is $path)";
    print $outFHandle "$point\n";

    # Sort in a case-insensitive way, just like the declarations generator
    my @lvals = keys %$rhInspections;
    @lvals    = sort {lc $a cmp lc $b} @lvals;

    foreach my $lval (@lvals) {
	my $rval = $rhInspections->{$lval};

	$lval =~ tr#/$#.#d;
	$lval =~ s#\.(?:[^\.:/]*?):#.#g;

	# Initially, we assume the the value hasn't changed and that
	# it is not nonsensical. According to the Daikon manul, Daikon
	# can detect changed values by itself just fine.
	my $modified = 0;
	my $type = undef;
	my $decl = $lval;
	$decl =~ s/\[[0-9]+\]/\[\]/g;

	if ($rval eq "\@N\@nonsensical\@N\@") {
	    # Nonsensical value
	    $modified = 2;
	    $rval = "nonsensical";
	    print $outFHandle "$lval\n$rval\n$modified\n";
	}
	elsif (defined ($type = $rhPoint->{$decl}->{C_TJAVA()})) {
	    # Element with scalar content
	    if ($type =~ "^java.lang.String") {
		$rval = "\"$rval\"";
	    }
	    print $outFHandle "$lval\n$rval\n$modified\n";
	}
	elsif (defined ($type = $rhPoint->{$decl . "~elems[]"}->{C_TJAVA()})) {
	    # Element with list content
	    my @rsubvals = split /\s+/, $rval;
	    @rsubvals = map { qq("$_") } @rsubvals if $type =~ /^java.lang.String/;

	    foreach my $isubval (0..$#rsubvals) {
		print $outFHandle "$lval~elems[" . ($isubval+1) . "]\n${rsubvals[$isubval]}\n$modified\n";
	    }
	}
	elsif (defined ($type = $rhPoint->{$decl . "[]"}->{C_TJAVA()})) {
	    # Attribute with scalar content
	    if ($type =~ "^java.lang.String") {
		$rval = "\"$rval\"";
	    }
	    print $outFHandle "${lval}[1]\n$rval\n$modified\n"
	}
	elsif (defined ($type = $rhPoint->{$decl . "[]~elems[]"}->{C_TJAVA()})) {
	    # Attribute with list content
	    my @rsubvals = split /\s+/, $rval;
	    @rsubvals = map { qq("$_") } @rsubvals if $type =~ /^java.lang.String/;

	    foreach my $isubval (0..$#rsubvals) {
		print $outFHandle "${lval}[1]~elems[$isubval]\n${rsubvals[$isubval]}\n$modified\n";
	    }
	}
	else {
	    warn "Could not find the type of $decl at the program point $point";
	}
    }

    print $outFHandle "\n";
}

1;
