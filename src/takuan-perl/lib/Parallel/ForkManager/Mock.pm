package Parallel::ForkManager::Mock;

use strict;
use warnings;

sub start {
    return 0; 
}

sub finish {}

sub wait_all_children {}

sub run_on_finish {}

return 1;
