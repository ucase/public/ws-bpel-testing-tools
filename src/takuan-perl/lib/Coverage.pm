# Sentence and branch coverage evaluation for ActiveBPEL 4.1 execution traces
#
# Copyright (C) 2008-2011 Alejandro Álvarez-Ayllón, Antonio García-Domínguez
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

package Coverage;

use strict;
use warnings;
use Exporter "import";
our @EXPORT_OK = qw(formatCoverageResults);

use Daikon qw(loadActiveBPELLog);

sub formatCoverageResults {
  my ($plainText, @tracePaths) = @_;

  my $traceCount = scalar @tracePaths;
  my ($rvNotRun, $rvRun) = _loadAndDivideTraces(@tracePaths);
  my ($rhNeverRun, $rhTimesRun) = _countTimesRun($rvNotRun, $rvRun);
  my ($rhBranchesNeverRun, $rhBranchesRun) = _countTimesBranchesRun($traceCount, $rvNotRun, $rhNeverRun, $rhTimesRun);

  return _header($plainText)
    . _formatSentenceCoverageResults($plainText, $traceCount, $rhNeverRun, $rhTimesRun)
      . _formatBranchCoverageResults($plainText, $rhBranchesNeverRun, $rhBranchesRun)
	. _footer($plainText);
}

sub _loadAndDivideTraces {
  my @tracePaths = @_;

  my ($rvNotRun, $rvRun) = ([], []);
  foreach my $tracePath (@tracePaths) {
    print STDERR "Processing $tracePath ("
      . (scalar @$rvRun + 1) . "/" . (scalar @tracePaths)
	. ") for coverage statistics\n";

    my $rvContents = loadActiveBPELLog($tracePath);
    my ($notRun, $run) = _divideTraces($rvContents);

    push @$rvRun, $run;
    push @$rvNotRun, $notRun;
  }

  return ($rvNotRun, $rvRun);
}

# Procesa la traza, marcando como ejecutado aquellos que aparezcan con
# "executing" delante, y añadiendo a no ejecutados a los que empiecen por
# "will not execute" si no ha sido ejecutado ya
sub _divideTraces {
  my ($rvContenido) = @_;
  my ($noEjecutados, $ejecutados) = ({}, {});

  my $numLinea  = 1;

  my $destino = '';	    # Indica a dónde van las líneas de interés
  foreach my $linea (@$rvContenido) {
    if ($linea =~ /^Executing \s+ \[(.*(?:sequence|if|elseif|while|repeatUntil|forEach).*)\]/x) {
      $destino = $1;
      # Insertar en ejecutados si no está ya
      $ejecutados->{$destino} = [];
      # Eliminar de no ejecutados si estaba
      if (exists $noEjecutados->{$destino}) {
	delete $noEjecutados->{$destino};
      }
    } elsif ($linea =~ /^Will \s+ not \s+ execute \s+ \[(.*(?:sequence|if|elseif|while|repeatUntil|forEach).*)\] \s+ \[d\]$/x) {
      $destino = $1;
      # Insertar en no ejecutados si no esta en noEjecutados o en ejecutados
      $noEjecutados->{$destino} = []
	unless (exists $ejecutados->{$destino} || exists $noEjecutados->{$destino});
    } elsif ($linea =~ /Condition .* \[(.*)\]$/) {
      # NADA
    }
    $numLinea++;
  }

  return ($noEjecutados, $ejecutados);
}

sub _countTimesRun {
  my ($rvNotRun, $rvRun) = @_;

  # A partir de la información obtenida por cada traza,
  # unificar en rhNeverRun y rhTimesRun
  my $rhNeverRun = {};
  my $rhTimesRun = {};

  # Los que estén en alguna de las trazas en ejecutado, se ha ejecutado
  foreach my $case (@$rvRun) {
    foreach my $path (keys %$case) {
      if (exists $rhTimesRun->{$path}) {
	$rhTimesRun->{$path}++;
      } else {
	$rhTimesRun->{$path} = 1;
      }
    }
  }

  # Los que estén en alguna de las trazas en no ejecutado, y no en
  # rhTimesRun, nunca se han ejecutado
  foreach my $case (@$rvNotRun) {
    foreach my $path (keys %$case) {
      $rhNeverRun->{$path} = 0 unless exists $rhTimesRun->{$path};
    }
  }

  return ($rhNeverRun, $rhTimesRun);
}

sub _countTimesBranchesRun {
  my ($traceCount, $rvNotRun, $rhNeverRun, $rhTimesRun) = @_;

  # Extraer las ramas que no se han ejecutado nunca
  my $rhBranchesNeverRun = _extractBranches($rhNeverRun);

  # Extraer las ramas ejecutadas alguna vez
  my $rhBranchesRun = _extractBranches($rhTimesRun);

  # Buscar por cada caso de prueba, el else correspondiente a un if no ejecutado,
  # es decir, un else que se ha ejecutado (y que no sea explícito)
  foreach my $case (@$rvNotRun) {

    my $rhBranchesNotRun = _extractBranches($case);
    foreach my $ruta (keys %$rhBranchesNotRun) {
      # Separar, y obtener el último elemento
      my @partes   = split('/', $ruta);
      my $elemento = pop(@partes);

      # Si es un if-condition
      if ($elemento =~ /if-condition[?.*]?/x) {
	# Generar el else
	push(@partes, 'else');
	my $elseR = join('/', @partes);

	# Comprobar que su elemento padre, de haberlo, sí se ha ejecutado
	pop(@partes);
	my $parent;
	do {
	  $parent = pop(@partes);
	} while ($parent && !($parent =~ /if-condition[?.*]?/x || $parent =~ /else/x));

	# Hay padre, ruta completa
	if ($parent) {
	  push(@partes, $parent);
	  $parent = join('/', @partes);
	}

	# Añadir si el padre se ejecutó o no existe
	if (!$parent || !(exists $case->{$parent})) {
	  $rhBranchesRun->{$elseR} = [];
	  # Eliminar de las no ejecutadas si está
	  if (exists $rhBranchesNeverRun->{$elseR}) {
	    delete $rhBranchesNeverRun->{$elseR};
	  }
	} else {
	  # Añadir el else a nunca ejecutadas si no está en ejecutadas
	  $rhBranchesNeverRun->{$elseR} = [] unless
	    exists $rhBranchesRun->{$elseR};
	}
      }
    }

  }

  # Buscar en todos los casos de prueba, los ifs que se han ejecutado siempre
  # que se ha entrado por la rama padre,
  # y añadir el else correspondiente a los nunca ejecutados (si no es explícito)
  foreach my $ruta (keys %$rhBranchesRun) {

    my $maxVeces;

    # Obtenemos el padre
    my @partes = split('/', $ruta);
    pop(@partes);
    my $parent;
    do {
      $parent = pop(@partes);
    } while ($parent && !($parent =~ /if-condition[?.*]?/x || $parent =~ /else/x));

    # El número de veces que puede haberse ejecutado = número de veces del padre
    if ($parent) {
      push(@partes, $parent);
      $parent = join('/', @partes);
      $maxVeces = $rhTimesRun->{$parent};
    } else {
      $maxVeces = $traceCount;
    }

    # Se han ejecutado siempre, si el valor del hash es igual
    # al número de veces que se entró por el padre
    if ($rhBranchesRun->{$ruta} == $maxVeces) {
      # Separar y obtener el último elemento
      my @partes   = split('/', $ruta);
      my $elemento = pop(@partes);

      # Si es un if-condition
      if ($elemento =~ /if-condition[?.*]?/x) {
	# Generar el else
	push(@partes, 'else');
	my $elseR = join('/', @partes);
	# Añadir a nunca ejecutados
	$rhBranchesNeverRun->{$elseR} = [];
      }
    }
  }

  return ($rhBranchesNeverRun, $rhBranchesRun);
}

# Extrae de una lista de rutas las ramas
# Pendiente añadir soporte para pick
sub _extractBranches {
  my ($instrucciones) = @_;
  my $ramas = {};
  my $rama;

  foreach my $instruccion (keys %$instrucciones) {
    my $r;

    #### Obtener la ruta de la rama

    # Partir en partes la ruta
    my @partes = split('/', $instruccion);

    # Comprobar si el último elemento es un if-condition o else
    my $elemento = pop(@partes);
    if ($elemento =~ /if-condition[?.*]?/x || $elemento =~ /else[?.*]?/x) {
      #### Añadir
      $ramas->{$instruccion} = $instrucciones->{$instruccion};
    }
    # Comprobar si es un while
    elsif ($elemento =~ /while[?.*]?/x) {
      #### Añadir
      $ramas->{$instruccion} = $instrucciones->{$instruccion};
    }
    # For-each
    elsif ($elemento =~ /forEach[?.*]?/x) {
      $ramas->{$instruccion} = $instrucciones->{$instruccion};
    }
    # Repeat-until
    elsif ($elemento =~ /repeatUntil[?.*]?/x) {
      $ramas->{$instruccion} = $instrucciones->{$instruccion};
    }
  }

  return $ramas;
}

sub _header {
  my ($plainText) = @_;

  if (!$plainText) {
    return <<HERE
<?xml version="1.0" encoding="UTF-8"?>
<cov:document xmlns:cov="http://www.uca.es/2009/10/coverage">
HERE
      ;
  } else {
    return "";
  }
}

sub _footer {
  my ($plainText) = @_;
  return $plainText ? "" : "</cov:document>\n";
}

sub _formatSentenceCoverageResults {
  my ($plainText, $traceCount, $rhNeverRun, $rhTimesRun) = @_;

  my $runCount    = scalar keys %$rhTimesRun;
  my $notRunCount = scalar keys %$rhNeverRun;
  my $totalCount  = $runCount + $notRunCount;
  my $percentRun    = int(($runCount / $totalCount) * 10000)/100;
  my $percentNotRun = int(($notRunCount / $totalCount) * 10000)/100;

  # Cobertura de instrucción
  if ($plainText) {
    return <<HERE
Instruction coverage
====================

Executed $runCount/$totalCount ($percentRun%)
-----------------------

HERE
      .	(join "\n", map {
	  my $num = $rhTimesRun->{$_};
	  "($num/$traceCount) $_";
        } sort keys %$rhTimesRun)
      . <<HERE
Never executed $notRunCount/$totalCount ($percentNotRun%)
-----------------------------
HERE
      . (join "\n", sort keys %$rhNeverRun)
      ;
  }
  else {
    return <<HERE
	<cov:sentences>
		<cov:executed count="$runCount" total="$totalCount" testCases="$traceCount">

HERE
      . (join "\n", map {
	  "\t\t\t<cov:sentence times=\"" . ($rhTimesRun->{$_}) . "\"> $_</cov:sentence>";
	} sort keys %$rhTimesRun)
      . <<HERE
		</cov:executed>
		<cov:notExecuted count="$notRunCount" total="$totalCount">
HERE
      . (join "\n", map {
	  "\t\t\t<cov:sentence>$_</cov:sentence>"
	} sort keys %$rhTimesRun)
      . <<HERE
		</cov:notExecuted>
	<cov:sentences>
HERE
      ;
  }

}

sub _formatBranchCoverageResults {
  my ($plainText, $rhBranchesNeverRun, $rhBranchesRun) = @_;

  my $ramasEjecutadas   = scalar keys %$rhBranchesRun;
  my $ramasNoEjecutadas = scalar keys %$rhBranchesNeverRun;
  my $totalRamas        = $ramasEjecutadas + $ramasNoEjecutadas;

  my $percentRamasEjecutadas   = int(($ramasEjecutadas / $totalRamas) * 10000) / 100;
  my $percentRamasNoEjecutadas = int(($ramasNoEjecutadas / $totalRamas) * 10000) / 100;

  if ($plainText) {
    return <<HERE

Branches coverage
=================
Branches executed $ramasEjecutadas / $totalRamas ($percentRamasEjecutadas%)
--------------------------------

HERE
      . (join "\n", sort keys %$rhBranchesRun)
      . <<HERE

Branches not executed $ramasNoEjecutadas / $totalRamas ($percentRamasNoEjecutadas%)
------------------------------------

HERE
      . (join "\n", sort keys %$rhBranchesNeverRun);

  }
  else {
    return <<HERE
	<cov:branches>
		<cov:executed count="$ramasEjecutadas" total="$totalRamas">
HERE
      . (join "\n", map {
	    "\t\t\t<cov:sentence>$_</cov:sentence>"
         } sort keys %$rhBranchesRun)
      . <<HERE
		</cov:executed>
		<cov:notExecuted count="$ramasNoEjecutadas" total="$totalRamas">
HERE
      . (join "\n", map {
	    "\t\t\t<cov:sentence>$_</cov:sentence>"
	 } sort keys %$rhBranchesNeverRun)
      . <<HERE
		</cov:notExecuted>
	</cov:branches>

HERE
      ;
  }
}

1;
