#
# Utility functions to handle Daikon declarations and traces
#
# Copyright (C) 2008-2011 Antonio García-Domínguez
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA.

package Daikon;

use strict;
use warnings;
use Exporter "import";

our $VERSION = "2.0.2";
our @EXPORT_OK = qw(cargarDecls measureDecls imprimirDecls parseConstraints formatConstraints
                    loadActiveBPELLog cargarDTrace imprimirDTrace
                    C_NAME C_VARLIST C_VALUE C_MOD C_TXSD C_TJAVA C_COMP C_TDUMMYHASH
                    rutaAsigAPunto pathToBasePP
                    $VERSION);

use constant {
    C_VALUE   => "value",
    C_MOD     => "mod",
    C_NAME    => "name",
    C_VARLIST => "varlist",
    C_TXSD   => "xsdType",
    C_TJAVA => "javaType",
    C_COMP  => "comp",
    C_TDUMMYHASH => "dummyhashcode",
};


## CUERPOS

#
# Carga un fichero de declaraciones de Daikon en memoria, devolviendo
# una referencia a un hash del nombre del punto a una referencia de
# hash con los atributos C_TXSD (tipo observado en el programa
# original), C_TJAVA (tipo correspondiente en Java), y C_COMP
# (identificador de comparabilidad).
#
# Puede recibir una ruta a un fichero, o una referencia a un vector
# con las líneas a cargar.
#
sub cargarDecls
{
    my $entrada = shift;
    my $disableXMLSchema = shift;

    my @vLineas;
    if (ref($entrada) eq "ARRAY") {
        @vLineas = @$entrada;
    }
    else {
        open(IFH, "<:crlf", $entrada)
            or die "Could not open $entrada: $!";
        @vLineas = <IFH>;
        close(IFH);
    }

    my $contenido = join "", @vLineas;
    my %infoTipos = ();
    while ($contenido =~ /(?-x:^DECLARE$) \s
                          (?-s:(.*)) \s # nombre del punto
                          (.*?)         # contenido
                          (?-m:\n{2,}|\n*$)/gmsx) {

        my ($punto, $contenidoPunto) = ($1,$2);
        my %infoTiposPunto           = ();

        while ($contenidoPunto =~ /(.*)\n(.*)\n(.*)\n(.*)\n?/g) {
            $infoTiposPunto{$1} = {
                C_TJAVA() => $3,
                C_COMP()  => $4
            };
            if ($disableXMLSchema) {
                # Remove # part with the XML Schema annotations
                my @parts = split /\s*#\s*/, $2;
                $infoTiposPunto{$1}{C_TXSD()} = $parts[0];
            }
            else {
                # Keep # part as is
                $infoTiposPunto{$1}{C_TXSD()} = $2;
            }
        }

        $infoTipos{$punto} = \%infoTiposPunto;
    }

    # Obtenemos el nombre del proceso del primer punto, aprovechando
    # que el formato de los puntos es (nombre proceso).(punto)::(tipo
    # punto)
    my $nombrePrimerPunto = (keys(%infoTipos))[0];
    my ($nombreProcesoBPEL, @resto) = split(/\./, $nombrePrimerPunto);

    return (\%infoTipos, $nombreProcesoBPEL);
}

sub imprimirDecls
{
    my ($rhInfoTipos) = @_;

    foreach my $punto (sort keys %$rhInfoTipos) {
        my $rhVars = $rhInfoTipos->{$punto};
        print "DECLARE\n$punto\n";

        foreach my $variable (sort keys %$rhVars) {
            my $rhCampos = $rhVars->{$variable};
            my $tob      = $rhCampos->{C_TXSD()};
            my $tj       = $rhCampos->{C_TJAVA()};
            my $comp     = $rhCampos->{C_COMP()};

            print "$variable\n$tob\n$tj\n$comp\n";
        }

        print "\n";
    }
}

#
# Takes a data structure like those returned by cargarDecls and
# returns a hash with several metrics of interest on the Daikon
# declaration file.
#
sub measureDecls
{
    my ($rhDecls) = @_;
    my $rhMetrics = {};

    my @var_counts   = map { scalar(keys(%{$_})) } values(%$rhDecls);
    my $total_points = scalar @var_counts;
    my $total_vars   =
        _reduce(sub { $_[0] + $_[1]; },
               0,
               @var_counts);
    my $mean_vars    = $total_vars / $total_points;
    my $stddev_vars  =
        sqrt(_reduce(sub { $_[0] + ($_[1]-$mean_vars)**2; },
                    0,
                    @var_counts)
             / $total_points);

    $rhMetrics->{total_program_points}                      = $total_points;
    $rhMetrics->{total_variables}                           = $total_vars;
    $rhMetrics->{average_variables_per_point}               = $mean_vars;
    $rhMetrics->{standard_deviation_of_variables_per_point} = $stddev_vars;

    return $rhMetrics;
}

#
# Carga un .dtrace de Daikon en memoria, modelándolo como una
# referencia a un vector cuyos elementos $a tienen esta estructura:
#
# $a = {
#     C_NAME()    => "A.B:::ENTER",
#     C_VARLIST() => {
#       "a" => {
#           C_VALUE() => "42",
#           C_MOD()   => "0",
#       },
#     },
# }
#
# C_VALUE señala al valor, y C_MOD al campo de modificado (0, 1, o 2:
# ver A.3 en manual de Daikon). No se ocupa de la información de
# comparabilidad.
#
sub cargarDTrace
{
    my $ruta = shift;

    open(IFH, "<:crlf", $ruta)
        or die "Could not open $ruta: $!";
    my @vLineas   = <IFH>;
    close(IFH);

    my $contenido = join "", @vLineas;
    my $rvTrazasPuntos = [];

    while ($contenido =~ /(.+)((?:.|\n)*?)(?:\n\n|$)/g) {
        my $nombrePunto    = $1;
        my $cadTrazasPunto = $2;
        my $rhVarlist      = {};
        my $rhTrazasPunto  = {C_NAME()    => $nombrePunto,
                              C_VARLIST() => $rhVarlist};

        while ($cadTrazasPunto =~ /(.+)\n(.+)\n(.+)\n?/g) {
            my ($expr, $valor, $mod) = ($1,$2,$3);

            $rhVarlist->{$expr} = {
                C_VALUE() => $valor,
                C_MOD() => $mod,
            };
        }

        push @$rvTrazasPuntos, $rhTrazasPunto;
    }

    return $rvTrazasPuntos;
}

#
# Imprime la estructura de cargarDTrace por la salida estándar en el
# formato de Daikon. No se ocupa de la información de comparabilidad.
#
sub imprimirDTrace
{
    my ($rvTrazasPuntos) = @_;

    foreach my $rhPunto (@$rvTrazasPuntos) {
        my $punto   = $rhPunto->{C_NAME()};
        my $rhVars  = $rhPunto->{C_VARLIST()};

        print "$punto\n";

        foreach my $expr (sort keys %$rhVars) {
            print "$expr\n";
            print $rhVars->{$expr}->{C_VALUE()} . "\n";
            print $rhVars->{$expr}->{C_MOD()}   . "\n";
        }

        print "\n";
    }
}

sub rutaAsigAPunto
{
    my $processName = shift;
    my $path          = shift;

    my (undef, @parts) = split m#/#, $path;
    my $lastPart = pop @parts;

    my $tipoPunto;
    if ($lastPart eq "assign") {
        # first assignment -> ENTER
        $tipoPunto = "ENTER";
    }
    elsif ($lastPart =~ /^assign/) {
        # not the first assignment -> EXIT
        $tipoPunto = "EXIT1";
    }
    else {
        # not an assignment -> ENTER, and return the last part to the path
        $tipoPunto = "ENTER";
        push @parts, $lastPart;
    }

    return pathToBasePP($processName, @parts) . ":::${tipoPunto}";
}

=head2 pathToBasePP(processName, pathComponents)

=over 2

=item Usage:

Public function.

=item Arguments:

=over 2

=item processName:

Name of the WS-BPEL process under study.

=item parts:

List of strings with each component of the XPath path of the node in
the WS-BPEL definition, after being split on the slashes (/).

=back

=item Description:

Converts the XPath path to the Daikon style, replacing / by _,
substituting steps with names with their names and removing useless
steps, such as the if-condition.

=back

=cut
sub pathToBasePP
{
    my ($processName, @parts) = @_;

    my $pp;
    # Remove the virtual "if-condition" steps added by ActiveBPEL
    @parts = grep { $_ ne "if-condition" } @parts;

    foreach my $i (0..$#parts) {
        if ($parts[$i] =~ /\[\@name='([^'\]]+)'\]/) {
            $parts[$i] = $1;
        }
        elsif ($parts[$i] =~ /^([^\[\]]+).*?\[([0-9]+)\]/) {
            $parts[$i] = $1 . $2;
        }
        else {
            my ($elemName, undef) = split /\[/, $parts[$i];
            $parts[$i] = "${elemName}1";
        }
        }
    $pp = "_" . (join "_", @parts);

    return "$processName.$pp";
}

sub loadActiveBPELLog
{
    my $path = shift;
    open(IFH, "<:crlf", $path)
        or die "Could not open $path: $!";
    my @vLines = <IFH>;
    close(IFH);

    # Remove the line header
    @vLines = map { $_ =~ s/^\[[0-9]+\]\[[0-9:. -]+\]\s*(?::\s*)?//; $_ } @vLines;

    return \@vLines;
}

=head2 parseConstraints(s)

=over 2

=item Usage:

Public function.

=item Description:

Parses the Daikon variable constraints from the original type
definition s (of the form 'type [# ctext]') and returns a reference to
an (type, rhConstraints) array, where rhConstraints is a reference to
a hash of the form 'cname -> cvalue' representing the constraints in
ctext.

If a constraint used a list for its value (such as '[1 1]' or '["a"
"b"]'), cvalue will be a reference to a list of strings with the
elements in the constraint. Otherwise, cvalue will be a string with
the literal value of the constraint.

All values have their surrounding whitespace stripped.

=back

=cut

sub parseConstraints
{
    my ($typedef) = @_;

    my ($type, $constraints_text) = split /\s*#\s*/, $typedef;
    !defined($constraints_text) && return [$type, {}];

    my @parts = split /\s*,\s*/, $constraints_text;
    my $constraints = {};
    foreach my $part (@parts) {
        my ($name, $value_text) = split /\s*=\s*/, $part;

        $value_text =~ /^\[([^\]]*)\]$/ && do {
            my @elems = split /\s+/, $1;
            $constraints->{$name} = \@elems;
            next;
        };

        $constraints->{$name} = $value_text;
    }

    return [$type, $constraints];
}

=head2 formatConstraints(s)

=over 2

=item Usage:

Public function.

=item Description:

Formats the Daikon variable constraints into a string, which should be
in the same format as that returned by parseConstraint(s). The
returned string will be of the form " # cname=cvalue, cname=cvalue..."
if there is at least one constraint, or simply "" if there are no
constraints. The cname are printed in ascending order.

=back

=cut

sub formatConstraints
{
    my ($constraints) = @_;

    # Return the empty string if there are no constraints
    scalar keys %$constraints == 0 && return "";

    my @values = ();
    foreach my $cname (sort keys %$constraints) {
        my $cvalue = $constraints->{$cname};
        if (ref($cvalue) eq "ARRAY") {
            push @values, "$cname=[" . (join " ", @$cvalue) . "]";
        }
        else {
            push @values, "$cname=$cvalue";
        }
    }
    return " # " . (join ", ", @values);
}

=head2 _reduce(f,z,x)

=over 2

=item Usage:

Private function.

=item Description:

Takes a binary function f, a neutral element z and a list built from
the elements x1,x2,...,xN and reduces it to a single value, y, using
the following equation:

y = f(...f(f(z,x1),x2)...,xN)

=item Example:

Summing the elements of a list would be done this way:

C<my $sum = reduce(sub { $_[0] + $_[1]; }, 0, @l);>

=back

=cut

sub _reduce
{
    my ($function, $neutral, @list) = @_;

    my $result = $neutral;
    foreach (@list) {
        $result = &$function($result, $_);
    }

    return $result;
}

1;
