#
# Daikon declarations flattening module.
#
# Copyright (C) 2008-2011 Antonio García-Domínguez
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package FlattenDecls;

use strict;
use warnings;
use Exporter "import";
our @EXPORT_OK = qw(printFlattenedDecls
                    indexFlatten indexFlattenVariable
                    nodesetFlatten nodesetFlattenVariable
                    computeMaxLength createHashcodeType
                    C_DUMMYHASHCODE_COMP);

use Daikon qw(C_TXSD C_TJAVA C_COMP C_TDUMMYHASH C_NAME C_VARLIST
              parseConstraints formatConstraints
              cargarDecls cargarDTrace imprimirDecls);

use constant {
    C_DUMMYHASHCODE_COMP => 1,
            C_MINLEN => "minlength",
                    C_MAXLEN => "maxlength",
                };

sub printFlattenedDecls {
    my ($disableXMLSchema, $discardEmptyLists, $indexFlattening, $pathToDecls, @pathsToTraces)  = @_;

    # Use the original .decls as a starting point
    my ($rhVarsByPP, $processName) = cargarDecls($pathToDecls, $disableXMLSchema);

    # Compute the maximum length of each vector component in the declarations from the .dtraces
    my $rhMaxLength = {};
    foreach my $pathToTrace (@pathsToTraces) {
        my $rvTraces = cargarDTrace($pathToTrace);
        computeMaxLength($rvTraces, $rhMaxLength);
    }

    # Flatten declarations
    my $rfFlatten = defined $indexFlattening ? \&indexFlatten : \&nodesetFlatten;
    my $rhFlatVarsByPP = &$rfFlatten($rhVarsByPP, $rhMaxLength, $discardEmptyLists);

    # Print the new declarations in the format accepted by Daikon
    imprimirDecls($rhFlatVarsByPP);
}

=head2 indexFlatten(rhVarsByPP, rhMaxLength, discardEmptyLists)

=over 2

=item Usage:

Public function.

=item Arguments:

=over 2

=item rhVarsByPP:

Reference to a hash with the variables and their types for every program point.

=item rhMaxLength:

Reference to a hash with the maximum length of every vector variable
in each program point.

=item discardEmptyLists:

If defined, intermediate empty vector variables and all their children
will be dropped. This can save quite a bit of CPU time when the WSDL
message format has many optional parts which are mostly unused. On the
other hand, we won't be able to explicitly infer that a certain
element is never used.

=back

=item Description:

Performs index flattening on rhVarsByPP, returning a reference to a
new hash where every subpath of the form 'x[].y...' has been replaced
by 'x[1].y...', 'x[2].y...' up to 'x[N].y...', where N is the maximum
length that x had in that base program point (including the entry
point and every exit point).

=back

=cut

sub indexFlatten {
    my ($rhVarsByPP, $rhMaxLength, $discardEmptyLists) = @_;
    my $rhNewTypeInfo = {};
    my $defaultLength    = defined $discardEmptyLists ? 0 : 1;

    foreach my $pp (keys %$rhVarsByPP) {
        my ($basepp, undef) = split /:::/, $pp;

        foreach my $variable (keys %{$rhVarsByPP->{$pp}}) {
            # We will decompose this variable into many others, by
            # replacing every occurrence of [] all the valid
            # indices. We'll keep adding these to a queue until we
            # remove all the inner occurrences of [].
            my @partialExpansionQueue = ($variable);

            while (scalar @partialExpansionQueue > 0) {
                my $expr = shift @partialExpansionQueue;

                if ($expr =~ /(.+?)\[\](.+)/) {
                    # Partially flattened vector/list
                    my ($prefix, $suffix) = ($1, $2);
                    my $length = $rhMaxLength->{$basepp}->{$prefix} || $defaultLength;
                    foreach my $i (1 .. $length) {
                        unshift @partialExpansionQueue, "$prefix\[$i\]$suffix";
                    }
                } else {
                    if ($expr =~ /(.+)\[\]$/) {
                        my $length = $rhMaxLength->{$basepp}->{$1};
                        next if $discardEmptyLists && !$length;
                    }
                    my $type = _get_type_without_indices($pp, $expr, $rhVarsByPP);
                    indexFlattenVariable($pp, $expr, $type, $rhNewTypeInfo);
                }
            }
        }
    }

    return $rhNewTypeInfo;
}

sub _get_type_without_indices {
    my ($punto, $exprSubindices, $rhVarsByPP) = @_;

    my $exprGeneral =  $exprSubindices;
    $exprGeneral    =~ s/\[([0-9]+)\]/\[\]/g;
    exists $rhVarsByPP->{$punto}->{$exprGeneral}
            or die "Could not find the type of $exprSubindices through $exprGeneral at $punto";

    return $rhVarsByPP->{$punto}->{$exprGeneral};
}

=head2 indexFlattenVariable(sPP, sVar, rhType, rhVarsByPP)

=over 2

=item Usage:

Public function.

=item Arguments:

=over 2

=item sPP: string with the name of the Daikon program point.

=item sVar: string with the name of the original Daikon variable. The
variable should only have at most one instance of "[]": all the others
should have been replaced with proper indices. For instance,
"x[0].y[]" is acceptable, but not "x[].y[]".

=item rhType: reference to a hash with information about the original
type of the variable.

=item rhVarsByPP: reference to a hash following a "program point ->
variable -> type" structure.

=back

=item Description:

Performs index flattening on a variable with all indices except the
last one (if it exists) resolved. Adds the flattened declaration to
rhVarsByPP.

=back

=cut

sub indexFlattenVariable {
    my ($sPP, $sVar, $rhType, $rhVarsByPP) = @_;

    if ($sVar =~ /(.*)\[\]$/) {
        $rhVarsByPP->{$sPP}->{$1} = createHashcodeType();
        $rhVarsByPP->{$sPP}->{$sVar}
                = _flattenVectorType($rhType, \&indexFlattenMinMaxLength);
    }
    elsif (!defined $rhType->{C_TJAVA()} || index($rhType->{C_TJAVA()}, "[]") == -1) {
        # Original type does not have any [] in the middle: use it as is
        $rhVarsByPP->{$sPP}->{$sVar} = $rhType;
    }
    else {
        my $copy = { %$rhType };

        # The original type might have some [] in the middle: remove
        # the comparabily indices for those
        $copy->{C_COMP()} =~ s/\[[0-9]+\]//g;

        $rhVarsByPP->{$sPP}->{$sVar} = $copy;
    }
}

sub indexFlattenMinMaxLength {
    my ($rhConstraints) = @_;

    if (ref($rhConstraints->{C_MINLEN()}) eq "ARRAY") {
        $rhConstraints->{C_MINLEN()} = $rhConstraints->{C_MINLEN()}->[-1];
    }
    if (ref($rhConstraints->{C_MAXLEN()}) eq "ARRAY") {
        $rhConstraints->{C_MAXLEN()} = $rhConstraints->{C_MAXLEN()}->[-1];
        delete $rhConstraints->{C_MAXLEN()}
                if $rhConstraints->{C_MAXLEN()} eq "*";
    }
}

=head2 nodesetFlatten(rhVarsByPP, rhMaxLength, discardEmptyLists)

=over 2

=item Usage:

Public function.

=item Arguments:

Same as indexFlatten.

=item Description:

Performs nodeset flattening on rhVarsByPP, returning a reference to a
new hash where every variable of the form 'x[].y[].z[]' has been
replaced by 'x.y.z[]', which aggregates all the z elements inside
every y element inside every x element.

=back

=cut

sub nodesetFlatten {
    my ($rhVarsByPP, $rhMaxLength, $discardEmptyLists) = @_;
    my $rhNewTypeInfo = {};

    foreach my $punto (keys %$rhVarsByPP) {
        my ($basePP, undef) = split /:::/, $punto;

        foreach my $variable (keys %{$rhVarsByPP->{$punto}}) {
            next if $discardEmptyLists && _isEmptyList($rhMaxLength->{$basePP}, $variable);

            my $type = $rhVarsByPP->{$punto}->{$variable};
            nodesetFlattenVariable($punto, $variable, $type, $rhNewTypeInfo);
        }
    }

    return $rhNewTypeInfo;
}

sub _isEmptyList {
    my ($rhMaxLengthInPP, $var) = @_;

    if ($var =~ /(.+?)\[\](.*)/) {
        my ($prefix, $suffix) = ($1, $2);

        # If this component has never appeared in the output, it is an empty list
        my $componentLength = $rhMaxLengthInPP->{$prefix} or return 1;

        # If at least one of its children is not an empty list, it is not an empty list
        foreach my $i (1 .. $componentLength) {
            my $subvar = "${prefix}[${i}]${suffix}";
            return 0 if !_isEmptyList($rhMaxLengthInPP, $subvar);
        }

        # If all its children were empty lists, it is an empty list
        return 1;
    } else {
        # This is not a list: therefore, it is not an empty list
        return 0;
    }
}

=head2 nodesetFlattenVariable(sPP, sVar, rhType, rhVarsByPP)

=over 2

=item Usage:

Public function.

=item Description:

=item Arguments:

=over 2

=item sPP: string with the name of the Daikon program point.

=item sVar: string with the name of the original Daikon variable.

=item rhType: reference to a hash with information about the original
type of the variable.

=item rhVarsByPP: reference to a hash following a "program point ->
variable -> type" structure.

=back

=item Description:

Performs nodeset flattening on a variable. Adds the flattened
declaration to rhVarsByPP.

=back

=cut

sub nodesetFlattenVariable {
    my ($sPP, $sVar, $rhType, $rhVarsByPP) = @_;

    if (index($sVar, "[]") != -1) {
        $sVar =~ s/\[\]//g;
        $rhVarsByPP->{$sPP}->{$sVar} = createHashcodeType();
        $rhVarsByPP->{$sPP}->{$sVar . "[]"}
                = _flattenVectorType($rhType, \&nodesetFlattenMinMaxLength);
    } else {
        $rhVarsByPP->{$sPP}->{$sVar} = $rhType;
    }
}

sub nodesetFlattenMinMaxLength {
    my ($rhConstraints) = @_;

    if (ref($rhConstraints->{C_MINLEN()}) eq "ARRAY") {
        my $min = 1;
        foreach my $x (@{$rhConstraints->{C_MINLEN()}}) {
            $min *= $x;
        }
        $rhConstraints->{C_MINLEN()} = $min;
    }
    if (ref($rhConstraints->{C_MAXLEN()}) eq "ARRAY") {
        my $max = 1;
        foreach my $x (@{$rhConstraints->{C_MAXLEN()}}) {
            $x eq "*" && do {
                $max = undef;
                delete $rhConstraints->{C_MAXLEN()};
                last;
            };

            $max *= $x;
        }
        if (defined $max) {
            $rhConstraints->{C_MAXLEN()} = $max;
        }
    }
}

=head2 create_hashcode_type()

=over 2

=item Usage:

Public function.

=item Description:

Returns a reference to a hash with the information for a dummy
hashcode variable, required by Daikon for vectors.

=back

=cut

sub createHashcodeType {
    return {
            C_TXSD()   => C_TDUMMYHASH(),
            C_TJAVA() => "hashcode",
            C_COMP()  => C_DUMMYHASHCODE_COMP(),
           };
}

=head2 computeMaxLength(rvTraces, rhMaxLength)

=over 2

=item Usage:

Public function.

=item Arguments:

=over 2

=item rvTraces:

Reference to an array with the dtrace entries to be analyzed. Each
entry is a reference to a hash with keys for the name and the list of
variable values.

=item rhMaxLength:

Reference to the hash to which results should be saved.

=back

=item Description:

rhMaxLength will be extended with the maximum length of the every vector
variable over every base program point. A base program point "x"
covers both x:::ENTER and all the x:::EXIT* program points. This
ensures that entry and exit variables are always the same for the
entry and exit program points of the same base program point.

If some of the variables found in rvTraces already existed in
rhMaxLength, they will be only updated if the new value is larger than
the previously existing one.

=back

=cut

sub computeMaxLength {
    my ($rvTrazas, $rhMaxLength) = @_;

    foreach my $rhPunto (@$rvTrazas) {
        my $punto  = $rhPunto->{C_NAME()};
        my $rhVars = $rhPunto->{C_VARLIST()};

        # We only want the base program point - entry and exit
        # variables must be guaranteed to be the same.
        my ($basepp, undef) = split /:::/, $punto;
        die "Could not obtain base program point from '$punto'" if !defined $basepp;

        if (!exists $rhMaxLength->{$basepp}) {
            $rhMaxLength->{$basepp} = {};
        }

        foreach my $variable (keys %$rhVars) {

            # Si tenemos una expresion de la forma a.b[x].c[y].d[z],
            # deseamos obtener:
            # * a.b, indice x
            # * a.b[x].c, indice y
            # * a.b[x].c[y].d, indice z

            while ($variable =~ /\[([0-9]+)\]/g) {
                my $prefix = substr $variable, 0, $-[0];
                my $indice  = $1;

                if (!exists $rhMaxLength->{$basepp}->{$prefix}
                    || $indice > $rhMaxLength->{$basepp}->{$prefix}) {
                    $rhMaxLength->{$basepp}->{$prefix} = $indice;
                }
            }

        }                       #foreach $variable

    }                           # foreach $punto

    return $rhMaxLength;
}

sub _flattenVectorType {
    my ($rhType, $rfModifyConstraints) = @_;

    # Leave only one instance of [] in observed and Java types
    my $rhNewType = { %$rhType };
    $rhNewType->{C_TXSD()} =~ s/(\[\])+/\[\]/g;
    $rhNewType->{C_TJAVA()} =~ s/(\[\])+/\[\]/g;

    # Modify constraints according to the received function reference
    my ($type, $rhConstraints) = @{parseConstraints($rhNewType->{C_TXSD()})};
    &$rfModifyConstraints($rhConstraints);
    $rhNewType->{C_TXSD()} = $type . formatConstraints($rhConstraints);

    # Leave only the last array index comparability index
    $rhNewType->{C_COMP()} =~ s/(?:\[[0-9]+\](?=\[))//g;

    return $rhNewType;
}

1;
