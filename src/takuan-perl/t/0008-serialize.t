#!perl -T

use TraceConverter qw(serialize);
use Test::More tests => 10;

sub ser {
    my @results = serialize(@_);
    return \@results;
}

is_deeply(ser(), [], "empty");

is_deeply(ser("Executing [/a]"), ["Executing [/a]"], "one");

is_deeply(ser("Executing [/a]",
              "Executing [/b]"),
          ["Executing [/a]",
           "Executing [/b]"], "two");

is_deeply(ser("Executing [/a]",
              "Executing [/a/b]",
              "Executing [/a/c]",
              "Executing [/a/b/d]",
              "Executing [/a/c/e]"),
          ["Executing [/a]",
           "Executing [/a/b]",
           "Executing [/a/b/d]",
           "Executing [/a/c]",
           "Executing [/a/c/e]"],
          "interleaved");

is_deeply(ser("l0",
              "Executing [/a]",
              "l1",
              "Executing [/a/b]",
              "l2",
              "Executing [/a/c]",
              "l3",
              "Executing [/a/b/d]",
              "l4",
              "Executing [/a/c/e]",
              "l5"),
          ["l0",
           "Executing [/a]",
           "l1",
           "Executing [/a/b]",
           "l2",
           "Executing [/a/b/d]",
           "l4",
           "Executing [/a/c]",
           "l3",
           "Executing [/a/c/e]",
           "l5"], "interleaved-extra-lines");

is_deeply(ser("Executing [/a]",
              "Executing [/a/b]",
              "Completed normally [/a/b]",
              "Completed normally [/a]"),
          ["Executing [/a]",
           "Executing [/a/b]",
           "Completed normally [/a/b]",
           "Completed normally [/a]"],
          "completed");

is_deeply(ser("Executing [/a]",
              "Executing [/a/b]",
              "Executing [/a/c]",
              "Executing [/a/b/d]",
              "Completed normally [/a/b/d]",
              "Completed normally [/a/b]",
              "Executing [/a/c/e]",
              "Completed normally [/a/c/e]",
              "Completed normally [/a/c]",
              "Completed normally [/a]"),
          ["Executing [/a]",
           "Executing [/a/b]",
           "Executing [/a/b/d]",
           "Completed normally [/a/b/d]",
           "Completed normally [/a/b]",
           "Executing [/a/c]",
           "Executing [/a/c/e]",
           "Completed normally [/a/c/e]",
           "Completed normally [/a/c]",
           "Completed normally [/a]"],
          "interleaved-completed");

is_deeply(ser("Executing [/a]",
              "Executing [/a/b]",
              "Completed normally [/a/b]",
              "Will not run [/a/c/d]",
              "Will not run [/a/c]",
              "Completed normally [/a]"),
          ["Executing [/a]",
           "Executing [/a/b]",
           "Completed normally [/a/b]",
           "Will not run [/a/c/d]",
           "Will not run [/a/c]",
           "Completed normally [/a]"],
          "not-run");

is_deeply(ser("Executing [/process]",
              q(Executing [/process/sequence[@name='Main']]),
              q(Executing [/process/sequence[@name='Main']/receive[@name='Receive1']]),
              q(Completed normally [/process/sequence[@name='Main']/receive[@name='Receive1']]),
              q(Executing [/process/sequence[@name='Main']/assign]),
              q(Executing [/process/sequence[@name='Main']/assign/copy[0]]),
              q(INSPECTION($processOutput.output) = @N@nonsensical@N@ []),
              q(Completed normally assignment to '$dummy_processOutput.output' [/process/sequence[@name='Main']/assign/copy[0]]),
              q(Executing [/process/sequence[@name='Main']/assign/copy[1]]),
              q(INSPECTION($processInput.input/ns0:ApprovalRequest[1]/ns0:amount[1]) = 150000 []),
              q(Completed normally assignment to '$dummy_processInput.input' [/process/sequence[@name='Main']/assign/copy[1]])),
          ["Executing [/process]",
           q(Executing [/process/sequence[@name='Main']]),
           q(Executing [/process/sequence[@name='Main']/receive[@name='Receive1']]),
           q(Completed normally [/process/sequence[@name='Main']/receive[@name='Receive1']]),
           q(Executing [/process/sequence[@name='Main']/assign]),
           q(Executing [/process/sequence[@name='Main']/assign/copy[0]]),
           q(INSPECTION($processOutput.output) = @N@nonsensical@N@ []),
           q(Completed normally assignment to '$dummy_processOutput.output' [/process/sequence[@name='Main']/assign/copy[0]]),
           q(Executing [/process/sequence[@name='Main']/assign/copy[1]]),
           q(INSPECTION($processInput.input/ns0:ApprovalRequest[1]/ns0:amount[1]) = 150000 []),
           q(Completed normally assignment to '$dummy_processInput.input' [/process/sequence[@name='Main']/assign/copy[1]])],
          "element-indices-are-ignored");

is_deeply(ser("Executing [/a]",
              "Executing [/a/b]",
              "foo []",
              "Completed normally [/a/b]",
              "Executing [/a/b]",
              "bar []",
              "Completed normally [/a/b]",
              "Completed normally [/a]"),
          ["Executing [/a]",
           "Executing [/a/b]",
           "foo []",
           "Completed normally [/a/b]",
           "Executing [/a/b]",
           "bar []",
           "Completed normally [/a/b]",
           "Completed normally [/a]"],
          "loop-not-interleaved");
