#!perl -T

use strict;
use warnings;

use Test::More tests => 9;

use Daikon qw(loadActiveBPELLog);
use TraceConverter qw(serialize);
use Data::Dumper;

my $rvLines   = loadActiveBPELLog("t/res/loops.log");
chop(@$rvLines);

my @ser_lines = serialize(@$rvLines);
my @expected_counters = ('@N@nonsensical@N@',
                         "0",
                         "1", "1",
                         "2", "2",
                         "3", "3",
                         "4");

my $i = 0;
foreach my $line (@ser_lines) {
  if ($line =~ /INSPECTION\(\$_contadorDias\) = ([^\s]+)/) {
    is($1, $expected_counters[$i],
       "$i-th occurrence should be ${expected_counters[$i]}");
    $i++;
  }
}
