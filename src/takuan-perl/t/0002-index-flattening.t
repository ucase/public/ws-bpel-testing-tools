#!perl -T

use strict;
use warnings;
use Daikon qw(C_TXSD C_TJAVA C_COMP C_NAME C_VARLIST C_VALUE C_MOD);
use FlattenDecls qw(indexFlatten indexFlattenVariable computeMaxLength createHashcodeType C_DUMMYHASHCODE_COMP);

use Test::More tests => 20;

use constant {
    C_DEFAULTCOMP => 1,
};

sub flatten {
    my ($var, $obType, $javaType, $comparability) = @_;

    my $pp = "pp";
    my $rhDest = {};
    indexFlattenVariable(
                         $pp,
                         $var,
                         {
                          C_TXSD() => $obType,
                          C_TJAVA() => $javaType,
                          C_COMP() => $comparability
                         },
                         $rhDest);
    return $rhDest->{$pp};
}

sub maxlength {
    return computeMaxLength(\@_, {});
}

sub pptrace {
    my ($name, @varNames) = @_;
    my %varList = map { vartrace($_) } @varNames;
    return { C_NAME() => $name, C_VARLIST() => \%varList };
}

sub vartrace {
    my ($var) = @_;
    # The exact value doesn't matter: we're only interested in the vector indices
    return ($var => {C_VALUE() => "pleaseignoreme", C_MOD() => 0});
}

my ($scalarName, $scalarTOb, $scalarTJava, $scalarComp) = ("x", "xsd:int # foo", "int", "0");
is_deeply(flatten($scalarName, $scalarTOb, $scalarTJava, $scalarComp),
          {$scalarName => {
                           C_TXSD() => $scalarTOb,
                           C_TJAVA() => $scalarTJava,
                           C_COMP() => $scalarComp,
                          }},
          "index-flatten-scalar");

is_deeply(flatten("x", "xsd:string # minlength=0, maxlength=20", "java.lang.String", "0"),
          {
           "x" => {
                   C_TXSD()   => "xsd:string # minlength=0, maxlength=20",
                   C_TJAVA() => "java.lang.String",
                   C_COMP() => "0",
                  }
          },
          "index-flatten-scalar-with-length");

is_deeply(flatten("x[]", "xsd:int[] # minlength=[1], maxlength=[1]", "int[]", "0[1]"),
          {
           "x" => createHashcodeType(),
           "x[]" => {
                     C_TXSD()   => "xsd:int[] # maxlength=1, minlength=1",
                     C_TJAVA() => "int[]",
                     C_COMP() => "0[1]",
                    }
          },
          "index-flatten-1level");

is_deeply(flatten("x[]", "xsd:int[] # minlength=1, maxlength=1", "int[]", "0[1]"),
          {
           "x" => createHashcodeType(),
           "x[]" => {
                     C_TXSD()   => "xsd:int[] # maxlength=1, minlength=1",
                     C_TJAVA() => "int[]",
                     C_COMP() => "0[1]",
                    }
          },
          "index-flatten-1level-previously-flattened");

is_deeply(flatten("x[0][]", "xsd:int[][] # minlength=[1 2], maxlength=[1 3]", "int[][]", "0[1][2]"),
          {
           "x[0]" => createHashcodeType(),
           "x[0][]" => {
                        C_TXSD()   => "xsd:int[] # maxlength=3, minlength=2",
                        C_TJAVA() => "int[]",
                        C_COMP() => "0[2]",
                       }
          },
          "index-flatten-2levels");

is_deeply(flatten("x[0][0][]", "xsd:int[][][] # minlength=[1 2 0], maxlength=[1 3 *]", "int[][][]", "0[1][2][3]"),
          {
           "x[0][0]" => createHashcodeType(),
           "x[0][0][]" => {
                           C_TXSD()   => "xsd:int[] # minlength=0",
                           C_TJAVA() => "int[]",
                           C_COMP() => "0[3]",
                          }
          },
          "index-flatten-3levels-unbounded");

is_deeply(maxlength(), {}, "maxlength-empty");

is_deeply(maxlength(pptrace("p.a:::ENTER", "x")),
          {
           "p.a" => {}},
          "maxlength-novectors");

is_deeply(maxlength(pptrace("p.a:::ENTER", "x[1]", "x[2]")),
          {
           "p.a" => {"x" => 2}},
          "maxlength-onepp");

is_deeply(maxlength(pptrace("p.a:::ENTER", "x[1]", "x[2]"),
                    pptrace("p.a:::ENTER", "x[1]")),
          {
           "p.a" => {"x" => 2}},
          "maxlength-twopp-max-preserved");

is_deeply(maxlength(pptrace("p.a:::ENTER", "x[1]", "x[2]"),
                    pptrace("p.a:::ENTER", "x[1]", "x[2]", "x[3]")),
          {
           "p.a" => {"x" => 3}},
          "maxlength-twopp-max-updated");

is_deeply(maxlength(pptrace("p.a:::ENTER", "x[1]", "x[2]"),
                    pptrace("p.a:::EXIT1", "x[1]")),
          {
           "p.a" => {"x" => 2}},
          "maxlength-entryexit-max-preserved");

is_deeply(maxlength(pptrace("p.a:::ENTER", "x[1]", "x[2]"),
                    pptrace("p.a:::EXIT1", "x[1]", "x[2]", "x[3]")),
          {
           "p.a" => {"x" => 3}},
          "maxlength-entryexit-max-updated");

is_deeply(maxlength(pptrace("p.a:::ENTER", "x[1].y", "x[2].y"),
                    pptrace("p.a:::EXIT1", "x[1].y", "x[2].y", "x[3].y")),
          {
           "p.a" => {"x" => 3}},
          "maxlength-entryexit-max-updated-two-components");

is_deeply(indexFlatten({}, {}),
          {
          },
          "flatten-empty");

is_deeply(indexFlatten({"p.a:::ENTER" => {"x" => {}}},
                       {
                       }),
          {
           "p.a:::ENTER" => {"x" => {}}},
          "flatten-scalar");

is_deeply(indexFlatten({"p.a:::ENTER" => {"x[].y" => {C_TJAVA() => "{xsd}int[]",
                                                      C_TXSD() => "int[]",
                                                      C_COMP() => "0[1]"
                                                     }}},
                       {
                        "p.a" => {"x" => 2}}),
          {
           "p.a:::ENTER" => {"x[1].y" => {C_TJAVA() => "{xsd}int[]",
                                          C_TXSD() => "int[]",
                                          C_COMP() => "0"
                                         },
                             "x[2].y" => {C_TJAVA() => "{xsd}int[]",
                                          C_TXSD() => "int[]",
                                          C_COMP() => "0"
                                         }}},
          "flatten-onelevel-twoelems");

is_deeply(indexFlatten({"p.a:::ENTER" => {"x[].y[]" => {C_TJAVA() => "{xsd}int[][]",
                                                        C_TXSD() => "int[][]",
                                                        C_COMP() => "0[1][2]"
                                                       }}},
                       {
                        "p.a" => {"x" => 2}}),
          {"p.a:::ENTER" => {"x[1].y" => createHashcodeType(),
                             "x[1].y[]" => {C_TJAVA() => "{xsd}int[]",
                                            C_TXSD() => "int[]",
                                            C_COMP() => "0[2]"
                                           },
                             "x[2].y" => createHashcodeType(),
                             "x[2].y[]" => {C_TJAVA() => "{xsd}int[]",
                                            C_TXSD() => "int[]",
                                            C_COMP() => "0[2]"
                                           }}},
          "flatten-twolevel-twoelems");

is_deeply(indexFlatten({"p.a:::ENTER" => {"x[].y[].z[]" => {C_TJAVA() => "{xsd}int[][][]",
                                                            C_TXSD() => "int[][][]",
                                                            C_COMP() => "0[1][2][3]"
                                                           },
                                          "x[].u[].w[]" => {C_TJAVA() => "{xsd}int[][][]",
                                                            C_TXSD() => "int[][][]",
                                                            C_COMP() => "0[1][2][3]"
                                                           }}},
                       {"p.a" => {"x" => 2,
                                  "x[1].u" => 3,
                                  "x[2].u" => 2}}),
          {"p.a:::ENTER" => {"x[1].u[1].w" => createHashcodeType(),
                             "x[1].u[1].w[]" => {C_TJAVA() => "{xsd}int[]",
                                                 C_TXSD() => "int[]",
                                                 C_COMP() => "0[3]"
                                                },
                             "x[1].u[2].w" => createHashcodeType(),
                             "x[1].u[2].w[]" => {C_TJAVA() => "{xsd}int[]",
                                                 C_TXSD() => "int[]",
                                                 C_COMP() => "0[3]"
                                                },
                             "x[1].u[3].w" => createHashcodeType(),
                             "x[1].u[3].w[]" => {C_TJAVA() => "{xsd}int[]",
                                                 C_TXSD() => "int[]",
                                                 C_COMP() => "0[3]"
                                                },
                             "x[2].u[1].w" => createHashcodeType(),
                             "x[2].u[1].w[]" => {C_TJAVA() => "{xsd}int[]",
                                                 C_TXSD() => "int[]",
                                                 C_COMP() => "0[3]"
                                                },
                             "x[2].u[2].w" => createHashcodeType(),
                             "x[2].u[2].w[]" => {C_TJAVA() => "{xsd}int[]",
                                                 C_TXSD() => "int[]",
                                                 C_COMP() => "0[3]"
                                                },
                             "x[1].y[1].z" => createHashcodeType(),
                             "x[1].y[1].z[]" => {C_TJAVA() => "{xsd}int[]",
                                                 C_TXSD() => "int[]",
                                                 C_COMP() => "0[3]"
                                                },
                             "x[2].y[1].z" => createHashcodeType(),
                             "x[2].y[1].z[]" => {C_TJAVA() => "{xsd}int[]",
                                                 C_TXSD() => "int[]",
                                                 C_COMP() => "0[3]"
                                                }}},
          "flatten-threelevel-unused-emptynotdiscarded");

is_deeply(indexFlatten({"p.a:::ENTER" => {"x[].y[].z[]" => {C_TJAVA() => "{xsd}int[][][]",
                                                            C_TXSD() => "int[][][]",
                                                            C_COMP() => "0[1][2][3]"
                                                           },
                                          "x[].u[].w[]" => {C_TJAVA() => "{xsd}int[][][]",
                                                            C_TXSD() => "int[][][]",
                                                            C_COMP() => "0[1][2][3]"
                                                           }}},
                       {"p.a" => {"x" => 2,
                                  "x[1].u" => 2,
                                  "x[1].u[1].w" => 1,
                                  "x[2].y" => 2,
                                  "x[2].y[2].z" => 1}},
                       "discard-unused"),
          {"p.a:::ENTER" => {"x[1].u[1].w" => createHashcodeType(),
                             "x[1].u[1].w[]" => {C_TJAVA() => "{xsd}int[]",
                                                 C_TXSD() => "int[]",
                                                 C_COMP() => "0[3]"
                                                },
                             "x[2].y[2].z" => createHashcodeType(),
                             "x[2].y[2].z[]" => {C_TJAVA() => "{xsd}int[]",
                                                 C_TXSD() => "int[]",
                                                 C_COMP() => "0[3]"
                                                }}},
          "flatten-threelevel-unused-emptydiscarded");
