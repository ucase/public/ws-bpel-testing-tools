#!perl -T

#
# Tests for the parseConstraints/formatConstraints in Daikon.pm
# Copyright (C) 2011 Antonio García-Domínguez
#

use strict;
use warnings;

use Daikon qw(parseConstraints formatConstraints);
use Test::More tests => 14;

is_deeply(parseConstraints("int"),
	  ["int", {}],
	  "parse-noconstraint");

is_deeply(parseConstraints("int # x=y"),
	  ["int", {"x" => "y"}],
	  "parse-1constraint");

is_deeply(parseConstraints("int # x=y, z=w"),
	  ["int", {"x" => "y", "z" => "w"}],
	  "parse-2constraints");

is_deeply(parseConstraints("int # minvalue=[]"),
	  ["int", {"minvalue" => []}],
	  "parse-emptylist");

is_deeply(parseConstraints("int # minvalue=[1]"),
	  ["int", {"minvalue" => ["1"]}],
	  "parse-list1element");

is_deeply(parseConstraints("int # minvalue=[1 42]"),
	  ["int", {"minvalue" => ["1", "42"]}],
	  "parse-list2elements");

is_deeply(parseConstraints("int # validvalues=[\"1\" \"42\"]"),
	  ["int", {"validvalues" => [q("1"), q("42")]}],
	  "parse-liststrings");

is_deeply(formatConstraints({}),
	  "",
	  "format-noconstraint");

is_deeply(formatConstraints({"x" => "y"}),
	  " # x=y",
	  "format-1constraint");

is_deeply(formatConstraints({"x" => "y", "z" => "w"}),
	  " # x=y, z=w",
	  "format-2constraints");

is_deeply(formatConstraints({"minvalue" => []}),
	  " # minvalue=[]",
	  "format-emptylist");

is_deeply(formatConstraints({"minvalue" => [1]}),
	  " # minvalue=[1]",
	  "format-list1element");

is_deeply(formatConstraints({"minvalue" => [1, 42]}),
	  " # minvalue=[1 42]",
	  "format-list2elements");

is_deeply(formatConstraints({"minvalue" => [q("1"), q("42")]}),
	  " # minvalue=[\"1\" \"42\"]",
	  "format-liststrings");
