#!perl -T

use strict;
use warnings;

use Daikon qw(cargarDecls cargarDTrace C_NAME C_VARLIST C_VALUE C_TXSD C_TDUMMYHASH);
use FlattenTraces qw(computeByNodeSetFlattening flattenDTrace);
use Test::More tests => 16;

use Data::Dumper;

my ($rhTypeInfo, $dummy)  = cargarDecls("t/res/flatten-attributes.flat.decls");
my $rvRawDTrace = cargarDTrace("t/res/flatten-attributes.dtrace");
my $rvFlatDTrace = flattenDTrace($rhTypeInfo, $rvRawDTrace, \&computeByNodeSetFlattening);

my $rhFirstPP = $rvFlatDTrace->[0];
my $rhPPName = $rhFirstPP->{C_NAME()};
my $rhPPValues = $rhFirstPP->{C_VARLIST()};

while (my ($varName, $rhVarInfo) = each(%$rhPPValues)) {
  my $xsdType = $rhTypeInfo->{$rhPPName}->{$varName}->{C_TXSD()};
  my $value = $rhVarInfo->{C_VALUE()};
  if ($xsdType eq C_TDUMMYHASH()) {
    is($value, "nonsensical", "$varName should be nonsensical");
  } else {
    isnt($value, "nonsensical", "$varName should not be nonsensical");
  }
}
