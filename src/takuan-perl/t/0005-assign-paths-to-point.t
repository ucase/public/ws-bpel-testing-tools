#!perl -T

use strict;
use warnings;

use Daikon qw(rutaAsigAPunto);
use Test::More tests => 8;

is(rutaAsigAPunto("squaresSum", "/process/sequence/assign"),
   "squaresSum._process1_sequence1:::ENTER",
   "pathtopoint-nonames");

is(rutaAsigAPunto("squaresSum", "/process/sequence"),
   "squaresSum._process1_sequence1:::ENTER",
   "pathtopoint-nonames-noassign");

is(rutaAsigAPunto("squaresSum", "/process/sequence/assign[2]"),
   "squaresSum._process1_sequence1:::EXIT1",
   "pathtopoint-nonames-exit");

is(rutaAsigAPunto("squaresSum",
		  q(/process/sequence/sequence[@name='Foo']/assign)),
   "squaresSum._process1_sequence1_Foo:::ENTER",
   "pathtopoint-lastwithname");

is(rutaAsigAPunto("squaresSum",
		  q(/process/sequence[@name='Foo']/sequence/assign)),
   "squaresSum._process1_Foo_sequence1:::ENTER",
   "pathtopoint-middlewithname");

is(rutaAsigAPunto("squaresSum",
		  q(/process/sequence/forEach[@name='GenerateResults']/scope[@name='ResultLoopBody'][instance()=1]/sequence/assign)),
   "squaresSum._process1_sequence1_GenerateResults_ResultLoopBody_sequence1:::ENTER",
   "pathtopoint-instancenumber");

is(rutaAsigAPunto("squaresSum",
		  q(/process/sequence[@name='main']/if[@name='Switch_10']/if-condition/sequence/assign)),
   "squaresSum._process1_main_Switch_10_sequence1:::ENTER",
   "pathtopoint-ifcondition");

# Comparability uses rutaAsigAPunto with things that aren't
# assignments - we shouldn't drop the last component of the path in
# those cases, to avoid losing information. We will assume that we are
# entering that program point.
is(rutaAsigAPunto("squaresSum",
		  q(/process/sequence[@name='main']/sequence[2])),
   "squaresSum._process1_main_sequence2:::ENTER",
   "pathtopoint-noassign")
