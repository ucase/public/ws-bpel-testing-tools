#!perl -T

use strict;
use warnings;
use Daikon qw(C_TXSD C_TJAVA C_COMP);
use FlattenDecls qw(nodesetFlatten nodesetFlattenVariable createHashcodeType C_DUMMYHASHCODE_COMP);

use Test::More tests => 16;

sub flatten {
    my ($var, $obType, $javaType, $comparability) = @_;

    my $pp = "pp";
    my $rhDest = {};
    nodesetFlattenVariable(
        $pp,
        $var,
        {
            C_TXSD() => $obType,
            C_TJAVA() => $javaType,
            C_COMP() => $comparability
        },
        $rhDest);
    return $rhDest->{$pp};
}

my ($scalarName, $scalarTOb, $scalarTJava, $scalarComp) = ("x", "xsd:int # foo", "int", "0");
is_deeply(flatten($scalarName, $scalarTOb, $scalarTJava, $scalarComp),
          {$scalarName => {
              C_TXSD() => $scalarTOb,
              C_TJAVA() => $scalarTJava,
              C_COMP() => $scalarComp,
           }},
          "nodeset-varflatten-scalar");

is_deeply(flatten("x[]", "xsd:int[] # minlength=[1], maxlength=[1]", "int[]", "0[1]"),
          {
              "x" => createHashcodeType(),
              "x[]" => {
                  C_TXSD()   => "xsd:int[] # maxlength=1, minlength=1",
                  C_TJAVA() => "int[]",
                  C_COMP() => "0[1]",
              }
          },
          "nodeset-varflatten-1level");

is_deeply(flatten("x[].y", "xsd:int[] # minlength=[1], maxlength=[1]", "int[]", "0[1]"),
          {
           "x.y" => createHashcodeType(),
           "x.y[]" => {
                       C_TXSD()   => "xsd:int[] # maxlength=1, minlength=1",
                       C_TJAVA() => "int[]",
                       C_COMP() => "0[1]",
                      }
          },
          "nodeset-varflatten-1level-withscalarchild");

is_deeply(flatten("x[]", "xsd:int[] # minlength=1, maxlength=1", "int[]", "0[1]"),
          {
              "x" => createHashcodeType(),
              "x[]" => {
                  C_TXSD()   => "xsd:int[] # maxlength=1, minlength=1",
                  C_TJAVA() => "int[]",
                  C_COMP() => "0[1]",
              }
          },
          "nodeset-varflatten-1level-previously-flattened");

is_deeply(flatten("x[][]", "xsd:int[][] # minlength=[2 2], maxlength=[3 3]", "int[][]", "0[1][2]"),
          {
              "x" => createHashcodeType(),
              "x[]" => {
                  C_TXSD()   => "xsd:int[] # maxlength=9, minlength=4",
                  C_TJAVA() => "int[]",
                  C_COMP() => "0[2]",
              }
          },
          "nodeset-varflatten-2levels");

is_deeply(flatten("x[][][]", "xsd:int[][][] # minlength=[1 2 4], maxlength=[1 3 6]", "int[][][]", "0[1][2][3]"),
          {
              "x" => createHashcodeType(),
              "x[]" => {
                  C_TXSD()   => "xsd:int[] # maxlength=18, minlength=8",
                  C_TJAVA() => "int[]",
                  C_COMP() => "0[3]",
              }
          },
          "nodeset-varflatten-3levels");

is_deeply(flatten("x[][][]", "xsd:int[][][] # minlength=[1 2 0], maxlength=[1 3 *]", "int[][][]", "0[1][2][3]"),
          {
              "x" => createHashcodeType(),
              "x[]" => {
                  C_TXSD()   => "xsd:int[] # minlength=0",
                  C_TJAVA() => "int[]",
                  C_COMP() => "0[3]",
              }
          },
          "nodeset-varflatten-3levels-unbounded");

is_deeply(nodesetFlatten({}, {}),
          {},
          "nodeset-flatten-empty");

is_deeply(nodesetFlatten({"p.a:::ENTER" => {"x" => {C_TJAVA() => "java.lang.String",
                                                    C_TXSD() => "string",
                                                    C_COMP() => "0"
                                                   }}},
                         {}),
          {"p.a:::ENTER" => {"x" => {C_TJAVA() => "java.lang.String",
                                     C_TXSD() => "string",
                                     C_COMP() => "0"
                                    }}},
          "nodeset-flatten-scalar");

is_deeply(nodesetFlatten({"p.a:::ENTER" => {"x[]" => {C_TJAVA() => "short[]",
                                                      C_TXSD() => "{xsd}short[]",
                                                      C_COMP() => "0[1]"
                                                     }}},
                         {}),
          {"p.a:::ENTER" => {"x" => createHashcodeType(),
                             "x[]" => {C_TJAVA() => "short[]",
                                       C_TXSD() => "{xsd}short[]",
                                       C_COMP() => "0[1]"
                                      }}},
          "nodeset-flatten-onelevel-donotdiscardempty");

is_deeply(nodesetFlatten({"p.a:::ENTER" => {"x[]" => {C_TJAVA() => "short[]",
                                                      C_TXSD() => "{xsd}short[]",
                                                      C_COMP() => "0[1]"
                                                     }}},
                         {"p.a" => {"x" => 2}},
                         "discardEmpty"),
          {"p.a:::ENTER" => {"x" => createHashcodeType(),
                             "x[]" => {C_TJAVA() => "short[]",
                                       C_TXSD() => "{xsd}short[]",
                                       C_COMP() => "0[1]"
                                      }}},
          "nodeset-flatten-onelevel-discardempty");

is_deeply(nodesetFlatten({"p.a:::ENTER" => {"x[].y[]" => {C_TJAVA() => "short[][]",
                                                          C_TXSD() => "{xsd}short[][]",
                                                          C_COMP() => "0[1][2]"
                                                         }}},
                         {}),
          {"p.a:::ENTER" => {"x.y" => createHashcodeType(),
                             "x.y[]" => {C_TJAVA() => "short[]",
                                         C_TXSD() => "{xsd}short[]",
                                         C_COMP() => "0[2]"
                                        }}},
          "nodeset-flatten-twolevels-donotdiscardempty");

is_deeply(nodesetFlatten({"p.a:::ENTER" => {"x[].y[]" => {C_TJAVA() => "short[][]",
                                                          C_TXSD() => "{xsd}short[][]",
                                                          C_COMP() => "0[1][2]"},
                                            "x[].w[]" => {C_TJAVA() => "double[][]",
                                                          C_TXSD() => "{xsd}decimal[][]",
                                                          C_COMP() => "0[1][2]"},
                                           },
                         },
                         {"p.a" => {"x" => 1, "x[1].y" => 2}},
                         "discardEmpty"),
          {"p.a:::ENTER" => {"x.y" => createHashcodeType(),
                             "x.y[]" => {C_TJAVA() => "short[]",
                                         C_TXSD() => "{xsd}short[]",
                                         C_COMP() => "0[2]"},
                             }},
          "nodeset-flatten-twolevels-discardempty");

is_deeply(nodesetFlatten({"p.a:::ENTER" => {"x[].y[].z[]" => {C_TJAVA() => "short[][][]",
                                                              C_TXSD() => "{xsd}short[][][]",
                                                              C_COMP() => "0[1][2][3]"}}},
                         {}),
          {"p.a:::ENTER" => {"x.y.z" => createHashcodeType(),
                             "x.y.z[]" => {C_TJAVA() => "short[]",
                                           C_TXSD() => "{xsd}short[]",
                                           C_COMP() => "0[3]"}}},
          "nodeset-flatten-threelevels-donotdiscardempty");

is_deeply(nodesetFlatten({"p.a:::ENTER" => {"x[].y[].z[]" => {C_TJAVA() => "short[][][]",
                                                              C_TXSD() => "{xsd}short[][][]",
                                                              C_COMP() => "0[1][2][3]"},
                                            "x[].u[].w[]" => {C_TJAVA() => "double[][][]",
                                                              C_TXSD() => "{xsd}decimal[][][]",
                                                              C_COMP() => "0[1][2][3]"},
                                           },
                         },
                         {"p.a" => {"x" => 2,
                                    "x[1].y" => 2,
                                    "x[1].y[1].z" => 1,
                                    "x[2].y" => 1,
                                   }},
                         "discardEmpty"),
          {"p.a:::ENTER" => {"x.y.z" => createHashcodeType(),
                             "x.y.z[]" => {C_TJAVA() => "short[]",
                                           C_TXSD() => "{xsd}short[]",
                                           C_COMP() => "0[3]"}}},
          "nodeset-flatten-threelevels-discardempty");

is_deeply(nodesetFlatten({"p.a:::ENTER" => {"x[].y[].z[]" => {C_TJAVA() => "short[][][]",
                                                              C_TXSD() => "{xsd}short[][][]",
                                                              C_COMP() => "0[1][2][3]"},
                                            "x[].u[].w[]" => {C_TJAVA() => "double[][][]",
                                                              C_TXSD() => "{xsd}decimal[][][]",
                                                              C_COMP() => "0[1][2][3]"},
                                           },
                         },
                         {"p.a" => {"x" => 2,
                                    "x[1].y" => 2,
                                    "x[2].y" => 1,
                                   }},
                         "discardEmpty"),
          {},
          "nodeset-flatten-threelevels-discardempty-lastdim");
