#!perl -T

use strict;
use warnings;

use Daikon qw(C_TXSD C_TJAVA C_COMP);
use TraceConverter qw(imprimirPunto);

use Test::More tests => 10;

use constant {
    C_PROCESS => "sample",
    C_PATH => "/process/sequence/assign",
    C_PP => "sample._process1_sequence1:::ENTER",
};

sub printPoint {
    my ($rhInspections, $rhVars) = @_;

    # Print to a scalar instead of to stdout
    my ($results, $fhResults);
    open($fhResults, '>', \$results);
    imprimirPunto(C_PROCESS(), C_PATH(),
		  $rhInspections, {C_PP() => $rhVars},
		  $fhResults);

    return $results;
}

sub formatAsPP {
    my ($pp, @lines) = @_;
    return (join "\n", ($pp, @lines)) . "\n\n";
}

is(printPoint(
       { "x" => "1" },
       {
	   "x" => {
	       C_TXSD() => "xsd:int",
	       C_TJAVA() => "int",
	       C_COMP() => 1
	   },
       }),
   formatAsPP(C_PP(), "x", "1", "0"),
   "convert-scalar");

is(printPoint(
       { "x" => "A" },
       {
	   "x" => {
	       C_TXSD() => "xsd:string",
	       C_TJAVA() => "java.lang.String",
	       C_COMP() => 1,
	   },
       },
   ),
   formatAsPP(C_PP(), "x", q("A"), "0"),
   "convert-scalar-string");

is(printPoint(
       {
	   "x[1]" => "1",
	   "x[2]" => "2",
       },
       {
	   "x[]" => {
	       C_TXSD() => "xsd:int[]",
	       C_TJAVA() => "int[]",
	       C_COMP() => 1,
	   },
       }),
   formatAsPP(C_PP(), "x[1]", "1", "0", "x[2]", "2", "0"),
    "convert-list-1level");

is(printPoint(
       {
	   "x[1]" => "1",
	   "x[2]" => "2",
       },
       {
	   "x[]" => {
	       C_TXSD() => "xsd:string[]",
	       C_TJAVA() => "java.lang.String[]",
	       C_COMP() => 1,
	   },
       }),
   formatAsPP(C_PP(), "x[1]", q("1"), "0", "x[2]", q("2"), "0"),
    "convert-list-1level-string");

is(printPoint(
       {
	   "x[1].y[1]" => "1",
	   "x[1].y[2]" => "2",
	   "x[2].y[1]" => "3",
	   "x[2].y[2]" => "4",
       },
       {
	   "x[].y[]" => {
	       C_TXSD() => "xsd:int[][]",
	       C_TJAVA() => "int[][]",
	       C_COMP() => 1,
	   },
       }
   ),
   formatAsPP(C_PP(),
	      "x[1].y[1]", "1", "0",
	      "x[1].y[2]", "2", "0",
	      "x[2].y[1]", "3", "0",
	      "x[2].y[2]", "4", "0"),
   "convert-list-2levels");

is(printPoint(
       {
	   "x[1].y[1]" => "1",
	   "x[1].y[2]" => "2",
	   "x[2].y[1]" => "3",
	   "x[2].y[2]" => "4",
       },
       {
	   "x[].y[]" => {
	       C_TXSD() => "xsd:string[][]",
	       C_TJAVA() => "java.lang.String[][]",
	       C_COMP() => 1,
	   },
       }
   ),
   formatAsPP(C_PP(),
	      "x[1].y[1]", q("1"), "0",
	      "x[1].y[2]", q("2"), "0",
	      "x[2].y[1]", q("3"), "0",
	      "x[2].y[2]", q("4"), "0"),
   "convert-list-2levels-string");

is(printPoint(
       {
	   "x" => "1 2 3",
       },
       {
	   "x~elems[]" => {
	       C_TXSD() => "xsd:int[]",
	       C_TJAVA() => "int[]",
	       C_COMP() => 1
	   },
       }),
   formatAsPP(C_PP(),
	      "x~elems[1]", "1", "0",
	      "x~elems[2]", "2", "0",
	      "x~elems[3]", "3", "0"),
   "convert-scalar-listcontent");

is(printPoint(
       {
	   "x[1]" => "A B C",
	   "x[2]" => "D E F",
       },
       {
	   "x[]~elems[]" => {
	       C_TXSD() => "xsd:string[][]",
	       C_TJAVA() => "java.lang.String[][]",
	       C_COMP() => 1
	   },
       }),
   formatAsPP(C_PP(),
	      "x[1]~elems[1]", q("A"), "0",
	      "x[1]~elems[2]", q("B"), "0",
	      "x[1]~elems[3]", q("C"), "0",
	      "x[2]~elems[1]", q("D"), "0",
	      "x[2]~elems[2]", q("E"), "0",
	      "x[2]~elems[3]", q("F"), "0"),
   "convert-1level-listcontent-string");

is(printPoint(
       {
	   q(x[1].@RPH) => "12",
       },
       {
	   q(x[].@RPH[]) => {
	       C_TXSD() => "xsd:int",
	       C_TJAVA() => "int",
	       C_COMP() => 1,
	   },
       }),
   formatAsPP(C_PP(),
	      q(x[1].@RPH[1]), "12", "0"),
   "convert-attribute");

is(printPoint(
       {
	   q(x[1].@ItemRPH_List) => "2 3 4",
       },
       {
	   q(x[].@ItemRPH_List[]~elems[]) => {
	       C_TXSD() => "xsd:string[][]",
	       C_TJAVA() => "java.lang.String[][]",
	       C_COMP() => 1
	   },
       }),
   formatAsPP(C_PP(),
	      q(x[1].@ItemRPH_List[1]~elems[0]), q("2"), "0",
	      q(x[1].@ItemRPH_List[1]~elems[1]), q("3"), "0",
	      q(x[1].@ItemRPH_List[1]~elems[2]), q("4"), "0"),
   "convert-list-in-attribute");
