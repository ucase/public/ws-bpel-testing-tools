#!perl -T

use strict;
use warnings;
use Daikon qw(cargarDecls C_TXSD C_TJAVA C_COMP);

use Test::More tests => 3;

# Sample Daikon declarations file
my $rvSampleLines = [
    "DECLARE\n",
    "tacService._process1_sequence1:::ENTER\n",
    "lineCount\n",
    "{http://www.w3.org/2001/XMLSchema}unsignedInt\n",
    "long\n",
    "0\n",
    "\n",
    "DECLARE\n",
    "tacService._process1_sequence1:::EXIT1\n",
    "lineCount\n",
    "{http://www.w3.org/2001/XMLSchema}unsignedInt\n",
    "long\n",
    "0"];

# Expected results of parsing
my $expectedProcessName = "tacService";
my $rhExpectedVarsByPP = {
    "tacService._process1_sequence1:::ENTER" => {
	"lineCount" => {
	    C_TXSD()   => "{http://www.w3.org/2001/XMLSchema}unsignedInt",
	    C_TJAVA() => "long",
	    C_COMP()  => 0,
	},
    },
    "tacService._process1_sequence1:::EXIT1" => {
	"lineCount" => {
	    C_TXSD()   => "{http://www.w3.org/2001/XMLSchema}unsignedInt",
	    C_TJAVA() => "long",
	    C_COMP()  => 0,
	},
    },
};

my ($rhVarsByPP, $processName) = cargarDecls($rvSampleLines);
is_deeply([$rhVarsByPP, $processName],
	  [$rhExpectedVarsByPP, $expectedProcessName],
	  "load-decls-no-trailing-newline");

$rvSampleLines->[-1] .= "\n";
($rhVarsByPP, $processName) = cargarDecls($rvSampleLines);
is_deeply([$rhVarsByPP, $processName],
	  [$rhExpectedVarsByPP, $expectedProcessName],
	  "load-decls-1-trailing-newline");

push @$rvSampleLines, "\n";
($rhVarsByPP, $processName) = cargarDecls($rvSampleLines);
is_deeply([$rhVarsByPP, $processName],
	  [$rhExpectedVarsByPP, $expectedProcessName],
	  "load-decls-2-trailing-newlines");
