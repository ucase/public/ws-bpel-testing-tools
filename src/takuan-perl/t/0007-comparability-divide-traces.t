#!perl -T

use Comparability qw(splitTraces);
use Test::More tests => 6;

sub traces {
    my ($rvPP, $text) = @_;

    my %hTypeInfo = map { ("myproc." . ${_}) => {} } @$rvPP;
    return splitTraces("myproc", \%hTypeInfo, $text);
}

sub split_lines {
  my @lines = split /\n/, $_[0];
  return \@lines;
}

is_deeply(traces([], []), {}, "empty");

########################################

is_deeply(traces([], split_lines(<<'LOG'
Executing [/process/sequence/if[@name='IfSmallAmount']]
LOG
		 )), {}, "no-lines-of-interest");

########################################

is_deeply(traces(["_process1_sequence1:::ENTER"], split_lines(<<'LOG'
Executing [/process/sequence/assign/copy[4]]
INSPECTION($request.amount) = 1500 []
Completed normally assignment to '$dummy_request.amount' [/process/sequence/assign/copy[4]]
Completed normally [/process/sequence/assign]
Executing [/process/sequence/if[@name='IfSmallAmount']]
---- INICIO AMBITO ---- []
Ruta accedida: $request.amount []
---- FIN AMBITO ---- []
LOG
		)),
	  {"myproc._process1_sequence1:::ENTER" => split_lines(<<'LOG'
---- INICIO AMBITO ---- []
Ruta accedida: $request.amount []
---- FIN AMBITO ---- []
LOG
					       )
	  },
	  "one-pp");

########################################

is_deeply(traces(["_process1_MarketplaceFlow_MarketplaceSwitch_sequence1:::ENTER",
		  "_process1_MarketplaceFlow_MarketplaceSwitch_else1_sequence1:::ENTER"],
		 split_lines(<<'LOG'
Executing [/process/flow[@name='MarketplaceFlow']/if[@name='MarketplaceSwitch']]
---- INICIO AMBITO ---- []
Ruta accedida: $sellerInfo.askingPrice []
Ruta accedida: $buyerInfo.offer []
---- FIN AMBITO ---- []
Executing [/process/flow[@name='MarketplaceFlow']/if[@name='MarketplaceSwitch']/if-condition/sequence/a
Executing [/process/flow[@name='MarketplaceFlow']/if[@name='MarketplaceSwitch']/if-condition/sequence/assign[@name='SuccessAssign']/copy[0]]
---- INICIO AMBITO ---- []
---- FIN AMBITO ---- []
LOG
		 )),
	  {"myproc._process1_MarketplaceFlow_MarketplaceSwitch_sequence1:::ENTER" => split_lines(<<'LOG'
---- INICIO AMBITO ---- []
Ruta accedida: $sellerInfo.askingPrice []
Ruta accedida: $buyerInfo.offer []
---- FIN AMBITO ---- []
---- INICIO AMBITO ---- []
---- FIN AMBITO ---- []
LOG
												),
	   "myproc._process1_MarketplaceFlow_MarketplaceSwitch_else1_sequence1:::ENTER" => split_lines(<<'LOG'
---- INICIO AMBITO ---- []
Ruta accedida: $sellerInfo.askingPrice []
Ruta accedida: $buyerInfo.offer []
---- FIN AMBITO ---- []
LOG
												     ),
	  },
	  "if-condition");

is_deeply(traces(["_process1_main_Flow_1_Sequence_2_GoogleSearchScope_Sequence_3:::ENTER"],
		 split_lines(<<'LOG'
Executing [/process/sequence[@name='main']/flow[@name='Flow_1']/sequence[@name='Sequence_2']/scope[@name='GoogleSearchScope']/sequence[@name='Sequence_3']/assign[@name='InitializeGoogleValues']/copy[1]]
---- INICIO AMBITO ---- []
Ruta accedida: $inputVariable.payload/client:MetaSearchProcessRequest[1]/client:query[1] []
---- FIN AMBITO ---- []
Completed normally assignment to '$Google_doGoogleSearch_InputVariable.request/ns1:GoogleSearchRequestElement[1]/query[1]' [/process/sequence[@name='main']/flow[@name='Flow_1']/sequence[@name='Sequence_2']/scope[@name='GoogleSearchScope']/sequence[@name='Sequence_3']/assign[@name='InitializeGoogleValues']/copy[1]]
Executing [/process/sequence[@name='main']/flow[@name='Flow_1']/sequence[@name='Sequence_2']/scope[@name='GoogleSearchScope']/sequence[@name='Sequence_3']/assign[@name='InitializeGoogleValues']/copy[2]]
---- INICIO AMBITO ---- []
Ruta accedida: $inputVariable.payload/client:MetaSearchProcessRequest[1]/client:country[1] []
---- FIN AMBITO ---- []
Completed normally assignment to '$Google_doGoogleSearch_InputVariable.request/ns1:GoogleSearchRequestElement[1]/restrict[1]' [/process/sequence[@name='main']/flow[@name='Flow_1']/sequence[@name='Sequence_2']/scope[@name='GoogleSearchScope']/sequence[@name='Sequence_3']/assign[@name='InitializeGoogleValues']/copy[2]]
LOG
		)),
	  {"myproc._process1_main_Flow_1_Sequence_2_GoogleSearchScope_Sequence_3:::ENTER" => split_lines(<<'LOG'
---- INICIO AMBITO ---- []
Ruta accedida: $inputVariable.payload/client:MetaSearchProcessRequest[1]/client:query[1] []
Completed normally assignment to '$Google_doGoogleSearch_InputVariable.request/ns1:GoogleSearchRequestElement[1]/query[1]' [/process/sequence[@name='main']/flow[@name='Flow_1']/sequence[@name='Sequence_2']/scope[@name='GoogleSearchScope']/sequence[@name='Sequence_3']/assign[@name='InitializeGoogleValues']/copy[1]]
---- FIN AMBITO ---- []
---- INICIO AMBITO ---- []
Ruta accedida: $inputVariable.payload/client:MetaSearchProcessRequest[1]/client:country[1] []
Completed normally assignment to '$Google_doGoogleSearch_InputVariable.request/ns1:GoogleSearchRequestElement[1]/restrict[1]' [/process/sequence[@name='main']/flow[@name='Flow_1']/sequence[@name='Sequence_2']/scope[@name='GoogleSearchScope']/sequence[@name='Sequence_3']/assign[@name='InitializeGoogleValues']/copy[2]]
---- FIN AMBITO ---- []
LOG
													),
	  },
	  "assignment-after-scope");

is_deeply(traces(["_process1_main_Flow_1_Sequence_1_MSNSearchScope_Sequence_4_Switch_10:::ENTER"],
		 split_lines(<<'LOG'
Executing [/process/sequence[@name='main']/flow[@name='Flow_1']/sequence[@name='Sequence_1']/scope[@name='MSNSearchScope']/sequence[@name='Sequence_4']/if[@name='Switch_10']]
---- INICIO AMBITO ---- []
---- INICIO AMBITO ---- []
Ruta accedida: $inputVariable.payload/client:MetaSearchProcessRequest[1]/client:country[1] []
---- FIN AMBITO ---- []
---- INICIO AMBITO ---- []
Ruta accedida: $inputVariable.payload/client:MetaSearchProcessRequest[1]/client:language[1] []
---- FIN AMBITO ---- []
---- FIN AMBITO ---- []
LOG
		 )),
	  {"myproc._process1_main_Flow_1_Sequence_1_MSNSearchScope_Sequence_4_Switch_10:::ENTER" => split_lines(<<'LOG'
---- INICIO AMBITO ---- []
---- INICIO AMBITO ---- []
Ruta accedida: $inputVariable.payload/client:MetaSearchProcessRequest[1]/client:country[1] []
---- FIN AMBITO ---- []
---- INICIO AMBITO ---- []
Ruta accedida: $inputVariable.payload/client:MetaSearchProcessRequest[1]/client:language[1] []
---- FIN AMBITO ---- []
---- FIN AMBITO ---- []
LOG
														),
	   },
	  "nested");
