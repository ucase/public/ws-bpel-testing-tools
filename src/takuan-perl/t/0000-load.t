#!perl -T

use Test::More tests => 7;

BEGIN {
  diag( "Testing takuan-perl, Perl $], $^X" );

  foreach (qw(
	       Comparability
	       Coverage
	       Daikon
	       FlattenDecls
	       FlattenTraces
	       TraceConverter
	       Parallel::ForkManager::Mock
	    )) {
    use_ok($_);
  }
}
