#!/usr/bin/perl

# Declaration flattening script - nodeset or index schemes
#
# Antonio García-Domínguez, Javier Santacruz

use strict;
use warnings;

use Cwd 'abs_path';
use lib ($0 =~ m,(.*)/[^/]+,);

use FlattenDecls qw(printFlattenedDecls);
use Getopt::Long;

### ARGUMENT PROCESSING
my $outputFile = undef;
my $disableXMLSchema = undef;
my $discardEmptyLists = undef;
my $indexFlattening = undef;
GetOptions(
    "output-file=s" => \$outputFile,
    "disable-xml-schema" => \$disableXMLSchema,
    "discard-empty-lists" => \$discardEmptyLists,
    "index-flattening|i" => \$indexFlattening,
);

if (scalar @ARGV < 2) {
  print STDERR "Usage: $0 [--disable-xml-schema|--discard-empty-lists|--index-flattening|--output-file=s] (decls) (.dtrace files)\n";
  exit 1;
}
if (defined $outputFile) {
  open(SALIDA, '>', $outputFile) or die "Cannot open the given output file $outputFile";
  select(SALIDA);
}

my ($pathToDecls, @pathsToTraces)  = @ARGV;

### MAIN BODY

printFlattenedDecls($disableXMLSchema, $discardEmptyLists, $indexFlattening,
		    $pathToDecls, @pathsToTraces);
exit 0;
