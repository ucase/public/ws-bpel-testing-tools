#!/usr/bin/perl

use strict;
use warnings;

use Cwd 'abs_path';
use lib ($0 =~ m,(.*)/[^/]+,);

use TraceConverter qw(printConvertedTrace);
use Getopt::Long;

my $ficheroOutput = undef;
GetOptions(
    "output-file=s" => \$ficheroOutput
);

if (scalar @ARGV < 2) {
    print "Uso: $0 [--output-file=s] (decls) (trace)\n";
    exit 1;
}

if( defined($ficheroOutput) ){
    open(STDOUT, '>', $ficheroOutput) or die "Can't open the given output file $ficheroOutput";
}

printConvertedTrace(@ARGV);
exit 0;
