#!/usr/bin/perl

# Añade información de comparabilidad a un fichero de declaraciones a
# partir de una serie de trazas.
#
# Antonio García Domínguez
# $Id$

use strict;
use warnings;

use Cwd 'abs_path';
use lib ($0 =~ m,(.*)/[^/]+,);

use Comparability qw(printComputedComparabilities);
use Getopt::Long;

# PROCESAMIENTO DE ENTRADAS
my $arrayIndicesAreComparable = undef;
my $disableFilterUnused = undef;
my $ficheroOutput = undef;
GetOptions("array-indices-are-comparable|a" => \$arrayIndicesAreComparable,
           "disable-filter-unused|d" => \$disableFilterUnused,
           "output-file=s" => \$ficheroOutput
);

if (scalar @ARGV < 2) {
    print STDERR "Usage: $0 [--array-indices-are-comparable|--disable-filter-unused|--output-file=s] (decls) (path to save comparability graphs to) (traces)\n";
    exit 1;
}

if (defined $ficheroOutput) {
    open(SALIDA, '>', $ficheroOutput) or die "Can't open the given output file $ficheroOutput";
    select(SALIDA);
}

my ($decls, $saveTo, @traces) = @ARGV;
printComputedComparabilities($disableFilterUnused, $arrayIndicesAreComparable, $decls, $saveTo, [], @traces);

exit 0;
