#!/usr/bin/perl

use strict;
use warnings "all";

use Cwd 'abs_path';
use lib ($0 =~ m,(.*)/[^/]+,);

use FlattenTraces qw(printFlattenedTraces);
use Getopt::Long;

## ARGUMENT PROCESSING
my $ficheroOutput = undef;
my $indexFlattening = undef;
GetOptions(
    "output-file=s" => \$ficheroOutput,
    "index-flattening|i" => \$indexFlattening,
);

if (scalar @ARGV != 2) {
    print STDERR "Usage: $0 [--output-file=s|--index-flattening] (decls) (dtrace)\n";
    exit 1;
}

if(defined($ficheroOutput)){
	open(SALIDA, '>', $ficheroOutput) or die "Cannot open the given output file $ficheroOutput";
	select(SALIDA);
}

printFlattenedTraces($indexFlattening, @ARGV);
exit 0;
