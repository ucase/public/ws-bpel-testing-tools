#!/usr/bin/perl

# Serializes the lines in an ActiveBPEL log file.
#
# Copyright (C) 2012 Antonio García-Domínguez
# Licensed under the GPLv3 or later

use strict;
use warnings;

use Daikon qw(loadActiveBPELLog);
use TraceConverter qw(serialize);

# ARGUMENT PROCESSING
if (scalar @ARGV != 1) {
  print STDERR "Usage: $0 (.log)\n";
  exit 1;
}

my $logfile = $ARGV[0];
my $rvLines = loadActiveBPELLog($logfile);

print "Before serialization:\n" . (join "", @$rvLines) . "\n\n";

my @serialized_lines = serialize(@$rvLines);
print "After serialization:\n" . (join "", @serialized_lines);

