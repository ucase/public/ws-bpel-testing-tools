#!/usr/bin/perl

# Extrae la información sobre la cobertura de ramas a partir
# de un fichero con la traza de ejecución de una composición BPEL
#
# Alejandro Álvarez-Ayllón, Antonio García-Domínguez
#

use strict;
use warnings;

use Cwd 'abs_path';
use lib ($0 =~ m,(.*)/[^/]+,);

use Coverage qw(formatCoverageResults);
use Getopt::Long;

# ARGUMENT PROCESSING
my $outputFile = undef;
my $plainText = 0;
GetOptions(
	   "plain" => \$plainText,
	   "output-file=s" => \$outputFile
	  );

if (scalar @ARGV < 1) {
  print STDERR "Uso: $0 [--plain|--output-file=s] (trazas)\n";
  exit 1;
}

if ( defined($outputFile) ) {
  open(SALIDA, '>', $outputFile) or die "Can't open the given output file $outputFile";
  select(SALIDA);
}

my @rutasTrazas = @ARGV;
if (scalar @rutasTrazas < 1) {
  print STDERR "Especifique al menos una traza\n";
  exit 1;
}

# MAIN BODY
print formatCoverageResults($plainText, @rutasTrazas);
exit 0;
