package es.uca.takuan.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.xml.XMLDocument;

/**
 * This class is used to abstract the details of creation and
 * processing of a project
 * @author Alejandro Álvarez Ayllón
 */
public class BpelProject {

    private final String BPTS_NAMESPACE  = "http://www.bpelunit.org/schema/testSuite";

    private final String ANALYZER_SCRIPT = "analizadorDaikon.pl";
    private final String PERL_CMD        = "/usr/bin/perl";

    // Properties
    private File                 projectDir   = null;
    private File                 bprFile      = null;
    private File                 bptsFile     = null;
    private File                 outputDir    = null;
    private InstrumenterTask     instrumenter = null;
    private BPELUnitClient       bpelunit     = null;
    private Map<String,String[]> parameters   = null;

    /** Creates a new temporary folder
     * @param prefix    The prefix of the temporary folder name
     * @param postfix   The postfix of the temporaty folder name
     * @return          The temporary folder
     */
    private static File createTempFolder(String prefix, String postfix) throws IOException {
        File folder = File.createTempFile(prefix, postfix);
        
        if(!folder.delete())
            throw new IOException("The temporary project folder can not be created (Remove file).");

        if(!folder.mkdir())
            throw new IOException("The temporary project folder can not be created.");

        return folder;
    }

    /** Constructor
     *  Creates a directory inside the java.io.tempdir
     */
    public BpelProject() throws IOException {
        // Create project dir
        projectDir = createTempFolder("TAKUAN_", "_Project");
    }

    /** Returns the File object associated with the project dir
     * @return The temporary project directory
     */
    public File getProjectFolder() {
        return projectDir;
    }

    /** Set the execution parameters
     * @param parameters
     */
    public void setParameters(Map<String, String[]> p) {
        parameters = p;
    }

    /** Creates a new File inside the project folder
     * @param fName The file name (relative)
     * @return The File object
     */
    public File newFile(String fName) throws IOException {
        File file = new File(projectDir, fName);

        // Create subdirectories if necessary
        File parent = file.getParentFile();
        if(!parent.equals(projectDir))
            if(!parent.mkdirs())
                throw new IOException("The temporary file can not be created (mkdirs): " + fName);

        // Create file
        if(!file.createNewFile())
            throw new IOException("The temporary file can not be created: " + fName);

        return file;
    }

    /** Initiates the instrumentation
     * @param stream Where to write the output messages
     * @return The instrumenter thread
     */
    public InstrumenterTask instrument(OutputStream stream) throws SAXException, IOException, ParserConfigurationException {

        // Search for BPEL and BPTS files
        File files[] = projectDir.listFiles();
        File bpelFile = null;

        for(File file : files) {
            if(file.getName().endsWith(".bpel") || file.getName().endsWith(".bpws"))
                bpelFile = file;
            else if(file.getName().endsWith(".bpts"))
                bptsFile = file;
        }

        // Generate BPR name from BPTS
        String bptsName = null;
        Document document = new XMLDocument(bptsFile).getDocument();

        Element n = document.getDocumentElement();
        n = (Element) n.getElementsByTagNameNS(BPTS_NAMESPACE, "deployment").item(0);
        n = (Element) n.getElementsByTagNameNS(BPTS_NAMESPACE, "put").item(0);

        NodeList list = n.getElementsByTagNameNS(BPTS_NAMESPACE, "property");

        int i;
        for(i = 0; i < list.getLength(); i++) {
            n = (Element) list.item(i);
            if(n.getAttribute("name").equals("BPRFile")) {
                bptsName = n.getTextContent();
            }
        }

        bprFile = new File(projectDir, bptsName);

        // Create and start task
        instrumenter = new InstrumenterTask(projectDir.getAbsolutePath(),
                                            bpelFile.getAbsolutePath(),
                                            bprFile.getAbsolutePath(),
                                            stream);
        instrumenter.start();
        return instrumenter;
    }

    /** Recovers the instrumenter task
     */
    public InstrumenterTask getInstrumenter() {
        return instrumenter;
    }

    /** Runs the BPR in BPELUnit
     * @param ActiveBpel        The path to ActiveBPEL
     * @param bpelUnitHome      The home dir of the BPELUnit installation
     * @param stream            The output will be written here
     * @return The BPELUnitClient thread instance
     */
    public BPELUnitClient runBPELUnit(String bpelUnitHome, String ActiveBpel, OutputStream stream)
            throws Exception {
        // Check
        if(bptsFile == null)
            throw new Exception("The intrumentation must be executed before");

        // Run
        bpelunit = new BPELUnitClient(bpelUnitHome, ActiveBpel, bptsFile, stream);
        bpelunit.run();

        return bpelunit;
    }

    /** Recover the BPEL Unit client thread
     * @return The BPELUnitClient thread instance
     */
    public BPELUnitClient getBPELUnit() {
        return bpelunit;
    }

    /** Returns the file associated with the instrumented BPEL file
     * @return A File associated with the instrumented BPEL file
     */
    private File getInstrumentedBPEL() {
        return new File(projectDir, InstrumenterTask.INSPECTED_BPEL_FILENAME);
    }

    /** Returns the file associated with the .decls file
     * @return  A File associated with the DECLS file
     */
    private File getDecls() {
        return new File(projectDir, InstrumenterTask.DAIKON_DECLS_FILENAME);
    }

    /** Returns a File associated with the invariants file
     * @return A File object associated with the invariants file
     */
    File getInvariantsFile() throws Exception {
        if(outputDir == null)
            throw new Exception("Daikon must be executed before");

        return new File(outputDir, InstrumenterTask.INVARIANTS_FILENAME);
    }

    /** Returns a File associated with the coverage file
     * @return A File object associated with the coverage file
     */
    File getCoverageFile() throws Exception {
        if(outputDir == null)
            throw new Exception("Daikon must be executed before");


        // Generate temporal ZIP file
        File zipFile = File.createTempFile("inv", "zip");
        ZipOutputStream zipStream = new ZipOutputStream(new FileOutputStream(zipFile));

        // Add all the invariant files
        for(String covName : InstrumenterTask.COVERAGE_FILENAMES) {
            ZipEntry zipEntry;
            FileInputStream fileInput;
            int count;
            byte [] buffer = new byte [512];

            fileInput = new FileInputStream(new File(outputDir, covName));
            zipEntry = new ZipEntry(covName);
            zipStream.putNextEntry(zipEntry);

            while((count = fileInput.read(buffer, 0, buffer.length)) > 0) {
                zipStream.write(buffer, 0, count);
            }

            fileInput.close();
        }

        zipStream.close();

        return zipFile;
    }

    /** Process the logs obtained with the execution
     * @param scriptsDir The directory with the Perl Scripts
     */
    void processLogs(String scriptsDir, String daikonScript, OutputStream stream) {
        PrintWriter out = new PrintWriter(stream, true);
        try {
            File logs[];
            File workingDir = new File(scriptsDir);

            logs = bpelunit.getLogs();
            if(logs == null)
                throw new Exception("BPELUnit must be executed before");

            // We have the logs, and the script dir, so run them and write
            // the output into a new temporary folder
            outputDir = createTempFolder("TAKUAN_", "_WD");

            // Build the log list
            String logList = new String();
            for(File f : logs) {
                logList += f.getAbsolutePath() + " ";
            }

            // Build the parameter list
            String PARAMETERS = new String(); //"--metrics --simplify --index-flattening";

            for(String key : parameters.keySet()) {
                // Filter unused
                if(key.toLowerCase().equals("filter_unused") &&
                   !parameters.get(key)[0].equals("yes"))
                    PARAMETERS += " --disable-filter-unused";
                // Disable comparabilities
                else if(key.toLowerCase().equals("comparabilities") &&
                        !parameters.get(key)[0].equals("yes"))
                    PARAMETERS += " --disable-comparability";
                // Simplify
                else if(key.toLowerCase().equals("simplify"))
                    PARAMETERS += " --simplify";
                // Generate metrics
                else if(key.toLowerCase().equals("generate_metrics") &&
                        parameters.get(key)[0].equals("yes"))
                    PARAMETERS += " --metrics";
                // Matrix mapping
                else if(key.toLowerCase().equals("mapping")) {
                    String mode = parameters.get(key)[0].toLowerCase();

                    if(mode.equals("matrix_flattening"))
                        PARAMETERS += " --index-flattening";
                }
                // Coverage output format
                else if(key.toLowerCase().equals("coverage_output")) {
                    String mode = parameters.get(key)[0].toLowerCase();

                    if(!mode.equals("xml"))
                        PARAMETERS += " --coverage-plain-text";
                }
            }

            // Build the command
            String cmd;

            cmd = PERL_CMD + " " + scriptsDir + "/" + ANALYZER_SCRIPT + " " + PARAMETERS + " " +
                  "--output-dir " + outputDir.getAbsolutePath() + " " +
                  "--daikon-script " + daikonScript + " " +
                  getDecls().getAbsolutePath() + " " +
                  getInstrumentedBPEL().getAbsolutePath() + " " +
                  logList;

            // Run it
            out.println("<takuan:processing/>");
            out.println("Instrumented file: " + getInstrumentedBPEL().getAbsolutePath());
            out.println("Executing " + cmd);

            Process p            = Runtime.getRuntime().exec(cmd, null, workingDir);
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line;
            while((line = input.readLine()) != null)
                out.println(line);

            input = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            while((line = input.readLine()) != null)
                out.println(line);

            p.waitFor();

            out.println("Execution finished");

        } catch(Throwable e) {
          e.printStackTrace(out);
        }
    }
}
