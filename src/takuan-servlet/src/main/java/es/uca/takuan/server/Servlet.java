package es.uca.takuan.server;

import java.io.*;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Takuan Servlet
 * @author Alejandro Álvarez Ayllón
 */
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/** State machine. We should get track of the state of the servlet.
     * i.e. After a GET method, if we get another PUT, we have a new process
     * Transitions:
     * New -> Receiving (after first PUT)
     * Receiving -> Processing (after first GET)
     * Processing -> Done (When first method ends)
     * Done -> Receiving (after next PUT)
     */
    private enum Status {NEW, RECEIVING, PROCESSING, DONE, ERROR};
    private Status status;

    // Properties
    private BpelProject project;
    
    /** Constructor
     */
    public Servlet() {
        super();
        status = Status.NEW;
    }
    
    /** Get handler method. This method must be called when all the files are uploaded
     * @param request The HTTP Request
     * @param response The HTTP Response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @SuppressWarnings("unchecked")
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        // Request?
        String resource            = request.getPathTranslated();
        ServletOutputStream output = response.getOutputStream();

        // Empty?
        if(resource.length() == 0) {
            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, "You must use this from a Takuan Client");
            output.println("You must use this from a Takuan Client");
            output.close();
            return;
        }

        // Check state machine
        if(status == Status.RECEIVING)
            status = Status.PROCESSING;

        // Want to run the process
        if(resource.endsWith("run")) {
            // The client wants to begin processing the BPEL
            // and recover the trace
            // JUST ONCE
            if(project.getInstrumenter() != null) {
                response.sendError(HttpServletResponse.SC_CONFLICT, "The process is already running");
                return;
            }
            // Run and returns the output
            try {
                // Arguments
                Map<String, String[]> parameters = request.getParameterMap();
                project.setParameters(parameters);

                // Run
                InstrumenterTask instrument = project.instrument(output);
                instrument.join();

                if(instrument.getError() != null) {
                    output.println(instrument.getError());
                    output.flush();
                    status = Status.ERROR;
                    return;
                }

                // Now, run the BPELUnit
                String bpelUnitHome  = getServletContext().getInitParameter("BPELUnitHome");
                String activeBpelLog = getServletContext().getInitParameter("ActiveBPELLogs");
                project.runBPELUnit(bpelUnitHome, activeBpelLog, output);

                // Great, we have the logs. Preprocess them
                String scriptsDir   = getServletContext().getInitParameter("AnalyzerScripts");
                String daikonScript = getServletContext().getInitParameter("DaikonScript");
                project.processLogs(scriptsDir, daikonScript, output);

            } catch (Exception ex) {
                Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace(new PrintWriter(output, true));
                output.flush();
                status = Status.ERROR;
                return;
            } catch(Throwable ex) {
                ex.printStackTrace(new PrintWriter(output, true));
                status = Status.ERROR;
                return;
            }

            // End
            status = Status.DONE;
            output.println("<takuan:done/>");
            output.println("You can download now the results");
            try {
                File invFile = project.getInvariantsFile();
                if(!invFile.exists())
                    output.println("Invariants file does not exist: " + invFile.getPath());
                else
                    output.println("Invariants file: " + invFile.getAbsolutePath());
            } catch (Exception ex) {
                Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace(new PrintWriter(output));
            }

        } else if( status != Status.DONE) {
            // Want to recover the results, but it is not done
            response.sendError(HttpServletResponse.SC_NO_CONTENT, "A process must be sended and executed before");
            return;
        } else if(resource.endsWith("invariants")){
            // Send the invariants file
            try {
                File            invFile = project.getInvariantsFile();
                FileInputStream input   = new FileInputStream(invFile);
                byte[]          buffer  = new byte[512];
                int n;

                response.setContentLength((int) invFile.length());
                while((n = input.read(buffer)) > 0) {
                    output.write(buffer, 0, n);
                }

            } catch (Exception ex) {
                Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
                response.sendError(HttpServletResponse.SC_NOT_FOUND, ex.getMessage());
            }
        } else if(resource.endsWith("coverage")) {
            // Send the coverage file
            try {
                File covFile = project.getCoverageFile();
                FileInputStream input = new FileInputStream(covFile);
                byte[] buffer = new byte[512];
                int n;

                response.setContentLength((int) covFile.length());
                while((n = input.read(buffer)) > 0) {
                    output.write(buffer, 0, n);
                }
            } catch (Exception ex) {
                Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
                response.sendError(HttpServletResponse.SC_NOT_FOUND, ex.getMessage());
            }
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, resource + " not found");
        }

        output.close();
    } 

    /** Post handler method. This method is not supported
     * @param request The HTTP Request
     * @param response The HTTP Response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }
    
    /** Put handler method. This method is used to upload the files that belong
     * to the process being tested.
     * @param request The HTTP Request
     * @param response The HTTP Response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Check state machine
        if(status == Status.NEW)
            status = Status.RECEIVING;
        else if(status == Status.DONE || status == Status.ERROR) {
            status  = Status.RECEIVING;
            project = null;
        } else if(status == Status.PROCESSING)
            response.sendError(HttpServletResponse.SC_FORBIDDEN);

        // Create project if it doesn't exists
        if(project == null)
            project = new BpelProject();
        
        // Write file
        OutputStream output        = new FileOutputStream(project.newFile(request.getPathInfo()));
        InputStream  input         = request.getInputStream();
        byte         buffer[]      = new byte[512];
        int          readed;

        while((readed = input.read(buffer)) > 0) {
            output.write(buffer, 0, readed);
        }

        output.flush();
        output.close();

        // Response
        response.sendError(HttpServletResponse.SC_ACCEPTED);
    }

    /** 
    * Returns a short description of the servlet.
    */
    @Override
    public String getServletInfo() {
        return "Takuan servlet implementation";
    }
}
