package es.uca.takuan.server;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;

import net.bpelunit.framework.base.BPELUnitBaseRunner;
import net.bpelunit.framework.control.util.BPELUnitConstants;
import net.bpelunit.framework.exception.ConfigurationException;
import net.bpelunit.framework.exception.DeploymentException;
import net.bpelunit.framework.exception.SpecificationException;
import net.bpelunit.framework.model.test.ITestResultListener;
import net.bpelunit.framework.model.test.TestCase;
import net.bpelunit.framework.model.test.TestSuite;
import net.bpelunit.framework.model.test.report.ITestArtefact;

/**
 * Client of BPELUnit framework
 * @author Alejandro Álvarez Ayllón
 */
public class BPELUnitClient extends BPELUnitBaseRunner implements ITestResultListener {

    // Attributes
    private int testCaseCount    = 0;
    private int testCaseRunning  = 0;
    private int testCaseFinished = 0;
    private String bpelUnitHome     = null;
    private String ActiveBPELLogDir = null;

    private PrintWriter out = null;

    private File   bptsFile      = null;
    private File[] executionLogs = null;

    /** Constructor
     * @param bprFile   The home dir of BPELUnit
     * @param stream    The output will be written here
     */
    BPELUnitClient(String bpelUnit, String ActiveBpel, File bpts, OutputStream stream) {
        out          = new PrintWriter(stream, true);
        bptsFile     = bpts;
        bpelUnitHome     = bpelUnit;
        ActiveBPELLogDir = ActiveBpel;
    }

    /** This is called by BPELUnit when a test case is started
     * @param test  The test case being executed
     */
    public void testCaseStarted(TestCase test) {
        testCaseCount = test.getSuite().getTestCaseCount();
        testCaseRunning++;
    }

    /** This is called by BPELUnit when a test case is ended
     * @param test  The test case that has ended
     */
    public void testCaseEnded(TestCase test) {
        testCaseFinished++;
        out.println("<takuan:testCaseEnded total=\"" + Integer.toString(testCaseCount)
                    + "\" finished=\"" + Integer.toString(testCaseFinished) + "\"/>");
    }

    /** This is called by BPELUnit to inform
     * NOT USED
     * @param arg
     */
    public void progress(ITestArtefact arg) {
        // None
    }


    /** This is called by BPELUnit to configure the logging
     * NOT USED
     * @throws ConfigurationException
     */
    @Override
    public void configureLogging() throws ConfigurationException {
    }

    /** This is called by BPELUnit to configure the environment and the execution
     * @throws ConfigurationException
     */
    @Override
	public void configureInit() throws ConfigurationException {
        try {
            if(bpelUnitHome == null || bpelUnitHome.length() == 0)
                throw new ConfigurationException("BPELUnit Home is not set");
            out.println("BPELUnit Home is set to " + bpelUnitHome);
            // Set home
            setHomeDirectory(bpelUnitHome);
        } catch(Throwable ex) {
            ex.printStackTrace(out);
            throw new ConfigurationException("Error while trying to configure BPELUnit");
        }
	}


    /** Run method
     */
    public void run() {
        try {
            out.println("INITIALIZING BPELUNIT");
            
            // Get current list of traces
            File logsDir = new File(ActiveBPELLogDir);
            if(!logsDir.isDirectory()) {
                out.println(ActiveBPELLogDir + " must be a directory");
                return;
            }

            File prevLogs[] = logsDir.listFiles();
            out.println("The ActiveBPEL log dir is " + ActiveBPELLogDir);
            out.println("There are " + Integer.toString(prevLogs.length) + " logs files right now");

            // Initialize
			initialize(BPELUnitConstants.NULL_OPTIONS);
            out.println("<takuan:running/>");

			out.println("Loading Test Suite...");
			TestSuite suite = loadTestSuite(bptsFile);
			out.println("Test Suite sucessfully loaded.");
            out.println("<takuan:testCaseCount total=\"" + Integer.toString(suite.getTestCaseCount()) + "\"/>");
			out.println("Deploying Services...");

			try {
				suite.setUp();
			} catch (DeploymentException e) {
				out.println("A deployment error occurred when deploying services for this test suite:");
				out.println("  " + e.getMessage());
				try {
					suite.shutDown();
				} catch (DeploymentException e1) {
					// do nothing
				}
				return;
			}

			out.println("Services successfully deployed.");
			out.println("Running Test Cases...");

			suite.addResultListener(this);
			suite.run();

			suite.removeResultListener(this);
			out.println("Done running test cases.");

			out.println("Undeploying services...");

			try {
				suite.shutDown();
			} catch (DeploymentException e) {
				out.println("An undeployment error occurred when undeploying services for this test suite:");
				out.println("  " + e.getMessage());
				return;
			}
			out.println("Services undeployed.");

            // Recover logs files
            File curLogs[] = logsDir.listFiles();
            int newLogs    = curLogs.length - prevLogs.length;

            out.println("There are " + Integer.toString(newLogs) + " new logs:");

            executionLogs = new File[newLogs];
            int i = 0;
            for(File f : curLogs) {
                boolean isPrevLog;

                // Search in previous logs
                isPrevLog = false;
                for(File f2 : prevLogs) {
                    if(f2.equals(f)) {
                        isPrevLog = true;
                        break;
                    }
                }
                // If didn't exist, it is new
                if(!isPrevLog) {
                    executionLogs[i] = f;
                    i++;
                    out.println("\t" + f.getAbsolutePath());
                }
            }

		} catch (ConfigurationException e) {
			out.println("A configuration error was encountered when initializing BPELUnit:");
			out.println(" " + e.getMessage());
			return;
		} catch (SpecificationException e) {
			out.println("An error was encountered when reading the test suite specification:");
			out.println(" " + e.getMessage());
            return;
		} catch(Throwable e) {
            e.printStackTrace(out);
            return;
        }
    }

    /** Return the execution logs
     * @return
     */
    File[] getLogs() {
        return executionLogs;
    }
}
