package es.uca.takuan.server;

import java.io.File;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.activebpel.DeploymentArchivePackager;
import es.uca.webservices.bpel.daikon.decls.DeclarationsFormatter;
import es.uca.webservices.bpel.daikon.decls.DeclarationsGenerator;
import es.uca.webservices.bpel.daikon.instrument.BPELFileInstrumenter;


/**
 * Runnable class for implement the Instrumenter Thread
 * @author Alejandro Álvarez Ayllón
 */
public class InstrumenterTask extends Thread {
    
	public static final String INSPECTED_BPEL_FILENAME = "procesoInspeccionado.bpel";
	public static final String TYPES_FILENAME          = "tiposReunidos.xml";
	public static final String PDD_FILENAME            = "proceso.pdd";
	public static final String DAIKON_DECLS_FILENAME   = "procesoInspeccionado.decls";
	public static final String CATALOGXML_FILENAME     = "catalog.xml";
    public static final String INVARIANTS_FILENAME     = "procesoInspeccionado.out";
    public static final String[] COVERAGE_FILENAMES    = {"coverage.log",
                                                          "pathCoverageAll.log",
                                                          "pathCoverageExecuted.log",
                                                          "pathCoverageNotExecuted.log"};

    // Attributes
    private String          basePath;
    private String          bpelPath;
    private String          bprPath;
    private PrintWriter     output;
    private String          errorMsg;

    /** Constructor
     * @param base  The working dir
     * @param bpel  The BPEL path
     * @param bpr   The BPR path
     * @param out   Where to write the output
     */
    public InstrumenterTask(String base, String bpel, String bpr, OutputStream out) {
        if(base.endsWith("/"))
            basePath = base;
        else
            basePath = base + "/";

        bpelPath = bpel;
        bprPath  = bpr;
        output   = new PrintWriter(out, true);
    }

	/**
	 * Returns the error message (if any)
	 * 
	 * @return null if there is no error
	 */
    public String getError() {
        return errorMsg;
    }

    @Override
    public void run() {
        String oldUserDir;

        // Change "working dir" to avoid problems with XSLT relative paths
        oldUserDir = System.getProperty("user.dir");
        System.setProperty("user.dir", basePath);

        try {
            output.println("<takuan:instrumenting/>");

            // Instrument the WS-BPEL process definition
            output.println("Generating instrumented WS-BPEL process definition...");
            final BPELProcessDefinition originalDef = new BPELProcessDefinition(new File(bpelPath));
       		final BPELProcessDefinition def = new BPELFileInstrumenter(originalDef).instrument();
            final DeploymentArchivePackager instrumenter = new DeploymentArchivePackager(def);

            // Catalog
            output.println("Generating ActiveBPEL 4.1 catalog.xml...");
            instrumenter.generateCatalogXML().dumpToFile(new File(basePath + CATALOGXML_FILENAME));

            // PDD
            output.println("Generating ActiveBPEL 4.1 Process Deployment Descriptor...");
            instrumenter.generatePDD().dumpToFile(new File(basePath + PDD_FILENAME));

            // *.decls
            output.println("Generating preliminary Daikon declarations file...");
            final DeclarationsGenerator gen = new DeclarationsGenerator();
            final FileWriter fWriter = new FileWriter(new File(basePath + DAIKON_DECLS_FILENAME));
            try {
            	gen.generateAndPrint(def, new DeclarationsFormatter(), fWriter);
            } finally {
            	fWriter.close();
            }

            // Generate BPR
            if (bprPath != null) {
                output.println("Packing BPR file '" + bprPath + "'...");
                instrumenter.packBPR(def.getFile().getPath(),
                                     basePath + PDD_FILENAME,
                                     basePath + CATALOGXML_FILENAME,
                                     bprPath);
            }
        } catch (Throwable ex) {
            output.println("[InstrumenterTask] Error processing the project");
            Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
            errorMsg = ex.getMessage();
            ex.printStackTrace(output);
            return;
        } finally {
            // Recover "working dir"
            System.setProperty("user.dir", oldUserDir);
        }
    }

}
