package es.uca.webservices.gamera.api;

import java.util.HashMap;

/**
 * Structure which saves the results comparison from the execution for each
 * individual. All individuals saved in this structure and all keys used
 * to query it are normalized on the fly.
 *
 * @author Emma Blanco-Muñoz, Antonio Garcia-Dominguez
 * @version 1.1
 */
public class GAHof extends HashMap<GAIndividual, ComparisonResults>{

	private static final long serialVersionUID = 1L;
	private final AnalysisResults analysis;

	// Column-wise sums of the execution matrix (skipping invalid mutants)
	private int[] mutantsKilledByTestCase;
	// Number of valid unique normalized mutants in the HOF
	private int validMutants = 0;

	public GAHof(AnalysisResults analysis) {
		this.analysis = analysis;
	}

	public AnalysisResults getAnalysis() {
		return analysis;
	}

	@Override
	public boolean containsKey(Object key) {
		return key instanceof GAIndividual ? super.containsKey(((GAIndividual)key).normalize(analysis)) : false;
	}

	@Override
	public ComparisonResults get(Object key) {
		return key instanceof GAIndividual ? super.get(((GAIndividual)key).normalize(analysis)) : null;
	}

	@Override
	public ComparisonResults put(GAIndividual key,  ComparisonResults value) {
		// After normalizing the individual, it is added to the HOF
		final GAIndividual normalized;
		if(!key.isNormalized()) {
			normalized = key.normalize(analysis);
		} else {
			normalized = key;
		}
		if (!super.containsKey(normalized) && value != null) {
			final ComparisonResult[] row = value.getRow();
			if (mutantsKilledByTestCase == null) {
				mutantsKilledByTestCase = new int[row.length];
			}
			if (row[0] != ComparisonResult.INVALID) {
				++validMutants;
				for (int i = 0; i < row.length; ++i) {
					if (row[i] == ComparisonResult.DIFFERENT_OUTPUT) {
						++mutantsKilledByTestCase[i];
					}
				}
			}
		}
		return super.put(normalized, value);
	}

	/**
	 * Convenience version of {@link #put(GAIndividual, ComparisonResults)}, as
	 * comparison results already store their GAIndividuals.
	 */
	public ComparisonResults put(ComparisonResults cR) {
		return put(cR.getIndividual(), cR);
	}

	/**
	 * Adds the individuals and comparison results arrays into the HOF
	 * @param individuals
	 * 			New individuals produced by the GA
	 * @param comparisons
	 * 			Execution rows of the new individuals of the GA
	 */
	public void concat2HOF(GAIndividual[] individuals, ComparisonResults[] comparisons) {
    	for(int i = 0; i < individuals.length; i++) {
    		put(individuals[i], comparisons[i]);
    	}
	}

	/**
	 * Returns the number of mutants that have been killed by each test case, or
	 * <code>null</code> if the HOF is empty.
	 */
	public int[] getMutantsKilledByTestCase() {
		return mutantsKilledByTestCase;
	}

	/**
	 * Returns the number of unique valid mutants in the HOF.
	 */
	public int getValidMutants() {
		return validMutants;
	}
}