package es.uca.webservices.gamera.api.util.ranges;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Subclass of {@link IntegerRangeListFilter} for ranges of positions over an
 * array of String objects. Ranges may use 1-based positions as indices, or the
 * strings in these positions. String comparisons are case-insensitive, and
 * inner whitespace is preserved. However, starting and trailing whitespace
 * around the range indices is removed.
 * </p>
 *
 * @author Antonio García-Domínguez
 */
public class StringArrayRangeListFilter extends IntegerRangeListFilter {
	private final Map<String, Integer> key2Pos = new HashMap<String, Integer>();

	public StringArrayRangeListFilter(Collection<String> keys) {
		super(1, keys.size());
		initKey2PosMap(keys);
	}

	public StringArrayRangeListFilter(Collection<String> keys, String includedRanges) {
		super(1, keys.size(), includedRanges);
		initKey2PosMap(keys);
	}

	public StringArrayRangeListFilter(Collection<String> keys, String includedRanges, String excludedRanges) {
		super(1, keys.size(), includedRanges, excludedRanges);
		initKey2PosMap(keys);
	}

	@Override
	protected int convertValueToInteger(String key) {
		final String lcKey = key.toLowerCase();
		if (key2Pos.containsKey(lcKey)) {
			return key2Pos.get(lcKey);
		}
		return super.convertValueToInteger(key);
	}

	private void initKey2PosMap(Collection<String> keys) {
		int pos = 1;
		for (String key : keys) {
			key2Pos.put(key.toLowerCase(), pos++);
		}
	}
}
