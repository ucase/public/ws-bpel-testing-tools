package es.uca.webservices.gamera.api.term;

import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Interface for all termination conditions.
 *
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public interface GATerminationCondition {

    /**
     * Indicates whether the algorithm should terminate or not.
     *
     * @param state
     *            Current state of the algorithm.
     * @return <code>true</code> when the algorithm should terminate,
     *         <code>false</code> otherwise.
     */
    boolean evaluate(GAState state);

    /**
     * Checks that the termination condition has been correctly configured.
     *
     * @throws InvalidConfigurationException
     *             The termination condition has not been correctly configured.
     */
    void validate() throws InvalidConfigurationException;
}
