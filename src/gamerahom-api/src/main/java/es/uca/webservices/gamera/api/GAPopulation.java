package es.uca.webservices.gamera.api;

import java.util.ArrayList;
import java.util.List;

/**
 * A population for the genetic algorithm. It might be empty.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.1
 */
public class GAPopulation {

    private List<GAIndividual> individuals = new ArrayList<GAIndividual>();

    public void setIndividuals(List<GAIndividual> individuals) {
        this.individuals = individuals;
    }
    
	public List<GAIndividual> getIndividuals() {
        return individuals;
    }
    
    public void addIndividual(GAIndividual a) {
        this.individuals.add(a);
    }
    
    public Integer getPopulationSize(){
        return this.individuals.size();
    }
  
}
