package es.uca.webservices.gamera.api.exec;


/**
 * Dummy progress monitor which doesn't do anything. Useful as a fallback when
 * the user passes <code>null</code> to a method expecting a
 * {@link GAProgressMonitor}.
 */
public class DummyProgressMonitor implements GAProgressMonitor {

	@Override
	public void start(int nUnits) {
		// do nothing
	}

	@Override
	public void status(String msg) {
		// do nothing
	}

	@Override
	public void done(int nUnits) {
		// do nothing
	}

	@Override
	public boolean isCancelled() {
		return false;
	}

}
