package es.uca.webservices.gamera.api.annotations;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Utility class with several useful methods to find the options available in a
 * GAmeraHOM component and localize their descriptions. Normally you should use
 * the 2-argument versions of {@link #getLongDescription} and
 * {@link #getShortDescription}, as they use the default localization set by the
 * user.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
public final class OptionProcessor {

	private static final String SHORT_DESC_SUFFIX = ".short";
	private static final String LONG_DESC_SUFFIX = ".long";

        // This line follows a Sonar advice: Hide Utility Class Constructor
        private OptionProcessor(){}
        
	/**
	 * Lists the options available in a GAmeraHOM component. These options are
	 * methods marked by the {@link Option} annotation.
	 * 
	 * @param klazz
	 *            Class to be queried.
	 * @return map from each of these methods to its {@link Option} annotation.
	 */
	public static Map<Method, Option> listOptions(Class<?> klazz) {
		final Map<Method, Option> results = new HashMap<Method, Option>();
		for (Method m : klazz.getMethods()) {
			final Option o = m.getAnnotation(Option.class);
			if (o != null) {
				results.put(m, o);
			}
		}
		return results;
	}

	/**
	 * Returns the localized short description of the option under the default
	 * localization. See {@link #getShortDescription(Method, Locale)} for
	 * details.
	 */
	public static String getShortDescription(Method m) {
		return getShortDescription(m, Locale.getDefault());
	}

	/**
	 * Returns the localized short description of the option under the specified
	 * localization. Let the class have <code>A</code> as its simple name
	 * (dropping the package part) and the method be called <code>m</code>. The
	 * lookup algorithm works as follows:
	 * 
	 * <ol>
	 * <li>Look for a resource bundle with the prefix <code>A</code>, using
	 * {@link ResourceBundle#getBundle}.</li>
	 * <li>Once the appropriate <code>.properties</code> file has been loaded,
	 * lookup the <code>m.short</code> property for the localized text.</li>
	 * </ol>
	 * 
	 * @return String with the localized short description of the option.
	 */
	public static String getShortDescription(Method m, Locale l) {
		return lookupPropertyByMethod(m, l, SHORT_DESC_SUFFIX);
	}

	private static String lookupPropertyByMethod(Method m, Locale l, final String suffix) {
		final String propertyName = m.getName() + suffix;

		for (Class<?> klazz = m.getDeclaringClass(); klazz != null; klazz = klazz.getSuperclass()) {
			try {
				final ResourceBundle bundle = ResourceBundle.getBundle(klazz.getSimpleName(), l);
				return bundle.getString(propertyName);
			} catch (MissingResourceException ex) {
				// keep on trying with the superclass
			}
		}

		final String className = m.getDeclaringClass().getSimpleName();
		throw new MissingResourceException(
				String.format(
						"Could not find property '{}' on the .properties for {} or its superclasses",
						propertyName, className), className, propertyName);
	}

	/**
	 * Returns the localized long description of the option under the default
	 * localization. See {@link #getLongDescription(Method, Locale)} for
	 * details.
	 */
	public static String getLongDescription(Method m) {
		return getLongDescription(m, Locale.getDefault());
	}

	/**
	 * Returns the localized long description of the option under the specified
	 * localization. Let the class have <code>A</code> as its simple name
	 * (dropping the package part) and the method be called <code>m</code>. The
	 * lookup algorithm works as follows:
	 * 
	 * <ol>
	 * <li>Look for a resource bundle with the prefix <code>A</code>, using
	 * {@link ResourceBundle#getBundle}.</li>
	 * <li>Once the appropriate <code>.properties</code> file has been loaded,
	 * lookup the <code>m.long</code> property for the localized text.</li>
	 * </ol>
	 * 
	 * @return String with the localized short description of the option.
	 */
	public static String getLongDescription(Method m, Locale l) {
		return lookupPropertyByMethod(m, l, LONG_DESC_SUFFIX);
	}
}
