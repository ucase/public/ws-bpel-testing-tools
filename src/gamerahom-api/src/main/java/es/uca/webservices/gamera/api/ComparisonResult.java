package es.uca.webservices.gamera.api;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Enumeration for the valid outcomes of a comparison.
 *
 * <p>
 * <em>Notice:</em> even though the codes for these results are the same as
 * those used by <code>mubpel compare</code>, this class is part of the
 * generic core of GAmeraHOM. Do not try and move it into mubpel, as it
 * would create an unwanted dependency.
 * </p>
 * 
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public enum ComparisonResult {
	
	/**
	 * The mutation was not executed, or (if coverage information is not available)
	 * it was executed but did not show a difference in behaviour.
	 */
	SAME_OUTPUT(0, ComparisonResult.IS_VALID | ComparisonResult.IS_ALIVE),
	/**
	 * The mutation was executed and produced a different result.
	 */
	DIFFERENT_OUTPUT(1, ComparisonResult.IS_VALID | ComparisonResult.IS_EXECUTED),
	/**
	 * The mutation could not be executed (i.e. failed to compile or deploy).
	 */
	INVALID(2, 0),
	/**
	 * The mutation was executed, but did not produce a different result.
	 */
	EXERCISED(3, ComparisonResult.IS_VALID | ComparisonResult.IS_ALIVE | ComparisonResult.IS_EXECUTED);

	public static final int IS_VALID = 0x1;
	public static final int IS_ALIVE = 0x2;
	public static final int IS_EXECUTED = 0x4;

	private final int value;
	private final int flags;
		
	private static final Map<Integer, ComparisonResult> MAPVALUETOOBJECT;

	static {
		MAPVALUETOOBJECT = new HashMap<Integer, ComparisonResult>();
		for (ComparisonResult res : ComparisonResult.values()) {
			MAPVALUETOOBJECT.put(res.getValue(), res);
		}
	}

	ComparisonResult(int value, int flags) {
		this.value = value;
		this.flags = flags;
	}

	public int getValue() {
		return value;
	}
	
	public boolean isValid() {
		return (flags & IS_VALID) != 0;
	}

	public boolean isAlive() {
		return (flags & IS_ALIVE) != 0;
	}
	
	public boolean isDead() {
		return !isAlive();
	}

	public boolean isExecuted() {
		return (flags & IS_EXECUTED) != 0;
	}

	public static ComparisonResult getObject(int value) {
		return MAPVALUETOOBJECT.get(value);
	}
}
