package es.uca.webservices.gamera.api.exec;

public class ComparisonException extends Exception {

	private static final long serialVersionUID = 1L;

	public ComparisonException(String message) {
		super(message);
	}

	public ComparisonException(Throwable cause) {
		super(cause);
	}

	public ComparisonException(String message, Throwable cause) {
		super(message, cause);
	}

}
