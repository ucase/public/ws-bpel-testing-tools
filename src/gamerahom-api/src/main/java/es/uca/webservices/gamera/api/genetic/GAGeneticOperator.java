package es.uca.webservices.gamera.api.genetic;

import java.util.Random;

import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.log.GALogger;

/**
 * <p>
 * Interface for all genetic operators.
 * </p>
 * <p>
 * Genetic operators are assumed to be unary: binary operators can be emulated
 * by setting the probability to the square root of the original value, storing
 * the first operand on the first call (while producing an empty population) and
 * producing a population with 2 individuals on the second call.
 * </p>
 * <p>
 * <emph>Note</emph>: these genetic operators should check if they should
 * produce new individuals using their own probabilities: the main algorithm
 * will call the apply method for all operators on all individuals.
 * </p>
 * 
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public interface GAGeneticOperator {

	/**
	 * Applies the genetic operator inside a population, producing one or more
	 * individuals from it. The individuals are appended at the end of the
	 * provided population.
	 * 
	 * @param state
	 *            State of the genetic algorithm.
	 * @param source
	 *            Population from which any required information can be
	 *            retrieved. Should remain unchanged.
	 * @param destination
	 *            Population where the new individuals should be added to.
	 * @param logger
	 * 			  Logger which shows information about the applied genetic operator
	 * @param mutateAttribute
	 * 			   Whether the attribute field is considered or not when applying genetic operators 
	 */
	void apply(GAState state, GAPopulation source, GAPopulation destination, GALogger logger, boolean mutateAttribute, boolean createTree);

	/**
	 * Validates that the genetic operator has been correctly configured.
	 * 
	 * @throws InvalidConfigurationException
	 *             The genetic operator has not been correctly configured.
	 */
	void validate() throws InvalidConfigurationException;

	/**
	 * Sets the pseudo-random number generator to be used.
	 */
	void setPRNG(Random prng);
}
