package es.uca.webservices.gamera.api;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * Comparison results between the outputs of the original program and the mutant.
 * Optionally, it can store the wall times required by each test (possibly cached)
 * and the time which was saved through caching (and which should be added to the
 * cached time in the {@link GAState}).
 * 
 * Note: {@link #hashCode()} and {@link #equals(Object)} are based on the individual
 * and the row of comparison results, but not on the times, as these might vary
 * across multiple executions.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.2
 */
public class ComparisonResults {

    private ComparisonResult[] row;
	private GAIndividual individual;
    private BigInteger[] testWallNanos;

	public ComparisonResults() {
		// no-arg ctor for SnakeYAML
	}

    public ComparisonResults(GAIndividual individual, ComparisonResult... row) {
        this.individual = individual;
        this.row = row.clone();
    }

    public ComparisonResults(int op, int loc, int attr, boolean normalized, ComparisonResult... row) {
        this.individual = new GAIndividual(op, loc, attr, normalized);
        this.row = row;
    }

    public ComparisonResults(int op, int loc, int attr, Integer... rawRow) {
    	this(op, loc, attr, false, rawRow);
    }

    public ComparisonResults(int op, int loc, int attr, boolean normalized, Integer... rawRow) {
    	this.individual = new GAIndividual(op, loc, attr, normalized);
    	this.row = new ComparisonResult[rawRow.length];
    	for (int i = 0; i < row.length; ++i) {
    		row[i] = ComparisonResult.getObject(rawRow[i]);
    	}
    }

    public ComparisonResult[] getRow() {
        return row;
    }

    public void setRow(ComparisonResult[] row) {
    	this.row = row;
    }

    /**
     * Return <code>true</code> if the mutant is valid and always produced the same result as the original program.
     */
    public boolean isAlive() {
        for (ComparisonResult r : row) {
        	if (!r.isAlive()) {
        		return false;
        	}
        }
        return true;
    }

    /**
     * Returns <code>true</code> if the mutant could not produce any results due to generation or execution errors.
     */
    public boolean isInvalid() {
        return row.length > 0 && !row[0].isValid();
    }

    /**
     * Returns <code>true</code> if the mutant is valid and produced a different result than the original program
     * for some test case.
     */
    public boolean isDead() {
    	return !isInvalid() && !isAlive();
    }

    public GAIndividual getIndividual() {
        return individual;
    }

    public void setIndividual(GAIndividual individual) {
    	this.individual = individual;
    }

    public BigInteger[] getTestWallNanos() {
		return testWallNanos;
	}

	public void setTestWallNanos(BigInteger[] testWallNanos) {
		this.testWallNanos = testWallNanos;
	}

	/**
	 * Returns the total amount of wall clock time required to run all tests, in
	 * nanoseconds. If no test execution times have been recorded, returns
	 * {@link BigInteger#ZERO}.
	 */
	public BigInteger getTotalNanos() {
		BigInteger total = BigInteger.ZERO;
		if (testWallNanos != null) {
			for (BigInteger testNanos : testWallNanos) {
				total = total.add(testNanos);
			}
		}
		return total;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ComparisonResults other = (ComparisonResults) obj;
		if (individual == null) {
			if (other.individual != null) {
				return false;
			}
		} else if (!individual.equals(other.individual)) {
			return false;
		}
		if (!Arrays.equals(row, other.row)) {
			return false;
		}
		return true;
	}

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((individual == null) ? 0 : individual.hashCode());
		result = prime * result + Arrays.hashCode(row);
		return result;
	}

	@Override
	public String toString() {
		return "ComparisonResults ["
				+ (row != null ? "row=" + Arrays.toString(row) + ", " : "")
				+ (individual != null ? "individual=" + individual + ", " : "")
				+ (testWallNanos != null ? "testWallNanos="
						+ Arrays.toString(testWallNanos) + ", " : "");
	}

}
