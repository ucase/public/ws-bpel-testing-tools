package es.uca.webservices.gamera.api.genetic;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Stores the options to be used by the genetic algorithm when applying
 * the genetic operator that do <emph>not</emph> influence the behaviour
 * of the genetic operator itself. For instance, it can be used to store
 * the probability with which it will be applied.
 * 
 *  @author Antonio García-Domínguez
 */
public class GAGeneticOperatorOptions {

	private double probability;

	/**
	 * Returns the probability with which this operator will be applied.
	 */
	public double getProbability() {
		return probability;
	}

	/**
	 * Changes the probability with which this operator will be applied.
	 * @param probability New probability to be set, within the (0, 1] range.
	 */
	public void setProbability(double probability) {
		this.probability = probability;
	}

	/**
	 * Checks if the options set for this genetic operator are valid.
	 * @throws InvalidConfigurationException The options set are not valid.
	 */
	public void validate() throws InvalidConfigurationException {
		if (probability <= 0 || probability > 1) {
			throw new InvalidConfigurationException("Probability must be within the (0, 1] range");
		}
	}

}
