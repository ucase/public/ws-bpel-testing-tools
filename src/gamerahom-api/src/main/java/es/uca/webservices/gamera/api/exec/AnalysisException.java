package es.uca.webservices.gamera.api.exec;

public class AnalysisException extends Exception {

	private static final long serialVersionUID = 1L;

	public AnalysisException(String message) {
		super(message);
	}

	public AnalysisException(Throwable cause) {
		super(cause);
	}

	public AnalysisException(String message, Throwable cause) {
		super(message, cause);
	}

}
