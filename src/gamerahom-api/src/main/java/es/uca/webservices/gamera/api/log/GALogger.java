package es.uca.webservices.gamera.api.log;


import java.util.List;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Interface for all loggers. Each method in this interface is an event that a
 * specific logger can process as desired.
 *
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public interface GALogger {

    // OVERALL EXECUTION //////////////////////////////////

    /**
     * The algorithm has started its execution.
     *
     * @param state
     *            State of the algorithm.
     */
    void started(GAState state);

    /**
     * The algorithm has finished its execution.
     *
     * @param state
     *            Final state of the algorithm after one of the termination
     *            conditions has evaluated to a true value.
     */
    void finished(GAState state);

    // ANALYSIS ///////////////////////////////////////////

    /**
     * The algorithm has started to analyze the original program.
     *
     * @param state
     *            Current state of the algorithm.
     */
    void startedAnalysis(GAState state);

    /**
     * The algorithm has finished analyzing the original program.
     *
     * @param state
     *            Current state of the algorithm (includes the analysis
     *            results).
     */
    void finishedAnalysis(GAState state);

    // GENERATION /////////////////////////////////////////

    /**
     * The algorithm has produced a new generation.
     *
     * @param state
     *            Current state of the algorithm.
     * @param population
     *            Population for this generation.
     */
    void newGeneration(GAState state, GAPopulation population);

    // COMPARISON /////////////////////////////////////////

    /**
     * The algorithm has started to compare the individuals in a new generation
     * with the original program.
     *
     * @param state
     *            Current state of the algorithm.
     * @param population
     *            Population to be compared with the original program.
     */
    void startedComparison(GAState state, GAPopulation population);

    /**
     * The algorithm has started to compare a specific individual with the
     * original program.
     *
     * @param state
     *            Current state of the algorithm.
     * @param population
     *            Population currently under evaluation.
     * @param individual
     *            Individual to be compared.
     */
    void startedComparison(GAState state, GAPopulation population,
            GAIndividual individual);

    /**
     * The algorithm has finished comparing a specific individual with the
     * original program.
     *
     * @param state
     *            Current state of the algorithm.
     * @param population
     *            Population currently under evaluation.
     * @param individual
     *            Individual which was compared.
     * @param comparison
     *            Results of the comparison.
     */
    void finishedComparison(GAState state, GAPopulation population,
            GAIndividual individual, ComparisonResults comparison);

    /**
     * The algorithm has finished comparing the individuals in the current
     * generation.
     *
     * @param state
     *            Current state of the algorithm.
     * @param population
     *            Population which was compared.
     */
    void finishedComparison(GAState state, GAPopulation population);

    // EVALUATION /////////////////////////////////////////

    /**
     * The algorithm has started evaluating the fitness of the individuals in
     * the current generation.
     *
     * @param state
     *            Current state of the algorithm.
     * @param population
     *            Population which is going to be evaluated.
     */
    void startedEvaluation(GAState state, GAPopulation population);

    /**
     * The algorithm has finished evaluating the fitness of the individuals in
     * the current generation.
     *
     * @param state
     *            Current state of the algorithm.
     * @param population
     *            Population which was evaluated. Fitness values are contained
     *            in the GAIndividual objects in the population.
     */
    void finishedEvaluation(GAState state, GAPopulation population);
    

    // GENETIC OPERATOR /////////////////////////////////////////
    
    /**
     * The algorithm has produced a new individual applying a genetic operator.
     * 
     * @param state
     *            Current state of the algorithm.
     * @param geneticOperatorName
     *            Name of the genetic operator which has been applied.
     * @param parents
     * 			  List of the individuals which are going to participate in the genetic operation.
     * @param children
     *            New individuals produced in the genetic operation. 
     */
    void appliedGeneticOperator(GAState state, String geneticOperatorName, 
    		List<GAIndividual> parents, List<GAIndividual> children);
    
    // VALIDATION /////////////////////////////////////////

    /**
     * Validate that the logger has been correctly configured.
     *
     * @throws InvalidConfigurationException
     *             The logger was not correctly configured.
     */
    void validate() throws InvalidConfigurationException;
    
}
