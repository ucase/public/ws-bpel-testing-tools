package es.uca.webservices.gamera.api.util;

/**
 * Used to store values by pairs
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @param <A>
 * @param <B>
 */
public class Pair<A, B> {
	private A a;
	private B b;

        /**
         * Constructor
         * @param a     The value of the first element
         * @param b     The value of the second element
         */
	public Pair(A a, B b) {
		this.setLeft(a);
		this.setRight(b);
	}

        /**
         * Sets the value of the first element
         * @param a     The new value
         */
	public void setLeft(A a) {
		this.a = a;
	}

        /**
         * Returns the value of the first element
         * @return  The current value of the fist element
         */
	public A getLeft() {
		return a;
	}

        /**
         * Sets the value of the second element
         * @param b The new value
         */
	public void setRight(B b) {
		this.b = b;
	}

        /**
         * Returns the value of the second element
         * @return  The current value of the second element
         */
	public B getRight() {
		return b;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((a == null) ? 0 : a.hashCode());
		result = prime * result + ((b == null) ? 0 : b.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Pair<?, ?> other = (Pair<?, ?>) obj;
		if (a == null) {
			if (other.a != null) {
				return false;
			}
		} else if (!a.equals(other.a)) {
			return false;
		}
		if (b == null) {
			if (other.b != null) {
				return false;
			}
		} else if (!b.equals(other.b)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Pair [a=" + a + ", b=" + b + "]";
	}

}
