package es.uca.webservices.gamera.api.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

import es.uca.webservices.gamera.api.GAIndividual;

/**
 * Computes the Euclidean distance between two points.
 */
public final class EuclideanDistance {

	private EuclideanDistance() {}

	/**
	 * Computes the Euclidean distance between two normalised individuals of order 1.
	 */
	public static BigDecimal distance(GAIndividual ind, GAIndividual go) {
		if (ind.getOrder() != go.getOrder()) {
			throw new IllegalArgumentException("Can only compute Euclidean distance between individuals of the same order");
		} else if (ind.getOrder() != 1) {
			throw new IllegalArgumentException("Can only compute Euclidean distance between individuals of order 1");
		}

		final BigInteger indOp = ind.getThOperator(1);
		final BigInteger indLoc = ind.getThLocation(1);
		final BigInteger indAttr = ind.getThAttribute(1);
		final BigInteger goOp = go.getThOperator(1);
		final BigInteger goLoc = go.getThLocation(1);
		final BigInteger goAttr = go.getThAttribute(1);

		BigInteger squaredDistance = indOp.subtract(goOp).pow(2)
				.add(indLoc.subtract(goLoc).pow(2))
				.add(indAttr.subtract(goAttr).pow(2));

		return sqrt(new BigDecimal(squaredDistance), 4);
	}

	/**
	 * Computes a square root of a {@link BigDecimal} through the Babylonian method,
	 * by repeatedly taking the average of an underestimation and overestimation of
	 * the root until we converge.
	 */
	private static BigDecimal sqrt(BigDecimal x, final int scale) {
		final BigDecimal two = new BigDecimal(2L);

		BigDecimal x0 = new BigDecimal("0");
		BigDecimal x1 = new BigDecimal(Math.sqrt(x.doubleValue()));
		while (!x0.equals(x1)) {
			x0 = x1;
			x1 = x.divide(x0, scale, RoundingMode.HALF_UP);
			x1 = x1.add(x0);
			x1 = x1.divide(two, scale, RoundingMode.HALF_UP);
		}

		return x1;
	}
}
