package es.uca.webservices.gamera.api.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to be used to declare options to be shown to the user to configure
 * a component. For the available option types and the valid attributes for each
 * option type, check {@link OptionType}.
 *
 * To get the localized short and long descriptions of an {@link Option}, please
 * use {@link OptionProcessor#getShortDescription} and
 * {@link OptionProcessor#getLongDescription}, respectively.
 *
 * Important: apparently, annotations are not inherited on overriden methods. If
 * you override an method that was annotated in the superclass, you will need to
 * annotate again in the child class.
 *
 * @author Antonio García-Domínguez
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Option {
	/**
	 * Type of option. Determines the kind of widget which should be used in the
	 * interface.
	 */
	OptionType type() default OptionType.STRING;
	
	/**
	 * Determines if the method is optional.
	 */
	boolean optional() default false;
	
	
	/**
	 * Determines if the method must be showed in the YAML editor.
	 */
	boolean hidden() default false;
	
	/**
	 * Determines if the method uses mutation operators
	 */
	boolean operators() default false;

	/**
	 * Extensions of the files to be selected by the user. Optional: if empty,
	 * any file can be selected. Not localizable.
	 */
	String[] fileExtensions() default {};

	/**
	 * If it is a string option, editors should make sure that the value
	 * matches this regular expression.
	 */
	String regex() default ".*";
	
	/**
	 * Short class description.
	 */
	String description() default "";
}
