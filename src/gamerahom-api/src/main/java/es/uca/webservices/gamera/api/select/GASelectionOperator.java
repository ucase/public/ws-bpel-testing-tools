package es.uca.webservices.gamera.api.select;

import java.util.Random;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Interface for all selection operators. Selection operators select individuals
 * among those in a population.
 *
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public interface GASelectionOperator {

    /**
     * Selects an individual from the source population, and returns it.
     *
     * @param source
     *            Source population.
     */
    GAIndividual select(GAPopulation source);

    /**
     * Validates that the selection operator has been correctly configured.
     *
     * @throws InvalidConfigurationException
     *             The operator was not correctly configured.
     */
    void validate() throws InvalidConfigurationException;

    /**
     * Sets the pseudo-random number generator used to select individuals.
     */
	void setPRNG(Random prng);
}
