package es.uca.webservices.gamera.api.predicates;

/**
 * Interface which encapsulates a Boolean predicate over integers.
 *
 * @author Antonio García-Domínguez
 */
public interface IIntegerPredicate {
	boolean evaluate(int value);
}
