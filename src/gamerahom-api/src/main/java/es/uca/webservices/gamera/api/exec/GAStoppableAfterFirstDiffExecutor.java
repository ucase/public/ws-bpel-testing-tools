package es.uca.webservices.gamera.api.exec;

/**
 * <p>Subinterface for executors that can stop after the first difference. Required
 * for some algorithms that allow enabling this option.</p>
 * 
 * <p>Executors should <em>never</em> stop at the first difference by default. This
 * behaviour should be explicitly enabled by the user through the configuration file
 * or a command line flag.</p>
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
public interface GAStoppableAfterFirstDiffExecutor extends GAExecutor {

	/**
	 * Returns <code>true</code> if the executor will stop at the first difference,
	 * reporting the rest of the comparison row as {@link ComparisonResult#SAME_OUTPUT}.
	 */
	boolean getStopAtFirstDiff();

	/**
	 * Changes whether the executor should stop at the first difference (<code>true</code>)
	 * or keep going (<code>false</code>). This method only needs to have effect when called
	 * <em>before</em> calling {@link #prepare()}. It might not have any effect after that. 
	 */
	void setStopAtFirstDiff(boolean stopAtFirstDiff);
}
