package es.uca.webservices.gamera.api.generate;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Stores the options to be used by the genetic algorithm when applying
 * the individual generator that do <emph>not</emph> influence the behaviour
 * of the individual generator itself. For instance, it can be used to store
 * the percentage of individuals that should be generated with it.
 * 
 *  @author Antonio García-Domínguez
 */
public class GAIndividualGeneratorOptions {

	private double percent;

	/**
	 * Returns the percentage of individuals for the next population
	 * that should be generated with the associated individual generator.
	 */
	public double getPercent() {
		return percent;
	}

	/**
	 * Changes the percentage of individuals for the next population
	 * that should be generated with the associated individual generator.
	 *
	 * @param percent New percentage to be set, within the [0, 1] range. A zero value
	 * may be used in order to use the generator for the first generation, but not for
	 * the subsequent ones. During the first generation, the individual generator will
	 * be used in a round-robin fashion regardless of the value of the 'percent' option.
	 */
	public void setPercent(double percent) {
		this.percent = percent;
	}

	/**
	 * Checks if the options used for this individual generator are valid.
	 * 
	 * @throws InvalidConfigurationException
	 *             The options used are not valid.
	 */
	public void validate() throws InvalidConfigurationException {
		if (percent < 0 || percent > 1) {
			throw new InvalidConfigurationException("Percentage must be within the [0, 1] range");
		}
	}

}
