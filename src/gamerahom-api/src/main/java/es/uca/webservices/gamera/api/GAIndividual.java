package es.uca.webservices.gamera.api;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Individual for the genetic algorithm. The individual is encoded using
 * arbitrary-precision integers. Individuals may be normalized to valid
 * operator/location/attribute triplets, or they may not. You can check for this
 * using the {@link #isNormalized()} method.
 * 
 * Individuals are immutable once created. This is important for the correctness
 * of the generational algorithms.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.1
 */
public class GAIndividual {

	/**
	 * This was the old way to represent an uninitialized fitness. GAIndividual will
	 * ignore this fitness value for backwards compatibility with old simrecord
	 * files.
	 */
	private static final double OLD_UNINITIALIZED_FITNESS = -1;

	private static final int MAGIC_NUMBER_HASH1 = 19;
    private static final int MAGIC_NUMBER_HASH2 = 71;
    
    // Order will be between [1 .. maximum order]
	private final int order; 

	// Individuals in a population may have different orders, but they
	// will be always between 1 and the maximum order defined in the
	// configuration.
	private final BigInteger[] operator;
	private final BigInteger[] location;
	private final BigInteger[] attribute;

	// Is the individual normalized?
	private final boolean normalized;

	private Double fitness;

	/** No-argument constructor for SnakeYAML. */
	public GAIndividual() {
		this(0, 0, 0);
	}

	/**
	 * Convenience constructor. The first-order individual is denormalized and its fields can fit in regular <code>int</code>s.
	 */
	public GAIndividual(int operator, int location, int attribute) {
		this(BigInteger.valueOf(operator), BigInteger.valueOf(location), BigInteger.valueOf(attribute));
	}

	/**
	 * Convenience constructor. The fields of the first-order can fit in regular <code>int</code>s.
	 */
	public GAIndividual(int operator, int location, int attribute, boolean normalized) {
		this(BigInteger.valueOf(operator), BigInteger.valueOf(location), BigInteger.valueOf(attribute), normalized);
	}

	/**
	 * Convenience constructor for a first-order denormalized individual.
	 */
	public GAIndividual(BigInteger operator, BigInteger location, BigInteger attribute) {
		this(operator, location, attribute, false);
	}

	/**
	 * Convenience constructor for a first-order individual.
	 */
	public GAIndividual(BigInteger operator, BigInteger location, BigInteger attribute, boolean normalized) {
		this(new BigInteger[] { operator }, new BigInteger[] { location }, new BigInteger[] { attribute }, 1, normalized);
	}

	/**
	 * Convenience constructor for a denormalized individual.
	 */
	public GAIndividual(BigInteger[] operators, BigInteger[] locations, BigInteger[] attributes, int order) {
		this(operators, locations, attributes, order, false);
	}

	/**
	 * Convenience constructor for creating an individual with a different order, but still within the original's {@link #getMaxOrder()}.
	 */
	public GAIndividual(GAIndividual original, int newOrder) {
		this(original.operator, original.location, original.attribute, newOrder, original.normalized);
		assert newOrder <= original.operator.length;
	}

	/**
	 * Creates a new individual.
	 * 
	 * @param operators
	 *            Array with the operators to be applied.
	 * @param locations
	 *            be applied.
	 * @param attributes
	 *            Array with the normalized attribute values to be passed to the
	 *            operator.
	 * @param order
	 *            Order of the individual, from 1 to operators.length.
	 * @param normalized
	 *            Whether the individual is normalized (<code>true</code>) or
	 *            not (<code>false</code>).
	 * @throws IllegalArgumentException
	 *             Operators, locations and attributes do not have the same
	 *             length, or the order is invalid.
	 */
	public GAIndividual(BigInteger[] operators, BigInteger[] locations, BigInteger[] attributes, int order, boolean normalized) {
        if(order < 1 || order > operators.length) {
        	throw new IllegalArgumentException("The individual order should be greater than zero"
        		+ " and lower than the number of mutants");
        }
		if (operators.length != locations.length || operators.length != attributes.length) {
			throw new IllegalArgumentException("All three arrays should have the same length");
		}
        this.operator = operators.clone();
        this.location = locations.clone();
        this.attribute = attributes.clone();
        this.normalized = normalized;
        this.order = order;
	}
	
	/**
	 * Returns the current order of an individual.
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * Returns the maximum order that could be used with the components of this individual.
	 */
	public int getMaxOrder() {
		return operator.length;
	}

	/**
	 * Returns an unmodifiable list with the operators. Trying to change any value in it will throw an exception.
	 */
	public List<BigInteger> getOperator() {
		return Collections.unmodifiableList(Arrays.asList(operator));
	}

	/**
	 * Returns an unmodifiable list with the locations. Trying to change any value in it will throw an exception.
	 */
	public List<BigInteger> getLocation() {
		return Collections.unmodifiableList(Arrays.asList(location));
	}

	/**
	 * Returns an unmodifiable list with the attributes. Trying to change any value in it will throw an exception.
	 */
	public List<BigInteger> getAttribute() {
		return Collections.unmodifiableList(Arrays.asList(attribute));
	}

	/**
	 * Returns the operator field for the <code>i</code>-th order triplet (starting from 1).
	 */
	public BigInteger getThOperator(int i) {
		return this.operator[i - 1];
	}

	/**
	 * Returns the location field for the <code>i</code>-th order triplet (starting from 1).
	 */
	public BigInteger getThLocation(int i) {
		return this.location[i - 1];
	}

	/**
	 * Returns the attribute field for the <code>i</code>-th order triplet (starting from 1).
	 */
	public BigInteger getThAttribute(int i) {
		return this.attribute[i - 1];
	}

	/**
	 * Changes the fitness of an individual. Valid fitness values
	 * must be greater or equal than zero.
	 *s
	 * @throws IllegalArgumentException A negative fitness was provided.
	 */
	public void setFitness(Double fitness) {
		if (fitness == null || fitness >= 0) {
			this.fitness = fitness;
		} else if (fitness == OLD_UNINITIALIZED_FITNESS) {
			// ignore
		} else {
			// fitness < 0 and not the old special value: reject
			throw new IllegalArgumentException(
					String.format("Fitness for individual %s} cannot be negative (%g)",
							formatIndividualFields(), fitness));
		}
	}

	/**
	 * Convenience version of {@link #setFitness(Double)}, to allow auto-promotion
	 * of integer values.
	 */
	public void setFitness(double fitness) {
		this.setFitness((Double) fitness);
	}

	/**
	 * Returns the fitness of an individual
	 */
	public Double getFitness() {
		return fitness;
	}

	/**
	 * Returns the <code>i</code>-th triplet of the individual.
	 * The triplet is composed by the operator, the location and the attribute.
	 */
	public BigInteger[] getMutant(int i) {
		return new BigInteger[] {
			getThOperator(i),
			getThLocation(i),
			getThAttribute(i)
		};
	}

	public String individualFieldsToString() {
		final StringBuilder sb = formatIndividualFields();
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = formatIndividualFields();
		sb.append("} - fitness ");
		sb.append(fitness);
		return sb.toString();
	}

	// NORMALIZATION
	public boolean isNormalized() {
		return normalized;
	}
	
	/**
	 * Locations (l) and attributes (a) of the individuals have to be normalized
	 * according to the next equation: (Y * X)/Z, where Y is l/a attribute
	 * individual, Y is the l/a numbers for those operator and Z is the lcmm
	 * calculated for l/a.
	 * 
	 * If this GAIndividual has been previously normalized, we return it as-is.
	 * 
	 * @param analysis
	 *            Analysis results to be used for normalization.
	 * @return New normalized individual, for which {@link #isNormalized()}
	 *         returns <code>true</code>.
	 */
	public GAIndividual normalize(AnalysisResults analysis) {
		if (isNormalized()) {
			return this;
		}

		final BigInteger[] normOperator  = operator.clone();
		final BigInteger[] normLocation  = location.clone();
		final BigInteger[] normAttribute = attribute.clone();

		for(int i = 0; i < normOperator.length; i++) {
			final BigInteger o = operator[i];
			final BigInteger l = location[i];
			final BigInteger a = attribute[i];

			// Operator is normalized getting the operator from the individual, which
			// will be decremented because it will be used as the 1-based position of
			// the real value in filteredOper (useful information without operator with
			// non-zero locations) 
			int index = o.subtract(BigInteger.ONE).intValue();
			normOperator[i] = analysis.getFilteredOper()[index];

			// Location is normalized applying the normalize operation which
			// receives the location from the individual, the number of locations
			// applicable to the operator identified by the variable j and the maximum
			// location
			normLocation[i] = normalizationOperation(l,
					analysis.getFilteredLoc()[index], 
					analysis.getMaxLoc());

			// Attribute is normalized applying the normalize operation which
			// receives the attribute from the individual, the number of attributes 
			// applicable to the operator identified by the variable j and the maximum
			// attribute
			normAttribute[i] = normalizationOperation(a, 
					analysis.getFilteredAtt()[index], 
					analysis.getMaxAtt());
		}

		return new GAIndividual(normOperator, normLocation, normAttribute, order, true);
	}

	/**
	 * Denormalizes the individual, if not already denormalized. Only
	 * individuals valid according to the bounds established by the
	 * {@link AnalysisResults} may be normalized. Any other individual will
	 * result in an {@link IllegalArgumentException} being thrown.
	 */
	public GAIndividual denormalize(AnalysisResults analysis) {
		if (!isNormalized()) {
			return this;
		}

		final BigInteger[] denormOperator = operator.clone();
		final BigInteger[] denormLocation = location.clone();
		final BigInteger[] denormAttribute = attribute.clone();

		for (int iOrder = 0; iOrder < denormOperator.length; ++iOrder) {
			denormOperator[iOrder] = denormalizeOperator(denormOperator[iOrder], analysis);
			final int iOperator = denormOperator[iOrder].intValue() - 1;

			final BigInteger l = denormLocation[iOrder];
			final BigInteger rangeLocation = analysis.getFilteredLoc()[iOperator];
			if (l.compareTo(BigInteger.ONE) < 0 || l.compareTo(rangeLocation) > 0) {
				throw new IllegalArgumentException(
						"Denormalized location " + l +
						" out of bounds [1, " + analysis.getMaxLoc() +
						"]: cannot denormalize");
			}
			denormLocation[iOrder] = denormalizeLocation(l, rangeLocation, analysis.getMaxLoc());

			final BigInteger a = denormAttribute[iOrder];
			final BigInteger rangeAttribute = analysis.getFilteredAtt()[iOperator];
			if (a.compareTo(BigInteger.ONE) < 0 || a.compareTo(rangeAttribute) > 0) {
				throw new IllegalArgumentException(
						"Denormalized attribute " + a +
						" out of bounds [1, " + analysis.getMaxAtt() +
						"]: cannot denormalize");
			}
			denormAttribute[iOrder] = denormalizeLocation(a, rangeAttribute, analysis.getMaxAtt());
		}

		return new GAIndividual(denormOperator, denormLocation, denormAttribute, getOrder());
	}

	// Useful overrides for HashSet and HashMap collections

	@Override
	public boolean equals(Object o) {
		// Two individuals will be equal if they have the same order
		// (from 1 to maxOrder) and if their values of op-loc-attr are
		// equals until the specified order. Their fitnesses are not
		// compared.
		if (!(o instanceof GAIndividual)) {
			return false;
		} else {
			GAIndividual other = (GAIndividual) o;
			if (order != other.order || normalized != other.normalized) {
				return false;
			} else {
				boolean s = true;
				final int n = other.order;
				int i = 0;
				do {
					s = s && operator[i].equals(other.operator[i])
							&& location[i].equals(other.location[i])
							&& attribute[i].equals(other.attribute[i]);
					i++;
				} while (i < n && s);
				return s;
			}
		}
	}

	@Override
	public int hashCode() {
		// We do not use the fitness to compute the hash code, as it can change depending on many factors 
		HashCodeBuilder builder = new HashCodeBuilder(MAGIC_NUMBER_HASH1, MAGIC_NUMBER_HASH2);
		appendHashCodeFor(builder, operator);
		appendHashCodeFor(builder, location);
		appendHashCodeFor(builder, attribute);
		builder.append(order);
		builder.append(normalized);
		return builder.toHashCode();
	}

	private void appendHashCodeFor(HashCodeBuilder builder, BigInteger[] l) {
		for (int i = 0; i < order; i++) {
			builder.append(l[i]);
		}
	}

	private StringBuilder formatIndividualFields() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append(normalized ? "N" : "D");
		sb.append(order);
		sb.append(" ");
		for (int i = 0; i < order; ++i) {
			if (i > 0) {
				sb.append(", ");
			}
			sb.append(String.format("(%s, %s, %s)", operator[i], location[i],
					attribute[i]));
		}
		return sb;
	}

	private BigInteger denormalizeOperator(final BigInteger o, AnalysisResults analysis) {
		BigInteger rangeOperator = BigInteger.valueOf(analysis.getOperatorNames().length + 1);
		if (o.compareTo(BigInteger.ONE) < 0 || o.compareTo(rangeOperator) > 0) {
			throw new IllegalArgumentException(
				"Denormalized operator " + o +
				" out of bounds [1, " + rangeOperator +
				"]: cannot denormalize");
		}
		final BigInteger[] filteredOper = analysis.getFilteredOper();
		BigInteger denormO = null;
		for (int iOperator = 0; iOperator < filteredOper.length && denormO == null; ++iOperator) {
			if (o.equals(filteredOper[iOperator])) {
				denormO = BigInteger.valueOf(iOperator + 1);
			}
		}
		if (denormO == null) {
			throw new IllegalArgumentException(
				"Could not find normalized operator " + o +
				" among the list of filtered operators: " +
				Arrays.toString(filteredOper));
		}
		return denormO;
	}
	
	private BigInteger denormalizeLocation(final BigInteger normalized, final BigInteger rangeWithinOperator, final BigInteger biLCMM) {
		final BigDecimal bdResult = new BigDecimal(normalized).subtract(BigDecimal.ONE).multiply(new BigDecimal(biLCMM)).divide(new BigDecimal(rangeWithinOperator), RoundingMode.FLOOR).add(BigDecimal.ONE);
		return bdResult.toBigInteger();
	}

	private BigInteger normalizationOperation(final BigInteger denormalized, final BigInteger rangeWithinOperator, final BigInteger biLCMM) {
		final BigDecimal bdResult = new BigDecimal(rangeWithinOperator.multiply(denormalized)).divide(new BigDecimal(biLCMM), RoundingMode.CEILING);
		final BigInteger result = bdResult.toBigInteger();
		if (result.compareTo(BigInteger.ONE) < 0 || result.compareTo(biLCMM) > 0) {
			throw new IllegalArgumentException("Normalized value " + result + " is outside bounds [1," + biLCMM + "]");
		}
		return result;
	}
}