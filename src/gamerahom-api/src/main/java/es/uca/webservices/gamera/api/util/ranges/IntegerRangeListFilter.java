package es.uca.webservices.gamera.api.util.ranges;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.uca.webservices.gamera.api.predicates.IIntegerPredicate;

/**
 * <p>
 * Filters integers using a combination of inclusive and exclusive ranges.
 * </p>
 * 
 * <p>
 * Ranges are provided as strings with comma separated lists of 0+ ranges.
 * Ranges may be a single value ("3" -&gt; [3, 3]) or a pair of values ("1-3"
 * -&gt; [1,3]). The start of a range must be less than or equal to the end of
 * the range. If <code>null</code>, all numbers in <code>[min, max]</code> will
 * be accepted.
 * </p>
 * 
 * <p>
 * This class computes the effective set of ranges of values that are accepted
 * by the filter, merging subsumed and adjacent ranges and taking into account
 * exclusions. The effective set is computed lazily, upon the first invocation
 * of {@link #evaluate(int)} or {@link #getEffectiveRanges()}.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */
public class IntegerRangeListFilter implements IIntegerPredicate {

	private static final Pattern RE_SPLIT_INTO_RANGES = Pattern.compile("\\s*,\\s*");
	private static final Pattern RE_SPLIT_RANGE = Pattern.compile("\\s*(\\S+?)\\s*[-]\\s*(\\S+)\\s*");

	private int min;
	private int max;
	private String includedRanges, excludedRanges;
	private List<Range> effectiveRanges = null;

	/**
	 * Creates a new filter that accepts every number between <code>min</code>
	 * and <code>max</code>.
	 * 
	 * @param min
	 *            Minimum value that should be accepted.
	 * @param max
	 *            Maximum value that should be accepted.
	 * @throws IllegalArgumentException
	 *             <code>min</code> is greater than <code>max</code>.
	 */
	public IntegerRangeListFilter(int min, int max) throws IllegalArgumentException {
		this(min, max, null);
	}

	/**
	 * Creates a new filter that accepts every number in the
	 * <code>includedRanges</code>.
	 * 
	 * @param min
	 *            Minimum value that can be used in <code>includedRanges</code>.
	 * @param max
	 *            Maximum value that can be used in <code>includedRanges</code>.
	 * @param includedRanges
	 *            Comma separated list of 0+ ranges of values to be included. If
	 *            <code>null</code>, all numbers will be included.
	 * @throws IllegalArgumentException
	 *             The effective range set is empty, or one of the ranges used a
	 *             position outside <code>[min, max]</code> or a position that
	 *             could not be converted into an integer with
	 *             {@link #convertValueToInteger(String)}.
	 * @throws IllegalArgumentException
	 *             <code>min</code> is greater than <code>max</code>.
	 */
	public IntegerRangeListFilter(int min, int max, String includedRanges) {
		this(min, max, includedRanges, "");
	}

	/**
	 * Creates a new filter that accepts every number in the
	 * <code>includedRanges</code>, after excluding the numbers in the
	 * <code>excludedRanges</code>.
	 * 
	 * @param min
	 *            Minimum value that can be used in <code>includedRanges</code>
	 *            and <code>excludedRanges</code>.
	 * @param max
	 *            Maximum value that can be used in <code>includedRanges</code>
	 *            and <code>excludedRanges</code>.
	 * @param includedRanges
	 *            Comma separated list of 0+ ranges of values to be included. If
	 *            <code>null</code>, all numbers will be included.
	 * @param excludedRanges
	 *            Comma separated list of 0+ ranges of values to be excluded. If
	 *            <code>null</code>, no numbers will be excluded.
	 * @throws IllegalArgumentException
	 *             <code>min</code> is greater than <code>max</code>.
	 */
	public IntegerRangeListFilter(int min, int max, String includedRanges, String excludedRanges) {
		this.min = min;
		this.max = max;
		this.includedRanges = includedRanges;
		this.excludedRanges = excludedRanges;
		if (min > max) {
			throw new IllegalArgumentException(String.format(
				"The minimum value '%d' cannot be greater than the maximum value '%d'", min, max));
		}
	}

	/**
	 * Returns <code>true</code> if the <code>value</code> is accepted by the
	 * filter, or <code>false</code> otherwise.
	 *
	 * @throws IllegalArgumentException
	 *             The effective range set is empty, or one of the ranges used a
	 *             position outside <code>[min, max]</code> or a position that
	 *             could not be converted into an integer with
	 *             {@link #convertValueToInteger(String)}.
	 */
	public boolean evaluate(int value) {
		for (Range r : getEffectiveRanges()) {
			if (r.isIncluded(value)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Convenience version of {@link #evaluate(int)} that performs the string
	 * to integer conversion on its own.
	 *
	 * @throws IllegalArgumentException
	 *             The effective range set is empty, or one of the ranges used a
	 *             position outside <code>[min, max]</code> or a position that
	 *             could not be converted into an integer with
	 *             {@link #convertValueToInteger(String)}.
	 */
	public boolean evaluate(String key) {
		return evaluate(convertValueToInteger(key));
	}

	/**
	 * Returns an unmodifiable list with the effective ranges of values that are
	 * accepted by this filter, after taking into account all the included and
	 * excluded ranges. The returned ranges are mutually disjoint and are sorted
	 * by minimum value.
	 */
	public synchronized List<Range> getEffectiveRanges() {
		if (effectiveRanges == null) {
			effectiveRanges = new ArrayList<Range>();

			checkedReadMergedRange(min, max, includedRanges, effectiveRanges);
			if (effectiveRanges.isEmpty()) {
				throw new IllegalArgumentException("At least one range must be included");
			}
			if (excludedRanges != null) {
				final List<Range> excluded = new ArrayList<Range>();
				checkedReadMergedRange(min, max, excludedRanges, excluded);
				applyExcludedRangesTo(excluded, effectiveRanges);
			}
		}
		return Collections.unmodifiableList(effectiveRanges);
	}

	/**
	 * Converts a string-based value into an integer that can be filtered. Does
	 * not perform any kind of bounds checking: that is handled elsewhere.
	 */
	protected int convertValueToInteger(String sValue) {
		return Integer.parseInt(sValue.trim());
	}

	private void applyExcludedRangesTo(List<Range> excluded, List<Range> included) {
		for (Range e : excluded) {
			int iIncluded = 0;
			while (iIncluded < included.size()) {
				final Range i = included.get(iIncluded);
				if (i.isIncluded(e.getMin()) && i.isIncluded(e.getMax())) {
					// the excluded range is contained in the included range: split it into two
					included.remove(iIncluded);
					if (e.getMax() < i.getMax()) {
						included.add(iIncluded, new Range(e.getMax() + 1, i.getMax()));
					}
					if (e.getMin() > i.getMin()) {
						included.add(iIncluded, new Range(i.getMin(), e.getMin() - 1));
					}
					iIncluded += 2;
				}
				else if (e.isIncluded(i.getMin())) {
					if (e.isIncluded(i.getMax())) {
						// the included range is contained in the excluded range: remove it
						included.remove(iIncluded);
					}
					else {
						// the excluded range contains part of the beginning of the included range
						included.remove(iIncluded);
						included.add(iIncluded, new Range(e.getMax() + 1,  i.getMax()));
						++iIncluded;
					}
				}
				else if (e.isIncluded(i.getMax())) {
					// the excluded range contains part of the end of the included range
					included.remove(iIncluded);
					included.add(iIncluded, new Range(i.getMin(), e.getMin() - 1));
					++iIncluded;
				}
				else {
					++iIncluded;
				}
			}
			
		}
	}

	private int checkedConvertValueToInteger(int min, int max, String includedRanges, String sRange) {
		final int value = convertValueToInteger(sRange);
		if (value < min || value > max) {
			throw new IllegalArgumentException(
				String.format("Ranges '%s' contain position %d, which is outside [%d, %d]", includedRanges, value, min, max));
		}
		return value;
	}

	private void checkedReadMergedRange(int min, int max, String ranges, final List<Range> destination) {
		if (ranges == null)  {
			destination.add(new Range(min, max));
			return;
		}

		// Empty range list?
		ranges = ranges.trim();
		if (ranges.length() == 0) {
			return;
		}
	
		final String[] sRanges = RE_SPLIT_INTO_RANGES.split(ranges);
		for (String sRange : sRanges) {
			destination.add(checkedReadRange(min, max, ranges, sRange));
		}
		sortByMinAscending(destination);
		mergeSortedRanges(destination);
	}

	private Range checkedReadRange(int min, int max, String allRanges, String sRange) {
		final Matcher sRangePartsMatcher = RE_SPLIT_RANGE.matcher(sRange);
		if (sRangePartsMatcher.matches()) {
			final int rangeMin = checkedConvertValueToInteger(min, max, allRanges, sRangePartsMatcher.group(1)); 
			final int rangeMax = checkedConvertValueToInteger(min, max, allRanges, sRangePartsMatcher.group(2));
			return new Range(rangeMin, rangeMax);
		}
		else {
			final int value = checkedConvertValueToInteger(min, max, allRanges, sRange);
			return new Range(value, value);
		}
	}

	private void mergeSortedRanges(List<Range> ranges) {
		int i = 1;
		while (i < ranges.size()) {
			final Range prev = ranges.get(i - 1);
			final Range curr = ranges.get(i);
			if (prev.isIncluded(curr.getMin()) || curr.getMin() == prev.getMax() + 1) {
				ranges.remove(i - 1);
				ranges.remove(i - 1);
				ranges.add(i - 1, new Range(prev.getMin(), Math.max(prev.getMax(), curr.getMax())));
			}
			else {
				++i;
			}
		}
	}

	private void sortByMinAscending(final List<Range> ranges) {
		Collections.sort(ranges, new Comparator<Range>() {
			@Override
			public int compare(Range o1, Range o2) {
				return o1.getMin() - o2.getMin();
			}
		});
	}

}
