package es.uca.webservices.gamera.api;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;


/**
 * Utility class which allows for iterating over all valid normalized
 * individuals up to a certain order, according to a {@link AnalysisResults}
 * instance. It can be used in a regular Java 5 foreach statement, like this:
 * 
 * <code>
 * for (GAIndividual ind : new ValidIndividualsList(analysis, 2)) {
 *   // do something with the GAIndividual 
 * }
 * </code>
 * 
 * @author Antonio García Domínguez
 */
public class ValidIndividualsList implements Iterable<GAIndividual> {

	private final AnalysisResults analysis;
	private final int maxOrder;

	public ValidIndividualsList(AnalysisResults analysis, int maxOrder) {
		if (maxOrder < 1) {
			throw new IllegalArgumentException("maxOrder must be >= 1");
		}
		if (analysis == null) {
			throw new NullPointerException();
		}
		this.analysis = analysis;
		this.maxOrder = maxOrder;
	}

	public AnalysisResults getAnalysis() {
		return analysis;
	}

	public int getMaxOrder() {
		return maxOrder;
	}

	public List<GAIndividual> all() {
		return (List<GAIndividual>)addAllTo(new ArrayList<GAIndividual>());
	}

	public Collection<GAIndividual> addAllTo(Collection<GAIndividual> destination) {
		for (GAIndividual i : this) {
			destination.add(i);
		}
		return destination;
	}

	@Override
	public Iterator<GAIndividual> iterator() {
		return new ValidIndividualsIterator();
	}

	private class ValidIndividualsIterator implements Iterator<GAIndividual> {

		private boolean bHasNext;

		private final BigInteger[] currentOperator;
		private final BigInteger[] currentLocation;
		private final BigInteger[] currentAttribute;
		private int currentOrder;

		public ValidIndividualsIterator() {
			currentOperator = new BigInteger[maxOrder];
			currentLocation = new BigInteger[maxOrder];
			currentAttribute = new BigInteger[maxOrder];
			currentOrder = 1;

			bHasNext = analysis.getFilteredLoc().length > 0;

			currentOperator[currentOrder - 1] = bHasNext ? analysis.getFilteredOper()[0] : BigInteger.ONE;
			currentLocation[currentOrder - 1] = BigInteger.ONE;
			currentAttribute[currentOrder - 1] = BigInteger.ONE;
		}

		@Override
		public boolean hasNext() {
			return bHasNext;
		}

		@Override
		public GAIndividual next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			// We will return the current individual at the end of this function
			GAIndividual result = new GAIndividual(currentOperator, currentLocation, currentAttribute, currentOrder, true);

			// Let's compute the next individual now, and update hasNext as well
			if (currentOrder < maxOrder) {
				// First, try increasing the order
				++currentOrder;
				currentOperator[currentOrder - 1] = analysis.getFilteredOper()[0];
				currentLocation[currentOrder - 1] = BigInteger.ONE;
				currentAttribute[currentOrder - 1] = BigInteger.ONE;
			}
			else {
				while (currentOrder >= 1 && !moveToNextIndividualWithSameOrder()) {
					// There is no next individual in the current order: move
					// back one order and try again
					--currentOrder;
				}
				if (currentOrder < 1) {
					// There are no more individuals left
					bHasNext = false;
				}
			}

			return result;
		}

		private boolean moveToNextIndividualWithSameOrder()
		{
			final int operator = currentOperator[currentOrder - 1].intValue();
			final BigInteger currentAttr = currentAttribute[currentOrder - 1];
			final BigInteger currentLoc  = currentLocation[currentOrder - 1];

			if (currentAttr.compareTo(analysis.getFieldRanges()[operator - 1]) < 0) {
				// First, try increasing the attribute field
				currentAttribute[currentOrder - 1] = currentAttr.add(BigInteger.ONE);
			} else if (currentLoc.compareTo(analysis.getLocationCounts()[operator-1]) < 0) {
				// If that does not work, increase the location field
				currentAttribute[currentOrder - 1] = BigInteger.ONE;
				currentLocation[currentOrder - 1] = currentLoc.add(BigInteger.ONE);
			} else {
				// Otherwise, try to find the next operator with locations
				int nextOperator;
				for (
						nextOperator = operator + 1;
						nextOperator - 1 < analysis.getLocationCounts().length
							&& analysis.getLocationCounts()[nextOperator - 1].compareTo(BigInteger.ZERO) <= 0
						;
						++nextOperator);
				if (nextOperator - 1 < analysis.getLocationCounts().length) {
					currentOperator[currentOrder - 1] = BigInteger.valueOf(nextOperator);
					currentLocation[currentOrder - 1] = BigInteger.ONE;
					currentAttribute[currentOrder - 1] = BigInteger.ONE;
				}
				else {
					// There are no more operators with locations after the current one
					return false;
				}
			}

			// We managed to increase at least one of the fields in the GAIndividual
			return true;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}
