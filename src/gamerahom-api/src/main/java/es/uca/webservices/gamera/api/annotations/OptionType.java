package es.uca.webservices.gamera.api.annotations;

/**
 * Option types for the {@link Option} annotation.
 *
 * TODO: Implement integer, string and boolean option types.
 */
public enum OptionType {

	/**
	 * This option should be presented as a path to a file, to be read by the
	 * component. The extension of the file should be set in the
	 * {@link Option#fileExtension()} attribute of the {@link Option}. The file
	 * must exist.
	 */
	PATH_LOAD,

	/**
	 * This option should be presented as a path to a file, to be saved by the
	 * component. The extension of the file should be set in the
	 * {@link Option#fileExtension()} attribute of the {@link Option}. The file
	 * does not need to exist.
	 */
	PATH_SAVE,

	/**
	 * This option should be presented as a text field. Valid values should
	 * match the regular expression defined in the {@link Option#regex()}
	 * attribute of the {@link Option}.
	 */
	STRING
}
