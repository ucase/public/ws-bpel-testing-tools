package es.uca.webservices.gamera.api.exec;

public class PreparationException extends Exception {

	private static final long serialVersionUID = 1L;

	public PreparationException(String message) {
		super(message);
	}

	public PreparationException(Throwable cause) {
		super(cause);
	}

	public PreparationException(String message, Throwable cause) {
		super(message, cause);
	}

}
