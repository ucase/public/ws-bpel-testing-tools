package es.uca.webservices.gamera.api;


/**
 * Measures the fitness of a certain individual, based on the current state of
 * the generational algorithm. The algorithm may use this information to select
 * which individuals should be generated next, or they may simply record the
 * value for later reference.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
public interface GAFitnessFunction {

	/**
	 * Computes the fitness of the individual, according to the current state of
	 * the generational algorithm. The fitness of the individual is
	 * <emph>not</emph> updated: this is at the caller's discretion.
	 * 
	 * @param i
	 *            Individual to be evaluated.
	 * @param state
	 *            Current state of the generational algorithm being run.
	 * @return Integer greater than zero with the fitness of <code>i</code>.
	 */
	double computeFitness(GAIndividual i, GAState state);

}
