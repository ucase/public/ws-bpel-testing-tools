package es.uca.webservices.gamera.api.select;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Stores the options to be used by the genetic algorithm when applying
 * the selection operator that do <emph>not</emph> influence the behaviour
 * of the selection operator itself. For instance, it can be used to store
 * the percentage of individuals which should be selected from the old
 * population, to be used in the new population.
 * 
 *  @author Antonio García-Domínguez
 */
public class GASelectionOperatorOptions {

	private double percent;

	/**
	 * Returns the percentage of the individuals in the next generation
	 * which should be produced using this selection operator.
	 */
	public double getPercent() {
		return percent;
	}

	/**
	 * Changes the percentage of the individuals in the next generation
	 * which should be produced using this selection operator.
	 * @param percent New percentage to be used, within the (0, 1] range.
	 */
	public void setPercent(double percent) {
		this.percent = percent;
	}

	/**
	 * Checks if the options used for this selection operator are valid.
	 * @throws InvalidConfigurationException The options used are not valid.
	 */
	public void validate() throws InvalidConfigurationException {
		if (percent < 0 || percent > 1) {
			throw new InvalidConfigurationException("Percent must be within the [0, 1] range");
		}
	}

}
