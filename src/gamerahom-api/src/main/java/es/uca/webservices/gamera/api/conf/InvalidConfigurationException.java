package es.uca.webservices.gamera.api.conf;

/**
 * Exception for all configurations which are valid YAML, but are either
 * incomplete, do not set up a component correct or produce exceptions while
 * validating.
 *
 * @author Antonio García-Domínguez
 * @version 1.1
 */
public class InvalidConfigurationException extends Exception {
    private static final long serialVersionUID = 1L;

    public InvalidConfigurationException(String msg) {
        super(msg);
    }

	public InvalidConfigurationException(String msg, Throwable e) {
		super(msg, e);
	}
}
