package es.uca.webservices.gamera.api.exec;

import java.io.File;
import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Interface for all executors which generate and run mutants of some program in
 * a certain language. Comparisons are done in batches, to take advantage of
 * parallelization, if available.
 *
 * @author Antonio García Domínguez, Emma Blanco-Muñoz
 * @version 1.2
 */
public interface GAExecutor {
	/**
	 * Java system property that should be set by launch scripts
	 * to the directory where the application is installed.
	 */
	static final String INSTALL_DIR_SYS_PROP = "install.dir";

	/**
	 * Prepares the environment to run the comparisons.
	 *
	 * @throws PreparationException
	 *             There was a problem while preparing the environment.
	 */
	void prepare() throws PreparationException;

	/**
	 * Cleans up after running all the comparisons.
	 *
	 * @throws PreparationException
	 *             There was a problem while cleaning up the environment.
	 */
	void cleanup() throws PreparationException;

	/**
	 * Analyzes the original program and reports the required parameters for the
	 * GA. The original program must have been set by the user previously.
	 * 
	 * This method may be called before {@link #prepare} is called and after
	 * {@link #cleanup()} is called. In some contexts, it is important to be
	 * able to perform an analysis without incurring the overhead of bringing up
	 * the full execution environment for the mutants.
	 * 
	 * @param monitor
	 *            Optional progress monitor to be kept up to date about how much
	 *            work is left. Can be <code>null</code>.
	 * @throws AnalysisException
	 *             There was a problem while analyzing the program.
	 */
	AnalysisResults analyze(GAProgressMonitor monitor) throws AnalysisException;

	/**
	 * Runs several programs while comparing their outputs to the original one,
	 * and returns the results of the comparisons.
	 * 
	 * {@link #prepare()} should have been called before calling this method for
	 * the first time. After all the comparisons are done, users of this class
	 * should invoke the {@link #cleanup()} to free up any resources.
	 * 
	 * @param monitor
	 *            Optional progress monitor to be kept up to date about how much
	 *            work is left. Can be <code>null</code>.
	 * @param mutants
	 *            Individuals whose mutants' outputs are to be compared with the
	 *            output of the original program.
	 * @throws ComparisonException
	 *             There was a problem while comparing the mutants to the
	 *             original program.
	 */
	ComparisonResults[] compare(GAProgressMonitor monitor,
			GAIndividual... individuals) throws ComparisonException;

	/**
	 * Loads a data file that will configure the tests to be run using the test
	 * suite. This data file will be used for all subsequent runs unless a
	 * different file is set later on, or the file is unset by passing
	 * <code>null</code>. This method may be called even before {@link #prepare()}.
	 * 
	 * @throws OperationNotSupportedException
	 *             This executor cannot load external data files into its test
	 *             suites.
	 */
	void loadDataFile(File dataFile) throws UnsupportedOperationException;

	/**
	 * If the executor supports it, returns the file for the original program.
	 *
	 * @throws UnsupportedOperationException
	 *             This executor does not represent mutants using single files.
	 */
	File getOriginalProgramFile() throws UnsupportedOperationException;

	/**
	 * If the executor supports it, returns the file with the mutant produced
	 * from the specified individual. If the file has not been produced yet,
	 * generate the mutant on the fly.
	 *
	 * @param individual
	 *            Individual whose file representing the mutant is to be
	 *            returned.
	 * @throws UnsupportedOperationException
	 *             This executor does not represent mutants using single files.
	 * @throws GenerationException
	 *             There was an error while generating the mutant on the fly.
	 */
	File getMutantFile(GAIndividual individual)
			throws UnsupportedOperationException, GenerationException;

	/**
	 * Returns the list of the names of the test cases in the test suite, if
	 * available. Otherwise, it returns <code>null</code>.
	 */
	List<String> getTestCaseNames() throws AnalysisException;

	/**
	 * Validates that the executor has been correctly configured.
	 *
	 * @throws InvalidConfigurationException
	 *             The executor has not been correctly configured.
	 */
	void validate() throws InvalidConfigurationException;

	/**
	 * If the executor supports it, returns the list of the names of all the
	 * mutation operators (whether they are applicable or not to the program
	 * under test).
	 */
	List<String> getMutationOperatorNames() throws UnsupportedOperationException;

	/**
	 * If the executor supports it, returns the actual name of the mutant represented by this individual
	 * @param ind
	 * @return String with 
	 * @throws UnsupportedOperationException
	 */
	//String mutantName(GAIndividual ind) throws UnsupportedOperationException;
}
