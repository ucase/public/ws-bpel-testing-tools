package es.uca.webservices.gamera.api.generate;

import java.util.Random;

import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Interface for new individual generators. These generators produce new
 * individuals without taking previously existing individuals into account.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public interface GAIndividualGenerator {

	/**
	 * Generates one individual, adding it at the end of the provided population.
	 * 
	 * @param state    Current state of the genetic algorithm.
	 * @param maxOrder Maximum order of the individuals.
	 * @param src      Previous population to use for deriving any required
	 *                 information. This population will remain unchanged.
	 * @param dst      Population to which the new individual will be added.
	 */
	void generate(GAState state, int maxOrder, GAPopulation src, GAPopulation dst);

	/**
	 * Validates that the generator has been correctly configured.
	 * 
	 * @throws InvalidConfigurationException
	 *             The generator was not correctly configured.
	 */
	void validate() throws InvalidConfigurationException;

	/**
	 * Sets the pseudo-random number generator to be used to generate individuals.
	 */
	void setPRNG(Random prng);
}
