package es.uca.webservices.gamera.api.exec;

public class GenerationException extends Exception {

	private static final long serialVersionUID = 1L;

	public GenerationException(String message) {
		super(message);
	}

	public GenerationException(Throwable cause) {
		super(cause);
	}

	public GenerationException(String message, Throwable cause) {
		super(message, cause);
	}

}
