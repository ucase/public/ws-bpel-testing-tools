package es.uca.webservices.gamera.api;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.uca.webservices.gamera.api.predicates.IIntegerPredicate;

/**
 * Results produced after analyzing the original program.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.1
 */
public class AnalysisResults {

    private String[] operatorNames;
    private BigInteger[] locationCounts;
    private BigInteger[] fieldRanges;
    
    // Next parameters are needed in the generation of individuals, in the
    // mutation operator and also, in the normalization process
    private List<BigInteger> operatorValues;

    private BigInteger maxOp;
    private BigInteger maxLoc;
    private BigInteger maxAtt;

    private BigInteger[] filteredOper;
    private BigInteger[] filteredLoc;
    private BigInteger[] filteredAtt;
    
    public AnalysisResults() {
    	operatorNames = new String[0];
    	locationCounts = new BigInteger[0];
    	fieldRanges = new BigInteger[0];
    }
    
    public AnalysisResults(String[] opNames, BigInteger[] locCounts, BigInteger[] fieldRanges) {
    	assert opNames.length == locCounts.length && locCounts.length == fieldRanges.length
    		: "There should be as many locations and attribute ranges as operators";
    	this.operatorNames  = opNames.clone();
        this.locationCounts = locCounts.clone();
        this.fieldRanges    = fieldRanges.clone();
    }

    /**
	 * Returns a new AnalysisResults object after filtering the available
	 * operators with the provided <code>filter</code>. Only those operators for
	 * which the predicate evaluates to <code>true</code> will be used.
	 */
    public AnalysisResults filterOperators(IIntegerPredicate filter) {
    	final BigInteger[] newLocs = locationCounts.clone();

    	for (int i = 0; i < operatorNames.length; ++i) {
			if (!filter.evaluate(1 + i)) {
				newLocs[i] = BigInteger.ZERO;
    		}
    	}

    	return new AnalysisResults(operatorNames, newLocs, fieldRanges);
    }

    public String[] getOperatorNames() {
    	return operatorNames.clone();
    }

	public BigInteger[] getLocationCounts() {
        return locationCounts.clone();
    }

    public BigInteger[] getFieldRanges() {
        return fieldRanges.clone();
    }

	public synchronized List<BigInteger> getOperatorValues() {
		if(operatorValues == null) {
			calculateMaxValues();
		}
		return operatorValues;
	}

	public synchronized BigInteger getMaxOp() {
		if(maxOp == null) {
			calculateMaxValues();
		}
		return maxOp;
	}

	public synchronized BigInteger getMaxLoc() {
		if(maxLoc == null) {
			calculateMaxValues();
		}
		return maxLoc;
	}

	public synchronized BigInteger getMaxAtt() {
		if(maxAtt == null) {
			calculateMaxValues();
		}
		return maxAtt;
	}

	public synchronized BigInteger[] getFilteredOper() {
		if(filteredOper == null) {
			calculateMaxValues();
		}
		return filteredOper;
	}

	public synchronized BigInteger[] getFilteredLoc() {
		if(filteredLoc == null) {
			calculateMaxValues();
		}
		return filteredLoc;
	}

	public synchronized BigInteger[] getFilteredAtt() {
		if(filteredAtt == null) {
			calculateMaxValues();
		}
		return filteredAtt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(fieldRanges);
		result = prime * result + Arrays.hashCode(locationCounts);
		result = prime * result + Arrays.hashCode(operatorNames);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnalysisResults other = (AnalysisResults) obj;
		if (!Arrays.equals(fieldRanges, other.fieldRanges))
			return false;
		if (!Arrays.equals(locationCounts, other.locationCounts))
			return false;
		if (!Arrays.equals(operatorNames, other.operatorNames))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AnalysisResults [operatorNames="
				+ Arrays.toString(operatorNames) + ", locationCounts="
				+ Arrays.toString(locationCounts) + ", fieldRanges="
				+ Arrays.toString(fieldRanges) + "]";
	}

	private void calculateMaxValues() {
		// This value is the operators' numerical representation
        operatorValues = new ArrayList<BigInteger>();
       
        //We save the indexes of the non-zero locations which will be the same
        // as the operator that can be applied
        for (int i = 0; i < locationCounts.length; i++) {
            if (locationCounts[i].intValue() > 0) {
                // It is necessary the increment because the index starts in
                // zero and the first operator is represented by one
                operatorValues.add(BigInteger.valueOf(i+1));
            }
        }
        
        // We create three vectors with the maximum number of non-zero locations
        filteredOper = new BigInteger[operatorValues.size()];
        filteredLoc = new BigInteger[operatorValues.size()];
        filteredAtt = new BigInteger[operatorValues.size()];
 
        for (int i = 0; i < operatorValues.size(); i++) {
            // We get the n-th value of operatorValues and complete the fields
            filteredOper[i] = operatorValues.get(i);
            filteredLoc[i] = locationCounts[operatorValues.get(i).intValue()-1];
            filteredAtt[i] = fieldRanges[operatorValues.get(i).intValue()-1];
        }

        maxOp = BigInteger.valueOf(filteredOper.length);
        maxLoc = lcmm(filteredLoc);
        maxAtt = lcmm(filteredAtt);
	}
    
    private BigInteger lcm(BigInteger a, BigInteger b) {
        return a.multiply(b).divide(a.gcd(b));
    }

    private BigInteger lcmm(BigInteger[] args) {
    	if (args.length == 0) {
    		return BigInteger.ZERO;
    	}

        BigInteger result = args[0];
        for (int i = 1; i < args.length; i++) {
            result = lcm(result, args[i]);
        }
        return result;
    }

}