package es.uca.webservices.gamera.api.util.ranges;

/**
 * Non-empty range <code>[a, b]</code>, where <code>a</code> and <code>b</code> are both integers.
 *
 * @author Antonio García-Domínguez
 */
public class Range {

	private final int min;
	private final int max;

	public Range(int min, int max) {
		this.min = min;
		this.max = max;
		if (max < min) {
			throw new IllegalArgumentException(String.format("range [%d, %d] is empty", min, max));
		}
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}

	public boolean isIncluded(int value) {
		return value >= min && value <= max;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + max;
		result = prime * result + min;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Range other = (Range) obj;
		if (max != other.max)
			return false;
		if (min != other.min)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Range [min=" + min + ", max=" + max + "]";
	}
}
