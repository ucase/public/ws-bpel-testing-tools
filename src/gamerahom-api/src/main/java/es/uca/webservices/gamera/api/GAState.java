package es.uca.webservices.gamera.api;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Overall state of the algorithm. The current individual is not stored, as it
 * changes too quickly and is only valid during the comparison stage. Timing
 * information is limited to the time at which the test was started, and the
 * time saved by using a simulation record (if any).
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.1
 */
public class GAState {

    private static final BigInteger NANOS_TO_MILLIS = BigInteger.valueOf(1000000);
	private long totalNumberOfMutants  = 0;
    private long currentGeneration     = 0;
    private Integer sizePopulation = 0;

    // Nanoseconds since the epoch since the GA was started
    private BigInteger startNanos = BigInteger.valueOf(System.nanoTime());
    // Nanoseconds saved through caching (if any)
    private BigInteger cachedNanos = BigInteger.ZERO;

    private AnalysisResults analysisResults = new AnalysisResults();
    private GAHof hof = new GAHof(analysisResults);

    private double[] sumExecutionMatrix = null;
    
    //TCE
    private List<String> equivalentMutants = new ArrayList<String>();
    private List<List<String>> duplicateMutants = new ArrayList<List<String>>();
    
    private List<List<String>> duplicateMutantsGenerated = new ArrayList<List<String>>();
    
    //Nuevo
    private Map<String, List<String>> ancestors = new HashMap<String, List<String>>();
    
    public void setCurrentGeneration(long currentGeneration) {
        this.currentGeneration = currentGeneration;
    }

    public long getCurrentGeneration() {
        return currentGeneration;
    }

    public long getSizePopulation() {
		return sizePopulation;
	}

	public void setSizePopulation(Integer sizePopulation) {
		this.sizePopulation = sizePopulation;
	}
    
    public double[] getSumExecutionMatrix() {
		return sumExecutionMatrix;
	}

	public void setSumExecutionMatrix(double[] sumExecutionMatrix) {
		this.sumExecutionMatrix = sumExecutionMatrix;
	}

    public void updateExecutionMatrix(GAHof hof, GAPopulation population) {
	
    	// Compute per-column sums of the execution matrix
    	final int[] mutantsKilledByTestCase = hof.getMutantsKilledByTestCase();
    	final int numTestCases = mutantsKilledByTestCase.length;
    	sumExecutionMatrix = new double[numTestCases];

		for (int iTestCase = 0; iTestCase < numTestCases; iTestCase++) {
			// Results for the population
			for (GAIndividual individual : population.getIndividuals()) {
				ComparisonResults cr = hof.get(individual);
				if(cr.isDead()) {
					final ComparisonResult comparisonResult = cr.getRow()[iTestCase];
					// TODO ideally, this should be separated into its own class
					final double resultValue = comparisonResult.isDead() ? 1 : (comparisonResult.isExecuted() ? 0.5 : 0);
					sumExecutionMatrix[iTestCase] += resultValue;
				}
			}
			// Results for the HOF (saved in mutantsKilledByTestCase)
			sumExecutionMatrix[iTestCase] += mutantsKilledByTestCase[iTestCase];
		}
    }

    public void setAnalysisResults(AnalysisResults analysisResults) {
        this.analysisResults = analysisResults;
    }

    public AnalysisResults getAnalysisResults() {
        return analysisResults;
    }

    public void setTotalNumberOfMutants(long totalNumberOfMutants) {
        this.totalNumberOfMutants = totalNumberOfMutants;
    }

    public long getTotalNumberOfMutants() {
        return totalNumberOfMutants;
    }

	public void setHof(GAHof hof) {
		this.hof = hof;
	}

	public GAHof getHof() {
		return hof;
	}

	/**
	 * Returns the nanoseconds since the epoch since the GA was started.
	 * By default, this field is set to {@link System#nanoTime()}
	 * when the GAState object is created.
	 * 
	 * Note: to compute the total time used by the algorithm, you should use
	 * {@link #getElapsedNanos()} instead, as that takes into account the
	 * time saved by simulation records as well.
	 */
	public BigInteger getStartTimeNanos() {
		return startNanos;
	}

	/**
	 * Sets the nanoseconds since the epoch since the GA was started.
	 */
	public void setStartTimeNanos(BigInteger startNanos) {
		this.startNanos = startNanos;
	}

	/**
	 * Returns the nanoseconds saved by caching.
	 */
	public BigInteger getCachedNanos() {
		return cachedNanos;
	}

	/**
	 * Changes the nanoseconds saved by caching.
	 */
	public void setCachedNanos(BigInteger simulatedNanos) {
		this.cachedNanos = simulatedNanos;
	}

	/**
	 * Increases the nanoseconds saved by caching by <code>delta</code>.
	 */
	public void addCachedNanos(BigInteger delta) {
		this.cachedNanos = this.cachedNanos.add(delta);
	}

	/**
	 * Returns the sum of the nanoseconds since the GA started
	 * and the time saved through caching.
	 */
	public BigInteger getElapsedNanos() {
		return BigInteger.valueOf(System.nanoTime()).subtract(startNanos).add(cachedNanos);
	}

	/**
	 * Returns the sum of the milliseconds since the GA started
	 * and the time saved through caching.
	 */
	public BigInteger getElapsedMillis() {
		return getElapsedNanos().divide(NANOS_TO_MILLIS);
	}
	
	public List<String> getEquivalentMutants() {
		return equivalentMutants;
	}

	public void setEquivalentMutants(List<String> equivalentMutants) {
		this.equivalentMutants = equivalentMutants;
	}

	public List<List<String>> getDuplicateMutants() {
		return duplicateMutants;
	}

	public void setDuplicateMutants(List<List<String>> duplicateMutants) {
		this.duplicateMutants = duplicateMutants;
	}

	public List<List<String>> getDuplicateMutantsGenerated() {
		return duplicateMutantsGenerated;
	}

	public void setDuplicateMutantsGenerated(
			List<List<String>> duplicateMutantsGenerated) {
		this.duplicateMutantsGenerated = duplicateMutantsGenerated;
	}
	 
    public Boolean isDuplicateMutant(String mutantName){
		
		//First: check whether a mutant equivalent to this was previously generated
		if(isMutantInDuplicateGenerated(mutantName))
			return true;
		//Second: check whether this is a mutant tagged as duplicate. In that case, transfer the row in which this mutant is to 
		else{
			transferMutantIfDuplicate(mutantName);
		}
		
		return false;
	}
    
    private Boolean isMutantInDuplicateGenerated(String mutantName){
    	
    	for(List<String> line : duplicateMutantsGenerated){
    		for(String mutant : line){
    			if(mutantName.equals(mutant))
    				return true;
    		}
    	}
    	
    	return false;
    }
    
    private void transferMutantIfDuplicate(String mutantName){

    	for(List<String> line : duplicateMutants){
    		for(String mutant : line){
    			if(mutantName.equals(mutant)){
    				//Transfer the whole line for the next time a duplicate mutant is found
    				duplicateMutantsGenerated.add(line);	
    			}
    		}
    	}
    }

	public Map<String, List<String>> getAncestors() {
		return ancestors;
	}

	public void setAncestors(Map<String, List<String>> ancestors) {
		this.ancestors = ancestors;
	}
	

	//Nuevo
	public Boolean isMutantAlreadyInTree(GAIndividual child){
		
		GAIndividual normalizedChild = child.normalize(getAnalysisResults());
        //String childName = "m" + normalizeCode(normalizedChild.getOperator().get(0)) + "_" + normalizedChild.getLocation().get(0) + "_" + normalizedChild.getAttribute().get(0);
        
		/*for (Iterator<GAIndividual> iAux = source.getIndividuals().iterator(); iAux.hasNext(); ) {
			GAIndividual norm = iAux.next().normalize(getAnalysisResults());
	        String name = "m" + normalizeCode(norm.getOperator().get(0)) + "_" + norm.getLocation().get(0) + "_" + norm.getAttribute().get(0);
	    
	        if(childName.equals(name))
	        	return true;
	    } */
        
        if(hof.keySet().contains(normalizedChild))
        	return true;
        
    	return false;
	}
	
	//Nuevo
	public void addMutantToTree(GAIndividual child, List<GAIndividual> ancs){
		
		GAIndividual normalizedChild = child.normalize(getAnalysisResults());
        String childName = "m" + normalizeCode(normalizedChild.getOperator().get(0)) + "_" + normalizedChild.getLocation().get(0) + "_" + normalizedChild.getAttribute().get(0);
       
        List<String> ancsString = new ArrayList<String>();
        for(GAIndividual anc : ancs){
        
        	GAIndividual normalizedAnc = anc.normalize(getAnalysisResults());
        	String ancName = "m" + normalizeCode(normalizedAnc.getOperator().get(0)) + "_" + normalizedAnc.getLocation().get(0) + "_" + normalizedAnc.getAttribute().get(0);
        	ancsString.add(ancName);
        }
		
		ancestors.put(childName, ancsString);
	}
	
	public String normalizeCode(BigInteger thOperator) {
		//Normalize the code of the operator with two digits
		if(thOperator.compareTo(new BigInteger("10")) == -1)
			return "0" + thOperator.toString();
		else
			return thOperator.toString();
	}
	
}
