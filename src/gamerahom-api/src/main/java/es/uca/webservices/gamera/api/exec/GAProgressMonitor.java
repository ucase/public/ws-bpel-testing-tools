package es.uca.webservices.gamera.api.exec;

/**
 * <p>Interface for a progress monitor, acting as callback for long-running
 * operations and providing facilities for checking completion or early
 * cancellation.</p>
 * 
 * <p>Typical usage from the UI will follow this pseudocode:</p>
 * <pre>
 * start(TOTAL_UNITS);
 * for (UnitOfWork : ...) {
 *   status("my status");
 *   // do things
 *   done(1);
 *   if (isCancelled()) break;
 * }
 * // isDone() should return true now, unless cancelled
 * </pre>
 *
 * @author Antonio García-Domínguez
 */
public interface GAProgressMonitor {

	/**
	 * Notifies the monitor that a task composed of <code>nUnits</code> units of
	 * work has been started.
	 * 
	 * @param nUnits Units of work that the task is made up of.
	 */
	void start(int nUnits);

	/**
	 * Provides the monitor with a human-readable description of the step of the
	 * task currently under execution.
	 * 
	 * @param msg
	 *            Message to be shown to the user about the progress of the
	 *            current task.
	 */
	void status(String msg);

	/**
	 * <p>Notifies the monitor that <code>nUnits</code> units of work have been
	 * completed. When all the units of work reported in {@link #start(int)} are
	 * done, {@link #isDone()} should return <code>true</code>.</p>
	 * 
	 * @param nUnits Units of work that have been completed.
	 */
	void done(int nUnits);

	/**
	 * Indicates if the task should be cancelled as soon as possible. This
	 * method will be usually called from the task itself, to check if it should
	 * stop or continue running.
	 * 
	 * @return <code>true</code> if the task should be cancelled as soon as
	 *         possible, <code>false</code> otherwise.
	 */
	boolean isCancelled();
}
