package es.uca.webservices.gamera;

import org.junit.Test;

import es.uca.webservices.gamera.api.ComparisonResults;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests for the {@link ComparisonResults} class.
 * @author Antonio Garcia-Dominguez.
 */
public class ComparisonResultsTest {

    @Test
    public void alive() {
        final ComparisonResults cr = new ComparisonResults(1, 1, 1, 0, 0, 0);
        assertTrue(cr.isAlive());
        assertFalse(cr.isInvalid());
        assertFalse(cr.isDead());
    }

    @Test
    public void dead() {
        final Integer[] row = new Integer[] {0, 0, 0};
        for (int i = 0; i < row.length; ++i) {
            row[i] = 1;
            ComparisonResults cr = new ComparisonResults(1, 1, 1, row);
            assertTrue(cr.isDead());
            assertFalse(cr.isAlive());
            assertFalse(cr.isInvalid());
            row[i] = 0;
        }
    }

    @Test
    public void invalid() {
        final ComparisonResults cr = new ComparisonResults(1, 1, 1, 2, 2, 2);
        assertTrue(cr.isInvalid());
        assertFalse(cr.isAlive());
        assertFalse(cr.isDead());
    }
}
