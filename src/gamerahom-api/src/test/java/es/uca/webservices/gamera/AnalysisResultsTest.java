package es.uca.webservices.gamera;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.predicates.IIntegerPredicate;

/**
 * Tests for the {@link AnalysisResults} class.
 *
 * @author Antonio García-Domínguez
 */
public class AnalysisResultsTest {

	private AnalysisResults originalResults;

	@Before
	public void setup() {
		originalResults = new AnalysisResults(
			new String[] { "a", "b", "c", "d" },
			bi(1, 2, 3, 4),
			bi(5, 6, 7, 8)
		);
	}

	@Test
	public void filteringNoneReturnsSameResults() {
		AnalysisResults filtered = originalResults.filterOperators(new IIntegerPredicate() {
			@Override
			public boolean evaluate(int value) {
				return true;
			}
		});
		assertEquals(originalResults, filtered);
	}

	@Test
	public void filteringAllReturnsEmptyResults() {
		AnalysisResults filtered = originalResults.filterOperators(new IIntegerPredicate() {
			@Override
			public boolean evaluate(int value) {
				return false;
			}
		});
		assertArrayEquals(bi(0, 0, 0, 0), filtered.getLocationCounts());
	}

	@Test
	public void filteringFirst() {
		AnalysisResults filtered = originalResults.filterOperators(new IIntegerPredicate() {
			@Override
			public boolean evaluate(int value) {
				return value != 1;
			}
		});
		assertArrayEquals(bi(0, 2, 3, 4), filtered.getLocationCounts());
	}

	@Test
	public void filteringMiddle() {
		AnalysisResults filtered = originalResults.filterOperators(new IIntegerPredicate() {
			@Override
			public boolean evaluate(int value) {
				return value != 2 && value != 3;
			}
		});
		assertArrayEquals(bi(1, 0, 0, 4), filtered.getLocationCounts());
	}

	@Test
	public void filteringLast() {
		AnalysisResults filtered = originalResults.filterOperators(new IIntegerPredicate() {
			@Override
			public boolean evaluate(int value) {
				return value != 4;
			}
		});
		assertArrayEquals(new String[] { "a", "b", "c", "d" }, filtered.getOperatorNames());
		assertArrayEquals(bi(1, 2, 3, 0), filtered.getLocationCounts());
		assertArrayEquals(bi(5, 6, 7, 8), filtered.getFieldRanges());
	}

	private static BigInteger[] bi(int... values) {
		final BigInteger[] result = new BigInteger[values.length];
		for (int i = 0; i < result.length; ++i) {
			result[i] = BigInteger.valueOf(values[i]);
		}
		return result;
	}
}
