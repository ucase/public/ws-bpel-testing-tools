package es.uca.webservices.gamera.util.ranges;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import es.uca.webservices.gamera.api.util.ranges.IntegerRangeListFilter;
import es.uca.webservices.gamera.api.util.ranges.Range;

/**
 * Tests for the {@link IntegerRangeListFilter} class.
 *
 * @author Antonio García-Domínguez
 */
public class IntegerRangeListFilterTest {

	@Test(expected=IllegalArgumentException.class)
	public void endBeforeStartIsInvalid() {
		new IntegerRangeListFilter(0, -1);
	}

	@Test
	public void sameStartAndEndIsValid() {
		new IntegerRangeListFilter(0, 0);
	}

	@Test
	public void endAfterStartIsValid() {
		new IntegerRangeListFilter(0, 1);
	}

	@Test
	public void minimumIsIncluded() {
		assertTrue(new IntegerRangeListFilter(0, 5).evaluate(0));
	}

	@Test
	public void maximumIsIncluded() {
		assertTrue(new IntegerRangeListFilter(0, 5).evaluate(5));
	}

	@Test
	public void lessThanMinimumIsExcluded() {
		assertFalse(new IntegerRangeListFilter(0, 5).evaluate(-1));
	}

	@Test
	public void greaterThanMaximumIsExcluded() {
		assertFalse(new IntegerRangeListFilter(0, 5).evaluate(6));
	}

	@Test
	public void betweenMinAndMaxIsIncluded() {
		assertTrue(new IntegerRangeListFilter(0, 5).evaluate(2));
	}

	@Test
	public void effectiveRangeMatchesMinAndMax() {
		final List<Range> ranges = new IntegerRangeListFilter(2, 6).getEffectiveRanges();
		assertRangesAre(ranges, new Range(2, 6));
	}

	@Test(expected=UnsupportedOperationException.class)
	public void effectiveRangesAreUnmodifiable() {
		final List<Range> ranges = new IntegerRangeListFilter(2, 6).getEffectiveRanges();
		ranges.add(new Range(3, 5));
	}

	@Test
	public void nullRangeIncludesAll() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, null).getEffectiveRanges();
		assertRangesAre(ranges, new Range(1, 100));
	}

	@Test(expected=IllegalArgumentException.class)
	public void atLeastOneRangeMustBeSpecifiedIfNotNull() {
		new IntegerRangeListFilter(1, 100, "").getEffectiveRanges();
	}

	@Test
	public void positionsAreTreatedAsRanges() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 3, "2").getEffectiveRanges();
		assertRangesAre(ranges, new Range(2, 2));
	}

	@Test(expected=IllegalArgumentException.class)
	public void invalidPositionsAreRejected() {
		new IntegerRangeListFilter(1, 3, "2, 4").getEffectiveRanges();
	}

	@Test
	public void multiplePositions() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 3, " 1, 3 ").getEffectiveRanges();
		assertRangesAre(ranges, new Range(1, 1), new Range(3, 3));
	}

	@Test
	public void positionsAreSortedByMinimumValue() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 10, "4,2").getEffectiveRanges();
		assertRangesAre(ranges, new Range(2, 2), new Range(4, 4));
	}

	@Test
	public void singleRangeBothValid() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 10, "2-5").getEffectiveRanges();
		assertRangesAre(ranges, new Range(2, 5));
	}

	@Test
	public void singleRangeFirstNegative() {
		final List<Range> ranges = new IntegerRangeListFilter(-10, 10, "-9-7").getEffectiveRanges();
		assertRangesAre(ranges, new Range(-9, 7));
	}

	@Test
	public void singleRangeBothNegative() {
		final List<Range> ranges = new IntegerRangeListFilter(-10, -5, "-9--7").getEffectiveRanges();
		assertRangesAre(ranges, new Range(-9, -7));
	}

	@Test(expected=IllegalArgumentException.class)
	public void invalidRangeWithTooManyComponents() {
		new IntegerRangeListFilter(1, 10, "2-5-7").getEffectiveRanges();
	}

	@Test(expected=IllegalArgumentException.class)
	public void invalidRangeMinimum() {
		new IntegerRangeListFilter(1, 10, "-1-5").getEffectiveRanges();
	}

	@Test(expected=IllegalArgumentException.class)
	public void invalidRangeMaximum() {
		new IntegerRangeListFilter(1, 10, "9-20").getEffectiveRanges();
	}

	@Test
	public void disjointRanges() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 10, "2 - 4, 6 - 8").getEffectiveRanges();
		assertRangesAre(ranges, new Range(2, 4), new Range(6, 8));
	}

	@Test
	public void disjointRangesAreSortedByMinimum() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 10, "6 - 8, 2 - 4").getEffectiveRanges();
		assertRangesAre(ranges, new Range(2, 4), new Range(6, 8));
	}

	@Test
	public void repeatedRangesAreRemoved() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 10, "2-5,2-5").getEffectiveRanges();
		assertRangesAre(ranges, new Range(2, 5));
	}

	@Test
	public void subsumedRangesAreRemoved() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 10, "2-5,3-4").getEffectiveRanges();
		assertRangesAre(ranges, new Range(2, 5));
	}

	@Test
	public void overlappingRangesAreMerged() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 10, "2-5,3-7").getEffectiveRanges();
		assertRangesAre(ranges, new Range(2, 7));
	}

	@Test
	public void adjacentRangesAreMerged() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 10, "2-5,6-7").getEffectiveRanges();
		assertRangesAre(ranges, new Range(2, 7));
	}

	@Test
	public void includeAllExcludeNone() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, null, "").getEffectiveRanges();
		assertRangesAre(ranges, new Range(1, 100));
	}

	@Test
	public void includeAllExcludeOne() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, null, "9").getEffectiveRanges();
		assertRangesAre(ranges, new Range(1, 8), new Range(10, 100));
	}

	@Test
	public void includeAllExcludeMiddleRange() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, null, "21-30").getEffectiveRanges();
		assertRangesAre(ranges, new Range(1, 20), new Range(31, 100));
	}

	@Test
	public void includeAllExcludeAtBegin() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, null, "1-5").getEffectiveRanges();
		assertRangesAre(ranges, new Range(6, 100));
	}

	@Test
	public void includeAllExcludeAtEnd() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, null, "91-100").getEffectiveRanges();
		assertRangesAre(ranges, new Range(1, 90));
	}

	@Test
	public void includeSomeExcludeWholeRange() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, "20-40,60-70", "50-85").getEffectiveRanges();
		assertRangesAre(ranges, new Range(20, 40));
	}

	@Test
	public void includeSomeExcludeEqual() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, "20-40,60-70", "20-40").getEffectiveRanges();
		assertRangesAre(ranges, new Range(60, 70));
	}

	@Test
	public void includeSomeExcludePartOfBeginning() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, "20-40,60-70", "55-64").getEffectiveRanges();
		assertRangesAre(ranges, new Range(20, 40), new Range(65, 70));
	}

	@Test
	public void includeSomeExcludeRangeBeforeMinimum() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, "20-40,60-70", "10-40").getEffectiveRanges();
		assertRangesAre(ranges, new Range(60, 70));
	}

	@Test
	public void includeSomeExcludePartOfEnd() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, "20-40,60-70", "35-45").getEffectiveRanges();
		assertRangesAre(ranges, new Range(20, 34), new Range(60, 70));
	}

	@Test
	public void includeSomeExcludeRangeBeyondMaximum() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100, "20-40,60-70", "20-50").getEffectiveRanges();
		assertRangesAre(ranges, new Range(60, 70));
	}

	@Test
	public void fullScenario() {
		final List<Range> ranges = new IntegerRangeListFilter(1, 100,
				"1, 3 - 9, 10, 20 - 40, 12 - 15, 60 - 70, 89",
				"30 - 50, 13, 50 - 64").getEffectiveRanges();
		assertRangesAre(ranges,
			new Range(1, 1), new Range(3,10), new Range(12, 12),
			new Range(14,15), new Range(20, 29), new Range(65,70),
			new Range(89,89));
	}

	private void assertRangesAre(List<Range> obtained, Range... expected) {
		assertArrayEquals(expected, obtained.toArray(new Range[expected.length]));
	}
}
