package es.uca.webservices.gamera.util.ranges;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import es.uca.webservices.gamera.api.util.ranges.IntegerRangeListFilter;
import es.uca.webservices.gamera.api.util.ranges.Range;
import es.uca.webservices.gamera.api.util.ranges.StringArrayRangeListFilter;

/**
 * Tests for the {@link StringArrayRangeListFilter} class.
 *
 * @author Antonio García-Domínguez
 */
public class StringArrayRangeListFilterTest {

	@Test(expected=IllegalArgumentException.class)
	public void emptyArraysAreDisallowed() {
		new StringArrayRangeListFilter(Collections.<String> emptyList());
	}

	@Test
	public void singleElementRangeIsAccepted() {
		final List<Range> ranges = new StringArrayRangeListFilter(Arrays.asList("foo")).getEffectiveRanges();
		assertEquals(1, ranges.size());
		assertEquals(new Range(1, 1), ranges.get(0));
	}

	@Test
	public void threeElementRangeIsAccepted() {
		final List<Range> ranges = new StringArrayRangeListFilter(Arrays.asList("foo", "bar", "baa")).getEffectiveRanges();
		assertEquals(1, ranges.size());
		assertEquals(new Range(1, 3), ranges.get(0));
	}

	@Test
	public void includedRangesAreAvailable() {
		final List<Range> ranges = new StringArrayRangeListFilter(Arrays.asList("foo", "bar", "baa"), "1-2").getEffectiveRanges();
		assertEquals(1, ranges.size());
		assertEquals(new Range(1, 2), ranges.get(0));
	}

	@Test
	public void excludedRangesAreAvailable() {
		final List<Range> ranges = new StringArrayRangeListFilter(
			Arrays.asList("foo", "bar", "baa", "xyz"), "2-4", "3").getEffectiveRanges();
		assertEquals(2, ranges.size());
		assertEquals(new Range(2, 2), ranges.get(0));
		assertEquals(new Range(4, 4), ranges.get(1));
	}

	@Test
	public void canUseStringBasedPositions() {
		final IntegerRangeListFilter filter = new StringArrayRangeListFilter(Arrays.asList("a", "b", "c", "d"), "1,d");
		final List<Range> ranges = filter.getEffectiveRanges();
		assertEquals(2, ranges.size());
		assertEquals(new Range(1, 1), ranges.get(0));
		assertEquals(new Range(4, 4), ranges.get(1));
		assertTrue(filter.evaluate(1));
		assertTrue(filter.evaluate(4));
		assertTrue(filter.evaluate("d"));
	}

	@Test
	public void canUseStringBasedRanges() {
		final IntegerRangeListFilter filter = new StringArrayRangeListFilter(Arrays.asList("a", "b", "c", "d", "e"), "a-D", "C-e");
		final List<Range> ranges = filter.getEffectiveRanges();
		assertEquals(1, ranges.size());
		assertEquals(new Range(1, 2), ranges.get(0));
		assertTrue(filter.evaluate("a"));
		assertTrue(filter.evaluate("b"));
		assertFalse(filter.evaluate("c"));
	}

	@Test
	public void canUseMixedRanges() {
		final IntegerRangeListFilter filter = new StringArrayRangeListFilter(Arrays.asList("a", "b", "c", "d", "e"), "1-d", "c-5");
		final List<Range> ranges = filter.getEffectiveRanges();
		assertEquals(1, ranges.size());
		assertEquals(new Range(1, 2), ranges.get(0));
		assertTrue(filter.evaluate("a"));
		assertTrue(filter.evaluate("b"));
		assertFalse(filter.evaluate("c"));
	}

}
