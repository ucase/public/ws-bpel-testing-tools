package es.uca.webservices.gamera;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertArrayEquals;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAHof;

/**
 * Tests for the {@link GAHof} class.
 *
 * @author Antonio García-Domínguez
 */
public class GAHofTest {

	private GAHof hof;

	@Before
	public void setUp() {
		hof = new GAHof(
			new AnalysisResults(
				new String[] { "a", "b" },
				new BigInteger[] { BigInteger.ONE, BigInteger.valueOf(2) },
				new BigInteger[] { BigInteger.valueOf(2), BigInteger.ONE }
			));
	}

	@Test
	public void emptyHofHasNullSums() {
		assertNull(hof.getMutantsKilledByTestCase());
		assertEquals(0, hof.getValidMutants());
	}

	@Test
	public void oneAlive() {
		hof.put(new ComparisonResults(1, 1, 1, true, 0, 0, 0));
		assertArrayEquals(new int[]{0, 0, 0}, hof.getMutantsKilledByTestCase());
		assertEquals(1, hof.getValidMutants());
	}

	@Test
	public void oneInvalid() {
		hof.put(new ComparisonResults(1, 1, 1, true, 2, 2, 2));
		assertArrayEquals(new int[]{0, 0, 0}, hof.getMutantsKilledByTestCase());
		assertEquals(0, hof.getValidMutants());
	}

	@Test
	public void oneDead100() {
		hof.put(new ComparisonResults(1, 1, 1, true, 1, 0, 0));
		assertArrayEquals(new int[]{1, 0, 0}, hof.getMutantsKilledByTestCase());
		assertEquals(1, hof.getValidMutants());
	}

	@Test
	public void oneDead010() {
		hof.put(new ComparisonResults(1, 1, 1, true, 0, 1, 0));
		assertArrayEquals(new int[]{0, 1, 0}, hof.getMutantsKilledByTestCase());
		assertEquals(1, hof.getValidMutants());
	}

	@Test
	public void oneDead001() {
		hof.put(new ComparisonResults(1, 1, 1, true, 0, 0, 1));
		assertArrayEquals(new int[]{0, 0, 1}, hof.getMutantsKilledByTestCase());
		assertEquals(1, hof.getValidMutants());
	}

	@Test
	public void mix() {
		hof.put(new ComparisonResults(1, 1, 1, true, 0, 0, 1));
		hof.put(new ComparisonResults(1, 1, 2, true, 1, 0, 1));
		hof.put(new ComparisonResults(2, 1, 1, true, 2, 2, 2));
		hof.put(new ComparisonResults(2, 2, 1, true, 0, 1, 1));

		// Repeated result shouldn't be taken into account
		hof.put(new ComparisonResults(1, 1, 1, true, 0, 0, 1));

		assertArrayEquals(new int[]{1, 1, 3}, hof.getMutantsKilledByTestCase());
		assertEquals(3, hof.getValidMutants());
	}

}
