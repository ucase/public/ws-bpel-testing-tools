package es.uca.webservices.gamera.util;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.List;

import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.ValidIndividualsList;

/**
 * Unit tests for the {@link ValidIndividualsList} class.
 *
 * @author Antonio García-Domínguez
 */
public class ValidIndividualsListTest {

	private static final int INDS_FIELD_RANGES_ORDER1 = 1 * 4 + 2 * 5 + 3 * 6;
	private static final AnalysisResults ANALYSIS_FIELD_RANGES = new AnalysisResults(
		new String[] { "a", "b", "c" },
		new BigInteger[] { BigInteger.ONE, BigInteger.valueOf(2), BigInteger.valueOf(3) },
		new BigInteger[] { BigInteger.valueOf(4), BigInteger.valueOf(5), BigInteger.valueOf(6) }
	);

	private static final AnalysisResults ANALYSIS_ZERO_THREE = new AnalysisResults(
			new String[] { "a", "b" },
			new BigInteger[] { BigInteger.ZERO, BigInteger.valueOf(3) },
			new BigInteger[] { BigInteger.ONE, BigInteger.ONE }
		);

	private static final AnalysisResults ANALYSIS_TWO_THREE = new AnalysisResults(
		new String[] { "a", "b" },
		new BigInteger[] { BigInteger.valueOf(2), BigInteger.valueOf(3) },
		new BigInteger[] { BigInteger.ONE, BigInteger.ONE }
	);

	private static final AnalysisResults ANALYSIS_ALL_ONES = new AnalysisResults(
		new String[] { "a" },
		new BigInteger[] { BigInteger.ONE },
		new BigInteger[] { BigInteger.ONE }
	);

	@Test
	public void individualList_empty() {
		final List<GAIndividual> inds = l(new AnalysisResults(), 1);
		assertEquals(0, inds.size());
	}

	@Test
	public void individualList_one_order1() {
		final List<GAIndividual> inds = l(ANALYSIS_ALL_ONES,	1);
		assertEquals(1, inds.size());
	}

	@Test
	public void individualList_one_order2() {
		final List<GAIndividual> inds = l(ANALYSIS_ALL_ONES,	2);
		assertEquals(2, inds.size());
	}

	@Test
	public void individualList_one_order5() {
		final List<GAIndividual> inds = l(ANALYSIS_ALL_ONES,	5);
		assertEquals(5, inds.size());
	}

	@Test
	public void individualList_zeroThree_order1() {
		final List<GAIndividual> inds = l(ANALYSIS_ZERO_THREE, 1);
		assertEquals(3, inds.size());
	}

	@Test
	public void individualList_zeroThree_order2() {
		final List<GAIndividual> inds = l(ANALYSIS_ZERO_THREE, 2);
		assertEquals(3 + 3*3, inds.size());
	}

	@Test
	public void individualList_twoThree_order1() {
		final List<GAIndividual> inds = l(ANALYSIS_TWO_THREE, 1);
		assertEquals(5, inds.size());
	}

	@Test
	public void individualList_twoThree_order2() {
		final List<GAIndividual> inds = l(ANALYSIS_TWO_THREE, 2);
		assertEquals(5 + 25, inds.size());
	}

	@Test
	public void individualList_twoThree_order3() {
		final List<GAIndividual> inds = l(ANALYSIS_TWO_THREE, 3);
		assertEquals(5 + 25 + 125, inds.size());
	}

	@Test
	public void individualList_withFieldRanges_order1() {
		final List<GAIndividual> inds = l(ANALYSIS_FIELD_RANGES, 1);
		assertEquals(INDS_FIELD_RANGES_ORDER1, inds.size());
	}

	@Test
	public void individualList_withFieldRanges_order2() {
		final List<GAIndividual> inds = l(ANALYSIS_FIELD_RANGES, 2);
		assertEquals(INDS_FIELD_RANGES_ORDER1 + INDS_FIELD_RANGES_ORDER1 * INDS_FIELD_RANGES_ORDER1, inds.size());
	}

	private List<GAIndividual> l(final AnalysisResults analysis, final int maxOrder)
	{
		return new ValidIndividualsList(analysis, maxOrder).all();
	}
}
