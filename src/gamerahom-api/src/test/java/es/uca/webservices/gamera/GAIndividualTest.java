package es.uca.webservices.gamera;

import java.math.BigInteger;
import org.junit.*;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

/**
 * Tests for the {@link GAIndividual} class.
 * 
 * @author Emma Blanco-Muñoz
 */

public class GAIndividualTest {
    
    private final int order = 2;
    
    private final BigInteger[] a = {BigInteger.valueOf(1),BigInteger.valueOf(2),
        BigInteger.valueOf(3), BigInteger.valueOf(4),BigInteger.valueOf(5)};
    private final BigInteger[] b = {BigInteger.valueOf(6),BigInteger.valueOf(7),
        BigInteger.valueOf(8), BigInteger.valueOf(9),BigInteger.valueOf(0)};
    private final BigInteger[] c = {BigInteger.valueOf(9),BigInteger.valueOf(8),
        BigInteger.valueOf(7), BigInteger.valueOf(6),BigInteger.valueOf(5)};

    private final BigInteger[] d = {BigInteger.valueOf(1),BigInteger.valueOf(2),
        BigInteger.valueOf(4), BigInteger.valueOf(5),BigInteger.valueOf(6)};
    private final BigInteger[] e = {BigInteger.valueOf(6),BigInteger.valueOf(7),
        BigInteger.valueOf(9), BigInteger.valueOf(0),BigInteger.valueOf(1)};
    private final BigInteger[] f = {BigInteger.valueOf(9),BigInteger.valueOf(8),
        BigInteger.valueOf(8), BigInteger.valueOf(7),BigInteger.valueOf(6)};

    @Test 
    public void orderEqualsToNumberMutants() {
         new GAIndividual(a, b, c, 5);
    }
    
    @Test 
    public void orderEqualsToOne() {
         new GAIndividual(a, b, c, 1);
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void orderGreaterThanNumberMutants() {
         assertNull(new GAIndividual(a, b, c, 6));
    }
        
    @Test
    public void completelyIdenticalIndividualsSameOrder() {
 		 GAIndividual indA = new GAIndividual(a, b, c, order);
         GAIndividual indB = new GAIndividual(cloneArray(a), cloneArray(b), cloneArray(c), order);
         assertTrue(indA.equals(indB));
         assertThat(indA.hashCode(), is(indB.hashCode()));    
    }

	@Test
    public void completelyIdenticalIndividualsDiffOrder() {
         GAIndividual indA = new GAIndividual(a, b, c, order);
         GAIndividual indB = new GAIndividual(a, b, c, order+1);
         assertFalse(indA.equals(indB));
         assertThat(indA.hashCode(), is(not(indB.hashCode())));    
    } 
    
    @Test
    public void identicalIndividualsSameOrder() {
         GAIndividual indA = new GAIndividual(a, b, c, order);
         GAIndividual indB = new GAIndividual(d, e, f, order);
         assertTrue(indA.equals(indB));
         assertThat(indA.hashCode(), is(indB.hashCode()));    
    } 
    
    @Test
    public void identicalIndividualsDiffOrder() {
         GAIndividual indA = new GAIndividual(a, b, c, order);
         GAIndividual indB = new GAIndividual(d, e, f, order+1);
         
         assertFalse(indA.equals(indB));
         assertThat(indA.hashCode(), is(not(indB.hashCode())));    
    }  
    
    @Test
    public void differentIndividualsSameOrder() {
         GAIndividual indA = new GAIndividual(a, b, c, order);
         GAIndividual indB = new GAIndividual(f, d, e, order);
         
         assertFalse(indA.equals(indB));
         assertThat(indA.hashCode(), is(not(indB.hashCode())));    
    }   
        
    @Test
    public void differentIndividualsDiffOrder() {
         GAIndividual indA = new GAIndividual(a, b, c, order);
         GAIndividual indB = new GAIndividual(f, d, e, order+1);
         
         assertFalse(indA.equals(indB));
         assertThat(indA.hashCode(), is(not(indB.hashCode())));    
    }

    @Test
    public void normalizationToFirstOperand() {
    	final AnalysisResults analysis = new AnalysisResults(
    			new String[] {"a", "b", "c"},
    			new BigInteger[] { BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(5) },
    			new BigInteger[] { BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(5) }
    	);
    	assertEquals(createIndNorm(1, 1, 1), new GAIndividual(1, 1, 1).normalize(analysis));
    	assertEquals(createIndNorm(1, 1, 1), new GAIndividual(1, 15, 15).normalize(analysis));
    	assertEquals(createIndNorm(1, 2, 2), new GAIndividual(1, 16, 16).normalize(analysis));
    	assertEquals(createIndNorm(1, 2, 2), new GAIndividual(1, 30, 30).normalize(analysis));
    	assertEquals(createIndNorm(2, 1, 1), new GAIndividual(2, 1, 1).normalize(analysis));
    	assertEquals(createIndNorm(2, 1, 1), new GAIndividual(2, 10, 10).normalize(analysis));
    	assertEquals(createIndNorm(2, 2, 2), new GAIndividual(2, 11, 11).normalize(analysis));
    	assertEquals(createIndNorm(2, 2, 2), new GAIndividual(2, 20, 20).normalize(analysis));
    	assertEquals(createIndNorm(2, 3, 3), new GAIndividual(2, 21, 21).normalize(analysis));
    	assertEquals(createIndNorm(2, 3, 3), new GAIndividual(2, 30, 30).normalize(analysis));
    	assertEquals(createIndNorm(3, 1, 1), new GAIndividual(3, 1, 1).normalize(analysis));
    	assertEquals(createIndNorm(3, 1, 1), new GAIndividual(3, 6, 6).normalize(analysis));
    	assertEquals(createIndNorm(3, 2, 2), new GAIndividual(3, 7, 7).normalize(analysis));
    	assertEquals(createIndNorm(3, 2, 2), new GAIndividual(3, 12, 12).normalize(analysis));
    	assertEquals(createIndNorm(3, 3, 3), new GAIndividual(3, 13, 13).normalize(analysis));
    	assertEquals(createIndNorm(3, 3, 3), new GAIndividual(3, 18, 18).normalize(analysis));
    	assertEquals(createIndNorm(3, 4, 4), new GAIndividual(3, 19, 19).normalize(analysis));
    	assertEquals(createIndNorm(3, 4, 4), new GAIndividual(3, 24, 24).normalize(analysis));
    	assertEquals(createIndNorm(3, 5, 5), new GAIndividual(3, 25, 25).normalize(analysis));
    	assertEquals(createIndNorm(3, 5, 5), new GAIndividual(3, 30, 30).normalize(analysis));
    }

    @Test
    public void denormalizationToFirstOperand() {
    	final AnalysisResults analysis = new AnalysisResults(
    			new String[] {"a", "b", "c", "d"},
    			new BigInteger[] { BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(0), BigInteger.valueOf(5) },
    			new BigInteger[] { BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(0), BigInteger.valueOf(5) }
    	);
    	assertEquals(new GAIndividual(1, 1, 1), createIndNorm(1, 1, 1).denormalize(analysis));
    	assertEquals(new GAIndividual(1, 16, 16), createIndNorm(1, 2, 2).denormalize(analysis));
    	assertEquals(new GAIndividual(2, 1, 1), createIndNorm(2, 1, 1).denormalize(analysis));
    	assertEquals(new GAIndividual(2, 11, 11), createIndNorm(2, 2, 2).denormalize(analysis));
    	assertEquals(new GAIndividual(2, 21, 21), createIndNorm(2, 3, 3).denormalize(analysis));
    	assertEquals(new GAIndividual(3, 1, 1), createIndNorm(4, 1, 1).denormalize(analysis));
    	assertEquals(new GAIndividual(3, 7, 7), createIndNorm(4, 2, 2).denormalize(analysis));
    	assertEquals(new GAIndividual(3, 13, 13), createIndNorm(4, 3, 3).denormalize(analysis));
    	assertEquals(new GAIndividual(3, 19, 19), createIndNorm(4, 4, 4).denormalize(analysis));
    	assertEquals(new GAIndividual(3, 25, 25), createIndNorm(4, 5, 5).denormalize(analysis));
    }

	private GAIndividual createIndNorm(final int operator, final int location, final int attribute) {
		return new GAIndividual(operator, location, attribute, true);
	}

	private BigInteger[] cloneArray(final BigInteger[] array) {
		final BigInteger[] cloneA = new BigInteger[array.length];
	     for (int i = 0; i < array.length; ++i) {
	    	 cloneA[i] = new BigInteger(array[i].toString());
	     }
		return cloneA;
	}

}
