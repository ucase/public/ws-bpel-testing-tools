package es.uca.webservices.gamera.select;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.select.GASelectionOperatorOptions;

/**
 * Tests for the {@link GASelectorOperationOptions} class.
 *
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class GASelectionOperatorOptionsTest {

	private GASelectionOperatorOptions ops;

	@Before
	public void setUp() {
		ops = new GASelectionOperatorOptions();
	}

	@Test
	public void zeroValid() throws InvalidConfigurationException {
		ops.setPercent(0);
		ops.validate();
	}

	@Test
	public void greaterThanZeroValid() throws InvalidConfigurationException {
		ops.setPercent(0.5);
		ops.validate();
	}

	@Test
	public void oneWorks() throws InvalidConfigurationException {
		ops.setPercent(1);
		ops.validate();
	}

	@Test(expected=InvalidConfigurationException.class)
	public void negativeNotValid() throws InvalidConfigurationException {
		ops.setPercent(-0.1);
		ops.validate();
	}

	@Test(expected=InvalidConfigurationException.class)
	public void greaterThanOneNotValid() throws InvalidConfigurationException {
		ops.setPercent(1.1);
		ops.validate();
	}
}
