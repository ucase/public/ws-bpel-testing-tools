package es.uca.webservices.bptsgen;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ShippingSyncTest extends AbstractCompositionTest{
	public ShippingSyncTest (){
		super("ShippingSync/shipping.bpel");
	}
	
	@Test
	public void assertWSDLName() {
		assertEquals("The WSDL name should be shippingLT.wsdl", getWSDLName(),
				"shippingLT.wsdl");
	}

	@Test
	public void countPartners() {
		assertPartnerCount(0);
	}

	@Test
	public void clientTrackName() {
		assertClientTrackService("shippingService");
	}

	@Test
	public void countPartnerTrack() {
		assertPartnerTrackCount(0);
	}
}
