package es.uca.webservices.bptsgen;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MetaSearchTest extends AbstractCompositionTest {
	public MetaSearchTest() {
		super("MetaSearch/MetaSearchBPEL2.bpel");
	}

	@Test
	public void assertWSDLName() {
		assertEquals("The WSDL name should be MetaSearch.wsdl", getWSDLName(),
				"MetaSearch.wsdl");
	}

	@Test
	public void countPartners() {
		assertPartnerCount(2);
	}

	@Test
	public void clientTrackName() {
		assertClientTrackService("MetaSearch");
	}

	@Test
	public void countPartnerTrack() {
		assertPartnerTrackCount(2);
	}
}
