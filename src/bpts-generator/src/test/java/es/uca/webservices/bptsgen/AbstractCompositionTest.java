package es.uca.webservices.bptsgen;

import static org.junit.Assert.*;

import java.net.URL;

import net.bpelunit.framework.exception.SpecificationException;
import net.bpelunit.framework.verify.TestSuiteXMLValidator;
import net.bpelunit.framework.xml.suite.XMLTestCase;
import net.bpelunit.framework.xml.suite.XMLTestSuite;
import net.bpelunit.framework.xml.suite.XMLTestSuiteDocument;

import org.junit.Before;
import org.junit.Test;

public abstract class AbstractCompositionTest {
	private String pathToBPELComposition;
	private XMLTestSuiteDocument bpts;
	private XMLTestSuite testSuiteLab;
	protected XMLTestCase testCase;

	public AbstractCompositionTest(String pathToBPELComposition) {
		this.pathToBPELComposition = pathToBPELComposition;
	}

	@Before
	public void setUp() throws Exception {
		BPTSGenerator bg = new BPTSGenerator("src/test/resources/" + pathToBPELComposition);

		// For testing, we want to use a fixed seed, as initializing from /dev/random takes a good while.
		bg.setSeed("0000000000000000");

		bpts = bg.generateTestTemplate();
		bg.printTestTemplate();
		testSuiteLab = bpts.getTestSuite();
		testCase = testSuiteLab.getTestCases().getTestCaseArray(0);
	}

	@Test
	public void BPTSValidation() throws SpecificationException {
		TestSuiteXMLValidator testSuiteValidator = new TestSuiteXMLValidator();
		testSuiteValidator.validate(bpts);
	}

	@Test
	public void nameNotNull() {
		assertNotNull("The name of the bpts file should not be null",
				testSuiteLab.getName());
	}

	@Test
	public void correctlyURL() {
		assertEquals("Ther baseURL should be: http://localhost:7777/ws",
				testSuiteLab.getBaseURL(), "http://localhost:7777/ws");
	}

	public String getWSDLName() {
		return testSuiteLab.getDeployment().getPut().getWsdl();
	}

	public void assertPartnerCount(int nPartners) {
		assertEquals(testSuiteLab.getDeployment().getPartnerList().size(),
				nPartners);
	}

	public void assertClientTrackService(String serviceName) {
		assertEquals(testCase.getClientTrack().getSendReceiveArray(0)
				.getService().getLocalPart(), serviceName);
	}

	public void assertPartnerTrackCount(int nPartners) {
		assertEquals(testCase.getPartnerTrackList().size(), nPartners);
	}
}
