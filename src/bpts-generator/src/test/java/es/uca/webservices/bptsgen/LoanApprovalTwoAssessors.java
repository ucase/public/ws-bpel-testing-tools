package es.uca.webservices.bptsgen;

import static org.junit.Assert.*;

import java.util.List;

import net.bpelunit.framework.xml.suite.XMLPartnerTrack;

import org.junit.Test;

public class LoanApprovalTwoAssessors extends AbstractCompositionTest {
	public LoanApprovalTwoAssessors() {
		super("LoanApprovalTwoAssessors/LoanApprovalProcess.bpel");
	}

	@Test
	public void assertWSDLName() {
		assertEquals("The WSDL name should be LoanService.wsdl", getWSDLName(),
				"LoanService.wsdl");
	}

	@Test
	public void countPartners() {
		assertPartnerCount(2);
	}

	@Test
	public void clientTrackName() {
		assertClientTrackService("LoanServiceService");
	}

	@Test
	public void countPartnerTrack() {
		assertPartnerTrackCount(2);
	}
	
	@Test
	public void assertAssessorSendReceives(){
		assertTrue(assertReceiveSendFromPartnerTrack("assessor"));
	}
	
	private boolean assertReceiveSendFromPartnerTrack(String name) {
		List<XMLPartnerTrack> partnerTracks = testCase.getPartnerTrackList();
		for (XMLPartnerTrack partnerTrack : partnerTracks) {
			if (partnerTrack.getName().equals(name)) {
				if (partnerTrack.getReceiveSendList().size() != 2) {
					return false;
				}
			}
		}
		return true;
	}
}
