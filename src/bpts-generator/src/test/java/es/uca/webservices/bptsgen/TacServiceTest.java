package es.uca.webservices.bptsgen;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TacServiceTest extends AbstractCompositionTest{
	public TacServiceTest (){
		super("TacService/tacService.bpel");
	}

	@Test
	public void assertWSDLName() {
		assertEquals("The WSDL name should be tacService.wsdl", getWSDLName(),
				"tacService.wsdl");
	}

	@Test
	public void countPartners() {
		assertPartnerCount(0);
	}

	@Test
	public void clientTrackName() {
		assertClientTrackService("tacService");
	}

	@Test
	public void countPartnerTrack() {
		assertPartnerTrackCount(0);
	}
}
