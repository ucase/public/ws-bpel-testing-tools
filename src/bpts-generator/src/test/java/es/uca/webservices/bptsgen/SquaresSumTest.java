package es.uca.webservices.bptsgen;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SquaresSumTest extends AbstractCompositionTest {
	public SquaresSumTest() {
		super("SquaresSum/squaresSum.bpel");
	}
	
	@Test
	public void assertWSDLName() {
		assertEquals("The WSDL name should be squaresSum.wsdl", getWSDLName(),
				"squaresSum.wsdl");
	}

	@Test
	public void countPartners() {
		assertPartnerCount(0);
	}

	@Test
	public void clientTrackName() {
		assertClientTrackService("squaresSumService");
	}

	@Test
	public void countPartnerTrack() {
		assertPartnerTrackCount(0);
	}
}
