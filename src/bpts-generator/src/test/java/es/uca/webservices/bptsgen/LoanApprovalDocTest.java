package es.uca.webservices.bptsgen;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoanApprovalDocTest extends AbstractCompositionTest {
	public LoanApprovalDocTest() {
		super("LoanApprovalDoc/LoanApprovalProcess.bpel");
	}

	@Test
	public void assertWSDLName() {
		assertEquals("The WSDL name should be LoanService.wsdl", getWSDLName(),
				"LoanService.wsdl");
	}

	@Test
	public void countPartners() {
		assertPartnerCount(2);
	}

	@Test
	public void clientTrackName() {
		assertClientTrackService("LoanServiceService");
	}

	@Test
	public void countPartnerTrack() {
		assertPartnerTrackCount(2);
	}
}
