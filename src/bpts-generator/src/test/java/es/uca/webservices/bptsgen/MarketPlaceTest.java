package es.uca.webservices.bptsgen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class MarketPlaceTest extends AbstractCompositionTest{
	public MarketPlaceTest(){
		super("MarketPlace/marketplace.bpel");
	}
	
	@Test
	public void assertWSDLName() {
		assertEquals("The WSDL name should be marketplace.wsdl", getWSDLName(),
				"marketplace.wsdl");
	}

	@Test
	public void countPartners() {
		assertPartnerCount(1);
	}

	@Test
	public void clientTrackName() {
		assertClientTrackService("marketplaceSeller");
	}

	@Test
	public void countPartnerTrack() {
		assertPartnerTrackCount(1);
	}
	
	@Test
	public void assertCorrelationSetIDFromPartnerTrack() {
		String template = testCase.getPartnerTrackArray(0).getSendReceiveArray(0).getSend()
				.getTemplate().toString();
		assertTrue(template.contains("submit.get(0)"));
	}
}
