package es.uca.webservices.bptsgen;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LoanApprovalRPCTest extends AbstractCompositionTest{
	public LoanApprovalRPCTest(){
		super("LoanApprovalRPC/loanApprovalProcess.bpel");
	}
	@Test
	public void assertWSDLName() {
		assertEquals("The WSDL name should be LoanService.wsdl", getWSDLName(),
				"LoanService.wsdl");
	}
	
	@Test
	public void countPartners(){
		assertPartnerCount(2);
	}
	
	@Test
	public void clientTrackName(){
		assertClientTrackService("LoanService");
	}
	
	@Test
	public void countPartnerTrack(){
		assertPartnerTrackCount(2);
	}
}
