package es.uca.webservices.bptsgen;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.wsdl.Definition;
import javax.wsdl.Port;
import javax.wsdl.Service;
import javax.wsdl.WSDLException;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;

import org.apache.xmlbeans.XmlException;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TCorrelationSet;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TImport;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TPartnerLink;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TPartnerLinks;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TProcess;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import serviceAnalyzer.messageCatalog.TypeOperation;
import serviceAnalyzer.messageCatalog.TypeService;
import serviceAnalyzer.messageCatalog.TypeTemplate;

import com.ibm.wsdl.Constants;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.InvalidProcessException;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.bpel.wsdl.BPELPropertyAlias;
import es.uca.webservices.bpel.wsdl.WSDLCatalog;
import es.uca.webservices.bptsgen.activities.Activity;
import es.uca.webservices.bptsgen.util.NamespaceContextLab;
import es.uca.webservices.bptsgen.util.PropertyAliasesUtil;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.parsers.sanalyzer.SACatalogUtils;
import es.uca.webservices.wsdl.analyzer.InvalidWSDLContentsException;
import es.uca.webservices.wsdl.analyzer.MissingSchemaComponentException;
import es.uca.webservices.wsdl.analyzer.SchemaReadingException;
import es.uca.webservices.wsdl.analyzer.ServiceAnalyzer;
import es.uca.webservices.wsdl.util.WSDL2XSDTreeException;

/**
 * This class is used to perform an analysis of the BPEL document
 * 
 * @author Jose Luis Ezquerro Casado (jose.ezquerroca@alum.uca.es)
 * 
 */
public class Parser {

	private BPELProcessDefinition bpelProcess;
	private TProcess bpelProcessLab;
	private File bpelFile;
	private Map<String, PropertyAliasesUtil> propertyAliasMap = new HashMap<String, PropertyAliasesUtil>();

	private javax.xml.xpath.XPath xpath = XPathFactory.newInstance().newXPath();

	/**
	 * Constructor. Initializes the global variables
	 * 
	 * @param bpelProcessLab
	 *            the TProcess document
	 * @param bpelFile
	 *            The BPEL File
	 */
	public Parser(TProcess bpelProcessLab,
			File bpelFile) {
		this.bpelProcessLab = bpelProcessLab;
		this.bpelFile = bpelFile;
	}

	/**
	 * Starts the analysis of the BPEL document
	 * 
	 * @return A List<Activity> with the resulting activities to test
	 */
	public List<Activity> parse() throws XPathExpressionException,
			InvalidProcessException, IOException, WSDL2XSDTreeException,
			SchemaReadingException, WSDLException, BPTSGeneratorException,
			InvalidWSDLContentsException, MissingSchemaComponentException,
			ParserException, ParserConfigurationException, SAXException,
			XmlException {
		this.bpelProcess = new BPELProcessDefinition(bpelFile);
		if (bpelProcessLab.getCorrelationSets() != null) {
			checkPropertyAliases();
		}
		List<Activity> activitiesResult = new ArrayList<Activity>();
		parseReceives(activitiesResult);
		return activitiesResult;
	}

	private void parseReceives(List<Activity> activitiesResult)
			throws InvalidProcessException, IOException, WSDL2XSDTreeException,
			SchemaReadingException, WSDLException, BPTSGeneratorException,
			InvalidWSDLContentsException, MissingSchemaComponentException,
			ParserException, ParserConfigurationException, SAXException,
			XPathExpressionException, XmlException {
		NodeList nodes = getNodeListFromActivity("receive");
		if (nodes != null) {
			for (int i = 0; i < nodes.getLength(); i++) {
				completeReceiveActivity(activitiesResult,nodes.item(i));
			}
		}
		parseOnMessages(activitiesResult);
	}

	private void parseOnMessages(List<Activity> activitiesResult)
			throws InvalidProcessException, IOException, WSDL2XSDTreeException,
			SchemaReadingException, WSDLException, BPTSGeneratorException,
			InvalidWSDLContentsException, MissingSchemaComponentException,
			ParserException, ParserConfigurationException, SAXException,
			XPathExpressionException, XmlException {
		NodeList nodes = getNodeListFromActivity("pick");
		if (nodes != null) {
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				if (node.hasChildNodes()
						&& node.getAttributes().getNamedItem("createInstance") != null) {
					if (node.getAttributes().getNamedItem("createInstance")
							.getNodeValue().equals("yes")) {
						NodeList onMessageNodes = node.getChildNodes();
						for (int j = 0; j < onMessageNodes.getLength(); j++) {
							if (onMessageNodes.item(j).getNodeName()
									.equals("onMessage")) {
								completeReceiveActivity(activitiesResult,
										onMessageNodes.item(j));
							}
						}
					}
				}
			}
		}
		parseInvokes(activitiesResult);
	}

	private void completeReceiveActivity(List<Activity> activitiesResult,
			Node node) throws XPathExpressionException,
			InvalidProcessException, IOException, WSDL2XSDTreeException,
			SchemaReadingException, WSDLException, BPTSGeneratorException,
			InvalidWSDLContentsException, MissingSchemaComponentException,
			ParserException, ParserConfigurationException, SAXException,
			XmlException {
		Activity newActivity = new Activity();
		boolean hasActivity = true;
		boolean sameVariable = true;
		if (activitiesResult.isEmpty()) {
			newActivity.setClient(true);
			newActivity.setNode(node);
		} else {
			Iterator<Activity> iter = activitiesResult.iterator();
			while (iter.hasNext()) {
				Activity auxActivity = iter.next();
				if (auxActivity.getPartnerLinkName().equals(
						node.getAttributes().getNamedItem("partnerLink")
								.getNodeValue())) {
					hasActivity = false;
					if(!checkReceiveVariable(auxActivity, node)){
						List<Activity> otherActivities = auxActivity.getOtherActivity();
						Iterator<Activity> otherIter = otherActivities.iterator();
						sameVariable = false;
						while(otherIter.hasNext()){
							if(checkReceiveVariable(otherIter.next(), node)){
								sameVariable = true;
							}
						}
					}
				}
			}
			if (hasActivity) {
				newActivity.setNode(node);
			}
		}
		if (hasActivity && sameVariable) {
			newActivity.setDirection("IN");
			parseNode(newActivity);
			activitiesResult.add(newActivity);
		}
	}

	private void parseInvokes(List<Activity> activitiesResult)
			throws InvalidProcessException, IOException, WSDL2XSDTreeException,
			SchemaReadingException, WSDLException, BPTSGeneratorException,
			InvalidWSDLContentsException, MissingSchemaComponentException,
			ParserException, ParserConfigurationException, SAXException,
			XPathExpressionException, XmlException {
		NodeList nodes = getNodeListFromActivity("invoke");
		if (nodes != null) {
			for (int i = 0; i < nodes.getLength(); i++) {
				Iterator<Activity> iter = activitiesResult.iterator();
				boolean hasInvoke = true;
				while (iter.hasNext()) {
					Activity auxActivity = iter.next();
					if (auxActivity.getNodeName().equals("invoke")) {
						if (auxActivity.getPartnerLinkName().equals(
								nodes.item(i).getAttributes()
										.getNamedItem("partnerLink")
										.getNodeValue())) {
							hasInvoke = false;
							if (!checkInvokeVariables(auxActivity,
									nodes.item(i))) {
								List<Activity> otherActivities = auxActivity
										.getOtherActivity();
								Iterator<Activity> otherIter = otherActivities
										.iterator();
								boolean differentVariables = true;
								while (otherIter.hasNext()) {
									if (checkInvokeVariables(otherIter.next(),
											nodes.item(i))) {
										differentVariables = false;
									}
								}
								if (differentVariables) {
									Activity newActivity = createNewActivity(nodes
											.item(i));
									newActivity.setRepeated(true);
									auxActivity.addOtherActivity(newActivity);
								}
							}

						}
					}
				}
				if (hasInvoke) {
					activitiesResult.add(createNewActivity(nodes.item(i)));
				}
			}
		}
	}

	private Activity createNewActivity(Node node)
			throws XPathExpressionException, InvalidProcessException,
			IOException, WSDL2XSDTreeException, SchemaReadingException,
			WSDLException, BPTSGeneratorException,
			InvalidWSDLContentsException, MissingSchemaComponentException,
			ParserException, ParserConfigurationException, SAXException,
			XmlException {
		Activity newActivity = new Activity(node);
		if (newActivity.getOutputVariable() == null) {
			newActivity.setDirection("IN");
		} else {
			newActivity.setDirection("OUT");
		}
		parseNode(newActivity);
		return newActivity;
	}

	private boolean checkInvokeVariables(Activity orig, Node other) {
		return orig.getInputVariableName().equals(
				other.getAttributes().getNamedItem("inputVariable")
						.getNodeValue())
				&& orig.getOutputVariableName().equals(
						other.getAttributes().getNamedItem("outputVariable")
								.getNodeValue());
	}

	private boolean checkReceiveVariable(Activity orig, Node other) {
		return orig.getVariableName().equals(
				other.getAttributes().getNamedItem("variable").getNodeValue());
	}

	private NodeList getNodeListFromActivity(String activity) {
		Document bpelDocument = bpelProcess.getDocument();
		return bpelDocument.getElementsByTagNameNS("*", activity);
	}

	private void checkPropertyAliases() throws InvalidProcessException,
			IOException, WSDL2XSDTreeException, SchemaReadingException,
			WSDLException {
		WSDLCatalog wsdlCatalog = bpelProcess.getWSDLCatalog();
		Set<BPELPropertyAlias> aliases = wsdlCatalog
				.getVariablePropertyAliases();
		for (BPELPropertyAlias alias : aliases) {
			String partOrQuery = null;
			if (alias.getQueryText() != null) {
				partOrQuery = alias.getQueryText();
			} else if (alias.getPart() != null) {
				partOrQuery = alias.getPart();
			}
			if (propertyAliasMap.containsKey(alias.getPropertyName()
					.getLocalPart())) {
				propertyAliasMap.get(alias.getPropertyName().getLocalPart())
						.addPartOrQuery(partOrQuery);
			} else {
				List<String> newList = new ArrayList<String>();
				newList.add(partOrQuery);
				propertyAliasMap
						.put(alias.getPropertyName().getLocalPart(),
								new PropertyAliasesUtil(newList, alias
										.getNamespaces()));
			}
		}
	}

	private void parseNode(Activity node) throws InvalidProcessException,
			IOException, WSDL2XSDTreeException, SchemaReadingException,
			WSDLException, BPTSGeneratorException,
			InvalidWSDLContentsException, MissingSchemaComponentException,
			ParserException, ParserConfigurationException, SAXException,
			XPathExpressionException, XmlException {
		TPartnerLink pl = getPartnerLink(getPartnerLinks(),
				node.getPartnerLinkName());
		node.setWsdlPath(getWsdlPath(pl));
		String serviceName = getServiceNameFromPartnerLink(pl);
		node.setService(getServiceFromCatalog(node.getWsdlPath(), serviceName));
		node.setCatalogTemplate(getTemplateFromCatalog(node));
	}

	private String getWsdlPath(TPartnerLink pl) {
		for (TImport imp : getImports()) {
			if (pl.getPartnerLinkType().getNamespaceURI()
					.equals(imp.getNamespace())) {
				return imp.getLocation();
			}
		}
		return null;
	}

	private TImport[] getImports() {
		return bpelProcessLab.getImportArray();
	}

	private TPartnerLink[] getPartnerLinks() {
		TPartnerLinks partnerLinksLab = bpelProcessLab.getPartnerLinks();
		return partnerLinksLab.getPartnerLinkArray();
	}

	private TPartnerLink getPartnerLink(TPartnerLink[] partnerLinks, String name) {
		for (TPartnerLink pl : partnerLinks) {
			if (pl.getName().equals(name)) {
				return pl;
			}
		}
		return null;
	}

	private String getServiceNameFromPartnerLink(TPartnerLink pl)
			throws InvalidProcessException, IOException, WSDL2XSDTreeException,
			SchemaReadingException, WSDLException, BPTSGeneratorException {
		final Set<Pair<QName, String>> usedPorts = new HashSet<Pair<QName, String>>();
		if (pl.getMyRole() != null && pl.getMyRole().length() > 0) {
			Pair<Service, Port> servicePort = bpelProcess.findService(
					pl.getPartnerLinkType(), pl.getMyRole(), usedPorts);
			if (servicePort == null) {
				throw new BPTSGeneratorException(
						String.format(
								"Could not find a matching service for role '%s' in partner link '%s'",
								pl.getMyRole(), pl.getName()));
			}
			return servicePort.getLeft().getQName().getLocalPart();
		}
		if (pl.getPartnerRole() != null && pl.getPartnerRole().length() > 0) {
			Pair<Service, Port> servicePort = bpelProcess.findService(
					pl.getPartnerLinkType(), pl.getPartnerRole(), usedPorts);
			if (servicePort == null) {
				throw new BPTSGeneratorException(
						String.format(
								"Could not find a matching service for role '%s' in partner link '%s'",
								pl.getPartnerRole(), pl.getName()));
			}
			return servicePort.getLeft().getQName().getLocalPart();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private Service getServiceFromCatalog(String wsdlPath, String serviceName)
			throws WSDLException, IOException {
		WSDLReader wreader = WSDLFactory.newInstance().newWSDLReader();
		wreader.setFeature(Constants.FEATURE_VERBOSE, false);
		Definition def = wreader.readWSDL(null, getWSDLPathFile(wsdlPath));
		for (Service service : (Collection<Service>) def.getAllServices()
				.values()) {
			if (service.getQName().getLocalPart().equals(serviceName)) {
				return service;
			}
		}
		return null;
	}

	private String getWSDLPathFile(String wsdlPath) throws IOException {
		File wsdlFile = new File(bpelFile.getParentFile(), wsdlPath);
		return wsdlFile.getCanonicalPath();
	}

	private TypeTemplate getTemplateFromCatalog(Activity node)
			throws WSDLException, SchemaReadingException,
			WSDL2XSDTreeException, IOException, InvalidWSDLContentsException,
			MissingSchemaComponentException, ParserException,
			ParserConfigurationException, SAXException,
			XPathExpressionException, XmlException {
		ServiceAnalyzer sa = new ServiceAnalyzer(
				getWSDLPathFile(node.getWsdlPath()));
		ServicesDocument catalog = sa.generateMessageCatalog();
		node.setCatalog(catalog);
		TypeService service = SACatalogUtils.findServiceByName(
				catalog.getServices(), node.getService().getQName()
						.getLocalPart());
		TypeOperation operation = SACatalogUtils.findOperationByName(service,
				node.getOperation());
		if (node.getDirection().equals("IN")) {
			if (propertyAliasMap.isEmpty()) {
				return operation.getInput().getTemplate();
			} else {
				return checkTemplate(node, operation.getInput().getTemplate());
			}
		} else {
			if (propertyAliasMap.isEmpty()) {
				return operation.getOutput().getTemplate();
			} else {
				return checkTemplate(node, operation.getOutput().getTemplate());
			}
		}
	}

	private Document generateDocumentFromTemplate(TypeTemplate template)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory documentFactory = DocumentBuilderFactory
				.newInstance();
		documentFactory.setNamespaceAware(true);
		DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
		return documentBuilder.parse(new InputSource(new StringReader(template
				.getStringValue())));
	}

	private TypeTemplate checkTemplate(Activity node, TypeTemplate templateLab)
			throws ParserConfigurationException, SAXException, IOException,
			XPathExpressionException, XmlException {
		List<String> correlationSetNames = node.getCorrelationSetNames();
		if (correlationSetNames.isEmpty()) {
			return templateLab;
		} else {
			Iterator<String> iter = correlationSetNames.iterator();
			while (iter.hasNext()) {
				List<QName> correlationSetProperties = getCorrelationSetProperties(iter
						.next());
				Iterator<QName> correIter = correlationSetProperties.iterator();
				while (correIter.hasNext()) {
					QName element = correIter.next();
					if (propertyAliasMap.containsKey(element.getLocalPart())) {
						PropertyAliasesUtil propertyUtil = propertyAliasMap
								.get(element.getLocalPart());
						List<String> partOrQueryList = propertyUtil
								.getPartOrQueryList();
						Iterator<String> partOrQueryIter = partOrQueryList
								.iterator();
						while (partOrQueryIter.hasNext()) {
							NodeList nodeList = evaluate(
									partOrQueryIter.next(),
									generateDocumentFromTemplate(templateLab),
									propertyUtil.getNamespaces());
							if (nodeList.getLength() != 0) {
								if (propertyUtil.getRestrictionValue() == null) {
									propertyUtil.setRestrictionValue(nodeList
											.item(0).getTextContent());
									return templateLab;
								} else {
									return alterTemplate(templateLab, nodeList
											.item(0).getNodeName(),
											propertyUtil.getRestrictionValue());
								}
							}
						}
					}
				}
			}
		}
		return null;
	}

	private TypeTemplate alterTemplate(TypeTemplate olderTemplate, String part,
			String restrictionValue) throws XmlException {
		String regex = "<" + part + ">.*</" + part + ">";
		String replacement = "<" + part + ">\\" + restrictionValue + "</"
				+ part + ">";
		TypeTemplate resultTemplate = TypeTemplate.Factory.parse(olderTemplate
				.toString().replaceAll(regex, replacement));
		return resultTemplate;
	}

	private NodeList evaluate(String xpathExpression, Document document,
			Map<String, String> namespaces) throws XPathExpressionException {
		NamespaceContextLab context = new NamespaceContextLab();
		context.addNamespaces(namespaces);
		context.addNamespaces(getAllNamespaces(document.getDocumentElement()));
		xpath.setNamespaceContext(context);
		NodeList nodeList = (NodeList) xpath.evaluate(xpathExpression,
				document.getDocumentElement(), XPathConstants.NODESET);
		return nodeList;
	}

	@SuppressWarnings("unchecked")
	private List<QName> getCorrelationSetProperties(String correlationSetName) {
		for (TCorrelationSet set : bpelProcessLab.getCorrelationSets()
				.getCorrelationSetArray()) {
			if (set.getName().equals(correlationSetName)) {
				return set.getProperties();
			}
		}
		return null;
	}

	private Map<String, String> getAllNamespaces(Node node) {
		HashMap<String, String> result = new HashMap<String, String>();
		if (node == null) {
			return result;
		}
		if (node.getParentNode() != null) {
			Map<String, String> resultParent = getAllNamespaces(node
					.getParentNode());
			for (Entry<String, String> entry : resultParent.entrySet()) {
				if (!result.containsKey(entry.getKey())) {
					result.put(entry.getKey(), entry.getValue());
				}
			}
		}
		NamedNodeMap attributes = node.getAttributes();
		if (attributes != null) {
			for (int i = 0; i < attributes.getLength(); i++) {
				if (attributes.item(i).getNodeName().contains("xmlns:")) {
					String nodeName = attributes.item(i).getNodeName();
					String prefix = nodeName.split(":")[1];
					if (!result.containsKey(prefix)) {
						result.put(prefix, attributes.item(i).getTextContent());
					}
				}
			}
		}
		return result;
	}

}
