/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.bptsgen;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.wsdl.Definition;
import javax.wsdl.Port;
import javax.wsdl.Service;
import javax.wsdl.WSDLException;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;

import net.bpelunit.framework.xml.suite.XMLAnyElement;
import net.bpelunit.framework.xml.suite.XMLDataSource;
import net.bpelunit.framework.xml.suite.XMLDeploymentSection;
import net.bpelunit.framework.xml.suite.XMLPUTDeploymentInformation;
import net.bpelunit.framework.xml.suite.XMLPartnerDeploymentInformation;
import net.bpelunit.framework.xml.suite.XMLPartnerTrack;
import net.bpelunit.framework.xml.suite.XMLProperty;
import net.bpelunit.framework.xml.suite.XMLReceiveActivity;
import net.bpelunit.framework.xml.suite.XMLSendActivity;
import net.bpelunit.framework.xml.suite.XMLSetUp;
import net.bpelunit.framework.xml.suite.XMLSoapActivity;
import net.bpelunit.framework.xml.suite.XMLTestCase;
import net.bpelunit.framework.xml.suite.XMLTestCasesSection;
import net.bpelunit.framework.xml.suite.XMLTestSuite;
import net.bpelunit.framework.xml.suite.XMLTestSuiteDocument;
import net.bpelunit.framework.xml.suite.XMLTrack;
import net.bpelunit.framework.xml.suite.XMLTwoWayActivity;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.SaveOptions;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.oasisOpen.docs.wsbpel.x20.process.executable.ProcessDocument;
import org.oasisOpen.docs.wsbpel.x20.process.executable.TProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import serviceAnalyzer.messageCatalog.TypeTemplate;

import com.google.inject.Injector;
import com.ibm.wsdl.Constants;

import es.uca.webservices.bptsgen.activities.Activity;
import es.uca.webservices.specgen.SpecGenerator;
import es.uca.webservices.testgen.TestGeneratorRun;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.parsers.MessageDirection;
import es.uca.webservices.testgen.parsers.spec.xtext.TestSpecStandaloneSetupGenerated;
import es.uca.webservices.testgen.parsers.spec.xtext.integration.XtextSpecParser;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Entry;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Model;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Restriction;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.RestrictionValue;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Restrictions;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.TestSpecFactory;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Typedef;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.Variable;
import es.uca.webservices.testgen.parsers.spec.xtext.testSpec.VariableList;

/**
 * This class is used to handle BPEL documents, generate the .spec and .vm files
 * and the bpts template
 * 
 * @author Jose Luis Ezquerro Casado (jose.ezquerroca@alum.uca.es)
 */
public class BPTSGenerator {
	private static final Logger LOGGER = LoggerFactory.getLogger(BPTSGenerator.class);

	private File bpelFile;
	private XMLTestSuiteDocument testSuiteDoc;

	// Seed for TestGenerator (if null, use /dev/random as a seed)
	private String seed;

	/**
	 * Constructor.
	 * 
	 * @param bpelPath
	 *            The path to the BPEL file
	 */
	public BPTSGenerator(String bpelPath) {
		bpelFile = new File(bpelPath);
	}

	/**
	 * <p>
	 * Returns the random seed to be used for generating the .vm file. If
	 * <code>null</code>, the seed will be set by the PRNG in TestGenerator.
	 * </p>
	 * <p>
	 * Currently, TestGenerator uses the Uncommon Maths PRNGs, which are seeded
	 * from /dev/random by default. This ensures a very high quality output, but
	 * it takes a while to initialize.
	 * </p>
	 */
	public String getSeed() {
		return seed;
	}

	/**
	 * Changes the random seed to be used for generating the .vm file.
	 *
	 * @see #getSeed()
	 */
	public void setSeed(String seed) {
		this.seed = seed;
	}

	/**
	 * Dumps the bpts template to the standard output stream.
	 * 
	 * @throws IOException
	 */
	public void printTestTemplate() throws IOException {
		if (this.testSuiteDoc != null) {
			XmlOptions options = new XmlOptions();
			options.setSavePrettyPrint();
			options.setSavePrettyPrintIndent(2);
			options.setSaveCDataEntityCountThreshold(0);
			options.setSaveCDataLengthThreshold(0);
			testSuiteDoc.save(System.out, options);
		}
	}

	/**
	 * Generates the bpts template
	 * 
	 * @return The resulting XMLTestSuiteDocument
	 * @throws Exception
	 */
	public XMLTestSuiteDocument generateTestTemplate() throws Exception {
		testSuiteDoc = XMLTestSuiteDocument.Factory.newInstance();
		XMLTestSuite testSuiteLab = testSuiteDoc.addNewTestSuite();
		ProcessDocument processDoc = ProcessDocument.Factory.parse(bpelFile);
		TProcess processLab = processDoc.getProcess();

		Parser parser = new Parser(processLab, bpelFile);
		List<Activity> activities = parser.parse();
		Model model = generateSpec(activities);
		generateVM();

		testSuiteLab.setName(processLab.getName() + "Test");
		testSuiteLab.setBaseURL(BPTSConstants.BASE_URL);
		testSuiteLab.newCursor().prefixForNamespace(
				processLab.getTargetNamespace());

		generateDeployment(processLab, testSuiteLab, activities);
		generateSetUp(testSuiteLab, model);
		generateTestCases(testSuiteLab, activities);

		return testSuiteDoc;
	}

	private void generateVM() throws ParserException, IOException,
			GenerationException {
		TestGeneratorRun tgr = new TestGeneratorRun(
				BPTSConstants.SPEC_FILE_NAME, "velocity",
				BPTSConstants.NUM_TESTS, BPTSConstants.TESTGENERATOR_STRATEGY);

		if (seed != null) {
			tgr.setSeed(seed);
		}
		tgr.run(new FileOutputStream(BPTSConstants.VM_FILE_NAME));
	}

	private Model generateSpec(List<Activity> activities) throws IOException,
			ParserException, XmlException {
		final Injector injector = new TestSpecStandaloneSetupGenerated()
				.createInjectorAndDoEMFRegistration();
		final XtextResourceSet resourceSet = injector
				.getInstance(XtextResourceSet.class);
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL,
				Boolean.TRUE);
		final Resource resource = resourceSet.createResource(URI
				.createURI("example.spec"));
		TestSpecFactory specFactory = TestSpecFactory.eINSTANCE;
		Model model = specFactory.createModel();
		resource.getContents().add(model);
		generateSpec(activities, model);
		resource.save(new FileOutputStream(BPTSConstants.SPEC_FILE_NAME),
				SaveOptions.newBuilder().noValidation().getOptions()
						.toOptionsMap());
		return model;
	}

	private void generateSpec(List<Activity> activities, Model model)
			throws IOException, ParserException, XmlException {
		Iterator<Activity> iter = activities.iterator();
		while (iter.hasNext()) {
			BufferedWriter output = new BufferedWriter(new FileWriter(
					"tempdata.spec"));
			Activity activity = iter.next();
			output.write(generateSpec(activity));
			output.close();
			analyzeResource(model, activity);
			boolean deleted = new File("tempdata.spec").delete();
			if (!deleted) {
				LOGGER.debug("An error occurs while deleting tempdata.spec");
			}
			if (activity.getOtherActivity().size() != 0) {
				generateSpec(activity.getOtherActivity(), model);
			}
		}
	}

	private String generateSpec(Activity activity) throws ParserException {
		SpecGenerator specLab = new SpecGenerator();
		specLab.setServiceName(activity.getService().getQName().getLocalPart());
		specLab.setOperationName(activity.getOperation());
		specLab.setDirection(MessageDirection.fromString(activity
				.getDirection()));
		specLab.setFaultName(null);
		return specLab.generate(activity.getCatalog());
	}

	private void generateDeployment(TProcess processLab,
			XMLTestSuite testSuiteLab, List<Activity> activities) {
		XMLDeploymentSection depSectionLab = testSuiteLab.addNewDeployment();
		XMLPUTDeploymentInformation putDepInfoLab = depSectionLab.addNewPut();
		putDepInfoLab.setName(processLab.getName());
		putDepInfoLab.setType(BPTSConstants.ENGINE_TYPE);
		Iterator<Activity> iter = activities.iterator();
		while (iter.hasNext()) {
			Activity auxiliar = iter.next();
			if (auxiliar.isClient()) {
				putDepInfoLab.setWsdl(auxiliar.getWsdlPath());
				XMLProperty propertyLab = putDepInfoLab.addNewProperty();
				propertyLab.setName("BPRFile");
				propertyLab.setStringValue(processLab.getName() + ".bpr");
			} else {
				XMLPartnerDeploymentInformation partnerLab = depSectionLab
						.addNewPartner();
				partnerLab.setName(auxiliar.getPartnerLinkName());
				partnerLab.setWsdl(auxiliar.getWsdlPath());
			}
		}
	}

	private void generateTestCases(XMLTestSuite testSuiteLab,
			List<Activity> activities) throws WSDLException, IOException {
		XMLTestCasesSection testCasesLab = testSuiteLab.addNewTestCases();
		XMLTestCase testCaseLab = testCasesLab.addNewTestCase();
		testCaseLab.setName(BPTSConstants.TEST_NAME);
		testCaseLab.setBasedOn("");
		testCaseLab.setAbstract(BPTSConstants.TEST_ABSTRACT);
		testCaseLab.setVary(BPTSConstants.TEST_VARY);
		Iterator<Activity> iter = activities.iterator();
		while (iter.hasNext()) {
			Activity auxiliar = iter.next();
			addWSDLNamespace(auxiliar, testSuiteLab);
			if (auxiliar.isClient()) {
				XMLTrack clientTrackLab = testCaseLab.addNewClientTrack();
				generateTestCaseClientTrack(auxiliar, clientTrackLab);
			} else {
				XMLPartnerTrack partnerTrackLab = testCaseLab
						.addNewPartnerTrack();
				partnerTrackLab.setName(auxiliar.getPartnerLinkName());
				generateTestCasePartnerTrack(auxiliar, partnerTrackLab);
			}
		}
	}

	private void generateTestCaseClientTrack(Activity activity,
			XMLTrack clientTrackLab) throws WSDLException, IOException {
		XMLTwoWayActivity twoWayActivity = clientTrackLab.addNewSendReceive();
		fillActivity(twoWayActivity, activity);
		completeTwoWayActivity(twoWayActivity, activity.getCatalogTemplate());
		if (activity.getOtherActivity().size() != 0) {
			List<Activity> otherActivities = activity.getOtherActivity();
			Iterator<Activity> otherActIterator = otherActivities.iterator();
			while (otherActIterator.hasNext()) {
				generateTestCaseClientTrack(activity, clientTrackLab);
			}
		}
	}

	private void generateTestCasePartnerTrack(Activity activity,
			XMLPartnerTrack partnerTrackLab) throws WSDLException, IOException {
		XMLTwoWayActivity twoWayActivity = XMLTwoWayActivity.Factory
				.newInstance();
		boolean receiveOnly = false;
		if (activity.getDirection().equals("IN")) {
			if (activity.getNodeName().equals("invoke")) {
				XMLReceiveActivity receiveActivity = XMLReceiveActivity.Factory
						.newInstance();
				receiveActivity = partnerTrackLab.addNewReceiveOnly();
				receiveOnly = true;
				fillActivity(receiveActivity, activity);
			} else {
				twoWayActivity = partnerTrackLab.addNewSendReceive();
			}
		} else {
			twoWayActivity = partnerTrackLab.addNewReceiveSend();
		}
		if (!receiveOnly) {
			fillActivity(twoWayActivity, activity);
			completeTwoWayActivity(twoWayActivity,
					activity.getCatalogTemplate());
		}
		if (activity.getOtherActivity().size() != 0) {
			List<Activity> otherActivities = activity.getOtherActivity();
			Iterator<Activity> otherActIterator = otherActivities.iterator();
			while (otherActIterator.hasNext()) {
				generateTestCasePartnerTrack(otherActIterator.next(),
						partnerTrackLab);
			}
		}

	}

	private void addWSDLNamespace(Activity activity, XMLTestSuite testSuiteLab)
			throws WSDLException, IOException {
		WSDLReader wreader = WSDLFactory.newInstance().newWSDLReader();
		wreader.setFeature(Constants.FEATURE_VERBOSE, false);
		Definition def = wreader.readWSDL(null,
				getWSDLPathFile(activity.getWsdlPath()));
		String targetNamespace = def.getTargetNamespace();
		testSuiteLab.newCursor().prefixForNamespace(targetNamespace);
	}

	private void fillActivity(XMLSoapActivity activityLab, Activity activity)
			throws WSDLException, IOException {
		activityLab.setService(activity.getService().getQName());
		activityLab.setPort(readWSDLPort(activity.getWsdlPath(), activity
				.getService().getQName().getLocalPart()));
		activityLab.setOperation(activity.getOperation());
	}

	private String getWSDLPathFile(String wsdlPath) throws IOException {
		File wsdlFile = new File(bpelFile.getParentFile(), wsdlPath);
		return wsdlFile.getCanonicalPath();
	}

	@SuppressWarnings("unchecked")
	private String readWSDLPort(String wsdlPath, String servName)
			throws WSDLException, IOException {
		WSDLReader wreader = WSDLFactory.newInstance().newWSDLReader();
		wreader.setFeature(Constants.FEATURE_VERBOSE, false);
		Definition def = wreader.readWSDL(null, getWSDLPathFile(wsdlPath));
		for (Service service : (Collection<Service>) def.getAllServices()
				.values()) {
			if (service.getQName().getLocalPart().equals(servName)) {
				for (Port port : (Collection<Port>) service.getPorts().values()) {
					return port.getName();
				}
			}
		}
		return null;
	}

	private void analyzeResource(Model model, Activity activity)
			throws ParserException, XmlException {
		XtextSpecParser xp = new XtextSpecParser("tempdata.spec");
		Resource resource = xp.getResource();
		final Model modelLab = (Model) resource.getContents().get(0);
		int modelLabSize = modelLab.getEntries().size();
		final TestSpecFactory specFactory = TestSpecFactory.eINSTANCE;
		for (int i = 0; i < modelLabSize; i++) {
			Map<String, Typedef> repeatedTypes = new HashMap<String, Typedef>();
			Entry entry = modelLab.getEntries().get(i);
			if (entry instanceof Typedef) {
				final Typedef typedef = (Typedef) entry;
				boolean hasTypedef = false;
				int size = model.getEntries().size();
				for (int j = 0; j < size; j++) {
					Entry entryLab = model.getEntries().get(j);
					if (entryLab instanceof Typedef) {
						hasTypedef = true;
						final Typedef typedefLab = (Typedef) entryLab;
						if (typedef.getName().equals(typedefLab.getName())) {
							String oldName = typedef.getName();
							typedef.setName(oldName + size);
							repeatedTypes.put(oldName, typedef);
						}
					}
				}
				final Typedef auxTypedef = specFactory.createTypedef();
				if (hasTypedef) {
					Restrictions restrictions = typedef.getRestrictions();
					for (Restriction restriction : restrictions
							.getRestriction()) {
						EList<RestrictionValue> elements = restriction
								.getValue().getList();
						int sizeList = elements.size();
						for (int j = 0; j < sizeList; j++) {
							RestrictionValue elem = elements.get(j);
							if (elem.getTypedef() != null) {
								if (repeatedTypes.containsKey(elem.getTypedef()
										.getName())) {
									elem.setTypedef(repeatedTypes.get(elem
											.getTypedef().getName()));
								} else {
									for (int k = 0; k < size; k++) {
										Entry entryLab = model.getEntries()
												.get(k);
										if (entryLab instanceof Typedef) {
											final Typedef typedefLab = (Typedef) entryLab;
											if (elem.getTypedef()
													.getName()
													.equals(typedefLab
															.getName())) {
												elem.setTypedef(typedefLab);
											}
										}
									}
								}
							}
						}
						if (sizeList == 0
								&& restriction.getValue().getTypedef() != null) {
							if (repeatedTypes.containsKey(restriction
									.getValue().getTypedef().getName())) {
								restriction.getValue().setTypedef(
										repeatedTypes.get(restriction
												.getValue().getTypedef()
												.getName()));
							} else {
								for (int j = 0; j < size; j++) {
									Entry entryLab = model.getEntries().get(j);
									if (entryLab instanceof Typedef) {
										final Typedef typedefLab = (Typedef) entryLab;
										if (restriction.getValue().getTypedef()
												.getName()
												.equals(typedefLab.getName())) {
											restriction.getValue().setTypedef(
													typedefLab);
										}
									}
								}
							}
						}
					}
				}
				auxTypedef.setName(typedef.getName());
				auxTypedef.setRestrictions(typedef.getRestrictions());
				auxTypedef.setPrimitive(typedef.getPrimitive());
				model.getEntries().add(auxTypedef);
			} else if (entry instanceof VariableList) {
				final VariableList auxVaList = specFactory.createVariableList();
				final VariableList variableList = (VariableList) entry;
				if (variableList.getTypedef() != null) {
					if (repeatedTypes.containsKey(variableList.getTypedef()
							.getName())) {
						auxVaList.setTypedef(repeatedTypes.get(variableList
								.getTypedef().getName()));
					} else {
						int size = model.getEntries().size();
						for (int j = 0; j < size; j++) {
							Entry entryLab = model.getEntries().get(j);
							if (entryLab instanceof Typedef) {
								final Typedef typedefLab = (Typedef) entryLab;
								if (variableList.getTypedef().getName()
										.equals(typedefLab.getName())) {
									auxVaList.setTypedef(typedefLab);
								}
							}
						}
					}
				}
				for (Variable variable : variableList.getVars()) {
					final Variable v = specFactory.createVariable();
					if (activity.isRepeated()) {
						String oldValue = variable.getName();
						String newValue = variable.getName()
								+ model.getEntries().size();
						v.setName(newValue);
						updateTemplateVariableValue(activity, oldValue,
								newValue);
					} else {
						v.setName(variable.getName());
					}
					auxVaList.getVars().add(v);
				}
				model.getEntries().add(auxVaList);
			}
		}
	}

	private void updateTemplateVariableValue(Activity activity,
			String oldValue, String newValue) throws XmlException {
		TypeTemplate oldTemplate = activity.getCatalogTemplate();
		TypeTemplate resultTemplate = TypeTemplate.Factory.parse(oldTemplate
				.toString().replaceAll("\\$" + oldValue, "\\$" + newValue));
		activity.setCatalogTemplate(resultTemplate);
	}

	private void generateSetUp(XMLTestSuite testSuiteLab, Model model) {
		XMLSetUp setUpLab = testSuiteLab.addNewSetUp();
		XMLDataSource dataSourceLab = setUpLab.addNewDataSource();
		dataSourceLab.setType(BPTSConstants.TEMPLATE_TYPE);
		dataSourceLab.setSrc(BPTSConstants.VM_FILE_NAME);
		XMLProperty propertyLab = dataSourceLab.addNewProperty();
		propertyLab.setName("iteratedVars");
		StringBuffer variables = new StringBuffer();
		for (int i = 0; i < model.getEntries().size(); i++) {
			Entry entry = model.getEntries().get(i);
			if (entry instanceof VariableList) {
				final VariableList vaList = (VariableList) entry;
				for (Variable v : vaList.getVars()) {
					variables.append(v.getName().toString() + " ");
				}
			}
		}
		propertyLab.setStringValue(variables.toString());
	}

	private void completeTwoWayActivity(XMLTwoWayActivity twoWayActivity,
			XmlObject template) {
		XMLSendActivity send = twoWayActivity.addNewSend();
		send.setFault(false);
		XMLAnyElement element = send.addNewTemplate();
		element.set(template);
		twoWayActivity.addNewReceive().setFault(false);
	}

}
