package es.uca.webservices.bptsgen.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility methods for handling the propertyAlias part of a .wsdl
 * 
 * @author Jose Luis Ezquerro Casado (jose.ezquerroca@alum.uca.es)
 * 
 */
public class PropertyAliasesUtil {
	private List<String> partOrQueryList = new ArrayList<String>();
	private String restrictionValue;
	private Map<String, String> namespaces = new HashMap<String, String>();

	/**
	 * Constructor. Initializes the variables
	 * 
	 * @param partOrQueryList
	 *            A list with the part or query part of the propertyAlias
	 * @param namespaces
	 *            All the namespaces used in the .wsdl
	 */
	public PropertyAliasesUtil(List<String> partOrQueryList,
			Map<String, String> namespaces) {
		this.partOrQueryList.addAll(partOrQueryList);
		this.namespaces.putAll(namespaces);
		this.restrictionValue = null;
	}

	/**
	 * Default constructor
	 */
	public PropertyAliasesUtil() {
		this.restrictionValue = null;
	}

	/**
	 * Adds a new part or query part
	 * 
	 * @param partOrQuery
	 */
	public void addPartOrQuery(String partOrQuery) {
		this.partOrQueryList.add(partOrQuery);
	}

	/**
	 * Adds more namespaces
	 */
	public void addNamespaces(Map<String, String> newNamespaces) {
		this.namespaces.putAll(newNamespaces);
	}

	/**
	 * Sets the common restriction value to all propertyAlias
	 */
	public void setRestrictionValue(String restrictionValue) {
		this.restrictionValue = restrictionValue;
	}

	/**
	 * Gets the restrictionValue attribute
	 */
	public String getRestrictionValue() {
		return restrictionValue;
	}

	/**
	 * Gets the list with all the parts or queries
	 */
	public List<String> getPartOrQueryList() {
		return partOrQueryList;
	}

	/**
	 * Gets all the namespaces
	 */
	public Map<String, String> getNamespaces() {
		return namespaces;
	}

}
