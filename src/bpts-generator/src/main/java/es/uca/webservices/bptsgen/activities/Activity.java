package es.uca.webservices.bptsgen.activities;

import java.util.ArrayList;
import java.util.List;

import javax.wsdl.Service;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import serviceAnalyzer.messageCatalog.ServicesDocument;
import serviceAnalyzer.messageCatalog.TypeTemplate;

/**
 * This class encapsulates the BPEL activities: invoke, receive and onMessage
 * 
 * @author Jose Luis Ezquerro Casado (jose.ezquerroca@alum.uca.es)
 */
public class Activity {
	private Node node;
	private boolean isClient;
	private Service service;
	private String wsdlPath;
	private String direction;
	private ServicesDocument catalog;
	private TypeTemplate catalogTemplate;
	private List<Activity> other = new ArrayList<Activity>();
	private boolean isRepeated;

	/**
	 * Constructor.
	 * 
	 * @param node
	 *            The activity Node
	 */
	public Activity(Node node) {
		this.node = node;
		this.isClient = false;
		this.isRepeated = false;
	}

	/**
	 * Constructor.
	 * 
	 * @param node
	 *            The activity Node
	 * @param isClient
	 *            Should be true if a receive or onMessage activity is client
	 */
	public Activity(Node node, boolean isClient) {
		this.node = node;
		this.isClient = isClient;
		this.isRepeated = false;
	}

	/**
	 * Default constructor
	 */
	public Activity() {
		this.isClient = false;
		this.isRepeated = false;
	}

	/**
	 * Sets the isClient attribute
	 */
	public void setClient(boolean isClient) {
		this.isClient = isClient;
	}

	/**
	 * Sets the isRepeated attribute
	 */
	public void setRepeated(boolean isRepeated) {
		this.isRepeated = isRepeated;
	}

	/**
	 * True if this activity is repeated
	 */
	public boolean isRepeated() {
		return isRepeated;
	}

	/**
	 * Sets the node attribute
	 */
	public void setNode(Node node) {
		this.node = node;
	}

	/**
	 * Sets the service attribute
	 */
	public void setService(Service service) {
		this.service = service;
	}

	/**
	 * Gets the service attribute
	 */
	public Service getService() {
		return service;
	}

	/**
	 * Sets the wsdlPath attribute
	 */
	public void setWsdlPath(String wsdlPath) {
		this.wsdlPath = wsdlPath;
	}

	/**
	 * Gets the wsdlPath attribute
	 * 
	 * @return
	 */
	public String getWsdlPath() {
		return wsdlPath;
	}

	/**
	 * Sets the direction attribute
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}

	/**
	 * Gets the direction attribute
	 * 
	 * @return
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * Sets the WSDL catalog ({@link ServicesDocument}) attribute
	 */
	public void setCatalog(ServicesDocument catalog) {
		this.catalog = catalog;
	}

	/**
	 * Gets the WSDL catalog ({@link ServicesDocument}) attribute
	 * 
	 * @return
	 */
	public ServicesDocument getCatalog() {
		return catalog;
	}

	/**
	 * Sets the catalogTemplate ({@link TypeTemplate}) attribute
	 */
	public void setCatalogTemplate(TypeTemplate catalogTemplate) {
		this.catalogTemplate = catalogTemplate;
	}

	/**
	 * Gets the catalogTemplate ({@link TypeTemplate}) attribute
	 * 
	 * @return
	 */
	public TypeTemplate getCatalogTemplate() {
		return catalogTemplate;
	}

	/**
	 * Adds other activity with the same partner link
	 */
	public void addOtherActivity(Activity other) {
		this.other.add(other);
	}

	/**
	 * Gets a list of other activities with the same partner link
	 * 
	 * @return
	 */
	public List<Activity> getOtherActivity() {
		return other;
	}

	/**
	 * True if this activity is client
	 */
	public boolean isClient() {
		return isClient;
	}

	/**
	 * Gets the node attribute
	 */
	public Node getNode() {
		return node;
	}

	/**
	 * Gets the node name (receive, invoke or onMessage)
	 */
	public String getNodeName() {
		return node.getNodeName();
	}
	
	/**
	 * Gets the "variable" name
	 */
	public String getVariableName(){
		return node.getAttributes().getNamedItem("variable").getNodeValue();
	}

	/**
	 * Gets the "outputVariable" node
	 */
	public Node getOutputVariable() {
		return node.getAttributes().getNamedItem("outputVariable");
	}

	/**
	 * Gets the "inputVariable" node
	 */
	public Node getInputVariable() {
		return node.getAttributes().getNamedItem("intputVariable");
	}

	/**
	 * Gets the "outputVariable" name
	 */
	public String getOutputVariableName() {
		return node.getAttributes().getNamedItem("outputVariable")
				.getNodeValue();
	}

	/**
	 * Gets the "inputVariable" name
	 */
	public String getInputVariableName() {
		return node.getAttributes().getNamedItem("inputVariable")
				.getNodeValue();
	}

	/**
	 * Gets the operation name
	 */
	public String getOperation() {
		return node.getAttributes().getNamedItem("operation").getNodeValue();
	}

	/**
	 * Gets the partner link name
	 */
	public String getPartnerLinkName() {
		return node.getAttributes().getNamedItem("partnerLink").getNodeValue();
	}

	/**
	 * Gets a list with all the correlation set names
	 */
	public List<String> getCorrelationSetNames() {
		if (node.hasChildNodes()) {
			NodeList nodeList = node.getChildNodes();
			List<String> result = new ArrayList<String>();
			for (int j = 0; j < nodeList.getLength(); j++) {
				Node correlationsNode = nodeList.item(j);
				if (correlationsNode.getNodeName().equals("correlations")
						&& correlationsNode.hasChildNodes()) {
					NodeList correlationNodes = correlationsNode
							.getChildNodes();
					for (int k = 0; k < correlationNodes.getLength(); k++) {
						Node correlationNode = correlationNodes.item(k);
						if (correlationNode.getNodeName().equals("correlation")
								&& correlationNode.hasAttributes()) {
							String correlationNodeName = correlationNode
									.getAttributes().getNamedItem("set")
									.getNodeValue();
							if (correlationNodeName != null) {
								result.add(correlationNodeName);
							}
						}
					}
				}
			}
			return result;
		} else {
			return null;
		}
	}

}
