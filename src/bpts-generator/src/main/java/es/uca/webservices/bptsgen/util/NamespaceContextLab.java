package es.uca.webservices.bptsgen.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;

/**
 * Namespace context, used in the evaluation of expression related to WSDL and
 * templates generated whit Service Analyzer catalog
 * 
 * @author Jose Luis Ezquerro Casado (jose.ezquerroca@alum.uca.es)
 */
public class NamespaceContextLab implements NamespaceContext {

	private static final Map<String, String> MAP_PREFIX_NS;
	private static final Map<String, String> MAP_NS_PREFIX;

	static {
		MAP_NS_PREFIX = new HashMap<String, String>();
		MAP_PREFIX_NS = new HashMap<String, String>();
	}

	/**
	 * Adds new namespaces
	 * 
	 * @param namespaces
	 *            A Map whit new namespaces
	 */
	public void addNamespaces(Map<String, String> namespaces) {
		for (Map.Entry<String, String> entry : namespaces.entrySet()) {
			MAP_PREFIX_NS.put(entry.getKey(), entry.getValue());
			MAP_NS_PREFIX.put(entry.getValue(), entry.getKey());
		}
	}

	/**
	 * Gets the URI associated with a given prefix
	 * 
	 * @param prefix
	 *            The prefix
	 * @return A String with the URI
	 */
	public String getNamespaceURI(String prefix) {
		return MAP_PREFIX_NS.get(prefix);
	}

	/**
	 * Gets the prefix associated with a given URI
	 * 
	 * @param nameURI
	 *            The URI of the namespace
	 * @return The associated prefix
	 */
	public String getPrefix(String nameURI) {
		return MAP_NS_PREFIX.get(nameURI);
	}

	/**
	 * Gets an iterator over the prefixes associated with a given namespace
	 * 
	 * @param nameURI
	 *            The namespace URI
	 * @return An iterator
	 */
	public Iterator<String> getPrefixes(String nameURI) {
		return MAP_PREFIX_NS.keySet().iterator();
	}

}
