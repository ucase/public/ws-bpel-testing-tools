package es.uca.webservices.bptsgen;

/**
 * Class with constants used throughout the generation of the bpts template
 * 
 * @author Jose Luis Ezquerro Casado (jose.ezquerroca@alum.uca.es)
 */
public final class BPTSConstants {
	private BPTSConstants() {
	}

	public static final String TEST_NAME = "MainTemplate";
	public static final boolean TEST_ABSTRACT = false;
	public static final boolean TEST_VARY = false;
	public static final String TEMPLATE_TYPE = "velocity";
	public static final String ENGINE_TYPE = "activebpel";
	public static final String SPEC_FILE_NAME = "data.spec";
	public static final String VM_FILE_NAME = "data.vm";
	public static final int NUM_TESTS = 5;
	public static final String BASE_URL = "http://localhost:7777/ws";
	public static final String BPTS_FILE = "-Velocity.bpts";
	public static final String TESTGENERATOR_STRATEGY = "";
}
