/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.bptsgen.cli;

import java.util.List;

import es.uca.webservices.bptsgen.BPTSGenerator;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * Implements the 'bpts-generator' command.
 * 
 * @author Jose Luis Ezquerro Casado (jose.ezquerroca@alum.uca.es)
 * 
 */
public class BPTSGenCommand {
	private static final String HELP_OPTION = "help";
	private static final String NAME = "bpts-generator";
	private static final String USAGE = "bpts-generator (path to .bpel file...)";
	private static final String DESCRIPTION = 
			"Generates BPELUnit parametric test sets for WS-BPEL compositions.";

	private boolean fRequestedHelp = false;

	private List<String> fNonOptionArgs;

	protected OptionParser createOptionParser() {
		OptionParser parser = new OptionParser();
		parser.accepts(HELP_OPTION, "Provides help for this program");
		return parser;
	}

	public void parseArgs(String... args) throws Exception {
		OptionParser parser = createOptionParser();
		try {
			final OptionSet options = parser.parse(args);
			fNonOptionArgs = options.nonOptionArguments();
			if (options.has(HELP_OPTION)) {
				this.fRequestedHelp = true;
			} else if (getNonOptionArgs().size() < 1) {
				throw new IllegalArgumentException("Wrong number of arguments");
			}
		} catch (OptionException ex) {
			throw new IllegalArgumentException(ex.getLocalizedMessage(), ex);
		}
	}

	public void run() throws Exception {
		if (fRequestedHelp) {
			printHelp(createOptionParser());
		} else {
			BPTSGenerator bg = new BPTSGenerator(getNonOptionArgs().get(0));
			bg.generateTestTemplate();
			bg.printTestTemplate();
		}
	}

	public final String getName() {
		return NAME;
	}

	public final String getUsage() {
		return USAGE;
	}

	public final String getDescription() {
		return DESCRIPTION;
	}

	public final List<String> getNonOptionArgs() {
		return fNonOptionArgs;
	}

	private void printHelp(OptionParser parser) {
		System.err.println("Usage: bpts-generator " + getUsage());
		System.err.println("Description:\n");
		System.err.println(getDescription() + "\n");
	}
}
