package es.uca.webservices.bptsgen;

/**
 * This exception implies that there have an error building the bpts file 
 * @author Jose Luis Ezquerro Casado (jose.ezquerroca@alum.uca.es)
 */

public class BPTSGeneratorException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor from a STring
	 * @param arg0
	 */
	public BPTSGeneratorException(String arg0){
		super(arg0);
	}
	
	/**
	 * Constructor from a throwable object
	 * @param arg0
	 */
	public BPTSGeneratorException(Throwable arg0){
		super(arg0);
	}
	
	/**
	 * Constructor from a string and a throwable object
	 * @param arg0
	 * @param arg1
	 */
	public BPTSGeneratorException(String arg0, Throwable arg1){
		super(arg0, arg1);
	}

}
