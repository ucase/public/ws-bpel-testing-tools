/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package es.uca.webservices.bptsgen.cli;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.wsdl.analyzer.Application;

/**
 * Main command line launcher for the BPTS Generator.
 * 
 * @author Jose Luis Ezquerro Casado (jose.ezquerroca@alum.uca.es)
 * @version 1.0
 */
public class BPTSGenLauncher {

	private static final String VERSION_PROPERTY = "bptsgen.version";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BPTSGenLauncher.class);
	private static Properties properties;

	public static void main(String[] args) {
		try {
			BPTSGenCommand com = new BPTSGenCommand();
			if (args.length < 1) {
				printUsage(System.err);
				System.exit(1);
			}
			com.parseArgs(args);
			com.run();
		} catch (IllegalArgumentException e) {
			// Bad arguments given through the interface: an usage message
			// should have been printed already
			System.exit(2);
		} catch (Exception e) {
			LOGGER.error("Unhandled exception", e);
			e.printStackTrace();
			System.exit(3);
		}
	}

	private static String getVersion() throws IOException {
		return getProperties().getProperty(VERSION_PROPERTY);
	}

	private static synchronized Properties getProperties() throws IOException {
		InputStream in = null;
		try {
			if (properties == null) {
				properties = new Properties();
				in = Application.class
						.getResourceAsStream("/bpts-generator.properties");
				properties.load(in);
			}
			return properties;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	private static void printUsage(PrintStream err) throws IOException {
		err.println("BPTS Generator version " + getVersion());
		err.println("Usage: bpts-generator (path to .bpel file...)");
	}

}
