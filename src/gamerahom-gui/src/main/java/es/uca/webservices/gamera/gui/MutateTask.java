package es.uca.webservices.gamera.gui;

import java.io.File;
import java.math.BigInteger;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TabFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAExecutor;

/**
 * Thread that executes the mutants and compares them with the original program.
 *
 * @author Juan Daniel Morcillo Regueiro, Antonio García Domínguez
 */
class MutateTask implements Runnable {

	private static class Pair<K, V> extends AbstractMap.SimpleEntry<K, V> {
		private static final long serialVersionUID = 1L;

		public Pair(K arg0, V arg1) {
			super(arg0, arg1);
		}
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(MainDialog.class);
	private AnalyzeTab analyzeTab;
	private Map<String, Integer> opName2Index;

	MutateTask(AnalyzeTab analyzeTab) {
		this.analyzeTab = analyzeTab;
	}

	/**
	 * Finds the 0-based index for an operator from its name. Returns -1 if not
	 * found.
	 */
	private synchronized int getOperatorIndexByName(String name) {
		if (opName2Index == null) {
			opName2Index = new HashMap<String, Integer>();
			final String[] sOperatorNames = analyzeTab.getAnalysis().getOperatorNames();
			for (int i = 0; i < sOperatorNames.length; ++i) {
				opName2Index.put(sOperatorNames[i], i);
			}
		}
		if (opName2Index.containsKey(name)) {
			return opName2Index.get(name);
		} else {
			return -1;
		}
	}

	/**
	 * Method that return a list of string with the name of the selected
	 * operators
	 */
	private List<String> getSelectedOperatorNames() {
		final List<String> selectedOperatorNames = new ArrayList<String>();
		Display.getDefault().syncExec(new Runnable() {
			public void run() {
				selectedOperatorNames.addAll(
					Arrays.asList(analyzeTab.getSelectedList().getItems()));
			}
		});
		return selectedOperatorNames;
	}

	/**
	 * Method that makes the mutation
	 */
	public void run() {
		final BigInteger[] biOperatorLocations = analyzeTab.getAnalysis().getLocationCounts();
		final BigInteger[] biOperatorRanges = analyzeTab.getAnalysis().getFieldRanges();
		final ProgressDialog p = new ProgressDialog();
		resetResults(p);

		// Create the ProgressMonitor to control the progress dialog
		final ProgressMonitor monitor = new ProgressMonitor(p);
		final GAExecutor executor = analyzeTab.getInstance();

		/*
		 * Obtain the selected individuals, while building statistics and
		 * preparing the UI for the results.
		 */
		final Map<String, Integer> mutantsForOperator = new HashMap<String, Integer>();
		final List<GAIndividual> individuals = getSelectedIndividuals(
				biOperatorLocations, biOperatorRanges, mutantsForOperator);
		if (p.isCancelled()) {
			return;
		}

		try {
			// Compare the generated mutants and save the results
			final ComparisonResults[] allCompResults = executor.compare(monitor,
				individuals.toArray(new GAIndividual[individuals.size()]));

			// Put the results into the table of results
			analyzeTab.getResultsTab().setResults(Arrays.asList(allCompResults));

			// Obtain the files produced from the mutants (whenever possible)
			final Pair<File, File[]> files = getGeneratedFiles(executor, allCompResults);
			final File   originalFile    = files.getKey();
			final File[] vGeneratedFiles = files.getValue();

			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					analyzeTab.getStatisticTab().updatePies(Arrays.asList(allCompResults), mutantsForOperator);

					// To generate the differences
					analyzeTab.getDifferencesTab().update(
							getSelectedOperatorNames(), biOperatorLocations,
							biOperatorRanges, originalFile, vGeneratedFiles,
							opName2Index);
					p.getShell().close();

					if (analyzeTab.getParent() instanceof TabFolder) {
						final TabFolder tab = (TabFolder) analyzeTab
								.getParent();
						// Go to the next tab
						tab.setSelection(tab.getSelectionIndex() + 1);
						for (String s : analyzeTab.getSelectedList().getItems()) {
							analyzeTab.getAvailableList().add(s);
						}
						// Restore the values
						analyzeTab.getSelectedList().removeAll();
						analyzeTab.getMutateButton().setEnabled(false);
						analyzeTab.getShell().setEnabled(true);

					}
				}
			});

		} catch (ComparisonException e) {
			LOGGER.error("Exception while comparing the mutants", e);
			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					// Create the error dialog
					final ErrorDialog error = new ErrorDialog();
					p.getShell().close();
					error.open();
					analyzeTab.getShell().setEnabled(true);
					if (p.isCancelled()) {
						error.setText("cancelMessage");
					} else {
						error.setText("compareError");
					}
				}
			});
		}
	}

	private Pair<File, File[]> getGeneratedFiles(final GAExecutor executor,
			final ComparisonResults[] allCompResults) {
		try
		{
			final File[] vGeneratedFiles = new File[allCompResults.length];
			int iFile = 0;
			for (ComparisonResults comparisonResults : allCompResults) {
				final GAIndividual ind = comparisonResults.getIndividual();
				vGeneratedFiles[iFile++] = executor.getMutantFile(ind);
			}
			return new Pair<File, File[]>(executor.getOriginalProgramFile(), vGeneratedFiles);
		}
		catch (Exception ex) {
			return new Pair<File, File[]>(null, new File[0]);
		}
	}

	private void resetResults(final ProgressDialog p) {
		Display.getDefault().syncExec(new Runnable() {
			public void run() {
				// We clean the table for previous executions and we prepare it
				ResultsTab results = analyzeTab.getResultsTab();
				results.prepare();
				results.getExportButton().setEnabled(true);
				p.reset();
				p.open(analyzeTab.getShell());
			}
		});
	}

	private List<GAIndividual> getSelectedIndividuals(
			final BigInteger[] biOperatorLocations,
			final BigInteger[] biOperatorRanges,
			final Map<String, Integer> mutantsForOperator) {
		final List<GAIndividual> individuals = new ArrayList<GAIndividual>();
		for (final String sOp : getSelectedOperatorNames()) {
			final int biIndex = getOperatorIndexByName(sOp);
			final BigInteger biOperatorLocation = biOperatorLocations[biIndex];
			final BigInteger biOperatorRange = biOperatorRanges[biIndex];

			for (BigInteger biLoc = BigInteger.ONE; biLoc.compareTo(biOperatorLocation) <= 0; biLoc = biLoc.add(BigInteger.ONE)) {
				for (BigInteger biAttr = BigInteger.ONE; biAttr.compareTo(biOperatorRange) <= 0; biAttr = biAttr.add(BigInteger.ONE)) {
					final GAIndividual individual = new GAIndividual(BigInteger.valueOf(biIndex + 1), biLoc, biAttr, true);
					individuals.add(individual);

					final int nMutantsForThisOp = mutantsForOperator.containsKey(sOp) ? mutantsForOperator .get(sOp) : 0;
					mutantsForOperator.put(sOp, 1 + nMutantsForThisOp);

					Display.getDefault().syncExec(new Runnable() {
						public void run() {
							// Add the name, line and value of operator to the table of results
							analyzeTab.getResultsTab().addRowForMutant(
								sOp, individual.getThLocation(1).toString(), individual.getThAttribute(1).toString());
						}
					});
				}
			}
		}
		return individuals;
	}

}
