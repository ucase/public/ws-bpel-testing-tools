package es.uca.webservices.gamera.gui;


import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.Collator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;

/**
 * Class to show the results of the mutation
 *
 */
public class ResultsTab extends Composite{
	
	/**
	 * Class to export the results in csv format
	 *
	 */
	private final class ExportListener extends SelectionAdapter {
		
		/**
		 * Export button event handler
		 */
		@Override
		public void widgetSelected(SelectionEvent e) {
			//Show the dialog to save the file
			final FileDialog fdExportFiles = new FileDialog(ResultsTab.this.getShell(), SWT.SAVE);
			fdExportFiles.open();
			if(!fdExportFiles.getFileName().equals("")){
				try {
					//Create the file
					PrintWriter exit=new PrintWriter(fdExportFiles.getFilterPath()+"/"+fdExportFiles.getFileName()+".csv");
					StringBuffer output=new StringBuffer();
					//Copy the table into the file
					for(int i=0; i<tbResults.getItemCount(); i++){
						for(int j=0; j<tbResults.getColumnCount(); j++){
							output.append(tbResults.getItem(i).getText(j));
							if(j!=tbResults.getColumnCount()-1){
									output.append(",");
							}
						}
						exit.println(output);
						output=output.delete(0,output.length());
					}
					exit.close();
				} catch (FileNotFoundException e1) {
					LOGGER.error("Exception to save the CSV", e1);
				}
			}
		}
	}
	
	/**
	 * Class to order the table by a column
	 *
	 */
	private final class ColumnListener extends SelectionAdapter {
		private Table tbResults;
		private int columnPosition;
		
		/**
		 * Construct of the class
		 * @param tbResults Table with the results
		 * @param columnPosition Number of the column
		 */
		ColumnListener(Table tbResults, int columnPosition){
			this.tbResults=tbResults;
			this.columnPosition=columnPosition;
		}
		
		@Override
		/**
		 * Column event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			TableItem[] items = tbResults.getItems();
	        Collator collator = Collator.getInstance(Locale.getDefault());
	        //Put each row in the correct position
	        for (int i = 1; i < items.length; i++) {
	          String value1 = items[i].getText(columnPosition);
	          //Put the first row in the correct position
	          for (int j = 0; j < i; j++) {
	            String value2 = items[j].getText(columnPosition);
	            if (collator.compare(value1, value2) < 0) {
	            	String[] values =new String[tbResults.getColumnCount()];
	            	for(int aux=0; aux<tbResults.getColumnCount();aux++){
	            		values[aux]=items[i].getText(aux);
	            	}
	              items[i].dispose();
	              //Put the row in the correct position
	              TableItem item = new TableItem(tbResults, SWT.NONE, j);
	              item.setText(values);
	              //Put the color
	              items = tbResults.getItems();
	              item.setBackground(0,color);
	              item.setBackground(1,color);
	              item.setBackground(2,color);
	            }
	          }
	        }
		}
	}

	/**
	 * Constructor of the class
	 * @param parent
	 * @param style
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	ResultsTab(Composite parent, int style) throws InstantiationException, IllegalAccessException {
		super(parent, style);
		createContents();
	}
	
	private Table tbResults;
	private Color color;
	private Button btnExport;
	private static final Logger LOGGER = LoggerFactory.getLogger(MainDialog.class);
	private static final ResourceBundle L10N_BUNDLE = ResourceBundle.getBundle("GameraGUI");
	
	/**
	 * Create contents of the window.
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private void createContents() throws InstantiationException, IllegalAccessException {
		this.setLayout(new GridLayout(1, true));
		
		color=this.getDisplay().getSystemColor(SWT.COLOR_CYAN);
		
		//Create the table
		tbResults=new Table(this, SWT.BORDER | SWT.VIRTUAL);
		tbResults.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tbResults.setHeaderVisible(true);
		tbResults.setToolTipText("");
		tbResults.setLinesVisible(true);
		tbResults.setItemCount(0);
        
		//Create the export button
        btnExport = new Button(this, SWT.NONE);
		btnExport.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		btnExport.setEnabled(false);
		btnExport.setText(getLocalizedText("btnExport"));
		btnExport.addSelectionListener(new ExportListener());

	}
	
	/**
	 * Method that create the identification columns to the table
	 */
	void prepare(){
		
		tbResults.setItemCount(0);
		
		//Clear the table
		while(tbResults.getColumnCount()>0){
			tbResults.getColumn(0).dispose();
		}
		
		//Create the name column
		final TableColumn tbcolOpNames = new TableColumn(tbResults, SWT.NONE);
		tbcolOpNames.setResizable(false);
		tbcolOpNames.setText(getLocalizedText("colName"));
		tbcolOpNames.pack();
		tbcolOpNames.addSelectionListener(new ColumnListener(tbResults,0));
		
		//Create the line column
		final TableColumn tbcolOpLines = new TableColumn(tbResults, SWT.NONE);
		tbcolOpLines.setResizable(false);
		tbcolOpLines.setText(getLocalizedText("colLine"));
		tbcolOpLines.pack();
		tbcolOpLines.addSelectionListener(new ColumnListener(tbResults,1));
		
		//Create the value column
		final TableColumn tbcolOpValue = new TableColumn(tbResults, SWT.NONE);
		tbcolOpValue.setResizable(false);
		tbcolOpValue.setText(getLocalizedText("colValue"));
		tbcolOpValue.pack();
		tbcolOpValue.addSelectionListener(new ColumnListener(tbResults,2));
	}
	
	/**
	 * Method to add an identification of mutant to the table
	 * @param mutantName Name of the mutant
	 * @param mutantLine Line of the mutant
	 * @param mutantValue Value of the mutant
	 */
	void addRowForMutant(String mutantName, String mutantLine, String mutantValue){
		final int newItemCount = tbResults.getItemCount()+1;
		tbResults.setItemCount(newItemCount);
		//Create the row
		TableItem tableItem = tbResults.getItem(newItemCount - 1);
		//Put the name
		tableItem.setText(0, mutantName);
		//Put the line
		tableItem.setText(1, mutantLine);
		//Put the value
		tableItem.setText(2, mutantValue);
		//Put the color
		tableItem.setBackground(0,color);
		tableItem.setBackground(1,color);
		tableItem.setBackground(2,color);
	}
	
	/**
	 * Method that return the table
	 */
	Table getTable(){
		return tbResults;
	}
	
	/**
	 * Method that return the export button
	 */
	Button getExportButton(){
		return btnExport;
	}

	/**
	 * Method to update the table with the mutate results
	 * @param alExecutionMatrix Results of the mutation
	 */
	void setResults(final List<ComparisonResults> alExecutionMatrix){
			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					//Create the columns to save the results
					for(int i=0;i<alExecutionMatrix.get(0).getRow().length;i++){
						final TableColumn tbcolumn = new TableColumn(tbResults, SWT.NONE);
	            		tbcolumn.setResizable(false);
	            		//Set the name of the column
	            		tbcolumn.setText(getLocalizedText("colTest")+" "+i);
	            		tbcolumn.pack();
	            		tbcolumn.addSelectionListener(new ColumnListener(tbResults,i+3));
					}
	                for(int i=0;i<tbResults.getItemCount() && i<alExecutionMatrix.size();i++){
	                	int j=0;
	                	for(ComparisonResult a:alExecutionMatrix.get(i).getRow()){
	                		tbResults.getItem(i).setText(j+3, String.valueOf(a.getValue()));
	                		j++;
	                	}
					}
				}
			});		
	}
	
	/**
	 * Method that return the internationalized name associated with a string
	 * @param stringID String to be internationalized
	 */
	private String getLocalizedText(final String stringID) {
		return L10N_BUNDLE.getString(stringID);
	}
}
