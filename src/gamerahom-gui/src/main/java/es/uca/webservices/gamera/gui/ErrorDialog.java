package es.uca.webservices.gamera.gui;

import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to create error messages
 *
 */
public class ErrorDialog {

	private Shell shellError;
	private Label lbMessageErr;
	private static final Logger LOGGER = LoggerFactory.getLogger(MainDialog.class);
	private static final ResourceBundle L10N_BUNDLE = ResourceBundle.getBundle("GameraGUI");

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			ErrorDialog wError = new ErrorDialog();
			wError.open();
		} catch (Exception e) {
			LOGGER.error("Exception to create the error message", e);
			
		}
	}
	
	/**
	 * Method that return the shell
	 */
	public Shell getShell(){
		return shellError;
	}

	/**
	 * Open the window.
	 */
	public void open() {
		createContents();
		shellError.open();
		shellError.layout();
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shellError = new Shell(SWT.APPLICATION_MODAL);
		shellError.setLayout(new GridLayout(1, true));
		
		shellError.setSize(400, 150);
		shellError.setText(ResourceBundle.getBundle("GameraGUI").getString("errorShell"));
		
		
		//Error message
		lbMessageErr=new Label(shellError, SWT.CENTER);
		lbMessageErr.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		
		//Accept button
		Button btnAccept = new Button(shellError, SWT.NONE);
		btnAccept.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));
		btnAccept.setText(ResourceBundle.getBundle("GameraGUI").getString("errorButton"));
		btnAccept.addSelectionListener(new SelectionAdapter() {	
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				shellError.close();
			}
		});
			
	}
	
	/**
	 * Method that modifies the error message
	 * @param text String with the new error message
	 */
	public void setText(final String text){
		lbMessageErr.setText(getLocalizedText(text));
		
	}
	
	/**
	 * Method that return the internationalized name associated with a string
	 * @param stringID String to be internationalized
	 */
	private String getLocalizedText(final String stringID) {
		return L10N_BUNDLE.getString(stringID);
	}
}