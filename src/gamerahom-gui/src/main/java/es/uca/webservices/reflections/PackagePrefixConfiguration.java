package es.uca.webservices.reflections;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.reflections.Configuration;
import org.reflections.adapters.JavassistAdapter;
import org.reflections.adapters.MetadataAdapter;
import org.reflections.scanners.Scanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.serializers.Serializer;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.FilterBuilder;

import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PackagePrefixConfiguration implements Configuration {

    private static final Logger LOGGER = LoggerFactory.getLogger(PackagePrefixConfiguration.class);

    @SuppressWarnings("rawtypes")
	private final MetadataAdapter adapter = new JavassistAdapter();

	private final Set<Scanner> scanners = new HashSet<Scanner>();
	private final Supplier<ExecutorService> execService;
	private final Predicate<String> inputsFilter;
	private final Set<URL> urls;

	public PackagePrefixConfiguration(String packagePrefix) {
		scanners.add(new SubTypesScanner());
		execService = new Supplier<ExecutorService>() {
            public ExecutorService get() {
                return Executors.newSingleThreadExecutor();
            }
        };
        inputsFilter = new FilterBuilder.Include(FilterBuilder.prefix(packagePrefix));
        urls = ClasspathHelper.getUrlsForPackagePrefix(packagePrefix);

        LOGGER.debug("URLs: {}", urls);
	}

	@Override
	public Set<Scanner> getScanners() {
		return scanners;
	}

	@Override
	public Set<URL> getUrls() {
		return urls;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public MetadataAdapter getMetadataAdapter() {
		return adapter;
	}

	@Override
	public boolean acceptsInput(String inputFqn) {
		return inputsFilter.apply(inputFqn);
	}

	@Override
	public Supplier<ExecutorService> getExecutorServiceSupplier() {
		return execService;
	}

	@Override
	public Serializer getSerializer() {
		return null;
	}

}
