package es.uca.webservices.gamera.gui;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import java.util.Map;
import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import es.uca.webservices.gamera.AbstractGenerationalAlgorithm;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionProcessor;
import es.uca.webservices.gamera.conf.Configuration;

public class ConfigureEvolutiveTab extends Composite{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MainDialog.class);
	private static final ResourceBundle L10N_BUNDLE = ResourceBundle.getBundle("GameraGUI");
	
	private final Reflections rReflections;
	private ScrolledComposite scrolledComposite;
	
	ConfigureEvolutiveTab(Composite parent, Reflections ref, int style) throws InstantiationException, IllegalAccessException {
		super(parent, style);
		rReflections=ref;
		createContents();
	}

	
	private final class ItemListener implements Listener {
		private Table table;
		private TableEditor editor;
		
		private ItemListener(Table table) {
			this.table=table;
			this.editor = new TableEditor(table);
		}

		@Override
		public void handleEvent(Event event) {
			Rectangle clientArea = table.getClientArea();
	        Point pt = new Point(event.x, event.y);
	        int index = table.getTopIndex();
	        while (index < table.getItemCount()) {
	          boolean visible = false;
	          final TableItem item = table.getItem(index);
	          for (int i = 0; i < table.getColumnCount(); i++) {
	            Rectangle rect = item.getBounds(i);
	            if (rect.contains(pt)) {
	    	        Text newEditor = new Text(table, SWT.NONE);
	    	        newEditor.setText(item.getText(i));
	    	        final int column=i;
	    	        newEditor.addModifyListener(new ModifyListener() {
	    	          public void modifyText(ModifyEvent me) {
	    	            Text text = (Text) editor.getEditor();
	    	           
	    	            String firstPrevious=item.getText(0);
	    	            String secondPrevious=item.getText(1);
	    	            editor.getItem().setText(column, text.getText());
	    	            table.getColumn(0).pack();
	    	            table.getColumn(1).pack();
	    		        if(firstPrevious.isEmpty() && secondPrevious.isEmpty() && !text.getText().isEmpty()){
	    		        	int previousHeight=table.getParent().getSize().y;
	    		        	table.setItemCount(table.getItemCount()+1);
	    		        	table.getParent().getParent().pack();
	    		        	scrolledComposite.setMinSize(scrolledComposite.getMinWidth(),
	    		        			scrolledComposite.getMinHeight()+table.getParent().getSize().y-previousHeight);
	    		    		
	    		        }
	    		        if((!firstPrevious.isEmpty() && text.getText().isEmpty() && column==0 && secondPrevious.isEmpty()) ||
	    		        		!secondPrevious.isEmpty() && text.getText().isEmpty() && column==1 && firstPrevious.isEmpty()){
	    		        	int previousHeight=table.getParent().getSize().y;
	    		        	table.remove(table.getSelectionIndex());
	    		        	table.getParent().getParent().pack();
	    		        	scrolledComposite.setMinSize(scrolledComposite.getMinWidth(),
	    		        			scrolledComposite.getMinHeight()-table.getParent().getSize().y-previousHeight);
	    		        }
	    		        
	    	          }
	    	        });
	    	        newEditor.selectAll();
	    	        newEditor.setFocus();
	    	        editor.setEditor(newEditor, item, column);
	            	
	            }
	            if (!visible && rect.intersects(clientArea)) {
	              visible = true;
	            }
	          }
	          if (!visible)
	            return;
	          index++;
	        }
	      }
	}

	private void createContents() throws InstantiationException, IllegalAccessException {
		this.setLayout(new FillLayout());
		scrolledComposite = new ScrolledComposite(this, SWT.V_SCROLL | SWT.BORDER);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		scrolledComposite.setAlwaysShowScrollBars(true);
		Composite composite = new Composite(scrolledComposite, SWT.BORDER);
		composite.setLayout(new GridLayout(2,false));
		
		Label lAlgorithm=new Label(composite, SWT.NONE);
		lAlgorithm.setText("Algorithm");
		
		Combo cbAlgorithms = new Combo(composite, SWT.NONE);
		cbAlgorithms.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		cbAlgorithms.setText("Seleccione un algoritmo");
		
		for (Class<? extends AbstractGenerationalAlgorithm> impl : rReflections.getSubTypesOf(AbstractGenerationalAlgorithm.class)) {
            LOGGER.debug("Checking if class '{}' is a non-abstract extension of AbstractGenerationalAlgorithm", impl);
			if (!impl.isInterface() && !Modifier.isAbstract(impl.getModifiers())) {
                LOGGER.info("Adding class '{}' to the list of available AbstractGenerationalAlgorithm extensions", impl);
				cbAlgorithms.add(impl.getName());
			}
		}
		
		final Map<Method, Option> configureMethods = OptionProcessor.listOptions(Configuration.class);
		for (final Method m : Configuration.class.getMethods()) {

			//A falta de anotaciones
			if(m.getName().contains("set")){
				Label l=new Label(composite, SWT.NONE);
				l.setText(m.getName());
				
				final Composite cmpParams = new Composite(composite, SWT.BORDER);
				//cmpParams.set
				int numParams=m.getParameterTypes().length;
				if(numParams>1){
					cmpParams.setLayout(new GridLayout(2*numParams, false));
				}
				else{
					cmpParams.setLayout(new GridLayout(numParams, false));
				}
				cmpParams.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
				
				for(Class<?> c:m.getParameterTypes()){
					boolean collection=false;
					
					if(numParams>1){
						Label paramName=new Label(cmpParams, SWT.NONE);
						paramName.setText(c.getName());
					}
					for(Class<?> i:c.getInterfaces()){
						
						if(i.getName().equals("java.util.Collection")){
							collection=true;
						}	
					}
					if(collection){

						Table tbParam = new Table(cmpParams, SWT.BORDER | SWT.VIRTUAL);
						tbParam.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
						tbParam.setHeaderVisible(false);
						tbParam.setToolTipText("");
						tbParam.setLinesVisible(true);
						tbParam.setItemCount(1);
						final TableColumn tbColumn = new TableColumn(tbParam, SWT.NONE);
						tbColumn.setResizable(false);
						tbColumn.pack();
						
						tbParam.addListener(SWT.MouseDown, new ItemListener(tbParam));
					}
					else if(c.getName().equals("java.util.Map")){
						Table tbParam = new Table(cmpParams, SWT.BORDER | SWT.VIRTUAL);
						tbParam.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
						tbParam.setHeaderVisible(true);
						tbParam.setToolTipText("");
						tbParam.setLinesVisible(true);
						tbParam.setItemCount(1);
						
						final TableColumn tbcolKey = new TableColumn(tbParam, SWT.CENTER);
						tbcolKey.setResizable(true);
						tbcolKey.setText("Key");
						tbcolKey.pack();
						
						final TableColumn tbcolValue = new TableColumn(tbParam, SWT.CENTER);
						tbcolValue.setResizable(true);
						tbcolValue.setText("Value");
						tbcolValue.pack();
						tbParam.addListener(SWT.MouseDown, new ItemListener(tbParam));
					
						
					}
					else{
						Text t=new Text(cmpParams, SWT.NONE);
						t.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
					}
				}
			}
		}
		scrolledComposite.setContent(composite);
		scrolledComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}
	

}
