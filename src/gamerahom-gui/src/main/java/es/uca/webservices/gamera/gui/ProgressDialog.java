package es.uca.webservices.gamera.gui;

import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to show the progress of a process
 *
 */
public class ProgressDialog {

	private Shell shellProgress;
	private ProgressBar pbAnalysis;
	private Label lbState;
	private static final Logger LOGGER = LoggerFactory.getLogger(MainDialog.class);
	private static final ResourceBundle L10N_BUNDLE = ResourceBundle.getBundle("GameraGUI");
	private boolean cancelled=false;
	private int maxValue;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			ProgressDialog wProgress = new ProgressDialog();
			wProgress.open(null);
		} catch (Exception e) {
			LOGGER.error("Exception to create the progress dialog", e);
		}
	}
	
	
	public Shell getShell(){
		return shellProgress;
	}

	/**
	 * Open the window.
	 */
	public void open(Shell shell) {
		createContents(shell);
		shellProgress.open();
		shellProgress.layout();
	}
	

	/**
	 * Create contents of the window.
	 */
	protected void createContents(Shell shell) {
		shellProgress = new Shell(SWT.APPLICATION_MODAL);
		shellProgress.setLayout(new GridLayout(1, true));
		
		shellProgress.setSize(400, 150);
		shellProgress.setText(ResourceBundle.getBundle("GameraGUI").getString("progressShell"));
		
		//Centrate the dialog
		shellProgress.setLocation(
				shell.getLocation().x+shell.getSize().x/2 - shellProgress.getSize().x/2, 
				shell.getLocation().y+shell.getSize().y/2 - shellProgress.getSize().y/2);
		
		
		//Create the label with the message
		Label lbMessage=new Label(shellProgress, SWT.NONE);
		lbMessage.setText(ResourceBundle.getBundle("GameraGUI").getString("progressMessage"));
		lbMessage.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true));
		
		lbState=new Label(shellProgress, SWT.CENTER);
		lbState.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		//Create the progress bar
		pbAnalysis = new ProgressBar(shellProgress, SWT.NONE);
		pbAnalysis.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		//Create the cancel button
		Button btnCancel = new Button(shellProgress, SWT.NONE);
		btnCancel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false));
		btnCancel.setText(ResourceBundle.getBundle("GameraGUI").getString("progressButton"));
		btnCancel.addSelectionListener(new CancelListener());
		
		maxValue=0;
	}
	
	/**
	 * Cancel button event handler
	 */
	private final class CancelListener extends SelectionAdapter {

		@Override
		public void widgetSelected(SelectionEvent e) {
			cancelled=true;
			//thread.interrupt();
		}
	}
	
	/**
	 * Method that return the ProgressBar
	 */
	public ProgressBar getProgressBar(){
		return pbAnalysis;
	}
	
	/**
	 * Method that return the label
	 */
	public Label getLabel(){
		return lbState;
	}
	
	/**
	 * Method that update the message
	 * @param state Message to be show
	 */
	public void setState(final String state){
		lbState.setText(getLocalizedText(state));
	}
	
	/**
	 * Method that return the internationalized name associated with a string
	 * @param stringID String to be internationalized
	 */
	private String getLocalizedText(final String stringID) {
		return L10N_BUNDLE.getString(stringID);
	}
	
	/**
	 * Method that return true if the cancel button has been pulsed
	 */
	public boolean isCancelled(){
		return cancelled;
	}
	
	/**
	 * Method that restart the dialog
	 */
	public void reset(){
		cancelled=false;
	}
	
	/**
	 * Put the new max value
	 * @param max New max value
	 */
	public void setMax(int max){
		maxValue=max;
	}
	
	/**
	 * Return the max value
	 */
	public int getMax(){
		return maxValue;
	}

}
