package es.uca.webservices.gamera.gui;

import java.math.BigInteger;
import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;


import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.exec.GAExecutor;

/**
 * Class which generate the analyze dialog and controls his events
 *
 */
public class AnalyzeTab extends Composite
{
	
	/**
	 * Class which controls the mutation
	 *
	 */
	private final class MutateListener extends SelectionAdapter {
		private Shell shell;
	
		/**
		 * Constructor of the class
		 * @param cmbMutTec Combo with the mutation technique
		 */
		private MutateListener(Shell shell) {
			this.shell=shell;
		}
		
		@Override
		/**
		 * Mutant button event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			//Check if the mutation technique is traditional mutation
			shell.setEnabled(false);
			//Create the thread that makes the mutation
			MutateTask mutateTask=new MutateTask(AnalyzeTab.this);
			Thread mutateThread=new Thread(mutateTask);
			mutateThread.start();
		}
	}
	
	/**
	 * Class which controls the addition of all operators
	 *
	 */
	private final class AddAllListener extends SelectionAdapter {
		
		private final Button btnAddAll;
		private final Button btnAdd;
		private final Button btnMutate;
		private final Button btnEraseAll;
		
		/**
		 * Constructor of the class
		 * @param btnAddAll AddAll button
		 * @param btnAdd Add button
		 * @param btnEraseAll Erase button
		 * @param btnMutate Mutate button
		 */
		private AddAllListener(Button btnAddAll, Button btnAdd, Button btnEraseAll, Button btnMutate) {
			this.btnAddAll=btnAddAll;
			this.btnMutate=btnMutate;
			this.btnAdd=btnAdd;
			this.btnEraseAll=btnEraseAll;
		}
	
		@Override
		/**
		 * AddAll button event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			//Put all the operator in the selected list
			for(String s:lstAvailable.getItems())
			{
				lstSelected.add(s);
			}
			//Clear the available list
			lstAvailable.removeAll();
			
			//Activate the mutate button
			btnMutate.setEnabled(true);
			
			//Update the other buttons
			btnAdd.setEnabled(false);
			btnAddAll.setEnabled(false);
			btnEraseAll.setEnabled(true);
		}
	}
	
	/**
	 * Class which control the addition of an operator
	 *
	 */
	private final class AddListener extends SelectionAdapter {
		
		private final Button btnAdd;
		private final Button btnMutate;
		private final Button btnAddAll;
		private final Button btnEraseAll;
		
		/**
		 * Constructor of the class
		 * @param btnAdd Add button
		 * @param btnAddAll AddAll button
		 * @param btnEraseAll EraseAll button
		 * @param btnMutate Mutate button
		 */
		private AddListener(Button btnAdd, Button btnAddAll, Button btnEraseAll, Button btnMutate) {
			this.btnAdd=btnAdd;
			this.btnMutate=btnMutate;
			this.btnAddAll=btnAddAll;
			this.btnEraseAll=btnEraseAll;
		}
	
		@Override
		/**
		 * Add button event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			final int index=lstAvailable.getFocusIndex();
			final String item=lstAvailable.getItem(index);
			
			//Put the operator in the selected list
			lstSelected.add(item);
			//Remove the operator from the available list
			lstAvailable.remove(index);
			lstAvailable.select(0);
			
			//Activate the mutate button
			btnMutate.setEnabled(true);
			
			
			//Update the other buttons
			if(lstAvailable.getItemCount()==0){
				btnAdd.setEnabled(false);
				btnAddAll.setEnabled(false);
			}
			btnEraseAll.setEnabled(true);
		}
	}
	
	/**
	 * Class which control the elimination of an selected operator
	 *
	 */
	private final class EraseListener extends SelectionAdapter{
		
		private final Button btnErase;
		private final Button btnMutate;
		private final Button btnEraseAll;
		private final Button btnAddAll;
		
		/**
		 * Constructor of the class
		 * @param btnErase Erase button
		 * @param btnAddAll AddAll button
		 * @param btnEraseAll EraseAll button
		 * @param btnMutate Mutate button
		 */
		private EraseListener(Button btnErase, Button btnAddAll, Button btnEraseAll, Button btnMutate) {
			this.btnErase=btnErase;
			this.btnMutate=btnMutate;
			this.btnEraseAll=btnEraseAll;
			this.btnAddAll=btnAddAll;
		}
		
		@Override
		/**
		 * Erase button event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			final int index=lstSelected.getFocusIndex();
			final String item=lstSelected.getItem(index);
			//Add the operator to the available list
			lstAvailable.add(item);
			//Remove the operator from the selected list
			lstSelected.remove(index);
			lstSelected.select(0);
			
			//Update the other buttons
			btnAddAll.setEnabled(true);
			if(lstSelected.getItemCount()==0){
				btnErase.setEnabled(false);
				btnEraseAll.setEnabled(false);
				btnMutate.setEnabled(false);
			}
		}
	}
	
	/**
	 * Class which control the elimination of all selected operator
	 *
	 */
	private final class EraseAllListener extends SelectionAdapter{
		
		private final Button btnErase;
		private final Button btnEraseAll;
		private final Button btnMutate;
		private final Button btnAddAll;
		
		/**
		 * Constructor of the class
		 * @param btnErase Erase button
		 * @param btnEraseAll EraseAll button
		 * @param btnAddAll AddAll button
		 * @param btnMutate Mutate button
		 */
		private EraseAllListener(Button btnErase, Button btnEraseAll, Button btnAddAll, Button btnMutate) {
			this.btnErase=btnErase;
			this.btnMutate=btnMutate;
			this.btnEraseAll=btnEraseAll;
			this.btnAddAll=btnAddAll;
		}
		
		@Override
		/**
		 * EraseAll button event handler
		 */
		public void widgetSelected(SelectionEvent e) {	
			//Put all the operators in the available list
			for(String s: lstSelected.getItems()){
				lstAvailable.add(s);
			}
			//Delete the selected operators
			lstSelected.removeAll();
			
			//Update the buttons
			btnErase.setEnabled(false);
			btnEraseAll.setEnabled(false);
			btnAddAll.setEnabled(true);
			btnMutate.setEnabled(false);
		}
	}
	
	/**
	 * Class which control the available list
	 *
	 */
	private final class AvailableListener extends SelectionAdapter {
		
		private final Button btnAdd;
		private final Button btnErase;
		
		/**
		 * Constructor of the class
		 * @param btnAdd Add button
		 * @param btnErase Erase button
		 */
		private AvailableListener(Button btnAdd, Button btnErase) {
			this.btnAdd=btnAdd;
			this.btnErase=btnErase;

		}
	
		@Override
		/**
		 * Available list event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			btnAdd.setEnabled(true);
			btnErase.setEnabled(false);
			lstSelected.deselectAll();
		}
	}
	
	/**
	 * Class which control the selected list
	 *
	 */
	private final class SelectedListener extends SelectionAdapter {
		
		private final Button btnAdd;
		private final Button btnErase;
		
		/**
		 * Constructor of the class
		 * @param btnAdd Add button
		 * @param btnErase Erase button
		 */
		private SelectedListener(Button btnAdd, Button btnErase) {
			this.btnAdd=btnAdd;
			this.btnErase=btnErase;
		}
	
		@Override
		/**
		 * Selected list event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			btnAdd.setEnabled(false);
			btnErase.setEnabled(true);
			lstAvailable.deselectAll();
		}
	}


	
	
	private static final ResourceBundle L10N_BUNDLE = ResourceBundle.getBundle("GameraGUI");
	private List lstAvailable;
	private List lstSelected;
	private Button btnAddAll;
	private GAExecutor inst;
	private ResultsTab results;
	private AnalysisResults lastResults;
	private Button btnMutate;
	private StatisticsTab statistics;
	private DifferencesTab differences;
	
	/**
	 * Constructor of the class
	 * @param results Instance of results dialog
	 * @param statistics Instance of statistics dialog
	 * @param differences Instance of differences dialog
	 * @param parent
	 * @param style
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public AnalyzeTab(ResultsTab results, StatisticsTab statistics, DifferencesTab differences,
			Composite parent, int style) throws InstantiationException, IllegalAccessException{
		super(parent, style);
		createContents();
		this.results=results;
		this.statistics=statistics;
		this.differences=differences;
	}
	
	/**
	 * Method that update the available list with the results of analysis
	 * @param ar Results of analysis
	 */
	public void updateFromResults(AnalysisResults ar) {
		lastResults = ar;
		lstAvailable.removeAll();
		lstSelected.removeAll();
		btnAddAll.setEnabled(true);

		// Vector with the number of lines in which each operator can be applied
		final BigInteger[] biListOperators = ar.getLocationCounts();

		// Put the available operators in the ArrayList
		for(int i=0;i<ar.getFieldRanges().length;i++){
			if(!biListOperators[i].toString().equals("0")){
				lstAvailable.add(ar.getOperatorNames()[i]);
			}
		}
	}

	/**
	 * Method that return the results of the analysis
	 */
	public AnalysisResults getAnalysis(){
		return lastResults;
	}
	
	/**
	 * Method that set the instance of the GAExecutor
	 * @param inst Instance of GAExecutor
	 */
	public void setIntance(GAExecutor inst){
		this.inst=inst;
	}
	
	/**
	 * Method that return the instance of the GAExecutor
	 */
	public GAExecutor getInstance(){
		return inst;
	}
	
	/**
	 * Method that return the available list
	 */
	public List getAvailableList(){
		return lstAvailable;
	}
	
	/**
	 * Method that return the selected list
	 */
	public List getSelectedList(){
		return lstSelected;
	}
	
	
	/**
	 * Method that return the mutate button
	 */
	public Button getMutateButton(){
		return btnMutate;
	}
	
	/**
	 * Method that generate the elements
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private void createContents() throws InstantiationException, IllegalAccessException {
		this.setLayout(new GridLayout(3, false));
		
		final Label lblMutationOperators = new Label(this, SWT.NONE);
		lblMutationOperators.setText(getLocalizedText("lblMutationOperators"));
		lblMutationOperators.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, false, false, 3, 1));

		final Label lblAvailable = new Label(this, SWT.NONE);
		lblAvailable.setText(getLocalizedText("lblAvailable"));
		lblAvailable.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, false, false, 1, 1));
		new Label(this, SWT.NONE);

		final Label lblSelected = new Label(this, SWT.NONE);
		lblSelected.setText(getLocalizedText("lblSelected"));
		lblSelected.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, false, false));

		//List with the available operators
		lstAvailable = new List(this, SWT.BORDER | SWT.V_SCROLL);
		lstAvailable.setItems(new String[] {});
		lstAvailable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Composite cmpButtons = new Composite(this, SWT.NONE);
		cmpButtons.setLayout(new FillLayout(SWT.VERTICAL));
		cmpButtons.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));

		//Button to add all the operators
		btnAddAll= new Button(cmpButtons, SWT.NONE);
		btnAddAll.setText(getLocalizedText("btnAddAll"));
		btnAddAll.setEnabled(false);
		
		//Button to add an operator
		final Button btnAdd = new Button(cmpButtons, SWT.NONE);
		btnAdd.setText("     ->      ");
		btnAdd.setEnabled(false);
		
		//Button to quit an operator
		final Button btnErase = new Button(cmpButtons, SWT.NONE);
		btnErase.setText("<-");
		btnErase.setEnabled(false);
		
		//Button to quit all operators
		final Button btnEraseAll= new Button(cmpButtons, SWT.NONE);
		btnEraseAll.setText(getLocalizedText("btnEraseAll"));
		btnEraseAll.setEnabled(false);

		//List with the selected operators
		lstSelected = new List(this, SWT.BORDER | SWT.V_SCROLL);
		lstSelected.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Label lblMutationTechnique = new Label(this, SWT.NONE);
		lblMutationTechnique.setText(getLocalizedText("lblMutationTechnique"));
		lblMutationTechnique.setLayoutData(new GridData(SWT.CENTER, SWT.BOTTOM, false, false, 3, 1));
		
		
		//Mutate button
		btnMutate = new Button(this, SWT.NONE);
		btnMutate.setText(getLocalizedText("btnMutate"));
		btnMutate.setEnabled(false);
		btnMutate.setLayoutData(new GridData(SWT.CENTER, SWT.BOTTOM, false, false, 3, 1));
		
		//Add event handlers
		btnMutate.addSelectionListener(new MutateListener(btnMutate.getShell()));
		btnErase.addSelectionListener(new EraseListener(btnErase,btnAddAll, btnEraseAll, btnMutate));
		btnAdd.addSelectionListener(new AddListener( btnAdd,btnAddAll, btnEraseAll, btnMutate));
		btnAddAll.addSelectionListener(new AddAllListener(btnAdd,btnAddAll,btnEraseAll, btnMutate));
		btnEraseAll.addSelectionListener(new EraseAllListener(btnErase,btnEraseAll, btnAddAll, btnMutate));
		
		lstAvailable.addSelectionListener(new AvailableListener(btnAdd, btnErase));
		lstSelected.addSelectionListener(new SelectedListener(btnAdd, btnErase));
	}
	
	/**
	 * Method that return the differences dialog
	 */
	public DifferencesTab getDifferencesTab(){
		return differences;
	}
	
	/**
	 * Method that return the statistics dialog
	 */
	public StatisticsTab getStatisticTab(){
		return statistics;
	}
	
	/**
	 * Method that return the results dialog
	 */
	public ResultsTab getResultsTab(){
		return results;
	}
	
	/**
	 * Method that return the internationalized name associated with a string
	 * @param stringID String to be internationalized
	 */
	private String getLocalizedText(final String stringID) {
		return L10N_BUNDLE.getString(stringID);
	}
}