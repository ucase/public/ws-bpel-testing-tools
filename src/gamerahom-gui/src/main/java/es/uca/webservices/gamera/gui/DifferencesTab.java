package es.uca.webservices.gamera.gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import difflib.Chunk;
import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;

/**
 * Class which generate the differences dialog and controls his events
 *
 */
public class DifferencesTab extends Composite{

	/**
	 * Class to load the mutated file using list
	 */
	private final class SelectListListener extends SelectionAdapter{
		
		private Text txMutate;
		private Button btnLoadMutate;
	
		/**
		 * Constructor of the class
		 * @param txMutate Label with the path of the mutated file
		 * @param btnLoadMutate Button to load the mutated file
		 */
		SelectListListener(Text txMutate, Button btnLoadMutate){
			this.txMutate=txMutate;
			this.btnLoadMutate=btnLoadMutate;
		}
		
		@Override
		/**
		 * Select list event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			//Disable the button and label to load the mutated file
			txMutate.setEnabled(false);
			btnLoadMutate.setEnabled(false);
			
			//Enable the list to select the mutated file
			cbSelectMutant.setEnabled(true);
			if(!cbSelectMutant.getText().equals(getLocalizedText("operator"))){
				spSelectLine.setEnabled(true);
				spSelectValue.setEnabled(true);
			}
		}
	}

	/**
	 * Class to select the operator
	 */
	private final class SelectOperatorListener extends SelectionAdapter{

		private final List<String> lsOriginal, lsMutant;
		
		SelectOperatorListener(List<String> originalLines, List<String> mutantLines){
			this.lsOriginal = originalLines;
			this.lsMutant = mutantLines;
		}
		
		@Override
		public void widgetSelected(SelectionEvent e) {
			//Put the lines of the operator into the combo
			String mutant=cbSelectMutant.getText();
			int index=opName2Index.get(mutant);
			spSelectLine.setMaximum(biOperatorLocation[index].intValue());
			spSelectLine.setEnabled(true);
			
			//Put the values of the operator into the combo
			spSelectValue.setMaximum(biOperatorRanges[index].intValue());
			spSelectValue.setEnabled(true);
			
			loadMutantFile(lsMutant, lsOriginal);
		}
	}

	/**
	 * Class to select the line and the value
	 */
	private final class SelectMutantListener extends SelectionAdapter{
		private final List<String> originalLines, mutantLines;
		
		/**
		 * Constructor of the class
		 * @param originalLines Text of the first text box
		 * @param mutantLines Text of the second text box
		 */
		SelectMutantListener(List<String> originalLines, List<String> mutantLines){
			this.originalLines = originalLines;
			this.mutantLines = mutantLines;
		}
	
		@Override
		public void widgetSelected(SelectionEvent e) {
			loadMutantFile(mutantLines, originalLines);
			// If the two text boxes have text, compare it
			if (!originalLines.isEmpty() && !mutantLines.isEmpty()) {
				diffs(originalLines, mutantLines);
			}
		}
	}

	/**
	 * Class to load the file using a button
	 */
	private final class SelectTextListener extends SelectionAdapter{

		private Text txMutate;
		private Button btnLoadMutate;

		/**
		 * Constructor of the class
		 * @param txMutate Text box with the code
		 * @param btnLoadMutate Load button
		 */
		SelectTextListener(Text txMutate, Button btnLoadMutate){
			this.txMutate=txMutate;
			this.btnLoadMutate=btnLoadMutate;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			txMutate.setEnabled(true);
			btnLoadMutate.setEnabled(true);
			cbSelectMutant.setEnabled(false);
			spSelectLine.setEnabled(false);
			spSelectValue.setEnabled(false);
		}
	}

	/**
	 * Class to control the load button
	 */
	private final class LoadListener extends SelectionAdapter {

		private Text txPath;

		/**
		 * Constructor of the class
		 * @param path Path of the file
		 */
		LoadListener(Text path) {
			this.txPath = path;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			// Create the dialog to select the file
			final FileDialog fdImportFile = new FileDialog(DifferencesTab.this.getShell(), SWT.OPEN);

			fdImportFile.open();
			if(!fdImportFile.getFileName().equals("")){
				// Get the path
				final String sPath = fdImportFile.getFilterPath() + "/"	+ fdImportFile.getFileName();
				txPath.setText(sPath);
			}
		}
	}

	private final class ModifyPathListener implements ModifyListener {
		private final StyledText stCode;
		private final ArrayList<String> myLines;
		private final ArrayList<String> otherLines;
	
		private ModifyPathListener(StyledText stCode, ArrayList<String> myLines, ArrayList<String> otherLines) {
			this.stCode = stCode;
			this.myLines = otherLines;
			this.otherLines = myLines;
		}

		@Override
		public void modifyText(ModifyEvent ev) {
			final String sPath = ((Text)ev.getSource()).getText();
			try {
				if (sPath != null && sPath.trim().length() > 0) {
					load(new File(sPath.trim()), stCode, otherLines, myLines);
				}
			} catch (Exception e) {
				LOGGER.error("Error while loading file " + sPath, e);
			}
		}
	}

	/**
	 * Constructor of the class
	 * @param parent
	 * @param style
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	DifferencesTab(Composite parent, int style) throws InstantiationException, IllegalAccessException {
		super(parent, style);
		createContents();
	}
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MainDialog.class);
	private static final ResourceBundle L10N_BUNDLE = ResourceBundle.getBundle("GameraGUI");
	private StyledText originalFile, mutantFile;
	private BigInteger[] biOperatorLocation, biOperatorRanges;
	private File[] vGeneratedFiles;
	private Map<String, Integer> mutantsForOperator;
	private Map<String, Integer> opName2Index;
	private Combo cbSelectMutant;
	private Spinner spSelectLine;
	private Spinner spSelectValue;
	private Text txOriginal;
	
	/**
	 * Method that generate the elements
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private void createContents() throws InstantiationException, IllegalAccessException {
		this.setLayout(new GridLayout(2, true));

		final Composite cmpButtons = new Composite(this, SWT.BORDER);
		cmpButtons.setLayout(new GridLayout(3, true));
		cmpButtons.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2,1));
		
		Label lbOriginalLoad=new Label(cmpButtons, SWT.NONE);
		lbOriginalLoad.setText(getLocalizedText("lbOriginalLoad"));
		lbOriginalLoad.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		
		//Create the button to load using the list
		Button btnList = new Button(cmpButtons, SWT.RADIO);
		btnList.setText(getLocalizedText("btnList"));
		btnList.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
		btnList.setSelection(true);
		
		//Create the button to load using the button
		Button btnText = new Button(cmpButtons, SWT.RADIO);
		btnText.setText(getLocalizedText("btnText"));
		btnText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));

		//Create the composite to load the original file
		final Composite cmpOriginal = new Composite(cmpButtons, SWT.BORDER);
		cmpOriginal.setLayout(new GridLayout(6, true));
		cmpOriginal.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));

		//Create the text to show the path of the file
		txOriginal=new Text(cmpOriginal, SWT.NONE);
		txOriginal.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,5,1));
		
		//Create the button to load the file
		Button btnLoadOriginal = new Button(cmpOriginal, SWT.NONE);
		btnLoadOriginal.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,1,1));
		btnLoadOriginal.setText(" ... ");
		
		//Create the composite to load using the lists
		final Composite cmpSelect = new Composite(cmpButtons, SWT.BORDER);
		cmpSelect.setLayout(new GridLayout(6, true));
		cmpSelect.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		//Create the combo with the operator names
		cbSelectMutant= new Combo(cmpSelect, SWT.NONE);
		cbSelectMutant.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		cbSelectMutant.setText(getLocalizedText("operator"));
		
		Label lbLine=new Label(cmpSelect, SWT.NONE);
		lbLine.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true));
		lbLine.setText(getLocalizedText("colLine"));
		
		// Create the spinner to select the location
		spSelectLine= new Spinner(cmpSelect, SWT.NONE);
		spSelectLine.setMinimum(1);
		spSelectLine.setEnabled(false);
		spSelectLine.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		Label lbValue=new Label(cmpSelect, SWT.RIGHT);
		lbValue.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true));
		lbValue.setText(getLocalizedText("colValue"));
		
		// Create the spinner to select the value of the attribute field
		spSelectValue= new Spinner(cmpSelect, SWT.NONE);
		spSelectValue.setMinimum(1);
		spSelectValue.setEnabled(false);
		spSelectValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		//Lists to store the texts
		final ArrayList<String> alOriginal = new ArrayList<String>();
		final ArrayList<String> alMutant = new  ArrayList<String>();
		
		//Add event handlers
		cbSelectMutant.addSelectionListener(new SelectOperatorListener(alOriginal, alMutant));
		spSelectValue.addSelectionListener(new SelectMutantListener(alOriginal, alMutant));
		spSelectLine.addSelectionListener(new SelectMutantListener(alOriginal, alMutant));
		
		//Create the composite to load using the button
		final Composite cmpMutate = new Composite(cmpButtons, SWT.BORDER);
		cmpMutate.setLayout(new GridLayout(6, true));
		cmpMutate.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		
		//Create the text to show the path of the mutated file
		Text txMutant=new Text(cmpMutate, SWT.NONE);
		txMutant.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,5,1));
		txMutant.setEnabled(false);
		
		//Create the button to load the mutated file
		Button btnLoadMutant = new Button(cmpMutate, SWT.NONE);
		btnLoadMutant.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		btnLoadMutant.setText(" ... ");
		btnLoadMutant.setEnabled(false);
		
		//Add the event handlers
		btnList.addSelectionListener(new SelectListListener(txMutant, btnLoadMutant));
		btnText.addSelectionListener(new SelectTextListener(txMutant, btnLoadMutant));

		Label lbOriginal=new Label(this, SWT.CENTER);
		lbOriginal.setText(getLocalizedText("lbOriginal"));
		lbOriginal.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		
		Label lbMutant=new Label(this, SWT.CENTER);
		lbMutant.setText(getLocalizedText("lbMutant"));
		lbMutant.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		
		//Create the StyledText to show the original file
		originalFile= new StyledText(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		originalFile.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			
		//Create the StyledText to show the mutated file
		mutantFile = new StyledText(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		mutantFile.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		btnLoadOriginal.addSelectionListener(new LoadListener(txOriginal));
		btnLoadMutant.addSelectionListener(new LoadListener(txMutant));
		txOriginal.addModifyListener(new ModifyPathListener(originalFile, alOriginal, alMutant));
		txMutant.addModifyListener(new ModifyPathListener(mutantFile, alMutant, alOriginal));
	}
	
	/**
	 * Method to update the combos when we mutate others operators
	 * @param selectedOperatorNames Names of the operators
	 * @param biOperatorLocation Lines of the operators
	 * @param biOperatorRanges Values of the operators
	 * @param originalFile 
	 * @param vGeneratedFiles Generated files of the mutation
	 * @param opName2Index	Map to translate the name to index
	 */
	public void update(final List<String> selectedOperatorNames, 
			final BigInteger[] biOperatorLocation, final BigInteger[] biOperatorRanges, 
			File originalFile, File[] vGeneratedFiles, Map<String, Integer> opName2Index){

		//Update the values
		this.biOperatorLocation=biOperatorLocation.clone();
		this.biOperatorRanges=biOperatorRanges.clone();
		this.opName2Index=opName2Index;
		this.vGeneratedFiles=vGeneratedFiles.clone();
		if (originalFile != null) {
			this.txOriginal.setText(originalFile.getAbsolutePath());
		}

		mutantsForOperator=new HashMap<String, Integer>();

		//Clear the combos
		cbSelectMutant.removeAll();
		cbSelectMutant.setText(getLocalizedText("operator"));

		//Put the names of the operators
		for (final String sOp : selectedOperatorNames) {
			final int index = opName2Index.get(sOp);
			cbSelectMutant.add(sOp);
			mutantsForOperator.put(sOp, biOperatorLocation[index].intValue()*biOperatorRanges[index].intValue());
		}
	}
	
	/**
	 * Method that return the internationalized name associated with a string
	 * @param stringID String to be internationalized
	 */
	private String getLocalizedText(final String stringID) {
		return L10N_BUNDLE.getString(stringID);
	}
	
	private void load(final File file, final StyledText stCode, final List<String> myLines, final List<String> otherLines)
			throws FileNotFoundException, IOException
	{
		// Open the file
		BufferedReader input = new BufferedReader(new FileReader(file));
		String line;
		stCode.setText("");

		// Clear previous loads
		myLines.clear();

		// Copy the file into the text box
		try {
			while ((line = input.readLine()) != null) {
				stCode.append(line + "\n");
				myLines.add(line);
			}
		} finally {
			input.close();
		}

		// If the two text boxes have text, compare it
		if (!myLines.isEmpty() && !otherLines.isEmpty()) {
			diffs(myLines, otherLines);
		}
	}

	private void loadMutantFile(final List<String> mutantLines, final List<String> originalLines) {
		int itemCount=cbSelectMutant.getSelectionIndex();
		int index=0;

		//Get the index of the vector corresponding to the mutant
		for(int i=0;i<itemCount;i++){
			index+=mutantsForOperator.get(cbSelectMutant.getItem(i));
		}
		index+=(spSelectLine.getSelection()-1)*spSelectValue.getMaximum();
		index+=spSelectValue.getSelection()-1;
			
		//Load the file
		try {
			load(vGeneratedFiles[index], mutantFile, mutantLines, originalLines);
		} catch (Exception e1) {
			LOGGER.error("Error while loading mutant", e1);
		}
	}
	
	/**
	 * Method that show the differences between the files
	 * @param lsFirst Text of the first file
	 * @param lsSecond Text of the second file
	 */
	private void diffs(List<String> lsFirst, List<String> lsSecond){
		//Clear the previous comparisons
		originalFile.setLineBackground(0, originalFile.getLineCount(), getDisplay().getSystemColor(SWT.COLOR_WHITE));
		mutantFile.setLineBackground(0, mutantFile.getLineCount(), getDisplay().getSystemColor(SWT.COLOR_WHITE));
		//Calculate the differences
		Patch patch = DiffUtils.diff(lsFirst, lsSecond);
		
		//Color the differences
		for (Delta delta: patch.getDeltas()) {
			Chunk original=delta.getOriginal();
			Chunk mutate=delta.getRevised();
			int lineNumber=original.getPosition();
			
			originalFile.setLineBackground(lineNumber, original.size(), getDisplay().getSystemColor(SWT.COLOR_RED));
			if(lineNumber+mutate.size()<mutantFile.getLineCount()){
				mutantFile.setLineBackground(lineNumber, mutate.size(), getDisplay().getSystemColor(SWT.COLOR_GREEN));
			}
			else{
				originalFile.setLineBackground(0, originalFile.getLineCount(), getDisplay().getSystemColor(SWT.COLOR_RED));
				mutantFile.setLineBackground(0, mutantFile.getLineCount(), getDisplay().getSystemColor(SWT.COLOR_GREEN));
			}
		}
	}
}

