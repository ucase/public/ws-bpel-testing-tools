package es.uca.webservices.gamera.gui;

import org.eclipse.swt.widgets.Display;


import es.uca.webservices.gamera.api.exec.GAProgressMonitor;


/**
 * Class which implements the GAExecutor; it is called after a big operation
 *
 */
public class ProgressMonitor implements GAProgressMonitor{
	
	private ProgressDialog progress;
	
	/**
	 * Constructor of the class
	 * @param progress Dialog which show the process of the operation
	 */
	ProgressMonitor(ProgressDialog progress){
		this.progress=progress;
	}

	@Override
	/**
	 * Method that update the progress bar with the actual progress
	 */
	public void done(final int value) {
		Display.getDefault().syncExec(new Runnable(){
			public void run() {
				int actValue=progress.getProgressBar().getSelection();
				progress.getProgressBar().setSelection(actValue+value);
			}
		});
		
	}

	@Override
	/**
	 * Method which return true if the process has been cancelled
	 */
	public boolean isCancelled() {
		return progress.isCancelled();
	}

	@Override
	/**
	 * Method which set the max value of the progress bar
	 */
	public void start(final int maxValue) {
		Display.getDefault().syncExec(new Runnable(){
			public void run() {
				int newMax=maxValue+progress.getMax();
				progress.getProgressBar().setMaximum(newMax);
				progress.getProgressBar().setSelection(progress.getMax());
			}
		});
	}

	@Override
	/**
	 * Method which update the message that the dialog shows
	 */
	public void status(final String message) {
		Display.getDefault().syncExec(new Runnable(){
			public void run() {
				progress.getLabel().setText(message);
			}
		});
		
	}
}
