package es.uca.webservices.gamera.gui;

import java.io.IOException;
import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.reflections.Reflections;

import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.reflections.PackagePrefixConfiguration;


/**
 * 
 * Dialogue that generate the different dialogs.
 * @author daniel
 *
 */

public class MainDialog {
	
	private static final ResourceBundle L10N_BUNDLE = ResourceBundle.getBundle("GameraGUI");
	private Shell shlUploadFiles;
	private Reflections rReflections;
	
	public MainDialog() {
		rReflections=new Reflections(new PackagePrefixConfiguration("es.uca.webservices.gamera"));
	}


	/**
	 * Open the window.
	 * @throws IOException 
	 * @throws AnalysisException 
	 */
	public Shell open() throws InstantiationException, IllegalAccessException, AnalysisException, IOException{
		createContents();
		shlUploadFiles.open();
		shlUploadFiles.layout();
		return shlUploadFiles;
	}

	/**
	 * Create contents of the window.
	 * @param reflections2 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws IOException 
	 * @throws AnalysisException 
	 */
	protected void createContents() throws InstantiationException, IllegalAccessException, AnalysisException, IOException {
		shlUploadFiles = new Shell();
		shlUploadFiles.setLayout(new FillLayout(SWT.VERTICAL));
		shlUploadFiles.setText(getLocalizedText("shellName"));
		shlUploadFiles.setLocation(
				shlUploadFiles.getDisplay().getBounds().width/2 - shlUploadFiles.getSize().x/2, 
				shlUploadFiles.getDisplay().getBounds().height/2 - shlUploadFiles.getSize().y/2);
		
		TabFolder tbMutationType=new TabFolder(shlUploadFiles, SWT.NONE);
		
		TabItem tbtmTraditional=new TabItem(tbMutationType, SWT.NONE);
		tbtmTraditional.setText("Tradicional");
		TabFolder tbTraditional = new TabFolder(tbMutationType, SWT.NONE);
		

		//Create the differences dialog
		DifferencesTab differences=new DifferencesTab(tbTraditional, SWT.NONE);
		
		TabItem tbtmConfigure = new TabItem(tbTraditional, SWT.NONE);
		tbtmConfigure.setText(getLocalizedText("tbtmFileImport"));

		TabItem tbtmAnalysis = new TabItem(tbTraditional, SWT.NONE);
		tbtmAnalysis.setText(getLocalizedText("tbtmAnalysis"));
		
		//Create the results dialog
		ResultsTab results=new ResultsTab(tbTraditional, SWT.NONE);
		
		//Create the statistics dialog
		StatisticsTab statistics=new StatisticsTab(tbTraditional, SWT.NONE);
		
		//Create the analyze dialog
		AnalyzeTab analyze=new AnalyzeTab(results, statistics, differences, tbTraditional, SWT.NONE);
		tbtmAnalysis.setControl(analyze);
		
		//Create the configure dialog
		tbtmConfigure.setControl(new ConfigureTab(analyze, rReflections, tbTraditional, SWT.NONE));

		TabItem tbtmResults = new TabItem(tbTraditional, SWT.NONE);
		tbtmResults.setText(getLocalizedText("tbtmResults"));
		tbtmResults.setControl(results);
		
		TabItem tbtmStatistics= new TabItem(tbTraditional, SWT.NONE);
		tbtmStatistics.setText(getLocalizedText("tbtmStatistics"));
		tbtmStatistics.setControl(statistics);
		
		TabItem tbtmDifferences= new TabItem(tbTraditional, SWT.NONE);
		tbtmDifferences.setText(getLocalizedText("tbtmDifferences"));
		tbtmDifferences.setControl(differences);
		
		
		tbtmTraditional.setControl(tbTraditional);
		
		TabItem tbtmEvolutive=new TabItem(tbMutationType, SWT.NONE);
		tbtmEvolutive.setText("Evolutiva");
		TabFolder tbEvolutive = new TabFolder(tbMutationType, SWT.NONE);
		
		
		ConfigureEvolutiveTab configureEvo=new ConfigureEvolutiveTab(tbEvolutive,rReflections, SWT.NONE);
		TabItem tbtmConfigureEvo=new TabItem(tbEvolutive, SWT.NONE);
		tbtmConfigureEvo.setText("Configuracion");
		tbtmConfigureEvo.setControl(configureEvo);
		
		DifferencesTab differencesEvo=new DifferencesTab(tbEvolutive, SWT.NONE);
		TabItem tbtmDifferencesEvo= new TabItem(tbEvolutive, SWT.NONE);
		tbtmDifferencesEvo.setText("Diferencias evolutivas");
		tbtmDifferencesEvo.setControl(differencesEvo);
		
		tbtmEvolutive.setControl(tbEvolutive);
	}
	
	
	/**
	 * Method that return the internationalized name associated with a string
	 * @param stringID String to be internationalized
	 */
	private String getLocalizedText(final String stringID) {
		return L10N_BUNDLE.getString(stringID);
	}
}
