package es.uca.webservices.gamera.gui;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionProcessor;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.PreparationException;

/**
 * Thread that prepares the GAExecutor and makes the analysis
 *
 */
class AnalyzeTask implements Runnable{
	private static final Logger LOGGER = LoggerFactory.getLogger(MainDialog.class);
	private GAExecutor inst;
	private AnalyzeTab analyzeTab;
	private TabFolder tab;

	/**
	 * Constructor of AnalyzeTask
	 * @param inst Instance of GAExecutor
	 * @param configure Instance of analysis window
	 * @param tab Table which contains the different dialogs
	 */
	AnalyzeTask(final GAExecutor inst, final ConfigureTab configure, final TabFolder tab){
		this.inst=inst;
		this.analyzeTab=configure.getAnalyzeTab();
		this.tab=tab;
	}

	/**
	 * Method that makes the analysis
	 */
	public void run(){
		
		//Create and open the progress dialog
		final ProgressDialog p=new ProgressDialog();
		try {	
			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					p.open(analyzeTab.getShell());
					p.setState("validating");
				}
			});
			
			//Validate the GAExecutor and actualize the progress dialog
			if(!p.isCancelled()){
				inst.validate();
				Display.getDefault().syncExec(new Runnable() {
					public void run() {
						p.getProgressBar().setMaximum(100);
						p.setMax(100);
						p.getProgressBar().setSelection(33);
						p.setState("preparing");
					}
				});
			}
			
			LOGGER.debug("Validated the GAExecutor");
			
			//Prepare the GAExecutor and actualize the progress dialog
			if(!p.isCancelled()){
				inst.prepare();
				Display.getDefault().syncExec(new Runnable() {
					public void run() {
						p.getProgressBar().setSelection(66);
						p.setState("analyzing");
						LOGGER.debug("Prepared the GAExecutor");
					}
				});
			}

			final ProgressMonitor monitor=new ProgressMonitor(p);
			final AnalysisResults results = inst.analyze(monitor);
			
			//Load the results in the analyze dialog and actualize the progress dialog
			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					analyzeTab.updateFromResults(results);

					p.getShell().setVisible(false);
					p.getShell().close();

					if(!p.isCancelled()){
						tab.setEnabled(true);
						// Go to the next tab
						tab.setSelection(tab.getSelectionIndex() + 1);
					}
					analyzeTab.getShell().setEnabled(true);
				}
			});

			LOGGER.info("Analyzed the program");
		} catch (Exception e1) {
			LOGGER.error("Exception to validate the GAExecutor", e1);
			
			//Create the error message
			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					final ErrorDialog error=new ErrorDialog();
					error.open();
					if(p.isCancelled()){
						error.setText("cancelMessage");
					}
					else{
						error.setText("gaExecutorNotValidated");
					}
					p.getShell().setVisible(false);
					p.getShell().close();
					analyzeTab.getShell().setEnabled(true);
				}
			});
			if(inst!=null){
				try {
					inst.cleanup();
				} catch (PreparationException e) {
					LOGGER.error("Exception to clean the GAExecutor", e);
				}
			}
		}
	}
}

/**
 * Class which generate the configure dialog and controls his events
 *
 */

public class ConfigureTab extends Composite {
	
	/**
	 * Class which controls the load of a configure file
	 *
	 */
	private final class LoadListener extends SelectionAdapter {
		
		private Button btnAnalyze;
		private Button btnSaveConfiguration;
		
		
		/**
		 * Constructor of the class
		 * @param btnAnalyze Button which load a configure file
		 */
		LoadListener(Button btnAnalyze, Button btnSaveConfiguration){
			this.btnAnalyze=btnAnalyze;
			this.btnSaveConfiguration=btnSaveConfiguration;
		}
		
		@Override
		/**
		 * Load button event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			//Create the dialog
			final FileDialog fdImportFile = new FileDialog(ConfigureTab.this.getShell(), SWT.OPEN);
			fdImportFile.open();
			
			//Check that there are a selected file
			if(!fdImportFile.getFileName().equals("")){
				String path=fdImportFile.getFilterPath()+"/"+fdImportFile.getFileName();
				try {
					
					//Clear the table
					for(int i=0; i<tbConfigure.getItemCount();i++){
						tbConfigure.getItem(i).setText(1, "");
					}
					
					//Read the file and update the table
					BufferedReader input= new BufferedReader(new FileReader(path));
					String line;
					try{
						while((line=input.readLine())!=null){
							String[] aux=line.split("=");
							for(int i=0;i<alMethodNames.size();i++){
								if(aux[0].equals(alMethodNames.get(i))){
									tbConfigure.getItem(i).setText(1, aux[1]);
								}
							}
						}
					}finally{
						input.close();
					}
					
					//Check that all the fields are completed
					boolean complete=true;
					for(int i=0;i<tbConfigure.getItemCount() && complete;i++){
						if(tbConfigure.getItem(i).getText(1).equals("")){
							complete=false;
						}
						else{
							btnSaveConfiguration.setEnabled(true);
						}
					}
					
					//Activate the analyze button
					if(complete){
						btnAnalyze.setEnabled(true);
					}
				} catch (Exception e1) {
					LOGGER.error("Exception to load the configuration", e1);
				}
			}
		}
	}
	
	
private final class SaveListener extends SelectionAdapter {
		
		private Table table;
		private List<String> alMethodNames;
		
		
		/**
		 * Constructor of the class
		 * @param btnAnalyze Button which load a configure file
		 */
		SaveListener(List<String> alMethodNames, Table table){
			this.table=table;
			this.alMethodNames=alMethodNames;
		}
	
		@Override
		/**
		 * Save button event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			//Create the dialog
			final FileDialog fdSaveConf = new FileDialog(ConfigureTab.this.getShell(), SWT.SAVE);
			fdSaveConf.open();
			//Check that there are a selected file
			if(!fdSaveConf.getFileName().equals("")){
				String path=fdSaveConf.getFilterPath()+"/"+fdSaveConf.getFileName();
				try {
					PrintWriter output=new PrintWriter(path);
					int i=0;
					for(TableItem item:table.getItems()){
						if(item.getText(1)!=""){
							output.println(alMethodNames.get(i)+"="+item.getText(1));
						}
						i++;
					}
					output.close();
				} catch (FileNotFoundException e1) {
					LOGGER.error("Exception to save the configuration", e1);
				}
			}
		}
	}
	
	
	/**
	 * Class which controls the analyze
	 *
	 */
	private final class AnalyzeListener extends SelectionAdapter {
	
		@Override
		/**
		 * Analyze button event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			final int index = cbImplementations.getSelectionIndex();
			final String item = cbImplementations.getItem(index);
			
			//Configure the GAExecutor with the files that we select
			configureGAExecutor(tbConfigure.getItems(), item);
			
			//Clean from previous executions
			analyze.getAvailableList().removeAll();
			analyze.getSelectedList().removeAll();
			
			if (ConfigureTab.this.getParent() instanceof TabFolder) {
				final TabFolder tab = (TabFolder)ConfigureTab.this.getParent();
				
				ConfigureTab.this.getShell().setEnabled(false);
				//Launch the analyze thread
				AnalyzeTask atAnalyze= new AnalyzeTask(inst,ConfigureTab.this,tab);
				analyze.setIntance(inst);
				Thread analyzeThread=new Thread(atAnalyze);
				analyzeThread.start();	
			}
		}

		/**
		 * Method that configure the GAExecutor with the selected files
		 * @param tableItems Parameters of the table
		 * @param className String with the name of selected class
		 */
		private void configureGAExecutor(TableItem[] tableItems, String className) {
			Class<?> selectedClass = null;
			try {
				selectedClass = Class.forName(className);
				
				int cont = 0;
				
				//Clean from previous executions
				if (inst != null) {
					inst.cleanup();
				}
				
				//Create an instance of the selected class
				inst = (GAExecutor) selectedClass.newInstance();

				final Map<Method, Option> options = OptionProcessor.listOptions(selectedClass);
				
				//Find the configure methods and invoke them with the selected files
				for (final Method m : selectedClass.getMethods()) {
					if (options.containsKey(m)) {
						String type=options.get(m).type().toString();
						if(type.equals("PATH_LOAD") || type.equals("PATH_SAVE")){
							final String sPath = tableItems[cont].getText(1);
							m.invoke(inst, new Object[] { sPath });
							cont++;
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error("The selected class does not exist", e);
				
				//Create the error dialog
				final ErrorDialog error=new ErrorDialog();

				if(inst!=null){
					try {
						inst.cleanup();
					} catch (PreparationException e1) {
						LOGGER.error("Exception to clean the GAExecutor", e1);
					}
				}
				
				//Clean the erroneous dates
				cbImplementations.setText(getLocalizedText("cbImplementations"));
				error.open();
				error.setText("classNotFound");
			}
		}
	}

	/**
	 * Class which controls the selection of items
	 *
	 */
	private final class TableItemSelectionListener extends SelectionAdapter {
		private final Button btnAnalyze;
		private final Button btnSaveConfiguration;
		private final TableEditor editor;

		/**
		 * Constructor of the class
		 * @param btnAnalyze Button of analysis
		 * @param editor TableEditor to write in the table
		 */
		private TableItemSelectionListener(Button btnAnalyze, Button btnSaveConfiguration,
				TableEditor editor) {
			this.btnAnalyze = btnAnalyze;
			this.btnSaveConfiguration=btnSaveConfiguration;
			this.editor=editor;
		}
		
		/**
		 * Write into the table event handler
		 */
		@Override
		public void widgetSelected(SelectionEvent e) {
			TableItem item = (TableItem) e.item;
	        Text newEditor = new Text(tbConfigure, SWT.NONE);
	        newEditor.setText(item.getText(1));
	        newEditor.addModifyListener(new ModifyListener() {
	          public void modifyText(ModifyEvent me) {
	            Text text = (Text) editor.getEditor();
	            editor.getItem().setText(1, text.getText());
	            //Check that all the fields are completed
	            boolean bFilledFields = true;
	            boolean bFullEmptyFiels= true;
		        for (TableItem aux : tbConfigure.getItems()) {
					if (aux.getText(1).equals("")) {
						bFilledFields = false;
					}
					else{
						bFullEmptyFiels=false;
					}
				} 
		        //Activate the analysis button
				if (bFilledFields) {
					btnAnalyze.setEnabled(true);
				}
				else{
					btnAnalyze.setEnabled(false);
				}
				if(bFullEmptyFiels){
					btnSaveConfiguration.setEnabled(false);
				}
				else{
					btnSaveConfiguration.setEnabled(true);
				}
	          }
	        });
	        newEditor.selectAll();
	        newEditor.setFocus();
	        editor.setEditor(newEditor, item, 1);
		}
	}
	
	/**
	 * Class which controls the selection of files into the  items
	 *
	 */
	private final class TableItemDoubleSelectionListener implements Listener {
		private final Button btnAnalyze;
		private final Button btnSaveConfiguration;

		
		/**
		 * Constructor of the class
		 * @param btnAnalyze Button of analysis
		 */
		private TableItemDoubleSelectionListener(Button btnAnalyze, Button btnSaveConfiguration) {
			this.btnAnalyze = btnAnalyze;
			this.btnSaveConfiguration=btnSaveConfiguration;
		}

		/**
		 * Load file event handler
		 */
		@Override
		public void handleEvent(Event arg0) {
			//Create the browser
			final FileDialog fdImportFiles = new FileDialog(ConfigureTab.this.getShell(), SWT.OPEN);
			int index = tbConfigure.getSelectionIndex();

			// Put the extension
			List<String> fieldExtensions = alExtensions.get(index);
			fdImportFiles.setFilterExtensions(fieldExtensions.toArray(new String[fieldExtensions.size()]));
			if (fdImportFiles.open() == null) {
				return;
			}

			//Set the path
			if(!fdImportFiles.getFileName().equals("")){
				final String folderPath = fdImportFiles.getFilterPath();
				final String fileName = fdImportFiles.getFileName();
				final String sCat = folderPath + "/" + fileName;
				tbConfigure.getItem(index).setText(1, sCat);
			}

			//Check that all the fields are completed
			boolean bFilledFields = true;
			for (TableItem aux : tbConfigure.getItems()) {
				if (aux.getText(1).equals("")) {
					bFilledFields = false;
				}
				else{
					btnSaveConfiguration.setEnabled(true);
				}
			}
			
			//Activate the analysis button
			if (bFilledFields) {
				btnAnalyze.setEnabled(true);
			}
			
		}
	}
	
	/**
	 * Class which controls the selection of the class
	 *
	 */
	private final class ExecutorTypeSelectionListener extends SelectionAdapter {
		private final Button btnAnalyze;
		private final Button btnLoadConfiguration;
		private final Button btnSaveConfiguration;
		private final TableEditor editor;

		/**
		 * Constructor of the class
		 * @param btnAnalyze Button of analysis
		 * @param btnLoadConfiguration Button of load configuration
		 * @param editor TableEditor to the event handler
		 */
		private ExecutorTypeSelectionListener(Button btnAnalyze, Button btnLoadConfiguration, Button btnSaveConfiguration,
				TableEditor editor) {
			this.btnAnalyze = btnAnalyze;
			this.btnLoadConfiguration=btnLoadConfiguration;
			this.btnSaveConfiguration=btnSaveConfiguration;
			this.editor=editor;
		}

		/**
		 * Create an instance of the selected class and Update the table
		 */
		@Override
		public void widgetSelected(SelectionEvent e) {
			
			try {
				//Clear the table from previous runs
				if(tbConfigure.getListeners(SWT.Selection).length>0){
					tbConfigure.removeListener(SWT.Selection, tbConfigure.getListeners(SWT.Selection)[0]);
				}
				tbConfigure.removeAll();
				
				btnAnalyze.setEnabled(false);
				btnLoadConfiguration.setEnabled(true);
				
				final int selectedIndex = cbImplementations.getSelectionIndex();
				final String selectedItem = cbImplementations.getItem(selectedIndex);
				
				// Create an instance of the selected class
				final Class<?> selectedClass = Class.forName(selectedItem);
				
				//Update the table with the configure methods of selected class
				addConfigureItems(tbConfigure, selectedClass, btnAnalyze, editor);
			} catch (ClassNotFoundException e1) {
				LOGGER.error("The class which we select doesn't exists", e1);
				final ErrorDialog error=new ErrorDialog();
				
				if(inst!=null){
					try {
						inst.cleanup();
					} catch (PreparationException e2) {
						LOGGER.error("Exception to clean the GAExecutor", e2);
					}
				}
				cbImplementations.setText(getLocalizedText("cbImplementations"));
				error.open();
				error.setText("classNotFound");
			}
		}

		/**
		 * Method that update the table with the configure methods of selected class
		 * @param tbConfigure Table to be update
		 * @param selectedClass Name of the selected class
		 * @param btnAnalyze Button of analysis
		 * @param editor TableEditor to the event handler
		 */
		private void addConfigureItems(final Table tbConfigure,	
				final Class<?> selectedClass, final Button btnAnalyze, final TableEditor editor) {
			alExtensions = new ArrayList<List<String>>();
			final Map<Method, Option> options = OptionProcessor.listOptions(selectedClass);
			tbConfigure.setItemCount(0);
			
			// Find the configured methdos
			for (final Method m : selectedClass.getMethods()) {
				if (options.containsKey(m)) {
					final List<String> mExtensions = new ArrayList<String>();
					alExtensions.add(mExtensions);

					Option methodOptions = options.get(m);
					String type=methodOptions.type().toString();
					//　If it is a configure method, add to the table
					if(type.equals("PATH_LOAD") || type.equals("PATH_SAVE")){
						alMethodNames.add(m.getName());
						final int newItemCount = tbConfigure.getItemCount()+1;
						tbConfigure.setItemCount(newItemCount);
						TableItem tableItem = tbConfigure.getItem(newItemCount - 1);
						tableItem.setText(0, OptionProcessor.getShortDescription(m));
						tableItem.setText(1, "");

						if (methodOptions.fileExtensions() != null) {
							for (String ext : methodOptions.fileExtensions()) {
								mExtensions.add("*." + ext);
							}
						}
					}
				}
			}
			for (TableColumn col : tbConfigure.getColumns()) {
				col.pack();
			}
			System.out.println(alExtensions);

			// Add the event handlers
			tbConfigure.addListener(SWT.MouseDoubleClick, new TableItemDoubleSelectionListener(btnAnalyze,
					btnSaveConfiguration));
			tbConfigure.addSelectionListener(new TableItemSelectionListener(btnAnalyze, btnSaveConfiguration,
					editor));
		}
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(MainDialog.class);
	private static final ResourceBundle L10N_BUNDLE = ResourceBundle.getBundle("GameraGUI");

	private final Reflections rReflections;
	private GAExecutor inst;
	private AnalyzeTab analyze;
	private Combo cbImplementations;
	private Table tbConfigure;
	private List<List<String>> alExtensions;
	private List<String> alMethodNames=new ArrayList<String>();

	/**
	 * Constructor of the class
	 * @param analyze 
	 * @param ref
	 * @param parent
	 * @param style
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public ConfigureTab(AnalyzeTab analyze, Reflections ref, Composite parent, int style)
			throws InstantiationException, IllegalAccessException {
		super(parent, style);
		this.rReflections = ref;
		this.analyze=analyze;
		createContents();
	}

	/**
	 * Method that return the Combo with the implementations
	 */
	public Combo getImplementantions(){
		return cbImplementations;
	}
	
	/**
	 * Method that return the table
	 */
	public Table getTable(){
		return tbConfigure;
	}

	/**
	 * Method that generate the elements
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private void createContents() throws InstantiationException, IllegalAccessException {
		this.setLayout(new GridLayout(3, true));

		final Label lblUploadFiles = new Label(this, SWT.NONE);
		lblUploadFiles.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		lblUploadFiles.setText(getLocalizedText("lblUploadFiles"));

		//Combo to select the implementation
		cbImplementations = new Combo(this, SWT.NONE);
		cbImplementations.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		//Table to configure the GAExecutor
		tbConfigure = new Table(this, SWT.BORDER | SWT.VIRTUAL);
		tbConfigure.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		tbConfigure.setHeaderVisible(true);
		tbConfigure.setToolTipText("");
		tbConfigure.setLinesVisible(true);
		tbConfigure.setItemCount(0);
		
		//TableEditor to write into the table
		final TableEditor editor = new TableEditor(tbConfigure);

		//Column with the name of the method
		final TableColumn tbcolFile = new TableColumn(tbConfigure, SWT.NONE);
		tbcolFile.setResizable(false);
		tbcolFile.setText(getLocalizedText("tbFile"));
		tbcolFile.pack();

		//Column with the paths
		final TableColumn tbcolPath = new TableColumn(tbConfigure, SWT.NONE);
		tbcolPath.setResizable(false);
		tbcolPath.setText(getLocalizedText("tbPath"));
		tbcolPath.pack();

		//Button to analyze
		final Button btnAnalyze = new Button(this, SWT.NONE);
		btnAnalyze.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnAnalyze.setEnabled(false);
		btnAnalyze.setText(getLocalizedText("btnAnalyze"));
		btnAnalyze.addSelectionListener(new AnalyzeListener());
		
		
		//Button to load the configuration file
		final Button btnLoadConfiguration= new Button(this, SWT.NONE);
		btnLoadConfiguration.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnLoadConfiguration.setEnabled(false);
		btnLoadConfiguration.setText(getLocalizedText("btnLoadConfiguration"));
		
		//Button to save the configuration file
		final Button btnSaveConfiguration= new Button(this, SWT.NONE);
		btnSaveConfiguration.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnSaveConfiguration.setEnabled(false);
		btnSaveConfiguration.setText(getLocalizedText("btnSaveConfiguration"));
		btnSaveConfiguration.addSelectionListener(new SaveListener(alMethodNames, tbConfigure));

		
		btnLoadConfiguration.addSelectionListener(new LoadListener(btnAnalyze, btnSaveConfiguration));

		cbImplementations.setText(getLocalizedText("cbImplementations"));
		cbImplementations.addSelectionListener(new ExecutorTypeSelectionListener(
				btnAnalyze, btnLoadConfiguration, btnSaveConfiguration, editor));
		for (Class<? extends GAExecutor> impl : rReflections.getSubTypesOf(GAExecutor.class)) {
            LOGGER.debug("Checking if class '{}' is a non-abstract implementation of GAExecutor", impl);
			if (!impl.isInterface() && !Modifier.isAbstract(impl.getModifiers())) {
                LOGGER.info("Adding class '{}' to the list of available GAExecutor implementations", impl);
				cbImplementations.add(impl.getName());
			}
		}

		this.getShell().addListener(SWT.Close, new Listener(){
			public void handleEvent(Event event){
				if(inst!=null){
					try {
						inst.cleanup();
					} catch (PreparationException e) {
						LOGGER.error("Exception to clean the GAExecutor", e);
					}
				}
			}
		});
		
	}
	
	/**
	 * Method that return the analyze dialog
	 * @return
	 */
	public AnalyzeTab getAnalyzeTab(){
		return analyze;
	}

	/**
	 * Method that return the internationalized name associated with a string
	 * @param stringID String to be internationalized
	 */
	private String getLocalizedText(final String stringID) {
		return L10N_BUNDLE.getString(stringID);
	}
}
