package es.uca.webservices.gamera.gui;


import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Entry point to the application from the command line.
 * 
 * @author Antonio García-Domínguez, Juan Daniel Morcillo-Regueiro
 */
public final class GUIRunner {
	
	private GUIRunner(){}
	private static final Logger LOGGER = LoggerFactory.getLogger(GUIRunner.class);
	
	public static void main(String[] args) {
		try {
			MainDialog window = new MainDialog();
			Shell shell=window.open();
			
			Display display=Display.getDefault();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
			display.dispose();
		} catch (Exception e) {
			LOGGER.error("Exception", e);
		}
	}
}
