package es.uca.webservices.gamera.gui;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.experimental.chart.swt.ChartComposite;
import org.jfree.util.Rotation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMImplementation;

import es.uca.webservices.gamera.api.ComparisonResults;


/**
 * Class which generate the statistics dialog and controls his events
 *
 */
public class StatisticsTab extends Composite{
	
	/**
	 * Class to generate the graphics of an operator
	 *
	 */
	private final class MutantSelectionListener extends SelectionAdapter {

		@Override
		/**
		 * Mutant selection event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			double mutantValues[]={0,0,0};
			
			//Get the operator name
			String operator=cbOperator.getItem(cbOperator.getSelectionIndex());
			
			int numberOfMutant=mutantsForOperator.get(operator);
			int start=0;

			//Get the number of mutants that the operator generates
			boolean found = false;
			for(int i=0; i<cbOperator.getItems().length && !found; i++) {
				String operatorAux=cbOperator.getItem(i);
				if (!operator.equals(operatorAux)) {
					start+=mutantsForOperator.get(operatorAux);
				}
				else {
					found=true;
				}
			}
			
			//Create the graphic
			getPercentsByOperator(mutantValues, start, start+numberOfMutant);
			DefaultPieDataset dpdOperator = new DefaultPieDataset();
			dpdOperator.setValue(getLocalizedText("stateAlive")+": "+ formatter.format(mutantValues[0])+"%",mutantValues[0]);
			dpdOperator.setValue(getLocalizedText("stateDead")+": "+ formatter.format(mutantValues[2])+"%", mutantValues[2]);
			dpdOperator.setValue(getLocalizedText("stateFail")+": "+ formatter.format(mutantValues[1])+"%", mutantValues[1]);
		    
			
			JFreeChart jfcOperatorPie = ChartFactory.createPieChart3D(
					getLocalizedText("operatorComposite"),  // chart title
	            dpdOperator,             // data
	            true,               // include legend
	            true,
	            false
	        );
			PiePlot ppOperator = (PiePlot) jfcOperatorPie.getPlot();
	        ppOperator.setDirection(Rotation.CLOCKWISE);

	        JFreeChart jfcOperator = new JFreeChart(getLocalizedText("operatorComposite"), null, ppOperator, true); 
	          
	        chartOperatorComposite.setChart(jfcOperator);
	        chartOperatorComposite.forceRedraw();    
		}
	}
	
	/**
	 * Class to generate the graphics of an state
	 *
	 */
	private final class StateSelectionListener extends SelectionAdapter {

		@Override
		/**
		 * State selection event handler
		 */
		public void widgetSelected(SelectionEvent e) {

			final DefaultPieDataset dpdState = new DefaultPieDataset();
			final String state = cbState.getText();
			
			// Get the total of mutants which are in the selected state
            int total = 0;
            for(ComparisonResults crAux : alExecutionMatrix){
            	//alExecutionMatrix.get(0).getIndividual()
				if(state.equals(getLocalizedText("stateAlive")) && crAux.isAlive()) {
                    // The mutant is alive
                    total++;
				}
				else if(state.equals(getLocalizedText("stateFail")) && crAux.isInvalid()) {
                    // The mutant is invalid
                    total++;
				}
				else if(state.equals(getLocalizedText("stateDead")) && crAux.isDead()) {
                    // The mutant is dead
                    total++;
				}
			}

			for(String operator : cbOperator.getItems()) {
				int start=0;
			    final int numberOfMutant = mutantsForOperator.get(operator);
				
				for(int i = 0; i < cbOperator.getItems().length; i++) {
					final String operatorAux = cbOperator.getItem(i);
					if (!operator.equals(operatorAux)) {
						start += mutantsForOperator.get(operatorAux);
					}
					else {
                        break;
					}
				}
				

				// For each operator, check if it is in that state
                double value = 0;
                for(int i = start; i < start + numberOfMutant; i++) {
					final ComparisonResults aux = alExecutionMatrix.get(i);

					if(state.equals(getLocalizedText("stateAlive")) && aux.isAlive()) {
						value++;
					}
					else if (state.equals(getLocalizedText("stateFail")) && aux.isInvalid()) {
						value++;
					}
					else if(state.equals(getLocalizedText("stateDead")) && aux.isDead()) {
						value++;
					}
				}

				// Calculate the percentage
				if(total > 0) {
					value = value * 100 / total;
				}
				dpdState.setValue(operator+": "+ formatter.format(value)+"%",value);
	
			}
			
			//Create the statistic
			JFreeChart jfcStatePie = ChartFactory.createPieChart3D(
				getLocalizedText("stateComposite"),  // chart title
	            dpdState,             // data
	            true,               // include legend
	            true,
	            false
	        );
			PiePlot ppState = (PiePlot) jfcStatePie.getPlot();
	        ppState.setDirection(Rotation.CLOCKWISE);
	        ppState.setNoDataMessage(getLocalizedText("noData"));

	        JFreeChart jfcState = new JFreeChart(getLocalizedText("stateComposite"), null, ppState, true); 
	          
	        chartStateComposite.setChart(jfcState);
	        chartStateComposite.forceRedraw();
			}
		}
	
	/**
	 * Class to save the statistic
	 *
	 */
	private final class ExportListener extends SelectionAdapter {

		private ChartComposite ccStatistic;
		
		/**
		 * Constructor of the class
		 * @param ccStatistic Statistic to be saved
		 */
		ExportListener(ChartComposite ccStatistic){
			this.ccStatistic=ccStatistic;
		}
		
		/**
		 * Method to save the statistic in png
		 * @param path Path to save the statistic
		 */
		public void saveAsPNG(final String path){
			try {
				//Get the size of the statistic
				final int width=ccStatistic.getSize().x;
				final int height=ccStatistic.getSize().y;
				//Create the png file
				ChartUtilities.saveChartAsPNG(new File(path+".png"), ccStatistic.getChart(), 
				        width, height);
			} catch (IOException e1) {
				LOGGER.error("Exception to save the png", e1);
			}
		}
		
		/**
		 * Method to save the statistic in pdf
		 * @param path Path to save the statistic
		 */
		public void saveAsPDF(final String path){
			//Get the size of the statistic
			final int width=ccStatistic.getSize().x;
			final int height=ccStatistic.getSize().y;
			//Create the document
			PDDocument document;
				try {
					document = new PDDocument();
					PDPage page = new PDPage();
					document.addPage( page );
					document.save( path+".pdf");
					PDJpeg image= new PDJpeg(document, ccStatistic.getChart().createBufferedImage(width, height));
					image.write2file(new File(path+".pdf"));
					document.close();
				} catch (IOException e) {
					LOGGER.error("Exception to create the document", e);
				} catch (COSVisitorException e) {
					LOGGER.error("Exception to write into the document", e);
				}
		}
		
		/**
		 * Method to save the statistic in svg
		 * @param path Path to save the statistic
		 */
		public void saveAsSVG(final String path){
			
			final int width=ccStatistic.getSize().x;
			final int height=ccStatistic.getSize().y;
			
			DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
	        org.w3c.dom.Document document = domImpl.createDocument(null, "svg", null);

	        //Create an instance of the SVG Generator
	        SVGGraphics2D svgGenerator = new SVGGraphics2D(document);

	        //Draw the chart in the SVG generator
	        Rectangle2D bounds=new Rectangle2D.Double(0, 0, width, height);
	        ccStatistic.getChart().draw(svgGenerator, bounds);

	        //Write svg file
	        OutputStream outputStream;
			try {
				outputStream = new FileOutputStream(path+".svg");
				Writer out = new OutputStreamWriter(outputStream, "UTF-8");
		        svgGenerator.stream(out, true /* use css */);						
		        outputStream.flush();
		        outputStream.close();
			} catch (IOException e1) {
				LOGGER.error("Exception to save the svg", e1);
			}
		}
		
		@Override
		/**
		 * Export event handler
		 */
		public void widgetSelected(SelectionEvent e) {
			//Create the save dialog
			final FileDialog fdExportFiles = new FileDialog(StatisticsTab.this.getShell(), SWT.SAVE);
			fdExportFiles.setFilterExtensions(new String[]{"*.pdf", "*.png", "*.svg"});
			fdExportFiles.open();
			String path=fdExportFiles.getFilterPath()+"/"+fdExportFiles.getFileName();

            if(!fdExportFiles.getFileName().equals("")){
            	//If the extension is png
				if(fdExportFiles.getFilterExtensions()[fdExportFiles.getFilterIndex()].equals("*.png")){
					saveAsPNG(path);
				}
				//If the extension is pdf
				if(fdExportFiles.getFilterExtensions()[fdExportFiles.getFilterIndex()].equals("*.pdf")){
	                saveAsPDF(path);
				}
				//If the extension is svg
				if(fdExportFiles.getFilterExtensions()[fdExportFiles.getFilterIndex()].equals("*.svg")){
					saveAsSVG(path);
				}	
            }
		}
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MainDialog.class);
	private static final ResourceBundle L10N_BUNDLE = ResourceBundle.getBundle("GameraGUI");
	private ChartComposite chartGlobalComposite;
	private ChartComposite chartOperatorComposite;
	private ChartComposite chartStateComposite;
	private ChartComposite chartMutantForOperator;
	private Combo cbOperator;
	private Combo cbState;
	private Button btnGlobalExport;
	private Button btnOperatorExport;
	private Button btnStateExport;
	private Button btnMutantForOperatorExport;
	private List<ComparisonResults> alExecutionMatrix; 
	private Map<String, Integer> mutantsForOperator;
	private JFreeChart jfcClean;
	private DecimalFormat formatter = new DecimalFormat ("###.##");
	
	/**
	 * Constructor of the class
	 * @param parent
	 * @param style
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	StatisticsTab(Composite parent, int style) throws InstantiationException, IllegalAccessException {
		super(parent, style);
		createContents();
	}
	
	/**
	 * Method that generate the elements
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private void createContents() throws InstantiationException, IllegalAccessException {
		this.setLayout(new GridLayout(2, true));

		//Empty statistic to show when the dialog is created
		DefaultPieDataset dpdClean = new DefaultPieDataset();    
	    	
		JFreeChart jfcCleanPie = ChartFactory.createPieChart3D(
            "",  // chart title
            dpdClean,             // data
            true,               // include legend
            true,
            false
        );
		PiePlot ppClean = (PiePlot) jfcCleanPie.getPlot();
        ppClean.setDirection(Rotation.CLOCKWISE);
        ppClean.setNoDataMessage(getLocalizedText("noData"));

        jfcClean = new JFreeChart("", null, ppClean, true); 

        //Create the global statistic
        chartGlobalComposite = new ChartComposite(this, SWT.NONE, jfcClean, false);  
        chartGlobalComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
        chartGlobalComposite.setDisplayToolTips(true);  
        chartGlobalComposite.setHorizontalAxisTrace(false);  
        chartGlobalComposite.setVerticalAxisTrace(false);  
        
        //Crate the mutant statistic
        chartMutantForOperator=new ChartComposite(this, SWT.NONE, jfcClean, false);  
        chartMutantForOperator.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
        chartMutantForOperator.setDisplayToolTips(true);  
        chartMutantForOperator.setHorizontalAxisTrace(false);  
        chartMutantForOperator.setVerticalAxisTrace(false);

        
        Composite globalComposite =new Composite(this, SWT.NONE);
        globalComposite.setLayout(new GridLayout(2,true));
        
        //Crate the button to export the global statistic
        btnGlobalExport=new Button(globalComposite,SWT.NONE);
        btnGlobalExport.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true));
        btnGlobalExport.setText(getLocalizedText("btnExport"));
        btnGlobalExport.addSelectionListener(new ExportListener(chartGlobalComposite));
        btnGlobalExport.setEnabled(false);
        
        Composite mutantForOperatorComposite =new Composite(this, SWT.NONE);
        mutantForOperatorComposite.setLayout(new GridLayout(2,true));
         
        //Crate the button to export the mutant statistic
        btnMutantForOperatorExport=new Button(mutantForOperatorComposite,SWT.NONE);
        btnMutantForOperatorExport.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true));
        btnMutantForOperatorExport.setText(getLocalizedText("btnExport"));
        btnMutantForOperatorExport.addSelectionListener(new ExportListener(chartMutantForOperator));
        btnMutantForOperatorExport.setEnabled(false);
        
        //Create the state statistic
        chartStateComposite = new ChartComposite(this, SWT.NONE, jfcClean, false);  
        chartStateComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
        chartStateComposite.setDisplayToolTips(true);  
        chartStateComposite.setHorizontalAxisTrace(false);  
        chartStateComposite.setVerticalAxisTrace(false);
        
        //Create the operator statistic
        chartOperatorComposite = new ChartComposite(this, SWT.NONE, jfcClean, false);  
        chartOperatorComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
        chartOperatorComposite.setDisplayToolTips(true);  
        chartOperatorComposite.setHorizontalAxisTrace(false);  
        chartOperatorComposite.setVerticalAxisTrace(false);
        
        Composite stateComposite =new Composite(this, SWT.NONE);
        stateComposite.setLayout(new GridLayout(2,true));
        
        //Combo to select the state
        cbState=new Combo(stateComposite, SWT.NONE);
        cbState.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
        cbState.setText(getLocalizedText("cbState"));
        cbState.addSelectionListener(new StateSelectionListener());
        cbState.setEnabled(false);
        cbState.add(getLocalizedText("stateAlive"));
        cbState.add(getLocalizedText("stateDead"));
        cbState.add(getLocalizedText("stateFail"));
        
        //Crate the button to export the state statistic
        btnStateExport=new Button(stateComposite,SWT.NONE);
        btnStateExport.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true));
        btnStateExport.setText(getLocalizedText("btnExport"));
        btnStateExport.addSelectionListener(new ExportListener(chartStateComposite));
        btnStateExport.setEnabled(false);
        
        Composite operatorComposite =new Composite(this, SWT.NONE);
        operatorComposite.setLayout(new GridLayout(2,true));
         
        //Combo to select the operator
        cbOperator=new Combo(operatorComposite, SWT.NONE);
        cbOperator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
        cbOperator.setText(getLocalizedText("cbOperator"));
        cbOperator.addSelectionListener(new MutantSelectionListener());
        cbOperator.setEnabled(false);
        
        //Crate the button to export the operator statistic
        btnOperatorExport=new Button(operatorComposite,SWT.NONE);
        btnOperatorExport.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true));
        btnOperatorExport.setText(getLocalizedText("btnExport"));
        btnOperatorExport.addSelectionListener(new ExportListener(chartOperatorComposite));
        btnOperatorExport.setEnabled(false);

	}
	
	/**
	 * Method to calculate the percentage by operator
	 * @param values Vector to save the percentages
	 * @param start Starting position of the operator on the vector of results
	 * @param end Ending position of the operator on the vector of results
	 */
	void getPercentsByOperator(double[] values, int start, int end){
		for(int i=start; i<end;i++){
			ComparisonResults aux=alExecutionMatrix.get(i);
			int add=0;
			//Sum of the results of a mutation
			for(int j=0; j<aux.getRow().length;j++){
				add+=aux.getRow()[j].isDead() ? 1 : 0;
			}
			//The mutant is alive
			if(add==0){
				values[0]++;
			}
			//The mutant is fail
			else if(add==aux.getRow().length*2){
				values[1]++;
			}
			//The mutant is dead
			else{
				values[2]++;
			}
		}
		//Create the percentages
		values[0]=values[0]*100/(end-start);
		values[1]=values[1]*100/(end-start);
		values[2]=values[2]*100/(end-start);
	}
	
	/**
	 * Method to update the statistics when we make a mutation
	 * @param alExecutionMatrix Results of the mutation
	 * @param mutantsForOperator Number of mutants generated by each operator
	 */
	void updatePies(final List<ComparisonResults> alExecutionMatrix, Map<String, Integer> mutantsForOperator){
		
		double globalValues[]={0,0,0};
		//Save the values
		this.alExecutionMatrix=alExecutionMatrix;
		this.mutantsForOperator=mutantsForOperator;

		//Enable the combos and export buttons
		cbOperator.setEnabled(true);
		cbState.setEnabled(true);
		btnGlobalExport.setEnabled(true);
		btnOperatorExport.setEnabled(true);
		btnStateExport.setEnabled(true);
		btnMutantForOperatorExport.setEnabled(true);
		
		//Calculate the percentages
		getPercentsByOperator(globalValues,0, alExecutionMatrix.size());
		DefaultPieDataset dpdGlobal = new DefaultPieDataset();
		
		//Create the global statistic
		dpdGlobal.setValue(getLocalizedText("stateAlive")+": "+ formatter.format(globalValues[0])+"%",globalValues[0]);
		dpdGlobal.setValue(getLocalizedText("stateDead")+": "+ formatter.format(globalValues[2])+"%", globalValues[2]);
		dpdGlobal.setValue(getLocalizedText("stateFail")+": "+ formatter.format(globalValues[1])+"%", globalValues[1]);
		
		JFreeChart jfcGlobalPie = ChartFactory.createPieChart3D(
			getLocalizedText("globalComposite"),  // chart title
            dpdGlobal,             // data
            true,               // include legend
            true,
            false
        );
		PiePlot ppGlobal = (PiePlot) jfcGlobalPie.getPlot();
        ppGlobal.setDirection(Rotation.CLOCKWISE);

        JFreeChart jfcGlobal = new JFreeChart(getLocalizedText("globalComposite"), null, ppGlobal, true); 
        
        chartGlobalComposite.setChart(jfcGlobal);
       /* chartGlobalComposite.forceRedraw();
        chartGlobalComposite.setRedraw(true);
        chartGlobalComposite.setSize(width, height)*/
        
   
        
        //Clear the operator statistic
        chartOperatorComposite.setChart(jfcClean);
        chartOperatorComposite.forceRedraw();
        
        //Clear the state composite
        chartStateComposite.setChart(jfcClean);
        chartStateComposite.forceRedraw();
        
        //Clear the operator combo
        cbOperator.removeAll();
        cbOperator.setText(getLocalizedText("cbOperator"));
        
        cbState.setText(getLocalizedText("cbState"));
        
        //Add the new operators to the combo
        for(String s:mutantsForOperator.keySet()){
        	cbOperator.add(s);
        }
        
        //Create the mutant statistic
        DefaultCategoryDataset dcdMutantForOperator = new DefaultCategoryDataset();
        for(String operator:mutantsForOperator.keySet()){
        	dcdMutantForOperator.setValue(mutantsForOperator.get(operator), operator, "");
        }
        
        JFreeChart jfcMutantForOperator = ChartFactory.createBarChart3D(
        		getLocalizedText("mutantsForOperator"),
                getLocalizedText("operator"),
                getLocalizedText("mutants"),
                dcdMutantForOperator,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
            );
        
		chartMutantForOperator.setChart(jfcMutantForOperator);
	}
	
	/**
	 * Method that return the internationalized name associated with a string
	 * @param stringID String to be internationalized
	 */
	private String getLocalizedText(final String stringID) {
		return L10N_BUNDLE.getString(stringID);
	}
}

