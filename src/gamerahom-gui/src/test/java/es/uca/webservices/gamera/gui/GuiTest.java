package es.uca.webservices.gamera.gui;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.utils.SWTBotPreferences;
import org.eclipse.swtbot.swt.finder.waits.Conditions;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotCombo;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotList;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTable;
import org.junit.Test;

import es.uca.webservices.gamera.gui.MainDialog;

public class GuiTest extends IsolatedShellTest {

	@Override
	protected Shell createShell() {
		MainDialog window =new MainDialog();
		Shell shell=null;
		try {
			shell= window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return shell;
	}
	
	public void configure(String xml, String bpts, String bpel){
		SWTBotCombo selectImpl=bot.comboBox();
		selectImpl.setSelection(0);
		SWTBotTable table=bot.table();
        table.getTableItem(0).click();
        bot.text(0).setText(xml);
        table.getTableItem(1).click();
        bot.text(1).setText(bpts);
        table.getTableItem(2).click();
        bot.text(2).setText(bpel);
        bot.button(0).click();
	}
	
	public void mutateAndCompare(String operator){
		SWTBotList lstAvailable=bot.list(0);
		lstAvailable.select(operator);
		bot.button(1).click();
		bot.button(4).click();
	}

	@Test
	/**
	 * Test to check if MuBPEL detect invalid test files
	 */
	public void testErroneous() {
		SWTBotPreferences.PLAYBACK_DELAY = 100;
		SWTBotPreferences.TIMEOUT=1000000000;
		//Load the files
		configure("src/test/resources/aux.xml", 
				"src/test/resources/TestFail.bpts", 
				"src/test/resources/LoanApprovalProcess.bpel");
        bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(1)));
        
        //Check if MuBPEL has analyzed the program
		assertEquals(true, bot.tabItem(1).isActive());
	}
	
	@Test
	/**
	 * Test to check if MuBPEL detects invalid programs
	 */
	public void programErroneous() {
		SWTBotPreferences.PLAYBACK_DELAY = 100;
		SWTBotPreferences.TIMEOUT=1000000000;
		//Load the files
		configure("src/test/resources/aux.xml", 
				"src/test/resources/LoanApprovalProcess-Velocity.bpts", 
				"src/test/resources/ProgramFail.bpel");
        bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(1)));
        
        //Check if MuBPEL has analyzed the program
		assertEquals(true, bot.tabItem(1).isActive());
		
	}
	
	@Test
	/**
	 * Test to check the normal execution, this test will configure MuBPEL and generate a mutant
	 */
	public void normalExecution() {
		SWTBotPreferences.PLAYBACK_DELAY = 100;
		//Load the files
		configure("src/test/resources/aux.xml", 
				"src/test/resources/LoanApprovalProcess-Velocity.bpts", 
				"src/test/resources/LoanApprovalProcess.bpel");
        
        SWTBotPreferences.TIMEOUT=1000000000;

        //Wait until MuBPEL analyzes the program
        bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(2)));
		assertEquals(true, bot.tabItem(2).isActive());
		
		
		//Generate a mutant
		mutateAndCompare("AIE");
		
		//Wait until the mutant has been created and compared
		bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(3)));
		assertEquals(true, bot.tabItem(3).isActive());
		
		//Check the result
		SWTBotTable table=bot.table();
		String firstMutant="";
		String secondMutant="";
		for(int i=0; i<table.columnCount();i++){
			firstMutant+=table.getTableItem(0).getText(i)+" ";
			secondMutant+=table.getTableItem(1).getText(i)+" ";
		}
		assertEquals("AIE 1 1 0 0 1 0 0 1 0 0 0", firstMutant.trim());
		assertEquals("AIE 2 1 1 1 0 0 0 0 0 0 0", secondMutant.trim());
	}
	
	@Test
	/**
	 * Test to check two consecutive analysis
	 */
	public void consecutiveAnalysis() {
		SWTBotPreferences.PLAYBACK_DELAY = 100;
		SWTBotPreferences.TIMEOUT=1000000000;
		
		//Load the files
		configure("src/test/resources/aux.xml", 
				"src/test/resources/LoanApprovalProcess-Velocity.bpts", 
				"src/test/resources/LoanApprovalProcess.bpel");
		
		//Wait until MuBPEL analyzes the program
        bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(1)));
		assertEquals(true, bot.tabItem(2).isActive());
		bot.tabItem(1).activate();
		
		//Analyze again
		bot.button(0).click();
		
		//Wait until MuBPEL analyzes the program
		bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(1)));
		assertEquals(true, bot.tabItem(2).isActive());
	}
	
	
	
	@Test
	/**
	 * Test to check two consecutive mutations
	 */
	public void consecutiveMutation() {
		SWTBotPreferences.PLAYBACK_DELAY = 100;
		//Load the files
		configure("src/test/resources/aux.xml",
				"src/test/resources/LoanApprovalProcess-Velocity.bpts", 
				"src/test/resources/LoanApprovalProcess.bpel");
        
        SWTBotPreferences.TIMEOUT=1000000000;

        //Wait until MuBPEL analyzes the program
        bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(1)));
		assertTrue("The second tab should be active", bot.tabItem(2).isActive());
		
		//Generate a mutant
		mutateAndCompare("ERR");
		
		//Wait until the mutant has been created and compared
		bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(1)));
		assertTrue("The third tab should be active", bot.tabItem(3).isActive());
		
		bot.tabItem(2).activate();
		
		//Generate a mutant
		mutateAndCompare("AIE");
		//Wait until the mutant has been created and compared
		bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(1)));
		assertTrue("The third tab should be still active", bot.tabItem(3).isActive());
		
		//Check the results
		SWTBotTable table=bot.table();
		String firstMutant="";
		String secondMutant="";
		for(int i=0; i<table.columnCount();i++){
			firstMutant+=table.getTableItem(0).getText(i)+" ";
			secondMutant+=table.getTableItem(1).getText(i)+" ";
		}
		assertEquals("AIE 1 1 0 0 1 0 0 1 0 0 0", firstMutant.trim());
		assertEquals("AIE 2 1 1 1 0 0 0 0 0 0 0", secondMutant.trim());
	}
	
	@Test
	/**
	 * Test to check the buttons
	 */
	public void addErase() {
		SWTBotPreferences.PLAYBACK_DELAY = 100;
		//Load the files
		configure("src/test/resources/aux.xml", 
				"src/test/resources/LoanApprovalProcess-Velocity.bpts", 
				"src/test/resources/LoanApprovalProcess.bpel");
		
		SWTBotPreferences.TIMEOUT=1000000000;

		//Wait until MuBPEL analyzes the program
		bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(1)));
		assertTrue("The second tab should be selected", bot.tabItem(2).isActive());
		
		int operators=bot.list(0).getItems().length;
		
		//Add all operators
		bot.button(0).click();
		assertEquals(0, bot.list(0).getItems().length);
		assertEquals(operators, bot.list(1).getItems().length);

		//Erase an operator
		String opName=bot.list(1).itemAt(0);
		bot.list(1).select(0);
		bot.button(2).click();
		assertEquals(opName, bot.list(0).itemAt(0));

		//Erase all operators
		bot.button(3).click();
		assertEquals(0, bot.list(1).getItems().length);
		assertEquals(operators, bot.list(0).getItems().length);
	}

	@Test
	/**
	 * Test to check if the mutate files has been loaded to compare
	 */
	public void loadFileDifferences() throws Exception {
		SWTBotPreferences.PLAYBACK_DELAY = 100;
		//Load the files
		configure("src/test/resources/aux.xml", 
				"src/test/resources/LoanApprovalProcess-Velocity.bpts", 
				"src/test/resources/LoanApprovalProcess.bpel");
		
		SWTBotPreferences.TIMEOUT=1000000000;

		//Wait until MuBPEL analyzes the program
        bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(1)));
		assertTrue("The second tab should be selected", bot.tabItem(2).isActive());
		
		//Generate a mutant
		mutateAndCompare("AIE");
		
		//Wait until the mutant has been created and compared
		bot.waitUntil(Conditions.widgetIsEnabled(bot.tabItem(01)));
		assertEquals(true, bot.tabItem(3).isActive());
		
		bot.tabItem(5).activate();
		assertEquals("", bot.text(1).getText());
		
		//Select a mutant
		bot.comboBox(0).setSelection(0);
		
		//Check if the file has been loaded
		assertEquals(true, bot.spinner(0).isEnabled());
		assertEquals(true, bot.spinner(1).isEnabled());
	}
	
	
}
