package es.uca.webservices.bpel.inv;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import daikon.Daikon;
import daikon.FileIO;
import daikon.Global;
import daikon.PptConditional;
import daikon.PptMap;
import daikon.PptTopLevel;
import daikon.VarInfo;
import daikon.inv.Equality;
import daikon.inv.Implication;
import daikon.inv.Invariant;
import daikon.inv.OutputFormat;
import daikon.inv.filter.InvariantFilter;
import daikon.inv.filter.InvariantFilters;
import daikon.split.PptSplitter;
import daikon.util.UtilMDE;

/**
 * This class does the real work, printing the invariants in XML format. It is
 * based in the code of PrintInvariants, from Daikon.
 *
 * @author Alejandro Álvarez-Ayllón, Antonio García-Domínguez
 * @version 0.2
 */
public final class XMLFromInv {

    private PptMap             all_ppts;
    private OutputStream outStream;

	/**
	 * Constructor
	 *
	 * @param path
	 *            The path to the .inv file
	 */
    public XMLFromInv(String path) throws IOException
    {
        this(new File(path));
    }

	/**
	 * Constructor
	 *
	 * @param file
	 *            The .inv file
	 */
    public XMLFromInv(File file) throws IOException {
    	loadInv(file);
	}

	/** Prints the XML generated content
     * @param out The output stream where the output must be written
     */
    public void dumpXML(OutputStream out)
    {
        outStream = out;
        print_invariants();
    }

  /**
   * This option must be given with "--format Java" option.
   *
   * Instead of outputting prestate expressions as "\old(E)" within an
   * invariant, output a variable names (e.g. `v1'). At the end of
   * each program point, output the list of variable-to-expression
   * mappings. For example: with this option set to false, a program
   * point might print like this:
   *
   * <pre>
   * foo.bar.Bar(int):::EXIT
   * \old(capacity) == sizeof(this.theArray)
   * </pre>
   *
   * With the option set to true, it would print like this:
   *
   * <pre>
   * foo.bar.Bar(int):::EXIT
   * v0 == sizeof(this.theArray)
   * prestate assignment: v0=capacity
   * </pre>
   */
  public static boolean dkconfig_replace_prestate = true;

  /** If true, print all invariants without any filtering.  **/
  public static boolean dkconfig_print_all = false;

  /**
   * In the new decl format print array names without as 'a[]' as
   * opposed to 'a[..]'  This creates names that are more compatible
   * with the old output.  This option has no effect in the old decl
   * format
   */
  public static boolean dkconfig_old_array_names = true;

  /**
   * Main debug tracer for PrintInvariants (for things unrelated to printing).
   **/
  public static final Logger debug = Logger.getLogger("daikon.PrintInvariants");

  /** Debug tracer for printing. **/
  public static final Logger debugRepr
    = Logger.getLogger("daikon.PrintInvariants.repr");

  /** Debug tracer for printing. **/
  public static final Logger debugPrint = Logger.getLogger("daikon.print");

  /** Debug tracer for printing modified variables in ESC/JML/DBC output. **/
  public static final Logger debugPrintModified
    = Logger.getLogger("daikon.print.modified");

  /** Debug tracer for printing equality. **/
  public static final Logger debugPrintEquality
    = Logger.getLogger("daikon.print.equality");

  /** Debug tracer for filtering. **/
  public static final Logger debugFiltering
    = Logger.getLogger("daikon.filtering");

  /** Debug tracer for variable bound information. **/
  public static final Logger debugBound  = Logger.getLogger ("daikon.bound");

  /** Regular expression that ppts must match to be printed **/
  private static Pattern ppt_regexp;

  /**
   * Switch for whether to print discarded Invariants or not, default is false.
   * Activated by --disc_reason switch.
   **/
  public static boolean print_discarded_invariants = false;

  // Avoid problems if daikon.Runtime is loaded at analysis (rather than
  // test-run) time.  This might have to change when JTrace is used.
  static { daikon.Runtime.no_dtrace = true; }


    /**
    * Load a .inv file
    *  @path The path to the .inv file
    **/
    public void loadInv(File file) throws IOException
    {
        // Read in the invariants
        all_ppts = FileIO.read_serialized_pptmap(file, false);

        // Setup the list of prototype invariants and initialize NIS suppressions
        Daikon.setup_proto_invs();
        Daikon.setup_NISuppression();

        // Make sure ppts' rep invariants hold
        all_ppts.repCheck();

        validateGuardNulls();
  }

    // To avoid the leading "UtilMDE." on all calls.
    private String nplural(int n, String noun) {
        return UtilMDE.nplural(n, noun);
    }


    /** Validate guardNulls config option. **/
    public void validateGuardNulls()
    {
        Daikon.dkconfig_guardNulls = Daikon.dkconfig_guardNulls.intern();
        // Complicated default!
        if (Daikon.dkconfig_guardNulls.equals("default"))
        { // interned
          if (Daikon.output_format == OutputFormat.JML
              || Daikon.output_format == OutputFormat.ESCJAVA) {
            Daikon.dkconfig_guardNulls = "missing";
          } else {
            Daikon.dkconfig_guardNulls = "never";
          }
        }
        if (! ((Daikon.dkconfig_guardNulls.equals("always"))// interned
               || (Daikon.dkconfig_guardNulls.equals("never")) // interned
               || (Daikon.dkconfig_guardNulls.equals("missing"))) // interned
            ) {
          throw new Error("Bad guardNulls config option \"" + Daikon.dkconfig_guardNulls + "\", should be one of \"always\", \"never\", or \"missing\"");
        }
    }

  // The following code is a little odd because it is trying to match the
  // output format of V2.  In V2, combined exit points are printed after
  // the original exit points (rather than before as they are following
  // the PptMap sort order).
  //
  // Also, V2 only prints out a single ppt when there is only one
  // exit point.  This seems correct.  Probably a better solution to
  // this would be to not create the combined exit point at all when there
  // is only a single exit.  Its done here instead so as not to futz with
  // the partial order stuff.
  //
  // All of this can (and should be) improved when V2 is dropped.

  public void print_invariants() {

    PrintWriter  pw                          = new PrintWriter(outStream, true);
    PptTopLevel combined_exit     = null;
    boolean         enable_exit_swap = true; // !Daikon.dkconfig_df_bottom_up;

    // Print root element
    pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    pw.println("<invariants xmlns=\"http://www.uca.es/invariants/2009\">");

    // Retrieve Ppt objects in sorted order.  Put them in an array list
    // so that it is easier to look behind and ahead.
    PptTopLevel[] ppts = new PptTopLevel [all_ppts.size()];
    int ii = 0;
    for (Iterator<PptTopLevel> itor = all_ppts.pptIterator(); itor.hasNext();)
        ppts[ii++] = itor.next();

    for (int i = 0 ; i < ppts.length; i++) {
        PptTopLevel ppt = ppts[i];

        // If this point is not an exit point, print out any retained combined
        // exit point
        if (enable_exit_swap && !ppt.ppt_name.isExitPoint()) {
            if (combined_exit != null)
                print_invariants_maybe(combined_exit, pw);
            combined_exit = null;
        }

      // Just cache the combined exit point for now, print it after the
      // EXITnn points.
      if (enable_exit_swap && ppt.ppt_name.isCombinedExitPoint()) {
        combined_exit = ppt;
        continue;
      }

      // If there is only one exit point, just show the combined one (since
      // the EXITnn point will be empty)  This is accomplished by skipping this
      // point if it is an EXITnn point and the previous point was a combined
      // exit point and the next one is not an EXITnn point.  But don't skip
      // any conditional ppts attached to the skipped ppt.
      if (enable_exit_swap && (i > 0) && ppt.ppt_name.isExitPoint()) {
            if (ppts[i-1].ppt_name.isCombinedExitPoint()) {
                if (((i + 1) >= ppts.length) || !ppts[i+1].ppt_name.isExitPoint())
                    continue;
            }
      }

      print_invariants_maybe(ppt, pw);
    }

    // print a last remaining combined exit point (if any)
    if (enable_exit_swap && combined_exit != null)
      print_invariants_maybe(combined_exit, pw);

    // Close root element
    pw.println("</invariants>");
    pw.flush();
  }

  /**
   * Print invariants for a single program point and its conditionals.
   * Does no output if no samples or no views.
   **/
  public void print_invariants_maybe(PptTopLevel ppt,
                                            PrintWriter out) {

    debugPrint.fine  ("Considering printing ppt " + ppt.name()
                      + ", samples = " + ppt.num_samples());

    // Skip this ppt if it doesn't match ppt regular expression
    if ((ppt_regexp != null) && !ppt_regexp.matcher(ppt.name()).find())
      return;

    // Be silent if we never saw any samples.
    // (Maybe this test isn't even necessary, but will be subsumed by others,
    // as all the invariants will be unjustified.)
    if (ppt.num_samples() == 0)
        return;

    if ((ppt.numViews() == 0) && (ppt.joiner_view.invs.size() == 0)) {
      if (! (ppt instanceof PptConditional))
        return;
    }

    print_invariants(ppt, out, all_ppts);

    if (Daikon.dkconfig_output_conditionals
        && Daikon.output_format == OutputFormat.DAIKON) {
      for (Iterator<PptConditional> j = ppt.cond_iterator(); j.hasNext() ; ) {
        PptConditional pcond = j.next();
        print_invariants_maybe(pcond, out);
      }
    }
  }


   /**
    * If Daikon.output_num_samples is enabled, prints the number of samples
    * for the specified ppt.  Also prints all of the variables for the ppt
    * if Daikon.output_num_samples is enabled or the format is ESCJAVA,
    * JML, or DBCJAVA
    */
    public void print_sample_data(PptTopLevel ppt, PrintWriter out)
    {
        // ENTER
        out.println("\t<point name=\"" + ppt.name() + "\" samples=\"" + ppt.num_samples() + "\">");
    }

  /** Count statistics (via Global) on variables (canonical, missing, etc.) **/
  public static void count_global_stats(PptTopLevel ppt) {
    for (int i=0; i<ppt.var_infos.length; i++) {
      if (ppt.var_infos[i].isDerived()) {
        Global.derived_variables++;
      }
    }
  }

  /** Prints the specified invariant to out. **/
  public void print_invariant(Invariant inv, PrintWriter out,
                                     int invCounter, PptTopLevel ppt) {
    int inv_num_samps = inv.ppt.num_samples();
    String num_values_samples = "\t\t(" + nplural(inv_num_samps, "sample") + ")";

    String inv_rep;

    // All this should turn into simply a call to format_using.
    inv_rep = inv.format_using(OutputFormat.DAIKON);

    if (Daikon.output_num_samples) {
      inv_rep += num_values_samples;
    }

    if (debugRepr.isLoggable(Level.FINE)) {
      debugRepr.fine ("Printing: [" + inv.repr_prob() + "]");
    } else if (debugPrint.isLoggable(Level.FINE)) {
      debugPrint.fine ("Printing: [" + inv.repr_prob() + "]");
    }

    if (dkconfig_old_array_names && FileIO.new_decl_format)
      inv_rep = inv_rep.replace ("[..]", "[]");


      out.print("\t\t<inv>");
      out.print(inv_rep);
      out.println("</inv> ");

    if (debug.isLoggable(Level.FINE)) {
      debug.fine (inv.repr());
    }

  }

  /**
   * Print invariants for a single program point, once we know that
   * this ppt is worth printing.
   **/
  public void print_invariants(PptTopLevel ppt, PrintWriter out,
                                      PptMap ppt_map) {


    // make names easier to read before printing
    ppt.simplify_variable_names();

    print_sample_data(ppt, out);

    // Dump some debugging info, if enabled
    if (debugPrint.isLoggable(Level.FINE)) {
      debugPrint.fine ("Variables for ppt "  + ppt.name());
      for (int i=0; i<ppt.var_infos.length; i++) {
        VarInfo vi = ppt.var_infos[i];
        debugPrint.fine ("      " + vi.name());
      }
      debugPrint.fine ("Equality set: ");
      debugPrint.fine ((ppt.equality_view == null) ? "null"
                       : ppt.equality_view.toString());
    }
    if (debugFiltering.isLoggable(Level.FINE)) {
      debugFiltering.fine ("----------------------------------------"
        + "--------------------------------------------------------");
      debugFiltering.fine (ppt.name());
    }

    // Count statistics (via Global) on variables (canonical, missing, etc.)
    count_global_stats(ppt);

    // I could instead sort the PptSlice objects, then sort the invariants
    // in each PptSlice.  That would be more efficient, but this is
    // probably not a bottleneck anyway.
    List<Invariant> invs_vector = new LinkedList<Invariant>(ppt.getInvariants());

    if (PptSplitter.debug.isLoggable (Level.FINE)) {
      PptSplitter.debug.fine ("Joiner View for ppt " + ppt.name);
      for (Invariant inv : ppt.joiner_view.invs) {
        PptSplitter.debug.fine ("-- " + inv.format());
      }
    }

    if (debugBound.isLoggable (Level.FINE))
      ppt.debug_unary_info (debugBound);

    Invariant[] invs_array = invs_vector.toArray(
      new Invariant[invs_vector.size()]);
    Arrays.sort(invs_array, PptTopLevel.icfp);

    Global.non_falsified_invariants += invs_array.length;

    List<Invariant> accepted_invariants = new Vector<Invariant>();

    for (int i = 0; i < invs_array.length; i++) {
      Invariant inv = invs_array[i];

      if (Invariant.logOn())
        inv.log ("Considering Printing");
      assert !(inv instanceof Equality);
      for (int j = 0; j < inv.ppt.var_infos.length; j++)
        assert !inv.ppt.var_infos[j].missingOutOfBounds()
        : "var '" + inv.ppt.var_infos[j].name()
                            + "' out of bounds in " + inv.format();
      InvariantFilters fi = InvariantFilters.defaultFilters();

      boolean fi_accepted = true;
      InvariantFilter filter_result = null;
      if (!dkconfig_print_all) {
        filter_result = fi.shouldKeep (inv);
        fi_accepted = (filter_result == null);
      }

      if ((inv instanceof Implication)
          && PptSplitter.debug.isLoggable(Level.FINE))
        PptSplitter.debug.fine ("filter result = " + filter_result
                                + " for inv " + inv);

      if (Invariant.logOn())
        inv.log ("Filtering, accepted = " + fi_accepted);

      // Never print the guarding predicates themselves, they should only
      // print as part of GuardingImplications
      if (fi_accepted && !inv.isGuardingPredicate) {
        Global.reported_invariants++;
        accepted_invariants.add(inv);
      } else {
        if (Invariant.logOn() || debugPrint.isLoggable(Level.FINE)) {
          inv.log (debugPrint, "fi_accepted = " + fi_accepted +
                    " inv.isGuardingPredicate = " + inv.isGuardingPredicate
                    + " not printing " + inv.repr());
        }
      }
    }

    accepted_invariants
      = InvariantFilters.addEqualityInvariants(accepted_invariants);

    if (debugFiltering.isLoggable(Level.FINE)) {
      for (Invariant current_inv : accepted_invariants) {
        if (current_inv instanceof Equality) {
          debugFiltering.fine ("Found Equality that says "
                                + current_inv.format());
        }
      }
    }

    finally_print_the_invariants(accepted_invariants, out, ppt);

    // Close element
    out.println("\t</point>");
  }

  /**
   * Does the actual printing of the invariants.
   **/
  private void finally_print_the_invariants(List<Invariant> invariants,
                                                   PrintWriter out,
                                                   PptTopLevel ppt) {
    //System.out.printf ("Ppt %s%n", ppt.name());
    //for (VarInfo vi : ppt.var_infos)
    // System.out.printf ("  var %s canbemissing = %b%n", vi, vi.canBeMissing);

    int index = 0;
    for (Invariant inv : invariants) {
      index++;
      Invariant guarded = inv.createGuardedInvariant(false);
      if (guarded != null) {
        inv = guarded;
      }
      print_invariant(inv, out, index, ppt);
    }
  }
  
}
