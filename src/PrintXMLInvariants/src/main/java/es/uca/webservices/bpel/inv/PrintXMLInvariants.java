package es.uca.webservices.bpel.inv;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * This class just provides the main method, so this can be used
 * from command line
 * @author Alejandro Álvarez Ayllón
 */
final public class PrintXMLInvariants {

    /**
     * This method is called when the tools is called from command line
     * @param args The arguments passed as parameters
     */
    public static void main(String [] args)
    {
        // Show the help if the arguments are wrong or
        // if the argument is -h or --help
        if(args.length == 0) {
            System.out.println("Daikon invariant converter");
            System.out.println("Usage:");
            System.out.println("java es.uca.webservices.bpel.inv.PrintXMLInvariants .inv [.out]");
            System.out.println("\t.inv\t\tThe invariants (.inv) file produced by Daikon. It can be compressed with gzip.");
            System.out.println("\t.out\t\tThe file where the output must be written. If it is not specified, it will be the standard output");
            System.exit(0);
        }

        // Show a message
        System.err.println("Reading invariants from file " + args[0]);

        // Load invariants
        XMLFromInv fromInv = null;
        try {
            fromInv = new XMLFromInv(args[0]);
        } catch (IOException ex) {
            System.err.println("ERROR: " + args[0] + " could not be loaded");
            ex.printStackTrace();
            System.exit(-1);
        }

        // About output?
        OutputStream output = null;
        if(args.length == 1) {
            output = System.out;
            System.err.println("Writing XML to standard output");
        } else {
            try {
                output = new FileOutputStream(args[1]);
                System.err.println("Writing XML to file " + args[1]);
            } catch (FileNotFoundException ex) {
                System.err.println("ERROR: " + args[1] + " could not be opened (" + ex.toString() + ")");
                System.exit(-1);
            }
        }

        // Dumping
        fromInv.dumpXML(output);

        // End
        System.err.println("Done");
        if(output != System.out) {
            try {
                output.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }

}
