package es.uca.mucpp.gamerahom.exec;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionType;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.GAProgressMonitor;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.api.util.Pair;


/**
 * <p>Executor for MuCPP
 *
 * @author Pedro Delgado-Pérez
 */

public class MuCppExecutor implements GAExecutor {
	
	// From mutant basename (as it may have been run in a remote machine) to GA individual
	private final Map<String, GAIndividual> mapBasenameToIndividual = new HashMap<String, GAIndividual>();
	// From GA individual to locally generated file
	private final Map<GAIndividual, File> mapIndividualToFile = new HashMap<GAIndividual, File>();

	//private File testSuite;
	private File[] originalPrograms;
	private File testExecution=null;
	private File compareResults = null;
	
	private String mutationDirectory=null;
	private String testDirectory=null;
	
	//Number of mutants per operator and file: rows = operator; columns = file
	private BigInteger[][] subOpCounts;
	private Map<BigInteger, String> mapOpCode = new HashMap<BigInteger, String>();
	
	private boolean runExecuted = false;
	private boolean JSONcreated;
	private File fileMutants = null;
	
	private int subOpCountsIndex;

	@Override
	public void validate() throws InvalidConfigurationException {
		
		// check the configuration of this executor
/*		if (getTestSuiteFile() == null) {
			throw new InvalidConfigurationException("The path test suite file must be specified");
		}			*/
		if (getOriginalProgramsFiles().length == 0) {
			throw new InvalidConfigurationException("The path to the original programs must be specified");
		}
		
		if(getTestDirectory() == null){
			throw new InvalidConfigurationException("The path to the test directory must be specified");
		}
		
		File testDirectoryFile = new File(getTestDirectory());
		if(!testDirectoryFile.exists())
			throw new InvalidConfigurationException("The test directory " + getTestDirectory() + " does not exist");
		else
			setTestDirectory(normalizeDirectory(getTestDirectory()));
		
		File mutationDirectoryFile = new File(getMutationDirectory());
		if(!mutationDirectoryFile.exists())
			throw new InvalidConfigurationException("The mutation directory " + getMutationDirectory() + " does not exist");
		else
			setMutationDirectory(normalizeDirectory(getMutationDirectory()));
		
	/*	if (!getTestSuiteFile().canRead()) {
			throw new InvalidConfigurationException("The file with the test suite at " + getTestSuite() + " is not readable");
		}*/
		
		for (int i = 0; i < getOriginalProgramsFiles().length; ++i) {
			File origP = new File(mutationDirectory + getOriginalProgramsFiles()[i].getPath());
			
			if(!origP.canRead())
				throw new InvalidConfigurationException("The file with the original program at " + origP.getPath() + " is not readable");
		}
	}

	@Override
	public void prepare() throws PreparationException {
		
	}

	@Override
	public AnalysisResults analyze(GAProgressMonitor monitor)
			throws AnalysisException {
		try{
			
			//Create the process to execute "analyze"
			List<String> args = new ArrayList<String>();
			args.add("mucpp"); args.add("analyze");
			for(File origP: originalPrograms){
				args.add(origP.getPath());
			}
			
			if(!isJSONcreated())
				args.add("--");
			
			// run "mucpp analyze ..." and process reports to produce AnalysisResults
			//	ProcessBuilder pb = new ProcessBuilder("mucpp", "--help");
			int result = launchProcess(args);
			
			if(result == 0){
			
				//Create array with the names of the analysis files
				File[] programsAndGlobal = new File[originalPrograms.length + 1];
				//Save the global file in the first position
				programsAndGlobal[0] = new File(mutationDirectory + "reports_analyze/analyze_mucpp_global.txt");
				
				//Save the rest of files
				String nameAnalysisFile = null;
				for (int i = 0; i < originalPrograms.length; i++) {
					nameAnalysisFile = mutationDirectory + "reports_analyze/analyze_" + normalizeNameFile(originalPrograms[i].getName()) + ".txt";
					programsAndGlobal[i+1] = new File(nameAnalysisFile);
				}
				
				//Read the results
			    FileReader fr = null;
			    BufferedReader br = null;
			    ArrayList<String> lines = new ArrayList<String>();
			    String delimiter = " ";
			    
				int nOperators = -1;
				
				String[] opNames = null;
				BigInteger[] opCounts = null;
				BigInteger[] attrRanges = null;
				int iFile = 0;
				
			    for(File origP: programsAndGlobal){
				    try {
				    	fr = new FileReader(origP);
				    	br = new BufferedReader(fr);
				    	
				    	String line = null;
				    	String[] subLines = null;
				    	
				    	//Read the global analysis
				    	if(nOperators == -1){
				    		
					    	lines.clear();
					    	while( (line = br.readLine()) != null){
					            lines.add(line);
					    	}	
					    	
					    	//Assign the number of operators to initialize arrays
					    	nOperators = lines.size();
					    
							opNames = new String[nOperators];
							opCounts = new BigInteger[nOperators];
							attrRanges = new BigInteger[nOperators];
				    	
							for(int iOperator=0; iOperator < lines.size(); iOperator++){
								//Take each line and split with " "
								subLines = lines.get(iOperator).split(delimiter);
								opNames[iOperator] = subLines[0];
								opCounts[iOperator] = new BigInteger(subLines[1]);
								attrRanges[iOperator] = new BigInteger(subLines[2]);
								
								//Create the list of code-operator
								mapOpCode.put(new BigInteger(Integer.toString(iOperator+1)), subLines[0]);
							}
							//Initialize supOpCounts (if initialized in else, it would be initialized several times)
							subOpCounts = new BigInteger[nOperators][originalPrograms.length];
			    		}
			    		else{
			    			//For the rest of reports, only take the locations and save them in subOpCounts
			    			int iOperator = 0;
			    			while( (line = br.readLine()) != null){
			    				subLines = line.split(delimiter);
			    				subOpCounts[iOperator][iFile] = new BigInteger(subLines[1]);
			    				iOperator++;
			    			}
			    			iFile++;
			    		}
				    }
				    catch(Exception e){
				    	throw new AnalysisException("Not possible to read the file " + origP.getPath());
				    }finally{
				    	try{                   
				    		if(fr !=  null){  
				    			fr.close();    
				    		}                 
						}catch (Exception e){
							throw new AnalysisException("Not possible to close the file " + origP.getPath());
						}
				    }
			    }
			
				return new AnalysisResults(opNames, opCounts, attrRanges);
			}
			else{
				throw new AnalysisException("Program exited with: " + result);
			}
		} catch (Exception e) {
			throw new AnalysisException(e);
		}
	}

	@Override
	public ComparisonResults[] compare(GAProgressMonitor monitor,
			GAIndividual... individuals) throws ComparisonException {
		try{
			
			final List<ComparisonResults> lComparisons = new ArrayList<ComparisonResults>();
				
			//Create the process to execute "compare"
			List<String> args = new ArrayList<String>();
			args.add("mucpp"); args.add("compare");
			args.add(testDirectory);
			String nameBranch = null;
			//String code = null;bu
			//String nameFileInd = null;
			for(GAIndividual individual : individuals){
				
				//First, generate the mutants
				nameBranch = getMutantFile(individual).toString();
				//nameFileInd = determineFileGAIndividual(individual).getName();
				//code = normalizeCode(individual.getThOperator(1));
				//nameBranch = "m" + code  + "_" + calculateRealLocation(individual).toString() + 
				//	"_" + individual.getThAttribute(1).toString() + "_" + normalizeNameFile(nameFileInd);
			
				args.add(nameBranch);
			}
			
			if(!isJSONcreated())
				args.add("--");
			
			//Second, run the original program
			if(!isRunExecuted()){
				runOriginalProgram(); ///XXX AQUÍ HABRÁ QUE: 1. CAMBIAR MUCPP PARA QUE NO EXIGA QUE SE CORRA EL PROGRAMA ORIGINAL DESPUÉS DE CREAR UN MUTANTE
									  ///                    2. GUARDAR LOS RESULTADOS DEL PROGRAMA ORIGINAL Y CREAR EL FICHERO CON LOS RESULTADOS
			}
			
			// run "mucpp compare mutants" and process reports
			int result = launchProcess(args);
			
			if(result == 0){
				
				//Read the results
			    FileReader fr = null;
			    BufferedReader br = null;
			    ArrayList<String> lines = new ArrayList<String>();
			    String delimiter = " ";
			   
			    List<Pair<String, Pair<int[], long[]>>> bkgResults = new ArrayList<Pair<String, Pair<int[], long[]>>>();
				
		    	String[] subLines = null;
		    	String line = null;
		    	int nTestCases = 0;
			    
			    try {
			    	//It is necessary to know the size of the test suite
			    	fr = new FileReader(testExecution);
			    	br = new BufferedReader(fr);

			    	while( (line = br.readLine()) != null){
			           nTestCases++;
			    	}
			    }	
		    	catch(Exception e){
			    	throw new ComparisonException("Not possible to read the file " + testExecution.getPath());
			    }finally{
			    	try{                   
			    		if(fr !=  null){  
			    			fr.close();    
			    		}                 
					}catch (Exception e){
						throw new ComparisonException("Not possible to close the file " + testExecution.getPath());
					}
			    }
			    	
			    try{
			    	fr = new FileReader(compareResults);
			    	br = new BufferedReader(fr);
			    	
			    	line = null;
			    	subLines = null;
			    	
			    	while( (line = br.readLine()) != null){
			            lines.add(line);
			    	}	
			    	
			    	//The number of lines should match the number of individuals (one line per mutant)
			    	if(lines.size() == individuals.length){
			    		
						for(int iResult=0; iResult < lines.size(); iResult++){
				    		String path = null;
				    		int[] comparisons = new int[nTestCases];
				    		long[] nanos = new long[nTestCases];
							
							//Take each line and split with " "
							subLines = lines.get(iResult).split(delimiter);
							path = subLines[0];
							int iTest = 1;
							while (iTest < subLines.length && !subLines[iTest].equals("T")){
								comparisons[iTest - 1] = Integer.parseInt(subLines[iTest]);
								iTest++;
							}
						
							//Si se da el caso de que un mutante termina su ejecución sin ejecutar todos los casos de 
							//prueba, debemos completar hasta el número de casos de prueba. Después es posible que te
							//venga la medición del tiempo de los casos de prueba que se ejecutaron. 
							int iTestAux = iTest;
							if(iTestAux < nTestCases){
								comparisons[iTestAux] = 0;
								iTestAux++;
							}
							
							int iTime = 0;
							iTest++;	//Saltar la T
							while (iTest < subLines.length){
								nanos[iTime] = Long.parseLong(subLines[iTest]);
								iTest++;
								iTime++;
							}
							
							int iTimeAux = iTime+1;
							if(iTimeAux < nTestCases){
								nanos[iTimeAux] = 0;
								iTimeAux++;
							}
							
							Pair<int[], long[]> results_times = new Pair<int[], long[]>(comparisons, nanos);
							bkgResults.add(new Pair<String, Pair<int[], long[]>>(path, results_times));
						}
						
						convertMatrixIntoResults(bkgResults, lComparisons);
			    	}
			    	else{
			    		throw new ComparisonException("Incomplete results");
			    	}
			    }
			    catch(Exception e){
			    	throw new ComparisonException("Not possible to read the file " + compareResults.getPath());
			    }finally{
			    	try{                   
			    		if(fr !=  null){  
			    			fr.close();    
			    		}                 
					}catch (Exception e){
						throw new ComparisonException("Not possible to close the file " + compareResults.getPath());
					}
			    }
			    
			    //NUEVO: Vamos a enviar los mutantes generados a un fichero externo; 
				PrintStream ps = new PrintStream(new File(fileMutants.toString()));
			    ps.flush();
				for (String key : mapBasenameToIndividual.keySet()) {
					ps.println(key);
				}
			    
				ps.close();
				
				return lComparisons.toArray(new ComparisonResults[lComparisons.size()]);
			}
			else{
				throw new ComparisonException("Program exited with: " + result);
			}
			
		} catch (Exception ex) {
			throw new ComparisonException(ex);
		}
	}

	private void runOriginalProgram() throws Exception {
		try{
			//Create the process to execute "run"
			List<String> args = new ArrayList<String>();
			args.add("mucpp"); args.add("run");
			args.add(testDirectory);
			
			if(!isJSONcreated())
				args.add("--");
			
			// run "mucpp run testDirectory"
			int result = launchProcess(args);
			
			if(result != 0){
				throw new Exception("Program exited with: " + result);
			}
			else
				//RunExecuted = true so that the original program is not executed any more
				setRunExecuted(true);
			
		}catch(Exception e){
			throw new Exception(e);
		}
	}
	
	@Override
	public File getMutantFile(GAIndividual individual)
			throws UnsupportedOperationException, GenerationException {
		return generate(individual);
	}

	public File generate(GAIndividual individual) throws GenerationException {
		try{
			//Check if the mutant was previously generated
			if (mapIndividualToFile.containsKey(individual)) {
				final File cachedFile = mapIndividualToFile.get(individual);
				return cachedFile;
			}
			
			//Create the process to execute "apply"
			File fileIndividual = determineFileGAIndividual(individual); 
			
			List<String> args = new ArrayList<String>();
			args.add("mucpp"); args.add("apply");
			args.add(getMapOpCode().get(individual.getThOperator(1)).toString()); 
			args.add(individual.getThLocation(1).toString()); 
			args.add(individual.getThAttribute(1).toString()); 
			for(File origP: originalPrograms){
				args.add(origP.getPath());
			}
			
			if(!isJSONcreated())
				args.add("--");
			
			// run "mucpp apply operator location attribute"
			int result = launchProcess(args);
			
			if(result != 0){
				throw new GenerationException("Program exited with: " + result);
			}
			
			String nameBranch = "m" + normalizeCode(individual.getThOperator(1))  + "_" + calculateRealLocation(individual).toString() + 
								"_" + individual.getThAttribute(1).toString() + "_" + normalizeNameFile(fileIndividual.getName());
			
			File mutatedFile = new File(nameBranch);
			
			//Save the mutant
			mapIndividualToFile.put(individual, mutatedFile);
			mapBasenameToIndividual.put(mutatedFile.getName(), individual);
			
			
			//No es necesario, se puede devolver null
			return mutatedFile;
			
		}catch(Exception e){
			throw new GenerationException(e);
		}
	}
	
	private BigInteger calculateRealLocation(GAIndividual individual) {
		
		subOpCountsIndex = 0;
		BigInteger counter = BigInteger.valueOf(0);
		BigInteger location_global = individual.getThLocation(1);
		int operator = (individual.getThOperator(1)).intValue() - 1;
		
		//While there are programs and the global location searched is not in the range of the file, continue the search
		while (subOpCountsIndex < originalPrograms.length && location_global.compareTo(subOpCounts[operator][subOpCountsIndex].add(counter)) == 1){
			counter = counter.add(subOpCounts[operator][subOpCountsIndex]);
			subOpCountsIndex++;
		}
		
		return location_global.subtract(counter);
	}

	private File determineFileGAIndividual(GAIndividual individual) throws Exception{
		try{
			/*int subOpCountsIndex = 0;
			BigInteger counter = BigInteger.valueOf(0);
			BigInteger location_global = individual.getThLocation(1);
			int operator = (individual.getThOperator(1)).intValue() - 1;
			
			//While there are programs and the global location searched is not in the range of the file, continue the search
			while (subOpCountsIndex < originalPrograms.length && location_global.compareTo(subOpCounts[operator][subOpCountsIndex].add(counter)) == 1){
				counter = counter.add(subOpCounts[operator][subOpCountsIndex]);
				subOpCountsIndex++;
			}*/
			
			calculateRealLocation(individual);
		    
			return new File(originalPrograms[subOpCountsIndex].getPath());
		}catch(Exception e){
			throw new Exception("The individual analyze files do not match with the global file.");
		}
	}

	private void convertMatrixIntoResults(
			final List<Pair<String, Pair<int[], long[]>>> cmpMatrix,
			final List<ComparisonResults> cmpResults) throws ComparisonException
	{
		for (final Pair<String, Pair<int[], long[]>> result : cmpMatrix) {
			final String mutantPath = result.getLeft();
			final int[] bkgComparisons = result.getRight().getLeft();
			final long[] bkgNanos = result.getRight().getRight();

			final String basename = new File(mutantPath).getName();
			final ComparisonResults comparisonResults = convertLineIntoResults(basename, bkgComparisons, bkgNanos);

			cmpResults.add(comparisonResults);
		}
	}

	private ComparisonResults convertLineIntoResults(final String basename, final int[] cmps, final long[] nanos) throws ComparisonException {
		
		final GAIndividual individual = mapBasenameToIndividual.get(basename);
		if (individual == null) {
			throw new ComparisonException("Could not find individual with basename " + basename);
		}

		final ComparisonResult[] results = new ComparisonResult[cmps.length];
		for (int i = 0; i < cmps.length; ++i) {
			results[i] = ComparisonResult.getObject(cmps[i]);
		}
		final ComparisonResults comparisonResults = new ComparisonResults(individual, results);

		final BigInteger[] bigNanos = new BigInteger[nanos.length];
		for (int i = 0; i < bigNanos.length; ++i) {
			bigNanos[i] = BigInteger.valueOf(nanos[i]);
		}
		comparisonResults.setTestWallNanos(bigNanos);

		return comparisonResults;
	}
	
	private int launchProcess(List<String> args) throws Exception{
		try{
			ProcessBuilder pb = new ProcessBuilder();
			pb.command(args).directory(new File(mutationDirectory));
			Process p = pb.start();
			p.waitFor();
			InputStreamReader tempReader = new InputStreamReader(new BufferedInputStream(p.getInputStream()));
			BufferedReader reader = new BufferedReader(tempReader);
			while (true) {
				String line = reader.readLine();
			if (line == null)
				break;
				System.out.println(line);
			}
			
			return p.exitValue();
		}catch(Exception e){
			throw new AnalysisException(e);
		}
	}
	
	private String normalizeDirectory(String directory) {
		return directory.charAt(directory.length()-1) == '/' ? directory : directory.concat("/");
	}
	
	private String normalizeNameFile(String nameFileInd) {
		//Erase the extension of the file
		return nameFileInd.substring(0, nameFileInd.lastIndexOf('.'));
	}

	private String normalizeCode(BigInteger thOperator) {
		//Normalize the code of the operator with two digits
		if(thOperator.compareTo(new BigInteger("10")) == -1)
			return "0" + thOperator.toString();
		else
			return thOperator.toString();
	}
	
	@Override
	public void cleanup() throws PreparationException {
		// nothing for now
		
		/*
		//NUEVO: Vamos a enviar los mutantes generados de forma que se sepan sus ancestros.
		PrintStream ps2;
		try {
			ps2 = new PrintStream(new File(treeResults.toString()));
			ps2.flush();
			
			for (String key : mapBasenameToIndividual.keySet()) {
				ps2.print(lineTree);
				lineTree++;
				ps2.print("\t");
				ps2.print(key);
				ps2.print("\t");
				ps2.print("ANC1");
				ps2.print("\t");
				//Ancestro 1
				ps2.print("ANC2");
				ps2.print("\t");
				//Ancestro 2 si lo hubiera
				ps2.println();
			}
		    
			ps2.close();
		} catch (Exception e) {
			
			throw new PreparationException(e);
		}
		*/
		
	}
	
	
	

/*	@Option(type=OptionType.PATH_LOAD, fileExtensions={"cpp"})
	public void setTestSuite(String testSuite) {
		this.testSuite = testSuite != null ? new File(testSuite) : null;
	}

	public String getTestSuite() {
		return testSuite != null ? testSuite.getPath() : null;
	}

	public File getTestSuiteFile() {
		return testSuite;
	}*/

	@Option(type=OptionType.PATH_LOAD, fileExtensions={"cpp", "c", "cxx", "cc"})
	public void setOriginalPrograms(List<String> originalPrograms) {
	
		if(originalPrograms.size() != 0){
			this.originalPrograms = new File[originalPrograms.size()];
					
			for (int i = 0; i < originalPrograms.size(); i++) {
				 File l = new File(originalPrograms.get(i));
				 this.originalPrograms[i] = l;
			}
			
			// Habría que ordenar los ficheros en el mismo orden que mucpp
			//Arrays.sort(this.originalPrograms);
		}
		else{
			this.originalPrograms = null;
		}
	}

	public List<String> getOriginalPrograms(){
		List<String> programs = new ArrayList<String>();
		if(originalPrograms.length != 0){
			for (int i = 0; i < originalPrograms.length; i++) {
				programs.add(originalPrograms[i].toString());
			}
		}
		
		return programs;
	}
	
	public File[] getOriginalProgramsFiles() {
		return originalPrograms.length != 0 ? originalPrograms : null;
	}

	@Option(type=OptionType.PATH_LOAD, fileExtensions={"txt"})
	public void setTestExecution(String testExecution) {
		this.testExecution = testExecution != null ? new File(testExecution) : null;
	}

	public File getTestExecutionFile() {
		return testExecution;
	}
	
	public String getTestExecution(){
		return testExecution.toString();
	}
	
	@Option(type=OptionType.PATH_LOAD, fileExtensions={"txt"})
	public void setCompareResults(String compareResults) {
		this.compareResults = compareResults != null ? new File(compareResults) : null;
	}
	
	public File getCompareResultsFile() {
		return compareResults;
	}
	
	public void setMutationDirectory(String path){
		mutationDirectory=path;
	}
	
	public String getCompareResults(){
		return compareResults.toString();
	}
		
	public String getMutationDirectory(){
		return mutationDirectory;
	}
	
	public String getTestDirectory() {
		return testDirectory;
	}

	public void setTestDirectory(String testDirectory) {
		this.testDirectory = testDirectory;
	}
	
/*	private BigInteger[][] getSubOpCounts() {
		return subOpCounts;
	}

	private void setSubOpCounts(BigInteger[][] subOpCounts) {
		this.subOpCounts = subOpCounts;
	}*/

	public Map<BigInteger, String> getMapOpCode() {
		return mapOpCode;
	}

	public void setMapOpCode(Map<BigInteger, String> mapOpCode) {
		this.mapOpCode = mapOpCode;
	}
	
	public boolean isRunExecuted() {
		return runExecuted;
	}

	public void setRunExecuted(boolean runExecuted) {
		this.runExecuted = runExecuted;
	}

	public boolean isJSONcreated() {
		return JSONcreated;
	}

	public void setJSONcreated(boolean jSONcreated) {
		JSONcreated = jSONcreated;
	}
	
	public int getSubOpCountsIndex() {
		return subOpCountsIndex;
	}

	public void setSubOpCountsIndex(int subOpCountsIndex) {
		this.subOpCountsIndex = subOpCountsIndex;
	}
	
	@Option(type=OptionType.PATH_LOAD, fileExtensions={"txt"})
	public void setFileMutants(String fileMutants) {
		this.fileMutants = fileMutants != null ? new File(fileMutants) : null;
	}
	
	//Not used
	@Override
	public File getOriginalProgramFile() throws UnsupportedOperationException {
		return null;
	}
	
	@Override
	public void loadDataFile(File dataFile)
			throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		
	}

	@Option(type=OptionType.PATH_LOAD, fileExtensions={"vm"}, optional=true)
	public void setDataFile(String path) {
		loadDataFile(path != null ? new File(path) : null);
	}
	
	@Override
	public List<String> getTestCaseNames() throws AnalysisException {
		// return the name of each test case
		return null;
	}
	
	@Override
	public List<String> getMutationOperatorNames() throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Not supported by MuCpp yet.");
	}

	
	/*@Override
	public String mutantName(GAIndividual ind)
			throws UnsupportedOperationException {
		
		//Check if the mutant was previously generated
		if (mapIndividualToFile.containsKey(ind)) {
			File cachedFile = mapIndividualToFile.get(ind);
			return cachedFile.getName();
		}
		return null;
	}*/
}
