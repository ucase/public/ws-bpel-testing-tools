package es.uca.mucpp.gamerahom.exec;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;


public class MuCppExecutorTest {

	@Test
	public void analyzeWorks() throws Exception {
		MuCppExecutor exec = new MuCppExecutor();
		exec.prepare();
		
		
		//Quitar el comentario -> jenkins
		/*String testDirectory = "./";
		String mutantDirectory = "/home/pedro/clang-llvm/build/ejemplo_mucpp/";
		
		//String testSuite = mutantDirectory + testDirectory + "tests.cpp";
		List<String> origPs = new  ArrayList<String>();
		origPs.add("f1.cpp");
		origPs.add("f2.cpp");
		origPs.add("ab.cpp");
		origPs.add("tt/hola.cpp");
		
		String testExecution = mutantDirectory + "tests_output.txt";
		String compareResults = mutantDirectory + "comparison_results.txt";
		
		//exec.setTestSuite(testSuite);
		exec.setOriginalPrograms(origPs);
		exec.setTestDirectory(testDirectory);
		exec.setMutationDirectory(mutantDirectory);
		exec.setTestExecution(testExecution);
		exec.setCompareResults(compareResults);
		
		exec.validate();
		
		AnalysisResults results = exec.analyze(null);
		//assertEquals(results.getOperatorNames().length, 5);
		*/ //Finaliza el comentario
		GAIndividual[] individuals = new GAIndividual[3];
		// ESTE NO DESCOMENTAR
		/*for(int iResult = 0; iResult < 3; iResult++){
			individuals[iResult] = new GAIndividual(new BigInteger(Integer.toString(iResult)), 
										  results.getLocationCounts()[iResult], 
										  results.getFieldRanges()[iResult]);
		}*/

		//Quitar el comentario -> jenkins
		/*individuals[0] = new GAIndividual(new BigInteger(Integer.toString(2)), results.getLocationCounts()[1], results.getFieldRanges()[1]);
		individuals[1] = new GAIndividual(new BigInteger(Integer.toString(5)), results.getLocationCounts()[4], results.getFieldRanges()[4]);
		individuals[2] = new GAIndividual(new BigInteger(Integer.toString(5)), results.getLocationCounts()[4].subtract(BigInteger.valueOf(1)), results.getFieldRanges()[4]);
		
		exec.getMutantFile(individuals[0]);
		exec.getMutantFile(individuals[1]);
		exec.getMutantFile(individuals[2]);
		exec.getMutantFile(individuals[2]);
		
		@SuppressWarnings("unused")
		ComparisonResults[] comparisons = exec.compare(null, individuals);
		*/	//Finaliza el comentario
	}
}
