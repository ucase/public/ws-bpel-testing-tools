package es.uca.webservices.gamera.ggen.generate;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.individuals.Component;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeDate;
import es.uca.webservices.testgen.api.types.TypeDateTime;
import es.uca.webservices.testgen.api.types.TypeDuration;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTime;
import es.uca.webservices.testgen.api.types.TypeTuple;

/**
 * Population generator which cyclically reuses the individuals from the specified .vm file over and over.
 * All variables in the data file are supposed to have lists with the the same number of entries.
 *
 * This class assumes that the types won't change after the first invocation of {@link #generate} to the next.
 */
public class FixedGenerator implements GgenIndividualGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(FixedGenerator.class);

    private Random prng;
    private File dataFile;
    private Map<String, List<Object>> variableData;
    private int currentRow, rowCount;
    private DatatypeFactory datatypeFactory;

    @Override
    public synchronized GgenIndividual generate(GgenPopulation src, IStrategy generator, List<IType> types) throws GenerationException {
        assert types != null : "The list of types should not be null";

        if (variableData == null) {
            try {
                loadDataFile(types);
            } catch (IOException ex) {
                throw new GenerationException(ex);
            }
        }

        final GgenIndividual ind = new GgenIndividual();
        for (IType type : types) {
            final String var = type.getNameVariable();
            ind.addComponent(new Component(type, variableData.get(var).get(currentRow)));
        }
        currentRow = (currentRow + 1) % rowCount;

        assert types.size() ==  ind.getNumberComponents() : "The individual should have as many components as types";
        return ind;
    }

    @Override
    public void validate() throws InvalidConfigurationException {
        if (prng == null) {
            throw new InvalidConfigurationException("No PRNG has been set");
        }
        if (dataFile == null) {
            throw new InvalidConfigurationException("No data file has been set");
        }
    }

    @Override
    public void setPRNG(Random prng) {
        this.prng = prng;
    }

    public File getDataFile() {
        return dataFile;
    }

    public void setDataFile(File dataFile) {
        this.dataFile = dataFile;
    }

    private void loadDataFile(List<IType> types) throws IOException, GenerationException {
        LOGGER.info("Loading fixed population from {}", dataFile);
        final VelocityEngine engine = new VelocityEngine();
        engine.init();

        final VelocityContext dataContext = new VelocityContext();
        final FileReader reader = new FileReader(dataFile);
        try {
            engine.evaluate(dataContext, new StringWriter(), FixedGenerator.class.getCanonicalName(), reader);
        } finally {
            reader.close();
        }

        final Map<String, IType> variables = new HashMap<String, IType>();
        for (IType type : types) {
            variables.put(type.getNameVariable(), type);
        }

        currentRow = 0;
        rowCount = -1;
        variableData = new LinkedHashMap<String, List<Object>>();
        for (Object key : dataContext.getKeys()) {
            final String sKey = (String) key;
            final Object value = dataContext.get(sKey);

            // All variables should be lists
            if (value instanceof List) {
                @SuppressWarnings("unchecked")
				final List<Object> listValue = (List<Object>) value;

                if (!variables.containsKey(sKey)) {
                    throw new GenerationException("Data file contained an unexpected variable: " + key);
                }

                // All variables should have the same number of elements
                if (rowCount == -1) {
                    rowCount = listValue.size();
                }
                else if (rowCount != listValue.size()) {
                    throw new GenerationException(
                            String.format("All variables should have the same number of elements " +
                                    "as the first one (%d)", rowCount));
                }

                // We need to undo now the transformation by VelocityFormatter: we'll look at the IType and
                // convert the raw Velocity value to the matching type in IStrategy. This should also provide
                // useful for early validation of the data file.
                final List<Object> mappedValues = new ArrayList<Object>();
                final IType type = variables.get(sKey);
                for (Object elem : listValue) {
                    mappedValues.add(mapValue(elem, type));
                }

                variableData.put(sKey, listValue);
            } else {
                throw new GenerationException("Expected all variables to be lists, but " + key + " was not");
            }
        }
        if (rowCount == -1) {
            rowCount = 0;
        }

        final Set<String> specVars = new HashSet<String>(variables.keySet());
        specVars.removeAll(Arrays.asList(dataContext.getKeys()));
        if (!specVars.isEmpty()) {
            throw new GenerationException("Some variables in the specification are not listed in the data file: " + specVars);
        }
    }

    Object mapValue(Object elem, IType type) throws GenerationException {
        if (datatypeFactory == null) {
            try {
                datatypeFactory = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException e) {
                throw new GenerationException(e);
            }
        }

        if (elem instanceof Integer) {
            checkType(elem, type, TypeInt.class);
            return BigInteger.valueOf((Integer)elem);
        }
        else if (elem instanceof Long) {
            checkType(elem, type, TypeInt.class);
            return BigInteger.valueOf((Long)elem);
        }
        else if (elem instanceof Double) {
            checkType(elem, type, TypeFloat.class);
            return BigDecimal.valueOf((Double)elem);
        }
        else if (elem instanceof Float) {
            checkType(elem, type, TypeFloat.class);
            return BigDecimal.valueOf((Float)elem);
        }
        else if (elem instanceof List) {
            return mapListValue(elem, type);
        }
        else if (elem instanceof String) {
            return mapStringValue(elem, type);
        }
        else {
            LOGGER.warn("Unknown type: returning as is");
            return elem;
        }
    }

	private Object mapStringValue(Object elem, IType type) throws GenerationException {
		final String sValue = (String)elem;
		if (type instanceof TypeString) {
		    return sValue;
		}
		else if (type instanceof TypeDate || type instanceof TypeDateTime || type instanceof TypeTime) {
		    return datatypeFactory.newXMLGregorianCalendar(sValue);
		}
		else if (type instanceof TypeDuration) {
		    return datatypeFactory.newDuration(sValue);
		}
		else {
		    throw new GenerationException(String.format("String value '%s' in the data file is not compatible " +
		            "with the '%s' type", sValue, type.getClass().getName()));
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Object mapListValue(Object elem, IType type) throws GenerationException {
		final List originalList = (List)elem;
		if (type instanceof TypeList) {
		    final TypeList listType = (TypeList)type;
		    final List mappedList = new ArrayList(originalList.size());
		    for (Object child : originalList) {
		        mappedList.add(mapValue(child, listType.getType()));
		    }
		    return mappedList;
		} else if (type instanceof TypeTuple) {
		    final TypeTuple tupleType = (TypeTuple)type;
		    if (originalList.size() != tupleType.size()) {
		        throw new GenerationException(String.format("List value '%s' in the data file does not have the " +
		                "expected %d components of the tuple type", elem, tupleType.size()));
		    }

		    final List mappedList = new ArrayList(originalList.size());
		    for (int i = 0; i < tupleType.size(); ++i) {
		        mappedList.add(mapValue(originalList.get(i), tupleType.getIType(i)));
		    }
		    return mappedList;
		} else {
		    throw new GenerationException(String.format("List value '%s' in the data file is not compatible " +
		            "with the '%s' type", elem, type.getClass().getName()));
		}
	}

    private void checkType(Object elem, IType type, Class<?> klazz) throws GenerationException {
        if (!(klazz.isInstance(type))) {
            throw new GenerationException(String.format("Value '%s' in the data file is not compatible " +
                    "with the '%s' type", elem, type.getClass().getName()));
        }
    }
}
