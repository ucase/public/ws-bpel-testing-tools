package es.uca.webservices.gamera.ggen.genetic.crossover;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.individuals.Component;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.testgen.api.generators.GenerationException;

/**
 * This class implements the Regular Crossover Operator. Each component of the
 * children is chosen from the parents according to a probability.
 * 
 * @author Álvaro Cortijo-García
 */
public class UniformCrossoverOperator implements GACrossoverOperator {

	private final static Logger LOGGER = LoggerFactory.getLogger(ComponentCrossoverOperator.class);
	private Random prng;

	@Override
	public List<Integer> apply(GAGeneticOperatorOptions opts, GgenIndividual left, GgenIndividual right) throws GenerationException {
		final int nc1 = left.getNumberComponents();
		final int nc2 = right.getNumberComponents();

		if (nc1 != nc2) {
			throw new IllegalArgumentException(
					String.format(
							"Cannot apply this operator: the parents have different number of components (%d and %d)",
							nc1, nc2));
		}
		else if (nc1 < 2) {
			throw new IllegalArgumentException(
					String.format(
							"Cannot apply this operator: the parents have less than two components (%d)",
							nc1));
		}
		
		final List<Boolean> crossComponents = new ArrayList<Boolean>(); 
		for(int iCmp = 0; iCmp < nc1; iCmp++){
			crossComponents.add(prng.nextBoolean());
		}
		doCrossover(left, right, crossComponents);
		final List<Integer> crossPositions = new ArrayList<Integer>();
		for (int i = 0 ; i < crossComponents.size(); i++) {
			if (crossComponents.get(i)) {
				crossPositions.add(i);
			}
		}
		return crossPositions;
	}
	
	void doCrossover (GgenIndividual left, GgenIndividual right, List<Boolean> crossComponents)
	{		
		final List<Component> leftComponents = left.getComponents();
		final List<Component> rightComponents = right.getComponents();

		LOGGER.debug("xover before: {}, {}", left, right);
		for (int i = 0 ; i < crossComponents.size(); i++) {
			if (crossComponents.get(i)) {
				final Component tmp = left.getComponent(i);
				leftComponents.set(i, right.getComponent(i));
				rightComponents.set(i, tmp);
			}
		}
		LOGGER.debug("xover after: {}, {}", left, right);
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (prng == null) {
			throw new InvalidConfigurationException("PRNG has not been set");
		}
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}

}
