package es.uca.webservices.gamera.ggen.genetic.mutation;

import java.math.BigDecimal;
import java.math.BigInteger;

import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;

/**
 * This class implements the operator that mutates components using the binary
 * representation of a number.
 * 
 * @author Álvaro Cortijo-García, Antonio García-Domínguez
 */

public class BinaryMutationOperator extends MutationOperator {

	@Override
	protected BigInteger doMutationInt(final double probability, TypeInt type, Object value) {
		if (type.getAllowedValues() != null && !type.getAllowedValues().isEmpty()) {
			return pickRandomAllowedValue(type);
		} else {
			return applyIntBinaryMutation(type, value);
		}
	}
	
	@Override
	protected BigDecimal doMutationFloat(final double probability, TypeFloat type, Object value) {
		if (type.getAllowedValues() != null && !type.getAllowedValues().isEmpty()) {
			return pickRandomAllowedValue(type);
		} else {
			return applyFloatBinaryMutation(type, value);
		}
	}
	
	private BigInteger applyIntBinaryMutation(TypeInt type, Object value) {
		final BigInteger oldValue = value instanceof BigInteger ? ((BigInteger)value) : BigInteger.valueOf((Integer)value);
		final BigInteger min = type.getMinValue();
		final BigInteger max = type.getMaxValue();
		final BigInteger maxMinRange = max.subtract(min);

		BigInteger newValue;
		if(!maxMinRange.equals(BigInteger.ZERO)){
			final int maxMinBits = maxMinRange.bitLength();
			final int randomPosition = getPRNG().nextInt(maxMinBits);

			// Shift the value to mutate from [min, max] to [0, max-min].
			final BigInteger v = oldValue.subtract(min);
			newValue = v.flipBit(randomPosition);
			
			// Shift the mutated value from [0, max-min] to [min, max] and clip the value to the maximum value.
			newValue = newValue.add(min);
			if (max != null && newValue.compareTo(max) > 0) {
				newValue = max;
			}
		} else {
			newValue = oldValue;
		}

		return newValue;
	}	

	private BigDecimal applyFloatBinaryMutation(TypeFloat type, Object value) {
		final BigDecimal oldValue = value instanceof BigDecimal ? ((BigDecimal)value) : BigDecimal.valueOf((Float)value);
		final BigDecimal min = type.getMinValue();
		final BigDecimal max = type.getMaxValue();

		int scale = oldValue.scale() > min.scale() ? oldValue.scale() : min.scale();
		scale = scale > max.scale() ? scale : max.scale();
		
		// We convert the unscaled float to a integer in order to use the same way of mutation.
		final BigInteger unscaledValue = oldValue.movePointRight(scale).toBigInteger();
		final BigInteger integerMin = min.movePointRight(scale).toBigInteger();
		final BigInteger integerMax = max.movePointRight(scale).toBigInteger();
		final TypeInt integerType = new TypeInt(integerMin, integerMax);
		final BigInteger newUnscaledValue = applyIntBinaryMutation(integerType, unscaledValue);
		
		// We convert again the mutated value to a float.
		BigDecimal newValue = new BigDecimal(newUnscaledValue,scale);
				
		return newValue;
	}	
	
}
