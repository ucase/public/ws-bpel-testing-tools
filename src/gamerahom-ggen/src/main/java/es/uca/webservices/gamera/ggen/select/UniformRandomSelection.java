package es.uca.webservices.gamera.ggen.select;

import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.select.GASelectionOperator;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;

/**
 * @author Álvaro Galán Piñero
 * @version 1.0
 */
public class UniformRandomSelection implements GASelectionOperator{
	private Random prng;

	@Override
	public GgenIndividual select(GgenPopulation source) {
		final List<GgenIndividual> individuals = source.getPopulation();
		int position = prng.nextInt(individuals.size());
		return source.getIndividual(position);
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (prng == null) {
			throw new InvalidConfigurationException("PRNG has not been set");
		}		
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;		
	}
}
