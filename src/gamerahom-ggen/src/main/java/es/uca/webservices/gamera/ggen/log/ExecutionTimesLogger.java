package es.uca.webservices.gamera.ggen.log;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import java.io.IOException;
import java.math.BigInteger;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;
import java.util.List;

/**
 * Logger which reports wall clock and aggregated execution times for each
 * generation.
 * 
 * @author Antonio García-Domínguez
 */
public class ExecutionTimesLogger extends AbstractOutputRedirectedLogger {

	private long algorithmStartNanos, generationStartNanos;
	private BigInteger algorithmTestNanos;

	@Override
	public void started(GgenState state) {
		algorithmStartNanos = generationStartNanos = System.nanoTime();
		algorithmTestNanos = BigInteger.ZERO;
		printHeader();
	}

	@Override
	public void finished(GgenState state) throws IOException {
		final long algorithmWallNanos = System.nanoTime() - algorithmStartNanos;
		print("Algorithm", algorithmWallNanos, algorithmTestNanos);
	}

	@Override
	public void newGeneration(GgenState state, GgenPopulation population) {
		final long generationWallNanos = System.nanoTime() - generationStartNanos;

		BigInteger generationTestNanos = BigInteger.ZERO;
		if (state.getHistory().currentGeneration() > 0) {
			for (ComparisonResults programResults : state.getHistory().lastEntry().getComparisonsByProgram()) {
				generationTestNanos = generationTestNanos.add(programResults.getTotalNanos());
			}
		}
		print("Generation " + state.getCurrentGeneration(),
			generationWallNanos,
			generationTestNanos);

		algorithmTestNanos = algorithmTestNanos.add(generationTestNanos);
		generationStartNanos = System.nanoTime();
	}

	private void printHeader() {
		getStream().printMsgByConsoleFile(
				"Stage\tWall nanos\tTest nanos",
				isConsole(),
				getFile());
	}

	private void print(String msg, long wallNanos, BigInteger testNanos) {
		getStream().printMsgByConsoleFile(
			String.format("%s\t%d\t%s", msg, wallNanos, testNanos.toString()),
			isConsole(),
			getFile());
	}

}
