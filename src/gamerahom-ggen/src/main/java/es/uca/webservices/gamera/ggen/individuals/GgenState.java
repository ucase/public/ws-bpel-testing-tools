package es.uca.webservices.gamera.ggen.individuals;

import java.util.List;

import es.uca.webservices.gamera.ggen.conf.Configuration;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * <p>Saves interesting information of a concrete moment of the algorithm.</p>
 *
 * <p><em>Note</em>: this class should be kept as a JavaBean. It shouldn't have any special functionality of its own:
 * it's just a common storage space for all the information that might be useful to any of the basic
 * component classes.</p>
 *
 * @author alvaro, Antonio Garcia-Dominguez
 */
public class GgenState {
	
    private Configuration configuration;
    private GgenHistory history = new GgenHistory();

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public GgenHistory getHistory() {
        return history;
    }

    public void setHistory(GgenHistory history) {
        this.history = history;
    }

    public synchronized List<IType> getTypes() throws ParserException {
        return configuration.getTypes();
    }

	public int getCurrentGeneration() {
		return history.currentGeneration();
	}

    public GgenPopulation getCurrentPopulation() {
        return history.lastEntry().getPopulation();
    }
}
