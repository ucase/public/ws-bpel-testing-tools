package es.uca.webservices.gamera.ggen.term;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.individuals.GgenHistory;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Termination condition that stops the algorithm when a test that kills all valid
 * mutants is found.
 *
 * @author Antonio Garcia-Dominguez
 */
public class AllValidProgramMutantsKilledCondition implements GATerminationCondition {
    @Override
    public boolean evaluate(GgenState state) {
        final GgenHistory.Entry entry = state.getHistory().lastEntry();
        if (entry == null) {
            return false;
        }
        final ComparisonResult[][] lastResultsByTestCase = entry.getComparisonsByTestCase();

        testCases:
        for (ComparisonResult[] testCaseResults : lastResultsByTestCase) {
            for (ComparisonResult cmp : testCaseResults) {
                if (cmp.equals(ComparisonResult.SAME_OUTPUT)) {
                    // This test case didn't kill some valid mutant: skip it
                    continue testCases;
                }
            }

            // If we're still here, all the mutants were either killed or not valid
            return true;
        }

        // We skipped through all the test cases: not done yet
        return false;
    }

    @Override
    public void validate() throws InvalidConfigurationException {
        // nothing to do
    }
}
