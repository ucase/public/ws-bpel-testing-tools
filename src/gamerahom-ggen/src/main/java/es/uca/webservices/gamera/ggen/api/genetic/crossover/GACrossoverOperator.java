package es.uca.webservices.gamera.ggen.api.genetic.crossover;

import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.testgen.api.generators.GenerationException;

/**
 * Base interface for a crossover operator.
 * @author Álvaro Cortijo-García
 */
public interface GACrossoverOperator {

	/**
	 * Applies the crossover operator to two individuals.
	 * 
	 * @param opts
	 *            Operator-independent options set in the .yaml configuration
	 *            file, such as the probability of being applied.
	 * @param left
	 *            First individual which will undergo the crossover, in-place.
	 *            The caller should ensure that this instance is not contained
	 *            in any previous population, to avoid interference between
	 *            populations.
	 * @param right
	 *            Second individual which will undergo the crossover, in-place.
	 *            The caller should keep in mind the same considerations as for
	 *            <code>left</code>.
	 * @return List with the positions involved in the crossover.
	 * @throws GenerationException
	 */
	List<Integer> apply(GAGeneticOperatorOptions opts, GgenIndividual left,
			GgenIndividual right) throws GenerationException;

	/**
	 * Validates that the crossover operator has been correctly configured.
	 * 
	 * @throws InvalidConfigurationException
	 *             The crossover operator has not been correctly configured.
	 */
	void validate() throws InvalidConfigurationException;

	/**
	 * Sets the pseudo-random number generator to be used.
	 * 
	 * @param prng
	 *            Pseudo-random number generator.
	 */
	void setPRNG(Random prng);

}
