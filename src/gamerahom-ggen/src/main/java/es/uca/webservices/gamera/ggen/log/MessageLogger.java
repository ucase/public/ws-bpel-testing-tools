package es.uca.webservices.gamera.ggen.log;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * Logger that writes in a file or in the console a specific message. There is a
 * verbose option, for extended messages.
 * 
 * @author Álvaro Cortijo-García, Antonio García-Domínguez
 */

public class MessageLogger extends AbstractOutputRedirectedLogger {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageLogger.class);

    private boolean verbose = false;

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	@Override
	public void started(GgenState state) {
		getStream().printMsgByConsoleFile("The algorithm has started its execution.",
				isConsole(), getFile());
	}
	
	@Override
	public void finished(GgenState state) {
		getStream().printMsgByConsoleFile("The algorithm has finished its execution.",
				isConsole(), getFile());
	}
	
	@Override
	public void newGeneration(GgenState state, GgenPopulation population) {
		getStream().printMsgByConsoleFile("The algorithm has generated " + 
				population.getPopulationSize() + " individuals for generation " +
				state.getCurrentGeneration(), isConsole(), getFile());
		if(isVerbose()){
			int number = 0;
			for (GgenIndividual ind : population.getPopulation()){
				getStream().printMsgByConsoleFile(number + " " + ind.toString(),
						isConsole(), getFile());
				number++;
			}
		}
	}

	@Override
	public void startedPopulationExecution(GgenState state) {
		getStream().printMsgByConsoleFile("The algorithm has started running " +
				"the test cases of generation " + state.getCurrentGeneration(),
				isConsole(), getFile());
	}

	@Override
	public void finishedPopulationExecution(GgenState state) {
		getStream().printMsgByConsoleFile("The algorithm has finished running " +
				"the test cases of generation " + state.getCurrentGeneration(),
				isConsole(), getFile());
	}

	@Override
	public void startedEvaluation(GgenState state, GgenPopulation population) {
		getStream().printMsgByConsoleFile("The algorithm has started computing " +
				"the fitness of individuals in generation " + state.getCurrentGeneration(),
                isConsole(), getFile());
	}
	
	@Override
	public void finishedEvaluation(GgenState state, GgenPopulation population) {
		getStream().printMsgByConsoleFile("The algorithm has finished computing " +
				"the fitness of individuals in generation " + state.getCurrentGeneration(),
                isConsole(), getFile());
		int number = 0;
		for (GgenIndividual ind : population.getPopulation()){
			getStream().printMsgByConsoleFile("Fitness individual " + number + ": " + 
					ind.getFitness(), isConsole(), getFile());
			number++;
		}
	}

	@Override
	public void createdVariables(GgenState state) {
        try {
            getStream().printMsgByConsoleFile("The algorithm has parsed the specification " +
                    "file and the components of the individuals have been created. " +
                    "In this execution, the individuals will be composed of " + state.getTypes().size()
                    + " variables.", isConsole(), getFile());
        } catch (ParserException e) {
            LOGGER.error("There was an error while parsing the variable declarations", e);
        }
    }

	@Override
	public void startedComparison(GgenState state) {
		getStream().printMsgByConsoleFile(
				"The algorithm has started comparing the individuals in generation "
						+ state.getCurrentGeneration(), isConsole(), getFile());
	}

	@Override
	public void finishedComparison(GgenState state, List<ComparisonResults> results) {
		getStream().printMsgByConsoleFile(
				"The algorithm has finished comparing the individuals in generation "
						+ state.getCurrentGeneration(), isConsole(), getFile());
		if(isVerbose()){
			final StringBuilder sb = new StringBuilder("Execution matrix:\n");
			for (ComparisonResults rs : results){
				sb.append(rs.getIndividual().individualFieldsToString());
				for(ComparisonResult r : rs.getRow()){
					sb.append(" ");							
					sb.append(r.getValue());
				}
				sb.append("\n");
			}
			getStream().printMsgByConsoleFile(sb.toString(), isConsole(), getFile());
		}
	}

	@Override
	public void selectedIndividuals(GgenIndividual left, GgenIndividual right) {
		getStream().printMsgByConsoleFile("Two individuals have been selected:  " +
				left.toString() + " and " + right.toString(),
                isConsole(), getFile());
	}

	@Override
	public void appliedCrossover(GgenState state, GACrossoverOperator op, GgenIndividual father, GgenIndividual mother, GgenIndividual leftSon, GgenIndividual rightSon, List<Integer> crossPoints) {
		getStream().printMsgByConsoleFile("Crossover operator " + op.getClass().getName() + " applied. Result: " +
				leftSon.toString() + " and " + rightSon.toString(),
                isConsole(), getFile());
	}

	@Override
	public void appliedMutation(GgenState state, GAMutationOperator op, List<Integer> mutatedComponents, GgenIndividual original, GgenIndividual mutated, boolean previousCrossover, int child) {
		getStream().printMsgByConsoleFile("Mutation operator " + op.getClass().getName() + " applied. Original: " +
				original.toString() + " Result: " + mutated.toString(),
                isConsole(), getFile());
	}

	@Override
	public void generatedIndividual(GgenState state, GgenIndividualGenerator gen, GgenIndividual ind) {
		getStream().printMsgByConsoleFile(
				"The generator " + gen.getClass().getName()
						+ " has generated a new individual.", isConsole(),
				getFile());
		if(isVerbose()){
			getStream().printMsgByConsoleFile(ind.toString(), isConsole(), getFile());
		}
	}

	@Override
	public void addedIndividual(GgenState state, GgenIndividual individual) {
		getStream().printMsgByConsoleFile("Individual added to the next generation: " + 
				individual.toString(), isConsole(), getFile());
	}

	@Override
	public void passedTerminationCondition(GATerminationCondition cond) {
		getStream().printMsgByConsoleFile("Termination condition "+ cond.getClass().getName()
				+" is true: done", isConsole(), getFile());
	}

	@Override
	public void generatedProgramMutants(AnalysisResults analysis, List<GAIndividual> programMutants) {
		// nothing
	}

}
