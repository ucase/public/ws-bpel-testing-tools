package es.uca.webservices.gamera.ggen.log;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.ggen.individuals.GgenHistory;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Logger that produces a textual report with information about the last population generated
 * by the algorithm.
 * @author Antonio Garcia-Dominguez
 */
public class LastGenerationReportLogger extends AbstractGenerationReportLogger {

    @Override
    public void finished(GgenState state) throws IOException {
        GgenHistory.Entry lastEntry = state.getHistory().lastEntry();
        if (lastEntry == null) {
            return;
        }

        final List<GgenIndividual> lastPopulation = lastEntry.getPopulation().getPopulation();
        final ComparisonResult[][] lastResults = lastEntry.getComparisonsByTestCase();
        final FileWriter fW = new FileWriter(getFile());
        try {
            fW.write(formatGeneration(lastPopulation, Arrays.asList(lastResults)));
        } finally {
            fW.close();
        }
    }

}
