package es.uca.webservices.gamera.ggen.genetic.mutation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeTuple;
/**
 * This class implements the Composite operator.
 * 
 * @author Álvaro Cortijo-García, Antonio García-Domínguez.
 */
public abstract class AbstractCompositeMutationOperator implements GAMutationOperator {
	private Random prng;

	protected abstract GAMutationOperator getOperator(IType type, int pos) throws InvalidConfigurationException;

	@SuppressWarnings("unchecked")
	@Override
	public Object doMutation(
		final double probability, final IStrategy strategy, final IType type,
		final Object value, List<Integer> componentsToMutate) throws GenerationException {
		if (type instanceof TypeTuple && componentsToMutate != null && !componentsToMutate.isEmpty()){
			// Do the mutation of the selected values of the individual
			final List<Object> newTuple = new ArrayList<Object>((List<Object>)value);
			for(int position : componentsToMutate){
				final IType componentType = ((TypeTuple) type).getIType(position);
				final Object componentValue = ((List<Object>) value).get(position);
				final Object newComponentValue = applySpecificMutation(probability, strategy, componentType, componentValue, position);
				// Change the selected values of the individual, replacing the old value with a new value.
				newTuple.set(position, newComponentValue);
			}
			return newTuple;
		} else {
			return applySpecificMutation(probability, strategy, type, value, -1);
		}
	}

	private Object applySpecificMutation(final double probability, final IStrategy strategy, final IType type, final Object value, final int pos) throws GenerationException {
		try {
			GAMutationOperator op = getOperator(type, pos);
			return op.doMutation(probability, strategy, type, value, null);
		} catch (Exception ex) {
			return new GenerationException(ex);
		}		
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (prng == null) {
			throw new InvalidConfigurationException("PRNG has not been set");
		}
	}
	
	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}

	public Random getPRNG() {
		return prng;
	}

}