package es.uca.webservices.gamera.ggen.log;

import java.util.Collection;

import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Logger which saves the data produced during the last generation to the selected outputs.
 * @author Antonio Garcia-Dominguez
 */
public class LastPopulationDataLogger extends AbstractDataLogger {
    @Override
    protected Collection<GgenIndividual> getIndividuals(GgenState state) {
        return state.getHistory().lastEntry().getPopulation().getPopulation();
    }
}
