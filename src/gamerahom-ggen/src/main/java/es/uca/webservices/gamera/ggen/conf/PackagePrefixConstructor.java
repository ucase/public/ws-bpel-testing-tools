package es.uca.webservices.gamera.ggen.conf;

import org.yaml.snakeyaml.constructor.Constructor;

/**
 * Constructor which will first try to look for a class using a specific package
 * prefix, and if it isn't found it will try again without it.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public class PackagePrefixConstructor extends Constructor {
    private String prefix;

    /**
     * Constructor for PackagePrefixConstructor which only receives a prefix
     * 
     * @param prefix
     */
    public PackagePrefixConstructor(String prefix) {
        this.prefix = prefix;
    }

    /**
     * Constructor for PackagePrefixConstructor which receives a root and a prefix
     * 
     * @param root
     * @param prefix
     * @throws ClassNotFoundException
     */
    public PackagePrefixConstructor(String root, String prefix)
            throws ClassNotFoundException {
        super(root);
        this.prefix = prefix;
    }

    @Override
    protected Class<?> getClassForName(String name)
            throws ClassNotFoundException {
        try {
            return super.getClassForName(prefix + "." + name);
        } catch (ClassNotFoundException e) {
            return super.getClassForName(name);
        }
    }
}
