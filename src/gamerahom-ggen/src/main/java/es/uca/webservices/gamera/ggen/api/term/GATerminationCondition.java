package es.uca.webservices.gamera.ggen.api.term;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Interface for all termination conditions.
 */
public interface GATerminationCondition {

    /**
     * Indicates whether the algorithm should terminate or not.
     *
     * @param state
     *            Current state of the algorithm.
     * @return <code>true</code> when the algorithm should terminate,
     *         <code>false</code> otherwise.
     */
    boolean evaluate(GgenState state);

    /**
     * Checks that the termination condition has been correctly configured.
     *
     * @throws InvalidConfigurationException
     *             The termination condition has not been correctly configured.
     */
    void validate() throws InvalidConfigurationException;
}
