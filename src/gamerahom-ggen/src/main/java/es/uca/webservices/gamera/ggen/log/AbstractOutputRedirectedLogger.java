package es.uca.webservices.gamera.ggen.log;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import java.io.IOException;

import es.uca.webservices.gamera.ggen.api.log.GALogger;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import java.util.List;

public abstract class AbstractOutputRedirectedLogger implements GALogger {
    private boolean console = true;
    private String file;

    private PrintingByConsoleOrFile stream = new PrintingByConsoleOrFile();
    
    @Override
    public void finished(GgenState state) throws IOException {
        // do nothing
    }

    @Override
    public void started(GgenState state) {
        // do nothing
    }
    
    @Override
    public void newGeneration(GgenState state, GgenPopulation population) {
        // do nothing
    }
    
    @Override
    public void startedPopulationExecution(GgenState state) {
        // do nothing
    }
    
    @Override
    public void finishedPopulationExecution(GgenState state) {
        // do nothing
    }
    
    @Override
    public void startedEvaluation(GgenState state, GgenPopulation population) {
        // do nothing
    }
    
    @Override
    public void finishedEvaluation(GgenState state, GgenPopulation population) {
        // do nothing
    }
    
    @Override
    public void createdVariables(GgenState state) {
        // do nothing
    }

    @Override
    public void startedComparison(GgenState state) {
        // do nothing
    }

    @Override
    public void finishedComparison(GgenState state, List<ComparisonResults> results) {
        // do nothing
    }

    @Override
    public void selectedIndividuals(GgenIndividual left, GgenIndividual right) {
        // do nothing
    }

    @Override
    public void appliedCrossover(GgenState state, GACrossoverOperator op, GgenIndividual father, GgenIndividual mother, GgenIndividual leftSon, GgenIndividual rightSon, List<Integer> crossPoints) {
        // do nothing
    }

    @Override
    public void appliedMutation(GgenState state, GAMutationOperator op, List<Integer> mutatedComponents, GgenIndividual original, GgenIndividual mutated, boolean previousCrossover, int child) {
        // do nothing
    }

    @Override
    public void generatedIndividual(GgenState state, GgenIndividualGenerator op, GgenIndividual ind) {
        // do nothing
    }

    @Override
    public void addedIndividual(GgenState state, GgenIndividual individual) {
        // do nothing
    }

    @Override
    public void passedTerminationCondition(GATerminationCondition cond) {
        // do nothing
    }

    @Override
    public void generatedProgramMutants(AnalysisResults analysis, List<GAIndividual> programMutants) {
        // do nothing
    }

    // VALIDATION

    @Override
    public void validate() throws InvalidConfigurationException {
        if (!isConsole() && getFile() == null) {
            throw new InvalidConfigurationException(
                    "At least the console or file output should be enabled. " +
                    "If you do not want any output, please use the " +
                    "NullLogger instead.");
        } else if (isConsole() && getFile() != null) {
            throw new InvalidConfigurationException(
                    "Only an output should be enabled. Please choose the console" +
                    " or file output.");
        }
    }

    public void setConsole(boolean console) {
        this.console = console;
    }

    public boolean isConsole() {
        return console;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFile() {
        return file;
    }
    
	public PrintingByConsoleOrFile getStream() {
        if (stream != null && stream.getPs() == null) {
            stream.createStream(console, file);
        }
		return stream;
	}

}
