package es.uca.webservices.gamera.ggen.genetic.mutation;

import java.util.Map;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.testgen.api.generators.IType;
/**
 * This class implements the Composite by type operator.
 * 
 * @author Álvaro Cortijo-García, Antonio García-Domínguez.
 */
public class CompositeByTypeMutationOperator extends AbstractCompositeMutationOperator {

	private Map<String, GAMutationOperator> ops;

	@Override
	public void validate() throws InvalidConfigurationException {
		super.validate();

		for (GAMutationOperator op : ops.values()) {
			if (op == null) {
				throw new InvalidConfigurationException("The operators must not be null");
			}
			op.setPRNG(getPRNG());
			op.validate();
		}
	}

	public Map<String, GAMutationOperator> getOps() {
		return ops;
	}

	public void setOps(Map<String, GAMutationOperator> specificTypesOp) {
		this.ops = specificTypesOp;
	}

	@Override
	protected GAMutationOperator getOperator(IType type, int pos) throws InvalidConfigurationException {
		for(String t : type.getNamesTypes()){
			if (ops.containsKey(t)) {
				return ops.get(t);
			}
		}
		throw new InvalidConfigurationException("No mutation operator has been defined for type " + type);
	}
}
