package es.uca.webservices.gamera.ggen.log;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenHistory;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

import java.util.*;

/**
 * Logger that saves a subset of the test cases to a file. The subset discards
 * test cases that are subsumed by other tests. A test T is said to be subsumed
 * by a test U when T kills a subset of the program mutants killed by U.
 *
 * @author Antonio Garcia-Dominguez
 */
public class SubsumeByKillsDataLogger extends AbstractDataLogger {
    @Override
    protected Collection<GgenIndividual> getIndividuals(GgenState state) {
        final Map<GgenIndividual, Set<GAIndividual>> currentResults = new HashMap<GgenIndividual, Set<GAIndividual>>();

        for (GgenHistory.Entry historyEntry : state.getHistory().getEntries()) {
        	// We visit test cases in the order in which they appeared in the data file, to make the entire process
        	// more predictable and easier to test (at the cost of some performance in comparison to using entrySet()).
        	final List<GgenIndividual> inds = historyEntry.getPopulation().getPopulation();
            final Map<GgenIndividual, Set<GAIndividual>> killed = historyEntry.getProgramMutantsKilledByTestCase();

            individual:
            for (GgenIndividual ind : inds) {
                final Set<GAIndividual> killedByCandidate = killed.get(ind);

                final Iterator<Map.Entry<GgenIndividual, Set<GAIndividual>>> itCurrentResults = currentResults.entrySet().iterator();
                while (itCurrentResults.hasNext()) {
                    final Set<GAIndividual> killedByCurrent = itCurrentResults.next().getValue();
                    if (killedByCurrent.containsAll(killedByCandidate)) {
                        // The existing individual subsumes the candidate - skip this candidate
                        continue individual;
                    }
                    else if (killedByCandidate.containsAll(killedByCurrent)) {
                        // The new candidate strictly subsumes the existing individual - remove the existing individual
                        itCurrentResults.remove();
                    }
                }

                // If we're still here, the candidate was not subsumed by any of the previously existing elements: add it
                currentResults.put(ind, killedByCandidate);
            }
        }



        return new ArrayList<GgenIndividual>(currentResults.keySet());
    }
}
