package es.uca.webservices.gamera.ggen.genetic.mutation;

import java.util.List;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.testgen.api.generators.IType;
/**
 * This class implements the Composite by position operator.
 * 
 * @author Álvaro Cortijo-García, Antonio García-Domínguez.
 */
public class CompositeByPositionMutationOperator extends AbstractCompositeMutationOperator {

	private List<GAMutationOperator> ops;

	public List<GAMutationOperator> getOps() {
		return ops;
	}

	public void setOps(List<GAMutationOperator> ops) {
		this.ops = ops;
	}

	@Override
	protected GAMutationOperator getOperator(IType type, int pos) {
		return ops.get(pos);
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		super.validate();

		for (GAMutationOperator op : ops) {
			if (op == null) {
				throw new InvalidConfigurationException("The operator must not be null");
			}
			op.setPRNG(getPRNG());
			op.validate();
		}
	}
	
}
