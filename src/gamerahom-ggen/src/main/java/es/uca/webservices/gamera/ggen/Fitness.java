package es.uca.webservices.gamera.ggen;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.ValidIndividualsList;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.ggen.api.IFitness;
import es.uca.webservices.gamera.ggen.api.log.GALogger;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;
import es.uca.webservices.gamera.ggen.util.FormatterUtilities;
import es.uca.webservices.testgen.api.formatters.IFormatter;

/**
 * Default fitness function.
 *
 * @author Álvaro Cortijo-García, alvaro, Antonio Garcia-Dominguez
 */
public class Fitness implements IFitness {

	private static final Logger LOGGER = LoggerFactory.getLogger(Fitness.class);

    public static final String DATAFILE = "data.vm";
    private List<GAIndividual> programMutants;

    /* (non-Javadoc)
	 * @see es.uca.webservices.gamera.ggen.IFitness#prepare(es.uca.webservices.gamera.AnalysisResults, java.util.List)
	 */
    @Override
	public void prepare(AnalysisResults analysis, List<List<Integer>> individuals, GALogger logger) throws InvalidConfigurationException {
        this.programMutants = new ArrayList<GAIndividual>();
        if (individuals == null || individuals.isEmpty()) {
            this.programMutants = new ValidIndividualsList(analysis, 1).all();
        }
        else {
            validateIndividuals(analysis, individuals);
            for (List<Integer> l : individuals) {
                this.programMutants.add(new GAIndividual(l.get(0), l.get(1), l.get(2)));
            }
        }
        logger.generatedProgramMutants(analysis, programMutants);
    }

    /* (non-Javadoc)
	 * @see es.uca.webservices.gamera.ggen.IFitness#getIndividuals()
	 */
    @Override
	public List<GAIndividual> getIndividuals() {
        return programMutants;
    }

    /* (non-Javadoc)
	 * @see es.uca.webservices.gamera.ggen.IFitness#computeFitness(es.uca.webservices.gamera.exec.bpel.GAExecutor, es.uca.webservices.gamera.ggen.individuals.GgenPopulation, es.uca.webservices.testgen.api.formatters.IFormatter, es.uca.webservices.gamera.ggen.individuals.GgenState, es.uca.webservices.gamera.ggenAPI.log.GALogger)
	 */
    @Override
	public ComparisonResults[] computeFitness(GAExecutor executor, GgenPopulation population, IFormatter formatter,
                                              GgenState state, GALogger logger) throws IOException, ComparisonException
    {
        logger.startedEvaluation(state, population);

        // Divide individuals into those that have been run already and those that have not
        final Map<Integer, ComparisonResult[]> prevResults = new HashMap<Integer, ComparisonResult[]>();
        final List<GgenIndividual> toBeRun = new ArrayList<GgenIndividual>();
    	setPreviousComparisonsAside(population, state, prevResults, toBeRun);

        // Run the new individuals (if any are left)
    	logger.startedComparison(state);

        ComparisonResults[] newTestResults;
        if (toBeRun.isEmpty()) {
        	newTestResults = new ComparisonResults[0];
        } else {
        	final File vmFile = FormatterUtilities.writeToFile(toBeRun, formatter, new File(DATAFILE));
        	newTestResults = executePopulation(executor, vmFile, state, logger);
        }

        final ComparisonResult[][] rawResults = mergePreviousAndNewTestResults(population, prevResults, newTestResults);

        // Convert the raw matrix back into a ComparisonResults[], so we know which programs were run
        final List<ComparisonResults> results = new ArrayList<ComparisonResults>(programMutants.size());
        for (int iIndividual = 0; iIndividual < programMutants.size(); ++iIndividual) {
        	results.add(new ComparisonResults(programMutants.get(iIndividual), rawResults[iIndividual]));
        }
        logger.finishedComparison(state, results);

        // Precompute the sum of the row of the comparison results from each valid mutant to speed up calculations.
        final int[] mutantSums = computeRowSums(rawResults);

        for (int iTestCase = 0; iTestCase < population.getPopulationSize(); iTestCase++){
            double total = 0;

            for (int iProgramMutant = 0; iProgramMutant < rawResults.length; iProgramMutant++) {
                final ComparisonResult[] row = rawResults[iProgramMutant];
                if (row[iTestCase] == ComparisonResult.DIFFERENT_OUTPUT) {
                    // There's no need to check that mutantSums[iMutant] != 0: if the mutant was killed
                    // by this test case, the sum of the row will be greater than zero anyways.
                    total += 1.0/mutantSums[iProgramMutant];
                }
            }

            population.getIndividual(iTestCase).setFitness(total);
        }

        // All invalid program mutants should have been removed after the initial generation
        final List<ComparisonResults> filteredResults = removeInvalidProgramMutants(results);
        logger.finishedEvaluation(state, population);
        return filteredResults.toArray(new ComparisonResults[filteredResults.size()]);
    }

	private ComparisonResult[][] mergePreviousAndNewTestResults(
			GgenPopulation population,
			final Map<Integer, ComparisonResult[]> prevResults,
			ComparisonResults[] newTestResults)
	{
		assert prevResults.size() + (newTestResults.length > 0 ? newTestResults[0].getRow().length : 0) == population.getPopulationSize();

		final ComparisonResult[][] rawResults = new ComparisonResult[programMutants.size()][];
        for (int iIndividual = 0; iIndividual < rawResults.length; ++iIndividual) {
        	rawResults[iIndividual] = new ComparisonResult[population.getPopulationSize()];
        }

        int iNewTestColumn = 0;
        for (int iColumn = 0; iColumn < population.getPopulationSize(); ++iColumn) {
        	if (prevResults.containsKey(iColumn)) {
        		final ComparisonResult[] cmps = prevResults.get(iColumn);

            	for (int iIndividual = 0; iIndividual < programMutants.size(); ++iIndividual) {
            		rawResults[iIndividual][iColumn] = cmps[iIndividual];
            	}
        	}
        	else {
        		for (int iIndividual = 0; iIndividual < programMutants.size(); ++iIndividual) {
        			rawResults[iIndividual][iColumn] = newTestResults[iIndividual].getRow()[iNewTestColumn];
        		}
        		iNewTestColumn++;
        	}
        }
		return rawResults;
	}

	private void setPreviousComparisonsAside(GgenPopulation population, GgenState state, final Map<Integer, ComparisonResult[]> prevResults, final List<GgenIndividual> toBeRun) {
		final Map<GgenIndividual, ComparisonResult[]> previousComparisons = state.getHistory().allUniqueIndividualsByComponentValues();

		int iIndividual = 0;
        for (GgenIndividual individual : population.getPopulation()) {
        	final ComparisonResult[] previousComparison = previousComparisons.get(individual);
        	if (previousComparison != null) {
        		LOGGER.info("Reusing previous execution for {}", individual);
        		prevResults.put(iIndividual, previousComparison);
        	}
        	else {
        		toBeRun.add(individual);
        	}
        	iIndividual++;
        }
	}

    private int[] computeRowSums(ComparisonResult[][] results) {
        final int[] mutantSums = new int[results.length];
        for (int iMutant = 0; iMutant < results.length; ++iMutant) {
            final ComparisonResult[] row = results[iMutant];

            // Skip the rest of the row as soon as we find it's for an invalid mutant.
            for (int iTest = 0; iTest < row.length && row[iTest] != ComparisonResult.INVALID; iTest++) {
                mutantSums[iMutant] += row[iTest].isDead() ? 1 : 0;
            }
        }
        return mutantSums;
    }

    private List<ComparisonResults> removeInvalidProgramMutants(List<ComparisonResults> results) {
    	assert results.size() == programMutants.size() : "There should be exactly as many comparisons as individuals";

    	final Iterator<GAIndividual> itIndividuals = programMutants.iterator();
    	final List<ComparisonResults> cmpValid = new ArrayList<ComparisonResults>();
    	for (ComparisonResults cmp : results) {
    		final GAIndividual ind = itIndividuals.next();
    		assert cmp.getIndividual().equals(ind) : "The individuals from the fitness function and the comparison results should be the same";
    		
    		if (cmp.isInvalid()) {
    			itIndividuals.remove();
    		}
    		else {
    			cmpValid.add(cmp);
    		}
    	}

    	return cmpValid;
    }

	private ComparisonResults[] executePopulation(GAExecutor executor, File file, GgenState state, GALogger logger)
            throws ComparisonException
    {
		executor.loadDataFile(file);

		final GAIndividual[] programMutantsCopy = programMutants.toArray(new GAIndividual[programMutants.size()]);
		logger.startedPopulationExecution(state);
		final ComparisonResults[] results = executor.compare(null, programMutantsCopy);
		logger.finishedPopulationExecution(state);

		return results;
	}

	private static void validateIndividuals (AnalysisResults analysis, List<List<Integer>> inds) throws InvalidConfigurationException {
		final int maxOperator = analysis.getOperatorNames().length;
		final BigInteger[] locations = analysis.getLocationCounts();
		final BigInteger[] attributes = analysis.getFieldRanges();
		for (List<Integer> l : inds) {
			final int operator = l.get(0);
			if (operator < 1 || operator > maxOperator) {
                throw new InvalidConfigurationException(
                        String.format("Individual %s is invalid: operator %d is outside bounds [1, %d]",
                                l, operator, maxOperator));
			}
			else {
                final int location = l.get(1);
                final int maxLocation = locations[operator - 1].intValue();
                if (location < 1 || location > maxLocation) {
                    throw new InvalidConfigurationException(String.format(
                            "Individual %s is invalid: location %d is outside bounds [1, %d] for operator %d",
                            l, location, maxLocation, operator));
				}
				else {
                    final int attribute = l.get(2);
                    final int maxAttribute = attributes[operator - 1].intValue();
                    if (attribute < 1 || attribute > maxAttribute) {
                        throw new InvalidConfigurationException(String.format(
                            "Individual %s is invalid: attribute %s is outside bounds [1, %d] for operator %d",
                            l, attribute, maxAttribute, operator));
					}
				}
			}
		}
	}
}
