package es.uca.webservices.gamera.ggen.api.generate;

import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;

/**
 * Interface for new individual generators. These generators produce new
 * individuals without taking previously existing individuals into account.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public interface GgenIndividualGenerator {

	/**
	 * Generates and returns one new individual. The individual should not be
	 * one of the objects in the source population.
	 *
	 * @param src
	 *            Previous population to use for deriving any required
	 *            information. This population will remain unchanged.
	 * @param strategy
	 *            New test data generation strategy to be used.
	 * @param types
	 *            Types and names of the variables to be filled in.
	 * @throws ComparisonException
	 * @throws GenerationException
	 */
	GgenIndividual generate(GgenPopulation src, IStrategy strategy, List<IType> types) throws GenerationException;

	/**
	 * Validates that the generator has been correctly configured.
	 * 
	 * @throws InvalidConfigurationException
	 *             The generator was not correctly configured.
	 */
	void validate() throws InvalidConfigurationException;

	/**
	 * Sets the pseudo-random number generator to be used to generate individuals.
	 */
	void setPRNG(Random prng);
}
