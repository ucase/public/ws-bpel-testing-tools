package es.uca.webservices.gamera.ggen.conf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.ggen.api.generate.GAIndividualGeneratorOptions;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.gamera.ggen.api.log.GALogger;
import es.uca.webservices.gamera.ggen.api.select.GASelectionOperator;
import es.uca.webservices.gamera.ggen.api.select.GASelectionOperatorOptions;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.testgen.api.parsers.IParser;

/**
 * @author Álvaro Cortijo-García, Álvaro Galán Piñero, Antonio Garcia-Dominguez
 * @version 1.0
 */

public class ConfigurationLoader {
	static final String CLASS_TAG_PREFIX = "es.uca.webservices";

	/**
	 * Parses a YAML document to configure the algorithm.
	 * @param in
	 * @return the loaded and validated configuration.
	 * @throws FileNotFoundException
	 * @throws InvalidConfigurationException
	 * @throws ClassNotFoundException
	 */
	public Configuration parse(InputStream in) throws FileNotFoundException, InvalidConfigurationException, ClassNotFoundException {
        Yaml yaml = new Yaml(constructor());
        return (Configuration)yaml.load(in);
    }

    /**
     * Convenience version of {@link #parse(java.io.InputStream)} for loading files.
     */
    public Configuration parse(File file) throws IOException, ClassNotFoundException, InvalidConfigurationException {
        final FileInputStream in = new FileInputStream(file);
        try {
            return parse(in);
        } finally {
            in.close();
        }
    }
	
	private Constructor constructor() throws ClassNotFoundException {
		PackagePrefixConstructor constructor = new PackagePrefixConstructor(Configuration.class.getName(), CLASS_TAG_PREFIX);
		TypeDescription confDescription = new TypeDescription(Configuration.class);
        
		confDescription.putMapPropertyType("crossoverOperators", GACrossoverOperator.class, GAGeneticOperatorOptions.class);		
		confDescription.putMapPropertyType("mutationOperators", GAMutationOperator.class, GAGeneticOperatorOptions.class);
        confDescription.putMapPropertyType("individualGenerators", GgenIndividualGenerator.class, GAIndividualGeneratorOptions.class);
        confDescription.putMapPropertyType("selectionOperators", GASelectionOperator.class, GASelectionOperatorOptions.class);
        confDescription.putListPropertyType("terminationConditions", GATerminationCondition.class);
        confDescription.putListPropertyType("loggers", GALogger.class);
        confDescription.putListPropertyType("parser", IParser.class);

        constructor.addTypeDescription(confDescription);

        return constructor;
	}
}
