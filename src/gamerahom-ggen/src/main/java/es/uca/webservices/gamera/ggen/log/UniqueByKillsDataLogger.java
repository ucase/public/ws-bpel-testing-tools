package es.uca.webservices.gamera.ggen.log;

import java.util.Collection;

import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Logger that saves the test data produced by the algorithm, discarding duplicates
 * that kill the exact same set of program mutants.
 * @author Antonio Garcia-Dominguez
 */
public class UniqueByKillsDataLogger extends AbstractDataLogger {
    @Override
    protected Collection<GgenIndividual> getIndividuals(GgenState state) {
        return state.getHistory().allUniqueIndividualsByComparisonResults().values();
    }
}
