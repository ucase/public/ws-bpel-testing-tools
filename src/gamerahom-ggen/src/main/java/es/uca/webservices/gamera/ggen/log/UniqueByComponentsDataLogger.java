package es.uca.webservices.gamera.ggen.log;

import java.util.Collection;

import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Logger that saves the test data produced by every generation of the algorithm,
 * discarding duplicates with the same values for every variable.
 * @author Antonio Garcia-Dominguez
 */
public class UniqueByComponentsDataLogger extends AbstractDataLogger {
    @Override
    protected Collection<GgenIndividual> getIndividuals(GgenState state) {
        return state.getHistory().allUniqueIndividualsByComponentValues().keySet();
    }
}
