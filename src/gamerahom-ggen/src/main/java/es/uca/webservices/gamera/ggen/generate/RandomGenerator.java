package es.uca.webservices.gamera.ggen.generate;

import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;

/**
 * Generates new individuals randomly using the generator in the YAML file.
 * @author Álvaro Galán Piñero
 * @version 1.0
 */
public class RandomGenerator implements GgenIndividualGenerator {
    private Random prng;

    public GgenIndividual generate(GgenPopulation src, IStrategy strategy, List<IType> types) throws GenerationException {
		final GgenIndividual ind = new GgenIndividual();
		ind.populate(strategy, types);
		return ind;
	}

	public void validate() throws InvalidConfigurationException {
        if (prng == null) {
            throw new InvalidConfigurationException("The PRNG has not been set yet");
        }
	}

	@Override
	public void setPRNG(Random prng) {
        this.prng = prng;
	}
}
