package es.uca.webservices.gamera.ggen.api.select;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

public class GASelectionOperatorOptions {

	private double percent;

	/**
	 * Returns the percentage of the individuals in the next generation
	 * which should be produced using this selection operator.
	 */
	public double getPercent() {
		return percent;
	}

	/**
	 * Changes the percentage of the individuals in the next generation
	 * which should be produced using this selection operator.
	 * @param percent New percentage to be used, within the (0, 1] range.
	 */
	public void setPercent(double percent) {
		this.percent = percent;
	}

	/**
	 * Checks if the options used for this selection operator are valid.
	 * @throws InvalidConfigurationException The options used are not valid.
	 */
	public void validate() throws InvalidConfigurationException {
		if (percent <= 0 || percent > 1) {
			throw new InvalidConfigurationException("Percent must be within the (0, 1] range");
		}
	}

}

