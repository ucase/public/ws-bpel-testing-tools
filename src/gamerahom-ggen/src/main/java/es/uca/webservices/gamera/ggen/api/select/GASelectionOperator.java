package es.uca.webservices.gamera.ggen.api.select;

import java.util.Random;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;

public interface GASelectionOperator {

    /**
     * Selects an individual from the source population, and returns it.
     *
     * @param source
     *            Source population.
     * @throws ComparisonException 
     */
    GgenIndividual select(GgenPopulation source);

    /**
     * Validates that the selection operator has been correctly configured.
     *
     * @throws InvalidConfigurationException
     *             The operator was not correctly configured.
     */
    void validate() throws InvalidConfigurationException;

    /**
     * Sets the pseudo-random number generator used to select individuals.
     */
	void setPRNG(Random prng);
}
