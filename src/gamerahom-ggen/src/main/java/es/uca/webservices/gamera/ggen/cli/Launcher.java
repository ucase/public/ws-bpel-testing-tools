package es.uca.webservices.gamera.ggen.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.ggen.GeneticAlgorithm;
import es.uca.webservices.gamera.ggen.api.generate.GAIndividualGeneratorOptions;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.conf.Configuration;
import es.uca.webservices.gamera.ggen.conf.ConfigurationLoader;
import es.uca.webservices.gamera.ggen.generate.FixedGenerator;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * Main command line launcher for GAmeraHOM-ggen.
 * @autor alvaro, Antonio García-Domínguez
 */
public class Launcher {
	private static final Logger LOGGER = LoggerFactory.getLogger(Launcher.class);

    static final String HELP_OPTION = "help";
    static final String INITIAL_OPTION = "initial";
    static final String SEED_OPTION = "seed";
    static final String POPSIZE_OPTION = "popsize";
    static final String LOG4JCONF_OPTION = "logconf";

    private static final int NUM_ARGUMENTS = 1;
	private static final String USAGE = "rodan [options] file.yaml";
	private static final String VERSION_PROPERTY = "gamerahom-ggen.version";
	private static Properties properties;

    private boolean argHelp = false;
    private File initialPopulationDataFile;
    private String seed;
    private Integer populationSize;

    public static void main(String[] args) throws InvalidConfigurationException,
            ClassNotFoundException, ParserException, GenerationException, ComparisonException,
            IOException, AnalysisException, PreparationException
    {
        final Launcher launcher = new Launcher();
		try {
			launcher.run(args, System.out);
		} catch (OptionException e) {
			LOGGER.error("Invalid option", e);
			launcher.printUsage(launcher.optionsArgs());
			System.exit(1);
		} catch (FileNotFoundException e) {
			LOGGER.error("File not found", e);
			launcher.printUsage(launcher.optionsArgs());
			System.exit(2);
		} catch (Exception e) {
			LOGGER.error("There was an error while test cases were generated", e);
			System.exit(254);
		}
	}

    public void run(String[] args, PrintStream out) throws IOException,
            ClassNotFoundException, InvalidConfigurationException, GenerationException,
            ComparisonException, ParserException, PreparationException, AnalysisException
    {
		List<String> nonOptionArgs = parseArgs(args);
		if (!argHelp) {
			final Configuration configuration = new ConfigurationLoader().parse(new File(nonOptionArgs.get(0)));
			if (initialPopulationDataFile != null) {
				addFixedInitialPopulationGenerator(configuration);
			}
			if (seed != null) {
				configuration.setSeed(seed);
			}
			if (populationSize != null) {
				configuration.setPopulationSize(populationSize);
			}

			GeneticAlgorithm genAlgorithm = new GeneticAlgorithm();
			genAlgorithm.setConfiguration(configuration);
			genAlgorithm.run();
		}
	}

    private void addFixedInitialPopulationGenerator(Configuration conf) {
        final Map<GgenIndividualGenerator,GAIndividualGeneratorOptions> existingGenerators = new HashMap<GgenIndividualGenerator,GAIndividualGeneratorOptions>(conf.getIndividualGenerators());
        conf.getIndividualGenerators().clear();

        final FixedGenerator fixedGenerator = new FixedGenerator();
        fixedGenerator.setPRNG(conf.getPrng());
        fixedGenerator.setDataFile(initialPopulationDataFile);
        final GAIndividualGeneratorOptions opts = new GAIndividualGeneratorOptions();
        opts.setOnlyFirstPopulation(true);
        opts.setPercent(1);

        conf.getIndividualGenerators().put(fixedGenerator, opts);
        conf.getIndividualGenerators().putAll(existingGenerators);
    }

    private List<String> parseArgs(String[] args) throws IOException {
        final OptionSet options = optionsArgs().parse(args);
        final List<String> nonOptionArgs = options.nonOptionArguments();

        if (options.has(HELP_OPTION)) {
            printUsage(optionsArgs());
            argHelp = true;
            return nonOptionArgs;
        }

        if (options.has(INITIAL_OPTION)) {
            initialPopulationDataFile = (File)options.valueOf(INITIAL_OPTION);
        }
        if (options.has(SEED_OPTION)) {
            seed = (String)options.valueOf(SEED_OPTION);
        }
        if (options.has(POPSIZE_OPTION)) {
            populationSize = (Integer)options.valueOf(POPSIZE_OPTION);
        }

        if (nonOptionArgs.size() != NUM_ARGUMENTS) {
            throw new IllegalArgumentException("Wrong number of arguments");
        }
        validateExtension(nonOptionArgs.get(0));

        return nonOptionArgs;
    }

    private OptionParser optionsArgs() {
        OptionParser parser = new OptionParser();
        parser.accepts(HELP_OPTION, "Provides help for using this program");
        parser.accepts(INITIAL_OPTION, "Uses the data file F as initial population, instead of a random one")
                .withRequiredArg().ofType(File.class).describedAs("F");
        parser.accepts(SEED_OPTION, "Uses S as the seed for the pseudo-random number generator")
                .withRequiredArg().ofType(String.class).describedAs("S");
        parser.accepts(POPSIZE_OPTION, "Sets the population size to PS")
                .withRequiredArg().ofType(Integer.class).describedAs("PS");

        return parser;
    }

    private void validateExtension(String file) {
        String extension;
        final String[] extensions = file.split("\\.");
        extension = extensions.length > 1 ? extensions[extensions.length - 1].toLowerCase().trim() : "";
        if (!"yaml".equals(extension) && !"yml".equals(extension)) {
            throw new IllegalArgumentException("Incorrect file extension: the " +
                    "configuration file must be a .yaml file");
        }
    }

	private void printUsage(OptionParser parser) throws IOException {
		System.out.println("GAmeraHOM-ggen version " + getVersion());
        System.out.println("Usage: " + USAGE);
		parser.printHelpOn(System.err);
	}

	private String getVersion() throws IOException {
		return getProperties().getProperty(VERSION_PROPERTY);
	}

	private synchronized Properties getProperties() throws IOException {
		InputStream in = null;
		try {
			if (properties == null) {
				properties = new Properties();
				in = Launcher.class
						.getResourceAsStream("/gameraHOM-ggen.properties");
				properties.load(in);
			}
			return properties;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}
}
