package es.uca.webservices.gamera.ggen.genetic.crossover;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.individuals.Component;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.testgen.api.generators.GenerationException;

/**
 * 
 * This class performs xovers by selecting a single crossover point and
 * exchanging the components that follow it between two individuals. As an
 * 
 * example:
 * <ol>
 * <li>Let A (a, b, c) and B (d, e, f) be two individuals with three components
 * each.</li>
 * <li>We select a crossover point: for instance, it could be between the first
 * and second components.</li>
 * <li>The resulting individuals would be A'(a, e, f) and B'(d, b, c).</li>
 * </ol>
 * 
 * @author Álvaro Cortijo-García, Álvaro Galán-Piñero, Antonio García-Domínguez
 */
public class ComponentCrossoverOperator implements GACrossoverOperator {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ComponentCrossoverOperator.class);

	private Random prng;

	@Override
	public List<Integer> apply(GAGeneticOperatorOptions opts, GgenIndividual left, GgenIndividual right) throws GenerationException {

		final int nc1 = left.getNumberComponents();
		final int nc2 = right.getNumberComponents();

		if (nc1 != nc2) {
			throw new IllegalArgumentException(
					String.format(
							"Cannot apply this operator: the parents have different number of components (%d and %d)",
							nc1, nc2));
		}
		else if (nc1 < 2) {
			throw new IllegalArgumentException(
					String.format(
							"Cannot apply this operator: the parents have less than two components (%d)",
							nc1));
		}
		
		// We select the crossover position: 0 is disallowed so we won't end up doing a simple swapping by mistake
		final int position = 1 + prng.nextInt(nc1 - 1);
		doCrossover(left, right, position);
		final List<Integer> crossPositions = new ArrayList<Integer>();
		for(int i = position; i < nc1; i++){
			crossPositions.add(i);
		}
		return crossPositions;
	}
	
	void doCrossover (GgenIndividual left, GgenIndividual right, int position)
	{
        assert position > 0 && position < left.getNumberComponents()
            	: "The crossover point must be between 1 and the individual size minus one";
		
		final List<Component> leftComponents = left.getComponents();
		final List<Component> rightComponents = right.getComponents();

		LOGGER.debug("xover before: {}, {}", left, right);
		for (int i = position; i < left.getNumberComponents(); i++) {
			final Component tmp = left.getComponent(i);
			leftComponents.set(i, right.getComponent(i));
			rightComponents.set(i, tmp);
		}
		LOGGER.debug("xover after: {}, {}", left, right);
	}
	
	@Override
	public void validate() throws InvalidConfigurationException {
		if (prng == null) {
			throw new InvalidConfigurationException("PRNG has not been set");
		}
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}
}
