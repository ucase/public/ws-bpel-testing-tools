package es.uca.webservices.gamera.ggen.term;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Termination condition that stops the algorithm if a certain percentage of all mutants have been killed.
 *
 * @author Álvaro Galán Piñero, Antonio Garcia-Dominguez
 * @version 1.0
 */

public class PercentAllMutantsCondition implements GATerminationCondition{
	private double percent;

	/**
	 * Returns the percent of killed mutants
	 * @return
	 */
	public double getPercent() {
		return percent;
	}

	/**
	 * Sets the percent of killed mutants
	 * @param percent
	 */
	public void setPercent(double percent) {
		this.percent = percent;
	}

	/**
	 * Evaluates if the percent of killed mutants is greater than the configured percentage.
	 */
    @Override
	public boolean evaluate(GgenState state) {
		return calculatePercentKilledMutants(state.getHistory().lastEntry().getComparisonsByProgram()) >= percent;
	}

    @Override
	public void validate() throws InvalidConfigurationException {
		if (getPercent() < 0 || getPercent() > 1) {
			throw new InvalidConfigurationException ("Percent of mutants must be between 0-1");
		}
	}

    /**
     * Calculates the percent of mutants that have been killed.
     */
    public double calculatePercentKilledMutants(ComparisonResults[] results) {
        int numberKilledMutants = 0;
        int numberTotalMutants = 0;

        for (int i = 0; i < results.length; i++) {
            ComparisonResult[] res = results[i].getRow();
            numberTotalMutants++;
            for (int j = 0; j < res.length; j++) {
                if (res[j].isDead()) {
                    numberKilledMutants++;
                    break;
                }
            }
        }
        return (double)numberKilledMutants/numberTotalMutants;
    }

}
