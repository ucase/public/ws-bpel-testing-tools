package es.uca.webservices.gamera.ggen.api.genetic.mutation;

import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;

/**
 * Base interface for a mutation operator.
 *
 * @author Álvaro Cortijo-García, Antonio García-Domínguez
 */
public interface GAMutationOperator {

	/**
	 * Performs a mutation on a <code>value</code> of a certain
	 * <code>type</code>, with a particular <code>strategy</code> for generating
	 * new random values and indicating that the probability of the mutation so
	 * far has been <code>probability</code> (e.g. for mutation range scaling
	 * and so on).
	 *
	 * If <code>value</code> is a tuple:
	 *   - If <code>componentsToMutate</code> is not empty, a new tuple with those
	 *     components mutated is returned.
	 *   - If <code>componentsToMutate</code> is empty, the operator will decide
	 *     which elements to mutate on its own.
	 *
	 * <code>componentsToMutate</code> is ignored for the other types.
	 */
	Object doMutation(double probability, IStrategy strategy, IType type, Object value, List<Integer> componentsToMutate) throws GenerationException;
	
	/**
	 * Validates that the mutation operator has been correctly configured.
	 * 
	 * @throws InvalidConfigurationException
	 *             The mutation operator has not been correctly configured.
	 */
	void validate() throws InvalidConfigurationException;

	/**
	 * Sets the pseudo-random number generator to be used.
	 * 
	 * @param prng
	 *            Pseudo-random number generator.
	 */
	void setPRNG(Random prng);

}