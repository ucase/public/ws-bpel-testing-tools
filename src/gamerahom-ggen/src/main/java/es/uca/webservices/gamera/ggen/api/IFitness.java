package es.uca.webservices.gamera.ggen.api;

import java.io.IOException;
import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.ggen.api.log.GALogger;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;
import es.uca.webservices.testgen.api.formatters.IFormatter;

public interface IFitness {

	/**
	 * Prepares the fitness function for evaluation. This function should be called once before the first
	 * invocation of {@link #computeFitness}.
	 * @param logger TODO
	 */
	public abstract void prepare(AnalysisResults analysis,
			List<List<Integer>> individuals, GALogger logger)
			throws InvalidConfigurationException;

	public abstract List<GAIndividual> getIndividuals();

	/**
	 * Computes the fitness of the individuals in a population.
	 * @param executor
	 * @param population
	 * @param formatter
	 * @param state
	 * @param logger
	 * @throws IOException
	 * @throws ComparisonException
	 */
	public abstract ComparisonResults[] computeFitness(GAExecutor executor,
			GgenPopulation population, IFormatter formatter, GgenState state,
			GALogger logger) throws IOException, ComparisonException;

}