package es.uca.webservices.gamera.ggen.individuals;

import es.uca.webservices.testgen.api.generators.IType;

/**
 * Component class. An individual is formed by some components.
 * @author Álvaro Galán Piñero
 * @version 1.0
 */

public class Component {
	
	private IType type;
	private Object value;
	/**
	 * Constructor
	 * @param type
	 * @param value
	 */
	public Component (IType type, Object value) {
		this.type = type;
		this.value = value;
	}
	/**
	 * Returns the type of the component
	 * @return The type of the component
	 */
	public IType getType() {
		return type;
	}
	/**
	 * Sets the type of the compenent
	 * @param type
	 */
	public void setType(IType type) {
		this.type = type;
	}
	/**
	 * Returns the value of the component
	 * @return The value of the component
	 */
	public Object getValue() {
		return value;
	}
	/**
	 * Sets the value of the component
	 * @param value
	 */
	public void setValue(Object value) {
		this.value = value;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Component component = (Component) o;

        if (type != null ? !type.equals(component.type) : component.type != null) return false;
        if (value != null ? !value.equals(component.value) : component.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }	
    
}
