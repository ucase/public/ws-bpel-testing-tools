package es.uca.webservices.gamera.ggen.log;

import es.uca.webservices.gamera.ggen.individuals.GgenState;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Logger that generates a report listing all the individuals generated by the algorithm, divided
 * by generation.
 */
public class FullHistoryReportLogger extends NullLogger {
    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public void finished(GgenState state) throws IOException {
        FileWriter fW = new FileWriter(file);
        try {
            fW.write(state.getHistory().toString());
        } finally {
            fW.close();
        }
    }
}
