package es.uca.webservices.gamera.ggen.genetic.mutation;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.AbstractNumber;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTuple;

/**
 * This class implements the mutation of the individuals.
 * 
 * @author Álvaro Cortijo-García, Álvaro Galán Piñero, Antonio García-Domínguez
 */
public class MutationOperator implements GAMutationOperator {
	private static final Logger LOGGER = LoggerFactory.getLogger(MutationOperator.class);

	private double mutationRange;
	private GAMutationOperator compositeMutOp = this;
	private Random prng;

	@Override
	@SuppressWarnings("unchecked")
	public Object doMutation(final double probability, final IStrategy strategy, final IType type, Object value, final List<Integer> componentsToMutate) throws GenerationException {
		if (type instanceof TypeTuple && componentsToMutate != null && !componentsToMutate.isEmpty()){
			// Do the mutation of the selected values of the individual
			final List<Object> newTuple = new ArrayList<Object>((List<Object>)value);
			for(int position : componentsToMutate){
				final IType componentType = ((TypeTuple) type).getIType(position);
				final Object componentValue = ((List<Object>) value).get(position);
				final Object newComponentValue = selectMutationByType(probability, strategy, componentType, componentValue);
				// Change the selected values of the individual, replacing the old value with
				// a new value.
				newTuple.set(position, newComponentValue);
			}
			return newTuple;
		} else{
			return selectMutationByType(probability, strategy, type, value);	
		}		
	}

	@SuppressWarnings("unchecked")
	private Object selectMutationByType(final double probability,
			final IStrategy strategy, final IType type, final Object value)
			throws GenerationException {
		if (type instanceof TypeString) {
			return doMutationString(strategy, (TypeString)type, (String)value);
		}
		else if (type instanceof TypeInt) {
			return doMutationInt(probability, (TypeInt)type, value);
		}
		else if (type instanceof TypeFloat) {
			return doMutationFloat(probability, (TypeFloat)type, value);
		}
		else if (type instanceof TypeList) {
			return doMutationList(probability, strategy, (TypeList)type, (List<Object>)value);
		}
		else if (type instanceof TypeTuple) {
			return doMutationTuple(probability, strategy, (TypeTuple)type, (List<Object>)value);
		}
		else {
			throw new IllegalArgumentException("Unknown type " + type);
		}
	}
	
	/**
	 * Returns the maximum range of a numeric mutation.
	 */
	public double getMutationRange() {
		return mutationRange;
	}

	/**
	 * Changes the maximum range of a numeric mutation.
	 */
	public void setMutationRange(double mutationRange) {
		this.mutationRange = mutationRange;
	}

	public GAMutationOperator getCompositeMutOp() {
		return compositeMutOp;
	}

	public void setCompositeMutOp(GAMutationOperator compositeMutOp) {
		this.compositeMutOp = compositeMutOp;
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (mutationRange <= 0) {
			throw new InvalidConfigurationException ("Constant mutation must be greater than 0");
		}
		if (prng == null) {
			throw new InvalidConfigurationException("PRNG has not been set");
		}

		if (compositeMutOp == null) {
			throw new InvalidConfigurationException("compositeMutOp must not be null");
		}
		else if (compositeMutOp != this) {
			compositeMutOp.setPRNG(getPRNG());
			compositeMutOp.validate();
		}
	}

	public Random getPRNG() {
		return this.prng;
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}	

	protected BigDecimal doMutationFloat(final double probability, TypeFloat type, Object value) {
		if (type.getAllowedValues() != null && !type.getAllowedValues().isEmpty()) {
			return pickRandomAllowedValue(type);
		} else {
			return applyRandomDeltaToFloat(probability, type, value);
		}
	}

	protected BigInteger doMutationInt(final double probability, TypeInt type, Object value) {
		if (type.getAllowedValues() != null && !type.getAllowedValues().isEmpty()) {
			return pickRandomAllowedValue(type);
		} else {
			return applyRandomDeltaToInt(probability, type, value);
		}
	}

	protected String doMutationString(IStrategy strategy, TypeString type, String value) throws GenerationException {
		return (String) strategy.generate(type)[0];
	}

	protected List<Object> doMutationList(final double probability, final IStrategy strategy, final TypeList type, final List<Object> value) throws GenerationException{
		final List<Object> l = new ArrayList<Object>(value);

		// Empty list, variable size (maxSize > minSize): we can only add elements.
		// Empty list, fixed size: we can't do anything.
		// Non-empty list, variable size: we can add, remove or change elements.
		// Non-empty list, fixed size: we can only change elements.
		if (type.getMaxNumElement() > type.getMinNumElement() && (l.isEmpty() || prng.nextBoolean())) {
			return doMutationListSize(probability, strategy, type, l);
		}
		else if (!l.isEmpty()) {
			final int randomPosition = prng.nextInt(l.size());
			final Object newValue = compositeMutOp.doMutation(probability, strategy, type.getType(), l.get(randomPosition), null);
			l.set(randomPosition, newValue);
		}
		return l;
	}	

	
	protected List<Object> doMutationTuple(final double probability, final IStrategy strategy, final TypeTuple type, final List<Object> value) throws GenerationException{
		final List<Object> l = new ArrayList<Object>(value);
		if (!l.isEmpty()) {
			final int randomPosition = prng.nextInt(l.size());
			final Object newValue = compositeMutOp.doMutation(probability, strategy, type.getIType(randomPosition), l.get(randomPosition), null);
			l.set(randomPosition, newValue);
		}
		return l;
	}	
	
	List<Object> doMutationListSize (final double probability, IStrategy strategy, TypeList list, List<Object> oldList)
			throws GenerationException
	{
		assert list.getMaxNumElement() > list.getMinNumElement() : "The list size should not be constant";

		// The largest possible mutation range is adding or removing 0.5*(maxSize-minSize) elements.
		final double range = (list.getMaxNumElement() - list.getMinNumElement())/2.0;
		// Scale the range by probability: the smallest mutation range is adding or removing 1 element. 
		final double scaledRange = (1 - probability) * range;
		// The old size is changed by at least 1 element
		final int oldSize = oldList.size();
		
		int sign;
		if (oldSize == list.getMinNumElement()) {
			sign = 1;
		}
		else if (oldSize == list.getMaxNumElement()) {
			sign = -1;
		}
		else {
			sign = prng.nextBoolean() ? -1 : 1;
		}

		final int delta = scaledRange < 1 ? sign : (int)Math.ceil(sign * prng.nextDouble() * scaledRange);

		final int newSize = Math.max(list.getMinNumElement(), Math.min(list.getMaxNumElement(), oldSize + delta));
		assert newSize >= list.getMinNumElement() : "new size >= " + list.getMinNumElement();
		assert newSize <= list.getMaxNumElement() : "new size <= " + list.getMaxNumElement();
		
		LOGGER.debug("Unscaled list size mutation range is {}", range);
		LOGGER.debug("Scaled list size mutation range is {} (probability is {})", scaledRange, probability);
		LOGGER.debug("Mutation delta is {} (new size is {} after clipping)", delta, newSize);

		// We need to create a new list so we don't accidentally mutate other
		// individuals that are reusing this list.
		//
		// Add as many elements as we need (or we can) from the old list
		final List<Object> newList = new ArrayList<Object>(newSize);
		final int minSize = Math.min(oldSize, newSize);
		for (int i = 0; i < minSize; ++i) {
			newList.add(oldList.get(i));
		}

		// Generate new elements beyond those in the old list if necessary
		final IType elementType = list.getType();
		for (int i = oldSize; i < newSize; ++i) {
			newList.add(strategy.generate(elementType)[0]);
		}

		return newList;
	}
	
	protected <T> T pickRandomAllowedValue(final AbstractNumber<T> type) {
		final List<T> allowed = type.getAllowedValues();
		final int randomPos = prng.nextInt(allowed.size());
		return allowed.get(randomPos);
	}

	private BigInteger applyRandomDeltaToInt(final double probability, TypeInt type, Object oldValue) {
		final BigInteger delta = BigInteger.valueOf((int)(1/probability * mutationRange * prng.nextFloat()));
	
		BigInteger old;
		if (oldValue instanceof BigInteger) {
			old = ((BigInteger)oldValue);
		}
		else {
			old = BigInteger.valueOf((Integer)oldValue);
		}
		
		// We add or subtract randomly
		BigInteger newValue;
		if (prng.nextBoolean()) {
			newValue = old.add(delta);
		} else {
			newValue = old.subtract(delta);
		}
		
		// We clip the value to the minimum and maximum values, if set
		if (type.getMinValue() != null && newValue.compareTo(type.getMinValue()) < 0) {
			newValue = type.getMinValue();
		}
		if (type.getMaxValue() != null && newValue.compareTo(type.getMaxValue()) > 0) {
			newValue = type.getMaxValue();
		}

		return newValue;
	}

	private BigDecimal applyRandomDeltaToFloat(final double probability, TypeFloat type, Object oldValue) {
		final double dDelta = 1/probability * mutationRange * prng.nextFloat();
		final BigDecimal delta = new BigDecimal(dDelta);
	
		BigDecimal old;
		if (oldValue instanceof Double) {
			old = new BigDecimal((Double)oldValue);
		}
		else if (oldValue instanceof Float) {
			old = new BigDecimal((Float)oldValue);
		}
		else {
			old = (BigDecimal)oldValue;
		}
	
		// We add or subtract randomly
		BigDecimal newValue;
		if (prng.nextBoolean()) {
			newValue = old.add(delta);
		} else {
			newValue = old.subtract(delta);
		}
	
		// Clip to the minimum and maximum values
		if (type.getMinValue() != null && newValue.compareTo(type.getMinValue()) < 0) {
			newValue = type.getMinValue();
		}
		if (type.getMaxValue() != null && newValue.compareTo(type.getMaxValue()) > 0) {
			newValue = type.getMaxValue();
		}
		return newValue;
	}
}
