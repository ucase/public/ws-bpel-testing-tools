package es.uca.webservices.gamera.ggen.genetic.mutation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.types.TypeList;
/**
 * This class implements the CustomProbabilityListMutationOperator.
 * 
 * @author Álvaro Cortijo-García, Antonio García-Domínguez.
 */
public class CustomProbabilityListMutationOperator extends MutationOperator {

	private double probabilityElimination;
	private double probabilityInsertion;
	private double probabilityMutationElement = 0.999;

	@Override
	protected List<Object> doMutationList(final double probability, final IStrategy strategy, final TypeList type, final List<Object> value) throws GenerationException{
		final List<Object> l = new ArrayList<Object>(value);

		final double removeProbability  = !(l.size() > type.getMinNumElement()) ? 0 : probabilityElimination;
		final double addProbability  = !(l.size() < type.getMaxNumElement()) ? 0 : probabilityInsertion;
		final double pme = (removeProbability + addProbability == 0)
				? probabilityMutationElement
				: 1 - removeProbability - addProbability;
		
		final Random prng = getPRNG();
		boolean mutatedAtLeastOneElement = false;
		final double p = 1 - Math.pow(1 - pme, 1.0/l.size());

		do {
			for (int i = 0; i < l.size(); i++) {
				if (prng.nextDouble() < p) {
					mutatedAtLeastOneElement = true;
					final Object newValue = getCompositeMutOp().doMutation(probability, strategy, type.getType(), l.get(i), null);
					l.set(i, newValue);
				}
			}

			if (!mutatedAtLeastOneElement && removeProbability + addProbability > 0) {
				final double x = (removeProbability + addProbability) * prng.nextDouble();
				final int randomPosition = prng.nextInt(l.size());
				if (x < removeProbability) {
					l.remove(randomPosition);
				} else {
					l.add(randomPosition,strategy.generate(type.getType())[0]);
				}
				mutatedAtLeastOneElement = true;
			}
		} while (!mutatedAtLeastOneElement && (l.size() > 0 || addProbability > 0));

		return l;
	}	
	
	@Override
	public void validate() throws InvalidConfigurationException {
		super.validate();

		if (probabilityElimination < 0 || probabilityElimination > 1) {
			throw new InvalidConfigurationException ("Remove probability must be greater or equal than 0 and less or equal than 1.");
		}
		if (probabilityInsertion < 0 || probabilityInsertion > 1) {
			throw new InvalidConfigurationException ("Add probability must be greater or equal than 0 and less or equal than 1.");
		}
		if (probabilityElimination + probabilityInsertion > 1) {
			throw new InvalidConfigurationException ("The sum of add and elimination probability must be less or equal than 1.");
		}
		if (probabilityMutationElement < 0 || probabilityMutationElement > 1) {
			throw new InvalidConfigurationException ("Mutation element probability must be greater or equal than 0 and less or equal than 1.");
		}
	}

	public double getProbabilityElimination() {
		return probabilityElimination;
	}

	public void setProbabilityElimination(double probabilityElimination) {
		this.probabilityElimination = probabilityElimination;
	}

	public double getProbabilityInsertion() {
		return probabilityInsertion;
	}

	public void setProbabilityInsertion(double probabilityInsertion) {
		this.probabilityInsertion = probabilityInsertion;
	}

	public double getProbabilityMutationElement() {
		return probabilityMutationElement;
	}

	public void setProbabilityMutationElement(double probabilityMutationElement) {
		this.probabilityMutationElement = probabilityMutationElement;
	}

}
