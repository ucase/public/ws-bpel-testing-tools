package es.uca.webservices.gamera.ggen;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rits.cloning.Cloner;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.api.util.ranges.StringArrayRangeListFilter;
import es.uca.webservices.gamera.ggen.api.IFitness;
import es.uca.webservices.gamera.ggen.api.generate.GAIndividualGeneratorOptions;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.gamera.ggen.api.log.GALogger;
import es.uca.webservices.gamera.ggen.api.select.GASelectionOperator;
import es.uca.webservices.gamera.ggen.api.select.GASelectionOperatorOptions;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.conf.Configuration;
import es.uca.webservices.gamera.ggen.individuals.Component;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;
import es.uca.webservices.gamera.ggen.util.FormatterUtilities;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.api.types.TypeTuple;

/**
 * Implements the main genetic algorithm.
 *
 * @author Álvaro Cortijo-García, alvaro, Antonio García-Domínguez
 */
public class GeneticAlgorithm {

	private static final Logger LOGGER = LoggerFactory.getLogger(GeneticAlgorithm.class);

	private Configuration configuration = new Configuration();
	private static final Cloner cloner = new Cloner();

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
	
	/**
	 * Runs the main algorithm and returns its final state.
	 */
	public GgenState run() throws InvalidConfigurationException, GenerationException,
		   ComparisonException, ParserException, IOException, PreparationException, 
		   AnalysisException
    {
		if (configuration.getFitness() == null) {
			// Install the default fitness function if necessary
			configuration.setFitness(new Fitness());
		}
		validate();

		// We load the spec file and we build the types in listTypes
		final GALogger logger = configuration.getAggregateLogger();
        final GgenState state = new GgenState();
        state.setConfiguration(configuration);
		logger.started(state);

        final List<IType> types = state.getTypes();
		logger.createdVariables(state);

        final GAExecutor executor = configuration.getExecutor();
        try {
            AnalysisResults analysis = executor.analyze(null);
            analysis = analysis.filterOperators(
                    new StringArrayRangeListFilter(
                            Arrays.asList(analysis.getOperatorNames()),
                            configuration.getIncludedOperators(),
                            configuration.getExcludedOperators()));

            final IFitness fitness = configuration.getFitness();
            fitness.prepare(analysis, configuration.getIndividuals(), logger);

            GgenPopulation nextPopulation = new GgenPopulation();
            do {
            	System.gc();

                if (state.getCurrentGeneration() == 0) {
                    generateNewIndividuals(new GgenPopulation(), nextPopulation, types, logger, state);
                    final File fDataFile = new File("firstPopulation.vm");
					FormatterUtilities.writeToFile(nextPopulation.getPopulation(), configuration.getFormatter(), fDataFile);
                    executor.loadDataFile(fDataFile);
                    executor.prepare();
                } else {
                    final GgenPopulation currentPopulation = nextPopulation;
                    nextPopulation = new GgenPopulation();
                    nextGeneration(currentPopulation, nextPopulation, types, logger, state);
                }

                assert nextPopulation.getPopulationSize() == configuration.getPopulationSize()
                        : "Population " + state.getCurrentGeneration() + " should have the expected number of individuals";

                final ComparisonResults[] results
                        = fitness.computeFitness(executor, nextPopulation, configuration.getFormatter(), state, logger);
                logger.newGeneration(state, nextPopulation);
                state.getHistory().add(nextPopulation, results);
            }
            while (!isDone(configuration.getTerminationConditions(), state, logger));

            logger.finished(state);
            return state;
        } finally {
            executor.cleanup();
        }
	}

	/**
	 * Creates the next generation from a previous generation
	 *
     * @param currentGeneration
	 * @param nextGeneration
	 * @param variables
	 * @param logger
	 * @param state
     */
    void nextGeneration(GgenPopulation currentGeneration, GgenPopulation nextGeneration, List<IType> variables, GALogger logger, GgenState state)
            throws GenerationException, ComparisonException,
            IOException, PreparationException, AnalysisException, InvalidConfigurationException
    {
		/**
		 * If assertions are enabled, store the original hashcode of the current
		 * generation - it should not change during the execution of this method.
		 */
    	boolean assertsEnabled = false;
    	assert assertsEnabled = true;
    	assert currentGeneration != nextGeneration;
    	final int currentHashcode = assertsEnabled ? currentGeneration.hashCode() : 0;

    	////////////////// RANDOM GENERATION AND SELECTION
    	
        // Creating the new individuals in the new population depends on the percentage of new individuals
        final IStrategy strategy = configuration.getStrategy();
        generateNewIndividuals(currentGeneration, nextGeneration, variables, logger, state);

        // The rest of the individuals will be selected from the previous generation and then
        // optionally modified through the crossover and mutation genetic operators.
		final GgenPopulation selected = new GgenPopulation();
		final int totalToSelect = currentGeneration.getPopulationSize() - nextGeneration.getPopulationSize();
        final Map<GASelectionOperator,GASelectionOperatorOptions> selOpe = configuration.getSelectionOperators();
        selectIndividuals(currentGeneration, selected, totalToSelect, selOpe);

        assert generationsAreDisjoint(currentGeneration, nextGeneration)
        	: "The next generation should not contain any of the GgenIndividual objects in the previous generation";
        assert currentGeneration.hashCode() == currentHashcode
        	: "Selecting the individuals for the next generation should not change the previous generation";

        ////////////////// GENETIC OPERATORS (IN-PLACE CROSSOVER AND MUTATION)
        final int sizePopulation = configuration.getPopulationSize();
        final Random prng = configuration.getPrng();
        final Map<GACrossoverOperator, GAGeneticOperatorOptions> crossover = configuration.getCrossoverOperators();
        final Map<GAMutationOperator, GAGeneticOperatorOptions> mutation = configuration.getMutationOperators();
        int indSelected = 0;

        while (nextGeneration.getPopulationSize() < sizePopulation) {
			final GgenIndividual individual1 = selected.getIndividual(indSelected++);
			final GgenIndividual individual2 = selected.getIndividual(indSelected++);
			logger.selectedIndividuals(individual1, individual2);
			final GgenIndividual newInd1 = new GgenIndividual(new ArrayList<Component>(individual1.getComponents()));
			final GgenIndividual newInd2 = new GgenIndividual(new ArrayList<Component>(individual2.getComponents()));
			boolean crossoverApplied = false;

			double crossoverOperatorRoll = prng.nextDouble();
			for (Map.Entry<GACrossoverOperator, GAGeneticOperatorOptions> entry : crossover.entrySet()) {
				final GACrossoverOperator cross = entry.getKey();
				final GAGeneticOperatorOptions crossOpts = entry.getValue();

				if (crossoverOperatorRoll < crossOpts.getProbability()) {
						List<Integer> crossPoints;
						crossPoints = cross.apply(crossOpts, newInd1, newInd2);
						logger.appliedCrossover(state, cross, individual1, individual2, newInd1, newInd2, crossPoints);
						crossoverApplied = true;
						assert currentGeneration.hashCode() == currentHashcode
								: "Operator " + cross.getClass().getName() + " should not modify the previous generation";

						// Exit the loop: a crossover operator has been applied, and we
						// do not want to apply more than one for a certain pair in one
						// generation.
						break;
				}
				else {
					// The operator was not applied: revise the operator roll and try with the next one 
					crossoverOperatorRoll -= crossOpts.getProbability();
				}
			}
			
			applyMutation(currentGeneration, currentHashcode, strategy, prng, mutation, newInd1, logger, state, crossoverApplied, 1);
			applyMutation(currentGeneration, currentHashcode, strategy, prng, mutation, newInd2, logger, state, crossoverApplied, 2);

			nextGeneration.addIndividual(newInd1);
			logger.addedIndividual(state, newInd1);
			if(nextGeneration.getPopulationSize() < sizePopulation) {
				nextGeneration.addIndividual(newInd2);
				logger.addedIndividual(state, newInd2);
			}
		} // while (nextGeneration.getPopulationSize() < sizePopulation)

        assert currentGeneration.hashCode() == currentHashcode
        	: "Applying the genetic operators on the current generation should not modify the previous one";
    }

	private void applyMutation(GgenPopulation currentGeneration,
			final int currentHashcode, final IStrategy strategy,
			final Random prng,
			final Map<GAMutationOperator, GAGeneticOperatorOptions> mutation,
			final GgenIndividual individual,
			final GALogger logger,
			final GgenState state,
			boolean individualFromCrossover,
			int childIndex) throws GenerationException {
			for (Map.Entry<GAMutationOperator, GAGeneticOperatorOptions> entry : mutation.entrySet()) {
				final GAMutationOperator mut = entry.getKey();
				final GAGeneticOperatorOptions mutOpts = entry.getValue();
				final List<Integer> componentsToMutate = new ArrayList<Integer>();
	
				selectComponentsToMutate(prng, individual, mutOpts, componentsToMutate);
				if (!componentsToMutate.isEmpty()) {
					final GgenIndividual indBeforeMutation = new GgenIndividual(new ArrayList<Component>(individual.getComponents()));
					apply(mut, mutOpts, individual, strategy, componentsToMutate);
					logger.appliedMutation(state, mut, componentsToMutate, indBeforeMutation, individual, individualFromCrossover, childIndex);
					break;
				}
	
				assert currentGeneration.hashCode() == currentHashcode : "Operator "
						+ mut.getClass().getName()
						+ " should not modify the previous generation";
			}
	}

	private void selectComponentsToMutate(final Random prng,
			final GgenIndividual individual,
			final GAGeneticOperatorOptions options,
			final List<Integer> componentsToMutate)
	{
		final double p = 1 - Math.pow(1 - options.getProbability(), 1.0/individual.getNumberComponents());

		for (int i = 0; i < individual.getNumberComponents(); i++) {
			if (prng.nextDouble() < p) {
				componentsToMutate.add(i);
			}
		}
	}
	
	/**
	 * Applies the mutation operator to two individuals.
	 * 
	 * @param mut
	 *            mutation operator to apply.
	 * @param opts
	 *            Operator-independent options set in the .yaml configuration
	 *            file, such as the probability of being applied.
	 * @param individual
	 *            Individual which will be mutated in-place. The caller should
	 *            ensure that this instance is not contained in any previous
	 *            population, to avoid interference between populations.
	 * @param strategy
	 *            Data generation strategy which should be applied if a
	 *            component needs to be entirely replaced.
	 * @throws GenerationException
	 */
	@SuppressWarnings("unchecked")
	public void apply(final GAMutationOperator mut,
			final GAGeneticOperatorOptions opts, GgenIndividual individual,
			final IStrategy strategy, final List<Integer> componentsToMutate)
			throws GenerationException {

			final List<Object> componentsValues = individual.getValues();
			final TypeTuple typeTuple = new TypeTuple(individual.getTypes());
			final List<Object> newTupleValue = (List<Object>) mut.doMutation(opts.getProbability(), strategy, typeTuple, componentsValues, componentsToMutate);

			for (int pos : componentsToMutate){
				individual.getComponents().set(pos, new Component(individual.getTypes().get(pos), newTupleValue.get(pos)));
			}
	}

	/**
	 * This function creates new individuals in the destination population from scratch.
     *
     * @param source
     *      Source population from which to extract information. If this population is empty, it is assumed that
     *      the <code>destination</code> population will be the first generation.
	 * @param destination
     *      Population in which all new individuals should be placed.
	 * @param variables
	 * @param gaLogger
	 * @param state
	 * @throws GenerationException
     *      There was a problem while generating an individual.
     */
    void generateNewIndividuals(GgenPopulation source, GgenPopulation destination, List<IType> variables, GALogger gaLogger, GgenState state) throws GenerationException {
        final boolean isFirstPopulation = source.getPopulationSize() == 0;
        final int sizePopulation = configuration.getPopulationSize();

        do {
            for (Entry<GgenIndividualGenerator, GAIndividualGeneratorOptions> ind : configuration.getIndividualGenerators().entrySet()) {
                if (!isFirstPopulation && ind.getValue().isOnlyFirstPopulation()) {
                    continue;
                }

                final int canGenerate = (int) Math.round(ind.getValue().getPercent() * sizePopulation);
                final int remaining = configuration.getPopulationSize() - destination.getPopulationSize();
                final int newIndividuals = Math.min(remaining, canGenerate);

                final GgenIndividualGenerator generate = ind.getKey();
                for (int i = 0; i < newIndividuals; ++i) {
                    GgenIndividual newInd = generate.generate(source, configuration.getStrategy(), variables);
                    destination.addIndividual(newInd);
                    gaLogger.generatedIndividual(state, generate, newInd);
                }
            }
        } while (isFirstPopulation && destination.getPopulationSize() < sizePopulation);
	}

	/**
	 * Select a number of individuals from a population. The number of individuals selected is always an even number, getting one
	 * individual more if it is necessary.
	 * 
	 * @param source
     *      Source population from which to select the individuals.
     * @param destination
     *      Population in which all the selected individuals should be placed.
     * @param selOpe
     * 		Selection operators used to extract the individuals from the source population.
	 */
	private void selectIndividuals(GgenPopulation source,
			GgenPopulation destination, final int totalToSelect,
			Map<GASelectionOperator, GASelectionOperatorOptions> selOpe)
	{
		// The number of individuals to be selected needs to be even, as we select in pairs
		final int finalToSelect = totalToSelect % 2 == 0 ? totalToSelect : totalToSelect + 1;

		int opCount = 1;
		final int numberOps = selOpe.entrySet().size();
		for (Entry<GASelectionOperator, GASelectionOperatorOptions> ope : selOpe.entrySet()) {
			final int remainingIndividuals = finalToSelect - destination.getPopulationSize();
			int individualsToSelect;
			if(opCount == numberOps){
				individualsToSelect = remainingIndividuals;
			} else{
				final double percentToSelect = (float) ope.getValue().getPercent();
				individualsToSelect = Math.min(
						// Remaining individuals until we reach the desired size
						remainingIndividuals,
						// Part of all individuals according to percentage
						(int) Math.round(percentToSelect * finalToSelect)
				);
			}

			int cont = 0;
			while (cont < individualsToSelect) {
				final GgenIndividual selected = ope.getKey().select(source);
				destination.addIndividual(cloner.deepClone(selected));
				cont ++;
			}
			opCount++;
		}

		assert destination.getPopulationSize() == finalToSelect
			: "The destination population should have " + finalToSelect + " individuals";
	}

	/**
	 * Evaluates if the termination condition is founded or not.
	 * @param term
	 * @param state
	 * @param logger
	 * @return <code>true</code> if the execution must be terminated, 
	 * or <code<false</code> in a different case.
	 */
	private boolean isDone(List<GATerminationCondition> term,
                           GgenState state, GALogger logger){
		for (GATerminationCondition cond : term){
			if (cond.evaluate(state)) {
				LOGGER.info("Termination condition {} is true: done", cond);
				logger.passedTerminationCondition(cond);
				return true;
			}
		}
		return false;
	}

	/**
	 * Tests that the objects in the previous generation are not in the next
	 * generation, using reference-equality semantics.
	 */
	private boolean generationsAreDisjoint(GgenPopulation prev, GgenPopulation next) {
		final Map<GgenIndividual, GgenIndividual> ind = new IdentityHashMap<GgenIndividual, GgenIndividual>();

		for (GgenIndividual i : prev.getPopulation()) {
			ind.put(i, i);
		}
		for (GgenIndividual i : next.getPopulation()) {
			if (ind.containsKey(i)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Validates the correct configuration
	 * @throws InvalidConfigurationException
	 */
	public void validate() throws InvalidConfigurationException {
		if (configuration == null) {
			throw new InvalidConfigurationException("The configuration must be set before running the algorithm");
		}
        configuration.validate();
	}

}
