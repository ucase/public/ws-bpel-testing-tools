package es.uca.webservices.gamera.ggen.log;

import java.io.File;
import java.util.Iterator;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;

/**
 * Abstract superclass for loggers that produce reports from the generations run by the algorithm.
 * @author Antonio Garcia-Dominguez
 */
public abstract class AbstractGenerationReportLogger extends NullLogger {
    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    protected String formatGeneration(Iterable<GgenIndividual> population, Iterable<ComparisonResult[]> comparisonResults) {
        final StringBuilder sb = new StringBuilder();

        final Iterator<ComparisonResult[]> it = comparisonResults.iterator();
        int count = 0;
        for (GgenIndividual ind : population) {
            if (count > 0) {
                sb.append("\n");
            }

            sb.append("INDIVIDUAL_");
            sb.append(count + 1);
            sb.append('\t');
            sb.append(ind);
            for (ComparisonResult cmp : it.next()) {
                sb.append('\t');
                sb.append(cmp.getValue());
            }

            ++count;
        }

        return sb.toString();
    }
}
