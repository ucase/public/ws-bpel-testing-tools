package es.uca.webservices.gamera.ggen.log;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.ggen.individuals.GgenHistory;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Logger that reports in which generation each unique individual killing different program mutants was generated.
 * @author Antonio Garcia-Dominguez
 */
public class UniqueByKillsGenerationReportLogger extends AbstractGenerationReportLogger {
    @Override
    public void finished(GgenState state) throws IOException {
        FileWriter fW = new FileWriter(getFile());
        PrintWriter pW = new PrintWriter(fW);
        try {
            final GgenHistory history = state.getHistory();
            final Set<List<ComparisonResult>> allUnique = new HashSet<List<ComparisonResult>>();

            int generation = 1;
            for (GgenHistory.Entry entry : history.getEntries()) {
                final Map<GgenIndividual, ComparisonResult[]> results = entry.getComparisonsByUniqueTestCase();
                final Map<GgenIndividual, ComparisonResult[]> uniqueResults = new LinkedHashMap<GgenIndividual, ComparisonResult[]>();
                for (Map.Entry<GgenIndividual, ComparisonResult[]> result : results.entrySet()) {
                    final ArrayList<ComparisonResult> lCmp = new ArrayList<ComparisonResult>(Arrays.asList(result.getValue()));
                    if (allUnique.add(lCmp)) {
                        uniqueResults.put(result.getKey(), result.getValue());
                    }
                }

                pW.println("Generation " + (generation++));
                pW.println(formatGeneration(uniqueResults.keySet(), uniqueResults.values()));
            }
        } finally {
            fW.close();
        }
    }
}
