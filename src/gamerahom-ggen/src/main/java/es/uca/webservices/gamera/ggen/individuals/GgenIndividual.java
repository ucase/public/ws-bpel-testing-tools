package es.uca.webservices.gamera.ggen.individuals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;

/**
 * A population is formed by some individuals
 * @author Álvaro Galán Piñero
 * @version 1.0
 */

public class GgenIndividual {

	private List<Component> components = new ArrayList<Component>();
	private double fitness = -1;

    /**
     * Creates an individual with no components initially created. It will have
     * to be populated through {@link #getComponents()} or {@link #populate}}.
     */
    public GgenIndividual() {
        // do nothing
    }

    /**
     * Creates an individual from an array of components.
     */
    public GgenIndividual(Component... components) {
        this.components.addAll(Arrays.asList(components));
    }

    /**
     * Creates an individual from a list of components.
     */
	public GgenIndividual(List<Component> components) {
		this.components = new ArrayList<Component>(components);
	}

	/**
	 * Populates an individual using a {@link IStrategy}.
	 * @throws GenerationException
	 */
	public void populate(IStrategy generator, Collection<IType> types) throws GenerationException {
		final IType[] vTypes = types.toArray(new IType[types.size()]);
		final Object[] vValues = generator.generate(vTypes);
		
		for (int i = 0; i < vTypes.length; i++) {
			addComponent(new Component(vTypes[i], vValues[i]));
		}
	}

	/**
	 * Returns the components of an individual.
	 */
	public List<Component> getComponents() {
		return components;
	}

	/**
	 * Returns the types of an individual.
	 */
	public List<IType> getTypes() {
        List<IType> types = new ArrayList<IType>();
        for (Component c : getComponents()) {
            types.add(c.getType());
        }
        return types;
    }

	/**
	 * Returns the values of an individual.
	 */
	public List<Object> getValues() {
        List<Object> values = new ArrayList<Object>();
        for (Component c : getComponents()) {
            values.add(c.getValue());
        }
        return values;
    }

	/**
	 * Returns the component at a specific position.
	 * @param pos Zero-based position of the component.
	 */
    public Component getComponent(int pos) {
        return components.get(pos);
    }

    public void addComponent(Component component) {
        components.add(component);
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public int getNumberComponents() {
        return components.size();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((components == null) ? 0 : components.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {	
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;	
		}
		GgenIndividual other = (GgenIndividual) obj;	
		if (components == null) {
			if (other.components != null) {
				return false;
		    }
		} else if (!components.equals(other.components)) {
			return false;
		}
			return true;
	}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        sb.append(getFitness());
        for(Component c : getComponents()) {
            sb.append(" ");
            sb.append(c.getValue());
        }
        sb.append('}');
        return sb.toString();
    }
}