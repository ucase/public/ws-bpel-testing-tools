package es.uca.webservices.gamera.ggen.log;

import java.io.IOException;
import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.gamera.ggen.api.log.GALogger;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Dummy logger that does not do anything. Useful as a placeholder or as a
 * superclass.
 * 
 * @author Álvaro Cortijo-García, Antonio García-Domínguez
 */
public class NullLogger implements GALogger {

    @Override
    public void started(GgenState state) {
        // nothing
    }

    @Override
    public void finished(GgenState state) throws IOException {
        // nothing
    }

    @Override
	public void createdVariables(GgenState state) {
		// nothing
	}

	@Override
	public void newGeneration(GgenState state, GgenPopulation population) {
		// nothing
	}

	@Override
	public void startedPopulationExecution(GgenState state) {
		// nothing
	}

	@Override
	public void finishedPopulationExecution(GgenState state) {
		// nothing
	}

	@Override
	public void startedEvaluation(GgenState state, GgenPopulation population) {
		// nothing
	}

	@Override
	public void finishedEvaluation(GgenState state, GgenPopulation population) {
		// nothing
	}

    @Override
    public void validate() throws InvalidConfigurationException {
        // nothing
    }

	@Override
	public void finishedComparison(GgenState state, List<ComparisonResults> results) {
		// nothing
	}
    
	@Override
	public void selectedIndividuals(GgenIndividual left, GgenIndividual right) {
		// nothing		
	}

	@Override
	public void appliedCrossover(GgenState state, GACrossoverOperator op, GgenIndividual father, GgenIndividual mother, GgenIndividual leftSon, GgenIndividual rightSon, List<Integer> crossPoints) {
		// nothing
	}

	@Override
	public void appliedMutation(GgenState state, GAMutationOperator op, List<Integer> mutatedComponents, GgenIndividual original, GgenIndividual mutated, boolean previousCrossover, int child) {
		// nothing		
	}

	@Override
	public void generatedIndividual(GgenState state, GgenIndividualGenerator gen, GgenIndividual ind) {
		// nothing		
	}

	@Override
	public void addedIndividual(GgenState state, GgenIndividual individual) {
		// nothing		
	}

	@Override
	public void startedComparison(GgenState state) {
		// nothing
	}

	@Override
	public void passedTerminationCondition(GATerminationCondition cond) {
		// nothing
	}

	@Override
	public void generatedProgramMutants(AnalysisResults analysis, List<GAIndividual> programMutants) {
		// nothing		
	}
    
}