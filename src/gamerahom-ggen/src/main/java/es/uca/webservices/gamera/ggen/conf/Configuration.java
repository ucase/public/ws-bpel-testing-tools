package es.uca.webservices.gamera.ggen.conf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.uncommons.maths.random.MersenneTwisterRNG;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.ggen.api.IFitness;
import es.uca.webservices.gamera.ggen.api.generate.GAIndividualGeneratorOptions;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.gamera.ggen.api.log.GALogger;
import es.uca.webservices.gamera.ggen.api.select.GASelectionOperator;
import es.uca.webservices.gamera.ggen.api.select.GASelectionOperatorOptions;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.log.AggregateLogger;
import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.api.generators.IRandomStrategy;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.parsers.IParser;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * @author Álvaro Cortijo-García, Álvaro Galán Piñero
 * @version 1.0
 */

public class Configuration {
	
	private static final int COORDINATES_MUTANTS = 3;

	private int populationSize = 0;
    private Integer removeKilledBy;
	private Random prng = null;

	private GAExecutor executor;

	private Map<GACrossoverOperator, GAGeneticOperatorOptions> crossoverOperators = new LinkedHashMap<GACrossoverOperator, GAGeneticOperatorOptions>();
	private Map<GAMutationOperator, GAGeneticOperatorOptions> mutationOperators = new LinkedHashMap<GAMutationOperator, GAGeneticOperatorOptions>();
	private Map<GgenIndividualGenerator, GAIndividualGeneratorOptions> individualGenerators = new LinkedHashMap<GgenIndividualGenerator, GAIndividualGeneratorOptions>();
	private Map<GASelectionOperator, GASelectionOperatorOptions> selectionOperators = new LinkedHashMap<GASelectionOperator, GASelectionOperatorOptions>();
	private List<GATerminationCondition> terminationConditions = new ArrayList<GATerminationCondition>();
	private List<GALogger> loggers = new ArrayList<GALogger>();

    private IParser parser;
	private IFormatter formatter;
	private IStrategy strategy;
    private IFitness fitness;

	private List<List<Integer>> individuals;
    private List<IType> types;

    private String includedOperators, excludedOperators;

    public int getPopulationSize() {
		return populationSize;
	}
	
	public void setPopulationSize (int populationSize){
		this.populationSize = populationSize;
	}

    public Integer getRemoveKilledBy() {
        return removeKilledBy;
    }

    public void setRemoveKilledBy(Integer removeKilledBy) {
        this.removeKilledBy = removeKilledBy;
    }

    public Random getPrng() {
    	if (prng == null) {
    		prng = new MersenneTwisterRNG();
    	}
		return prng;
	}

    public void setSeed(String seed) {
    	setSeedBytes(seed.getBytes());
    }
    
	public void setSeedBytes(byte[] seed) {
		prng = new MersenneTwisterRNG(seed);
	}
	
	public GAExecutor getExecutor() {
		return executor;
	}
	
	public void setExecutor(GAExecutor executor) {
		this.executor = executor;
	}

	public Map<GACrossoverOperator, GAGeneticOperatorOptions> getCrossoverOperators() {
		return crossoverOperators;
	}
	
	public void setCrossoverOperators(Map<GACrossoverOperator, GAGeneticOperatorOptions> crossoverOperators) {
		this.crossoverOperators = crossoverOperators;
	}
	
	public Map<GAMutationOperator, GAGeneticOperatorOptions> getMutationOperators() {
		return mutationOperators;
	}
	
	public void setMutationOperators(Map<GAMutationOperator, GAGeneticOperatorOptions> mutationOperators) {
		this.mutationOperators = mutationOperators;
	}

	public Map<GgenIndividualGenerator,GAIndividualGeneratorOptions> getIndividualGenerators() {
		return individualGenerators;
	}
	
	public void setIndividualGenerators(Map<GgenIndividualGenerator,GAIndividualGeneratorOptions> individualGenerators) {
		this.individualGenerators = individualGenerators;
	}
	
	public Map<GASelectionOperator,GASelectionOperatorOptions> getSelectionOperators() {
		return selectionOperators;
	}
	
	public void setSelectionOperators(Map<GASelectionOperator,GASelectionOperatorOptions> selectionOperators) {
		this.selectionOperators = selectionOperators;
	}
		
	public List<GATerminationCondition> getTerminationConditions() {
		return terminationConditions;
	}
	
	public void setTerminationConditions(List<GATerminationCondition> terminationConditions) {
		this.terminationConditions = terminationConditions;
	}
	
	public List<GALogger> getLoggers() {
		return loggers;
	}
	
	public void setLoggers(List<GALogger> loggers) {
		this.loggers = loggers;
	}
	
	public GALogger getAggregateLogger() {
        return new AggregateLogger(loggers);
    }
	
	private IParser getParser() {
		return parser;
	}

	public synchronized void setParser (IParser parser) {
		this.parser = parser;
	}
	
	public IFormatter getFormatter () {
		return formatter;
	}
	
	public void setFormatter (IFormatter formatter) {
		this.formatter = formatter;
	}
	
	public IStrategy getStrategy () {
		return strategy;
	}
	
	public void setStrategy(IStrategy generator) {
		this.strategy = generator;
	}
	
	public List<List<Integer>> getIndividuals() {
		return individuals;
	}

	public void setIndividuals(List<List<Integer>> individuals) {
		this.individuals = individuals;
	}

    public IFitness getFitness() {
        return fitness;
    }

    public void setFitness(IFitness fitness) {
        this.fitness = fitness;
    }

    public String getIncludedOperators() {
        return includedOperators;
    }

    public void setIncludedOperators(String includedOperators) {
        this.includedOperators = includedOperators;
    }

    public String getExcludedOperators() {
        return excludedOperators;
    }

    public void setExcludedOperators(String excludedOperators) {
        this.excludedOperators = excludedOperators;
    }

    /**
     * Ensures that all the settings in this configuration are valid and consistent with each other.
     */
	public void validate() throws InvalidConfigurationException {
        if (getPopulationSize() <= 0) {
            throw new InvalidConfigurationException("PopulationSize must be greater than 0");
        }
        if (getExecutor() == null) {
            throw new InvalidConfigurationException("Executor has not been defined");
        }
        if (getCrossoverOperators() == null || getCrossoverOperators().isEmpty()) {
            throw new InvalidConfigurationException("Crossover operators have not been defined");
        }
        if (getMutationOperators() == null || getMutationOperators().isEmpty()) {
            throw new InvalidConfigurationException("Mutation operators have not been defined");
        }
        if (getIndividualGenerators() == null || getIndividualGenerators().isEmpty()) {
            throw new InvalidConfigurationException("Individual generators have not been defined");
        }
        if (getSelectionOperators() == null || getSelectionOperators().isEmpty()) {
            throw new InvalidConfigurationException("Selection operators have not been defined");
        }
        if (getTerminationConditions() == null || getTerminationConditions().isEmpty()) {
            throw new InvalidConfigurationException("Terminations conditions have not been defined");
        }
        if (getLoggers() == null || getLoggers().isEmpty()) {
            throw new InvalidConfigurationException("Loggers have not been defined");
        }
        if (getParser() == null) {
            throw new InvalidConfigurationException("Parser has not been defined");
        }
        if (getFormatter() == null) {
            throw new InvalidConfigurationException("Formatter has not been defined");
        }

        final IStrategy strategy = getStrategy();
        if (strategy == null) {
            throw new InvalidConfigurationException("Generator has not been defined");
        }
        else if (strategy instanceof IRandomStrategy) {
        	((IRandomStrategy)strategy).setPRNG(getPrng());
        }

        if (getIndividuals() != null) {
            for (List<Integer> l : getIndividuals()) {
                if (l.size() != COORDINATES_MUTANTS) {
                    throw new InvalidConfigurationException(
                            String.format("Individuals should have exactly %d components, but found one with %d",
                                    COORDINATES_MUTANTS, l.size()));
                }
            }
        }

        getExecutor().validate();
        internalValidation();
    }

	public void internalValidation() throws InvalidConfigurationException {
        validateCrossoverOperators();
        validateMutationOperators();
        validateIndividualGenerators();
        validateSelectionOperators();

		for (GATerminationCondition terminationCondition : getTerminationConditions()) {
			terminationCondition.validate();
		}
		for (GALogger logger : getLoggers()) {
			logger.validate();
		}
	}

    private void validateSelectionOperators() throws InvalidConfigurationException {
        double totalPercent = 0;
        for (Map.Entry<GASelectionOperator, GASelectionOperatorOptions> selectionOperator : getSelectionOperators().entrySet()) {
            selectionOperator.getKey().setPRNG(getPrng());
            selectionOperator.getKey().validate();
            selectionOperator.getValue().validate();
            totalPercent = totalPercent + selectionOperator.getValue().getPercent();
        }
        if (Math.abs(totalPercent - 1) > 1e-4) {
            throw new InvalidConfigurationException("The sum of both selection operators must be one");
        }
    }

    private void validateIndividualGenerators() throws InvalidConfigurationException {
        for (Map.Entry<GgenIndividualGenerator, GAIndividualGeneratorOptions> individualGenerator : getIndividualGenerators().entrySet()) {
            individualGenerator.getKey().setPRNG(getPrng());
            individualGenerator.getKey().validate();
            individualGenerator.getValue().validate();
        }
    }

    private void validateCrossoverOperators() throws InvalidConfigurationException {
        double totalPercent = 0;
        for (Map.Entry<GACrossoverOperator, GAGeneticOperatorOptions> crossoverOperatorEntry : getCrossoverOperators().entrySet()) {
        	final GACrossoverOperator crossoverOperator = crossoverOperatorEntry.getKey();
        	final GAGeneticOperatorOptions options = crossoverOperatorEntry.getValue();
        	
            crossoverOperator.setPRNG(getPrng());
            crossoverOperator.validate();
            options.validate();
            totalPercent = totalPercent + options.getProbability();
        }
        if (totalPercent <= 0 || totalPercent > 1) {
            throw new InvalidConfigurationException("The sum of all crossover operators must be greater than zero and less or equal than one");
        }
    }

    private void validateMutationOperators() throws InvalidConfigurationException {
        double totalPercent = 0;
        for (Map.Entry<GAMutationOperator, GAGeneticOperatorOptions> mutationOperatorEntry : getMutationOperators().entrySet()) {
        	final GAMutationOperator mutationOperator = mutationOperatorEntry.getKey();
        	final GAGeneticOperatorOptions options = mutationOperatorEntry.getValue();
        	
            mutationOperator.setPRNG(getPrng());
            mutationOperator.validate();
            options.validate();
            totalPercent = totalPercent + options.getProbability();
        }
        if (totalPercent <= 0 || totalPercent > 1) {
            throw new InvalidConfigurationException("The sum of all mutation operators must be greater than zero and less or equal than one");
        }
    }

    public synchronized  List<IType> getTypes() throws ParserException {
        if (types == null) {
            types = getParser().parse();
        }
        return types;
    }
}

