package es.uca.webservices.gamera.ggen.term;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * This class implements the count termination condition. If the algorihm has more generations
 * than the value configured we stop the execution
 * @author Álvaro Galán Piñero
 * @version 1.0
 */
public class GenerationCountCondition implements GATerminationCondition{
	
	private int count;
	/**
	 * Returns the count value configured
	 * @return
	 */
	public int getCount() {
		return count;
	}
	/**
	 * Sets the count generation condition
	 * @param count
	 */
	public void setCount(int count) {
		this.count = count;
	}
	/**
	 * Validates the configuration
	 */
	public void validate() throws InvalidConfigurationException {
		if (count <= 0){
			throw new InvalidConfigurationException ("Count termination condition must be greater than 0");
		}
	}
	/**
	 * Evaluates if the execution must be stopped
	 */
	public boolean evaluate(GgenState state) {
		return state.getCurrentGeneration() >= count;
	}
}
