package es.uca.webservices.gamera.ggen.term;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.IFitness;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Termination condition which stops the algorithm if the average fitness
 * does not "noticeably" improve within a certain number of generations. An
 * improvement is "noticeable" if the value improves beyond a certain ratio
 * (by default, 1%).
 * 
 * @author Álvaro Galán Piñero, Antonio García-Domínguez
 * @version 1.1
 */
public class StagnationAverageFitness implements GATerminationCondition {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(StagnationAverageFitness.class);
	private int count;

    private Integer prevIndividuals;
    private int countAverageFitness = 0;
    private double averageFitness = Double.MIN_NORMAL;

    // By default, we require that the average improves by at least 1%
    // in order to consider the change to be noticeable.
    private double relativeMinimumChange = 0.01;

    /**
	 * Returns the number of generations that the average fitness should
	 * not improve beyond {@link #getRelativeMinimumChange()} so the algorithm
	 * stops.
	 */
	public int getCount() {
		return count;
	}

	/**
	 * Sets the number of generations that the average fitness should
	 * not improve beyond {@link #getRelativeMinimumChange()} so the algorithm stops.
	 */
    public void setCount(int count) {
        this.count = count;
	}
    
    /**
     * Returns the amount by which the new average should exceed the original
     * average to be considered a real improvement.
     * @return Minimum improvement (0 is 0% and 1 is 100%) 
     */
	public double getRelativeMinimumChange() {
		return relativeMinimumChange;
	}

    /**
     * Sets the amount by which the new average should exceed the original
     * average to be considered a real improvement.
     * @param relativeMinimumChange Minimum improvement (0 is 0% and 1 is 100%) 
     */
	public void setRelativeMinimumChange(double relativeMinimumChange) {
		this.relativeMinimumChange = relativeMinimumChange;
	}

	@Override
	public boolean evaluate(GgenState state) {
        final IFitness fitness = state.getConfiguration().getFitness();
        final List<GAIndividual> currentIndividuals = fitness.getIndividuals();
        if (currentIndividuals != null) {
            final int nCurrentIndividuals = currentIndividuals.size();
            if (prevIndividuals != null && nCurrentIndividuals < prevIndividuals) {
                countAverageFitness = 0;
                averageFitness = 0;
                LOGGER.debug("Reset due to change in the number of individuals ({} < {})", nCurrentIndividuals, prevIndividuals);
            }
            prevIndividuals = nCurrentIndividuals;
        }

        update(state.getCurrentPopulation());
		return countAverageFitness >= count;
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (count <= 0) {
			throw new InvalidConfigurationException("count must be > 0");
		}
		if (relativeMinimumChange <= 0) {
			throw new InvalidConfigurationException("relativeMinimumChange must be > 0");
		}
	}

    private void update(GgenPopulation population) {
        double totalFitness = 0;
        for (GgenIndividual ind : population.getPopulation()) {
            totalFitness += ind.getFitness();
        }

        final double newAverageFitness = totalFitness / population.getPopulationSize();
        final double improvement = newAverageFitness/averageFitness - 1;
        if (improvement >= relativeMinimumChange) {
            LOGGER.debug(String.format("Average fitness %g is greater than previous maximum %g by %g%% or more", newAverageFitness, averageFitness, relativeMinimumChange * 100));
            countAverageFitness = 0;
            averageFitness = newAverageFitness;
        } else {
            countAverageFitness++;
            LOGGER.debug(String.format("Average fitness %g is not better than previous maximum (%g, %d generations ago)", newAverageFitness, averageFitness, countAverageFitness));
        }
    }

}
