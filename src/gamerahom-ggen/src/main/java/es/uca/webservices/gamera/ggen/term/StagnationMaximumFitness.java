package es.uca.webservices.gamera.ggen.term;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.IFitness;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

import java.util.List;

/**
 * This class implements the maximum fitness stagnation termination condition. If the maximum fitness
 * of the population has not improved during a certain number of generations, the condition will be
 * triggered.
 *
 * @author Álvaro Galán Piñero, Antonio Garcia-Dominguez
 * @version 1.0
 */
public class StagnationMaximumFitness implements GATerminationCondition {

    private int count = 5;

    private int generationsWithoutImprovement = 0;
    private double maximumFitness = 0, relativeMinimumChange = 0.01;
    private Integer prevIndividualCount;

    /**
	 * Returns the number of generations that the maximum fitness
	 * should not change beyond {@link #getRelativeMinimumChange()} so the
	 * algorithm stops.
	 */
	public int getCount() {
		return count;
	}

	/**
	 * Changes the number of generations that the maximum fitness
	 * should not change beyond {@link #getRelativeMinimumChange()} so the
	 * algorithm stops.
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * Returns the ratio by which the new maximum fitness needs to exceed the
	 * previous maximum fitness in order to be considered an improvement.
	 * 
	 * @return Ratio by which the maximum fitness should improve so the change
	 * is "noticeable": 0.01 is 1%, 1 is 100%.
	 */	
	public double getRelativeMinimumChange() {
		return relativeMinimumChange;
	}

	/**
	 * Changes the amount by which the new maximum fitness needs to exceed the
	 * previous maximum fitness in order to be considered an improvement.
	 *
	 * @return Ratio by which the maximum fitness should improve so the change
	 * is "noticeable": 0.01 is 1%, 1 is 100%. 0 is not acceptable.
	 */ 
	public void setRelativeMinimumChange(double minimumChange) {
		this.relativeMinimumChange = minimumChange;
	}

	/**
	 * Evaluates if the maximum fitness has not changed during more generations than the configured value.
	 */
	@Override
	public boolean evaluate(GgenState state) {
        final IFitness fitness = state.getConfiguration().getFitness();
        final List<GAIndividual> currentIndividuals = fitness.getIndividuals();
        if (currentIndividuals != null) {
            final int nCurrentIndividuals = currentIndividuals.size();
            if (prevIndividualCount != null && nCurrentIndividuals < prevIndividualCount) {
                // Reset the stopping criteria when some of the mutants are removed
                maximumFitness = 0;
                generationsWithoutImprovement = 0;
            }
            prevIndividualCount = nCurrentIndividuals;
        }

        update(state.getCurrentPopulation());
        return generationsWithoutImprovement >= count;
	}

	@Override
	public void validate() throws InvalidConfigurationException {
        if (count <= 0) {
            throw new InvalidConfigurationException("count must be > 0");
        }
        if (relativeMinimumChange <= 0) {
        	throw new InvalidConfigurationException("relativeMinimumChange must be > 0");
        }
    }

    private void update(GgenPopulation population) {
        double fitness = 0;
        for (GgenIndividual ind : population.getPopulation()) {
            if (ind.getFitness() > fitness) {
                fitness = ind.getFitness();
            }
        }
        
        final double improvement = fitness/maximumFitness - 1;
        if (improvement > relativeMinimumChange) {
            generationsWithoutImprovement = 0;
            maximumFitness = fitness;
        } else {
            generationsWithoutImprovement++;
        }
    }

}
