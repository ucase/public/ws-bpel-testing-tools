package es.uca.webservices.gamera.ggen.api.generate;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Stores the options to be used by the genetic algorithm when applying
 * the individual generator that do <emph>not</emph> influence the behaviour
 * of the individual generator itself. For instance, it can be used to store
 * the percentage of individuals that should be generated with it.
 * 
 *  @author Antonio García-Domínguez
 */
public class GAIndividualGeneratorOptions {

	private double percent;
    private boolean onlyFirstPopulation = false;

	/**
	 * Returns the percentage of individuals (between 0 and 1) for the next population
	 * that should be generated with the associated individual generator.
	 */
	public double getPercent() {
		return percent;
	}

	/**
	 * Changes the percentage of individuals for the next population
	 * that should be generated with the associated individual generator.
	 *
	 * @param percent New percentage to be set, within the [0, 1] range. A zero value
	 * may be used in order to use the generator for the first generation, but not for
	 * the subsequent ones. During the first generation, the individual generator will
	 * be used in a round-robin fashion regardless of the value of the 'percent' option.
	 */
    public void setPercent(double percent) {
		this.percent = percent;
	}

	/**
	 * Checks if the options used for this individual generator are valid.
	 * 
	 * @throws InvalidConfigurationException
	 *             The options used are not valid.
	 */
	public void validate() throws InvalidConfigurationException {
		if (percent < 0 || percent > 1) {
			throw new InvalidConfigurationException("Percentage must be within the [0, 1] range");
		}
	}

    /**
     * Returns <code>true</code> if the individual generator will only be used when generating the first population.
     * Otherwise, it will return <code>false</code> and it will be used for generating all populations.
     *
     * By default, individual generators are used for generating all populations.
     */
    public boolean isOnlyFirstPopulation() {
        return onlyFirstPopulation;
    }

    /**
     * If <code>true</code>, the generator will only be used to generate the first population. Otherwise, it will be
     * used to generate all populations.
     */
    public void setOnlyFirstPopulation(boolean onlyFirstPopulation) {
        this.onlyFirstPopulation = onlyFirstPopulation;
    }
}
