package es.uca.webservices.gamera.ggen.api.log;

import java.io.IOException;
import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/** Base interface for a logger.
 * @author Álvaro Cortijo-García, Antonio García-Domínguez
 */

public interface GALogger {

	/**
	 * The algorithm has started its execution.
	 *
	 * @param state
	 *            State of the algorithm.
	 */
	void started(GgenState state);

	/**
	 * The algorithm has finished its execution.
	 *
	 * @param state
	 *            Final state of the algorithm after one of the termination
	 *            conditions has evaluated to a true value.
	 * @throws IOException
	 */
	void finished(GgenState state) throws IOException;

	/**
	 * The algorithm has parsed the variable specification.
	 */
	void createdVariables(GgenState state);

	/**
	 * The algorithm has produced a new generation.
	 *
	 * @param state
	 *            Current state of the algorithm.
	 * @param population
	 *            Population for this generation.
	 */
	void newGeneration(GgenState state, GgenPopulation population);

	/**
	 * The algorithm has started to execute the test cases in the current generation.
	 *
	 * @param state
	 *            Current state of the algorithm.
	 * @param population
	 *            Population to be compared with the original program.
	 */
	void startedPopulationExecution(GgenState state);

	/**
	 * The algorithm has finished executing the test cases in the current
	 * generation.
	 *
	 * @param state
	 *            Current state of the algorithm.
	 */
	void finishedPopulationExecution(GgenState state);

	/**
	 * The algorithm has started evaluating the fitness of the individuals in
	 * the current generation.
	 *
	 * @param state
	 *            Current state of the algorithm.
	 * @param population
	 *            Population which is going to be evaluated.
	 */
	void startedEvaluation(GgenState state, GgenPopulation population);

	/**
	 * The algorithm has finished evaluating the fitness of the individuals in
	 * the current generation.
	 *
	 * @param state
	 *            Current state of the algorithm.
	 * @param population
	 *            Population which was evaluated. Fitness values are contained
	 *            in the GAIndividual objects in the population.
	 */
	void finishedEvaluation(GgenState state, GgenPopulation population);

	/**
	 * Validate that the logger has been correctly configured.
	 *
	 * @throws InvalidConfigurationException
	 *             The logger was not correctly configured.
	 */
	void validate() throws InvalidConfigurationException;

	/**
	 * The algorithm has started the comparison of all the individuals in the
	 * current generation.
	 * 
	 * @param state
	 *            Current state of the algorithm.
	 */
	void startedComparison(GgenState state);

	/**
	 * The algorithm has done the comparison of all the individuals in the
	 * current generation.
	 * 
	 * @param state
	 *            Current state of the algorithm.
	 * @param results
	 *            Results of the comparison.
	 */
	void finishedComparison(GgenState state, List<ComparisonResults> results);

	/**
	 * The algorithm has selected two individuals to apply (or not) the
	 * crossover and/or mutation operators.
	 *
	 * @param left
	 *            First selected individual.
	 * @param right
	 *            Second selected individual.
	 */
	void selectedIndividuals(GgenIndividual left, GgenIndividual right);

	/**
	 * The algorithm has applied the crossover operator between two individuals.
	 * 
	 * @param state
	 *            Current state of the algorithm.
	 * @param father
	 *            Individual that acts as father in the crossover operation
	 * @param mother
	 *            Individual that acts as mother in the crossover operation
	 * @param leftSon
	 *            First individual son.
	 * @param rightSon
	 *            Second individual son.
	 * @param crossPoints
	 *            List of points used to do the crossover.
	 */
	void appliedCrossover(GgenState state, GACrossoverOperator op,
			GgenIndividual father, GgenIndividual mother,
			GgenIndividual leftSon, GgenIndividual rightSon,
			List<Integer> crossPoints);

	/**
	 * The algorithm has applied the mutation operator to an individual.
	 * 
	 * @param state
	 *            Current state of the algorithm
	 * @param mutatedComponents
	 *            Indices of the components that have been mutated.
	 * @param original
	 *            original individual.
	 * @param mutated
	 *            mutated individual.
	 * @param previousCrossover
	 *            indicates if crossover has been applied previously.
	 * @param child
	 *            indicates the order of the individual in the pair of the
	 *            children (1 for the first child or 2 for the second child).
	 */
	void appliedMutation(GgenState state, GAMutationOperator op,
			List<Integer> mutatedComponents, GgenIndividual original,
			GgenIndividual mutated, boolean previousCrossover, int child);

	/**
	 * The algorithm has created new individuals with an individual generator.
	 * 
	 * @param state
	 *            state of the algorithm
	 * @param op
	 *            operator which has generated the new individual.
	 * @param ind
	 *            newly generated individual.
	 */
	void generatedIndividual(GgenState state, GgenIndividualGenerator op, GgenIndividual ind);

	/**
	 * The algorithm has added a new individual to the next generation.
	 * 
	 * @param state
	 *            state of the algorithm
	 * @param individual
	 *            individual added to the next generation.
	 */
	void addedIndividual(GgenState state, GgenIndividual individual);
	
	/**
	 * A termination condition has been passed.
	 *
	 * @param cond
	 *            termination condition passed.
	 */
	void passedTerminationCondition(GATerminationCondition cond);
	
	/**
	 * The algorithm has generated the program mutants.
	 * 
	 * @param analysis
	 *            Analysis of the original program
	 * @param programMutants
	 *            program mutants generated.
	 */
	void generatedProgramMutants(AnalysisResults analysis, List<GAIndividual> programMutants);
}
