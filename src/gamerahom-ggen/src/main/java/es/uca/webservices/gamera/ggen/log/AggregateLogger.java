package es.uca.webservices.gamera.ggen.log;

import java.io.IOException;
import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.gamera.ggen.api.log.GALogger;
import es.uca.webservices.gamera.ggen.api.term.GATerminationCondition;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Class that contains all the loggers which should be notified.
 * 
 * @author Álvaro Cortijo-García, Antonio García-Domínguez
 */

public class AggregateLogger implements GALogger {

    private List<GALogger> loggers;

    /**
     * Creates a new aggregate logger.
     * 
     * @param loggers
     *            List of loggers which should be notified of each event. The
     *            list is not copied: if it is modified elsewhere, it will
     *            affect this aggregate logger. This class is <emph>not</emph>
     *            thread safe!
     */
    public AggregateLogger(List<GALogger> loggers) {
        this.loggers = loggers;
    }

    @Override
    public void finished(GgenState state) throws IOException {
        for (GALogger logger : loggers) {
            logger.finished(state);
        }
    }

    @Override
    public void started(GgenState state) {
        for (GALogger logger : loggers) {
            logger.started(state);
        }
    }

    @Override
    public void validate() throws InvalidConfigurationException {
        if (loggers == null || loggers.isEmpty()) {
            throw new InvalidConfigurationException(
                    "Aggregate logger should contain at least one logger");
        }
    }

	@Override
	public void newGeneration(GgenState state, GgenPopulation population) {
		for (GALogger logger : loggers) {
			logger.newGeneration(state, population);
		}
		
	}

	@Override
	public void startedPopulationExecution(GgenState state) {
		for (GALogger logger : loggers) {
			logger.startedPopulationExecution(state);
		}
	}

	@Override
	public void finishedPopulationExecution(GgenState state) {
		for (GALogger logger : loggers) {
			logger.finishedPopulationExecution(state);
		}			
	}
	
	@Override
	public void startedEvaluation(GgenState state, GgenPopulation population) {
		for (GALogger logger : loggers) {
			logger.startedEvaluation(state, population);
		}			
	}

	@Override
	public void finishedEvaluation(GgenState state, GgenPopulation population) {
		for (GALogger logger : loggers) {
			logger.finishedEvaluation(state, population);
		}	
	}

	@Override
	public void createdVariables(GgenState state) {
		for (GALogger logger : loggers) {
			logger.createdVariables(state);
		}		
	}

	@Override
	public void finishedComparison(GgenState state, List<ComparisonResults> results) {
		for (GALogger logger : loggers) {
			logger.finishedComparison(state, results);
		}		
	}
	
	public void selectedIndividuals(GgenIndividual left, GgenIndividual right) {
		for (GALogger logger : loggers) {
			logger.selectedIndividuals(left, right);
		}			
	}

	@Override
	public void appliedCrossover(GgenState state, GACrossoverOperator op, GgenIndividual father, GgenIndividual mother, GgenIndividual leftSon, GgenIndividual rightSon, List<Integer> crossPoints) {
		for (GALogger logger : loggers) {
			logger.appliedCrossover(state, op, father, mother, leftSon, rightSon, crossPoints);
		}			
	}

	@Override
	public void appliedMutation(GgenState state, GAMutationOperator op, List<Integer> mutatedComponents, GgenIndividual original, GgenIndividual mutated, boolean previousCrossover, int child) {
		for (GALogger logger : loggers) {
			logger.appliedMutation(state, op, mutatedComponents, original, mutated, previousCrossover, child);
		}			
	}

	@Override
	public void generatedIndividual(GgenState state, GgenIndividualGenerator gen, GgenIndividual ind) {
		for (GALogger logger : loggers) {
			logger.generatedIndividual(state, gen, ind);
		}			
	}

	@Override
	public void addedIndividual(GgenState state, GgenIndividual individual) {
		for (GALogger logger : loggers) {
			logger.addedIndividual(state, individual);
		}			
	}

	@Override
	public void startedComparison(GgenState state) {
		for (GALogger logger : loggers) {
			logger.startedComparison(state);
		}
	}

	@Override
	public void passedTerminationCondition(GATerminationCondition cond) {
		for (GALogger logger : loggers) {
			logger.passedTerminationCondition(cond);
		}
	}

	@Override
	public void generatedProgramMutants(AnalysisResults analysis, List<GAIndividual> programMutants) {
		for (GALogger logger : loggers) {
			logger.generatedProgramMutants(analysis, programMutants);
		}
	}

}
