package es.uca.webservices.gamera.ggen.log;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.ggen.individuals.GgenHistory;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Logger that reports in which generation each unique individual
 * with different components was generated.
 * @author Antonio Garcia-Dominguez
 */
public class UniqueByComponentsGenerationReportLogger extends AbstractGenerationReportLogger {
    @Override
    public void finished(GgenState state) throws IOException {
        FileWriter fW = new FileWriter(getFile());
        PrintWriter pW = new PrintWriter(fW);
        try {
            final GgenHistory history = state.getHistory();
            final Set<GgenIndividual> allUnique = new HashSet<GgenIndividual>();

            int generation = 1;
            for (GgenHistory.Entry entry : history.getEntries()) {
                final Map<GgenIndividual, ComparisonResult[]> results = entry.getComparisonsByUniqueTestCase();
                final Map<GgenIndividual, ComparisonResult[]> uniqueResults = new LinkedHashMap<GgenIndividual, ComparisonResult[]>();
                for (Map.Entry<GgenIndividual, ComparisonResult[]> result : results.entrySet()) {
                    if (allUnique.add(result.getKey())) {
                        uniqueResults.put(result.getKey(), result.getValue());
                    }
                }

                pW.println("Generation " + (generation++));
                pW.println(formatGeneration(uniqueResults.keySet(), uniqueResults.values()));
            }
        } finally {
            fW.close();
        }
    }
}
