package es.uca.webservices.gamera.ggen.genetic.crossover;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.ggen.api.genetic.crossover.GACrossoverOperator;
import es.uca.webservices.gamera.ggen.individuals.Component;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.testgen.api.generators.GenerationException;

/**
 * This class implements the Double Point Component Crossover Operator. Two
 * crossover points are selected in order to exchange the components of the
 * individuals that are between those two points.
 * 
 * @author Álvaro Cortijo-García
 *
 */
public class DoublePointComponentCrossoverOperator implements GACrossoverOperator {

	private final static Logger LOGGER = LoggerFactory.getLogger(ComponentCrossoverOperator.class);
	private Random prng;

	@Override
	public List<Integer> apply(final GAGeneticOperatorOptions opts, final GgenIndividual left, final GgenIndividual right) throws GenerationException {

		final int nc1 = left.getNumberComponents();
		final int nc2 = right.getNumberComponents();

		if (nc1 != nc2) {
			throw new IllegalArgumentException(
					String.format(
							"Cannot apply this operator: the parents have different number of components (%d and %d)",
							nc1, nc2));
		}
		else if (nc1 < 2) {
			throw new IllegalArgumentException(
					String.format(
							"Cannot apply this operator: the parents have less than two components (%d)",
							nc1));
		}
		
		// We select both crossover positions. We retry if the xover points
		// would not result in new individuals (no swapping if p1 == p2 or
		// full swapping if p1 == 0 and p2 == N).
		int xoverP1, xoverP2;
		do {
			xoverP1 = prng.nextInt(nc1 + 1);
			xoverP2 = prng.nextInt(nc1 + 1);
			if (xoverP1 > xoverP2) {
				final int aux = xoverP1;
				xoverP1 = xoverP2;
				xoverP2 = aux;
			}
		} while (xoverP1 == xoverP2	|| (xoverP1 == 0 && xoverP2 == nc1));

		doCrossover(left, right, xoverP1, xoverP2);
		final List<Integer> crossPositions = new ArrayList<Integer>();
		for(int i = xoverP1; i < xoverP2; i++){
			crossPositions.add(i);
		}
		return crossPositions;
	}

	void doCrossover (final GgenIndividual left, final GgenIndividual right, final int crossoverPosition1, final int crossoverPosition2)
	{		
        assert crossoverPosition1 >= 0 && crossoverPosition1 <= left.getNumberComponents()
        		: "The first crossover point must be between 0 and the size of the individual";
        assert crossoverPosition2 >= 0 && crossoverPosition2 <= left.getNumberComponents()
        		: "The second crossover point must be between 0 and the size of the individual";

		final List<Component> leftComponents = left.getComponents();
		final List<Component> rightComponents = right.getComponents();

		LOGGER.debug("xover before: {}, {}", left, right);
		for (int i = crossoverPosition1 ; i < crossoverPosition2; i++) {
			final Component tmp = left.getComponent(i);
			leftComponents.set(i, right.getComponent(i));
			rightComponents.set(i, tmp);
		}
		LOGGER.debug("xover after: {}, {}", left, right);
	}
	
	@Override
	public void validate() throws InvalidConfigurationException {
		if (prng == null) {
			throw new InvalidConfigurationException("PRNG has not been set");
		}
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}

}
