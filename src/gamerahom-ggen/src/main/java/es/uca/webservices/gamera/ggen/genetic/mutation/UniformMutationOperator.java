package es.uca.webservices.gamera.ggen.genetic.mutation;

import java.math.BigDecimal;
import java.math.BigInteger;

import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;

/**
 * Mutation operator that replaces integer and float values
 * with new values, chosen between their minimum and maximum
 * values according to a uniform distribution.
 *
 * @author Antonio García Domínguez
 */
public class UniformMutationOperator extends MutationOperator {

	@Override
	protected BigDecimal doMutationFloat(final double probability, TypeFloat type, Object value) {
		if (type.getAllowedValues() != null && !type.getAllowedValues().isEmpty()) {
			return pickRandomAllowedValue(type);
		} else {
			return generateNewFloat(type.getMinValue(), type.getMaxValue());
		}
	}

	@Override
	protected BigInteger doMutationInt(final double probability, TypeInt type, Object value) {
		if (type.getAllowedValues() != null && !type.getAllowedValues().isEmpty()) {
			return pickRandomAllowedValue(type);
		} else {
			return generateNewInt(type.getMinValue(), type.getMaxValue());
		}
	}

	/**
	 * Generates a new BigDecimal in the range [<code>min</code>, <code>max</code>),
	 * according to a uniform distribution.
	 *
	 * @param min Left bound (inclusive).
	 * @param max Right bound (exclusive).
	 */
	private BigDecimal generateNewFloat(final BigDecimal min, final BigDecimal max) {
		final BigDecimal maxMinRange = max.subtract(min);

		if(maxMinRange.compareTo(BigDecimal.ZERO) > 0) {
			final BigDecimal delta = maxMinRange.multiply(new BigDecimal(getPRNG().nextDouble()));
			return min.add(delta);
		} else {
			return min;
		}
	}

	/**
	 * Generates a new BigInteger in the range [<code>min</code>, <code>max</code>],
	 * according to a uniform distribution.
	 *
	 * @param min Left bound (inclusive).
	 * @param max Right bound (inclusive).
	 */
	private BigInteger generateNewInt(final BigInteger min, final BigInteger max) {
		final BigDecimal maxMinRange = new BigDecimal(max.subtract(min));

		if(maxMinRange.compareTo(BigDecimal.ZERO) > 0) {
		        // Generate a number in [0, max - min + 1) and then truncate to [0, max - min]
			final BigInteger delta = maxMinRange
				.add(BigDecimal.ONE)
				.multiply(new BigDecimal(getPRNG().nextDouble()))
				.toBigInteger();

			// Shift the mutated value from [0, max - min] to [min, max]
			return min.add(delta);
		} else {
			return min;
		}
	}

}
