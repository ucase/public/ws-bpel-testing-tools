package es.uca.webservices.gamera.ggen.individuals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * This class implements the population. A population is formed by some individuals
 * @author Álvaro Galán Piñero
 * @version 1.0
 */

public class GgenPopulation {
	
	private List<GgenIndividual> population = new ArrayList<GgenIndividual>();

	// This field is lazily initialized by getTotalFitness()
	private Double totalFitness;

    public GgenPopulation() {
        // do nothing
    }

    public GgenPopulation(GgenIndividual... individuals) {
        this(Arrays.asList(individuals));
    }

    public GgenPopulation(Collection<GgenIndividual> individuals) {
        population.addAll(individuals);
    }

	/**
	 * Returns a view of the individuals of a population.
	 */
	public List<GgenIndividual> getPopulation() {
		return population;
	}

	/**
	 * Sets the list of individuals of the population.
	 */
	public void setPopulation(List<GgenIndividual> population) {
		this.population = population;
	}

	/**
	 * Adds an individual to the list of individuals.
	 */
	public void addIndividual(GgenIndividual individual) {
		population.add(individual);

		// Will need to recompute the total fitness on the next call to getIndividual()
		totalFitness = null;
	}

	/**
	 * Returns a single individual of a population.
	 * @param n Zero-based position of the individual within the population.
	 */
	public GgenIndividual getIndividual(int n) {
		if (n >= population.size()) {
			throw new IllegalArgumentException(String.format("The individual (%d) is out of range (population size: %d).", n, population.size()));
		}
		else {
			return population.get(n);
		}
	}

	/**
	 * Returns the population size
	 */
	public int getPopulationSize() {
		return population.size();
	}

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        boolean bFirst = true;
        int count = 0;
        for (GgenIndividual ind : population) {
            if (bFirst) {
                bFirst = false;
            } else {
                sb.append("\n");
            }
            sb.append("INDIVIDUAL_");
            sb.append(count++);
            sb.append(' ');
            sb.append(ind);
        }
        return sb.toString();
    }

    /**
     * Returns the total fitness of all the individuals in this population. Before
     * calling this method, the fitness of each individual must have been set by
     * an {@link IFitness} implementation. The computed total fitness is cached for
     * later executions. If the individuals in the population change at some point
     * after this method is called, the total fitness will be recomputed on the next
     * call.
     */
	public double getTotalFitness() {
		if (totalFitness == null) {
			totalFitness = 0.0d;
			for (GgenIndividual ind : getPopulation()) {
				totalFitness += ind.getFitness();
			}
		}

		return totalFitness;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((population == null) ? 0 : population.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GgenPopulation other = (GgenPopulation) obj;
		if (population == null) {
			if (other.population != null)
				return false;
		} else if (!population.equals(other.population))
			return false;
		return true;
	}

}
