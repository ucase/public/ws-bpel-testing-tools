package es.uca.webservices.gamera.ggen.select;

import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.select.GASelectionOperator;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;

/**
 * This class selects individuals by the roulette method.
 *
 * @author Álvaro Galán-Piñero, Antonio García-Domínguez
 * @version 1.1
 */
public class RouletteSelection implements GASelectionOperator {
	private Random prng;
	
	@Override
	public GgenIndividual select(GgenPopulation source) {
		final double totalFitness = source.getTotalFitness();
		
		// Random number between 0 and totalFitness
		final double rouletteResult = Math.abs(prng.nextFloat()) * totalFitness;
		// Random number between 0 and population size-1
		final int startingPosition = prng.nextInt(source.getPopulationSize());

		return doRoulette(source.getPopulation(), rouletteResult, startingPosition);
	}

	/**
	 * Implements the roulette selection.
	 * @param individuals
	 * @param rouletteResult
	 * @param startingPosition
	 * @return The selected individual by roulette selection
	 */
	public GgenIndividual doRoulette(final List<GgenIndividual> individuals, final double rouletteResult, final int startingPosition) {
		if (startingPosition == individuals.size()) {
			throw new IllegalArgumentException("The starting position should be" +
					"in the [0, population size) range");
		}
		if (rouletteResult <= 0) {
			// If the rouletteResult is zero, returns the individual of the 
			// starting position 
			return individuals.get(startingPosition);
		} else {
			// This loop will be produced while the count isn't equal or greater
			// than Y. When it finish, X-1 will be the individual selected.
			final int nIndividuals = individuals.size();

			int x;
			double subtotal = 0;
			for (x = startingPosition; subtotal < rouletteResult; x++) {
				if (x == nIndividuals) {
					x = 0;
				}
				subtotal += individuals.get(x).getFitness();
			}

			// Undo the final increment of the loop
			return individuals.get(x-1);
		}
	}

	/**
	 * Validates the configuration
	 */
	@Override
	public void validate() throws InvalidConfigurationException {
		if (prng == null) {
			throw new InvalidConfigurationException("PRNG has not been set");
		}		
	}

	/**
	 * Sets the seed
	 */
	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;		
	}
}
