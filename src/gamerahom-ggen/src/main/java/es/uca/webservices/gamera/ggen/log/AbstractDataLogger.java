package es.uca.webservices.gamera.ggen.log;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenState;
import es.uca.webservices.gamera.ggen.util.FormatterUtilities;
import es.uca.webservices.testgen.api.formatters.IFormatter;

/**
 * Superclass for all loggers that saved a subset of the generated test cases.
 */
public abstract class AbstractDataLogger extends NullLogger {
    private File file;
    private IFormatter formatter;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
    
    public IFormatter getFormatter() {
		return formatter;
	}

	public void setFormatter(IFormatter formatter) {
		this.formatter = formatter;
	}

	@Override
    public void finished(GgenState state) throws IOException {
        final Collection<GgenIndividual> population = getIndividuals(state);
        FormatterUtilities.writeToFile(population, getFormatter(), getFile());
    }

    protected abstract Collection<GgenIndividual> getIndividuals(GgenState state);

	@Override
	public void validate() throws InvalidConfigurationException {
		super.validate();
		
		if (file == null) {
			throw new InvalidConfigurationException("No file has been set");
		}
		if (formatter == null) {
			throw new InvalidConfigurationException("No formatter has been set");
		}
	}
    
    
}
