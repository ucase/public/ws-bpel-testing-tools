package es.uca.webservices.gamera.ggen.util;

import es.uca.webservices.gamera.ggen.individuals.Component;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.testgen.api.formatters.IFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

/**
 * Utilities for working with the {@link es.uca.webservices.testgen.api.formatters.IFormatter} interface.
 */
public final class FormatterUtilities {
    private static final Logger LOGGER = LoggerFactory.getLogger(FormatterUtilities.class);

    private FormatterUtilities() {}

    /**
     * Creates a vm file with the individuals of a population.
     *
     * @param individuals list of individuals
     * @param formatter
     * @param path
     * @return A new file with the individuals generated
     * @throws java.io.IOException
     */
    public static File writeToFile(Collection<GgenIndividual> individuals, IFormatter formatter, File file) throws IOException {
		try {
			formatter.suiteStart();
            for (GgenIndividual ind : individuals) {
                formatter.testStart();
                for (Component c : ind.getComponents()) {
                    formatter.valueFor(c.getType().getNameVariable(), c.getValue());
                }
                formatter.testEnd();
            }
            OutputStream os = new FileOutputStream(file);
			formatter.save(os);
			os.close();
			formatter.suiteEnd();
		} catch (Exception e) {
			LOGGER.error("There was an error writing to the file", e);
		}
		return file;
	}
}
