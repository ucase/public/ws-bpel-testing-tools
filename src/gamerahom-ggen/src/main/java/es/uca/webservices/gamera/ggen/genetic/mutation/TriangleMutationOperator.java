package es.uca.webservices.gamera.ggen.genetic.mutation;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeTuple;

/**
 * This class implements the operator that mutates the components of triangle
 * individuals.
 * 
 * @author Álvaro Cortijo-García
 *
 */
public class TriangleMutationOperator implements GAMutationOperator {

	private Random prng;

	@SuppressWarnings("unchecked")
	@Override
	public Object doMutation(final double probability, final IStrategy strategy, final IType type, final Object value, List<Integer> componentsToMutate)
			throws GenerationException {
		if (type instanceof TypeTuple && componentsToMutate != null && !componentsToMutate.isEmpty()){
			// Do the mutation of the selected values of the individual
			final List<Object> newTuple = new ArrayList<Object>((List<Object>)value);
			for(int position : componentsToMutate){
				final IType componentType = ((TypeTuple) type).getIType(position);
				final Object componentValue = ((List<Object>) value).get(position);
				if(componentType instanceof TypeInt){
					final Object newComponentValue = doMutationInt(newTuple, (TypeInt) componentType, componentValue, position);
					// Change the selected values of the individual, replacing the old value with
					// a new value.
					newTuple.set(position, newComponentValue);
				} else {
					throw new IllegalArgumentException("No behaviour defined to mutate the type " + type);
				}
			} 
			return newTuple;
		} else {
			throw new IllegalArgumentException("No behaviour defined to mutate the type " + type);
		}

	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (prng == null) {
			throw new InvalidConfigurationException("PRNG has not been set");
		}
	}

	public Random getPRNG() {
		return this.prng;
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}
	
	/**
	 * This method mutates a component of type TypeInt.
	 * 
	 * @param individualValues
	 *            values of the individual that is being mutated.
	 * @param position
	 *            position of the component to mutate.
	 * @param type
	 *            type of the component to mutate.
	 * @param value
	 *            value of the component to mutate.
	 * @return A BigInteger that is the result of the mutation.
	 */
	protected BigInteger doMutationInt(final List<Object> individualValues, final TypeInt type, final Object value, final int position){
		final int opt = getPRNG().nextInt(3);			
		final BigInteger newValue;

		if(opt == 0){
			newValue = setValueToZero();
		} else if(opt == 1){
			newValue = copyComponentValue(individualValues, position);
		} else{
			newValue = addComponentValue(individualValues, position, type, value);
		}
		return newValue;
	}

	/**
	 * This method returns a BigInteger with value 0.
	 * 
	 * @return A BigInteger with value 0.
	 */
	private BigInteger setValueToZero(){
		return new BigInteger("0");
	}

	/**
	 * This method copies the value of another component of the given
	 * individual.
	 * 
	 * @param individualValues
	 *            values of the individual that is being mutated.
	 * @param position
	 *            position of the component to mutate.
	 * @return A BigInteger whose value is copied from another component.
	 */
	private BigInteger copyComponentValue(final List<Object> individualValues, final int position){
		final int posToCopy = getRandomPosition(individualValues.size(), position);
		final BigInteger newValue = convertToBigInteger(individualValues.get(posToCopy));

		return newValue;
	}

	/**
	 * This method adds the value of another component of the given individual
	 * to the current value.
	 * 
	 * @param individual
	 *            individual that is being mutated.
	 * @param position
	 *            position of the component to mutate.
	 * @param type
	 *            type of the component to mutate.
	 * @param value
	 *            value of the component to mutate.
	 * @return A BigInteger whose value is the current value plus the value of
	 *         another component.
	 */
	private BigInteger addComponentValue(final List<Object> individual, int position, final TypeInt type, final Object value){
		final BigInteger oldValue = convertToBigInteger(value);
		final int posToAdd = getRandomPosition(individual.size(), position);
		BigInteger newValue = convertToBigInteger(individual.get(posToAdd));
		newValue = newValue.add(oldValue);
		
		if(type.getMinValue() != null && newValue.compareTo(type.getMinValue()) < 0){
			newValue = type.getMinValue();
		}
		if(type.getMaxValue() != null && newValue.compareTo(type.getMaxValue()) > 0){
			newValue = type.getMaxValue();
		}
		
		return newValue;
	}

	/**
	 * This method selects another position of the individual randomly.
	 * 
	 * @param numberComponents
	 *            number of components of the individual.
	 * @param position
	 *            position of the current component.
	 * @return another position of the individual randomly.
	 */
	private int getRandomPosition(final int numberComponents, final int position) {
		/*
		 * According to the section 15.17.3 of the 3º Edition of The Java
		 * Language Specification, the result of applying the remainder
		 * operation to a negative dividend can be negative. This is because the
		 * expression used is: (a/b)*b+(a%b) = a, where a%b = a-(a/b)*b. So we
		 * have to control if the result of the reminder operation is negative
		 * in order to avoid accessing to a negative index.
		 */
		int newPosition = getPRNG().nextBoolean() ?
				(position - 1) % numberComponents :
				(position + 1) % numberComponents;
		if(newPosition < 0){
			newPosition = newPosition + numberComponents;
		}
		return newPosition;
	}
	
	/**
	 * This method turns the given value into a BigInteger.
	 * 
	 * @param value
	 *            value to be converted.
	 * @return A BigInteger with the given value.
	 */
	private BigInteger convertToBigInteger(final Object value) {
		final BigInteger newValue = value instanceof BigInteger ?
				((BigInteger)value) :
				BigInteger.valueOf((Integer)value);
		return newValue;
	}
}
