package es.uca.webservices.gamera.ggen.genetic.mutation;

import java.math.BigDecimal;
import java.math.BigInteger;

import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;

/**
 * Mutation operator that applies the Gray Code.
 * 
 * @author Álvaro Cortijo-García
 */
public class GrayBinaryMutationOperator extends MutationOperator {

	@Override
	protected BigInteger doMutationInt(final double probability, TypeInt type, Object value) {
		if (type.getAllowedValues() != null && !type.getAllowedValues().isEmpty()) {
			return pickRandomAllowedValue(type);
		} else {
			return applyIntGrayBinaryMutation(type, value);
		}
	}
	
	@Override
	protected BigDecimal doMutationFloat(final double probability, TypeFloat type, Object value) {
		if (type.getAllowedValues() != null && !type.getAllowedValues().isEmpty()) {
			return pickRandomAllowedValue(type);
		} else {
			return applyFloatGrayBinaryMutation(type, value);
		}
	}
	
	private BigInteger applyIntGrayBinaryMutation(TypeInt type, Object value) {
		final BigInteger oldValue = value instanceof BigInteger ? ((BigInteger)value) : BigInteger.valueOf((Integer)value);
		final BigInteger min = type.getMinValue();
		final BigInteger max = type.getMaxValue();
		final BigInteger maxMinRange = max.subtract(min);

		BigInteger newValue;
		if(!maxMinRange.equals(BigInteger.ZERO)){
			final int maxMinBits = maxMinRange.bitLength();
			final int randomPosition = getPRNG().nextInt(maxMinBits);

			// Shift the value to mutate from [min, max] to [0, max-min].
			final BigInteger v = oldValue.subtract(min);
			// Convert to Gray Code the value to mutate.
			final BigInteger grayValue = binaryToGray(v);
			final BigInteger mutatedGrayValue = grayValue.flipBit(randomPosition);
			// Convert to Binary Code the mutated value.
			newValue = grayToBinary(mutatedGrayValue);

			// Shift the mutated value from [0, max-min] to [min, max] and clip the value to the maximum value.
			newValue = newValue.add(min);
			if (max != null && newValue.compareTo(max) > 0) {
				newValue = max;
			}
		} else {
			newValue = oldValue;
		}
		
		return newValue;
	}

	private BigDecimal applyFloatGrayBinaryMutation(TypeFloat type, Object value) {
		final BigDecimal oldValue = value instanceof BigDecimal ? ((BigDecimal)value) : BigDecimal.valueOf((Float)value);
		final BigDecimal min = type.getMinValue();
		final BigDecimal max = type.getMaxValue();

		final int scale = Math.max(Math.max(oldValue.scale(), min.scale()), max.scale());

		// We convert the unscaled float to a integer in order to use the same way of mutation.
		final BigInteger unscaledValue = oldValue.movePointRight(scale).toBigInteger();
		final BigInteger integerMin = min.movePointRight(scale).toBigInteger();
		final BigInteger integerMax = max.movePointRight(scale).toBigInteger();
		final TypeInt integerType = new TypeInt(integerMin, integerMax);
		final BigInteger newUnscaledValue = applyIntGrayBinaryMutation(integerType, unscaledValue);
		
		// We convert again the mutated value to a decimal.
		BigDecimal newValue = new BigDecimal(newUnscaledValue,scale);
		
		return newValue;
	}
	
	/**
	 * Method that converts a number to Gray Code.
	 * 
	 * @param value
	 *            number to convert
	 * @return a BigInteger converted to Gray Code.
	 */
	protected BigInteger binaryToGray(final BigInteger value) {
		final BigInteger grayValue = value.abs().xor(value.abs().shiftRight(1));
		return grayValue;
	}

	/**
	 * Method that converts from Gray Code to binary.
	 * 
	 * @param value
	 *            number to convert
	 * @return a BigInteger converted from Gray Code.
	 */
	protected BigInteger grayToBinary(final BigInteger value){
		BigInteger binary = value;
		BigInteger shift = value;
		
		while(!shift.shiftRight(1).equals(BigInteger.ZERO)){
			shift = shift.shiftRight(1);
			binary = binary.xor(shift);
		}
		
		return binary;
	}
}
