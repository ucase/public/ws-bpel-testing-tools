/* File to test the mutation operator */
typedef string(values={"true", "true", "true", "false"}) Boolean;

Boolean googleAvailable;
Boolean msnAvailable;

typedef int(min=0,max=1000) MaxResults;
typedef float(min=0,max=1000) Price;

typedef string(pattern="[a-z]{1,3}[.](com|org)") Domain;
typedef tuple(element={Domain,Price,MaxResults}) GoogleResult;
typedef list(min=0,max=30,element={GoogleResult}) GoogleResults;



MaxResults maxResults;
Domain domain;
GoogleResult google;
GoogleResults lGoogle;
Price price;
