package es.uca.webservices.gamera.ggen.genetic.mutation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;

/**
 * Tests for the BinaryMutationOperator class.
 *
 * @author Álvaro Cortijo-García, Antonio García-Domínguez
 */
public class BinaryMutationOperatorTest {

	private BinaryMutationOperator mutation;

	@Before
	public void setUp() throws Exception {
		mutation = new BinaryMutationOperator();
		mutation.setPRNG(new Random(0));
	}

	@Test
	public void testDoMutationIntFirst() {
		final BigInteger oldValue = new BigInteger("10000");
		final BigInteger minValue = new BigInteger("-100000");
		final BigInteger maxValue = new BigInteger("100000");
		final TypeInt type = new TypeInt(minValue, maxValue);
		final BigInteger newValue = mutation.doMutationInt(1.0, type, oldValue);
		assertTrue("The mutation should have produced a new object", !oldValue.equals(newValue));
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}

	@Test
	public void testDoMutationIntSecond() {
		final BigInteger oldValue = new BigInteger("1040");
		final BigInteger minValue = new BigInteger("1023");
		final BigInteger maxValue = new BigInteger("3070");
		final TypeInt type = new TypeInt(minValue, maxValue);
		final BigInteger newValue = mutation.doMutationInt(1.0, type, oldValue);
		assertTrue("The mutation should have produced a new object", !oldValue.equals(newValue));
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}

	@Test
	public void testDoMutationSpecificBit() {
		final BigInteger oldValue = new BigInteger("1040");
		final BigInteger minValue = new BigInteger("1023");
		final BigInteger maxValue = new BigInteger("3070");
		final TypeInt type = new TypeInt(minValue, maxValue);
		
		final Random r = mock(Random.class);
		when(r.nextInt(11)).thenReturn(5);
		mutation.setPRNG(r);
		
		final BigInteger newValue = mutation.doMutationInt(1.0, type, oldValue);

		verify(r, times(1)).nextInt(11);
		
		assertTrue("The mutation should have produced a new object", !oldValue.equals(newValue));
		assertEquals(new BigInteger("1072"), newValue);
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}
	
	@Test
	public void testDoMutationFloatFirst() {
		final BigDecimal oldValue = new BigDecimal(new BigInteger("10000"),4);
		final BigDecimal minValue = new BigDecimal(new BigInteger("-100000"),5);
		final BigDecimal maxValue = new BigDecimal(new BigInteger("100000"),5);
		final TypeFloat type = new TypeFloat(minValue, maxValue);
		final BigDecimal newValue = mutation.doMutationFloat(1.0, type, oldValue);
		assertTrue("The mutation should have produced a new object", !oldValue.equals(newValue));
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}
	
	@Test
	public void testDoMutationFloatSecond() {
		final BigDecimal oldValue = new BigDecimal(new BigInteger("104"),2);
		final BigDecimal minValue = new BigDecimal(new BigInteger("1023"),3);
		final BigDecimal maxValue = new BigDecimal(new BigInteger("307"),2);
		final TypeFloat type = new TypeFloat(minValue, maxValue);
		final BigDecimal newValue = mutation.doMutationFloat(1.0, type, oldValue);
		assertTrue("The mutation should have produced a new object", !oldValue.equals(newValue));
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}
	
	@Test
	public void testDoMutationMaxInt() {
		final BigInteger oldValue = new BigInteger("128");
		final BigInteger minValue = BigInteger.ZERO;
		final BigInteger maxValue = new BigInteger("128");
		final TypeInt type = new TypeInt(minValue, maxValue);
		final BigInteger newValue = mutation.doMutationInt(1.0, type, oldValue);
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}
}
