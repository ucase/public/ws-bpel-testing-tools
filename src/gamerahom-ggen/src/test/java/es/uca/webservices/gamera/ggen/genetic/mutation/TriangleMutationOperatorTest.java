package es.uca.webservices.gamera.ggen.genetic.mutation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeTuple;

/**
 * Tests for the TriangleMutationOperator class.
 *
 * @author Álvaro Cortijo-García
 */
public class TriangleMutationOperatorTest {

	private TriangleMutationOperator mutation;

	private TypeInt typeInt;
	private TypeTuple typeTuple;
	private BigInteger l1;
	private BigInteger l2;
	private BigInteger l3;
	private List<Object> oldTuple;
	
	@Before
	public void setUp() throws Exception {
		mutation =  new TriangleMutationOperator();
		mutation.setPRNG(new Random(0));

		typeInt = new TypeInt(0, 127);
		typeTuple = new TypeTuple(typeInt, typeInt, typeInt);
		l1 = new BigInteger("15");
		l2 = new BigInteger("25");
		l3 = new BigInteger("35");
		oldTuple = Arrays.asList((Object)l1, l2, l3);
	}

	@Test(expected = InvalidConfigurationException.class)
	public void testNullSeed() throws Exception {
		mutation.setPRNG(null);
		mutation.validate();
	}	

	@SuppressWarnings("unchecked")
	@Test
	public void testOneTriangleComponentMutated() throws GenerationException {
		final List<Integer> componentsToMutate = new ArrayList<Integer>();
		componentsToMutate.add(1);
		final List<Object> newTuple = (List<Object>)mutation.doMutation(0.1, null, typeTuple, oldTuple, componentsToMutate);
		
		assertTrue("Exactly only the selected elements should have changed",
					l1.equals(newTuple.get(0)) &&
					!l2.equals(newTuple.get(1)) &&
					l3.equals(newTuple.get(2)));

	}

	@SuppressWarnings("unchecked")
	@Test
	public void testValueToZero() throws GenerationException {
		final List<Integer> componentsToMutate = new ArrayList<Integer>();
		componentsToMutate.add(1);

		final Random r = mock(Random.class);
		when(r.nextInt(3)).thenReturn(0);
		mutation.setPRNG(r);

		final List<Object> newTuple = (List<Object>)mutation.doMutation(0.1, null, typeTuple, oldTuple, componentsToMutate);
		
		verify(r, times(1)).nextInt(3);
		assertTrue("Exactly only the selected elements should have changed",
					l1.equals(newTuple.get(0)) &&
					!l2.equals(newTuple.get(1)) &&
					l3.equals(newTuple.get(2)));
		assertTrue("The mutated value should be 0", newTuple.get(1).equals(new BigInteger("0")));
		
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCopyComponentValue() throws GenerationException {
		final List<Integer> componentsToMutate = new ArrayList<Integer>();
		componentsToMutate.add(1);

		final Random r = mock(Random.class);
		when(r.nextInt(3)).thenReturn(1);
		when(r.nextBoolean()).thenReturn(true);
		mutation.setPRNG(r);

		final List<Object> newTuple = (List<Object>)mutation.doMutation(0.1, null, typeTuple, oldTuple, componentsToMutate);
		
		verify(r, times(1)).nextInt(3);
		verify(r, times(1)).nextBoolean();
		assertTrue("Exactly only the selected elements should have changed",
					l1.equals(newTuple.get(0)) &&
					!l2.equals(newTuple.get(1)) &&
					l3.equals(newTuple.get(2)));
		assertTrue("The mutated value should be the same that the copied value", newTuple.get(1).equals(l1));		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testAddComponentValue() throws GenerationException {
		final List<Integer> componentsToMutate = new ArrayList<Integer>();
		componentsToMutate.add(1);

		final Random r = mock(Random.class);
		when(r.nextInt(3)).thenReturn(2);
		when(r.nextBoolean()).thenReturn(false);
		mutation.setPRNG(r);

		final List<Object> newTuple = (List<Object>)mutation.doMutation(0.1, null, typeTuple, oldTuple, componentsToMutate);
		
		verify(r, times(1)).nextInt(3);
		verify(r, times(1)).nextBoolean();
		assertTrue("Exactly only the selected elements should have changed",
					l1.equals(newTuple.get(0)) &&
					!l2.equals(newTuple.get(1)) &&
					l3.equals(newTuple.get(2)));
		System.out.println(newTuple.get(1));
		assertTrue("The mutated value should be the sum of the selected values", newTuple.get(1).equals(l2.add(l3)));		
	}
}
