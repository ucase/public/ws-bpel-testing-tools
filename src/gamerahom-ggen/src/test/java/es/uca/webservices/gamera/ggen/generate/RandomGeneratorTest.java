package es.uca.webservices.gamera.ggen.generate;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.util.List;

import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.ggen.DefaultConfiguration;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * Tests the {@link RandomGenerator} class. New individuals are generated properly.
 * @author alvaro
 */
public class RandomGeneratorTest {
	
	@Test(expected = InvalidConfigurationException.class)
	public void nullPrng() throws Exception {
		RandomGenerator generator = new RandomGenerator();
		generator.setPRNG(null);
		generator.validate();
	}
	
	@Test
	public void validGeneration() throws FileNotFoundException, ClassNotFoundException, InvalidConfigurationException, ParserException, GenerationException, ComparisonException {
		final DefaultConfiguration conf = new DefaultConfiguration();
		conf.setFirstGenerationTest();

		final GgenPopulation source = conf.getPopulation();
		final IStrategy strategy = conf.getConf().getStrategy();
		final RandomGenerator generatorIndividual = new RandomGenerator();

        final List<IType> types = conf.getConf().getTypes();
        final GgenIndividual dstInd1 = generatorIndividual.generate(source, strategy, types);

        final GgenIndividual srcInd1 = source.getIndividual(0);
        assertEquals(srcInd1.getNumberComponents(), dstInd1.getNumberComponents());
        for (int iComponent = 0; iComponent < srcInd1.getNumberComponents(); ++iComponent) {
            assertEquals(srcInd1.getComponent(iComponent).getType(), dstInd1.getComponent(iComponent).getType());
        }
	}

}
