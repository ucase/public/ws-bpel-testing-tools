package es.uca.webservices.gamera.ggen;

import java.io.FileNotFoundException;
import java.util.Random;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.conf.Configuration;
import es.uca.webservices.gamera.ggen.conf.ConfigurationLoader;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * This class loads a default configuration and creates a population to do tests.
 * @author alvaro
 */

public class DefaultConfiguration {
	
	private Configuration conf;
	private GgenPopulation population = new GgenPopulation();

    public DefaultConfiguration() throws FileNotFoundException, InvalidConfigurationException, ClassNotFoundException {
    	this("/confTest.yaml");
	}
	
	public DefaultConfiguration(String pathFile) throws FileNotFoundException, InvalidConfigurationException, ClassNotFoundException {
		final ConfigurationLoader yaml = new ConfigurationLoader();
		conf = yaml.parse(getClass().getResourceAsStream(pathFile));
		conf.validate();
	}
	
	public void setFirstGenerationTest() throws GenerationException, ParserException {
		GeneticAlgorithm main = new GeneticAlgorithm();
        main.setConfiguration(conf);
        main.generateNewIndividuals(new GgenPopulation(), population, conf.getTypes(), conf.getAggregateLogger(), null);
		setFakeFitnessToTest(population);
	}

	public Configuration getConf() {
		return conf;
	}

	public GgenPopulation getPopulation() {
		return population;
	}

	private void setFakeFitnessToTest(GgenPopulation population) {
		Random prng = conf.getPrng();
		for (GgenIndividual ind : population.getPopulation()){
			ind.setFitness(prng.nextInt(20));
		}
	}

}
