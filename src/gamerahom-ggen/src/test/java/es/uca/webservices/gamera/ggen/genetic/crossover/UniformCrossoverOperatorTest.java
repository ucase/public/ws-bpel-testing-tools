package es.uca.webservices.gamera.ggen.genetic.crossover;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.ggen.individuals.Component;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.testgen.api.types.TypeInt;
/**
 * Tests for the UniformCrossoverOperator class.
 *
 * @author Álvaro Cortijo-García, Antonio García-Domínguez
 */
public class UniformCrossoverOperatorTest {

	private UniformCrossoverOperator crossover;
	private GgenIndividual father, mother, left, right;

	@Before
	public void setUp() {
		crossover = new UniformCrossoverOperator();

		final TypeInt type = new TypeInt();
		List<Component> cmps = new ArrayList<Component>();
		for(int i = 0; i < 8; i++){
			final Component cmp = new Component(type, i);
			cmps.add(cmp);
		}
		father = new GgenIndividual(cmps.get(0), cmps.get(1), cmps.get(2),cmps.get(3));
		mother = new GgenIndividual(cmps.get(4),cmps.get(5), cmps.get(6),cmps.get(7));
		left =  new GgenIndividual(new ArrayList<Component>(father.getComponents()));
		right =  new GgenIndividual(new ArrayList<Component>(mother.getComponents()));
	}

	@Test
	public void fourComponentsDoCross() throws Exception {
		final List<Boolean> crossComponents = new ArrayList<Boolean>();
		
		crossComponents.add(true);
		crossComponents.add(false);
		crossComponents.add(true);
		crossComponents.add(false);

		crossover.doCrossover(left, right, crossComponents);
		assertCrossoverIsCorrect(crossComponents, father, mother, left, right);
	}

	@Test
	public void noCrossoverDone() throws Exception {
		final List<Boolean> crossComponents = new ArrayList<Boolean>();
		
		crossComponents.add(false);
		crossComponents.add(false);
		crossComponents.add(false);
		crossComponents.add(false);

		crossover.doCrossover(left, right, crossComponents);
		assertCrossoverIsCorrect(crossComponents, father, mother, left, right);
	}

	@Test
	public void exchangeComponentsCrossover() throws Exception {
		final List<Boolean> crossComponents = new ArrayList<Boolean>();
		
		crossComponents.add(true);
		crossComponents.add(true);
		crossComponents.add(true);
		crossComponents.add(true);

		crossover.doCrossover(left, right, crossComponents);
		assertCrossoverIsCorrect(crossComponents, father, mother, left, right);
	}
	
	@Test
	public void applyCrossoverTest() throws Exception {
		final Random r = mock(Random.class);
		when(r.nextBoolean()).thenReturn(true).thenReturn(false).thenReturn(false).thenReturn(true);
		crossover.setPRNG(r);

		crossover.apply(new GAGeneticOperatorOptions(), father, mother);
		verify(r, times(4)).nextBoolean();

		final List<Boolean> crossComponents = new ArrayList<Boolean>();
		
		crossComponents.add(true);
		crossComponents.add(false);
		crossComponents.add(false);
		crossComponents.add(true);

		assertCrossoverIsCorrect(crossComponents, father, mother, left, right);
	}

	
	private static void assertCrossoverIsCorrect(final List<Boolean> crossComponents,
			GgenIndividual father, GgenIndividual mother,
			GgenIndividual leftChild, GgenIndividual rightChild) {
		for (int i = 0 ; i < crossComponents.size(); i++) {
			if (crossComponents.get(i)) {
				assertEquals(father.getComponent(i), rightChild.getComponent(i));
				assertEquals(mother.getComponent(i), leftChild.getComponent(i));
			} else{
				assertEquals(father.getComponent(i), leftChild.getComponent(i));
				assertEquals(mother.getComponent(i), rightChild.getComponent(i));
			}
		}
	}

}
