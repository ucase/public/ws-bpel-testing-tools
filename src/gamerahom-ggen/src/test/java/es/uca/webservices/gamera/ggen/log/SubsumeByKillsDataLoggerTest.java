package es.uca.webservices.gamera.ggen.log;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.ggen.individuals.*;
import es.uca.webservices.testgen.api.types.TypeInt;
import org.junit.Test;

import java.util.Collection;

import static junit.framework.Assert.assertEquals;

/**
 * Tests for the {@link SubsumeByKillsDataLogger} class.
 *
 * @author Antonio García-Domínguez
 */
public class SubsumeByKillsDataLoggerTest {

    private final SubsumeByKillsDataLogger logger = new SubsumeByKillsDataLogger();

    @Test
    public void empty() {
        final Collection<GgenIndividual> individuals = logger.getIndividuals(new GgenState());
        assertEquals(0, individuals.size());
    }

    @Test
    public void uselessTestsAreDiscarded() {
        final GgenState state = new GgenState();
        final GgenHistory history = state.getHistory();
        history.add(new GgenPopulation(new GgenIndividual(new Component(new TypeInt(), 2))), new ComparisonResults[] {
                new ComparisonResults(1, 1, 1, true, 1),
                new ComparisonResults(1, 1, 2, true, 0),
        });

        final Collection<GgenIndividual> individuals = logger.getIndividuals(state);
        assertEquals(1, individuals.size());
    }

    @Test
    public void disjointTestsAreKept() {
        final GgenState state = new GgenState();
        final GgenHistory history = state.getHistory();
        final TypeInt type = new TypeInt();
        history.add(
                new GgenPopulation(
                        new GgenIndividual(new Component(type, 2)),
                        new GgenIndividual(new Component(type, 3))
                ),
                new ComparisonResults[]{
                        new ComparisonResults(1, 1, 1, true, 1, 0),
                        new ComparisonResults(1, 1, 2, true, 0, 1),
                });

        final Collection<GgenIndividual> individuals = logger.getIndividuals(state);
        assertEquals(2, individuals.size());
    }

    @Test
    public void subsumedByNextAreDiscarded() {
        final GgenState state = new GgenState();
        final GgenHistory history = state.getHistory();
        final TypeInt type = new TypeInt();
        history.add(
                new GgenPopulation(
                        new GgenIndividual(new Component(type, 2)),
                        new GgenIndividual(new Component(type, 3)),
                        new GgenIndividual(new Component(type, 4)),
                        new GgenIndividual(new Component(type, 5))
                ),
                new ComparisonResults[]{
                        new ComparisonResults(1, 1, 1, true, 1, 0, 0, 1),
                        new ComparisonResults(1, 1, 2, true, 0, 1, 0, 1),
                        new ComparisonResults(1, 1, 3, true, 0, 0, 1, 0),
                        new ComparisonResults(1, 1, 4, true, 0, 0, 0, 0),
                });

        final Collection<GgenIndividual> individuals = logger.getIndividuals(state);
        assertEquals(2, individuals.size());
    }

    @Test
    public void subsumedByPreviousAreDiscarded() {
        final GgenState state = new GgenState();
        final GgenHistory history = state.getHistory();
        final TypeInt type = new TypeInt();
        history.add(
                new GgenPopulation(
                        new GgenIndividual(new Component(type, 2)),
                        new GgenIndividual(new Component(type, 3)),
                        new GgenIndividual(new Component(type, 4)),
                        new GgenIndividual(new Component(type, 5))
                ),
                new ComparisonResults[]{
                        new ComparisonResults(1, 1, 1, true, 1, 0, 1, 1),
                        new ComparisonResults(1, 1, 2, true, 1, 1, 0, 0),
                        new ComparisonResults(1, 1, 3, true, 0, 0, 0, 0),
                        new ComparisonResults(1, 1, 4, true, 0, 0, 0, 1),
                });

        final Collection<GgenIndividual> individuals = logger.getIndividuals(state);
        assertEquals(2, individuals.size());
    }

}
