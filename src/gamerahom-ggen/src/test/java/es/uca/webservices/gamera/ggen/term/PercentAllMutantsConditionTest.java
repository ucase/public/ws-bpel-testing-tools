package es.uca.webservices.gamera.ggen.term;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import es.uca.webservices.gamera.ggen.individuals.GgenHistory;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import org.junit.Test;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Tests the termination condition, a value of killed mutants is found
 * @author alvaro
 *
 */
public class PercentAllMutantsConditionTest {
	
	@Test(expected=InvalidConfigurationException.class)
	public void greaterThanOnePercent() throws InvalidConfigurationException{
		PercentAllMutantsCondition term = new PercentAllMutantsCondition();
		term.setPercent(1.5);
		term.validate();
	}
	
	@Test(expected=InvalidConfigurationException.class)
	public void negativePercent() throws InvalidConfigurationException{
		PercentAllMutantsCondition term = new PercentAllMutantsCondition();
		term.setPercent(-1);
		term.validate();
	}
	
	@Test
	public void zeroPercent() throws InvalidConfigurationException{
		PercentAllMutantsCondition term = new PercentAllMutantsCondition();
		term.setPercent(0);
		term.validate();
	}
	
	@Test
	public void positivePercent() throws InvalidConfigurationException{
		PercentAllMutantsCondition term = new PercentAllMutantsCondition();
		term.setPercent(0.5);
		term.validate();
	}
	
	@Test
	public void trueWhenEqual(){
        PercentAllMutantsCondition term = new PercentAllMutantsCondition();
		term.setPercent(0.5);

        GgenState state = new GgenState();
        state.getHistory().add(
                new GgenPopulation(new GgenIndividual(), new GgenIndividual(), new GgenIndividual()),
                new ComparisonResults[]{
                        new ComparisonResults(1, 1, 1, true, 1, 0, 0),
                        new ComparisonResults(1, 2, 1, true, 0, 0, 0)
                });
        assertTrue(term.evaluate(state));
	}
	
	@Test
	public void trueWhenGreater() {
		PercentAllMutantsCondition term = new PercentAllMutantsCondition();
		term.setPercent(0.5);

        GgenState state = new GgenState();
        state.getHistory().add(
                new GgenPopulation(new GgenIndividual(), new GgenIndividual(), new GgenIndividual()),
                new ComparisonResults[]{
                        new ComparisonResults(1, 1, 1, true, 1, 0, 0),
                        new ComparisonResults(1, 2, 1, true, 0, 0, 0),
                        new ComparisonResults(1, 3, 1, true, 1, 0, 0),
                        new ComparisonResults(1, 4, 1, true, 1, 0, 0),
                });
        assertTrue(term.evaluate(state));
    }
	
	@Test
	public void falseWhenLower(){
        PercentAllMutantsCondition term = new PercentAllMutantsCondition();
        term.setPercent(0.5);

        GgenState state = new GgenState();
        state.getHistory().add(
                new GgenPopulation(new GgenIndividual(), new GgenIndividual(), new GgenIndividual()),
                new ComparisonResults[]{
                        new ComparisonResults(1, 1, 1, true, 1, 0, 0),
                        new ComparisonResults(1, 2, 1, true, 0, 0, 0),
                        new ComparisonResults(1, 3, 1, true, 0, 0, 0),
                        new ComparisonResults(1, 4, 1, true, 0, 0, 0),
                });
        assertFalse(term.evaluate(state));
    }
}
