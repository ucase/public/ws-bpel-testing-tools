package es.uca.webservices.gamera.ggen.log;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;

import es.uca.webservices.gamera.ggen.conf.ConfigurationLoader;

import org.junit.Test;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.DefaultConfiguration;
import es.uca.webservices.gamera.ggen.individuals.GgenState;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * The log messages work good.  
 * @author alvaro
 *
 */
public class MessageLoggerTest {
	
	private static final String LOGGER = "logger-messages.txt";
    public static final String TEST_CONFIGURATION = "src/test/resources/confTest.yaml";

    @Test(expected=InvalidConfigurationException.class)
	public void noConsoleAndNoFile () throws InvalidConfigurationException {
		MessageLogger message = new MessageLogger();
		message.setConsole(false);
		message.setFile(null);
		message.validate();
	}
	
	@Test(expected=InvalidConfigurationException.class)
	public void ConsoleAndFile () throws InvalidConfigurationException {
		MessageLogger message = new MessageLogger();
		message.setConsole(true);
		message.setFile(LOGGER);
		message.validate();
	}
	
	@Test
	public void onlyConsole () throws InvalidConfigurationException {
		MessageLogger message = new MessageLogger();
		message.setConsole(true);
		message.setFile(null);
		message.validate();
	}
	
	@Test
	public void noConsole () throws InvalidConfigurationException {
		MessageLogger message = new MessageLogger();
		message.setConsole(false);
		message.setFile(LOGGER);
		message.validate();
	}
	
	@Test
	public void consoleWork () throws InvalidConfigurationException {
		MessageLogger message = new MessageLogger();
		GgenState state = new GgenState();
		message.setConsole(true);
		message.setFile(null);
		message.validate();
		message.started(state);
	}
	
	@Test
	public void finishedMessageWork () throws InvalidConfigurationException {
		MessageLogger message = new MessageLogger();
		GgenState state = new GgenState();
		message.setConsole(true);
		message.setFile(null);
		message.validate();
		message.finished(state);
	}
	
	@Test
	public void newGenerationMessageWork () throws InvalidConfigurationException, 
	FileNotFoundException, ClassNotFoundException, GenerationException, ParserException {
		MessageLogger message = new MessageLogger();
		GgenState state = new GgenState();
		DefaultConfiguration defaultConfiguration = new DefaultConfiguration();
		defaultConfiguration.setFirstGenerationTest();
		message.setConsole(true);
		message.setFile(null);
		message.validate();
		message.newGeneration(state,defaultConfiguration.getPopulation());
	}
	
	@Test
	public void startedComparisonMessageWork () throws InvalidConfigurationException {
		MessageLogger message = new MessageLogger();
		GgenState state = new GgenState();
		message.setConsole(true);
		message.setFile(null);
		message.validate();
		message.startedPopulationExecution(state);
	}
	
	@Test
	public void finishedComparisonMessageWork () throws InvalidConfigurationException {
		MessageLogger message = new MessageLogger();
		GgenState state = new GgenState();
    	ComparisonResults[] results = null;
		message.setConsole(true);
		message.setFile(null);
		message.validate();
		message.finishedPopulationExecution(state);
	}
	
	@Test
	public void startedEvaluationMessageWork () throws InvalidConfigurationException, 
	FileNotFoundException, ClassNotFoundException, GenerationException, ParserException {
		MessageLogger message = new MessageLogger();
		GgenState state = new GgenState();
		DefaultConfiguration defaultConfiguration = new DefaultConfiguration();
		defaultConfiguration.setFirstGenerationTest();
		message.setConsole(true);
		message.setFile(null);
		message.validate();
		message.startedEvaluation(state,defaultConfiguration.getPopulation());
	}
	
	@Test
	public void finishedEvaluationMessageWork () throws InvalidConfigurationException, 
	FileNotFoundException, ClassNotFoundException, GenerationException, ParserException {
		MessageLogger message = new MessageLogger();
		GgenState state = new GgenState();
		DefaultConfiguration defaultConfiguration = new DefaultConfiguration();
		defaultConfiguration.setFirstGenerationTest();
		message.setConsole(true);
		message.setFile(null);
		message.validate();
		message.newGeneration(state,defaultConfiguration.getPopulation());
	}
	
	@Test
	public void createdTypesMessageWork() throws Exception {
		MessageLogger message = new MessageLogger();
		GgenState state = new GgenState();
        state.setConfiguration(new ConfigurationLoader().parse(new File(TEST_CONFIGURATION)));
		message.setConsole(true);
		message.setFile(null);
		message.validate();
		message.createdVariables(state);
	}
	
	@Test
	public void FileExists() throws InvalidConfigurationException {
		MessageLogger message = new MessageLogger();
		GgenState state = new GgenState();
		message.setConsole(false);
		message.setFile(LOGGER);
		message.validate();
		message.started(state);
		
		assertTrue(new File(LOGGER).exists() == true);
	}

}
