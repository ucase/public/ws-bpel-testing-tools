package es.uca.webservices.gamera.ggen.log;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.ggen.DefaultConfiguration;
import es.uca.webservices.gamera.ggen.individuals.GgenHistory;
import es.uca.webservices.gamera.ggen.individuals.GgenState;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * The hall of fame works good
 * @author alvaro
 */
public class FullHistoryReportLoggerTest {
    private static final String LOGFILE = "logger-messages.txt";

    @Test
    public void FileExists() throws InvalidConfigurationException, ClassNotFoundException,
            GenerationException, ParserException, ComparisonException, IOException
    {
        final DefaultConfiguration defaultConf = new DefaultConfiguration();
		defaultConf.setFirstGenerationTest();

        final GgenHistory indHistory = new GgenHistory();
        indHistory.add(defaultConf.getPopulation(), new ComparisonResults[] {
                new ComparisonResults(4, 1, 3, true, 1, 0, 1, 0, 0),
                new ComparisonResults(7, 1, 3, true, 1, 0, 1, 1, 0),
                new ComparisonResults(11, 1, 1, true, 1, 1, 1, 0, 0),
                new ComparisonResults(13, 2, 1, true, 1, 0, 0, 0, 0),
                new ComparisonResults(16, 2, 1, true, 1, 0, 0, 1, 1),
        });

        final GgenState state = new GgenState();
        state.setConfiguration(defaultConf.getConf());
        state.setHistory(indHistory);

        final FullHistoryReportLogger hof = new FullHistoryReportLogger();
		hof.setFile(new File(LOGFILE));
		hof.validate();
		hof.finished(state);
		
		assertTrue(new File(LOGFILE).exists() == true);
	}

}
