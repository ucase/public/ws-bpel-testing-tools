package es.uca.webservices.gamera.ggen.genetic.mutation;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;

/**
 * Tests for the CustomProbabilityListMutationOperator class.
 *
 * @author Álvaro Cortijo-García
 */
public class CustomProbabilityListMutationOperatorTest {

	private CustomProbabilityListMutationOperator mutation;

	private List<Object> oldList;
	
	@Before
	public void setUp() throws Exception {
		mutation = new CustomProbabilityListMutationOperator();
		mutation.setPRNG(new Random(0));
		mutation.setProbabilityElimination(0.2);
		mutation.setProbabilityInsertion(0.2);
		final GrayBinaryMutationOperator compMutOp = new GrayBinaryMutationOperator();
		compMutOp.setPRNG(new Random(0));
		mutation.setCompositeMutOp(compMutOp);

		final BigInteger oldValue1 = new BigInteger("0");
		final BigInteger oldValue2 =  new BigInteger("1");
		final BigInteger oldValue3 =  new BigInteger("2");
		final BigInteger oldValue4 = new BigInteger("3");
		final BigInteger oldValue5 =  new BigInteger("4");
		final BigInteger oldValue6 =  new BigInteger("5");
		oldList = Arrays.asList((Object)oldValue1, oldValue2, oldValue3, oldValue4, oldValue5, oldValue6);
	}

	@Test(expected = InvalidConfigurationException.class)
	public void negativeProbabilityElimination() throws Exception {
		mutation.setProbabilityElimination(-0.1);
		mutation.validate();
	}
	@Test(expected = InvalidConfigurationException.class)
	public void negativeProbabilityInsertion() throws Exception {
		mutation.setProbabilityInsertion(-0.1);
		mutation.validate();
	}
	@Test(expected = InvalidConfigurationException.class)
	public void greaterThanOneProbabilityElimination() throws Exception {
		mutation.setProbabilityElimination(1.1);
		mutation.validate();
	}	
	@Test(expected = InvalidConfigurationException.class)
	public void greaterThanOneProbabilityInsertion() throws Exception {
		mutation.setProbabilityInsertion(1.1);
		mutation.validate();
	}	
	@Test(expected = InvalidConfigurationException.class)
	public void greaterThanOneSumOfInsertionAndEliminationProbabilities() throws Exception {
		mutation.setProbabilityElimination(0.6);
		mutation.setProbabilityInsertion(0.5);
		mutation.validate();
	}
	@Test(expected = InvalidConfigurationException.class)
	public void negativeMutationEliminationProbability() throws Exception {
		mutation.setProbabilityMutationElement(-0.1);
		mutation.validate();
	}	
	@Test(expected = InvalidConfigurationException.class)
	public void greaterThanOneMutationEliminationProbability() throws Exception {
		mutation.setProbabilityMutationElement(1.1);
		mutation.validate();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testFixedSizeList() throws GenerationException {
		final TypeList typeList = new TypeList(new TypeInt(), 6, 6);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		final List<Object> newList = (List<Object>)mutation.doMutation(0.1, strategy, typeList, oldList, null);
		
		assertTrue("Some components of the list should have been mutated if the list has a fixed size and it is not empty.", !newList.equals(oldList));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testNonFixedSizeList() throws GenerationException {
		final TypeList typeList = new TypeList(new TypeInt(), 1, 8);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		final List<Object> newList = (List<Object>)mutation.doMutation(0.1, strategy, typeList, oldList, null);

		assertTrue("if the list has a non-fixed size the list should have been mutated by the addition, elimination or mutation of one component.", !newList.equals(oldList));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testInsertionProbabilityEqualsToOne() throws GenerationException {
		final TypeList typeList = new TypeList(new TypeInt(), 1, 8);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		mutation.setProbabilityInsertion(1);
		mutation.setProbabilityElimination(0);
		final List<Object> newList = (List<Object>)mutation.doMutation(0.1, strategy, typeList, oldList, null);
		
		assertTrue("If the list is not full and the inserction probability is 1, the list should have a new element.", oldList.size() < newList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testEliminationProbabilityEqualsToOne() throws GenerationException {
		final TypeList typeList = new TypeList(new TypeInt(), 1, 8);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		mutation.setProbabilityElimination(1);
		mutation.setProbabilityInsertion(0);
		final List<Object> newList = (List<Object>)mutation.doMutation(0.1, strategy, typeList, oldList, null);
		
		assertTrue("If the list is not empty and the elimination probability is 1 the size of the list should be less than before.", oldList.size() > newList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testInsertionProbabilityMinElements() throws GenerationException {
		final TypeList typeList = new TypeList(new TypeInt(), 6, 8);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		final List<Object> newList = (List<Object>)mutation.doMutation(0.1, strategy, typeList, oldList, null);
		
		assertTrue("If the list has the minimum number of elements the list should have a new element.", oldList.size()+1 == newList.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testEliminationProbabilityMinElements() throws GenerationException {
		final TypeList typeList = new TypeList(new TypeInt(), 4, 6);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		final List<Object> newList = (List<Object>)mutation.doMutation(0.1, strategy, typeList, oldList, null);
		
		assertTrue("If the list has the maximum number of elements  the size of the list should be less than before.", oldList.size() == newList.size()+1);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testMutationSpecificPositionsList() throws GenerationException {
		final TypeList typeList = new TypeList(new TypeInt(), 6, 6);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		
		final Random r = mock(Random.class);
		when(r.nextDouble()).thenReturn(0.70).thenReturn(0.70).thenReturn(0.65).thenReturn(0.70).thenReturn(0.65).thenReturn(0.70);
		mutation.setPRNG(r);
		
		final List<Object> newList = (List<Object>)mutation.doMutation(0.1, strategy, typeList, oldList, null);

		verify(r, times(6)).nextDouble();
		
		assertTrue("Some components of the list should have been mutated if the list has a fixed size and it is not empty.", !newList.equals(oldList));
		assertTrue("Only the selected positions should have been mutated",
				newList.get(0).equals(oldList.get(0)) &&
				newList.get(1).equals(oldList.get(1)) &&
				!newList.get(2).equals(oldList.get(2)) &&
				newList.get(3).equals(oldList.get(3)) &&
				!newList.get(4).equals(oldList.get(4)) &&
				newList.get(5).equals(oldList.get(5)));
	}	
}
