package es.uca.webservices.gamera.ggen.genetic.mutation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.genetic.mutation.GAMutationOperator;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;

/**
 * Tests for the CompositeByPositionMutationOperator class.
 *
 * @author Álvaro Cortijo-García, Antonio García-Domínguez
 */
public class CompositeByPositionMutationOperatorTest {

	private CompositeByPositionMutationOperator mutation;
	private BinaryMutationOperator intMutOp;
	private GrayBinaryMutationOperator floatMutOp;
	private TypeInt typeInt = new TypeInt();
	private TypeFloat typeFloat = new TypeFloat();
	private List<IType> typesTuple = new ArrayList<IType>();
	private List<Object> fatherCmpValues = new ArrayList<Object>();
	private TypeTuple typeTuple;
	private UniformRandomStrategy strategy = new UniformRandomStrategy();

	@Before
	public void setUp() throws Exception {
		mutation = new CompositeByPositionMutationOperator();
		mutation.setPRNG(new Random(0));
		intMutOp = mock(BinaryMutationOperator.class);
		floatMutOp = mock(GrayBinaryMutationOperator.class);
		final List<GAMutationOperator> mutOps = new ArrayList<GAMutationOperator>();
		mutOps.add(intMutOp);
		mutOps.add(intMutOp);
		mutOps.add(floatMutOp);
		mutOps.add(floatMutOp);
		mutation.setOps(mutOps);

		for(int i = 0; i < 2; i++){
			fatherCmpValues.add(i);
			typesTuple.add(typeInt);
		}
		fatherCmpValues.add((float)1.6);
		typesTuple.add(typeFloat);
		fatherCmpValues.add((float)3.0);
		typesTuple.add(typeFloat);
		typeTuple = new TypeTuple(typesTuple.toArray(new IType[typesTuple.size()]));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void correctApplyIntMutation() throws Exception {
		when(intMutOp.doMutation(0.1, strategy, typeInt, 1, null)).thenReturn(33);

		final List<Integer> componentsToMutate = new ArrayList<Integer>();
		componentsToMutate.add(1);
		final List<Object> childCmpValues = (List<Object>)mutation.doMutation(0.1, strategy, typeTuple, fatherCmpValues, componentsToMutate);
		
		verify(intMutOp, times(1)).doMutation(0.1, strategy, typeInt, 1, null);
		
		assertTrue("Only the selected components have been mutated",
				fatherCmpValues.get(0) == childCmpValues.get(0) &&
				fatherCmpValues.get(1) != childCmpValues.get(1) &&
				fatherCmpValues.get(2) == childCmpValues.get(2) &&
				fatherCmpValues.get(3) == childCmpValues.get(3));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void correctApplyFloatMutation() throws Exception {
		when(floatMutOp.doMutation(0.1, strategy, typeFloat, (float)1.6, null)).thenReturn(2.4);

		final List<Integer> componentsToMutate = new ArrayList<Integer>();
		componentsToMutate.add(2);
		final List<Object> childCmpValues = (List<Object>)mutation.doMutation(0.1, strategy, typeTuple, fatherCmpValues, componentsToMutate);
		
		verify(floatMutOp, times(1)).doMutation(0.1, strategy, typeFloat, (float)1.6, null);
		
		assertTrue("Only the selected components have been mutated",
				fatherCmpValues.get(0) == childCmpValues.get(0) &&
				fatherCmpValues.get(1) == childCmpValues.get(1) &&
				fatherCmpValues.get(2) != childCmpValues.get(2) &&
				fatherCmpValues.get(3) == childCmpValues.get(3));
	}
		
	@SuppressWarnings("unchecked")
	@Test
	public void correctApplyMutation() throws Exception {
		final List<Integer> componentsToMutate = new ArrayList<Integer>();
		componentsToMutate.add(1);
		componentsToMutate.add(2);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		final List<Object> childCmpValues = (List<Object>)mutation.doMutation(0.1, strategy, typeTuple, fatherCmpValues, componentsToMutate);
		assertTrue("Only the selected components have been mutated",
				fatherCmpValues.get(0) == childCmpValues.get(0) &&
				fatherCmpValues.get(1) != childCmpValues.get(1) &&
				fatherCmpValues.get(2) != childCmpValues.get(2) &&
				fatherCmpValues.get(3) == childCmpValues.get(3));
	}

	@Test(expected = InvalidConfigurationException.class)
	public void testNullSeed() throws Exception {
		mutation.setPRNG(null);
		mutation.validate();
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void testNullOperator() throws Exception {
		final List<GAMutationOperator> mutList = new ArrayList<GAMutationOperator>();
		mutList.add(null);
		mutation.setOps(mutList);
		mutation.validate();
	}
}
