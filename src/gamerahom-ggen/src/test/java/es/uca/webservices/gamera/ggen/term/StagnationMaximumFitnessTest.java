package es.uca.webservices.gamera.ggen.term;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.Fitness;
import es.uca.webservices.gamera.ggen.conf.Configuration;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Tests the stagnation of maximum fitness
 * @author alvaro
 *
 */
public class StagnationMaximumFitnessTest {
	
	@Test(expected=InvalidConfigurationException.class)
	public void negativeCount() throws InvalidConfigurationException{
		StagnationMaximumFitness term = new StagnationMaximumFitness();
		term.setCount(-1);
		term.validate();
	}
	
	@Test(expected=InvalidConfigurationException.class)
	public void zeroCount() throws InvalidConfigurationException{
		StagnationMaximumFitness term = new StagnationMaximumFitness();
		term.setCount(0);
		term.validate();
	}
	
	@Test
	public void positiveCount() throws InvalidConfigurationException{
		StagnationMaximumFitness term = new StagnationMaximumFitness();
		term.setCount(1);
		term.validate();
	}
	
	@Test(expected=InvalidConfigurationException.class)
	public void zeroRelativeChange() throws Exception {
		StagnationMaximumFitness term = new StagnationMaximumFitness();
		term.setCount(1);
		term.setRelativeMinimumChange(0);
		term.validate();
	}
	
	@Test(expected=InvalidConfigurationException.class)
	public void negativeRelativeChange() throws Exception {
		StagnationMaximumFitness term = new StagnationMaximumFitness();
		term.setCount(1);
		term.setRelativeMinimumChange(-1);
		term.validate();
	}

	@Test
	public void worksAsExpected() {
		final StagnationMaximumFitness term = new StagnationMaximumFitness();
		term.setCount(2);
        final GgenState state = createStateWithFitnessValues(3);
        assertFalse(term.evaluate(state));
        assertFalse(term.evaluate(state));
        assertTrue(term.evaluate(state));
	}
	
    @Test
    public void minimumChangeIsRelative() {
    	final StagnationMaximumFitness term = new StagnationMaximumFitness();
		term.setCount(2);
		term.setRelativeMinimumChange(0.01);
        final GgenState originalState = createStateWithFitnessValues(100);
        final GgenState notBetterEnoughState = createStateWithFitnessValues(100.5);
        final GgenState betterEnoughState = createStateWithFitnessValues(101);
        final GgenState notBetterEnoughState2 = createStateWithFitnessValues(101.5);
        assertFalse(term.evaluate(originalState));
        assertFalse(term.evaluate(notBetterEnoughState));
        assertFalse(term.evaluate(betterEnoughState));
        assertFalse(term.evaluate(notBetterEnoughState2));
        assertTrue(term.evaluate(notBetterEnoughState2));
    }
	
    private GgenState createStateWithFitnessValues(double... fitnessValues) {
        final GgenPopulation pop = new GgenPopulation();
        final List<ComparisonResult> fakeComparisons = new ArrayList<ComparisonResult>();
        for (double fitness : fitnessValues) {
            GgenIndividual ind = new GgenIndividual();
            ind.setFitness(fitness);
            pop.addIndividual(ind);
            fakeComparisons.add(ComparisonResult.SAME_OUTPUT);
        }

        final ComparisonResult[] comparisonResults = fakeComparisons.toArray(new ComparisonResult[fakeComparisons.size()]);
        final GgenState state = new GgenState();
        final Configuration configuration = new Configuration();
        configuration.setFitness(new Fitness());
		state.setConfiguration(configuration);
        state.getHistory().add(pop, new ComparisonResults[] {
                new ComparisonResults(1, 1, 1, true, comparisonResults)
        });
        return state;
    }

}
