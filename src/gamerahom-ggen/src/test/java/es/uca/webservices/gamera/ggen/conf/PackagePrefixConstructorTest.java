package es.uca.webservices.gamera.ggen.conf;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.ConstructorException;

/**
 * Tests for the package prefix SnakeYAML constructor.
 *
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class PackagePrefixConstructorTest {

    @Test
    public void missingRootIsReported() throws Exception {
        try {
            Yaml parser = getParser();
            parser.load("!!do.not.exist {}");
            fail("A ConstructorException was expected");
        } catch (ConstructorException ex) {
            assertClassWasNotFound(ex);
        }
    }

    @Test
    public void wrongPrefixDoesNotWork() throws Exception {
        try {
            Yaml parser = getParser("es.uca.webservices.wrong");
            parser.load("!!conf.TestBean");
            fail("A ConstructorException was expected");
        } catch (ConstructorException ex) {
            assertClassWasNotFound(ex);
        }
    }

    private void assertClassWasNotFound(ConstructorException ex) {
        assertContains(
                "The constructor exception should be due to a ClassNotFoundException",
                "Class not found", ex.getCause().getMessage());
    }

    private void assertContains(String msg, String needle, String haystack) {
        if (!haystack.contains(needle)) {
            fail(msg);
        }
    }

    private Yaml getParser() throws ClassNotFoundException {
        return getParser(ConfigurationLoader.CLASS_TAG_PREFIX);
    }

    private Yaml getParser(final String prefix) {
        return new Yaml(new PackagePrefixConstructor(prefix));
    }

}
