package es.uca.webservices.gamera.ggen.genetic.mutation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;

/**
 * Tests the mutation operator class. The mutation of an individual is correct
 * @author Álvaro Cortijo-García, alvaro
 *
 */
public class MutationOperatorTest {
	
	private MutationOperator mutation;
	
	@Before
	public void setUp() {
		mutation = new MutationOperator();
		mutation.setPRNG(new Random(0));
		mutation.setMutationRange(10);
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void nullPrng() throws Exception {
		mutation.setMutationRange(10);
		mutation.setPRNG(null);
		mutation.validate();
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void negativeConstantMutation() throws Exception {
		testConstantMutation(-1);
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void zeroConstantMutation() throws Exception {
		mutation.setMutationRange(0);
		mutation.validate();
	}
	
	@Test
	public void validConstantMutation() throws Exception {
		mutation.setMutationRange(1);
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void negativeProbability() throws Exception {
		testProbability(-0.5);
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void largerThanOneProbability() throws Exception {
		testProbability(1.1);
	}
	
	@Test
	public void oneProbability() throws Exception {
		testProbability(1);
	}
	
	@Test(expected=InvalidConfigurationException.class)
	public void zeroProbability() throws Exception {
		testProbability(0);
	}
	
	private void testProbability(final double percent) throws InvalidConfigurationException {
		final GAGeneticOperatorOptions opts = new GAGeneticOperatorOptions();
		opts.setProbability(percent);
        opts.validate();
	}
	
	private void testConstantMutation(final double value) throws InvalidConfigurationException {
		mutation.setMutationRange(value);
		mutation.validate();
    }
	
	@Test
	public void intMutationHonorsMinMax() throws Exception {
		final TypeInt type = new TypeInt();
		final int minValue = 0;
		final int maxValue = 5;
		type.setMinValue(minValue);
		type.setMaxValue(maxValue);

		mutation.setMutationRange(100);
		final double probability = 0.1;
		for (int i = 0; i < 100; ++i) {
			final int value = ((BigInteger)mutation.doMutation(probability, null, type, 3, null)).intValue();
			assertTrue("Mutations on integers should respect minimum value constraints", value >= minValue);
			assertTrue("Mutations on integers should respect maximum value constraints",value <= maxValue);
		}
	}

	@Test
	public void intMutationHonorsValues() throws Exception {
		final Set<BigInteger> allowedValues = new HashSet<BigInteger>();
		allowedValues.add(BigInteger.ONE);
		allowedValues.add(BigInteger.TEN);
		allowedValues.add(BigInteger.ZERO);

		final TypeInt type = new TypeInt();
		type.setAllowedValues(new ArrayList<BigInteger>(allowedValues));

		mutation.setMutationRange(100);
		final double probability = 0.1;
		for (int i = 0; i < 100; ++i) {
			final BigInteger value = (BigInteger)mutation.doMutation(probability, null, type, BigInteger.ONE, null);
			assertTrue("Mutations on integers should respect enumeration constraints",
					allowedValues.contains(value));
		}
	}

	@Test
	public void floatMutationHonorsMinMax() throws Exception {
		final TypeFloat type = new TypeFloat();
		final double minValue = 0.5d;
		final double maxValue = 5.5d;
		type.setMinValue(new BigDecimal(minValue));
		type.setMaxValue(new BigDecimal(maxValue));

		mutation.setMutationRange(100);
		final double probability = 0.1;
		for (int i = 0; i < 100; ++i) {
			final double value = ((BigDecimal)mutation.doMutation(probability, null, type, (float)Math.PI, null)).doubleValue();
			assertTrue("Mutations on floats should respect minimum value constraints", value >= minValue);
			assertTrue("Mutations on floats should respect maximum value constraints", value <= maxValue);
		}
	}

	@Test
	public void doubleMutationHonorsMinMax() throws Exception {
		final TypeFloat type = new TypeFloat();
		final double minValue = 0.5d;
		final double maxValue = 5.5d;
		type.setMinValue(new BigDecimal(minValue));
		type.setMaxValue(new BigDecimal(maxValue));

		final double probability = 0.1;
		mutation.setMutationRange(100);
		for (int i = 0; i < 100; ++i) {
			final double value = ((BigDecimal)mutation.doMutation(probability, null, type, Math.PI, null)).doubleValue();
			assertTrue("Mutations on floats should respect minimum value constraints", value >= minValue);
			assertTrue("Mutations on floats should respect maximum value constraints", value <= maxValue);
		}
	}
	
	@Test
	public void doubleMutationHonorsValues() throws Exception {
		final Set<BigDecimal> allowed = new HashSet<BigDecimal>();
		allowed.add(new BigDecimal(Math.E));
		allowed.add(new BigDecimal(Math.PI));
		allowed.add(new BigDecimal(Math.E * 2));

		final double probability = 0.1;
		final TypeFloat type = new TypeFloat(new ArrayList<BigDecimal>(allowed));
		mutation.setMutationRange(100);
		for (int i = 0; i < 100; ++i) {
			final BigDecimal value = (BigDecimal)mutation.doMutation(probability, null, type, Math.PI, null);
			assertTrue("Mutations on floats should respect enumeration constraints", allowed.contains(value));
		}
	}

	@Test
	public void emptyTupleMutationDoesNothing() throws Exception {
		final TypeTuple type = new TypeTuple();
		final ArrayList<Object> original = new ArrayList<Object>();
		final Object result = mutation.doMutation(1, null, type, original, null);
		assertEquals("Empty tuples should be kept as is", original, result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void singleTupleMutationShouldChangeItsOnlyElement() throws Exception {
		mutation.setMutationRange(100);
		final double probability = 0.1;

		final TypeInt type1D6 = new TypeInt(1, 6);
		final TypeTuple typeTuple = new TypeTuple(type1D6);

		final int oldInt = 2;
		final List<Object> oldTuple = Arrays.asList((Object)oldInt);
		final List<Object> newTuple = (List<Object>)mutation.doMutation(probability, null, typeTuple, oldTuple, null);
		final int newInt = ((BigInteger)newTuple.get(0)).intValue();

		assertNotSame("The original tuple should be changed while mutating", oldTuple, newTuple);
		assertTrue("The only value in the tuple should have changed", newInt != oldInt);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void pairMutationShouldChangeOnlyOneOfItsElements() throws Exception {
		mutation.setMutationRange(100);
		final double probability = 0.1;

		final TypeTuple typeTuple = new TypeTuple(new TypeInt(1, 10), new TypeFloat());
		final BigInteger oldInt = BigInteger.TEN;
		final BigDecimal oldFloat = BigDecimal.ONE;
		final List<Object> oldTuple = Arrays.asList((Object)oldInt, oldFloat);
		final List<Object> newTuple = (List<Object>)mutation.doMutation(probability, null, typeTuple, oldTuple, null);
		final BigInteger newInt = (BigInteger)newTuple.get(0);
		final BigDecimal newFloat = (BigDecimal)newTuple.get(1);
		
		assertTrue("Exactly one of the elements of the tuple should have changed",
					oldInt.equals(newInt) != oldFloat.equals(newFloat));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void correctApplyMutation() throws Exception {
		final TypeInt type = new TypeInt();
		final List<TypeInt> typesTuple = new ArrayList<TypeInt>();
		final List<Integer> fatherCmpValues = new ArrayList<Integer>();
		for(int i = 0; i < 4; i++){
			fatherCmpValues.add(i);
			typesTuple.add(type);
		}
		final TypeTuple typeTuple = new TypeTuple(typesTuple.toArray(new TypeInt[typesTuple.size()]));
		final List<Integer> componentsToMutate = new ArrayList<Integer>();
		componentsToMutate.add(1);
		componentsToMutate.add(2);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		final List<Object> childCmpValues = (List<Object>)mutation.doMutation(0.1, strategy, typeTuple, fatherCmpValues, componentsToMutate);
		assertTrue("Only the selected components have been mutated",
				fatherCmpValues.get(0) == childCmpValues.get(0) &&
				fatherCmpValues.get(1) != childCmpValues.get(1) &&
				fatherCmpValues.get(2) != childCmpValues.get(2) &&
				fatherCmpValues.get(3) == childCmpValues.get(3));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void nonFixedAndEmptyListMutation() throws Exception {
		final TypeList typeList = new TypeList(new TypeInt(), 0, 8);
		final List<Object> oldList = new ArrayList<Object>();
		final GAGeneticOperatorOptions opts = new GAGeneticOperatorOptions();
		opts.setProbability(0.1);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		final List<Object> newList = (List<Object>)mutation.doMutation(opts.getProbability(), strategy, typeList, oldList, null);
		assertTrue("An empty list without a fixed size should have at least one component",
				!newList.isEmpty());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void nonFixedAndNonEmptyListMutation() throws Exception {
		final TypeList typeList = new TypeList(new TypeInt(), 3, 8);
		final BigInteger oldValue1 = new BigInteger("0");
		final BigInteger oldValue2 =  new BigInteger("1");
		final BigInteger oldValue3 =  new BigInteger("2");
		final List<Object> oldList = Arrays.asList((Object)oldValue1, oldValue2, oldValue3);
		final GAGeneticOperatorOptions opts = new GAGeneticOperatorOptions();
		opts.setProbability(0.1);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		final List<Object> newList = (List<Object>)mutation.doMutation(opts.getProbability(), strategy, typeList, oldList, null);
		assertTrue("Size or one component should have been mutated",
				oldList.size() != newList.size() && assertListPartsEquals(oldList, newList) ||
				oldList.size() == newList.size() && !oldList.equals(newList));
	}
	
	private static boolean assertListPartsEquals(List<Object> list1, List<Object> list2) {
		int tam = Math.min(list1.size(), list2.size());
		for(int i = 0; i < tam; i++){
			if(!list1.get(i).equals(list2.get(i)))
				return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void fixedAndEmptyListMutation() throws Exception {
		final TypeList typeList = new TypeList(new TypeInt(), 0, 0);
		final List<Object> oldList = new ArrayList<Object>();
		final GAGeneticOperatorOptions opts = new GAGeneticOperatorOptions();
		opts.setProbability(0.1);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		final List<Object> newList = (List<Object>)mutation.doMutation(opts.getProbability(), strategy, typeList, oldList, null);
		assertTrue("An empty list with fixed size should not be mutated",
				newList.isEmpty());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void fixedAndNonEmptyListMutation() throws Exception {
		final TypeList typeList = new TypeList(new TypeInt(), 3, 3);
		final BigInteger oldValue1 = new BigInteger("0");
		final BigInteger oldValue2 =  new BigInteger("1");
		final BigInteger oldValue3 =  new BigInteger("2");
		final List<Object> oldList = Arrays.asList((Object)oldValue1, oldValue2, oldValue3);
		final GAGeneticOperatorOptions opts = new GAGeneticOperatorOptions();
		opts.setProbability(0.1);
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		final List<Object> newList = (List<Object>)mutation.doMutation(opts.getProbability(), strategy, typeList, oldList, null);
		final BigInteger newValue1 = (BigInteger)newList.get(0);
		final BigInteger newValue2 = (BigInteger)newList.get(1);
		final BigInteger newValue3 = (BigInteger)newList.get(2);
		assertTrue("Only one component of a non-empty list with fixed size should be mutated",
				oldValue1 != newValue1 && oldValue2 == newValue2 && oldValue3 == newValue3 ||
				oldValue1 == newValue1 && oldValue2 != newValue2 && oldValue3 == newValue3 ||
				oldValue1 == newValue1 && oldValue2 == newValue2 && oldValue3 != newValue3);
	}

}
