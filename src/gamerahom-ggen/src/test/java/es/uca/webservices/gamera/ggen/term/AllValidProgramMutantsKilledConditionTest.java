package es.uca.webservices.gamera.ggen.term;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.ggen.individuals.Component;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;
import es.uca.webservices.testgen.api.types.TypeInt;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests for the {@link AllValidProgramMutantsKilledCondition} class.
 *
 * @author Antonio García Domínguez
 */
public class AllValidProgramMutantsKilledConditionTest {

    private final AllValidProgramMutantsKilledCondition condition = new AllValidProgramMutantsKilledCondition();

    @Test
    public void done() {
        GgenState state = new GgenState();
        final TypeInt type = new TypeInt();
        state.getHistory().add(
                new GgenPopulation(
                        new GgenIndividual(new Component(type, 2)),
                        new GgenIndividual(new Component(type, 3))
                ),
                new ComparisonResults[] {
                        new ComparisonResults(1, 1, 1, true, 0, 1),
                        new ComparisonResults(1, 1, 2, true, 1, 1),
                }
        );

        assertTrue(condition.evaluate(state));
    }

    @Test
    public void doneWithInvalid() {
        GgenState state = new GgenState();
        final TypeInt type = new TypeInt();
        state.getHistory().add(
                new GgenPopulation(
                        new GgenIndividual(new Component(type, 2)),
                        new GgenIndividual(new Component(type, 3))
                ),
                new ComparisonResults[] {
                        new ComparisonResults(1, 1, 1, true, 0, 1),
                        new ComparisonResults(1, 1, 2, true, 1, 1),
                        new ComparisonResults(1, 1, 3, true, 2, 2),
                }
        );

        assertTrue(condition.evaluate(state));
    }

    @Test
    public void notDone() {
        GgenState state = new GgenState();
        final TypeInt type = new TypeInt();
        state.getHistory().add(
                new GgenPopulation(
                        new GgenIndividual(new Component(type, 2)),
                        new GgenIndividual(new Component(type, 3))
                ),
                new ComparisonResults[] {
                        new ComparisonResults(1, 1, 1, true, 0, 1),
                        new ComparisonResults(1, 1, 2, true, 1, 0),
                }
        );

        assertFalse(condition.evaluate(state));
    }

    @Test
    public void notDoneWithInvalid() {
        GgenState state = new GgenState();
        final TypeInt type = new TypeInt();
        state.getHistory().add(
                new GgenPopulation(
                        new GgenIndividual(new Component(type, 2)),
                        new GgenIndividual(new Component(type, 3))
                ),
                new ComparisonResults[] {
                        new ComparisonResults(1, 1, 1, true, 0, 1),
                        new ComparisonResults(1, 1, 2, true, 1, 0),
                        new ComparisonResults(1, 1, 3, true, 2, 2),
                }
        );

        assertFalse(condition.evaluate(state));
    }

    @Test
    public void empty() {
        assertFalse(condition.evaluate(new GgenState()));
    }
}
