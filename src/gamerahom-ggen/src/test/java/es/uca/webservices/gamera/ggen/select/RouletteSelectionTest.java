package es.uca.webservices.gamera.ggen.select;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.Random;

import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.ggen.DefaultConfiguration;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.parsers.ParserException;
/**
 * The selection produced by roulette algorithm is correct
 * @author alvaro
 */
public class RouletteSelectionTest {
	
	@Test(expected = InvalidConfigurationException.class)
	public void nullPrng() throws Exception {
		RouletteSelection rouletteSelection = new RouletteSelection();
		rouletteSelection.setPRNG(null);
		rouletteSelection.validate();
	}
	
	@Test
	public void notNullSelection() throws FileNotFoundException, 
		InvalidConfigurationException, ClassNotFoundException, GenerationException, 
			ParserException {
		DefaultConfiguration defaultConf = new DefaultConfiguration();
		final Random prng = defaultConf.getConf().getPrng();
		defaultConf.setFirstGenerationTest();
		GgenPopulation father = defaultConf.getPopulation();
		RouletteSelection rouletteSelection = new RouletteSelection();
		rouletteSelection.setPRNG(prng);
		GgenIndividual individual = new GgenIndividual();
		individual = rouletteSelection.select(father);
		assertNotNull(individual);
	}

	@Test
	public void testRoulette() throws FileNotFoundException, ClassNotFoundException, 
		InvalidConfigurationException, ParserException, GenerationException, 
			ComparisonException {
		DefaultConfiguration defaultConf = new DefaultConfiguration();
		final Random prng = defaultConf.getConf().getPrng();
		defaultConf.setFirstGenerationTest();
		double totalFitness = 0;
		GgenPopulation father = defaultConf.getPopulation();
		for (GgenIndividual ind : father.getPopulation()){
			ind.setFitness(prng.nextInt(20));
			totalFitness += ind.getFitness();
		}
		RouletteSelection rouletteSelection = new RouletteSelection();
		rouletteSelection.setPRNG(prng);
		double rouletteResult = Math.abs(prng.nextDouble()) * totalFitness;
		int startingPosition = prng.nextInt(father.getPopulationSize());
		
		assertTrue(rouletteResult >= 0 && rouletteResult < totalFitness);
		
		GgenIndividual indSelected = rouletteSelection.doRoulette(father.getPopulation(), 0, startingPosition);
		
		assertEquals(indSelected,father.getIndividual(startingPosition));
		assertNotNull(rouletteSelection.doRoulette(father.getPopulation(), rouletteResult, startingPosition));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void startingPositionIlegal() throws GenerationException, ParserException, FileNotFoundException, InvalidConfigurationException, ClassNotFoundException{
		DefaultConfiguration defaultConf = new DefaultConfiguration();
		defaultConf.setFirstGenerationTest();
		GgenPopulation father = defaultConf.getPopulation();
		
		RouletteSelection rouletteSelection = new RouletteSelection();
		rouletteSelection.doRoulette(father.getPopulation(), 0, father.getPopulationSize());
	}
}
