package es.uca.webservices.gamera.ggen.genetic.crossover;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.ggen.individuals.Component;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.testgen.api.types.TypeInt;
/**
 * Tests for the DoublePointComponentCrossoverOperator class.
 *
 * @author Álvaro Cortijo-García, Antonio García-Domínguez
 */
public class DoublePointComponentCrossoverOperatorTest {

	private DoublePointComponentCrossoverOperator crossover;
	private GgenIndividual father, mother, left, right;

	@Before
	public void setUp() {
		crossover = new DoublePointComponentCrossoverOperator();

		final TypeInt type = new TypeInt();
		List<Component> cmps = new ArrayList<Component>();
		for(int i = 0; i < 8; i++){
			final Component cmp = new Component(type, i);
			cmps.add(cmp);
		}
		father = new GgenIndividual(cmps.get(0), cmps.get(1), cmps.get(2),cmps.get(3));
		mother = new GgenIndividual(cmps.get(4),cmps.get(5), cmps.get(6),cmps.get(7));
		left =  new GgenIndividual(new ArrayList<Component>(father.getComponents()));
		right =  new GgenIndividual(new ArrayList<Component>(mother.getComponents()));
	}

	@Test
	public void fourComponentsDoCross() throws Exception {
		final int position1 = 1;
		final int position2 = 2;

		crossover.doCrossover(left, right, position1, position2);
		assertCrossoverIsCorrect(position1, position2, father, mother, left, right);
	}

	@Test
	public void samePointsDoNotCross() throws Exception {
		final int position1 = 2;
		final int position2 = 2;

		crossover.doCrossover(left, right, position1, position2);
		assertEquals(father, left);
		assertEquals(mother, right);
	}

	@Test
	public void pointsInLimitsDoExchange() throws Exception {
		final int position1 = 0;
		final int position2 = 4;

		crossover.doCrossover(left, right, position1, position2);
		assertEquals(father, right);
		assertEquals(mother, left);
	}
	
	@Test
	public void applyRetriesForSamePoints() throws Exception {
		final Random r = mock(Random.class);
		when(r.nextInt(5)).thenReturn(2).thenReturn(2).thenReturn(1).thenReturn(2);
		crossover.setPRNG(r);

		crossover.apply(new GAGeneticOperatorOptions(), father, mother);
		verify(r, times(4)).nextInt(5);
		
		assertCrossoverIsCorrect(1, 2, father, mother, left, right);
	}

	@Test
	public void applyRetriesForPointsInLimits() throws Exception {
		final Random r = mock(Random.class);
		when(r.nextInt(5)).thenReturn(0).thenReturn(4).thenReturn(1).thenReturn(2);
		crossover.setPRNG(r);

		crossover.apply(new GAGeneticOperatorOptions(), father, mother);
		verify(r, times(4)).nextInt(5);
		
		assertCrossoverIsCorrect(1, 2, father, mother, left, right);
	}


	private static void assertCrossoverIsCorrect(final int position1, final int position2,
			GgenIndividual father, GgenIndividual mother,
			GgenIndividual leftChild, GgenIndividual rightChild) {
		int cont = 0;
		while (cont < position1) {
			assertEquals(father.getComponent(cont), leftChild.getComponent(cont));
			assertEquals(mother.getComponent(cont), rightChild.getComponent(cont));
			cont++;
		}
		while (cont < position2) {
			assertEquals(mother.getComponent(cont), leftChild.getComponent(cont));
			assertEquals(father.getComponent(cont), rightChild.getComponent(cont));
			cont++;
		}
		while (cont < father.getNumberComponents()) {
			assertEquals(father.getComponent(cont), leftChild.getComponent(cont));
			assertEquals(mother.getComponent(cont), rightChild.getComponent(cont));
			cont++;
		}
	}

}
