package es.uca.webservices.gamera.ggen.generate;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.api.types.TypeDate;
import es.uca.webservices.testgen.api.types.TypeDateTime;
import es.uca.webservices.testgen.api.types.TypeDuration;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.api.types.TypeTime;
import es.uca.webservices.testgen.api.types.TypeTuple;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;

/**
 * Tests for the {@link es.uca.webservices.gamera.ggen.generate.FixedGenerator} class.
 */
public class FixedGeneratorTest {

    public static final String PATH_TO_VM = "src/test/resources/LoanApprovalRPC/data.vm";
    public static final String PATH_TO_SPEC = "src/test/resources/LoanApprovalRPC/data.spec";

    @Test
    public void validConfiguration() throws Exception {
        final FixedGenerator gen = createGoodConfiguration();
        gen.validate();
    }

    @Test(expected = InvalidConfigurationException.class)
    public void missingDataIsRejected() throws Exception {
        final FixedGenerator gen = createGoodConfiguration();
        gen.setDataFile(null);
        gen.validate();
    }

    @Test(expected = InvalidConfigurationException.class)
    public void missingPRNGIsRejected() throws Exception {
        final FixedGenerator gen = createGoodConfiguration();
        gen.setPRNG(null);
        gen.validate();
    }

    @Test
    public void mapInteger() throws Exception {
        final int value = 42;
        BigInteger bi = (BigInteger)new FixedGenerator().mapValue(value, new TypeInt());
        assertThat(bi.intValue(), is(equalTo(value)));
    }

    @Test
    public void mapLong() throws Exception {
        final long value = 1337;
        BigInteger bi = (BigInteger)new FixedGenerator().mapValue(value, new TypeInt());
        assertThat(bi.longValue(), is(equalTo(value)));
    }

    @Test(expected = GenerationException.class)
    public void mapBadInteger() throws Exception {
        new FixedGenerator().mapValue("foo", new TypeInt());
    }

    @Test
    public void mapFloat() throws Exception {
        final float pi = 3.14f;
        BigDecimal bdPi = (BigDecimal)new FixedGenerator().mapValue(pi, new TypeFloat());
        assertThat(bdPi.setScale(2, RoundingMode.DOWN).toString(), is(equalTo("3.14")));
    }

    @Test
    public void mapDouble() throws Exception {
        final double e = 2.71828183;
        BigDecimal bdE = (BigDecimal)new FixedGenerator().mapValue(e, new TypeFloat());
        assertThat(bdE.toString(), is(equalTo("2.71828183")));
    }

    @Test(expected = GenerationException.class)
    public void mapBadDouble() throws Exception {
        new FixedGenerator().mapValue("moo", new TypeFloat());
    }

    @Test
    public void mapString() throws Exception {
        final String value = "bar";
        final String s = (String) new FixedGenerator().mapValue(value, new TypeString());
        assertThat(s, is(equalTo(value)));
    }

    @Test(expected = GenerationException.class)
    public void mapBadString() throws Exception {
        new FixedGenerator().mapValue(28, new TypeString());
    }

    @Test
    public void mapEmptyList() throws Exception {
        final List l = (List) new FixedGenerator().mapValue(new ArrayList(), new TypeList(new TypeInt()));
        assertThat(l.size(), is(equalTo(0)));
    }

    @Test
    public void mapListWithOneInt() throws Exception {
        final List<BigInteger> l = (List<BigInteger>) new FixedGenerator().mapValue(Arrays.asList(Integer.valueOf(1)), new TypeList(new TypeInt()));
        assertThat(l.size(), is(equalTo(1)));
        assertThat(l.get(0).intValue(), is(equalTo(1)));
    }

    @Test
    public void mapListWithTwoStrings() throws Exception {
        final List<String> l = (List<String>) new FixedGenerator().mapValue(Arrays.asList("hi", "ho"), new TypeList(new TypeString()));
        assertThat(l.size(), is(equalTo(2)));
        assertThat(l.get(0), is(equalTo("hi")));
        assertThat(l.get(1), is(equalTo("ho")));
    }

    @Test(expected = GenerationException.class)
    public void mapBadList() throws Exception {
        new FixedGenerator().mapValue(Arrays.asList(4, "yo"), new TypeList(new TypeInt()));
    }

    @Test
    public void mapTupleList() throws Exception {
        final List l = (List) new FixedGenerator().mapValue(Arrays.asList(
                Arrays.asList(1, "a"),
                Arrays.asList(2, "b")
        ), new TypeList(new TypeTuple(new TypeInt(), new TypeString())));
        assertThat(l.size(), is(equalTo(2)));

        final List firstTuple = (List) l.get(0);
        assertThat(firstTuple.size(), is(equalTo(2)));
        assertThat(((BigInteger)firstTuple.get(0)).intValue(), is(equalTo(1)));
        assertThat((String)firstTuple.get(1), is(equalTo("a")));

        final List secondTuple = (List) l.get(1);
        assertThat(secondTuple.size(), is(equalTo(2)));
        assertThat(((BigInteger)secondTuple.get(0)).intValue(), is(equalTo(2)));
        assertThat((String)secondTuple.get(1), is(equalTo("b")));
    }

    @Test
    public void roundtripDate() throws Exception {
        roundtripThroughString(new TypeDate());
    }

    @Test
    public void roundtripDateTime() throws Exception {
        roundtripThroughString(new TypeDateTime());
    }

    @Test
    public void roundtripTime() throws Exception {
        roundtripThroughString(new TypeTime());
    }
    
    @Test
    public void roundtripDuration() throws Exception {
    	roundtripThroughString(new TypeDuration());
    }

    private void roundtripThroughString(IType date) throws GenerationException {
    	// Use a fixed seed to avoid collecting entropy
		final UniformRandomStrategy strategy = new UniformRandomStrategy();
		strategy.setSeed("0000111100001111");

		final Object generated = strategy.visit(date);
        final String lexical = generated.toString();
        final Object recovered = new FixedGenerator().mapValue(lexical, date);
        assertThat(recovered, is(equalTo(generated)));
    }

    private FixedGenerator createGoodConfiguration() throws ParserException {
        final FixedGenerator gen = new FixedGenerator();
        gen.setDataFile(new File(PATH_TO_VM));
        gen.setPRNG(new Random());
        return gen;
    }
}
