package es.uca.webservices.gamera.ggen;

import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;
import java.util.*;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.ggen.api.generate.GAIndividualGeneratorOptions;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.conf.Configuration;
import es.uca.webservices.gamera.ggen.conf.ConfigurationLoader;
import es.uca.webservices.gamera.ggen.generate.FixedGenerator;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.log.NullLogger;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * Tests using a fixed initial population in the YAML configuration.
 */
public class FixedInitialPopulationConfigurationTest {
    public static final String VM_PREFIX = "src/test/resources/LoanApprovalRPC_ODE/";

    private GgenIndividualGenerator mockGenerator;
    private GAIndividualGeneratorOptions mockGeneratorOptions;
    private GgenPopulation zeroPopulation;
    private GeneticAlgorithm geneticAlgorithm;
    private FixedGenerator fixedGenerator;

    public FixedInitialPopulationConfigurationTest(){
    	System.setProperty("install.dir", new File("target/dependency").getAbsolutePath());
    }
    
    @Test
    public void generateTwoPopulations() throws Exception {
        final Configuration conf = createConfigurationForDataFile(VM_PREFIX + "data.vm");

        final File originalData = new File(VM_PREFIX + "data.vm");
        final File backup = File.createTempFile("backup", ".vm");
        
        FileUtils.copyFile(originalData, backup);
        try {
            final List<IType> types = conf.getTypes();
            final IStrategy generator = conf.getStrategy();
            GAExecutor executor = conf.getExecutor();          
            executor.prepare();
            conf.setFitness(new Fitness());
            conf.getFitness().prepare(executor.analyze(null), null, new NullLogger());

            // Generate the first population
            final GgenPopulation firstPop = generateFirstPopulation(conf);
            verify(mockGenerator, times(0)).generate(zeroPopulation, generator, types);
            verify(fixedGenerator, times(6)).generate(zeroPopulation, generator, types);

            // Check the contents of the first population
            assertThat(firstPop.getPopulationSize(), is(equalTo(conf.getPopulationSize())));
            assertThat(firstPop.getIndividual(0).getNumberComponents(), is(equalTo(5)));
            final Set<String> validNames = new HashSet<>(Arrays.asList("Manuel", "Maria", "Antonio", "Juan", "Pepe", "Miguel", "Ana", "Rocio"));

            for (int i = 0; i < firstPop.getPopulationSize(); ++i) {
                final String firstName = (String) firstPop.getIndividual(i).getComponent(0).getValue();
                final Integer qty = (Integer) firstPop.getIndividual(i).getComponent(2).getValue();
                assertTrue("Element " + i + " should have a valid firstName", validNames.contains(firstName));
                assertTrue("Element " + i + " should have quantity >= 0", qty >= 0);
                assertTrue("Element " + i + " should have quantity <= 1_000_000", qty <= 1_000_000);

            }

            // Make sure the initial population generators are *not* called from the second generation
            final GgenPopulation secondPopulation = new GgenPopulation();
            geneticAlgorithm.nextGeneration(firstPop, secondPopulation, types, new NullLogger(), null);
            verify(mockGenerator, times(0)).generate(firstPop, generator, types);
            verify(fixedGenerator, times(0)).generate(firstPop, generator, types);
        } finally {
            conf.getExecutor().cleanup();
            FileUtils.copyFile(backup, originalData);
            backup.delete();
        }
    }

    @Test(expected = GenerationException.class)
    public void dataFileWithExtraVariableIsRejected() throws Exception {
        generateFirstPopulation(createConfigurationForDataFile(VM_PREFIX + "bad-data-extra-var.vm"));
    }

    @Test(expected = GenerationException.class)
    public void dataFileWithMissingVariableIsRejected() throws Exception {
        generateFirstPopulation(createConfigurationForDataFile(VM_PREFIX + "bad-data-missing-var.vm"));
    }

    @Test(expected = GenerationException.class)
    public void dataFileWithWrongTypesOfDataIsRejected() throws Exception {
        generateFirstPopulation(createConfigurationForDataFile(VM_PREFIX + "bad-data-wrong-types.vm"));
    }

    private GgenPopulation generateFirstPopulation(Configuration conf) throws GenerationException, ParserException {
        geneticAlgorithm = new GeneticAlgorithm();
        geneticAlgorithm.setConfiguration(conf);

        zeroPopulation = new GgenPopulation();
        final GgenPopulation firstPop = new GgenPopulation();
        geneticAlgorithm.generateNewIndividuals(zeroPopulation, firstPop, conf.getTypes(), conf.getAggregateLogger(), null);
        return firstPop;
    }

    private Configuration createConfigurationForDataFile(String pathToDataFile) throws IOException, ClassNotFoundException, InvalidConfigurationException {
        Configuration config = new ConfigurationLoader().parse(new File("src/test/resources/conf.yaml"));
        addFixedInitialPopulation(pathToDataFile, config);
        config.validate();
        return config;
    }

    private void addFixedInitialPopulation(String pathToDataFile, Configuration config) {
        final Map<GgenIndividualGenerator, GAIndividualGeneratorOptions> oldGenerators = new HashMap<GgenIndividualGenerator, GAIndividualGeneratorOptions>(config.getIndividualGenerators());
        config.getIndividualGenerators().clear();

        mockGenerator = mock(GgenIndividualGenerator.class);
        mockGeneratorOptions = new GAIndividualGeneratorOptions();
        mockGeneratorOptions.setOnlyFirstPopulation(true);
        mockGeneratorOptions.setPercent(0);

        fixedGenerator = new FixedGenerator();
        final GAIndividualGeneratorOptions fixedGeneratorOptions = new GAIndividualGeneratorOptions();
        fixedGenerator.setPRNG(new Random());
        fixedGenerator.setDataFile(new File(pathToDataFile));
        fixedGenerator = spy(fixedGenerator);
        fixedGeneratorOptions.setOnlyFirstPopulation(true);
        fixedGeneratorOptions.setPercent(1);

        config.getIndividualGenerators().put(mockGenerator, mockGeneratorOptions);
        config.getIndividualGenerators().put(fixedGenerator, fixedGeneratorOptions);
        config.getIndividualGenerators().putAll(oldGenerators);
    }

}
