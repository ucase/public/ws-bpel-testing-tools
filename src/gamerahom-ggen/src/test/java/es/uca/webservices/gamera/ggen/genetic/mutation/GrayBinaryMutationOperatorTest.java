package es.uca.webservices.gamera.ggen.genetic.mutation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;

/**
 * Tests the GrayBinaryMutation operator class.
 * 
 * @author Álvaro Cortijo-García
 */
public class GrayBinaryMutationOperatorTest {

	private GrayBinaryMutationOperator mutation;
	
	@Before
	public void setUp() {
		mutation = new GrayBinaryMutationOperator();
		mutation.setPRNG(new Random(0));
	}

	@Test
	public void testDoMutationIntFirst() {
		final BigInteger oldValue = new BigInteger("10011100010000",2);		//10000
		final BigInteger minValue = new BigInteger("-11000011010100000",2);	//-100000
		final BigInteger maxValue = new BigInteger("11000011010100000",2);	//100000
		final TypeInt type = new TypeInt(minValue, maxValue);
		final BigInteger newValue = mutation.doMutationInt(1.0, type, oldValue);
		assertTrue("The mutation should have produced a new object with a different value", !oldValue.equals(newValue));
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}

	@Test
	public void testDoMutationIntSecond() {
		final BigInteger oldValue = new BigInteger("10000010000",2); //1040
		final BigInteger minValue = new BigInteger("1111111111",2); //1023
		final BigInteger maxValue = new BigInteger("101111111110",2); //3070
		final TypeInt type = new TypeInt(minValue, maxValue);
		final BigInteger newValue = mutation.doMutationInt(1.0, type, oldValue);
		assertTrue("The mutation should have produced a new object with a different value", !oldValue.equals(newValue));
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}
	
	@Test
	public void testZeroBinaryToGray() {
		final String binaryString = new String("0");
		assertDoCorrectBinaryToGray(binaryString, binaryString);
	}

	@Test
	public void testOneBinaryToGray() {
		final String binaryString = new String("1");
		assertDoCorrectBinaryToGray(binaryString, binaryString);
	}

	@Test
	public void testTenThousandBinaryToGray() {
		final String binaryString = new String("10011100010000"); //10000
		final String expectedGrayString = new String("11010010011000"); //13464
		assertDoCorrectBinaryToGray(binaryString, expectedGrayString);
	}

	@Test
	public void testOneHundredThousandBinaryToGray() {
		final String binaryString = new String("11000011010100000"); //100000
		final String expectedGrayString = new String("10100010111110000"); //83440
		assertDoCorrectBinaryToGray(binaryString, expectedGrayString);
	}

	@Test
	public void testZeroGrayToBinary() {
		final String grayString = new String("0");
		assertDoCorrectGrayToBinary(grayString, grayString);
	}

	@Test
	public void testOneGrayToBinary() {
		final String grayString = new String("1");
		assertDoCorrectGrayToBinary(grayString, grayString);
	}

	@Test
	public void testTenThousandGrayToBinary() {
		final String grayString = new String("10011100010000"); //10000
		final String expectedBinaryString = new String("11101000011111"); //14879
		assertDoCorrectGrayToBinary(grayString, expectedBinaryString);
	}

	@Test
	public void testOneHundredThousandGrayToBinary() {
		final String grayString = new String("11000011010100000"); //100000
		final String expectedBinaryString = new String("10000010011000000"); //66752
		assertDoCorrectGrayToBinary(grayString, expectedBinaryString);
	}

	@Test
	public void testTwoConversions() {
		final String binaryString = new String("10011100010000"); //10000
		final BigInteger oldValue = new BigInteger(binaryString,2);
		final BigInteger grayValue = mutation.binaryToGray(oldValue);
		final BigInteger newValue = mutation.grayToBinary(grayValue);
		assertEquals("A number converted to gray and then converted to binary should be the same.", binaryString, newValue.toString(2));
	}
	
	@Test
	public void testDoMutationMaxInt() {
		final BigInteger oldValue = new BigInteger("10000000",2); //128
		final BigInteger minValue = new BigInteger("0",2);
		final BigInteger maxValue = new BigInteger("10000000",2); //128
		final TypeInt type = new TypeInt(minValue, maxValue);
		final BigInteger newValue = mutation.doMutationInt(1.0, type, oldValue);
		System.out.println("testDoMutationMaxInt -> oldValue = " + oldValue + ", mutatedValue = " + newValue);
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}

	@Test
	public void testDoMutationMinInt() {
		final BigInteger oldValue = new BigInteger("1111111",2); //127
		final BigInteger minValue = new BigInteger("1111111",2); //127
		final BigInteger maxValue = new BigInteger("100000000",2); //256
		final TypeInt type = new TypeInt(minValue, maxValue);
		final BigInteger newValue = mutation.doMutationInt(1.0, type, oldValue);
		System.out.println("testDoMutationMinInt -> oldValue = " + oldValue + ", mutatedValue = " + newValue);
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}

	@Test
	public void testDoMutationFloatFirst() {
		final BigDecimal oldValue = new BigDecimal(new BigInteger("10000"),4);
		final BigDecimal minValue = new BigDecimal(new BigInteger("-100000"),5);
		final BigDecimal maxValue = new BigDecimal(new BigInteger("100000"),5);
		final TypeFloat type = new TypeFloat(minValue, maxValue);
		final BigDecimal newValue = mutation.doMutationFloat(1.0, type, oldValue);
		assertTrue("The mutation should have produced a new object", !oldValue.equals(newValue));
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}
	
	@Test
	public void testDoMutationFloatSecond() {
		final BigDecimal oldValue = new BigDecimal(new BigInteger("104"),2);
		final BigDecimal minValue = new BigDecimal(new BigInteger("1023"),3);
		final BigDecimal maxValue = new BigDecimal(new BigInteger("307"),2);
		final TypeFloat type = new TypeFloat(minValue, maxValue);
		final BigDecimal newValue = mutation.doMutationFloat(1.0, type, oldValue);
		assertTrue("The mutation should have produced a new object", !oldValue.equals(newValue));
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}

	@Test
	public void testDoMutationSpecificBit() {
		final BigInteger oldValue = new BigInteger("1040");
		final BigInteger minValue = new BigInteger("1023");
		final BigInteger maxValue = new BigInteger("3070");
		final TypeInt type = new TypeInt(minValue, maxValue);
		
		final Random r = mock(Random.class);
		when(r.nextInt(11)).thenReturn(5);
		mutation.setPRNG(r);
		
		final BigInteger newValue = mutation.doMutationInt(1.0, type, oldValue);

		verify(r, times(1)).nextInt(11);
		
		assertTrue("The mutation should have produced a new object", !oldValue.equals(newValue));
		assertEquals(new BigInteger("1069"), newValue);
		assertTrue("The mutated value should respect minimum value constraints", newValue.compareTo(minValue) >= 0);
		assertTrue("The mutated value should respect maximum value constraints", newValue.compareTo(maxValue) <= 0);
	}	
	
	private void assertDoCorrectBinaryToGray(final String binaryString, final String expectedGrayString){
		final BigInteger binaryValue = new BigInteger(binaryString,2);
		final BigInteger grayValue = mutation.binaryToGray(binaryValue);
		assertEquals("The conversion to Gray should be correct", expectedGrayString, grayValue.toString(2));		
	}

	private void assertDoCorrectGrayToBinary(final String grayString, final String expectedBinaryString){
		final BigInteger grayValue = new BigInteger(grayString,2);
		final BigInteger binaryValue = mutation.grayToBinary(grayValue);
		assertEquals("The conversion to Binary should be correct", expectedBinaryString, binaryValue.toString(2));		
	}
	
}
