package es.uca.webservices.gamera.ggen.cli;

import static es.uca.webservices.gamera.ggen.cli.Launcher.INITIAL_OPTION;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;

import joptsimple.OptionException;

import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;

public class LauncherTest {

    public static final String DATA_VM = "src/test/resources/LoanApprovalRPC_ODE/data.vm";
    public static final String CONFIG_YAML = "src/test/resources/confTest.yaml";
    
    public LauncherTest(){
    	System.setProperty("install.dir", new File("target/dependency").getAbsolutePath());
    }

    @Test
    public void OptionHelp() throws Exception {
		Launcher launcher = new Launcher();
        String[] args = {"--help"};
        launcher.run(args, System.out);
    }
	
	@Test(expected = OptionException.class)
    public void OptionNoExist() throws Exception {
		Launcher launcher = new Launcher();
        String[] args = {"--noExist"};
        launcher.run(args, System.out);
    }
	
	@Test(expected = IllegalArgumentException.class)
	public void incorrectNumberArgs() throws Exception {
		Launcher launcher = new Launcher();
		String[] args = {"src/test/resources/conf.yaml", "error"};
		launcher.run(args, System.out);
	}
	
	@Test(expected = FileNotFoundException.class)
	public void fileNotFound() throws Exception {
		Launcher launcher = new Launcher();
		String[] args = {"src/test/resources/noExist.yaml"};
		launcher.run(args, System.out);
	}
	
	@Test(expected = FileNotFoundException.class)
	public void emptyFile() throws Exception {
		Launcher launcher = new Launcher();
		String[] args = {"src/test/resources/emptyFile.yaml"};
		launcher.run(args, System.out);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void invalidExtension() throws Exception {
		Launcher launcher = new Launcher();
		String[] args = {"src/test/resources/conf.txt"};
		launcher.run(args, System.out);
	}

	@Test
	public void normalExecution() throws Exception {
        final File originalData = new File(DATA_VM);
        final File backup = File.createTempFile("backup", ".vm");
        FileUtils.copyFile(originalData, backup);

        try {
            Launcher launcher = new Launcher();
            String[] args = {CONFIG_YAML};
            launcher.run(args, System.out);
        } finally {
            FileUtils.copyFile(backup, originalData);
            backup.delete();
        }
    }

    @Ignore // too slow
    @Test
    public void twiceWithSameSeedProducesSameResults() throws Exception {
        final File dataFile = new File(DATA_VM);
        final File backup = File.createTempFile("backup", ".vm");
        FileUtils.copyFile(dataFile, backup);

        try {
            Launcher launcher = new Launcher();
            launcher.run(new String[]{CONFIG_YAML}, System.out);

            assertTrue(FileUtils.checksumCRC32(dataFile) != FileUtils.checksumCRC32(backup));
            final File backupFirstOutput = File.createTempFile("backupFO", ".vm");
            FileUtils.copyFile(dataFile, backupFirstOutput);

            try {
                FileUtils.copyFile(backup, dataFile);
                launcher.run(new String[]{CONFIG_YAML}, System.out);
                assertEquals(FileUtils.checksumCRC32(dataFile), FileUtils.checksumCRC32(backupFirstOutput));
            } finally {
                backupFirstOutput.delete();
            }
        } finally {
            FileUtils.copyFile(backup, dataFile);
            backup.delete();
        }
    }

    @Test
    public void fixedInitialPopulation() throws Exception {
        final File dataFile = new File(DATA_VM);
        final File backup = File.createTempFile("backup", ".vm");
        FileUtils.copyFile(dataFile, backup);

        try {
            Launcher launcher = new Launcher();
            launcher.run(new String[]{"--" + INITIAL_OPTION, DATA_VM, CONFIG_YAML}, System.out);
        } finally {
            FileUtils.copyFile(backup, dataFile);
            backup.delete();
        }
    }
}
