package es.uca.webservices.gamera.ggen;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.GAProgressMonitor;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.exec.PreparationException;

/**
 * Mock executor used to speed up slow test cases.
 *
 * @author Antonio García-Domínguez
 */
class MockExecutor implements GAExecutor {
    private final AnalysisResults analysis;
    private final Map<GAIndividual, ComparisonResults> results;
    private int firstTest, lastTest;

    public MockExecutor(AnalysisResults analysis, Map<GAIndividual, ComparisonResults> results) {
        this.analysis = analysis;
        this.results = results;

        final ComparisonResult[] firstTestCase = results.values().iterator().next().getRow();
		this.firstTest = 0;
		this.lastTest = firstTestCase.length;
    }

    @Override
    public void prepare() throws PreparationException {
        // do nothing
    }

    @Override
    public void cleanup() throws PreparationException {
        // do nothing
    }

    @Override
    public AnalysisResults analyze(GAProgressMonitor monitor) throws AnalysisException {
        return analysis;
    }

    @Override
    public ComparisonResults[] compare(GAProgressMonitor monitor, GAIndividual... individuals) throws ComparisonException {
        final ComparisonResults[] mocked = new ComparisonResults[individuals.length];
        int count = 0;
        for (GAIndividual ind : individuals) {
            mocked[count++] = new ComparisonResults(ind, Arrays.copyOfRange(results.get(ind).getRow(), firstTest, lastTest));
        }
        return mocked;
    }

    @Override
    public void loadDataFile(File dataFile) throws UnsupportedOperationException {
        // do nothing
    }

    @Override
    public File getOriginalProgramFile() throws UnsupportedOperationException {
        return null;
    }

    @Override
    public File getMutantFile(GAIndividual individual) throws UnsupportedOperationException, GenerationException {
        return null;
    }

    @Override
    public List<String> getTestCaseNames() throws AnalysisException {
        return null;
    }

    @Override
    public void validate() throws InvalidConfigurationException {
        // do nothing
    }

	public void setTestRange(int firstTest, int lastTest) {
		this.firstTest = firstTest;
		this.lastTest = lastTest;
	}

	@Override
	public List<String> getMutationOperatorNames() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}
	
}