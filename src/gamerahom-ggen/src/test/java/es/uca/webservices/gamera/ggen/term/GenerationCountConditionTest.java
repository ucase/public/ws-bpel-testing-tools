package es.uca.webservices.gamera.ggen.term;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import org.junit.Test;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * Tests the count generation termination condition 
 * @author alvaro
 *
 */
public class GenerationCountConditionTest {
	
	@Test(expected=InvalidConfigurationException.class)
	public void negativeCount() throws InvalidConfigurationException{
		GenerationCountCondition term = new GenerationCountCondition();
		term.setCount(-1);
		term.validate();
	}
	
	public void zeroCount() throws InvalidConfigurationException{
		GenerationCountCondition term = new GenerationCountCondition();
		term.setCount(0);
		term.validate();
	}
	
	@Test
	public void positiveCount() throws InvalidConfigurationException{
		GenerationCountCondition term = new GenerationCountCondition();
		term.setCount(15);
		term.validate();
	}
	
	@Test
	public void trueWhenEqual(){
		final int currentGeneration = 15;
		GgenState state = new GgenState();
        addEmptyGenerations(state, currentGeneration);

        GenerationCountCondition term = new GenerationCountCondition();
		term.setCount(currentGeneration);
		assertTrue(term.evaluate(state));
	}

    @Test
	public void trueWhenGreater(){
		final int currentGeneration = 15;
		GgenState state = new GgenState();
        addEmptyGenerations(state, currentGeneration + 1);

        GenerationCountCondition term = new GenerationCountCondition();
		term.setCount(currentGeneration);
		assertTrue(term.evaluate(state));
	}

	@Test
	public void FalseWhenLower(){
		final int currentGeneration = 15;
		GgenState state = new GgenState();
        addEmptyGenerations(state, currentGeneration - 1);

        GenerationCountCondition term = new GenerationCountCondition();
		term.setCount(currentGeneration);
		assertFalse(term.evaluate(state));
	}

    private void addEmptyGenerations(GgenState state, int nGenerations) {
        for (int i = 0; i < nGenerations; ++i) {
            state.getHistory().add(new GgenPopulation(), new ComparisonResults[0]);
        }
    }

}
