package es.uca.webservices.gamera.ggen.conf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map.Entry;

import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.ggen.DefaultConfiguration;
import es.uca.webservices.gamera.ggen.api.generate.GAIndividualGeneratorOptions;
import es.uca.webservices.gamera.ggen.api.generate.GgenIndividualGenerator;
import es.uca.webservices.gamera.ggen.api.select.GASelectionOperator;
import es.uca.webservices.gamera.ggen.api.select.GASelectionOperatorOptions;

/**
 * @autor Álvaro Galán Piñero
 * @version 1.0
 */

public class ConfigurationTest {
	
	//We test different values for population size, zero or negative
	@Test(expected = InvalidConfigurationException.class)
	public void invalidConfigurationNegativePopulation () throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setPopulationSize(-1);
		testConfiguration.validate();
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void invalidConfigurationZeroPopulation () throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setPopulationSize(0);
		testConfiguration.validate();
	}
	
	//We test different possibilities about genetic operators
	@Test(expected = InvalidConfigurationException.class)
	public void nullMutationOperators() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setMutationOperators(null);
		testConfiguration.validate();
	}

	@Test(expected = InvalidConfigurationException.class)
	public void nullCrossoverOperators() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setCrossoverOperators(null);
		testConfiguration.validate();
	}

	@Test(expected = InvalidConfigurationException.class)
	public void emptyMutationOperators() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.getMutationOperators().clear();
		testConfiguration.validate();
	}

	@Test(expected = InvalidConfigurationException.class)
	public void emptyCrossoverOperators() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.getCrossoverOperators().clear();
		testConfiguration.validate();
	}

	//We test different possibilities about individual generators
	@Test(expected = InvalidConfigurationException.class)
	public void nullIndividualGen () throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setIndividualGenerators(null);
		testConfiguration.validate();
	}
		
	@Test(expected = InvalidConfigurationException.class)
	public void emptyIndividualGen () throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.getIndividualGenerators().clear();
		testConfiguration.validate();
	}
	
	//We test different possibilities about selection operators
	@Test(expected = InvalidConfigurationException.class)
	public void nullSelecOpe () throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setSelectionOperators(null);
		testConfiguration.validate();
	}
		
	@Test(expected = InvalidConfigurationException.class)
	public void emptySelecOpe () throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.getSelectionOperators().clear();
		testConfiguration.validate();
	}
	
	//We test different possibilities about termination conditions
	@Test(expected = InvalidConfigurationException.class)
	public void nullTermCond() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setTerminationConditions(null);
		testConfiguration.validate();
	}
			
	@Test(expected = InvalidConfigurationException.class)
	public void emptyTermCond () throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.getTerminationConditions().clear();
		testConfiguration.validate();
	}
	
	//We test different possibilities about loggers
	@Test(expected = InvalidConfigurationException.class)
	public void nullLoggers() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setLoggers(null);
		testConfiguration.validate();
	}
				
	@Test(expected = InvalidConfigurationException.class)
	public void emptyLoggers () throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.getLoggers().clear();
		testConfiguration.validate();
	}
	
	//We test different possibilities about parser
	@Test(expected = InvalidConfigurationException.class)
	public void nullParser() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setParser(null);
		testConfiguration.validate();
	}
	
	//We test different possibilities about parser
	@Test(expected = InvalidConfigurationException.class)
	public void nullFormatter() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setFormatter(null);
		testConfiguration.validate();
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void nullExecutor() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setExecutor(null);
		testConfiguration.validate();
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void nullGenerator() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		testConfiguration.setStrategy(null);
		testConfiguration.validate();
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void percentSelectionInvalid() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		for (Entry<GASelectionOperator,GASelectionOperatorOptions> sel : testConfiguration.getSelectionOperators().entrySet()){
			sel.getValue().setPercent(1.2);
		}
		testConfiguration.validate();
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void percentMutationInvalid() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		for (GAGeneticOperatorOptions opts : testConfiguration.getMutationOperators().values()) {
			opts.setProbability(-0.5);
		}
		testConfiguration.validate();
	}

	@Test(expected = InvalidConfigurationException.class)
	public void percentCrossoverInvalid() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		for (GAGeneticOperatorOptions opts : testConfiguration.getCrossoverOperators().values()) {
			opts.setProbability(-0.5);
		}
		testConfiguration.validate();
	}

	@Test(expected = InvalidConfigurationException.class)
	public void percentGeneratorInvalid() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		for (Entry<GgenIndividualGenerator,GAIndividualGeneratorOptions> indGen : testConfiguration.getIndividualGenerators().entrySet()){
			indGen.getValue().setPercent(1.2);
		}
		testConfiguration.validate();
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void sumPercentSelectionInvalid() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		for (GASelectionOperatorOptions opts : testConfiguration.getSelectionOperators().values()) {
			opts.setPercent(1.3);
		}
		testConfiguration.validate();
	}

	@Test(expected=InvalidConfigurationException.class)
	public void incorrectIndividuals() throws Exception {
		Configuration testConfiguration = defaultConfiguration();
		List<List<Integer>> individuals = defaultConfiguration().getIndividuals();
		individuals.get(0).add(3);
		testConfiguration.setIndividuals(individuals);
		testConfiguration.validate();
	}
	
	// The default configuration loads and validates properly
	@Test
	public void invalidConfiguration () throws Exception {
		defaultConfiguration().validate();
	}
	
	// We load a missing file
	@Test(expected = FileNotFoundException.class)
	public void FileNotFound() throws Exception {
		ConfigurationLoader yaml = new ConfigurationLoader();
		File notFound = new File("not-file.yaml");
		yaml.parse(new FileInputStream(notFound));
	}
	
	//We load the default configuration
	private Configuration defaultConfiguration() throws FileNotFoundException, ClassNotFoundException, InvalidConfigurationException {
		DefaultConfiguration defaultConf = new DefaultConfiguration();
		return defaultConf.getConf();
	}
}
