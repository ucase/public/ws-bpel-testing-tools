package es.uca.webservices.gamera.ggen.select;

import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.util.Random;

import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.ggen.DefaultConfiguration;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.parsers.ParserException;

/**
 * The selection produced randomly works good
 * @author alvaro
 *
 */
public class UniformRandomSelectionTest {
	
	@Test(expected = InvalidConfigurationException.class)
	public void nullPrng() throws Exception {
		UniformRandomSelection randomSelection = new UniformRandomSelection();
		randomSelection.setPRNG(null);
		randomSelection.validate();
	}
	
	@Test
	public void notNullSelection() throws FileNotFoundException, ClassNotFoundException, 
	InvalidConfigurationException, ParserException, GenerationException, ComparisonException {
		DefaultConfiguration defaultConf = new DefaultConfiguration();
		final Random prng = defaultConf.getConf().getPrng();
		defaultConf.setFirstGenerationTest();
		GgenPopulation father = defaultConf.getPopulation();
		UniformRandomSelection randomSelection = new UniformRandomSelection();
		randomSelection.setPRNG(prng);
		GgenIndividual individual = new GgenIndividual();
		individual = randomSelection.select(father);
		assertNotNull(individual);
	}

}
