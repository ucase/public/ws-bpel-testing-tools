package es.uca.webservices.gamera.ggen;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.IFitness;
import es.uca.webservices.gamera.ggen.individuals.Component;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;
import es.uca.webservices.gamera.ggen.log.NullLogger;
import es.uca.webservices.testgen.api.formatters.IFormatter;
import es.uca.webservices.testgen.api.types.TypeInt;

/**
 * Tests for the {@link Fitness} class.
 * @author Antonio Garcia-Dominguez
 */
public class FitnessTest {

    private MockExecutor executor;
    private GgenPopulation allIndividuals;

    @Before
    public void setUp() throws Exception {
        executor = createMockExecutor();
        allIndividuals = createMockPopulation();
    }
    
    @SuppressWarnings("unchecked")
	@Test
    public void validIndividuals() throws Exception {
    	IFitness f = new Fitness();
    	f.prepare(executor.analyze(null),
    			Arrays.asList(
    				Arrays.asList(1, 1, 1),
    				Arrays.asList(1, 3, 1)
    			), new NullLogger());
    }
    
    @SuppressWarnings("unchecked")
	@Test(expected=InvalidConfigurationException.class)
    public void operatorTooBig() throws Exception {
    	IFitness f = new Fitness();
    	f.prepare(executor.analyze(null),
    			Arrays.asList(
    				Arrays.asList(2, 1, 1),
    				Arrays.asList(1, 3, 1)
    			), new NullLogger());
    }
    
    @SuppressWarnings("unchecked")
	@Test(expected=InvalidConfigurationException.class)
    public void operatorTooSmall() throws Exception {
    	IFitness f = new Fitness();
    	f.prepare(executor.analyze(null),
    			Arrays.asList(
    				Arrays.asList(0, 1, 1),
    				Arrays.asList(1, 3, 1)
    			), new NullLogger());
    }

    @SuppressWarnings("unchecked")
	@Test(expected=InvalidConfigurationException.class)
    public void locationTooBig() throws Exception {
    	IFitness f = new Fitness();
    	f.prepare(executor.analyze(null),
    			Arrays.asList(
    				Arrays.asList(1, 1, 1),
    				Arrays.asList(1, 11, 1)
    			), new NullLogger());
    }

    @SuppressWarnings("unchecked")
	@Test(expected=InvalidConfigurationException.class)
    public void locationTooSmall() throws Exception {
    	IFitness f = new Fitness();
    	f.prepare(executor.analyze(null),
    			Arrays.asList(
    				Arrays.asList(1, 0, 1),
    				Arrays.asList(1, 3, 1)
    			), new NullLogger());
    }

    @SuppressWarnings("unchecked")
	@Test(expected=InvalidConfigurationException.class)
    public void attributeTooBig() throws Exception {
    	IFitness f = new Fitness();
    	f.prepare(executor.analyze(null),
    			Arrays.asList(
    				Arrays.asList(1, 1, 5),
    				Arrays.asList(1, 3, 1)
    			), new NullLogger());
    }

    @SuppressWarnings("unchecked")
	@Test(expected=InvalidConfigurationException.class)
    public void attributeTooSmall() throws Exception {
    	IFitness f = new Fitness();
    	f.prepare(executor.analyze(null),
    			Arrays.asList(
    				Arrays.asList(1, 1, 1),
    				Arrays.asList(1, 3, 0)
    			), new NullLogger());
    }
    
    @Test
    public void comparisonRemovesInvalid() throws Exception {
    	IFitness f = new Fitness();
    	f.prepare(executor.analyze(null), null, new NullLogger());
    	final ComparisonResults[] cmp = f.computeFitness(executor, allIndividuals, mock(IFormatter.class), new GgenState(), new NullLogger());

    	assertEquals("Two invalid individuals should have been removed from the comparison results", 8, cmp.length);
    	for (ComparisonResults c : cmp) {
    		final GAIndividual ind = c.getIndividual();
			assertTrue("Only the two invalid individuals should have been removed from the comparison results", ind.getThLocation(1).intValue() != 7);
    		assertTrue("Only the two invalid individuals should have been removed from the comparison results", ind.getThLocation(1).intValue() != 9);
    	}

    	assertEquals("Two invalid individuals should have been removed from the fitness function", 8, f.getIndividuals().size());
    	for (GAIndividual ind : f.getIndividuals()) {
    		assertTrue("Only the two invalid individuals should have been removed", ind.getThLocation(1).intValue() != 7);
    		assertTrue("Only the two invalid individuals should have been removed", ind.getThLocation(1).intValue() != 9);
    	}
    }
    
    @Test
    public void comparisonReusesAllResults() throws Exception {
    	Fitness f = new Fitness();
    	f.prepare(executor.analyze(null), null, new NullLogger());
    	final GgenState state = new GgenState();
		final IFormatter formatter = mock(IFormatter.class);
		final ComparisonResults[] cmp1 = f.computeFitness(executor, allIndividuals, formatter, state, new NullLogger());
    	state.getHistory().add(allIndividuals, cmp1);
    	final ComparisonResults[] cmp2 = f.computeFitness(executor, allIndividuals, formatter, state, new NullLogger());
    	
    	assertArrayEquals(cmp1, cmp2);
    	verify(formatter, times(1)).suiteStart();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
    public void comparisonReusesMostResultsExceptFirst() throws Exception {
    	Fitness f = new Fitness();
    	f.prepare(executor.analyze(null), new ArrayList(), new NullLogger());
    	final GgenState state = new GgenState();
		final IFormatter formatter = mock(IFormatter.class);

		// First evaluation doesn't use the first individual. The first
		// individual will be run on the second invocation, and the rest
		// of the comparison results will be reused.
		final GgenPopulation pop1 = new GgenPopulation(allIndividuals.getPopulation());
		pop1.getPopulation().remove(0);
		executor.setTestRange(1, allIndividuals.getPopulationSize());
		final ComparisonResults[] cmp1 = f.computeFitness(executor, pop1, formatter, state, new NullLogger());
		state.getHistory().add(pop1, cmp1);

		executor.setTestRange(0, 1);
		final ComparisonResults[] cmp2 = f.computeFitness(executor, allIndividuals, formatter, state, new NullLogger());

		// The reused results should be the same
		for (int iProgram = 0; iProgram < cmp1.length; ++iProgram) {
			final ComparisonResult[] row1 = cmp1[iProgram].getRow();
			final ComparisonResult[] row2 = cmp2[iProgram].getRow();
			for (int iTest = 0; iTest < pop1.getPopulationSize(); ++iTest) {
				assertEquals(row1[iTest], row2[iTest + 1]);
			}
		}

		// We should have only run each test case exactly once
    	verify(formatter, times(allIndividuals.getPopulationSize())).testStart();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
    public void comparisonReusesMostResultsExceptLast() throws Exception {
    	Fitness f = new Fitness();
    	f.prepare(executor.analyze(null), new ArrayList(), new NullLogger());
    	final GgenState state = new GgenState();
		final IFormatter formatter = mock(IFormatter.class);

		// First evaluation doesn't use the first individual. The last
		// individual will be run on the second invocation, and the rest
		// of the comparison results will be reused.
		final GgenPopulation pop1 = new GgenPopulation(allIndividuals.getPopulation());
		pop1.getPopulation().remove(pop1.getPopulationSize() - 1);
		executor.setTestRange(0, pop1.getPopulationSize());
		final ComparisonResults[] cmp1 = f.computeFitness(executor, pop1, formatter, state, new NullLogger());
		state.getHistory().add(pop1, cmp1);

		executor.setTestRange(allIndividuals.getPopulationSize() - 1, allIndividuals.getPopulationSize());
		final ComparisonResults[] cmp2 = f.computeFitness(executor, allIndividuals, formatter, state, new NullLogger());

		// The reused results should be the same
		for (int iProgram = 0; iProgram < cmp1.length; ++iProgram) {
			final ComparisonResult[] row1 = cmp1[iProgram].getRow();
			final ComparisonResult[] row2 = cmp2[iProgram].getRow();
			for (int iTest = 0; iTest < pop1.getPopulationSize(); ++iTest) {
				assertEquals(row1[iTest], row2[iTest]);
			}
		}

		// We should have only run each test case exactly once
    	verify(formatter, times(allIndividuals.getPopulationSize())).testStart();
    }
    
    private GgenPopulation createMockPopulation() {
        // Mocked test cases
        final TypeInt tInt = new TypeInt(1, 6);
        final GgenPopulation ggenPop = new GgenPopulation();
        ggenPop.addIndividual(new GgenIndividual(new Component(tInt, 1)));
        ggenPop.addIndividual(new GgenIndividual(new Component(tInt, 2)));
        ggenPop.addIndividual(new GgenIndividual(new Component(tInt, 3)));
        ggenPop.addIndividual(new GgenIndividual(new Component(tInt, 4)));
        return ggenPop;
    }

    private MockExecutor createMockExecutor() {
        // Mocked analysis: 1 operator with 3 locations
        final AnalysisResults analysis = new AnalysisResults(
                new String[]{"a"},
                new BigInteger[]{BigInteger.TEN},
                new BigInteger[]{BigInteger.ONE}
        );

        // Mocked comparison results for the 3 locations
        final ComparisonResults[] vResults = new ComparisonResults[] {
                new ComparisonResults(1,  1, 1, true, 1, 0, 0, 0),
                new ComparisonResults(1,  2, 1, true, 1, 1, 0, 0),
                new ComparisonResults(1,  3, 1, true, 0, 0, 1, 0),
                new ComparisonResults(1,  4, 1, true, 1, 0, 0, 1),
                new ComparisonResults(1,  5, 1, true, 1, 1, 0, 0),
                new ComparisonResults(1,  6, 1, true, 0, 0, 1, 1),
                new ComparisonResults(1,  7, 1, true, 2, 2, 2, 2),
                new ComparisonResults(1,  8, 1, true, 1, 1, 0, 0),
                new ComparisonResults(1,  9, 1, true, 2, 2, 2, 2),
                new ComparisonResults(1, 10, 1, true, 0, 0, 1, 0),
        };
        final Map<GAIndividual, ComparisonResults> results = new HashMap<GAIndividual, ComparisonResults>();
        for (ComparisonResults r : vResults) {
            results.put(r.getIndividual(), r);
        }

        return new MockExecutor(analysis, results);
    }
}
