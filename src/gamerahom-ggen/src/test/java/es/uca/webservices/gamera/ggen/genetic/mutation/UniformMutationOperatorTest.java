package es.uca.webservices.gamera.ggen.genetic.mutation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;

/**
 * Tests for the {@link UniformMutationOperator} class.
 *
 * TODO: use Chi-squared test instead of just generating 50 numbers.
 *
 * @author Antonio García Domínguez
 */
public class UniformMutationOperatorTest {

	private static final TypeInt TYPE_INT_VARIABLE = new TypeInt(1, 10);
	private static final TypeInt TYPE_INT_CONSTANT = new TypeInt(5, 5);
	private static final TypeInt TYPE_INT_ALLOWEDVALS = new TypeInt(
		Arrays.asList(BigInteger.ONE, new BigInteger("3"), new BigInteger("5"))
	);

	private static final TypeFloat TYPE_FLOAT_VARIABLE = new TypeFloat("21", "34.5");
	private static final TypeFloat TYPE_FLOAT_CONSTANT = new TypeFloat(6, 6);
	private static final TypeFloat TYPE_FLOAT_ALLOWEDVALS = new TypeFloat(
		Arrays.asList(new BigDecimal("3.14"), new BigDecimal("2.718"), new BigDecimal("1.618"))
	);

	private UniformMutationOperator op;

	@Before
	public void setUp() {
		op = new UniformMutationOperator();
		op.setPRNG(new Random());
	}

	@Test
	public void allowedValuesInt() {
		for (int i = 0; i < 50; i++) {
			final BigInteger val = op.doMutationInt(0, TYPE_INT_ALLOWEDVALS, 1);
			assertTrue(TYPE_INT_ALLOWEDVALS.getAllowedValues().contains(val));
		}
	}

	@Test
	public void allowedValuesFloat() {
		for (int i = 0; i < 50; i++) {
			final BigDecimal val = op.doMutationFloat(0, TYPE_FLOAT_ALLOWEDVALS, 1);
			assertTrue(TYPE_FLOAT_ALLOWEDVALS.getAllowedValues().contains(val));
		}
	}

	@Test
	public void constantInt() {
		for (int i = 0; i < 50; i++) {
			final BigInteger val = op.doMutationInt(0, TYPE_INT_CONSTANT, 0);
			assertEquals(TYPE_INT_CONSTANT.getMinValue(), val);
		}
	}

	@Test
	public void constantFloat() {
		for (int i = 0; i < 50; i++) {
			final BigDecimal val = op.doMutationFloat(0, TYPE_FLOAT_CONSTANT, 0);
			assertTrue(TYPE_FLOAT_CONSTANT.getMinValue().compareTo(val) == 0);
		}
	}

	@Test
	public void variableInt() {
		for (int i = 0; i < 50; i++) {
			final BigInteger val = op.doMutationInt(0, TYPE_INT_VARIABLE, 0);
			//System.out.println("Generated int " + val);
			assertTrue(val.compareTo(TYPE_INT_VARIABLE.getMinValue()) >= 0);
			assertTrue(val.compareTo(TYPE_INT_VARIABLE.getMaxValue()) <= 0);
		}
	}

	@Test
	public void variableFloat() {
		for (int i = 0; i < 50; i++) {
			final BigDecimal val = op.doMutationFloat(0, TYPE_FLOAT_VARIABLE, 0);
			//System.out.println("Generated float " + val);
			assertTrue(val.compareTo(TYPE_FLOAT_VARIABLE.getMinValue()) >= 0);
			assertTrue(val.compareTo(TYPE_FLOAT_VARIABLE.getMaxValue()) <= 0);
		}
	}
}
