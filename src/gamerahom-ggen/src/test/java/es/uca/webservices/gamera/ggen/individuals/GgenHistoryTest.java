package es.uca.webservices.gamera.ggen.individuals;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.generators.IStrategy;
import es.uca.webservices.testgen.api.generators.IType;
import es.uca.webservices.testgen.api.types.TypeFloat;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeList;
import es.uca.webservices.testgen.api.types.TypeString;
import es.uca.webservices.testgen.strategies.random.UniformRandomStrategy;

/**
 * Tests for the hall of fame.
 *
 * @author Antonio Garcia Dominguez
 */
public class GgenHistoryTest {

    private final List<IType> types = new ArrayList<IType>(Arrays.asList(new TypeInt(), new TypeString(), new TypeList(new TypeFloat())));
    private final IStrategy strategy = new UniformRandomStrategy();
    private GgenHistory history;

    @Before
    public void setUp() {
        history = new GgenHistory();
    }

    @Test
    public void detectRepeatedIndividuals() throws GenerationException {
        final GgenIndividual indA = generateRandomIndividual(), indB = generateRandomIndividual();

        history.add(
                new GgenPopulation(indA), new ComparisonResults[] { new ComparisonResults(1, 1, 1, true, 1) }
        );
        assertEquals(1, history.currentGeneration());

        history.add(new GgenPopulation(indA, indB), new ComparisonResults[] {
                new ComparisonResults(1, 1, 1, true, 1, 0),
        });
        assertEquals(2, history.currentGeneration());

        final Set<GgenIndividual> setIndA = new HashSet<GgenIndividual>(Arrays.asList(indA));
        final Set<GgenIndividual> setBoth = new HashSet<GgenIndividual>(Arrays.asList(indA, indB));
        assertEquals(setBoth, history.allUniqueIndividualsByComponentValues().keySet());
        assertEquals(setIndA, new HashSet<GgenIndividual>(history.individualsFrom(1)));
        assertEquals(setBoth, new HashSet<GgenIndividual>(history.individualsFrom(2)));
        assertTrue("Generations which have not been run should report no individuals", history.individualsFrom(3).isEmpty());
    }

    private GgenIndividual generateRandomIndividual() throws GenerationException {
        final GgenIndividual ind = new GgenIndividual();
        ind.populate(strategy, types);
        return ind;
    }
}
