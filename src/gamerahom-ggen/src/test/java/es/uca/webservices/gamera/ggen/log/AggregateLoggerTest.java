package es.uca.webservices.gamera.ggen.log;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.ggen.api.log.GALogger;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.gamera.ggen.individuals.GgenState;

/**
 * The loggers are aggregated properly
 * @author alvaro
 */
public class AggregateLoggerTest {
	
	private static final String LOGGER = "logger-messages.txt";

    private GALogger sublogger1;
    private GALogger sublogger2;
    private AggregateLogger aggregateLogger;

    @Before
    public void setUp() {
        List<GALogger> loggers = new ArrayList<GALogger>();
        sublogger1 = mock(GALogger.class);
        sublogger2 = mock(GALogger.class);
        loggers.add(sublogger1);
        loggers.add(sublogger2);

        aggregateLogger = new AggregateLogger(loggers);
    }

    @Test
    public void testSeveralLogger() {
        MessageLogger messageLogger = new MessageLogger();
        messageLogger.setConsole(false);
        messageLogger.setFile(LOGGER);
        
        FullHistoryReportLogger hofLogger = new FullHistoryReportLogger();
        hofLogger.setFile(null);
        
        List<GALogger> loggers = new ArrayList<GALogger>();
        loggers.add(messageLogger);
        loggers.add(hofLogger);
        
        aggregateLogger = new AggregateLogger(loggers);
        
        // This instruction should print by console a message from
        // the MessageLogger and the message produced by the method started
        // of the FullHistoryReportLogger. In this case, FullHistoryReportLogger doesn't override this
        // method, so the file will be empty.
        
        aggregateLogger.started(new GgenState());

        assertTrue("Using setFile should make messages appear in the specified file", new File(LOGGER).exists());
    }
    
    
    @Test(expected = InvalidConfigurationException.class)
    public void AggregatedLoggerIsClear() throws Exception {
    	aggregateLogger = new AggregateLogger(null);
    	aggregateLogger.validate();
    }
    
    @Test
    public void startIsAggregated() {
        GgenState state = new GgenState();
        aggregateLogger.started(state);
        verify(sublogger1).started(state);
        verify(sublogger2).started(state);
    }

    @Test
    public void finishIsAggregated() throws IOException {
        GgenState state = new GgenState();
        aggregateLogger.finished(state);
        verify(sublogger1).finished(state);
        verify(sublogger2).finished(state);
    }

    @Test
    public void finishEvaluationIsAggregated() {
    	GgenState state = new GgenState();
    	GgenPopulation population = new GgenPopulation();
    	aggregateLogger.finishedEvaluation(state, population);
        verify(sublogger1).finishedEvaluation(state, population);
        verify(sublogger2).finishedEvaluation(state, population);
    }
    
    @Test
    public void startEvaluationIsAggregated() {
    	GgenState state = new GgenState();
    	GgenPopulation population = new GgenPopulation();
    	aggregateLogger.startedEvaluation(state, population);
        verify(sublogger1).startedEvaluation(state, population);
        verify(sublogger2).startedEvaluation(state, population);
    }
    
    @Test
    public void newGenerationIsAggregated() {
        GgenState state = new GgenState();
        GgenPopulation population = new GgenPopulation();
        aggregateLogger.newGeneration(state, population);
        verify(sublogger1).newGeneration(state, population);
        verify(sublogger2).newGeneration(state, population);
    }
    
    @Test
    public void startComparisonIsAggregated() {
    	GgenState state = new GgenState();
    	aggregateLogger.startedPopulationExecution(state);
        verify(sublogger1).startedPopulationExecution(state);
        verify(sublogger2).startedPopulationExecution(state);
    }
    
    @Test
    public void finishedComparisonIsAggregated() {
    	GgenState state = new GgenState();
    	aggregateLogger.finishedPopulationExecution(state);
        verify(sublogger1).finishedPopulationExecution(state);
        verify(sublogger2).finishedPopulationExecution(state);
    }
    
    @Test
    public void createdTypesIsAggregated() {
    	GgenState state = new GgenState();
    	aggregateLogger.createdVariables(state);
        verify(sublogger1).createdVariables(state);
        verify(sublogger2).createdVariables(state);
    }
}
