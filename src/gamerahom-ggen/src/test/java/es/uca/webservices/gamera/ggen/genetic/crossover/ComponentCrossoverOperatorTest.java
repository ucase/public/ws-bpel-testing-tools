package es.uca.webservices.gamera.ggen.genetic.crossover;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.ggen.DefaultConfiguration;
import es.uca.webservices.gamera.ggen.genetic.crossover.ComponentCrossoverOperator;
import es.uca.webservices.gamera.ggen.individuals.Component;
import es.uca.webservices.gamera.ggen.individuals.GgenIndividual;
import es.uca.webservices.gamera.ggen.individuals.GgenPopulation;
import es.uca.webservices.testgen.api.generators.GenerationException;
import es.uca.webservices.testgen.api.parsers.ParserException;
import es.uca.webservices.testgen.api.types.TypeInt;
import es.uca.webservices.testgen.api.types.TypeTuple;

/**
 * Tests for the {@link ComponentCrossoverOperator} class.
 *
 * @author Álvaro Cortijo-García, Álvaro Galán-Piñero, Antonio García-Domínguez
 */
public class ComponentCrossoverOperatorTest {
	
	private ComponentCrossoverOperator crossover;

	@Before
	public void setUp() {
		crossover = new ComponentCrossoverOperator();
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void nullPrng() throws Exception {
		crossover.setPRNG(null);
		crossover.validate();
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void negativeProbability() throws Exception {
		testProbability(-0.5);
	}
	
	@Test(expected = InvalidConfigurationException.class)
	public void largerThanOneProbability() throws Exception {
		testProbability(1.1);
	}
	
	@Test
	public void oneProbability() throws Exception {
		testProbability(1);
	}
	
	@Test(expected=InvalidConfigurationException.class)
	public void zeroProbability() throws Exception {
		testProbability(0);
	}

	@Test(expected=IllegalArgumentException.class)
	public void oneTupleComponentIsRejected() throws Exception {
		final TypeTuple threeIntTuple = new TypeTuple(new TypeInt(), new TypeInt(), new TypeInt());
		final Component cmpA = new Component(threeIntTuple, Arrays.asList(1, 2, 3));
		final Component cmpB = new Component(threeIntTuple, Arrays.asList(1, 2, 3));
		final GgenIndividual indA = new GgenIndividual(cmpA);
		final GgenIndividual indB = new GgenIndividual(cmpB);

		crossover.apply(new GAGeneticOperatorOptions(), indA, indB);
	}

	@Test(expected=IllegalArgumentException.class)
	public void noCrossOneComponent() throws Exception {
		final TypeInt type = new TypeInt();
		final Component cmpA = new Component(type, 1);
		final Component cmpB = new Component(type, 2);
		final GgenIndividual father = new GgenIndividual(cmpA);
		final GgenIndividual mother = new GgenIndividual(cmpB);

		crossover.apply(new GAGeneticOperatorOptions(), father, mother);
	}

	@Test(expected=IllegalArgumentException.class)
	public void noCrossDifferentSize() throws Exception {
		final TypeInt type = new TypeInt();
		List<Component> cmps = new ArrayList<Component>();
		for(int i = 0; i < 7; i++){
			final Component cmp = new Component(type, i);
			cmps.add(cmp);
		}
		final GgenIndividual father = new GgenIndividual(cmps.get(0), cmps.get(1), cmps.get(2), cmps.get(3));
		final GgenIndividual mother = new GgenIndividual(cmps.get(4),cmps.get(5), cmps.get(6));

		crossover.apply(new GAGeneticOperatorOptions(), father, mother);
	}
	
	@Test
	public void twoComponentsDoCross() throws Exception {
		final TypeInt type = new TypeInt();
		List<Component> cmps = new ArrayList<Component>();
		for(int i = 0; i < 4; i++){
			final Component cmp = new Component(type, i);
			cmps.add(cmp);
		}
		final GgenIndividual father = new GgenIndividual(cmps.get(0), cmps.get(1));
		final GgenIndividual mother = new GgenIndividual(cmps.get(2),cmps.get(3));
		final int position = 1;

		GgenIndividual left =  new GgenIndividual(new ArrayList<Component>(father.getComponents()));
		GgenIndividual right =  new GgenIndividual(new ArrayList<Component>(mother.getComponents()));

		crossover.doCrossover(left, right, position);

		assertCrossoverIsCorrect(position, father, mother, left, right);
	}

	@Test
	public void threeComponentsDoCross() throws Exception {
		final TypeInt type = new TypeInt();
		List<Component> cmps = new ArrayList<Component>();
		for(int i = 0; i < 6; i++){
			final Component cmp = new Component(type, i);
			cmps.add(cmp);
		}
		final GgenIndividual father = new GgenIndividual(cmps.get(0), cmps.get(1), cmps.get(2));
		final GgenIndividual mother = new GgenIndividual(cmps.get(3), cmps.get(4), cmps.get(5));
		final int position = 1;

		GgenIndividual left =  new GgenIndividual(new ArrayList<Component>(father.getComponents()));
		GgenIndividual right =  new GgenIndividual(new ArrayList<Component>(mother.getComponents()));

		crossover.doCrossover(left, right, position);

		assertCrossoverIsCorrect(position, father, mother, left, right);
	}

	@Test
	public void threeComponentsCorrectApplyCross() throws Exception {
		final TypeInt type = new TypeInt();
		List<Component> cmps = new ArrayList<Component>();
		for(int i = 0; i < 6; i++){
			final Component cmp = new Component(type, i);
			cmps.add(cmp);
		}
		final GgenIndividual father = new GgenIndividual(cmps.get(0), cmps.get(1), cmps.get(2));
		final GgenIndividual mother = new GgenIndividual(cmps.get(3), cmps.get(4), cmps.get(5));

		GgenIndividual left =  new GgenIndividual(new ArrayList<Component>(father.getComponents()));
		GgenIndividual right =  new GgenIndividual(new ArrayList<Component>(mother.getComponents()));
		
		final int position = 1;
		
		final Random r = mock(Random.class);
		when(r.nextInt(2)).thenReturn(position);
		crossover.setPRNG(r);
		
		crossover.apply(new GAGeneticOperatorOptions(), left, right);
		verify(r, times(1)).nextInt(2);

		assertCrossoverIsCorrect(position+1, father, mother, left, right);
	}
	
	@Test(expected=AssertionError.class)
	public void noCrossIllegalPoint() throws Exception {
		final TypeInt type = new TypeInt();
		List<Component> cmps = new ArrayList<Component>();
		for(int i = 0; i < 4; i++){
			final Component cmp = new Component(type, i);
			cmps.add(cmp);
		}
		final GgenIndividual father = new GgenIndividual(cmps.get(0), cmps.get(1));
		final GgenIndividual mother = new GgenIndividual(cmps.get(2),cmps.get(3));
		final int position = 2;

		GgenIndividual left =  new GgenIndividual(new ArrayList<Component>(father.getComponents()));
		GgenIndividual right =  new GgenIndividual(new ArrayList<Component>(mother.getComponents()));

		crossover.doCrossover(left, right, position);
	}

	private static void assertCrossoverIsCorrect(final int position,
			GgenIndividual father, GgenIndividual mother,
			GgenIndividual leftChild, GgenIndividual rightChild) {
		int cont = 0;
		while (cont < position) {
			assertEquals(father.getComponent(cont), leftChild.getComponent(cont));
			assertEquals(mother.getComponent(cont), rightChild.getComponent(cont));
			cont++;
		}
		while (cont < father.getNumberComponents()) {
			assertEquals(mother.getComponent(cont), leftChild.getComponent(cont));
			assertEquals(father.getComponent(cont), rightChild.getComponent(cont));
			cont++;
		}
	}

	private void testProbability(final double percent) throws InvalidConfigurationException {
		final GAGeneticOperatorOptions opts = new GAGeneticOperatorOptions();
		opts.setProbability(percent);
        opts.validate();
    }
}
