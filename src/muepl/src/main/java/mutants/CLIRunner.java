package mutants;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import subcommands.ApplyAllSubcommand;
import subcommands.CaptureSubcommand;
import subcommands.AnalyzeSubcommand;
import subcommands.AnalyzeXYLocationSubcommand;
import subcommands.ApplySubcommand;
import subcommands.CompareSubcommand;
import subcommands.RunAllSubcommand;
import subcommands.RunAllSequenceSubcommand;
import subcommands.RunSubcommand;
import subcommands.CountMutantsSubcommand;
import subcommands.ISubcommand;

/** 
 * Command line runner for the mutation tool.
 * 
 * @author Lorena Gutiérrez-Madroñal
 *
 */

public final class CLIRunner {

	private static final int EXITCODE_EXECERROR = 4;
	private static final int EXITCODE_PARSEERROR = 3;
	private static final int EXITCODE_UNKNOWNSUBCMD = 2;
	private static final int EXITCODE_NOSUBCMD = 1;
	private static final Logger LOGGER = LoggerFactory.getLogger(CLIRunner.class);
	private static Properties PROPERTIES;

	private CLIRunner() {}

	/**
	 * <p>
	 * Main method which selects the appropriate subcommand and runs it.
	 * </p>
	 * 
	 * <p>
	 * Errors and warnings are reported through the standard error output
	 * stream.
	 * </p>
	 * 
	 * @param args
	 *            Command-line arguments.
	 * @throws Exception
	 *             There was a problem while running the subcommand.
	 */
	public static void main(String[] args) {
		Map<String, ISubcommand> subcmdMap = getSubcommandMap();
		if (args.length < 1) {
			printUsage(subcmdMap);
			System.exit(EXITCODE_NOSUBCMD);
		}

		String subcmdName = args[0];
		if (!subcmdMap.containsKey(subcmdName)) {
			LOGGER.error("Unknown subcommand: " + subcmdName);
			printUsage(subcmdMap);
			System.exit(EXITCODE_UNKNOWNSUBCMD);
		}

		ISubcommand subcmd = subcmdMap.get(subcmdName);
		
		try {
			subcmd.parseArgs(
					Arrays.asList(args).subList(1, args.length)
						.toArray(new String[args.length-1]));
		} catch (IllegalArgumentException ex) {
			LOGGER.error("Error during argument parsing", ex);
			printUsage(subcmdMap);
			System.exit(EXITCODE_PARSEERROR);
		}

		try {
			subcmd.run();
		} catch (Exception e) {
			System.err.println("Error during command execution" + e.toString());
			//LOGGER.error("Error during command execution", e);
			System.exit(EXITCODE_EXECERROR);
		}
	}

	private static void printUsage(Map<String, ISubcommand> subcmdMap) {
		//System.err.println("muepl v" + getVersion() + "\n");
		System.err.println("Available subcommands:");
		for (ISubcommand subcmd : subcmdMap.values()) {
			System.err.println(" * " + subcmd.getName() + " " + subcmd.getUsage());
		}
		System.err.println("\nFor more help about a subcommand, use 'muepl subcmd -h'");
	}

	private static String getVersion() {
		if (PROPERTIES == null) {
			PROPERTIES = new Properties();
			try {
				PROPERTIES.load(CLIRunner.class.getResourceAsStream("/muepl.properties"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return PROPERTIES.getProperty("muepl.version");
	}

	private static Map<String, ISubcommand> getSubcommandMap() {
		final ISubcommand[] subcommands = new ISubcommand[] {
				new CaptureSubcommand(),
				new AnalyzeSubcommand(),
				new AnalyzeXYLocationSubcommand(),
				new ApplyAllSubcommand(),
				new ApplySubcommand(),
				new CountMutantsSubcommand(),
				new RunSubcommand(),
				new RunAllSubcommand(),
				new RunAllSequenceSubcommand(),
				new CompareSubcommand()
				};

		final Map<String, ISubcommand> commandMap = new LinkedHashMap<String, ISubcommand>();
		System.out.println("MuEPL starts...");
		for (ISubcommand subcmd : subcommands) {
			commandMap.put(subcmd.getName(), subcmd);
		}
		return commandMap;
	}

	
}
