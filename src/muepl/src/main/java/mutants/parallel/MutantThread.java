package mutants.parallel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.lang.ProcessBuilder;

import operators.OperatorConstants;

import subcommands.RunSubcommand;

public class MutantThread implements Runnable{
	long time_start, time_end = 0;
	String DirectoryName = "";
	String OutputEventFile = "";
	Map<String,String> environment = null;
	List<String> runcommand = null;
	CyclicBarrier cyclicbarrierThread = null;
	boolean originalprogram = false;
	static List<String> paths = new ArrayList<String>();
	static List<String> times = new ArrayList<String>();
	
	public MutantThread(String outputsFile, String PathToFolder, boolean original){
		OutputEventFile = outputsFile;
		DirectoryName = PathToFolder;
		originalprogram = original;
	}
	
	@Override
	public void run() {
		
		try {
			
			cyclicbarrierThread.await();
						
			Map <String,List<String>> OutputEvents = new HashMap<String, List<String>>();
			
			ProcessBuilder aux = new ProcessBuilder(runcommand).directory(new File(DirectoryName));
			aux.redirectErrorStream(true);

			time_start = System.currentTimeMillis();
			Process a = aux.start();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(a.getInputStream()));
			String readerEvent = null;
			
			// Volcamos en un Map <Key, List<String>> los eventos de salida según el número de la consulta
			while((readerEvent = reader.readLine()) != null) {
				int index = readerEvent.indexOf(" ");
				System.out.println(readerEvent);
				String key = readerEvent.substring(0, index);
				boolean number = true;
				
				try {
					int k = Integer.parseInt(key);
				} catch (NumberFormatException e) {
					number = false;
				}
				
				if (number) {
					String event = readerEvent.substring(index);
					
					if (OutputEvents.containsKey(key)) {
						List<String> currentValues = OutputEvents.get(key);
						currentValues.add(event);
						OutputEvents.put(key, currentValues);
					} else {
						List<String> emptyValues = new ArrayList<String>();
						emptyValues.add(event);
						OutputEvents.put(key, emptyValues);
					}
				}
			}
			
			
			a.waitFor();
			a.destroy();
			time_end = System.currentTimeMillis();
			long time = (time_end - time_start) * 1000000;
						
			// En el fichero OutputsFile que se pasa como parámetro en run, se escriben los paths de los ficheros
			// donde se van a volcar los eventos de salida, Key + OutputEventsFileName.events
			
			String value = GetName(DirectoryName);
			
			FileWriter OutputpathFile = new FileWriter(OutputEventFile, true);
			BufferedWriter bw = new BufferedWriter(OutputpathFile);
			
			// Se vuelca el contenido de cada clave, una lista de String, en el fichero cuyo nombre tiene la clave
			for (Map.Entry<String, List<String>> mapEntry : OutputEvents.entrySet()) {
				String OutputEventsFileName;
				OutputEventsFileName = RunSubcommand.dumpFileToStream(value, mapEntry.getKey());
				
				FileWriter EventsFile = new FileWriter(OutputEventsFileName, true);
				BufferedWriter bwe = new BufferedWriter(EventsFile);
				
				for (String singleEntry : mapEntry.getValue()) {
					bwe.write(singleEntry + "\n");	
				}
				
				bwe.close();
				EventsFile.close();
								
				synchronized (paths) {
					if (originalprogram == true){
						paths.add(0, OutputEventsFileName);
					} else {
						paths.add(OutputEventsFileName);
					}
				}
			}
			bw.close();
			OutputpathFile.close();	

			synchronized (times) {
				if (originalprogram == true){
					times.add(0, "/tmp/outputs/" + value + ".event" + " " + time + "");
				} else {
					times.add("/tmp/outputs/" + value + ".event" + " " + time + "");
				}
			}
			
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void SetEnvironment(Map<String, String> newEnvironment){
		environment = newEnvironment;
	}
	
	public void SetRunCommand(List<String> ePLRunCommand){
		runcommand = ePLRunCommand;
	}
	
	public static List<String> getpath(){
		return paths;
	}
	
	public static List<String> gettimes(){
		return times;
	}
	
	/**
	 * Take the name of the mutant directory from the path
	 * @param eplfile: the path which contains the name of the mutant directory
	 * @return mutantDir: contains the name of the mutant directory
	 */
	public static String GetName(String eplfile) {
		
		String[] parts = eplfile.split("/");
		String name = "";
		
		for (int j = 0; j < OperatorConstants.OPERATOR_NAMES.length; j++){
			for (int i = 0; i < parts.length; i++){
				if (parts[i].contains(OperatorConstants.OPERATOR_NAMES[j])){
					name = parts[i];
				}
			}
		}
		
		if (name == ""){
			name = "original";
		}
		
		return name;
	}

	public void SetCyclicBarrier(CyclicBarrier cb) {
		cyclicbarrierThread = cb;
		
	}

}
