package mutants.parallel;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class MutantFactory {
	
	private static List<MutantThread> RunnableList = new ArrayList<MutantThread>();
	private static Map<String,String> EPLEnvironment = null;
	private static List<String> EPLRunCommand = new ArrayList<String>();
	private static List<Thread> ThreadList = new ArrayList<Thread>();
	
	public static void SetEPLFolder(String PathToFolder){
	}
	
	public static void addProccess(String outputsFile, String EPLFolder, boolean original){
		RunnableList.add(new MutantThread(outputsFile, EPLFolder, original));
	}
	
	public static void runParallel(String OutputsFile, String executioncommand, String TestCaseFile) throws IOException, InterruptedException, BrokenBarrierException{
		
		CyclicBarrier cb = new CyclicBarrier(RunnableList.size() + 1);
		String[] inputParam = executioncommand.split(" ");		
		for (int i = 0; i < inputParam.length; i++){
			EPLRunCommand.add(inputParam[i]);
		}
		EPLRunCommand.add(TestCaseFile);
		
		for (MutantThread item : RunnableList){
			item.SetEnvironment(EPLEnvironment);
			item.SetRunCommand(EPLRunCommand);
			item.SetCyclicBarrier(cb);
		}
		
		for (MutantThread item : RunnableList){
			Thread t = new Thread(item);
			t.start();
			ThreadList.add(t);
		}
		
		cb.await();
		
		for (Thread t : ThreadList){
				t.join();
			}
		
		List<String> times = MutantThread.gettimes();

		int lastindex = OutputsFile.lastIndexOf("/");
		String TimesFile = OutputsFile.substring(0, lastindex) + "/ExecutionTimes.txt";
   		FileWriter ExecutionTimesFileNew = new FileWriter(TimesFile, true);
   		BufferedWriter bwf = new BufferedWriter(ExecutionTimesFileNew);
    		
   		for (String t : times){
   			long time = (long) (Long.parseLong(t.split(" ")[1])); //Seconds to nanoseconds x 1000000
   			String line = t.split(" ")[0] + " T " + time;
   			bwf.write(line + "\n");
   		}
        bwf.close();
        ExecutionTimesFileNew.close();
        
		List<String> paths = MutantThread.getpath();
		
		FileWriter OutputpathFile = new FileWriter(OutputsFile, true);
		BufferedWriter bw = new BufferedWriter(OutputpathFile);
		
		for (String s : paths){
			bw.write(s + "\n");
		}
        bw.close();
        OutputpathFile.close();
		
		}

}
