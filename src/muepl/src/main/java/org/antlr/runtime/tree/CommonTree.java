/*
00002  [The "BSD license"]
00003  Copyright (c) 2005-2009 Terence Parr
00004  All rights reserved.
00005 
00006  Redistribution and use in source and binary forms, with or without
00007  modification, are permitted provided that the following conditions
00008  are met:
00009  1. Redistributions of source code must retain the above copyright
00010      notice, this list of conditions and the following disclaimer.
00011  2. Redistributions in binary form must reproduce the above copyright
00012      notice, this list of conditions and the following disclaimer in the
00013      documentation and/or other materials provided with the distribution.
00014  3. The name of the author may not be used to endorse or promote products
00015      derived from this software without specific prior written permission.
00016 
00017  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
00018  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
00019  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
00020  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
00021  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
00022  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
00023  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
00024  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
00025  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
00026  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.antlr.runtime.tree;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

import operators.Operator;
import org.antlr.runtime.Token;
 
public class CommonTree extends BaseTree {
         public Token token;
 
         protected int startIndex=-1, stopIndex=-1;
 
         public CommonTree parent;
 
         public int childIndex = -1;
         static int countmut = 0;
         static boolean mutation = false;
         public boolean flagmutation = false;
 
         public CommonTree() { }
         
         public CommonTree(CommonTree node) {
                 super(node);
                 this.token = node.token;
                 this.startIndex = node.startIndex;
                 this.stopIndex = node.stopIndex;
         }
 
         public CommonTree(Token t) {
                 this.token = t;
         }
 
         public Token getToken() {
                 return token;
         }
 
         public Tree dupNode() {
                 return new CommonTree(this);
         }
 
         public boolean isNil() {
                 return token==null;
         }
 
         public int getType() {
                 if ( token==null ) {
                         return Token.INVALID_TOKEN_TYPE;
                 }
                 return token.getType();
         }
 
         public String getText() {
                 if ( token==null ) {
                         return null;
                 }
                 return token.getText();
         }
 
         public int getLine() {
                 if ( token==null || token.getLine()==0 ) {
                         if ( getChildCount()>0 ) {
                                 return getChild(0).getLine();
                         }
                         return 0;
                 }
                 return token.getLine();
         }

         public int getCharPositionInLine() {
                 if ( token==null || token.getCharPositionInLine()==-1 ) {
                         if ( getChildCount()>0 ) {
                                 return getChild(0).getCharPositionInLine();
                         }
                         return 0;
                 }
                 return token.getCharPositionInLine();
         }
 
         public int getTokenStartIndex() {
                 if ( startIndex==-1 && token!=null ) {
                         return token.getTokenIndex();
                 }
                 return startIndex;
         }
 
         public void setTokenStartIndex(int index) {
                 startIndex = index;
         }
 
         public int getTokenStopIndex() {
                 if ( stopIndex==-1 && token!=null ) {
                         return token.getTokenIndex();
                 }
                 return stopIndex;
         }
 
         public void setTokenStopIndex(int index) {
                 stopIndex = index;
        }

         public void setUnknownTokenBoundaries() {
         if ( children==null ) {
             if ( startIndex<0 || stopIndex<0 ) {
                 startIndex = stopIndex = token.getTokenIndex();
             }
             return;
         }
         for (int i=0; i<children.size(); i++) {
             ((CommonTree)children.get(i)).setUnknownTokenBoundaries();
         }
         if ( startIndex>=0 && stopIndex>=0 ) return; // already set
         if ( children.size() > 0 ) {
             CommonTree firstChild = (CommonTree)children.get(0);
             CommonTree lastChild = (CommonTree)children.get(children.size()-1);
             startIndex = firstChild.getTokenStartIndex();
             stopIndex = lastChild.getTokenStopIndex();
         }
     }
 
         public int getChildIndex() {
                 return childIndex;
         }
 
         public Tree getParent() {
                 return parent;
         }
 
         public void setParent(Tree t) {
                 this.parent = (CommonTree)t;
         }
 
         public void setChildIndex(int index) {
                 this.childIndex = index;
         }
 
         public String toString() {
                 if ( isNil() ) {
                         return "nil";
                 }
                 if ( getType()==Token.INVALID_TOKEN_TYPE ) {
                         return "<errornode>";
                 }
                 if ( token==null ) {
                         return null;
                 }
                 return token.getText();
         }
     		
     	public void apply (Operator op, int operandIndex, int attribute, boolean coverage) throws Exception {
     		
     		//TODO Cambiar el tipo de la función check, devolver un entero 0 = noes, 1 = es, >1 = caso especial (ELKE)
     		// Añadir el operador como parámetro de check, en ELKE se guardaría en una variable constante para ese operador
     		// Y a la hora del apply ya tenemos el operador que hay que modificar/o bien podemos guardar la posición.
     		
     		int thereIs = 0;
     		
     		if (!mutation){
     			
     			List children = this.getChildren();
     				
     			if (!isNil()) {
     				thereIs = op.check(this, operandIndex);
     				
     				if (thereIs == 0){
     					if ((children == null) || children.size() == 0) { 
     						return ;
     					}
     				}
     				else {
     					countmut += thereIs; 
     					if (countmut == operandIndex){
     						op.apply(this, attribute, coverage);
     						mutation = true;
     					}
     				}
     			}
     			else {
     				if (children == null || children.size() == 0) { 
     					return ;
     				}
     			}
     			
     			if (!mutation){
	     			for (int i = 0; children != null && i < this.getChildCount(); i++){
	     				if (mutation){
	     					break;
	     				}
	     				CommonTree t = (CommonTree) children.get(i);
	     				t.apply(op, operandIndex, attribute, coverage);
	     			}
     			}
     		}
     	}

		public String treeToQuery(Operator op, int operandIndex, int attribute) throws Exception {
			FileWriter MutFile = new FileWriter(op.getName() + "_" + operandIndex + "_" + attribute + ".epl");
			BufferedWriter bw = new BufferedWriter(MutFile);
			
			bw.write(finalquery);
	    	bw.close();
	    	
	    	return finalquery;
		}

		public static String finalquery="";
		
		public String toQuery() {
			
			int tokenID;
			tokenID = this.token.getType();
			
			String[] Operators = {"-", "+", "*", "/", "%"} ;
			
			int children, cont;
			boolean coma = false;
			boolean punto = false;
			String subquery = "";
				
			switch(tokenID){
			case 281: //EPL_EXPR
				children = this.getChildCount();
				subquery = "";
				for (int i = 0; i < children; i++){
					subquery = subquery + ((CommonTree) this.getChild(i)).toQuery();
				}						
				return "" + subquery;
			case 129: //FOLLOWED_BY_EXPR
				return ((CommonTree) this.getChild(0)).toQuery() + " -> " + ((CommonTree) this.getChild(1)).toQuery();
			case 130: //FOLLOWED_BY_ITEM
				return ((CommonTree) this.getChild(0)).toQuery();
			case 134: //EVENT_FILTER_EXPR
				children = this.getChildCount();
				subquery = "";
				punto = false;
				for (int i = 0; i < children; i++){
					if (punto == false){
						punto = true;
					} else {
						subquery = subquery + ".";
					}
					subquery = subquery + ((CommonTree) this.getChild(i)).toQuery();
				}
				return subquery;
			case 135: //EVENT_FILTER_PROPERTY_EXPR
				children = this.getChildCount();
				for (int i = 0; i < children; i++){
					subquery = subquery + ((CommonTree) this.getChild(i)).toQuery();
				}
				return subquery;
			case 136: //EVENT_FILTER_PROPERTY_EXPR_ATOM
				children = this.getChildCount();
				for (int i = 0; i < children; i++){
					subquery = subquery + ((CommonTree) this.getChild(i)).toQuery();
				}
				return "[" + subquery + "]";
			case 155: //OBSERVER_EXPR
				return ((CommonTree) this.getChild(0)).toQuery() + ":" + ((CommonTree) this.getChild(1)).toQuery() + "(" + ((CommonTree) this.getChild(2)).toQuery() + ")";
			case 162: //EVAL_ISNOT_EXPR
				return ((CommonTree) this.getChild(0)).toQuery() + " is not " + ((CommonTree) this.getChild(1)).toQuery();
			case 161: //EVAL_IS_EXPR
				return ((CommonTree) this.getChild(0)).toQuery() + " is " + ((CommonTree) this.getChild(1)).toQuery();
			case 166: //SELECTION_EXPR
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (this.getChild(cont).getType() == 167 && coma == false){
						coma = true;
					} else if (this.getChild(cont).getType() == 167 && coma == true){
						subquery = subquery + ", ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				if (this.parent.children.toString().contains("STREAM_EXPR")){
					return "select " + subquery + " from ";
				} else{					
					return "select " + subquery;
				}
			case 167: //SELECTION_ELEMENT_EXPR
				children = this.getChildCount();
				if (children == 1){
					return ((CommonTree) this.getChild(0)).toQuery();
				}
				if (children == 2){
					return ((CommonTree) this.getChild(0)).toQuery() + " as " + ((CommonTree) this.getChild(1)).toQuery();
				}
			case 170: //OUTERJOIN_EXPR
				return " outer join " + " on " + ((CommonTree) this.getChild(0)).toQuery() + " = " + ((CommonTree) this.getChild(1)).toQuery();
			case 171: //INNERJOIN_EXPR
				return " inner join " + " on " + ((CommonTree) this.getChild(0)).toQuery() + " = " + ((CommonTree) this.getChild(1)).toQuery();
			case 172: //LEFT_OUTERJOIN_EXPR
				return " left outer join " + " on " + ((CommonTree) this.getChild(0)).toQuery() + " = " + ((CommonTree) this.getChild(1)).toQuery();
			case 173: //RIGHT_OUTERJOIN_EXPR
				return " right outer join " + " on " + ((CommonTree) this.getChild(0)).toQuery() + " = " + ((CommonTree) this.getChild(1)).toQuery();
			case 174: //FULL_OUTERJOIN_EXPR
				return " full outer join " + " on " + ((CommonTree) this.getChild(0)).toQuery() + " = " + ((CommonTree) this.getChild(1)).toQuery();
			case 176: //ORDER BY
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else{
						subquery = subquery + ", ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return " order by " + subquery;
			case 177: //ORDER_ELEMENT_EXPR
				children = this.getChildCount();
				if (children == 1){
					return ((CommonTree) this.getChild(0)).toQuery();
				}
				if (children == 2){
					return ((CommonTree) this.getChild(0)).toQuery() + " " + ((CommonTree) this.getChild(1)).toQuery();
				}
			case 178: //EVENT_PROP_EXPR
				children = this.getChildCount();
				if (children == 1){
					return ((CommonTree) this.getChild(0)).toQuery();
				}
				if (children == 2){
					return ((CommonTree) this.getChild(0)).toQuery() + "." + ((CommonTree) this.getChild(1)).toQuery();
				}
			case 179: //EVENT_PROP_SIMPLE
				return ((CommonTree) this.getChild(0)).toQuery();
			case 182: //EVENT_PROP_DYNAMIC_SIMPLE
				return ((CommonTree) this.getChild(0)).toQuery() + "?";
			case 191: //INSERTINTO_EXPR
				return "insert into " + ((CommonTree) this.getChild(0)).toQuery() + " ";
			case 195: //LIB_FUNCTION
				children = this.getChildCount();
				subquery = "";
				String subsub = "";
				punto = false;
				coma = false;
				if (children > 2){
					for (int i = 0; i < 2; i++){
						if (this.getChild(i).getType() == 148 && punto == false){
							punto = true;
						} else if (punto == true){
							subsub = subsub + ".";
							punto = false;
						}
						subsub = subsub + ((CommonTree) this.getChild(i)).toQuery();
					}
					subquery = subsub;
					subsub = "";
					for (int i = 2; i < children; i++){
						if (coma == false){
							coma = true;
						} else if (i != children - 1){
							subsub = subsub + ", ";
						}
						subsub = subsub + ((CommonTree) this.getChild(i)).toQuery();
					}
					subquery = subquery + "(" + subsub + ")";
				} else{
					subquery = ((CommonTree) this.getChild(0)).toQuery();;
				}
				return subquery;
			case 196: //LIB_FUNC_CHAIN
				return ((CommonTree) this.getChild(0)).toQuery();
			case 6: //IN
				subquery = "";
				children = this.getChildCount();
				for (int i = 0; i < children; i++){
					subquery = subquery + ((CommonTree) this.getChild(i)).toQuery();
					if (this.getChild(i).getType() == 277){
						subquery = subquery + ", ";
					}
				}
				return this.toString() + " " + subquery;
			case 7: //BETWEEN
				return "(" + ((CommonTree) this.getChild(0)).toQuery() + " between " + ((CommonTree) this.getChild(1)).toQuery() + " and " + ((CommonTree) this.getChild(2)).toQuery() + ")";
			case 8: //LIKE
				return ((CommonTree) this.getChild(0)).toQuery() + " like " + ((CommonTree) this.getChild(1)).toQuery();
			case 11: //or
				return ((CommonTree) this.getChild(0)).toQuery() + " " + this.toString() + " " + ((CommonTree) this.getChild(1)).toQuery();
			case 13: //NOT_EXPR
				return "not " + ((CommonTree) this.getChild(0)).toQuery();
			case 18: //sum()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else{
						if (this.getChild(cont - 1).getType() != 47 && this.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 19: //avg()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (this.getChild(cont - 1).getType() != 47 && this.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 20: //max()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (this.getChild(cont - 1).getType() != 47 && this.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 21: //min()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (this.getChild(cont - 1).getType() != 47 && this.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 23: //median()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (this.getChild(cont - 1).getType() != 47 && this.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 24: //stddev()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (this.getChild(cont - 1).getType() != 47 && this.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 25: //avedev()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else{
						if (this.getChild(cont - 1).getType() != 47 && this.getChild(cont - 1).getType() != 46){							
							subquery = subquery + ", ";
						}
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 26: //count()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (this.getChild(cont - 1).getType() != 47 && this.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 28: //case
				children = this.getChildCount();
				subquery = "";
				if (children == 3){
					subquery = ((CommonTree) this.getChild(0)).toQuery() + "then " + ((CommonTree) this.getChild(1)).toQuery() + " else " + ((CommonTree) this.getChild(2)).toQuery();
				}				
				return "=" + this.toString() + " when " + subquery + " end";
			case 46: //All
				return this.toString() + " ";
			case 47: //distinct
				return this.toString() + " ";
			case 48: //any
				return this.toString() + " ";
			case 69: //PREVIOUS
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						subquery = subquery + ", ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 70: //prevtail()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						subquery = subquery + ", ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 71: //prevcount()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						subquery = subquery + ", ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 72: //prevwindow()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						subquery = subquery + ", ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 73: //prior()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						subquery = subquery + ", ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 77: //instanceof()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						subquery = subquery + ", ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 79: //cast()
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						subquery = subquery + ", ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				return this.toString() + "(" + subquery + ")";
			case 106: //BOOLEAN_TRUE
				return " true";
			case 107: //BOOLEAN_FALSE
				return " false";
			case 154: //WHERE_EXPR
				children = this.getChildCount();
				subquery = "";
				if (children != 0){					
					for (cont = 0; cont < children; cont++){
						if (coma == false){
							coma = true;
						} else {
							subquery = subquery + ", ";
						}
						subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
					}
					return " where " + subquery;
				} else{
					return "";
				}				
			case 164: //EVAL_NOTEQUALS_GROUP_EXPR
				children = this.getChildCount();
				subquery = "";
				boolean done = false;
				for (cont = 0; cont < children; cont++){
					if (this.getChild(cont).getType() != 178){
						subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery() + "(" + ((CommonTree) this.getChild(cont + 1)).toQuery() + ")";
						done = true;
					} else if (this.getChild(cont).getType() == 178 && done){
						;
					} else if (this.getChild(cont).getType() == 178 && done == false){
						subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery() + " != ";
					}
				}
				return subquery;
			case 169: //STREAM_EXPR
				children = this.getChildCount();
							
				for (cont = 0; cont < children; cont++){
					CommonTree kid = (CommonTree) this.getChild(cont);
					if (kid.token.getType() == 294){
						subquery = subquery + " as " + ((CommonTree) this.getChild(cont)).toQuery();
					} else{
						subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery(); 
					}
				}
				return subquery;
			case 152: //PATTERN_INCL_EXPR
				children = this.getChildCount();
				for (cont = 0; cont < children; cont++){
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery(); 
				}
				return "pattern[" + subquery + "]";
			case 14: //EVERY
				return "every " + ((CommonTree) this.getChild(0)).toQuery();
			case 148: //Variable
				return this.toString();
			case 149: //GUARD_EXPR
				children = this.getChildCount();
				subquery = "";
				for (int i = 0; i < children; i++){					
					subquery = subquery + ((CommonTree) this.getChild(i)).toQuery();
					coma = false;
					if (i == 0){
						subquery = subquery + " where ";
					}
					if (i == 1){
						subquery = subquery + ":";
					}
					if (i < children - 1){						
						if (this.getChild(i).getType() == 199 && this.getChild(i + 1).getType() == 199){
							subquery = subquery + ", ";
						}
					}
				}
				return subquery;
			case 131: //PATTERN_FILTER_EXPR
				children = this.getChildCount();
				if (children == 2){					
					return ((CommonTree) this.getChild(0)).toQuery() + "=" + ((CommonTree) this.getChild(1)).toQuery();
				}
				if (children == 3){
					return ((CommonTree) this.getChild(0)).toQuery() + "=" + ((CommonTree) this.getChild(1)).toQuery() + " (" + ((CommonTree) this.getChild(2)).toQuery() + ")";
				}
			case 151: //VIEW_EXPR
				children = this.getChildCount();
				subquery = "";
				coma = false;
				
				if (children == 2){					
					return "." + ((CommonTree) this.getChild(0)).toQuery() + ":" + ((CommonTree) this.getChild(1)).toQuery() + "()";
				}
				if (children > 2){
					for (int i = 2; i < children; i++){
						if (coma == false){							
							subquery = ((CommonTree) this.getChild(i)).toQuery();
							coma = true;
						} else{
							subquery = subquery + ", " + ((CommonTree) this.getChild(i)).toQuery();
						}
					}
					return "." + ((CommonTree) this.getChild(0)).toQuery() + ":" + ((CommonTree) this.getChild(1)).toQuery() + "(" + subquery + ")";
				}
			case 160: //HAVING_EXPR
				return " having " + ((CommonTree) this.getChild(0)).toQuery();
			case 157: //EVAL_AND_EXPR 
				return "(" + ((CommonTree) this.getChild(0)).toQuery() + " and " + ((CommonTree) this.getChild(1)).toQuery() + ")";
			case 158: //EVAL_OR_EXPR 
				return  "(" + ((CommonTree) this.getChild(0)).toQuery() + " or " + ((CommonTree) this.getChild(1)).toQuery() + ")";
			case 159: //EVAL_EQUALS_EXPR
				children = this.getChildCount();
				if (children == 2){
					return ((CommonTree) this.getChild(0)).toQuery() + " = " + ((CommonTree) this.getChild(1)).toQuery();
				} else{
					return ((CommonTree) this.getChild(0)).toQuery() + " = " + ((CommonTree) this.getChild(1)).toQuery() + ((CommonTree) this.getChild(2)).toQuery();
				}
			case 165: //EVAL_NOTEQUALS_EXPR
				children = this.getChildCount();
				if (children == 2){
					return ((CommonTree) this.getChild(0)).toQuery() + " != " + ((CommonTree) this.getChild(1)).toQuery();
				} else{
					return ((CommonTree) this.getChild(0)).toQuery() + " != " + ((CommonTree) this.getChild(1)).toQuery() + ((CommonTree) this.getChild(2)).toQuery();
				}
			case 210: //NOT_BETWEEN
				return "(" + ((CommonTree) this.getChild(0)).toQuery() + " not between " + ((CommonTree) this.getChild(1)).toQuery() + " and " + ((CommonTree) this.getChild(2)).toQuery() + ")";
			case 221: //SUBSELECT_GROUP_EXPR
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (this.getChild(cont).getType() == 167 && coma == false){
						coma = true;
					} else if (this.getChild(cont).getType() == 167 && coma == true){
						subquery = subquery + ", ";
					} else if (this.getChild(cont).getType() == 169){
						subquery = subquery + " from ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}						
				return "(select " + subquery + ")";
			case 222: //EXISTS_SUBSELECT_EXPR				
				children = this.getChildCount();
				subquery = "";
				
				if (this.getChild(0).getType() == 178){
					for (int i = 1; i < children; i++){
						subquery = subquery + ((CommonTree) this.getChild(i)).toQuery();
					}
					return ((CommonTree) this.getChild(0)).toQuery() + " exists (" + subquery + ")";						
				} else {
					for (int i = 0; i < children; i++){
						if (this.getChild(i).getType() == 167 && coma == false){
							coma = true;
						} else if (this.getChild(i).getType() == 167 && coma == true){
							subquery = subquery + ", ";
						} else if (this.getChild(i).getType() == 169){
							subquery = subquery + " from ";
						}
						subquery = subquery + ((CommonTree) this.getChild(i)).toQuery();
					}
					return "exists (select " + subquery + ")";

				}
			case 223: //IN_SUBSELECT_EXPR
				String variable = "";
				cont = 0;
				if (this.getChild(cont).getType() == 178){
					variable = ((CommonTree) this.getChild(cont)).toQuery();
					cont++;
				}
				return variable + " in (" + ((CommonTree) this.getChild(cont)).toQuery() + ")";
			case 225: //IN_SUBSELECT_QUERY_EXPR
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (this.getChild(cont).getType() == 167 && coma == false){
						coma = true;
					} else if (this.getChild(cont).getType() == 167 && coma == true){
						subquery = subquery + ", ";
					} else if (this.getChild(cont).getType() == 169){
						subquery = subquery + " from ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}
				if (parent.getType() == 222){
					return subquery;
				} else{					
					return "select " + subquery;
				}
			case 229: //NOT_IN_SUBSELECT_EXPR
				return ((CommonTree) this.getChild(0)).toQuery() + " not in (" + ((CommonTree) this.getChild(1)).toQuery() + ")";
			case 233: //ON_EXPR
				subquery = "";
				children = this.getChildCount();
				for (int i = 0; i < children; i++){
					subquery = subquery + ((CommonTree) this.getChild(i)).toQuery();
				}
				return "on " + subquery;
			case 234: //ON_STREAM
				return ((CommonTree) this.getChild(0)).toQuery();
			case 237: //CREATE_WINDOW_EXPR
				subquery = "";
				children = this.getChildCount();
				if (children == 3){
					subquery = subquery + ((CommonTree) this.getChild(0)).toQuery() + ((CommonTree) this.getChild(1)).toQuery() + ((CommonTree) this.getChild(2)).toQuery();
				}
				if (children == 4){
					subquery = subquery + ((CommonTree) this.getChild(0)).toQuery() + ((CommonTree) this.getChild(1)).toQuery() + " as " + ((CommonTree) this.getChild(2)).toQuery() + ((CommonTree) this.getChild(3)).toQuery();
				}
				return "create window " + subquery;
			case 242: //ON_SELECT_EXPR
				return "select " + ((CommonTree) this.getChild(0)).toQuery();
			case 243: //ON_UPDATE_EXPR
				subquery = "";
				children = this.getChildCount();
				for (int i = 0; i < children; i++){
					subquery = subquery + ((CommonTree) this.getChild(i)).toQuery();
				}
				return "update " + subquery;
			case 241: //ON_EXPR_FROM
				return ((CommonTree) this.getChild(0)).toQuery() + " as " + ((CommonTree) this.getChild(1)).toQuery();
			case 261: //ON_SET_EXPR_ITEM
				subquery = "";
				children = this.getChildCount();
				for (int i = 0; i < children; i++){
					subquery = subquery + ((CommonTree) this.getChild(i)).toQuery();
				}
				return " set " + subquery;
			case 305: //* (multiplicación)
				return ((CommonTree) this.getChild(0)).toQuery() + " " + this.toString() + " " + ((CommonTree) this.getChild(1)).toQuery();
			case 307: //+
				return ((CommonTree) this.getChild(0)).toQuery() + " " + this.toString() + " " + ((CommonTree) this.getChild(1)).toQuery();
			case 317: //>
				children = this.getChildCount();
				if (children == 2){
					return ((CommonTree) this.getChild(0)).toQuery() + " " + this.toString() + " " + ((CommonTree) this.getChild(1)).toQuery();
				} else{
					return ((CommonTree) this.getChild(0)).toQuery() + " " + this.toString() + " " + ((CommonTree) this.getChild(1)).toQuery() + ((CommonTree) this.getChild(2)).toQuery();
				}
			case 316: //<
				children = this.getChildCount();
				if (children == 2){
					return ((CommonTree) this.getChild(0)).toQuery() + " " + this.toString() + " " + ((CommonTree) this.getChild(1)).toQuery();
				} else{
					return ((CommonTree) this.getChild(0)).toQuery() + " " + this.toString() + " " + ((CommonTree) this.getChild(1)).toQuery() + ((CommonTree) this.getChild(2)).toQuery();
				}
			case 318: //<=
				children = this.getChildCount();
				if (children == 2){
					return ((CommonTree) this.getChild(0)).toQuery() + " " + this.toString() + " " + ((CommonTree) this.getChild(1)).toQuery();
				} else{
					return ((CommonTree) this.getChild(0)).toQuery() + " " + this.toString() + " " + ((CommonTree) this.getChild(1)).toQuery() + ((CommonTree) this.getChild(2)).toQuery();
				}
			case 319: //>=
				children = this.getChildCount();
				if (children == 2){
					return ((CommonTree) this.getChild(0)).toQuery() + " " + this.toString() + " " + ((CommonTree) this.getChild(1)).toQuery();
				} else{
					return ((CommonTree) this.getChild(0)).toQuery() + " " + this.toString() + " " + ((CommonTree) this.getChild(1)).toQuery() + ((CommonTree) this.getChild(2)).toQuery();
				}		
			case 175: //GROUP_BY_EXPR
				children = this.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (this.getChild(cont).getType() == 167 && coma == false){
						coma = true;
					} else if (this.getChild(cont).getType() == 167 && coma == true){
						subquery = subquery + ", ";
					}
					subquery = subquery + ((CommonTree) this.getChild(cont)).toQuery();
				}						
				return "group by " + subquery;
			case 199: //TIME_PERIOD
				return "(" + ((CommonTree) this.getChild(0)).toQuery() + ")";
			case 204: //HOUR_PART
				if (this.token.getText().equals("MILLISECOND_PART")){			
					return ((CommonTree) this.getChild(0)).toQuery() + " msec";
				}
				if (this.token.getText().equals("SECOND_PART")){			
					return ((CommonTree) this.getChild(0)).toQuery() + " sec";
				}
				if (this.token.getText().equals("MINUTE_PART")){	
					return ((CommonTree) this.getChild(0)).toQuery() + " min";
				}
				if (this.token.getText().equals("HOUR_PART")){
					return ((CommonTree) this.getChild(0)).toQuery() + " hr";
				}
				if (this.token.getText().equals("DAY_PART")){
					return ((CommonTree) this.getChild(0)).toQuery() + " days";
				}
			case 205: //HOUR_PART
				if (this.token.getText().equals("MILLISECOND_PART")){			
					return ((CommonTree) this.getChild(0)).toQuery() + " msec";
				}
				if (this.token.getText().equals("SECOND_PART")){			
					return ((CommonTree) this.getChild(0)).toQuery() + " sec";
				}
				if (this.token.getText().equals("MINUTE_PART")){	
					return ((CommonTree) this.getChild(0)).toQuery() + " min";
				}
				if (this.token.getText().equals("HOUR_PART")){
					return ((CommonTree) this.getChild(0)).toQuery() + " hr";
				}
				if (this.token.getText().equals("DAY_PART")){
					return ((CommonTree) this.getChild(0)).toQuery() + " days";
				}
			case 206: //SECOND_PART
				if (this.token.getText().equals("MILLISECOND_PART")){			
					return ((CommonTree) this.getChild(0)).toQuery() + " msec";
				}
				if (this.token.getText().equals("SECOND_PART")){			
					return ((CommonTree) this.getChild(0)).toQuery() + " sec";
				}
				if (this.token.getText().equals("MINUTE_PART")){	
					return ((CommonTree) this.getChild(0)).toQuery() + " min";
				}
				if (this.token.getText().equals("HOUR_PART")){
					return ((CommonTree) this.getChild(0)).toQuery() + " hr";
				}
				if (this.token.getText().equals("DAY_PART")){
					return ((CommonTree) this.getChild(0)).toQuery() + " days";
				}
			case 207: //SECOND_PART
				if (this.token.getText().equals("MILLISECOND_PART")){			
					return ((CommonTree) this.getChild(0)).toQuery() + " msec";
				}
				if (this.token.getText().equals("SECOND_PART")){			
					return ((CommonTree) this.getChild(0)).toQuery() + " sec";
				}
				if (this.token.getText().equals("MINUTE_PART")){	
					return ((CommonTree) this.getChild(0)).toQuery() + " min";
				}
				if (this.token.getText().equals("HOUR_PART")){
					return ((CommonTree) this.getChild(0)).toQuery() + " hr";
				}
				if (this.token.getText().equals("DAY_PART")){
					return ((CommonTree) this.getChild(0)).toQuery() + " days";
				}
			case 208: //SECOND_PART
				if (this.token.getText().equals("MILLISECOND_PART")){			
					return ((CommonTree) this.getChild(0)).toQuery() + " msec";
				}
				if (this.token.getText().equals("SECOND_PART")){			
					return ((CommonTree) this.getChild(0)).toQuery() + " sec";
				}
				if (this.token.getText().equals("MINUTE_PART")){	
					return ((CommonTree) this.getChild(0)).toQuery() + " min";
				}
				if (this.token.getText().equals("HOUR_PART")){
					return ((CommonTree) this.getChild(0)).toQuery() + " hr";
				}
				if (this.token.getText().equals("DAY_PART")){
					return ((CommonTree) this.getChild(0)).toQuery() + " days";
				}
			case 194:
				return "Math." + ((CommonTree) this.getChild(0)).toQuery() + "(" + ((CommonTree) this.getChild(1)).toQuery() + ")";
			case 216: //*
				return this.toString();
			case 276: //Variables
				return this.toString();
			case 277: //Text
				for (int i = 0; i < Operators.length; i ++){
					if (Operators[i].equals(this.token.getText())){
						return ((CommonTree) this.getChild(0)).toQuery() + " " + this.token.getText() + " " + ((CommonTree) this.getChild(1)).toQuery();
					}
				}
				return this.toString();
			case 273: //Numbers
				return this.toString();
			case 294: //Variables
				return this.toString();
			case 297: //(
				if (this.parent.getChildren().toString().contains(")")){
					return "(";
				} else {
					return "";
				}
			case 322: //Numbers
				return this.toString();
			default:
				return this.toString() + " ";
			}
		}
	}

