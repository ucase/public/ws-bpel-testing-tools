package org.antlr.runtime.tree;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import operators.Operator;

public class EPLTree {

         static public int countmut = 0;
         boolean mutation = false;
         public CommonTree mycommontree;
         HashMap<Integer, String> outerjoinnodes = new HashMap<Integer, String>();
         List<Integer> types = new ArrayList<Integer>();
         List<Integer> nocommastokens = new ArrayList<Integer>();
         
         public EPLTree() { }
         
         public EPLTree(CommonTree node) {
               mycommontree = node;
               outerjoinnodes.put(177, " left outer join ");
               outerjoinnodes.put(175, " outer join ");
               outerjoinnodes.put(179, " full outer join ");
               outerjoinnodes.put(178, " right outer join ");
               outerjoinnodes.put(176, " inner join ");
               
               // Variable types
               types.add(300);
               types.add(301);
               types.add(302);
               types.add(303);
               
               // Tokens without commas
               nocommastokens.add(7);
               nocommastokens.add(6);
               nocommastokens.add(216);
               nocommastokens.add(172);
               nocommastokens.add(224);
         }
 
     	
    	public void apply (Operator op, int operandIndex, int attribute, boolean coverage) throws Exception {
     		
     		int thereIs = 0;
     		
     		if (!mutation){
     			
     			List children = mycommontree.getChildren();
     				
     			if (!mycommontree.isNil()) {
     				thereIs = op.check(mycommontree, operandIndex);
     				//System.out.println("thereIs: " + thereIs);
     				if (thereIs == 0){
     					if ((children == null) || children.size() == 0) { 
     						return ;
     					}
     				}
     				else {
     					countmut += thereIs;
     					if (countmut == operandIndex){
     						op.apply(mycommontree, attribute, coverage);
     						mutation = true;
     					}
     				}
     			}
     			else {
     				if (children == null || children.size() == 0) { 
     					return ;
     				}
     			}
     			
     			if (!mutation){
	     			for (int i = 0; children != null && i < mycommontree.getChildCount(); i++){
	     				if (mutation){
	     					break;
	     				}
	     				CommonTree child = (CommonTree) mycommontree.children.get(i);
	     				EPLTree t = new EPLTree (child);
	     				t.apply(op, operandIndex, attribute, coverage);
	     			}
     			}
     		}
     	}

		/**
		 * Create a new directory with the mutant queries in a file called queries.epl
		 * @param pathToEPL 
		 * @param pathToEPL: The path to the original directory
		 * @param op: Operator name
		 * @param operandIndex: Operator index
		 * @param attribute: Operator attribute
		 * @param query: The mutant query
		 * @throws Exception
		 */
    	public void QueryToFile(String pathToProgram, String pathToEPL, String QueryLocationFolder, Operator op, int operandIndex, int attribute, String query) throws Exception {
    					
    		String pathToMutantProgram = pathToProgram + op.getName() + "_" + operandIndex + "_" + attribute;
			File directMutant = new File(pathToMutantProgram);
			String QueryLocationPath = "";
			
			if (!directMutant.exists()){
				directMutant.mkdirs();
				// Copying all files from Original directory to the Mutant directory
				CopyingFiles(pathToProgram, pathToMutantProgram);
			}			
			
			if (QueryLocationFolder.equals("./")) {
				QueryLocationPath = directMutant.toPath().resolve(QueryLocationFolder).toString();
			} else {
				QueryLocationPath = directMutant.toString() + QueryLocationFolder;
			}
			
			File outputFolder = new File(QueryLocationPath);
			if (!outputFolder.exists()){
				outputFolder.mkdirs();
			}
			
			FileWriter MutFile = new FileWriter(new File(outputFolder, "queries.epl"), true);
			BufferedWriter bw = new BufferedWriter(MutFile);
			
			bw.write(query + "\n");
	    	bw.close();
		}    	


		/**
    	 * Check if the original directory exist before copy
    	 * @param substring: the original directory
    	 * @param pathToEPLMutant: the directory where the copy will be done
    	 */
    	private void CopyingFiles(String substring, String pathToEPLMutant) {
			File srcFolder = new File(substring);
			File destFolder = new File(pathToEPLMutant);

    		if (!srcFolder.exists()){
    			System.out.println("Directory does not exist.");
    			System.exit(0);
    		}else {
    			try {
    				copyFolder(srcFolder, destFolder);
    			}catch(IOException e){
    				e.printStackTrace();
    				System.exit(0);
    			}
    		}			
		}

		
    	/**
    	 * Recursive function which copy from the original directory to 
    	 * the mutant one the necessary files to build the program, with
    	 * the exception of "queries.epl" which will be create in 
    	 * {@link #QueryToFile(String, Operator, int, int, String)}
    	 * @param src: Original directory
    	 * @param dest: Destination directory
    	 * @throws IOException
    	 */
    	private void copyFolder(File src, File dest) throws IOException {
			
			if (src.isDirectory()){
				if (!dest.exists()){
					dest.mkdir();
				}
				
				String files[] = src.list();
				
				for (String file : files){
					// Construct the file structure
					File srcFile = new File(src, file);
					File destFile = new File(dest, file);
					// recursive copy
					copyFolder(srcFile, destFile);
				}
			} else{
				// If it is a file
				
				if (!src.getName().equals("queries.epl")){
					InputStream in = new FileInputStream(src);
					OutputStream out = new FileOutputStream(dest);
						
					byte[] buffer = new byte[1024];
						
					int length;
					// copy the file content in bytes
					while ((length = in.read(buffer)) > 0){
						out.write(buffer, 0, length);
					}
						
					in.close();
					out.close();
				}							
			}
		}

		/**
    	 * Transform the mutant EPLtree to a String
    	 * @return according to the tokens the Strings of the query
    	 */
    	public String toQuery() {
			
			int tokenID;
			tokenID = mycommontree.token.getType();
			
			String[] Operators = {"-", "+", "*", "/", "%"} ;
			
			int children, cont, i;
			boolean coma = false;
			boolean parenthesis = false;
			String subquery = "";
			CommonTree ctree1, ctree2, ctree3, ctree4, ctree5;
			EPLTree epltree1, epltree2, epltree3, epltree4, epltree5;
			
			switch(tokenID){
			case 308: //EPL_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (i = 0; i < children; i++){
					if (i + 1 < children){
						if (outerjoinnodes.containsKey(mycommontree.getChild(i + 1).getType())){
							subquery = subquery + outerjoinnodes.get(mycommontree.getChild(i + 1).getType());
						} else {
						if (mycommontree.getChild(i).getType() == 174 && coma == false){
							coma = true;
						}
						else if (mycommontree.getChild(i).getType() == 174 && coma == true){
							subquery = subquery + ", ";
							}
						}
					}
					
					if (i + 1 == children && mycommontree.getChild(i).getType() == 174 && coma == true){
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(i);
					EPLTree epltree = new EPLTree(ctree1); 
					subquery = subquery + epltree.toQuery();
				}					
				return "" + subquery;
			case 134: //FOLLOWED_BY_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (i = 0; i < children - 1; i++){
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					subquery += epltree1.toQuery() + " -> ";
				}
				
				ctree2 = (CommonTree) mycommontree.getChild(children - 1);
				epltree2 = new EPLTree(ctree2);
				return subquery + epltree2.toQuery();
			case 135: //FOLLOWED_BY_ITEM
				children = mycommontree.getChildCount();
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return epltree1.toQuery();
			case 139: //EVENT_FILTER_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				parenthesis = false;
				for (i = 0; i < children; i++){
					if (mycommontree.getChild(i).getChildCount() != 0){
						if (mycommontree.getChild(i).getChild(0).getType() != 141){
							if (mycommontree.getChild(i).getType() != 140){
								subquery = subquery + "(";
								parenthesis = true;
							} else {
								subquery = subquery + ".";
							}
						}
					}
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				
				if (parenthesis == true){
					subquery = subquery + ")";
				}
				return subquery;
			case 140: //EVENT_FILTER_PROPERTY_EXPR
				children = mycommontree.getChildCount();
				for (i = 0; i < children; i++){
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return subquery;
			case 141: //EVENT_FILTER_PROPERTY_EXPR_ATOM
				children = mycommontree.getChildCount();
				for (i = 0; i < children; i++){
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return "[" + subquery + "]";
			case 167: //EVAL_ISNOT_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " is not " + epltree2.toQuery();
			case 166: //EVAL_IS_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " is " + epltree2.toQuery();
			case 171: //SELECTION_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (mycommontree.getChild(cont).getType() == 172 && coma == false){
						coma = true;
					} else if (mycommontree.getChild(cont).getType() == 172 && coma == true){
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (mycommontree.parent.children.toString().contains("STREAM_EXPR")){
					return "select " + subquery + " from ";
				} else{					
					return "select " + subquery;
				}
			case 172: //SELECTION_ELEMENT_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (i = 0; i < children; i++){
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					if (ctree1.getType() == 325){
						subquery = subquery + " as " + epltree1.toQuery();
					}

					if (ctree1.getType() == 352 || ctree1.getType() == 336 || ctree1.getType() == 342 || ctree1.getType() == 353 || ctree1.getType() == 354) {
						subquery = subquery + "(" + epltree1.toQuery() + ")";
					}
					if (ctree1.getType() != 325 && ctree1.getType() != 352 && ctree1.getType() != 336 && ctree1.getType() != 342 && ctree1.getType() != 353 && ctree1.getType() != 354){
						subquery = subquery + epltree1.toQuery();
					}
				}
				return subquery;
			case 175: //OUTERJOIN_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				ctree3 = (CommonTree) mycommontree.getChild(2);
				epltree3 = new EPLTree(ctree3);
				return epltree1.toQuery() + epltree2.toQuery() + " = " + epltree3.toQuery(); // " outer join " 
			case 176: //INNERJOIN_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				ctree3 = (CommonTree) mycommontree.getChild(2);
				epltree3 = new EPLTree(ctree3);
				return epltree1.toQuery() + epltree2.toQuery() + " = " + epltree3.toQuery(); // " inner join "
			case 177: //LEFT_OUTERJOIN_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				ctree3 = (CommonTree) mycommontree.getChild(2);
				epltree3 = new EPLTree(ctree3);
				return epltree1.toQuery() + epltree2.toQuery() + " = " + epltree3.toQuery(); //" left outer join " + 
			case 178: //RIGHT_OUTERJOIN_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				ctree3 = (CommonTree) mycommontree.getChild(2);
				epltree3 = new EPLTree(ctree3);
				return epltree1.toQuery() + epltree2.toQuery() + " = " + epltree3.toQuery(); //" right outer join "
			case 179: //FULL_OUTERJOIN_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				ctree3 = (CommonTree) mycommontree.getChild(2);
				epltree3 = new EPLTree(ctree3);
				return epltree1.toQuery() + epltree2.toQuery() + " = " + epltree3.toQuery(); // " full outer join "
			case 181: //ORDER BY EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else{
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return " order by " + subquery;
			case 182: //ORDER_ELEMENT_EXPR
				children = mycommontree.getChildCount();
				if (children == 1){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery();
				}
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return epltree1.toQuery() + " " + epltree2.toQuery();
				}
			case 183: //EVENT_PROP_EXPR
				children = mycommontree.getChildCount();
				if (children == 1){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery();
				}
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return epltree1.toQuery() + "." + epltree2.toQuery();
				}
				if (children == 3){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					return epltree1.toQuery() + "." + epltree2.toQuery() + "." + epltree3.toQuery();
				}
			case 184: //EVENT_PROP_SIMPLE
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return epltree1.toQuery();
			case 185: //EVENT_PROP_MAPPED
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + "(" + epltree2.toQuery() + ")";
			case 187: //EVENT_PROP_DYNAMIC_SIMPLE
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return epltree1.toQuery() + "?";
			case 197: //INSERTINTO_EXPR
				children = mycommontree.getChildCount();
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				if (children == 1){
					return "insert into " + epltree1.toQuery() + " ";
				}
				if (children == 2){
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return "insert into " + epltree1.toQuery() + epltree2.toQuery();
				}
				
			case 201: //LIB_FUNCTION
				children = mycommontree.getChildCount();
				subquery = "";
				String subsub = "";
				coma = false;
				if (children > 2){
					if ((mycommontree.getChild(2).toString().equals("(") && mycommontree.getChild(0).toString().equals("aggregate")) || 
							((mycommontree.getChild(2).toString().equals("(") && mycommontree.getChild(0).toString().equals("max"))) ||
							((mycommontree.getChild(2).toString().equals("(") && mycommontree.getChild(0).toString().equals("min")))){
						ctree1 = (CommonTree) mycommontree.getChild(0);
						epltree1 = new EPLTree(ctree1);
						for (i = 1; i < children; i++){
							if (!mycommontree.getChild(i).toString().equals("(")){
								ctree2 = (CommonTree) mycommontree.getChild(i);
								epltree2 = new EPLTree(ctree2);
								subsub = subsub + epltree2.toQuery();
							}
						}
						
						subquery = epltree1.toQuery() + "(" + subsub + ")";
					} else {
						for (i = 0; i < 2; i++){
							if (mycommontree.getChild(i).getType() == 325){
								subsub = subsub + ".";
							}
							ctree1 = (CommonTree) mycommontree.getChild(i);
							epltree1 = new EPLTree(ctree1);
							subsub = subsub + epltree1.toQuery();
						}
						subquery = subsub;
						subsub = "";
						for (i = 2; i < children; i++){
							if (coma == false){
								coma = true;
							} else if (i != children - 1){
								subsub = subsub + ", ";
							}
							if (!mycommontree.getChild(i).toString().equals("(")){
								ctree1 = (CommonTree) mycommontree.getChild(i);
								epltree1 = new EPLTree(ctree1);
								subsub = subsub + epltree1.toQuery();
							}							
						}
						subquery = subquery + "(" + subsub + ")";
					}
				} else{
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					subquery = epltree1.toQuery();
				}
				return subquery;
			case 202: //LIB_FUNC_CHAIN
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return epltree1.toQuery();
			case 6: //IN_SET
				subquery = "";
				String values = "";
				children = mycommontree.getChildCount();
				if (mycommontree.getChild(0).getType() == 183){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					subquery = epltree1.toQuery() + " in " + epltree2.toQuery();
					i = 2;
					while (mycommontree.getChild(i).getType() != 327){
						ctree1 = (CommonTree) mycommontree.getChild(i);
						epltree1 = new EPLTree(ctree1);
						if (mycommontree.getChild(i + 1).getType() != 327){
							values = values + epltree1.toQuery() + ", ";
						} else {
							values = values + epltree1.toQuery();
						}
						i++;
					}
					ctree1 = (CommonTree) mycommontree.getChild(children - 1);
					epltree1 = new EPLTree(ctree1);
					values = values + epltree1.toQuery();
					subquery = subquery + values;
				} else {
					for (i = 0; i < children; i++){
						ctree1 = (CommonTree) mycommontree.getChild(i);
						epltree1 = new EPLTree(ctree1);
						subquery = subquery + epltree1.toQuery();
						if (mycommontree.getChild(i).getType() == 277){
							subquery = subquery + ", ";
						}
					}
					subquery = " in " + subquery;
				}
				return subquery;
			case 7: //BETWEEN
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				ctree3 = (CommonTree) mycommontree.getChild(2);
				epltree3 = new EPLTree(ctree3);
				return epltree1.toQuery() + " between " + epltree2.toQuery() + " and " + epltree3.toQuery();
			case 8: //LIKE
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " like " + epltree2.toQuery();
			case 11: //OR_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery();
			case 12: //AND_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery();
			case 13: //NOT_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return "not " + epltree1.toQuery();
			case 18: //sum()
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else{
						if (mycommontree.getChild(cont - 1).getType() != 47 && mycommontree.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (children == 0){
					return mycommontree.toString() + " " + subquery;
				} else {
					return mycommontree.toString() + "(" + subquery + ")";
				}				
			case 19: //avg()
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (mycommontree.getChild(cont - 1).getType() != 47 && mycommontree.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (children == 0){
					return mycommontree.toString() + " " + subquery;
				} else {
					return mycommontree.toString() + "(" + subquery + ")";
				}		
			case 20: //max()
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (mycommontree.getChild(cont - 1).getType() != 47 && mycommontree.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (children == 0){
					return mycommontree.toString() + " " + subquery;
				} else {
					return mycommontree.toString() + "(" + subquery + ")";
				}		
			case 21: //min()
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (mycommontree.getChild(cont - 1).getType() != 47 && mycommontree.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (children == 0){
					return mycommontree.toString() + " " + subquery;
				} else {
					return mycommontree.toString() + "(" + subquery + ")";
				}		
			case 22: //coalesce()
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (mycommontree.getChild(cont - 1).getType() != 47 && mycommontree.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (children == 0){
					return mycommontree.toString() + " " + subquery;
				} else {
					return mycommontree.toString() + "(" + subquery + ")";
				}		
			case 23: //median()
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (mycommontree.getChild(cont - 1).getType() != 47 && mycommontree.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (children == 0){
					return mycommontree.toString() + " " + subquery;
				} else {
					return mycommontree.toString() + "(" + subquery + ")";
				}		
			case 24: //stddev()
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (mycommontree.getChild(cont - 1).getType() != 47 && mycommontree.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (children == 0){
					return mycommontree.toString() + " " + subquery;
				} else {
					return mycommontree.toString() + "(" + subquery + ")";
				}		
			case 25: //avedev()
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else{
						if (mycommontree.getChild(cont - 1).getType() != 47 && mycommontree.getChild(cont - 1).getType() != 46){							
							subquery = subquery + ", ";
						}
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (children == 0){
					return mycommontree.toString() + " " + subquery;
				} else {
					return mycommontree.toString() + "(" + subquery + ")";
				}		
			case 26: //count()
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						coma = true;
					} else {
						if (mycommontree.getChild(cont - 1).getType() != 47 && mycommontree.getChild(cont - 1).getType() != 46){
							subquery = subquery + ", ";
						}
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (mycommontree.parent.getType() != 201){
					if (subquery == ""){
						subquery = "*";
					}
				}

				if (subquery == ""){
					return mycommontree.toString() + " " + subquery;
				} else {
					return mycommontree.toString() + "(" + subquery + ")";
				}
			case 28: //case
				children = mycommontree.getChildCount();
				subquery = "";
				if (children == 3){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					subquery = epltree1.toQuery() + "then " + epltree2.toQuery() + " else " + epltree3.toQuery();
				}				
				return "=" + mycommontree.toString() + " when " + subquery + " end";
			case 41: //ON
				return " on ";
			case 47: //All
				return mycommontree.toString() + " ";
			case 46: //distinct
				return mycommontree.toString() + " ";
			case 48: //any
				return mycommontree.toString() + " ";
			case 59: //RSTREAM
				return mycommontree.toString() + " ";
			case 61: //IRSTREAM
				return mycommontree.toString() + " ";
			case 60: //ISTREAM
				return mycommontree.toString() + " ";
			case 69: //PREVIOUS
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						if (!types.contains(mycommontree.getChild(cont).getType())){
							coma = true;
						}
					} else {
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return mycommontree.toString() + "(" + subquery + ")";
			case 70: //PREVIOUSTAIL
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						if (!types.contains(mycommontree.getChild(cont).getType())){
							coma = true;
						}
					} else {
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return mycommontree.toString() + "(" + subquery + ")";
			case 71: //PREVIOUSCOUNT
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						if (!types.contains(mycommontree.getChild(cont).getType())){
							coma = true;
						}
					} else {
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return mycommontree.toString() + "(" + subquery + ")";
			case 72: //PREVIOUSWINDOW
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						if (!types.contains(mycommontree.getChild(cont).getType())){
							coma = true;
						}
					} else {
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return mycommontree.toString() + "(" + subquery + ")";
			case 73: //PRIOR
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						if (!types.contains(mycommontree.getChild(cont).getType())){
							coma = true;
						}
					} else {
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return mycommontree.toString() + "(" + subquery + ")";
			case 77: //instanceof()
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						if (!types.contains(mycommontree.getChild(cont).getType())){
							coma = true;
						}
					} else {
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return mycommontree.toString() + "(" + subquery + ")";
			case 79: //cast()
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (coma == false){
						if (!types.contains(mycommontree.getChild(cont).getType())){
							coma = true;
						}
					} else {
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return mycommontree.toString() + "(" + subquery + ")";
			case 106: //BOOLEAN_TRUE
				return " true";
			case 107: //BOOLEAN_FALSE
				return " false";
			case 159: //WHERE_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				if (children != 0){					
					for (cont = 0; cont < children; cont++){
						if (coma == false){
							coma = true;
						} else {
							subquery = subquery + ", ";
						}
						ctree1 = (CommonTree) mycommontree.getChild(cont);
						epltree1 = new EPLTree(ctree1);
						subquery = subquery + epltree1.toQuery();
					}
					return " where " + subquery;
				} else{
					return "";
				}				
			case 169: //EVAL_NOTEQUALS_GROUP_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				boolean done = false;
				for (cont = 0; cont < children; cont++){
					if (mycommontree.getChild(cont).getType() != 183){
						ctree1 = (CommonTree) mycommontree.getChild(cont);
						epltree1 = new EPLTree(ctree1);
						ctree2 = (CommonTree) mycommontree.getChild(cont + 1);
						epltree2 = new EPLTree(ctree2);
						subquery = subquery + epltree1.toQuery() + "(" + epltree2.toQuery() + ")";
						done = true;
					} else if (mycommontree.getChild(cont).getType() == 183 && done){
						;
					} else if (mycommontree.getChild(cont).getType() == 183 && done == false){
						ctree1 = (CommonTree) mycommontree.getChild(cont);
						epltree1 = new EPLTree(ctree1);
						subquery = subquery + epltree1.toQuery() + " != ";
					}
				}
				return subquery;
			case 173: //SELECTION_STREAM
				children = mycommontree.getChildCount();
				subquery = "";
				if (children == 1){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery() + ".*";
				}
				else{
					for (cont = 0; cont < children; cont++){
						ctree1 = (CommonTree) mycommontree.getChild(cont);
						epltree1 = new EPLTree(ctree1);
						subquery = subquery + "." + epltree1.toQuery();
					}
				}
				return subquery;
			case 174: //STREAM_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					CommonTree kid = (CommonTree) mycommontree.getChild(cont);
					if (kid.token.getType() == 325){
						ctree1 = (CommonTree) mycommontree.getChild(cont);
						epltree1 = new EPLTree(ctree1);
						subquery = subquery + " " + epltree1.toQuery();
					} else{
						ctree1 = (CommonTree) mycommontree.getChild(cont);
						epltree1 = new EPLTree(ctree1);
						subquery = subquery + epltree1.toQuery(); 
					}
				}
				return subquery;
			case 157: //PATTERN_INCL_EXPR
				children = mycommontree.getChildCount();
				for (cont = 0; cont < children; cont++){
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery(); 
				}
				return "pattern [" + subquery + "]";
			case 14: //EVERY_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				if (mycommontree.parent.toString().equals("PATTERN_INCL_EXPR")){
					subquery = "(" + epltree1.toQuery() + ")";
				} else {
					subquery = epltree1.toQuery();
				}
				return "every " + subquery;
			case 84: //Variable
				return mycommontree.toString();
			case 154: //GUARD_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (i = 0; i < children; i++){
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
					coma = false;
					if (i == 0){
						subquery = subquery + " where ";
					}
					if (i == 1){
						subquery = subquery + ":";
					}
					if (i == 2){
						subquery = subquery + "(";
					}
					if (i < children - 1){						
						if (mycommontree.getChild(i).getType() == 205 && mycommontree.getChild(i + 1).getType() == 205){
							subquery = subquery + ", ";
						}
					}
				}
				return subquery + ")";
			case 136: //PATTERN_FILTER_EXPR
				children = mycommontree.getChildCount();
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					if (ctree2.getType() != 153){
						return epltree1.toQuery() + "(" + epltree2.toQuery() + ")";
					} else {
						return epltree1.toQuery() + "=" + epltree2.toQuery();
					}					
				}
				if (children == 3){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					return epltree1.toQuery() + "=" + epltree2.toQuery() + " (" + epltree3.toQuery() + ")";
				}
				if (children == 4){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					ctree4 = (CommonTree) mycommontree.getChild(3);
					epltree4 = new EPLTree(ctree4);
					return epltree1.toQuery() + "=" + epltree2.toQuery() + " (" + epltree3.toQuery() + ", " + epltree4.toQuery() + ")";
				}
			case 137: //PATTERN_NOT_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				if (ctree1.getType() == 136){
					return "not " + epltree1.toQuery();
				} else {
					return "not (" + epltree1.toQuery() + ")";
				}
			case 155: //OBSERVER_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				ctree3 = (CommonTree) mycommontree.getChild(2);
				epltree3 = new EPLTree(ctree3);
				return epltree1.toQuery() + ":" + epltree2.toQuery() + "(" + epltree3.toQuery() + ")";
			case 156: //VIEW_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				coma = false;
				
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return "." + epltree1.toQuery() + ":" + epltree2.toQuery() + "()";
				}
				if (children > 2){
					for (i = 2; i < children; i++){
						if (coma == false){
							ctree1 = (CommonTree) mycommontree.getChild(i);
							epltree1 = new EPLTree(ctree1);
							subquery = epltree1.toQuery();
							coma = true;
						} else{
							ctree1 = (CommonTree) mycommontree.getChild(i);
							epltree1 = new EPLTree(ctree1);
							subquery = subquery + ", " + epltree1.toQuery();
						}
					}
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return "." + epltree1.toQuery() + ":" + epltree2.toQuery() + "(" + subquery + ")";
				}
			case 160: //HAVING_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return " having " + epltree1.toQuery();
			case 162: //EVAL_AND_EXPR
				children = mycommontree.getChildCount();
				for (i = 0; i < children; i++){
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					if (i < children - 1){
						subquery += epltree1.toQuery() + " and ";
					} else {
						subquery += epltree1.toQuery();
					}
				}
				
				return subquery;
			case 163: //EVAL_OR_EXPR 
				children = mycommontree.getChildCount();
				for (i = 0; i < children; i++){
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					if (i < children - 1){
						subquery += epltree1.toQuery() + " or ";
					} else {
						subquery += epltree1.toQuery();
					}
				}
				
				return subquery;
			case 164: //EVAL_EQUALS_EXPR
				children = mycommontree.getChildCount();
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return epltree1.toQuery() + " = " + epltree2.toQuery();
				} else{
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					return epltree1.toQuery() + " = " + epltree2.toQuery() + epltree3.toQuery();
				}
			case 165: //EVAL_NOTEQUALS_EXPR
				children = mycommontree.getChildCount();
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return epltree1.toQuery() + " != " + epltree2.toQuery();
				} else{
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					return epltree1.toQuery() + " != " + epltree2.toQuery() + epltree3.toQuery();
				}
			case 203: //DOT_EXPR
				children = mycommontree.getChildCount();
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return epltree1.toQuery() + "." + epltree2.toQuery();
				}
			case 216: //NOT_BETWEEN
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				ctree3 = (CommonTree) mycommontree.getChild(2);
				epltree3 = new EPLTree(ctree3);
				return epltree1.toQuery() + " not between " + epltree2.toQuery() + " and " + epltree3.toQuery();
			case 222: //WILDCARD_SELECT *
				return mycommontree.toString();
			case 224: //IN_RANGE
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				ctree3 = (CommonTree) mycommontree.getChild(2);
				epltree3 = new EPLTree(ctree3);
				ctree4 = (CommonTree) mycommontree.getChild(3);
				epltree4 = new EPLTree(ctree4);
				ctree5 = (CommonTree) mycommontree.getChild(4);
				epltree5 = new EPLTree(ctree5);
				return epltree1.toQuery() + " in " + epltree2.toQuery() + epltree3.toQuery() + ":" + epltree4.toQuery() + epltree5.toQuery();
			case 227: //SUBSELECT_GROUP_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (mycommontree.getChild(cont).getType() == 172 && coma == false){
						coma = true;
					} else if (mycommontree.getChild(cont).getType() == 172 && coma == true){
						subquery = subquery + ", ";
					} else if (mycommontree.getChild(cont).getType() == 174){
						subquery = subquery + " from ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return "(select " + subquery + ")";				
			case 228: //EXISTS_SUBSELECT_EXPR				
				children = mycommontree.getChildCount();
				subquery = "";
				
				if (mycommontree.getChild(0).getType() == 183){
					for (i = 1; i < children; i++){
						ctree1 = (CommonTree) mycommontree.getChild(i);
						epltree1 = new EPLTree(ctree1);
						subquery = subquery + epltree1.toQuery();
					}
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " exists (" + subquery + ")";						
				} else {
					for (i = 0; i < children; i++){
						if (mycommontree.getChild(i).getType() == 172 && coma == false){
							coma = true;
						} else if (mycommontree.getChild(i).getType() == 172 && coma == true){
							subquery = subquery + ", ";
						} else if (mycommontree.getChild(i).getType() == 174){
							subquery = subquery + " from ";
						}
						ctree1 = (CommonTree) mycommontree.getChild(i);
						epltree1 = new EPLTree(ctree1);
						subquery = subquery + epltree1.toQuery();
					}
					return "exists (select " + subquery + ")";

				}
			case 226: //SUBSELECT_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (mycommontree.getChild(cont).getType() == 172 && coma == false){
						coma = true;
					} else if (mycommontree.getChild(cont).getType() == 172 && coma == true){
						subquery = subquery + ", ";
					} else if (mycommontree.getChild(cont).getType() == 174){
						subquery = subquery + " from ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (mycommontree.parent.getType() == 228){
					return subquery;
				} else{					
					return "(select " + subquery + ")";
				}
			case 229: //IN_SUBSELECT_EXPR
				String variable = "";
				cont = 0;
				if (mycommontree.getChild(cont).getType() == 183){
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					variable = epltree1.toQuery();
					cont++;
				}
				ctree1 = (CommonTree) mycommontree.getChild(cont);
				epltree1 = new EPLTree(ctree1);
				return variable + " in (" + epltree1.toQuery() + ")";
			case 231: //IN_SUBSELECT_QUERY_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (mycommontree.getChild(cont).getType() == 172 && coma == false){
						coma = true;
					} else if (mycommontree.getChild(cont).getType() == 172 && coma == true){
						subquery = subquery + ", ";
					} else if (mycommontree.getChild(cont).getType() == 174){
						subquery = subquery + " from ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (mycommontree.parent.getType() == 228){
					return subquery;
				} else{					
					return "select " + subquery;
				}
			case 230: //NOT_IN_SUBSELECT_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " not in (" + epltree2.toQuery() + ")";
			case 237: //CREATE_WINDOW_EXPR
				subquery = "";
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					if (ctree1.getType() == 156){ //VIEW_EXPR
						subquery = subquery + epltree1.toQuery() + " as ";
					}
					else {
						subquery = subquery + epltree1.toQuery();
					}
				}
				return "create window " + subquery;
			case 238: //CREATE_WINDOW_SELECT_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return "select " + epltree1.toQuery() + " from ";
			case 239: //ON_EXPR
				subquery = "";
				children = mycommontree.getChildCount();
				for (i = 0; i < children; i++){
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
					if (mycommontree.getChild(i).getType() == 240 && mycommontree.getChild(i).getChild(0).getType() == 157){
						subquery = subquery + " ";
					}
				}
				return "on " + subquery;
			case 240: //ON_STREAM
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return epltree1.toQuery();
			case 242: //ON_SELECT_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (mycommontree.getChild(cont).getType() == 172 && coma == false){
						coma = true;
					} else if (mycommontree.getChild(cont).getType() == 172 && coma == true){
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				if (mycommontree.parent.children.toString().contains("STREAM_EXPR")){
					return " select " + subquery + " from ";
				} else{					
					return " select " + subquery;
				}
			case 243: //ON_UPDATE_EXPR
				subquery = "";
				children = mycommontree.getChildCount();
				for (i = 0; i < children; i++){
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return "update " + subquery;
			case 244: //ON_MERGE_EXPR
				subquery = "";
				children = mycommontree.getChildCount();
				if (children == 4){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					ctree4 = (CommonTree) mycommontree.getChild(3);
					epltree4 = new EPLTree(ctree4);
					subquery = epltree1.toQuery() + epltree4.toQuery() + " when " + epltree2.toQuery() + " when " + epltree3.toQuery();
				}
				if (children == 3){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					subquery = epltree1.toQuery() + epltree3.toQuery() + " when " + epltree2.toQuery();
				}
				return " merge " + subquery;
			case 247: //ON_EXPR_FROM
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return " from " + epltree1.toQuery();
			case 256: //CREATE_COL_TYPE_LIST
				subquery = "";
				children = mycommontree.getChildCount();
				for (i = 0; i < children; i++){
					if (coma == false){
						coma = true;
					} else {
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}
				return "(" + subquery + ")";
			case 257: //CREATE_COL_TYPE
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " " + epltree2.toQuery();
			case 264: //WINDOW_AGGREG
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return "window" + epltree1.toQuery();
			case 265: //ACCESS_AGG
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return "(" + epltree1.toQuery() + ")";
			case 267: //ON_SET_EXPR_ITEM
				subquery = "";
				children = mycommontree.getChildCount();
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					subquery = epltree1.toQuery() + " = " + epltree2.toQuery();
				}
				else {
					for (i = 0; i < children; i++){
						ctree1 = (CommonTree) mycommontree.getChild(i);
						epltree1 = new EPLTree(ctree1);
						subquery = subquery + epltree1.toQuery();
					}
				}
				return "set " + subquery;
			case 268: //CREATE_SCHEMA_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return "create schema " + epltree1.toQuery();
			case 270: //CREATE_SCHEMA_DEF
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " as " + epltree2.toQuery();
			case 272: //MERGE_UNM
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return "not matched then " + epltree1.toQuery();
			case 273: //MERGE_MAT
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return "matched then " + epltree1.toQuery();
			case 274: //MERGE_UPD
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return "update " + epltree1.toQuery();
			case 275: //MERGE_INS
				subquery = "";
				children = mycommontree.getChildCount();
				for (i = 0; i < children; i++){
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					if (coma == false){
						coma = true;
					} else{
						subquery = subquery + ", ";
					}
					subquery = subquery + epltree1.toQuery();
				}
				return "insert select " + subquery;
			case 336: //STAR* (multiplicación)
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery();
			case 342: //PLUS+
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery();
			case 352: //MINUS-
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery();
			case 353: //DIV /
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery();
			case 354: //MOD %
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery();
			case 339: //>
				children = mycommontree.getChildCount();
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery();
				} else{
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery() + epltree3.toQuery();
				}
			case 338: //LT <
				children = mycommontree.getChildCount();
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery();
				} else{
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery() + epltree3.toQuery();
				}
			case 350: //<=
				children = mycommontree.getChildCount();
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery();
				} else{
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery() + epltree3.toQuery();
				}
			case 349: //>=
				children = mycommontree.getChildCount();
				if (children == 2){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery();
				} else{
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					ctree3 = (CommonTree) mycommontree.getChild(2);
					epltree3 = new EPLTree(ctree3);
					return epltree1.toQuery() + " " + mycommontree.toString() + " " + epltree2.toQuery() + epltree3.toQuery();
				}
			case 180: //GROUP_BY_EXPR
				children = mycommontree.getChildCount();
				subquery = "";
				for (cont = 0; cont < children; cont++){
					if (mycommontree.getChild(cont).getType() == 172 && coma == false){
						coma = true;
					} else if (mycommontree.getChild(cont).getType() == 172 && coma == true){
						subquery = subquery + ", ";
					} 
					if (mycommontree.getChild(cont).getType() == 183 && coma == false){
						coma = true;
					}
					else if (mycommontree.getChild(cont).getType() == 183 && coma == true){
						subquery = subquery + ", ";
					}
					ctree1 = (CommonTree) mycommontree.getChild(cont);
					epltree1 = new EPLTree(ctree1);
					subquery = subquery + epltree1.toQuery();
				}						
				return " group by " + subquery;
			case 186: //EVENT_PROP_INDEXED
				children = mycommontree.getChildCount();
				if (children > 1){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					ctree2 = (CommonTree) mycommontree.getChild(1);
					epltree2 = new EPLTree(ctree2);
					return epltree1.toQuery() + "[" + epltree2.toQuery() + "]";
				}
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return epltree1.toQuery();
			case 205: //TIME_PERIOD
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return epltree1.toQuery();
			case 211: //HOUR_PART
				if (mycommontree.token.getText().equals("MILLISECOND_PART")){			
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " milliseconds";
				}
				if (mycommontree.token.getText().equals("SECOND_PART")){			
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " seconds";
				}
				if (mycommontree.token.getText().equals("MINUTE_PART")){	
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " minutes";
				}
				if (mycommontree.token.getText().equals("HOUR_PART")){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " hours";
				}
				if (mycommontree.token.getText().equals("DAY_PART")){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " days";
				}
			case 214: //MILLISECOND_PART
				if (mycommontree.token.getText().equals("MILLISECOND_PART")){			
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " milliseconds";
				}
				if (mycommontree.token.getText().equals("SECOND_PART")){			
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " seconds";
				}
				if (mycommontree.token.getText().equals("MINUTE_PART")){	
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " minutes";
				}
				if (mycommontree.token.getText().equals("HOUR_PART")){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " hours";
				}
				if (mycommontree.token.getText().equals("DAY_PART")){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " days";
				}
			case 213: //SECOND_PART
				if (mycommontree.token.getText().equals("MILLISECOND_PART")){			
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " milliseconds";
				}
				if (mycommontree.token.getText().equals("SECOND_PART")){			
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " seconds";
				}
				if (mycommontree.token.getText().equals("MINUTE_PART")){	
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " minutes";
				}
				if (mycommontree.token.getText().equals("HOUR_PART")){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " hours";
				}
				if (mycommontree.token.getText().equals("DAY_PART")){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " days";
				}
			case 210: //DAY_PART
				if (mycommontree.token.getText().equals("MILLISECOND_PART")){			
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " milliseconds";
				}
				if (mycommontree.token.getText().equals("SECOND_PART")){			
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " seconds";
				}
				if (mycommontree.token.getText().equals("MINUTE_PART")){	
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " minutes";
				}
				if (mycommontree.token.getText().equals("HOUR_PART")){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " hours";
				}
				if (mycommontree.token.getText().equals("DAY_PART")){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " days";
				}
			case 212: //MINUTE_PART
				if (mycommontree.token.getText().equals("MILLISECOND_PART")){			
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " milliseconds";
				}
				if (mycommontree.token.getText().equals("SECOND_PART")){			
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " seconds";
				}
				if (mycommontree.token.getText().equals("MINUTE_PART")){	
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " minutes";
				}
				if (mycommontree.token.getText().equals("HOUR_PART")){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " hours";
				}
				if (mycommontree.token.getText().equals("DAY_PART")){
					ctree1 = (CommonTree) mycommontree.getChild(0);
					epltree1 = new EPLTree(ctree1);
					return epltree1.toQuery() + " days";
				}
			case 191: //TIMEPERIOD_LIMIT_EXPR
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return " output " + epltree1.toQuery() + " every " + epltree2.toQuery();
			case 194:
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				ctree2 = (CommonTree) mycommontree.getChild(1);
				epltree2 = new EPLTree(ctree2);
				return "Math." + epltree1.toQuery() + "(" + epltree2.toQuery() + ")";
			case 198: //EXPRCOL
				children = mycommontree.getChildCount();
				subsub = "";
				for (i = 0; i < children; i++){
					ctree1 = (CommonTree) mycommontree.getChild(i);
					epltree1 = new EPLTree(ctree1);
					if (i != children - 1){
						subsub = subsub + epltree1.toQuery() + ", ";
					} else{
						subsub = subsub + epltree1.toQuery();
					}
				}
				return "(" + subsub + ")";
			case 276: //Variables
				return mycommontree.toString();
			case 277: //Text
				for (i = 0; i < Operators.length; i ++){
					if (Operators[i].equals(mycommontree.token.getText())){
						ctree1 = (CommonTree) mycommontree.getChild(0);
						epltree1 = new EPLTree(ctree1);
						ctree2 = (CommonTree) mycommontree.getChild(1);
						epltree2 = new EPLTree(ctree2);
						return epltree1.toQuery() + " " + mycommontree.token.getText() + " " + epltree2.toQuery();
					}
				}
				return mycommontree.toString();
			case 300: //INT_TYPE
				subsub = "";
				if (!nocommastokens.contains(mycommontree.parent.getType())){
					int size = mycommontree.parent.getChildCount(); 
					if (size > 1 && mycommontree.childIndex != size - 1){
						subsub = ", ";
					}
				} 
				return mycommontree.toString() + subsub;
			case 301: //LONG_TYPE
				subsub = "";
				if (!nocommastokens.contains(mycommontree.parent.getType())){
					int size = mycommontree.parent.getChildCount(); 
					if (size > 1 && mycommontree.childIndex != size - 1){
						subsub = ", ";
					}
				}
				return mycommontree.toString() + subsub;
			case 302: //FLOAT_TYPE
				subsub = "";
				if (!nocommastokens.contains(mycommontree.parent.getType())){
					int size = mycommontree.parent.getChildCount(); 
					if (size > 1 && mycommontree.childIndex != size - 1){
						subsub = ", ";
					}
				}
				return mycommontree.toString() + subsub;
			case 303: //DOUBLE_TYPE
				subsub = "";
				if (!nocommastokens.contains(mycommontree.parent.getType())){
					int size = mycommontree.parent.getChildCount(); 
					if (size > 1 && mycommontree.childIndex != size - 1){
						subsub = ", ";
					}
				} 
				return mycommontree.toString() + subsub;
			case 306: //NULL_TYPE
				return mycommontree.toString() + " ";
			case 325: //IDENT
				return mycommontree.toString();
			case 331: //GOES
				ctree1 = (CommonTree) mycommontree.getChild(0);
				epltree1 = new EPLTree(ctree1);
				return epltree1.toQuery() + " => ";
			case 333: //COMMA
				return mycommontree.toString() + " ";
			case 307: //NUM_DOUBLE
				return mycommontree.toString();
			case 361: //NUM_FLOAT
				return mycommontree.toString();
			case 355: //NUM_INT
				return mycommontree.toString();
			case 360: //NUM_LONG
				return mycommontree.toString();
			case 294: //GOPOUTITM
				return mycommontree.toString();
			case 327: //) LPAREN
				return ")";
			case 326: //( RPAREN
				if (mycommontree.parent.getChildren().toString().contains(")")){
					return "(";
				} else {
					return "";
				}
			default:
				return mycommontree.toString();
			}
		}
} 
