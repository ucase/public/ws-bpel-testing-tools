package subcommands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.MatchResult;

import org.antlr.runtime.tree.EPLTree;

import subcommands.ApplySubcommand;

public class ApplyAllSubcommand extends AbstractSubcommand{

	private static final int APPLY_NUM_ARGS = 5;
	private static final String USAGE =
		"Applies the specified operator (1-based index or case-insensitive name) on\n" +
		"its n-th operand (1-based), using the extra attribute (1-based) to customize\n" +
		"its behaviour. The resulting EPL process definition is dumped to stdout.\n" +
		"If the selected operand does not exist, a warning will be reported to the\n" +
		"standard error output stream. There is an option of coverage (default no applied)," +
		"to apply it: coverage = true";

	public ApplyAllSubcommand() {
		super(APPLY_NUM_ARGS, APPLY_NUM_ARGS, "applyall", "PathToOriginalFolder PathToEPLFile QueryLocationFolder PathToAnalyzeFile coverage", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {
		final List<String> nonOptionArgs = getNonOptionArgs();
		applyAll(nonOptionArgs.get(0), nonOptionArgs.get(1), nonOptionArgs.get(2), nonOptionArgs.get(3), nonOptionArgs.get(4));
	}

	private void applyAll(String ProgramFolder, String pathEPLFile, String QueryLocationFolder, String pathToAnalyzeFile, String coverageOption) throws Exception {
	
		System.out.println("The command apply starts...");
		
		String line = "";
		FileReader analyzef = new FileReader(new File(pathToAnalyzeFile));
    	BufferedReader bf = new BufferedReader(analyzef);
    	List<List<String>> mutants = new ArrayList<List<String>>();
    	boolean coverage = Boolean.parseBoolean(coverageOption);
    	
    	while ((line = bf.readLine()) != null)
    	{
    		List<String> mutant = new ArrayList<String>();
    		Scanner sc = new Scanner(line);
    		sc.findInLine("(\\w+) (\\d+) (\\d+)");
    		MatchResult result = sc.match();
    		for (int i = 1; i <= result.groupCount(); i++)
    		{
    			mutant.add(result.group(i));
    		}
    		mutants.add(mutant);
    		sc.close();
    	}
    	
    	bf.close();
    	
    	for (List<String> m : mutants){
    		String operator = m.get(0);
    		int operandIndex = Integer.parseInt(m.get(1));
    		int attribute = Integer.parseInt(m.get(2));
    		if (operandIndex != 0){
    			int operanditer = 0;
    			while (operanditer < operandIndex){
    				int attributeiter = 0;
    				while (attributeiter < attribute){
    					EPLTree.countmut = 0;
    					ApplySubcommand.apply(ProgramFolder, pathEPLFile, QueryLocationFolder, operator.toLowerCase(), operanditer + 1, attributeiter + 1, coverage);
    					attributeiter++;
    				}
    				operanditer++;
    			}
    		}    		
    	}
	}

}
