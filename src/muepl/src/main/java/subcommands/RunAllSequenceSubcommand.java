package subcommands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class RunAllSequenceSubcommand extends ExecutionSubcommand{
	
	private static final String USAGE =
		"Applies the specified operator (1-based index or case-insensitive name) on\n" +
		"its n-th operand (1-based), using the extra attribute (1-based) to customize\n" +
		"its behaviour. The resulting EPL process definition is dumped to stdout.\n" +
		"If the selected operand does not exist, a warning will be reported to the\n" +
		"standard error output stream.";
	
	public RunAllSequenceSubcommand() {
		super(5, 5, "runallsequence", "PathToOriginalFolder PathToOutputsFile \"executioncommand\" TestCaseFile PathToFileWithMutantPaths", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {
		final List<String> nonOptionArgs = getNonOptionArgs();
		final String pathToOriginal = nonOptionArgs.get(0);
		final String OutputsFile = nonOptionArgs.get(1);
		final String executioncommand = nonOptionArgs.get(2);
		final String TestCaseFile = nonOptionArgs.get(3);
		final String EPLpathFile = nonOptionArgs.get(4);
		runallsequence (pathToOriginal, OutputsFile, executioncommand, TestCaseFile, EPLpathFile);		
	}

	static void runallsequence(String pathToOriginal, String OutputsFile, String executioncommand, String TestCaseFile, String ePLpathFile) throws Exception {
		
		String line = "";
		FileReader eplfolder = new FileReader(new File(ePLpathFile));
    	BufferedReader bf = new BufferedReader(eplfolder);
    	List<String> mutanteplfolders = new ArrayList<String>();
    	File OutputFile = new File(OutputsFile);
    	if (OutputFile.exists()){
        	OutputFile.delete();
    	}
    	
    	while ((line = bf.readLine()) != null)
    	{
    		mutanteplfolders.add(line);
    	}
    	bf.close();
    	
    	Calendar cal2 = Calendar.getInstance();
    	System.out.println("Start time: "+cal2.get(Calendar.HOUR)+":"+cal2.get(Calendar.MINUTE)+":"+cal2.get(Calendar.SECOND)+":"+cal2.get(Calendar.MILLISECOND));
    	RunSubcommand.SetSequence(true);
    	RunSubcommand.run(pathToOriginal, OutputsFile, executioncommand, TestCaseFile, mutanteplfolders);
    	Calendar cal3 = Calendar.getInstance();
    	System.out.println("End time: "+cal3.get(Calendar.HOUR)+":"+cal3.get(Calendar.MINUTE)+":"+cal3.get(Calendar.SECOND)+":"+cal3.get(Calendar.MILLISECOND));
	
	}

}
