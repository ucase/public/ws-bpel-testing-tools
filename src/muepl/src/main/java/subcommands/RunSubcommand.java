package subcommands;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;

import subcommands.ExecutionSubcommand;
import mutants.parallel.*;

/**
 * <p>
 * Implements the 'run' subcommand.
 * </p>
 * 
 * @author Lorena Gutiérrez Madroñal
 */

public class RunSubcommand extends ExecutionSubcommand{

	static final boolean DEFAULT_LIST_FORMAT = false;
	private List<List<String>> fResults = new ArrayList<List<String>>();
	long time_start, time_end;
	static boolean original = true;
	static boolean sequence = false;
	
	private static final String USAGE =
		"Applies the specified operator (1-based index or case-insensitive name) on\n" +
		"its n-th operand (1-based), using the extra attribute (1-based) to customize\n" +
		"its behaviour. The resulting EPL process definition is dumped to stdout.\n" +
		"If the selected operand does not exist, a warning will be reported to the\n" +
		"standard error output stream.";

	public RunSubcommand() {
		super(5, 5, "run", "PathToOriginalFolder PathToOutputsFile \"executioncommand\" TestCaseFile mutantFolder1...", USAGE);
	}
	
	@Override
	protected void runCommand() throws Exception {
		final List<String> nonOptionArgs = getNonOptionArgs();
		final String pathToOriginal = nonOptionArgs.get(0);
		final String OutputsFile = nonOptionArgs.get(1);
		final String executioncommand = nonOptionArgs.get(2);
		final String TestCaseFile = nonOptionArgs.get(3);
		run (pathToOriginal, OutputsFile, executioncommand, TestCaseFile, nonOptionArgs);
	}

	public static void SetSequence(boolean seq){
		sequence = seq;
	}
	
	public void run(String pathToOriginal, String OutputsFile, String executioncommand, String TestCaseFile, String... pathToEPL) throws Exception {
		run(pathToOriginal, OutputsFile, executioncommand, TestCaseFile, Arrays.asList(pathToEPL));
	}
	
	/**
	 * Runs one or several mutant EPL set of queries against the original EPL set of queries
	 * And reports the results.
	 * 
	 * @param pathcompile: folder with the compilation files
	 * @param pathToEPL: original EPL file, with a set of EPL queries typed, to be run.
	 * @param eplFiles: List of paths to the mutants EPL files which should be tested.
	 * @throws Exception There was a problem while starting the Esper engine or running the queries.
	 */
	
	public static void run(String pathToOriginal, String OutputsFile, String executioncommand, String TestCaseFile, List<String> mutanteplfolders) throws Exception {
		MutantFactory.SetEPLFolder(pathToOriginal);
		if (sequence){
			SequenceProcess(OutputsFile, executioncommand, TestCaseFile, mutanteplfolders);
		} else {			
			parallelProcess(OutputsFile, executioncommand, TestCaseFile, mutanteplfolders);
		}
	}

	
	private static void SequenceProcess(String outputsFile,
			String executioncommand, String testCaseFile,
			List<String> eplFiles) {
		for (String file : eplFiles){
			
			Map <String,List<String>> OutputEvents = new HashMap<String, List<String>>();
			
			try {
				
				List<String> EPLRunCommand = new ArrayList<String>();
				
				String[] inputParam = executioncommand.split(" ");		
				for (int i = 0; i < inputParam.length; i++){
					EPLRunCommand.add(inputParam[i]);
				}
				EPLRunCommand.add(testCaseFile);
				
				ProcessBuilder aux = new ProcessBuilder(EPLRunCommand).directory(new File(file));
				aux.redirectErrorStream(true);
				
				long time_start, time_end = 0;
				time_start = System.currentTimeMillis();
				Process a = aux.start();
				
				BufferedReader reader = new BufferedReader(new InputStreamReader(a.getInputStream()));
				String readerEvent = null;
				
				// Volcamos en un Map <Key, List<String>> los eventos de salida según el número de la consulta
				while((readerEvent = reader.readLine()) != null) {
					int index = readerEvent.indexOf(" ");
					String key = readerEvent.substring(0, index);
					boolean number = true;
					
					try {
						int k = Integer.parseInt(key);
					} catch (NumberFormatException e) {
						number = false;
					}
					
					if (number) {
						String event = readerEvent.substring(index);
						
						if (OutputEvents.containsKey(key)) {
							List<String> currentValues = OutputEvents.get(key);
							currentValues.add(event);
							OutputEvents.put(key, currentValues);
						} else {
							List<String> emptyValues = new ArrayList<String>();
							emptyValues.add(event);
							OutputEvents.put(key, emptyValues);
						}
					}
				}
				a.waitFor();
				a.destroy();
				time_end = System.currentTimeMillis();
				long time = (time_end - time_start) * 1000000; //Seconds to nanoseconds x 1000000
				
				String value = MutantThread.GetName(file);
				
				// En el fichero OutputsFile que se pasa como parámetro en run, se escriben los paths de los ficheros
				// donde se van a volcar los eventos de salida, Key + OutputEventsFileName.events
				FileWriter OutputpathFile = new FileWriter(outputsFile, true);
				BufferedWriter bw = new BufferedWriter(OutputpathFile);
				
				// Se vuelca el contenido de cada clave, una lista de String, en el fichero cuyo nombre tiene la clave
				for (Map.Entry<String, List<String>> mapEntry : OutputEvents.entrySet()) {
					String OutputEventsFileName;
					OutputEventsFileName = RunSubcommand.dumpFileToStream(value, mapEntry.getKey());
					
					FileWriter EventsFile = new FileWriter(OutputEventsFileName, true);
					BufferedWriter bwe = new BufferedWriter(EventsFile);
					for (String singleEntry : mapEntry.getValue()) {
						bwe.write(singleEntry + "\n");
						
					}
					bwe.close();
					EventsFile.close();
					bw.write(OutputEventsFileName + "\n");
				}
				bw.close();
				OutputpathFile.close();	
					
				//Crea un fichero con los tiempos de ejecución de cada operador
				int lastindex = outputsFile.lastIndexOf("/");
				String TimesFile = outputsFile.substring(0, lastindex) + "/ExecutionTimes.txt";
				FileWriter ExecutionTimesFile = new FileWriter(TimesFile, true);
				BufferedWriter bwf = new BufferedWriter(ExecutionTimesFile);
				String line = "/tmp/outputs/" + value + ".event T " + time;
				bwf.write(line + "\n");
				bwf.close();
				ExecutionTimesFile.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}

	private static void parallelProcess(String OutputsFile, String executioncommand, String TestCaseFile, List<String> eplFiles) throws IOException, InterruptedException, BrokenBarrierException {
		for (String file : eplFiles){
			MutantFactory.addProccess(OutputsFile, file, original);
			original = false;
		}
		MutantFactory.runParallel(OutputsFile, executioncommand, TestCaseFile);
	}

	/**
	 * Returns a list of all the results produced up to now by this subcommand.
	 */
	public List<List<String>> getResults() {
		return fResults;
	}


	public static String dumpFileToStream(String value, String key) throws IOException {
		
		String resultDirectory = "/tmp/outputs";
		File rdirectory = new File(resultDirectory);
		
		if (!rdirectory.exists()){
			rdirectory.mkdir();
		}
		
		String resultfile = "/tmp/outputs/" + key + "_" + value + ".event";
				
		return resultfile;
	}

	public static void main(String[] args) throws Exception {
		ExecutionSubcommand cmd = new RunSubcommand();
		cmd.parseArgs(args);
		cmd.run();
	}
	
}