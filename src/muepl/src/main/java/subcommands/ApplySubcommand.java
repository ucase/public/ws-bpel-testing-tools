package subcommands;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.EPLTree;
import org.antlr.runtime.tree.Tree;
import com.espertech.esper.epl.generated.EsperEPL2GrammarParser;
import com.espertech.esper.epl.parse.ParseHelper;
import com.espertech.esper.epl.parse.ParseResult;
import com.espertech.esper.epl.parse.ParseRuleSelector;

import operators.Operator;
import operators.OperatorConstants;

/**
 * <p>
 * Implements the 'apply' subcommand.
 * </p>
 * 
 * @author Lorena Gutiérrez Madroñal
 */

public class ApplySubcommand extends AbstractSubcommand {

	private static final int APPLY_NUM_ARGS = 7;
	private static final String USAGE =
		"Applies the specified operator (1-based index or case-insensitive name) on\n" +
		"its n-th operand (1-based), using the extra attribute (1-based) to customize\n" +
		"its behaviour. The resulting EPL process definition is dumped to stdout.\n" +
		"If the selected operand does not exist, a warning will be reported to the\n" +
		"standard error output stream. There is an option of coverage (default no applied),\" +\n" + 
		"to apply it: coverage = true\";";

	public ApplySubcommand() {
		super(APPLY_NUM_ARGS, APPLY_NUM_ARGS, "apply", "PathToOriginalFolder PathToEPLFile QueryLocationFolder operator operandIndex attribute coverage", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {
		final List<String> nonOptionArgs = getNonOptionArgs();
		
		String def = apply(nonOptionArgs.get(0), nonOptionArgs.get(1), nonOptionArgs.get(2),
			  nonOptionArgs.get(3).toLowerCase(),
			  Integer.parseInt(nonOptionArgs.get(4)),
			  Integer.parseInt(nonOptionArgs.get(5)),
			  Boolean.parseBoolean(nonOptionArgs.get(6)));
	}

	public static String apply(final String pathToProgram, String pathToEPL, String QueryLocationFolder, String operatorID,
			int operandIndex, int attribute, boolean coverage) throws Exception {
		try {
			// First, try to interpret it as a number
			int operatorIndex = Integer.parseInt(operatorID);	    	
			return apply(pathToProgram, pathToEPL, QueryLocationFolder, operatorIndex, operandIndex, attribute, coverage);
		} catch (NumberFormatException ex) {
			// If that does not work, take it as the name of the operator
			if (OperatorConstants.getOperatorMap().containsKey(operatorID)) {
				Operator op = OperatorConstants.getOperatorMap().get(operatorID);
				return apply(pathToProgram, pathToEPL, QueryLocationFolder, op, operandIndex, attribute, coverage);
			} else {
				throw new IllegalArgumentException(String.format(
						"Operator does not exist: %s", operatorID), ex);
			}
		}
	}

	public static String apply(final String pathToProgram, String pathToEPL, String QueryLocationFolder,
			int operatorIndex, int operandIndex, int attribute, boolean coverage)
			throws Exception {
		final Map<String, Operator> operatorMap
			= OperatorConstants.getOperatorMap();

		if (operatorIndex >= 1 && operatorIndex <= operatorMap.size()) {
			// NOTE: operator indexes are 1-based, as provided from the CLI
			Operator op = getValueAt(operatorMap, operatorIndex - 1);
	    		    	
			return apply(pathToProgram, pathToEPL, QueryLocationFolder, op, operandIndex, attribute, coverage);
		} else {
			throw new IllegalArgumentException(String.format(
					"Operator does not exist: valid range is [1,%d]",
					operatorMap.size()));
		}
	}

	public static String apply(String pathToProgram, String pathToEPL, String QueryLocationFolder, Operator op,
			int operandIndex, int attribute, boolean coverage) throws Exception {
		String querybf, queryorg = "";
		
		FileReader eplProc = new FileReader(new File(
				pathToEPL));
				
    	BufferedReader bf = new BufferedReader(eplProc);
    	
    	while ((querybf = bf.readLine()) != null){
    		queryorg += querybf + " ";
	    	
	    	ParseRuleSelector eplParseRule = new ParseRuleSelector(){
				public Tree invokeParseRule(EsperEPL2GrammarParser parser) throws RecognitionException {
					EsperEPL2GrammarParser.startEPLExpressionRule_return r = parser.startEPLExpressionRule();
					return (Tree) r.getTree();
				}
			};
			
			ParseResult tree = ParseHelper.parse(queryorg, "Error", true, eplParseRule, false);
	
			Tree mepltree =  tree.getTree();
			CommonTree cepltree = (CommonTree) mepltree;
			EPLTree epltree = new EPLTree (cepltree);
			
			System.out.println(queryorg);
			//System.out.println(tree.getTree().toStringTree());
			
			epltree.apply(op, operandIndex, attribute, coverage);
			//System.out.println(epltree.mycommontree.toStringTree());

			String query = epltree.toQuery();
			System.out.println(query);
			
			epltree.QueryToFile(pathToProgram, pathToEPL, QueryLocationFolder, op, operandIndex, attribute, query);
			queryorg = "";
    	}
    	bf.close();
		return queryorg;
	}

	/**
	 * Gets the n-th value in a map. This is only useful is the map preserves
	 * the order in which the elements were inserted, such as a LinkedHashMap.
	 */
	private static <X, Y> Y getValueAt(Map<X, Y> map, int operatorIndex) {
		Iterator<X> it = map.keySet().iterator();
		while (operatorIndex > 0) {
			it.next();
			--operatorIndex;
		}
		return map.get(it.next());
	}

}