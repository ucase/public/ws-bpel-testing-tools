package subcommands;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

/**
 * Abstract base class for every subcommand supported by muepl
 * 
 * @author Lorena Gutiérrez Madroñal
 *
 */

public abstract class AbstractSubcommand implements ISubcommand{

	private static final Level DEFAULT_LOGFILE_LOGLEVEL = Level.INFO;
	private static final Level DEFAULT_CONSOLE_LOGLEVEL = Level.FATAL;

	// Name of the console appender in the bundled log4j.properties file
	private static final String CONSOLE_APPENDER = "CONSOLE";

	private static final String DEFAULT_LOGFILE = "execution.log";
	private static final String HELP_OPTION = "help";
	public static final String LOGFILE_OPTION = "logfile";
	public static final String LOGFILE_LEVEL_OPTION = "logfile-level";
	public static final String CONSOLE_LEVEL_OPTION = "console-level";

	private int fMinArgumentCount;
	private int fMaxArgumentCount;
	private String fUsage;
	private String fName;
	private String fDescription;
	private List<String> fNonOptionArgs;
	private boolean fRequestedHelp = false;
	private PrintStream fPrintStream = System.out;
	private File fLogFile = new File(DEFAULT_LOGFILE);
	private Level fLogfileLogLevel = DEFAULT_LOGFILE_LOGLEVEL;
	private Level fConsoleLogLevel = DEFAULT_CONSOLE_LOGLEVEL;

	/** If used in the constructor, it means that it can receive an unlimited
	 *  number of arguments. */
	public static final int NARGS_UNLIMITED = 0;

	/**
	 * Creates a new instance.
	 * 
	 * @param minArgumentCount
	 *            Minimum number of required non-option arguments.
	 * @param maximumArgumentCount
	 *            Maximum number of required non-options arguments (
	 *            {@link #NARGS_UNLIMITED} for unlimited).
	 * @param name
	 *            Name of the subcommand (as in 'run' or 'compare')
	 * @param usage
	 *            Usage string (without the 'mubpel subcmd' part)
	 */
	public AbstractSubcommand(int minArgumentCount, int maxArgumentCount,
			String name, String usage, String description) {
		this.fMinArgumentCount = minArgumentCount;
		this.fMaxArgumentCount = maxArgumentCount;
		this.fName = name;
		this.fUsage = usage;
		this.fDescription = description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.uca.webservices.mutants.subcommands.ISubcommand#getName()
	 */
	public final String getName() {
		return fName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.uca.webservices.mutants.subcommands.ISubcommand#getUsage()
	 */
	public final String getUsage() {
		return fUsage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.uca.webservices.mutants.subcommands.ISubcommand#getDescription()
	 */
	public final String getDescription() {
		return fDescription;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.uca.webservices.mutants.subcommands.ISubcommand#parseArgs(java.lang
	 * .String)
	 */
	public final void parseArgs(String... args) {
		OptionParser parser = createOptionParser();
		try {
			OptionSet options = parser.parse(args);
			fNonOptionArgs = options.nonOptionArguments();

			if (options.has(HELP_OPTION)) {
				this.fRequestedHelp = true;
			} else if (getNonOptionArgs().size() < fMinArgumentCount
					|| (fMaxArgumentCount > 0 && getNonOptionArgs().size() > fMaxArgumentCount)) {
				throw new IllegalArgumentException("Wrong number of arguments");
			} else {
				parseOptions(options);
			}
		} catch (OptionException ex) {
			throw new IllegalArgumentException(ex.getLocalizedMessage(), ex);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.uca.webservices.mutants.subcommands.ISubcommand#run()
	 */
	public final void run() throws Exception {
		// Set up the log file for debug messages, if needed
		final Logger rootLogger = Logger.getRootLogger();
		if (getLogFile() != null && rootLogger.getAppender("logfile") == null) {
			FileAppender appender = new FileAppender(
					new PatternLayout("%d %-5p [%t]: %m%n"), getLogFile().getCanonicalPath(), true);
			appender.setName("logfile");
			appender.setThreshold(getLogfileLogLevel());
		    rootLogger.addAppender(appender);
		}

		ConsoleAppender consoleAppender = (ConsoleAppender)rootLogger.getAppender(CONSOLE_APPENDER);
		// consoleAppender.setThreshold(getConsoleLogLevel()); DA ERROR el APPENDER devuelve null
		
		if (fRequestedHelp) {
			printHelp(createOptionParser());
		} else {
			runCommand();
		}
	}

	/**
	 * Sets the output stream to which the XML results will be dumped. The
	 * output stream will be internally wrapped with a PrintStream.
	 */
	public final void setOutputStream(OutputStream fOutputStream) {
		this.fPrintStream = new PrintStream(fOutputStream);
	}

	/**
	 * Returns the output stream to which the XML results will be dumped.
	 */
	public final PrintStream getOutputStream() {
		return fPrintStream;
	}

	/**
	 * Sets the file to which logging messages should be sent, in addition to the console.
	 * If <code>null</code> or "", messages will only be sent to the console.
	 * @param logFile File to which the logging messages should be sent.
	 */
	public final void setLogFile(File logFile) {
		this.fLogFile = logFile;
	}

	/**
	 * Returns the file to which logging messages should be sent, if set. If unset, it
	 * will return <code>null</code>.
	 */
	public final File getLogFile() {
		return fLogFile;
	}

	/**
	 * Sets the level threshold for the messages in the log file. By default, it
	 * is {@link AbstractSubcommand#DEFAUT_LOGLEVEL}.
	 *
	 * @param level
	 *            Level to be set, such as {@link Level#DEBUG},
	 *            {@link Level#INFO}, {@link Level#ERROR} or {@link Level#FATAL}.
	 */
	public final void setLogfileLogLevel(Level level) {
		this.fLogfileLogLevel = level;
	}

	/**
	 * Returns the level threshold for the messages in the log file. By default,
	 * it is {@link #DEFAULT_LOGFILE_LOGLEVEL}.
	 */
	public final Level getLogfileLogLevel() {
		return fLogfileLogLevel;
	}

	/**
	 * Changes the level threshold for the messages printed to the standard
	 * error stream. By default, it is {@link #DEFAULT_CONSOLE_LOGLEVEL}.
	 */
	public void setConsoleLogLevel(Level level) {
		this.fConsoleLogLevel = level;
	}

	/**
	 * Returns the level threshold for the messages printed to the standard
	 * error stream.
	 */
	public Level getConsoleLogLevel() {
		return fConsoleLogLevel;
	}

	/* PROTECTED METHODS */

	/**
	 * Runs the command itself. This should be called after everything has been
	 * properly configured either through {@link #parseArgsAndRun} or by
	 * manually setting the proper options.
	 */
	protected abstract void runCommand() throws Exception;

	/**
	 * Parses the options given by the user, setting various values required by
	 * the subcommand. The default implementation is empty: --help is handled in
	 * {@link #parseArgs(String...)} and {@link #run()}.
	 */
	protected void parseOptions(OptionSet options) {
		if (getLogFile() != null && !"".equals(getLogFile().getPath())) {
			setLogFile(new File((String)options.valueOf(LOGFILE_OPTION)));
		} else {
			setLogFile(null);
		}

		if (options.has(LOGFILE_LEVEL_OPTION)) {
			setLogfileLogLevel(Level.toLevel((String)options.valueOf(LOGFILE_LEVEL_OPTION)));
		}
		if (options.has(CONSOLE_LEVEL_OPTION)) {
			setConsoleLogLevel(Level.toLevel((String)options.valueOf(CONSOLE_LEVEL_OPTION)));
		}
	}

	/**
	 * Returns the option parser. Please do not forget to reuse the OptionParser
	 * created by the superclass in your subclasses!
	 */
	protected OptionParser createOptionParser() {
		OptionParser parser = new OptionParser();
		parser.accepts(HELP_OPTION, "Provides help on the subcommand");
		parser.accepts(LOGFILE_OPTION, "Redirects logs messages to a file (use '' to disable)")
			.withRequiredArg().ofType(String.class)
			.describedAs("FILE").defaultsTo(DEFAULT_LOGFILE);
		parser.accepts(LOGFILE_LEVEL_OPTION, "Sets the minimum logging level for the logfile (DEBUG, INFO, FATAL, etc.)")
			.withRequiredArg().ofType(String.class)
			.describedAs("LEVEL").defaultsTo(DEFAULT_LOGFILE_LOGLEVEL.toString());
		parser.accepts(CONSOLE_LEVEL_OPTION, "Sets the minimum logging level for the console (DEBUG, INFO, FATAL, etc.)")
			.withRequiredArg().ofType(String.class)
			.describedAs("LEVEL").defaultsTo(DEFAULT_CONSOLE_LOGLEVEL.toString());
		return parser;
	}

	/**
	 * Returns the list of all non-option arguments.
	 */
	protected final List<String> getNonOptionArgs() {
		return fNonOptionArgs;
	}

	/* PRIVATE METHODS */

	private void printHelp(OptionParser parser) throws IOException {
		System.err.println("Usage: muepl " + getName() + " " + getUsage());
		System.err.println("Description:\n");
		System.err.println(getDescription() + "\n");
		parser.printHelpOn(System.err);
	}

	
}
