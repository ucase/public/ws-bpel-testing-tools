package subcommands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public class TestCaseGenerationSubcommand extends AbstractSubcommand {

	public TestCaseGenerationSubcommand() {
		super(3, 3, "tcgenerate", "eplfolderprogram executorFileName input-parameters", USAGE);
	}

	private static final String USAGE = "Catch the values of the events while the program\n"
			+ "is running. After the execution of the command, \n"  
			+ "all the event values are saved in a file: TestCase_eplfolderprogramName_imput-parameters.values.";

	@Override
	protected void runCommand() throws Exception {
		tcgenerate(getNonOptionArgs().get(0), getNonOptionArgs().get(1), getNonOptionArgs().get(2));
	}
	
	public static void tcgenerate(String Folderpath, String executorFileName, String parameters)
			throws Exception {
		
		String pathcompile = Folderpath + "/etc";
		String outputpath = "/home/lorena/EPLcapture";
		String programName = Folderpath.substring(Folderpath.lastIndexOf("/") + 1, Folderpath.length());
		String tcfile = outputpath + "/Testcase_" + programName + "_" + parameters;
		String scfile = outputpath + "/TestSuite";
		
		//To save the path of each test case
		FileWriter suitcasefile = new FileWriter(scfile, true);
		BufferedWriter bw = new BufferedWriter(suitcasefile);
		bw.write(tcfile + "\n");
		bw.close();
		suitcasefile.close();
		
		initializeExecution(pathcompile);
		File compileDirectory = new File(pathcompile);
		String executioncommand = "";
		
		if (parameters == "null"){
			executioncommand = executorFileName;
		} else{
			
			String[] inputParam = parameters.split("-");
			String values = "";
			
			for (int i = 0; i < inputParam.length; i++){
				values = values + " " + inputParam[i];
			}
			executioncommand = executorFileName + values + " " + tcfile;
		}
		
		runSingleCompositionInFileListFormat(compileDirectory, executioncommand);
		
		System.out.println("The command TestCaseGeneration ends. All the event values are saved");
	}
	
	/**
	 * It is necessary to execute the compile files before running the EPL code.
	 * @param pathcompile: directory with the compile files
	 * @return command: Specific order to the program which will be executed
	 * @throws IOException 
	 */
	private static void initializeExecution(String pathcompile) throws IOException {
		
		String commandSet = "sh setenv.sh";
		String commandCompile = "sh compile.sh";
		String javapath = System.getenv("JAVA_HOME");
		File workDirectory = new File(pathcompile);
		
		String temporaljavapath = System.getenv().get("PATH");
		String[] environment = {"PATH=" + temporaljavapath + ":.", "JAVA_HOME=" + javapath};
		Process a = Runtime.getRuntime().exec(commandSet, environment, workDirectory);
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(a.getInputStream()));
		
		Process b = Runtime.getRuntime().exec(commandCompile, environment, workDirectory);

		stdInput = new BufferedReader(new InputStreamReader(b.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(b.getErrorStream()));

	}
	
	/**
	 * Run a EPL program in a required environment
	 * @param compileDirectory: Where the compile files exist
	 * @param eplfile: the path of the EPL program
	 * @param command: the name of the execution file and, depend on the program
	 * some arguments
	 * @return the results after the execution of the EPL program
	 * @throws IOException
	 */
	private static void runSingleCompositionInFileListFormat(
			File compileDirectory, String command) throws IOException {
		
		String s = "";
		String runcommand = "sh " + command;
		String javapath = System.getenv("JAVA_HOME");
		String[] environment = {"PATH=$PATH:.", "JAVA_HOME=" + javapath};
		List<String> results = new ArrayList<String>();
		Process a = Runtime.getRuntime().exec(runcommand, environment, compileDirectory);
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(a.getInputStream()));
        
		while ((s = stdInput.readLine()) != null) {
            System.out.println(s);
            results.add(s);
        }
	}
	
}