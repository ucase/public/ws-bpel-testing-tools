package subcommands;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.BaseTree;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.EPLTree;
import org.antlr.runtime.tree.Tree;
import com.espertech.esper.epl.generated.EsperEPL2GrammarParser;
import com.espertech.esper.epl.parse.ParseHelper;
import com.espertech.esper.epl.parse.ParseResult;
import com.espertech.esper.epl.parse.ParseRuleSelector;

import operators.Operator;
import operators.OperatorConstants;
import subprocesses.ProcessUtils;
//import mutants.parallel.MultiProcessPipe;

public abstract class ExecutionSubcommand extends AbstractSubcommand {
	
	private int fDeckSize = 0;
	private File fRootWorkDirectory;
	private int fThreadCount = 1;
	private File fEPLDir;
	
	public ExecutionSubcommand(int minArgumentCount, int maxArgumentCount,
			String name, String usage, String description) {
		super(minArgumentCount, maxArgumentCount, name, usage, description);
	}

	
	/**
     * Sets the maximum number of parallel EPL instances to run. If set
     * to 1, no parallelization is performed. For a good default value, you can
     * use {@link #DEFAULT_PARALLEL_THREAD_COUNT}.
     */
	
    /**
     * Splits a series of lines over several subprocesses, which produce a line
     * of output for each line of input. The split tries to keep all
     * subprocesses busy as long as possible, while keeping the output lines in
     * the same relative order as the input lines.
     *
     * <emph>Note 1:</emph> either <code>stdinLines</code> or <code>in</code>
     * must be null. Both cannot be not null at the same time.
     *
     * <emph>Note 2:</emph> the class which uses this method should define its
     * own <code>main</code> method, as the subprocesses will be running in
     * forked JVMs.
     *
     * @param stdinLines
     *            List of lines to be processed.
     * @param in
     *            Stream from which the lines to be processed should be read.
     * @param subprocessArgs
     *            Basic arguments to be passed to each subprocess. In addition,
     *            each subprocess will get the appropriate options (ports,
     *            directories, etc.) for setting up a different ActiveBPEL
     *            instance.
     */
/*    protected void parallelProcess(List<String> stdinLines, Reader in,
            List<String> subprocessArgs) throws IOException,
            InterruptedException, ExecutionException
    {
        // Create the commands required to start the compare subprocesses
        List<List<String>> processes = createSubprocessArgs(subprocessArgs);

        // Create the multiprocess pipe
        MultiProcessPipe pipe = new MultiProcessPipe(System.err, processes);

        if (in != null) {
            parallelProcessInOrder(new LineIterator(in), pipe);
        } else if (getDeckSize() == 0) {
            parallelProcessInOrder(stdinLines.iterator(), pipe);
        } else {
            parallelProcessDecks(stdinLines, pipe, getDeckSize());
        }
    }
	
    /**
     * Changes how many jobs are internally shuffled together in each iterator
     * of the parallel comparison. If set to 0, shuffling is disabled.
     */
 /*   public void setDeckSize(int fDeckSize) {
        this.fDeckSize = fDeckSize;
    }

    /**
     * Returns how many jobs are internally shuffled together in each iterator
     * of the parallel comparison. If 0, shuffling is disabled.
     */
 /*   private int getDeckSize() {
    	return fDeckSize;
	}

	public void setThreadCount(int threads) {
        this.fThreadCount = threads;
    }

	/**
	 * Returns the root work directory. If <code>null</code>,
     * the field will be initialized during execution to a new
     * temporary directory, which will be cleaned up after the
     * command is executed.
	 */
	public File getRootWorkDirectory() {
		return fRootWorkDirectory;
	}
	
	/**
     * Returns the maximum number of parallel EPL instances to run.
     */
    public int getThreadCount() {
        return fThreadCount;
    }
    
    /**
     * Returns the directory that contain the EPL program files or
     * null if the initials files haven't been initialized yet. 
     */
    public File getEPLDir(){
    	return fEPLDir;
    }
    
    /**
     * Set the directory that will contain the EPL program files
     * @param fEPLDir will contain the EPL program files
     */
    public void setEPLDir(File fEPLDir){
    	this.fEPLDir = fEPLDir;
    }
}
    /**
     * Processes a single line of output produced from a single line of input
     * sent to one of the subprocesses.
     *
     * @param line
     *            Output line to be processed.
     */
 /*   protected abstract void parallelProcessOutputLine(String line);

    private List<List<String>> createSubprocessArgs(List<String> baseArgs) throws IOException {
        if (getRootWorkDirectory() == null) {
            fRootWorkDirectory = es.uca.webservices.bpel.io.util.FileUtils.createTemporaryDirectory("mubpel", ".dir");
        }

        List<List<String>> processes = new ArrayList<List<String>>(fThreadCount);
        for (int i = 0; i < fThreadCount; ++i) {
            final File leafWorkDir = new File(fRootWorkDirectory, "subengine" + i);

            List<String> args = new ArrayList<String>();
            args.add("--" + BPEL_LOGLEVEL_OPTION);
            args.add(getActiveBPELLogLevel());
            args.add("--" + ENGINE_PORT_OPTION);
            args.add(Integer.toString(getEnginePort() + i));
            args.add("--" + MOCKUP_PORT_OPTION);
            args.add(Integer.toString(getMockupPort() + i));
            args.add("--" + WORKDIR_OPTION);
            args.add(leafWorkDir.getAbsolutePath());
            if (isPreserveServiceURLs()) {
            	args.add("--" + PRESERVE_SERVICE_URLS_OPTION);
            }
            if (isAbortAfterClientTrack()) {
            	args.add("--" + ABORT_AFTER_CT_OPTION);
            }
            if (getLogFile() != null) {
                args.add("--" + LOGFILE_OPTION);
                args.add(getLogFile().getCanonicalPath() + "." + i);
                args.add("--" + LOGFILE_LEVEL_OPTION);
                args.add(getLogfileLogLevel().toString());
            }
            args.addAll(baseArgs);
            args.add("-");

            processes.add(ProcessUtils.callJavaClassArgs(getClass(), args));
        }
        return processes;
    }
    
    private void parallelProcessDecks(List<String> pathsToOtherBPEL,
            MultiProcessPipe pipe, final int deckSize)
            throws InterruptedException, ExecutionException {

        // Divide the job list into shuffled sublists of {@link #getDeckSize()}
        // elements and run them, while preserving the expected result ordering
        int startPos = 0;
        while (startPos < pathsToOtherBPEL.size()) {
            int nextPos = Math
                    .min(startPos + deckSize, pathsToOtherBPEL.size());
            parallelProcessDeck(pathsToOtherBPEL.subList(startPos, nextPos),
                    pipe);
            startPos = nextPos;
        }
        pipe.doneWriting();
        pipe.doneReading();
    }

    private void parallelProcessDeck(List<String> sublist, MultiProcessPipe pipe)
            throws InterruptedException, ExecutionException {

        // Create shuffled positions
        final List<Integer> positions = createPositionList(sublist.size());
        Collections.shuffle(positions);

        // Write the jobs into the pipe as shuffled
        for (int position : positions) {
            pipe.writeLine(sublist.get(position));
        }

        // Read the results into the job buffer
        final List<String> bufferedLines = createFilledList(null, sublist
                .size());
        for (int position : positions) {
            bufferedLines.set(position, pipe.readLine());
        }

        // Print the results while preserving the original order
        for (String line : bufferedLines) {
            parallelProcessOutputLine(line);
        }
    }

    private void parallelProcessInOrder(
            Iterator<String> itPathsToOtherBPEL, MultiProcessPipe pipe)
        throws InterruptedException, ExecutionException {

        // Since we don't know how many elements we are working with, we need to
        // perform reads and writes concurrently.

        final PipeWriteTask writeTask = new PipeWriteTask(pipe, itPathsToOtherBPEL);
        Thread writeThread = new Thread(writeTask);
        writeThread.setName("CompareWrite");
        writeThread.start();

        final PipeReadTask readTask = new PipeReadTask(pipe);
        Thread readThread = new Thread(readTask);
        readThread.setName("CompareRead");
        readThread.start();

        // Wait for all jobs to be scheduled
        LOGGER.info("Waiting for all jobs to be scheduled...");
        writeThread.join();
        pipe.doneWriting();

        // Wait for the pending jobs to complete and produce their results
        LOGGER.info("All jobs scheduled: waiting for completion...");
        readThread.join();
        LOGGER.info("All jobs completed: shutting down...");
        pipe.doneReading();
        LOGGER.debug("Shutdown complete");

        // Rethrow any pending exceptions in the reading thread
        if (readTask.getException() != null) {
            throw readTask.getException();
        }
    }

    private <T> List<T> createFilledList(final T defaultVal, int nElements) {
        final List<T> bufferedLines = new ArrayList<T>();
        for (int i = 0; i < nElements; ++i) {
            bufferedLines.add(defaultVal);
        }
        return bufferedLines;
    }

    private List<Integer> createPositionList(final int nElements) {
        final List<Integer> positions = new ArrayList<Integer>();
        for (int i = 0; i < nElements; ++i) {
            positions.add(i);
        }
        return positions;
    }

    private final class PipeWriteTask implements Runnable {
        private final MultiProcessPipe pipe;
        private final Iterator<String> itPathsToOtherBPEL;

        private PipeWriteTask(MultiProcessPipe pipe,
                Iterator<String> itPathsToOtherBPEL) {
            this.pipe = pipe;
            this.itPathsToOtherBPEL = itPathsToOtherBPEL;
        }

        public void run() {
            // Write all the paths to the pipe
            while (itPathsToOtherBPEL.hasNext()) {
                pipe.writeLine(itPathsToOtherBPEL.next());
            }
        }
    }

    private final class PipeReadTask implements Runnable {
        private final MultiProcessPipe pipe;

        private ExecutionException mException;

        private PipeReadTask(MultiProcessPipe pipe) {
            this.pipe = pipe;
        }

        public void run() {
            // Read all the results and print them
            String line;
            try {
                while ((line = pipe.readLine()) != null) {
                    parallelProcessOutputLine(line);
                }
            } catch (InterruptedException e) {
                // nothing to do
            } catch (ExecutionException e) {
                // save it so it can be rethrown later
                mException = e;
            }
        }

        private ExecutionException getException() {
            return mException;
        }
    }
}
*/