package subcommands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.EPLTree;
import org.antlr.runtime.tree.Tree;
import com.espertech.esper.epl.generated.EsperEPL2GrammarParser;
import com.espertech.esper.epl.parse.ParseHelper;
import com.espertech.esper.epl.parse.ParseResult;
import com.espertech.esper.epl.parse.ParseRuleSelector;
import epl.util.Pair;
import operators.Operator;
import operators.OperatorConstants;

/**
 * <p>
 * Implements the 'analyze' subcommand.
 * </p>
 * 
 * @author 
 */
public class AnalyzeSubcommand extends AbstractSubcommand {

	private static final String USAGE = "Produces a listing of the number of operands available for each operator\n"
			+ "in a specific EPL process definition. Each operator is listed in a\n"
			+ "separate line, where its name, number of operands and maximum value for\n"
			+ "the attribute field are separated by a single space.";
	
	public AnalyzeSubcommand() {
		super(1, 2, "analyze", "PathToEPLFile [number_of_query_to_mutate]", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {

		final PrintStream os = getOutputStream();
		final Map<String, Pair<Integer, Integer>> analysisResults = analyze(getNonOptionArgs().get(0), Integer.parseInt(getNonOptionArgs().get(1)));
		os.print(formatResult(analysisResults));
	}


	/**
	 * Convenience version of
	 * {@link #analyze(EPLProcessDefinition, Map, Operator)}. Useful when we
	 * don't need any progress information. Collects results from all available
	 * mutation operators into a map.
	 *
	 * @param pathToEPL
	 *            Path to the <code>.epl</code> file with the process
	 *            definition to be analyzed.
	 */
	public Map<String, Pair<Integer, Integer>> analyze(String pathToEPL, int nquery)
			throws Exception {
		FileReader eplProc = new FileReader(new File(
				pathToEPL));
		
		String querybf = "", queryorg = "";
    	BufferedReader bf = new BufferedReader(eplProc);
    	Map<String, Pair<Integer, Integer>> results = createResultMap();
    	
    	System.out.println("The command analyze starts...");
    	
    	String path = GetPath(pathToEPL);
		File AnalyzeFile = new File(path + "/Analyze.txt");
    	
    	if (AnalyzeFile.exists()){
    		AnalyzeFile.delete();
    	}    	
    	
		int cont = 0;
		while (cont < nquery) {
			querybf = bf.readLine();
    		queryorg += querybf + " ";
    		cont++;
		
			Map<String, Pair<Integer, Integer>> Individualresults = createResultMap();
			
			ParseRuleSelector eplParseRule = new ParseRuleSelector(){
				public Tree invokeParseRule(EsperEPL2GrammarParser parser) throws RecognitionException {
					EsperEPL2GrammarParser.startEPLExpressionRule_return r = parser.startEPLExpressionRule();
					return (Tree) r.getTree();
				}
			};
			
			System.out.println(queryorg);
			ParseResult tree = ParseHelper.parse(queryorg, "Error", true, eplParseRule, false);
	
			Tree mepltree =  tree.getTree();
			CommonTree cepltree = (CommonTree) mepltree;
			EPLTree epltree = new EPLTree (cepltree);
			String ast = epltree.mycommontree.toStringTree();	
			
			for (Operator op : OperatorConstants.getOperatorMap().values()) {
				analyze(ast, Individualresults, op);
			}
			
			if (results.isEmpty()){
				results.putAll(Individualresults);
			} else {
				results = addOperatorOcurrence(Individualresults, results);
			}
			queryorg = "";
    	}
    	
    	
    	bf.close();
    	ResultsToFile(results, pathToEPL);
    	return results;
	}


	private void ResultsToFile(Map<String, Pair<Integer, Integer>> results,
		String pathToEPL) throws Exception{
		String path = GetPath(pathToEPL);
		FileWriter MutFile = new FileWriter(path + "/Analyze.txt", true);
		BufferedWriter bw = new BufferedWriter(MutFile);
		
		bw.write(formatResult(results));
		bw.close();		
	}

	private String GetPath(String pathToEPL) {
		int index = pathToEPL.lastIndexOf("/");
		String fatherpath = pathToEPL.substring(0, index);
		return fatherpath;
	}

	private Map<String, Pair<Integer, Integer>> addOperatorOcurrence(
			Map<String, Pair<Integer, Integer>> individualresults, Map<String, Pair<Integer, Integer>> results) {
		
		java.util.Iterator<Entry<String, Pair<Integer, Integer>>> it = individualresults.entrySet().iterator();
		java.util.Iterator<Entry<String, Pair<Integer, Integer>>> itR = results.entrySet().iterator();
		
		
		
		while (it.hasNext()) {
			Map.Entry ent = it.next();
			Map.Entry entR = itR.next();
			Pair<Integer, Integer> i = (Pair<Integer, Integer>)ent.getValue();
			Pair<Integer, Integer> r = (Pair<Integer, Integer>)entR.getValue();
			Integer sum = i.first + r.first;
			Pair add = new Pair(sum, r.second);
			entR.setValue(add);
		}

		return results;
	}

	/**
	 * Analyzes the process definition to check in how many locations the
	 * operator can be located, and computes the range of the attribute field
	 * for that operator.
	 *
	 * @param eplProc
	 *            EPL process definition to be analyzed.
	 * @param results
	 *            Result map to which the result of this analysis should be
	 *            saved, as a "operator name -> (number of locations, attribute range)"
	 *            key-value pair. If <code>null</code>, an appropriate empty map
	 *            will be created.
	 * @param op
	 *            Mutation operator for which the analysis should be performed.
	 * @return Result map after adding the key-value pair with the results of
	 *         this operator.
	 */
	public Map<String, Pair<Integer, Integer>> analyze(
			String eplDoc,
			Map<String, Pair<Integer, Integer>> results,
			Operator op)
			throws Exception {
		if (results == null) {
			results = createResultMap();
		}

		//final FileReader eplDoc = eplProc; 
		final int operandCount = op.getOperandCount(eplDoc);
		final int maxAttrValue = op.getMaximumDistinctAttributeValue(eplDoc);
		final Pair<Integer, Integer> value = new Pair<Integer, Integer>(
				operandCount, maxAttrValue);
		results.put(op.getName(), value);
		return results;
	}

	private Map<String, Pair<Integer, Integer>> createResultMap() {
		// It is very important that we use a LinkedHashMap: this way, the keys
		// will be traversed in the same order they were inserted.
		Map<String, Pair<Integer, Integer>> results = new LinkedHashMap<String, Pair<Integer, Integer>>();
		return results;
	}

	private String formatResult(Map<String, Pair<Integer, Integer>> analyze) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, Pair<Integer, Integer>> entry : analyze
				.entrySet()) {
			final String key = entry.getKey();
			final Pair<Integer, Integer> value = entry.getValue();
			sb.append(String.format("%s %d %d", key, value.getFirst(),
					value.getSecond()));
			sb.append("\n");
		}
		return sb.toString();
	}

}