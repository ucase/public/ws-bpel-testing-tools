package subcommands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.List;
import epl.util.Pair;
import epl.util.MutantCompare;

public class CompareSubcommand extends AbstractSubcommand{
	
	List<Integer> originalKeys = new ArrayList<Integer>(); 
			
	private static final String USAGE = "Produces a listing of the mutants. Each mutant is listed in a\n"
			+ "separate line, where its name, result of comparation against the original output (in different ways)\n"
			+ "are separated by a single space.";
	
	public CompareSubcommand() {
		super(3, 4, "compare", "PathToOriginalFolder FileWithMutantsPaths FileWithOutputsEventPaths", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {
		final List<String> nonOptionArgs = getNonOptionArgs();
		final PrintStream os = getOutputStream();
		final List<String> outputsResults = compare(getNonOptionArgs()
				.get(0), nonOptionArgs.get(1), nonOptionArgs.get(2));
		for (int l = 0; l < outputsResults.size(); l++) {os.print(outputsResults.get(l) + "\n");}
		
	}
	/**
	 * Convenience version of
	 * {@link #compare(String)}. Useful when we
	 * don't need any progress information. Collects results from all available
	 * mutation operators into a map.
	 *
	 * @param pathToEPL
	 *            Path to the <code>.epl</code> file with the process
	 *            definition to be analyzed.
	 */
	public List<String> compare(String pathToProgram, String pathToMutants, String pathToOutputEvents)
			throws Exception {  	
    	
		System.out.println("The command compare starts...");
		
		//Leemos el fichero pathToEPLoutputs para obtener la clave más alta y saber el número de columnas
		
		int maxKey = 1;
		
		maxKey = GetMaxKey(pathToOutputEvents); //También obtenemos las claves del original
		
		List<String> allMutantComparisions = new ArrayList<String>();
    	Map <String,List<String>> OriginalOutputs = new HashMap<String, List<String>>();
    			
		//Guardamos el contenido de las salidas del programa original
    	
    	for (int key : originalKeys) {
    		FileReader eventsReader = new FileReader("/tmp/outputs/" + key + "_original.event");
    		BufferedReader br = new BufferedReader(eventsReader);
    		
    		String reader = null;
    		List<String> eventValues = new ArrayList<String>();
    		
    		while((reader = br.readLine()) != null) {
    			eventValues.add(reader);
    		}
    		
    		br.close();
    		OriginalOutputs.put(Integer.toString(key), eventValues);
    	}

    	//Utilizamos el fichero ExecutionTimes para obtener:
    	//el nombre de los mutantes y los tiempos de ejecución (después de T)
    	//La primera línea del fichero es el original.
    	
    	String path = GetPath(pathToMutants);
    	FileReader TimeFile = new FileReader(path + "/ExecutionTimes.txt");

		BufferedReader br = new BufferedReader(TimeFile);
		String MutantFile = "";
		Long ExecutionTimeOriginal = null;
		
		MutantCompare mutant_compare = new MutantCompare();
		boolean firstLine = true;
		
		while ((MutantFile = br.readLine()) != null){
			Map <String,List<String>> MutantsOutputs = new HashMap<String, List<String>>();
			
			if (firstLine) {
				String Time = GetExecutionTime(MutantFile); //Obtenemos el tiempo de ejecución
				ExecutionTimeOriginal = Long.parseLong(Time);
				firstLine = false;
			} else {
				// Hay que leer de cada fichero el nombre del mutante y con esta información se crea
				// el path para llegar a los ficheros de salidas /tmp/outputs/key_nombreMutante.
				// Por cada key hay que volcar los eventos al map de mutantes
				
				boolean errorOutput = false;
				String mutantName = GetFileName(MutantFile); //Obtenemos el nombre del mutante
				mutant_compare.setMutantName(mutantName);
				
				for (int k = 1; k <= maxKey; k++) {
					String pathOutputFile = "/tmp/outputs/" + k + "_" + mutantName + ".event";
					File checkExists = new File(pathOutputFile);
					if (checkExists.exists()) {
						FileReader EventsFile = new FileReader(pathOutputFile);
						BufferedReader er = new BufferedReader(EventsFile);
				    	List<String> MutantEvents = new ArrayList<String>();
						String event = "";
						while ((event = er.readLine()) != null) {
							MutantEvents.add(event);
						}
						er.close();
						MutantsOutputs.put(Integer.toString(k), MutantEvents);
					} /*else {
						errorOutput = true;
					}*/
				}
								
				// Una vez volcados los eventos en el map de mutantes por cada consulta, se
				// comparan los ficheros para indicar si está vivo o no
				
				// Comparamos:
				// 0. Las claves del original y el mutante
				// 1. El número de líneas
				// 2. Tiempos de ejecución
				// 3. Contenido
								
				List<Integer> comparisions = new ArrayList<Integer>();
				
				for (int k = 1; k <= maxKey; k++) {	
					
					if (originalKeys.contains(k) && MutantsOutputs.containsKey(String.valueOf(k))) {
						
						int numLinesOriginal = OriginalOutputs.get(Integer.toString(k)).size();
						int numLinesMutant = MutantsOutputs.get(Integer.toString(k)).size();
						if (numLinesOriginal != numLinesMutant) { // Comparativa del número de líneas
							comparisions.add(1);
						} else {						
							//String mutantTime = GetExecutionTime(MutantFile); //Obtenemos el tiempo de ejecución
							//Long MutantExecutionTime = Long.parseLong(mutantTime);
							//mutant_compare.setExecutionTime(MutantExecutionTime);
							
							/*if (!MutantExecutionTime.equals(ExecutionTimeOriginal)) { // Comparativa de los tiempos de ejecución
								comparisions.add(1);
							} else {*/
							int line = 0;
							boolean alivemutant = true;
							while (line < numLinesOriginal) {
								
								if (!MutantsOutputs.get(Integer.toString(k)).contains(OriginalOutputs.get(Integer.toString(k)).get(line)) || 
										!OriginalOutputs.get(Integer.toString(k)).contains(MutantsOutputs.get(Integer.toString(k)).get(line))) {
									alivemutant = false;
									break;
								}
								
								line++;
							}
							if (alivemutant) {
								
								// Antes de indicar que está vivo, comprobamos si se incluyó la cobertura en el operador
								String pathToMutantProgram = pathToProgram.substring(0, pathToProgram.lastIndexOf("/") + 1) + mutantName;
								if (checkCoveragePoint(pathToMutantProgram)) { //La cobertura está 
									comparisions.add(3);
								} else {
									comparisions.add(0);
								}
							} else {
								comparisions.add(1);
							}
							//}
						}
					} else if (!originalKeys.contains(k) && !MutantsOutputs.containsKey(String.valueOf(k))){
						comparisions.add(0); 
						} else {
							comparisions.add(1); //The errors and exceptions should be solved in each mutant operator
						}
				}
			
				String mutantTime = GetExecutionTime(MutantFile); //Obtenemos el tiempo de ejecución
				Long MutantExecutionTime = Long.parseLong(mutantTime);
				mutant_compare.setExecutionTime(MutantExecutionTime);
				
				mutant_compare.setComparisions(comparisions);
				allMutantComparisions.add(mutant_compare.MutantToString());
			}
    	}
    	br.close();
    	
    	ResultsToFile(allMutantComparisions, pathToMutants);
    	return allMutantComparisions;
	}
	
	// Obtenemos el número mayor de claves existentes en las columnas y también obtenemos
	// las claves que tiene el fichero original para la comparativa posterior
	
	private int GetMaxKey(String pathToEPLoutputs) throws IOException {
		
		int maxKey = 1;
		FileReader File = new FileReader(pathToEPLoutputs);
		BufferedReader br = new BufferedReader(File);
		String line = "";
		String stringKey = "";
		
		while ((line = br.readLine()) != null){
			
			stringKey = line.substring(line.lastIndexOf("/") + 1, line.indexOf("_"));
			if (line.contains("original")) {
				maxKey = Integer.parseInt(stringKey);
				originalKeys.add(maxKey);
			}
			if (Integer.parseInt(stringKey) > maxKey) {
				maxKey = Integer.parseInt(stringKey);
			}
		}
		return maxKey;
	}

	private void ResultsToFile(List<String> allMutantComparisions, String pathToEPLoutputs) {
		String path = GetPath(pathToEPLoutputs);
		
    	// The new results in the list go to Compare file

    	FileWriter MutFile;
		try {
			MutFile = new FileWriter(path + "/Compare.txt", true);
			BufferedWriter bw = new BufferedWriter(MutFile);
			int numMutants = allMutantComparisions.size();
			
			for (int n = 0; n < numMutants; n++) {
				bw.write(allMutantComparisions.get(n) + "\n");
			}
			bw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		
	}

	private boolean checkCoveragePoint(String pathToMutantProgram) {
		File coveragefile = new File(pathToMutantProgram + "/CoverageFile");
		boolean exist = false;
		
		if (coveragefile.exists()){
			exist = true;
		}
		
		return exist;
	}

	private String GetFileName(String filepath) {
		String name = "";
		String[] splitpath = filepath.split("/");
		name = splitpath[splitpath.length - 1];
		name = name.substring(0, name.indexOf(".")); //.event is removed

		return name;
	}
	
	private String GetExecutionTime(String filepath) {
		String time = "";
		time = filepath.substring(filepath.indexOf(" T ") + 3);
		return time;
	}

	private String GetPath(String pathToEPL) {
		int index = pathToEPL.lastIndexOf("/");
		String fatherpath = pathToEPL.substring(0, index);
		return fatherpath;
	}
}
