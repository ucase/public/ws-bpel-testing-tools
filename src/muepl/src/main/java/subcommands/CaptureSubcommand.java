package subcommands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

public class CaptureSubcommand extends AbstractSubcommand {

	public CaptureSubcommand() {
		super(3, 3, "capture", "outputEPLFolder \"executioncommand\" TestCaseFile", USAGE);
	}

	private static final String USAGE = "Catch the EPL queries in the program,\n"
			+ "which follows the \"esper structure\". After the execution of the command, \n"  
			+ "all the queries are saved in a file: queries.epl, moreover the values of the\n"
			+ "involved events are saved as a test case";

	@Override
	protected void runCommand() throws Exception {
		capture(getNonOptionArgs().get(0), getNonOptionArgs().get(1), getNonOptionArgs().get(2));
	}
	
	public void capture(String OutputEPLFile, String executioncommand, String TestCaseFile)
			throws Exception {
		
		File outputFolder = new File(OutputEPLFile);
		String s = "";
		List<String> results = new ArrayList<String>();
		
		if (!outputFolder.exists()){
			outputFolder.mkdir();
		}
		
		Process a = Runtime.getRuntime().exec(executioncommand + " " + TestCaseFile);
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(a.getInputStream()));
		while ((s = stdInput.readLine()) != null) {
            System.out.println(s);
            results.add(s);
        }
		
		File EPLqueryFile = new File("/tmp/queries.epl");
		EPLqueryFile.createNewFile();
		Path origin = FileSystems.getDefault().getPath("/tmp/queries.epl");
		
		if (EPLqueryFile.exists()){
			
			Path destiny = FileSystems.getDefault().getPath(OutputEPLFile + "/queries.epl");
			Files.move(origin, destiny, StandardCopyOption.REPLACE_EXISTING);
		} else {
			System.out.println("No existe el fichero queries.epl");
		}		
		
		System.out.println("The command capture ends. All the EPL queries are saved");
	}
	
}