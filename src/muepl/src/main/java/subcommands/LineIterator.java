package subcommands;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterator which iterates over the lines in a Reader. It will wrap the Reader
 * as needed to read its lines.
 * 
 * @author Lorena Gutiérrez-Madroñal
 * 
 */

class LineIterator implements Iterator<String> {
	private final BufferedReader fReader;
	private String nextLine;

	LineIterator(Reader bIn) throws IOException {
		this.fReader = new BufferedReader(bIn);
	}

	@Override
	public boolean hasNext() {
		if (nextLine == null) {
			try {
				nextLine = fReader.readLine();
			} catch (IOException e) {
				return false;
			}
		}
		return nextLine != null;
	}

	@Override
	public String next() {
		if (hasNext()) {
			String oldValue = nextLine;
			nextLine = null;
			return oldValue;
		}

		throw new NoSuchElementException();
	}

	/**
	 * Unsupported operation.
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}