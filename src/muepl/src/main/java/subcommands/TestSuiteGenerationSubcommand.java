package subcommands;

import java.io.BufferedReader;
import java.io.FileReader;

public class TestSuiteGenerationSubcommand extends AbstractSubcommand {

	public TestSuiteGenerationSubcommand() {
		super(3, 3, "tsgenerate", "eplfolderprogram executorFileName parametersfile", USAGE);
	}

	private static final String USAGE = "Generates a set of test case which contain the values of the events while the program\n"
			+ "is running. The input file parametersfile, for each line, includes: input-parameters seed, that define each test case.\n"  
			+ "All the event values are saved in a appropiate file: TestCase_eplfolderprogramName_imput-parameters.values.";

	@Override
	protected void runCommand() throws Exception {
		tsgenerate(getNonOptionArgs().get(0), getNonOptionArgs().get(1), getNonOptionArgs().get(2));
	}
	
	public void tsgenerate(String Folderpath, String executorFileName, String parametersfile)
			throws Exception {

		FileReader suitcasefile = new FileReader(parametersfile);
		BufferedReader br = new BufferedReader(suitcasefile);
		String line = "";
    	
    	while( (line = br.readLine()) != null){
            //String[] parts = line.split(" ");
            TestCaseGenerationSubcommand.tcgenerate(Folderpath, executorFileName, line);
    	}
		
		br.close();
		suitcasefile.close();
		
		
		
		System.out.println("The command TestSuitGeneration ends. All the test cases are created");
	}
	
}