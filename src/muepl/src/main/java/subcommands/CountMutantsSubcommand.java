package subcommands;

import java.io.PrintStream;
import java.util.Map;

import epl.util.*;

public class CountMutantsSubcommand extends AbstractSubcommand{
	private static final String USAGE =
			      "Prints the number of mutants that can be produced from the original\n"
			      + "program to the standard output stream.";
	
	public CountMutantsSubcommand() {
			super(1, 1, "count", "PathToEPLFile [number_of_query_to_mutate]", USAGE);	
	}
	
	@Override
	protected void runCommand() throws Exception {
			final PrintStream os = getOutputStream();
			final String epl = getNonOptionArgs().get(0);
			final int numb = Integer.parseInt(getNonOptionArgs().get(1));
			os.print(count(epl, numb));
	}
	
	public int count(String string, int number) throws Exception {
		final Map<String, Pair<Integer, Integer>> results = new AnalyzeSubcommand().analyze(string, number);
			
		int count = 0;
		for (Map.Entry<String, Pair<Integer, Integer>> entry : results.entrySet()) {
			final Pair<Integer, Integer> pair = entry.getValue();
			count += pair.getFirst() * pair.getSecond();
		}
		
		return count;	
	}
}
