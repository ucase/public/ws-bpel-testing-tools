package subcommands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.EPLTree;
import org.antlr.runtime.tree.Tree;

import com.espertech.esper.epl.generated.EsperEPL2GrammarParser;
import com.espertech.esper.epl.parse.ParseHelper;
import com.espertech.esper.epl.parse.ParseResult;
import com.espertech.esper.epl.parse.ParseRuleSelector;

import epl.util.Pair;
import epl.util.Quadruple;
import operators.Operator;
import operators.OperatorConstants;

/**
 * <p>
 * Implements the 'analyze' subcommand.
 * </p>
 * 
 * @author 
 */
public class AnalyzeXYLocationSubcommand extends AbstractSubcommand {

	private static final String USAGE = "Produces a listing of the number of operands available for each operator\n"
			+ "in a specific EPL process definition. Each operator is listed in a\n"
			+ "separate line, where its name, number of operands and maximum value for\n"
			+ "the attribute field are separated by a single space.";
	
	public AnalyzeXYLocationSubcommand() {
		super(1, 2, "analyzepositionXY", "PathToEPLFile [number_of_query_to_mutate]", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {

		final PrintStream os = getOutputStream();
		final Map<Quadruple<String, Integer, Integer, Integer>, Pair<Integer, Integer>> analysisResults = analyze(getNonOptionArgs().get(0), Integer.parseInt(getNonOptionArgs().get(1)));
		os.print(formatResult(analysisResults));
	}


	/**
	 * Convenience version of
	 * {@link #analyze(EPLProcessDefinition, Map, Operator)}. Useful when we
	 * don't need any progress information. Collects results from all available
	 * mutation operators into a map.
	 *
	 * @param pathToEPL
	 *            Path to the <code>.epl</code> file with the process
	 *            definition to be analyzed.
	 */
	public Map<Quadruple<String, Integer, Integer, Integer>, Pair<Integer, Integer>> analyze(String pathToEPL, int nquery)
			throws Exception {
		FileReader eplProc = new FileReader(new File(
				pathToEPL));
		
		String querybf = "", queryorg = "";

		Map<Quadruple<String, Integer, Integer, Integer>, Pair<Integer, Integer>> results = new LinkedHashMap<>();
		try (BufferedReader bf = new BufferedReader(eplProc)) {
			System.out.println("The command analyzepositionXY starts...");

			String path = GetPath(pathToEPL);
			File AnalyzeFile = new File(path + "/AnalyzePositionXY.txt");

			if (AnalyzeFile.exists()) {
				AnalyzeFile.delete();
			}

			int cont = 0;
			int opId = 1;
			int queryId = 1;
			final Map<String, Integer> operandCounts = new HashMap<>();
			while (cont < nquery) {
				querybf = bf.readLine();
				queryorg += querybf + " ";
				cont++;

				ParseRuleSelector eplParseRule = new ParseRuleSelector() {
					public Tree invokeParseRule(EsperEPL2GrammarParser parser) throws RecognitionException {
						EsperEPL2GrammarParser.startEPLExpressionRule_return r = parser.startEPLExpressionRule();
						return (Tree) r.getTree();
					}
				};

				System.out.println(queryorg);
				ParseResult tree = ParseHelper.parse(queryorg, "Error", true, eplParseRule, false);

				Tree mepltree = tree.getTree();
				CommonTree cepltree = (CommonTree) mepltree;
				EPLTree epltree = new EPLTree(cepltree);
				String ast = epltree.mycommontree.toStringTree();

				for (Operator op : OperatorConstants.getOperatorMap().values()) {
					analyze(ast, results, op, opId, queryId, queryorg, operandCounts);
					opId++;
				}

				queryorg = "";
				queryId++;
				opId = 1;
			}
		}

		ResultsToFile(results, pathToEPL);
		return results;
	}

	private void ResultsToFile(Map<Quadruple<String, Integer, Integer, Integer>, Pair<Integer, Integer>> results,
		String pathToEPL) throws Exception{
		String path = GetPath(pathToEPL);
		FileWriter MutFile = new FileWriter(path + "/AnalyzePositionXY.txt", true);
		BufferedWriter bw = new BufferedWriter(MutFile);
		
		bw.write(formatResult(results));
		bw.close();		
	}

	private String GetPath(String pathToEPL) {
		int index = pathToEPL.lastIndexOf("/");
		String fatherpath = pathToEPL.substring(0, index);
		return fatherpath;
	}

	/**
	 * Analyzes the process definition to check in how many locations the
	 * operator can be located, and computes the range of the attribute field
	 * for that operator.
	 *
	 * @param eplProc
	 *            EPL process definition to be analyzed.
	 * @param results
	 *            Result map to which the result of this analysis should be
	 *            saved, as a "operator name -> (number of locations, attribute range)"
	 *            key-value pair. If <code>null</code>, an appropriate empty map
	 *            will be created.
	 * @param op
	 *            Mutation operator for which the analysis should be performed.
	 * @param queryorg 
	 * @param xposition 
	 * @return Result map after adding the key-value pair with the results of
	 *         this operator.
	 */
	private Map<Quadruple<String, Integer, Integer, Integer>, Pair<Integer, Integer>> analyze(
			String eplDoc,
			Map<Quadruple<String, Integer, Integer, Integer>, Pair<Integer, Integer>> results,
			Operator op, int opId, int nquery, String queryorg, Map<String, Integer> operandCounts)
			throws Exception {

		final int operandCount = op.getOperandCount(eplDoc);

		if (operandCount > 0) {
			final int maxAttrValue = op.getMaximumDistinctAttributeValue(eplDoc);
			final int nExistingLocations = operandCounts.getOrDefault(op.getName(), 0);

			int counter;
			for (counter = 1; counter <= operandCount; counter++) {
				final int column = op.getColumnPosition(eplDoc, queryorg, counter);

				final int location = nExistingLocations + counter;		
				for (int attrcounter = 1; attrcounter <= maxAttrValue; attrcounter++) {
					Quadruple<String, Integer, Integer, Integer> keyvalue = new Quadruple<String, Integer, Integer, Integer>(
							op.getName(), opId, location, attrcounter);				
					Pair<Integer, Integer> value = new Pair<Integer, Integer>(column, nquery);
						results.put(keyvalue, value);
					}				
			}
			operandCounts.put(op.getName(), nExistingLocations + operandCount);
		}

		return results;
	}

	private String formatResult(Map<Quadruple<String, Integer, Integer, Integer>, Pair<Integer, Integer>> analyze) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<Quadruple<String, Integer, Integer, Integer>, Pair<Integer, Integer>> entry : analyze
				.entrySet()) {
			final Quadruple<String, Integer, Integer, Integer> key = entry.getKey();
			final Pair<Integer, Integer> value = entry.getValue();
			sb.append(String.format("%s %d %d %d %d %d", key.getFirst(), key.getSecond(), key.getThird(), key.getFourth(), 
					value.getFirst(), value.getSecond()));
			sb.append("\n");
		}
		return sb.toString();
	}

}