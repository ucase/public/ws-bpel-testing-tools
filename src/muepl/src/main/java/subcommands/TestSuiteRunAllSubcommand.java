package subcommands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TestSuiteRunAllSubcommand extends ExecutionSubcommand{
	
	private static final String USAGE =
		"Run all the generated test-cases against all the mutants. It is needed the \n" +
		"path of the original program, its executorFileName, the folder where are \n" +
		"going to be saved the files with the output paths, the file which the test\n" +
		"suite and the file which contains the paths to mutated programs.\n";
	
	public TestSuiteRunAllSubcommand() {
		super(5, 5, "testsuiterunall", "originaleplFolder EPLFile executorFileName OutputFolder TestSuiteFile EPLpathFile", USAGE);
	}

	@Override
	protected void runCommand() throws Exception {
		final List<String> nonOptionArgs = getNonOptionArgs();
		final String pathToOriginal = nonOptionArgs.get(0);
		final String EPLFile = nonOptionArgs.get(1);
		final String OutputFolder = nonOptionArgs.get(2);
		final String executorFileName = nonOptionArgs.get(3);
		final String TestSuiteFile = nonOptionArgs.get(4);
		final String EPLpathFile = nonOptionArgs.get(5);
		runtestsuiteagainstall (pathToOriginal, EPLFile, executorFileName, OutputFolder, TestSuiteFile, EPLpathFile);		
	}

	private void runtestsuiteagainstall(String pathToOriginal, String EPLFile, String executorFileName, String OutputFolder, String TestSuiteFile, String ePLpathFile) throws Exception {
		
		String line = "";
    	FileReader tsfile = new FileReader(new File(TestSuiteFile));
    	BufferedReader tsbf = new BufferedReader(tsfile);
    	
    	System.out.println("Test suite agains all the mutants...");
    	Calendar cal1 = Calendar.getInstance();
    	System.out.println("Test-suite start time: "+cal1.get(Calendar.HOUR)+":"+cal1.get(Calendar.MINUTE)+":"+cal1.get(Calendar.SECOND)+":"+cal1.get(Calendar.MILLISECOND));
    	
    	while ((line = tsbf.readLine()) != null) {
    		//String[] parts = line.split(" ");
    		String parameters = line; //parts[0];
    		String OutputsFile = OutputFolder + "/output" + parameters;
    		RunAllSubcommand.runall(pathToOriginal, OutputsFile, executorFileName, parameters, ePLpathFile); //EPLFile,
        }
    	Calendar cal4 = Calendar.getInstance();
    	System.out.println("Test-suite end time: "+cal4.get(Calendar.HOUR)+":"+cal4.get(Calendar.MINUTE)+":"+cal4.get(Calendar.SECOND)+":"+cal4.get(Calendar.MILLISECOND));
    	
	}
}
