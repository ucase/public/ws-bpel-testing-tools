package subprocesses;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Convenience methods for creating subprocesses.
 */
public final class ProcessUtils {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ProcessUtils.class);

	private ProcessUtils() {}

	/**
	 * Invokes the main method of the specified Java class in a nested JVM, with
	 * the same classpath as the current one.
	 * 
	 * @param javaClassname
	 *            Java class to be run.
	 * @param args
	 *            Arguments to be passed through the command line.
	 * @return Resulting process after building the command to be run and
	 *         starting it.
	 * @throws IOException
	 *             There was an I/O error while spawning the subprocess.
	 */
	public static Process callJavaClass(Class<?> javaClass, String... args)
			throws IOException {
		final String javaPath = System.getProperty("java.home")
				+ File.separator + "bin" + File.separator + "java";
		final String javaClasspath = System.getProperty("java.class.path");
		final String javaClassname = javaClass.getCanonicalName();

		// Print debugging information: java exec, java classpath and name of
		// the main class
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("java executable: " + javaPath);
			LOGGER.debug("current classpath is:");
			String[] javaClasspathElements = javaClasspath
					.split(File.pathSeparator);
			for (String s : javaClasspathElements) {
				LOGGER.debug(" - " + s);
			}
			LOGGER.debug("java classname: " + javaClassname);
			LOGGER.debug("args:");
			for (String s : args) {
				LOGGER.debug("  " + s);
			}
		}

		List<String> cmdParts = new ArrayList<String>();
		cmdParts.addAll(Arrays.asList(javaPath, "-cp", javaClasspath,
				javaClassname));
		cmdParts.addAll(Arrays.asList(args));
		ProcessBuilder builder = new ProcessBuilder(cmdParts);
		return builder.start();
	}
}