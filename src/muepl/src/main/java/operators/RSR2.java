package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;


/**
 * ESUSBRII operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class RSR2 implements Operator {

    private final String name;
    private final String[] EROROperators = {">", "<", ">=", "<=", "EVAL_EQUALS_EXPR", "EVAL_NOTEQUALS_EXPR"};
    private final int[] ERORtype = {339, 338, 349, 350, 164, 165};
    private final int NumRSR2Operators = 21; 
    private final String[] RSRTokens = {"all", "any", "some"};
    private final int[] RSRtype = {47, 48, 49};
    private final String[] RSR2Tokens = {"NOT_IN_SUBSELECT_EXPR", "IN_SUBSELECT_EXPR"};
    private final String RSR3Tokens = "EXISTS_SUBSELECT_EXPR";
    private final int[] RSR2type = {230, 229}; 
    
    public RSR2(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	int countop = 0;
    	String query = source;
    	
    	for (int i = 0; i < RSR2Tokens.length; i++){
    		while (query.indexOf(RSR2Tokens[i].toString()) > -1){
				query = query.substring(query.indexOf(RSR2Tokens[i].toString()) + RSR2Tokens[i].toString().length());
				countop++;
			}
			query = source;
			count += countop;
			countop = 0;
    	}    		
    	    	
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumRSR2Operators;
    }

	public int check(CommonTree token, int operand) {
		
		String t = token.toString();
		int thereIs = 0;
		
		for (int i = 0; i < RSR2Tokens.length; i++){
			if (t.equals(RSR2Tokens[i])){
				thereIs = 1;
			}
		}
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo ESUBRII
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		if (attribute == 1){
			int type = 277;
			
			if (RSR2Tokens[0] == tree.toString()){
				type = RSR2type[1];
				attribute = 1;
			}
			
			if (RSR2Tokens[1] == tree.toString()){
				type = RSR2type[0];
				attribute = 0;
			}
			
			CommonToken t = new CommonToken(type, RSR2Tokens[attribute]);
			CommonTree node = new CommonTree(t);
			List kids = tree.getChildren(); //To save the children
			node.addChildren(kids);
			CommonTree parent = (CommonTree) tree.getParent();
			
			for (int i = 0; i < parent.getChildCount(); i++){
				for (int j = 0; j < RSR2Tokens.length; j++){				
					if (parent.getChild(i).toString().equals(RSR2Tokens[j])){				
						parent.deleteChild(i);
						parent.insertChild(i, node);
					}
				}
			}
		}
		if (attribute == 2 || attribute == 3){
			Tree kids = tree.getChild(1);
			CommonTree parent = (CommonTree) tree.getParent();
			CommonToken existsT = new CommonToken(228, RSR3Tokens);
			CommonTree existsnode = new CommonTree(existsT);
			existsnode.addChild(kids);
			
			if (attribute == 2){
				parent.insertChild(0, existsnode);
				parent.deleteChild(1);
			}
			
			if (attribute == 3){
				CommonToken t = new CommonToken(13, "not");
				CommonTree node = new CommonTree(t);
				node.addChild(existsnode);
				parent.insertChild(0, node);
				parent.deleteChild(1);
			}
		}
		if (attribute > 3){
			attribute-= 4;
			//Creating the new tree
			//Right kid
			CommonToken st = new CommonToken(227, "SUBSELECT_GROUP_EXPR");
			CommonTree nodesubselec = new CommonTree(st);
			CommonTree subquery = (CommonTree) tree.getChild(1);
			List kids = subquery.getChildren();
			nodesubselec.addChildren(kids);
			
			//Left kid
			CommonTree nodeexpr = (CommonTree) tree.getChild(0);
			
			//Main node
			int order = attribute % 6;
			CommonToken rt = new CommonToken(ERORtype[order], EROROperators[order]);
			CommonTree relationalnode = new CommonTree(rt);
			
			relationalnode.addChild(nodeexpr);
			
			//Middle kid
			if (attribute < 6){ //"all" keyword
				CommonToken t = new CommonToken(RSRtype[0], RSRTokens[0]);
				CommonTree keywordnode = new CommonTree(t);
				relationalnode.insertChild(1, keywordnode);
			}
			
			if (attribute < 12 && attribute >= 6){ //"any" keyword
				CommonToken t = new CommonToken(RSRtype[1], RSRTokens[1]);
				CommonTree keywordnode = new CommonTree(t);
				relationalnode.insertChild(1, keywordnode);
			}
			
			if (attribute >= 12){ //"some" keyword
				CommonToken t = new CommonToken(RSRtype[2], RSRTokens[2]);
				CommonTree keywordnode = new CommonTree(t);
				relationalnode.insertChild(1, keywordnode);
			}

			relationalnode.insertChild(2, nodesubselec);
			
			//Erase the old tree
			
			CommonTree parent = tree.parent;				
			parent.deleteChild(0);
			parent.addChild(relationalnode);
		}
		
		
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}