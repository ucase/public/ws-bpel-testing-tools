package operators;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;
import java.math.BigInteger;

/**
 * RNO operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.1
 */

public class RNO implements Operator {
	
    private final String name;
    private final int NumRNOOperators = 2;
    private final String[] Numbers = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};
    
    public RNO(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	String split = " ";
    	// We need divided the string " "
    	
		while (query.indexOf(split) > -1){
			if (IsANumber(query)){
				if (CheckPosition(query, source)){
					count++;
				}
			}
			query = query.substring(query.indexOf(split) + split.length());
		}
		
		// To check the last string
		
		if (IsANumber(query)){
			if (CheckPosition(query, source)){
				count++;
			}
		}

		return count;
    }

    private boolean CheckPosition(String query, String source) {
		
    	String[] validtokens = {"SELECTION_EXPR", "STREAM_EXPR", "HAVING_EXPR", "IN_RANGE", "EVENT_FILTER_NOT_RANGE", "EVENT_FILTER_IN", "EVENT_FILTER_NOT_IN", "TIMEPERIOD_LIMIT_EXPR"};
    	String[] novalidtokens = {"GROUP_BY_EXPR", "ORDER_BY_EXPR", "SUBSELECT_EXPR", "TIME_PERIOD", "VIEW_EXPR"};
    	int position = source.indexOf(query);
		boolean valid = false;
		String partquery = "";
		
		partquery = source.substring(0, position);
		int auxposition = 0;
		int indexvalid = 0;
		int indexnovalid = 0;
		
		for (int i = 0; i < validtokens.length; i++){
			String token = validtokens[i];
			indexvalid = partquery.indexOf(token);
			if (auxposition < indexvalid){
				auxposition = indexvalid;
			}
		}
		
		indexvalid = auxposition;
		auxposition = 0;
		
		for (int i = 0; i < novalidtokens.length; i++){
			String token = novalidtokens[i];
			indexnovalid = partquery.indexOf(token);
			if (auxposition < indexnovalid){
				auxposition = indexnovalid;
			}
		}
		
		indexnovalid = auxposition;
		
		if (indexnovalid < indexvalid){
			valid = true;
		}
		
		if (partquery.contains("TIMEPERIOD_LIMIT_EXPR")){ // To check TIMEPERIOD_LIMIT_EXPR
			if (indexvalid == partquery.indexOf("TIMEPERIOD_LIMIT_EXPR") || indexnovalid == partquery.indexOf("TIME_PERIOD")){
				valid = true;
			}
		}
		
		return valid;
	}


	private boolean IsANumber(String query) {
		boolean isnumber = false;
		String split = " ";
		String toevaluate = query;
		
		if (query.indexOf(split) != -1){			
			toevaluate = query.substring(0, query.indexOf(split));
		}
		
		if (toevaluate.contains(")")){
			toevaluate = toevaluate.substring(0, toevaluate.indexOf(")"));
		}
		
		if (isNumeric(toevaluate)){
			isnumber = true;
		}
		else {
			if (isNumericFloat(toevaluate)){
				isnumber = true;
			}
			else{
				if (isNumericDouble(toevaluate)){
					isnumber = true;
				}
				else{
					if (isNumericLong(toevaluate)){
						isnumber = true;
					}
				}
			}
		}
		
		return isnumber;
	}

    private static boolean isNumeric(String cadena){
    	try {
    		Integer.parseInt(cadena);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}
    }
    
    public static boolean isNumericFloat(String cadena) {
    	try {
    		Float.parseFloat(cadena);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}	
    }
    
    public static boolean isNumericDouble(String cadena) {
    	try {
    		Double.parseDouble(cadena);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}	
    }
    
    public static boolean isNumericLong(String cadena) {
    	try {
    		Long.parseLong(cadena);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}	
    }
    
    public static boolean isNumericBigInteger(String cadena) {
    	try {
    		BigInteger bd = new BigInteger(cadena);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}	
    }
    
	public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumRNOOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		boolean hasnovalidparent = false;
		String[] novalidtokens = {"GROUP_BY_EXPR", "ORDER_BY_EXPR", "SUBSELECT_EXPR", "TIME_PERIOD", "VIEW_EXPR"};
		
		if (token.toString() != "EPL_EXPR"){
			
			CommonTree parent = token.parent;
			
			while (parent.toString() != "EPL_EXPR"){
				for (int i = 0; i < novalidtokens.length; i++){
					if (parent.toString().equals(novalidtokens[i])){
						hasnovalidparent = true;
						if (novalidtokens[i].equals("TIME_PERIOD")){
							if (parent.parent.toString().equals("TIMEPERIOD_LIMIT_EXPR")){
								hasnovalidparent = false;
							}
						}
						break;
					}
				}
				parent = parent.parent;
			}
			
			if (!hasnovalidparent){
				
				if (isNumeric(token.toString())){
					thereIs = 1;
				} else{
					if (isNumericFloat(token.toString())){
						thereIs = 1;
				} else{
					if (isNumericDouble(token.toString())){
						thereIs = 1;
				} else{
					if (isNumericLong(token.toString())){
						thereIs = 1;
				}}}}
			}
		}

		return thereIs;

	}
	
	/**
	 * Función para realizar la mutación de un nodo específico de tipo RNO
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		CommonTree parent = tree.parent;
		int kids = parent.getChildCount();
		String[] operators = {">", "<", ">=", "<=", "EVAL_EQUALS_EXPR", "EVAL_NOTEQUALS_EXPR"};
		boolean applyedcov = false;
		
		if (coverage) { // To check if the parent is a relational operation
			for (int j = 0; j < operators.length; j++){
				if (parent.toString().equals(operators[j])){
					applyedcov = true;
				}
			}	
		}
		
		if (isNumeric(tree.toString())){
			int number = Integer.parseInt(tree.toString());
			number = applyOperation(number, attribute);
			String token = String.valueOf(number);
			CommonToken tint = new CommonToken(355, token);
			CommonTree t = new CommonTree(tint);
			
			for (int i = 0; i < kids; i++){
				if (parent.getChild(i).toString().equals(tree.toString())){
					parent.deleteChild(i);
					if (parent.getChildCount() != 0){
						parent.insertChild(i, t);
					} else {						
						parent.addChild(t);
					}
				}
			}
		}
		
		if (isNumericFloat(tree.toString())){
			float number = Float.parseFloat(tree.toString());
			number = applyOperation(number, attribute);
			String token = String.valueOf(number);
			CommonToken tfloat = new CommonToken(361, token);
			CommonTree t = new CommonTree(tfloat);
			
			for (int i = 0; i < kids; i++){
				if (parent.getChild(i).toString().equals(tree.toString())){
					parent.deleteChild(i);
					if (parent.getChildCount() != 0){
						parent.insertChild(i, t);
					} else {						
						parent.addChild(t);
					}
				}
			}
		}
		
		if (isNumericDouble(tree.toString())){
			double number = Double.parseDouble(tree.toString());
			number = applyOperation(number, attribute);
			String token = String.valueOf(number);
			CommonToken tdouble = new CommonToken(307, token);
			CommonTree t = new CommonTree(tdouble);
			
			for (int i = 0; i < kids; i++){
				if (parent.getChild(i).toString().equals(tree.toString())){
					parent.deleteChild(i);
					if (parent.getChildCount() != 0){
						parent.insertChild(i, t);
					} else {						
						parent.addChild(t);
					}
				}
			}
		}
		
		if (isNumericLong(tree.toString())){
			long number = Long.parseLong(tree.toString());
			number = applyOperation(number, attribute);
			String token = String.valueOf(number);
			CommonToken tlong = new CommonToken(360, token);
			CommonTree t = new CommonTree(tlong);

			for (int i = 0; i < kids; i++){
				if (parent.getChild(i).toString().equals(tree.toString())){
					parent.deleteChild(i);
					if (parent.getChildCount() != 0){
						parent.insertChild(i, t);
					} else {						
						parent.addChild(t);
					}
				}
			}
		}
		
		if (coverage) {
			if (applyedcov) { //Coverage applied to mutants with relational operation parents
				
				// The call to coverage function: coveragePoint()
				CommonToken andT = new CommonToken(162, "EVAL_AND_EXPR");
				CommonTree andNode = new CommonTree(andT);
					
				CommonToken coverageT = new CommonToken(202, "LIB_FUNC_CHAIN");
				CommonTree coverageParent = new CommonTree(coverageT);
				andNode.addChild(coverageParent);
					
				CommonToken coverageLibF = new CommonToken(201, "LIB_FUNCTION");
				CommonTree coverageLibFNode = new CommonTree(coverageLibF);
				coverageParent.addChild(coverageLibFNode);
					
				CommonToken coverageF = new CommonToken(277, "coveragePoint ()");
				CommonTree coverageFunctionNode = new CommonTree(coverageF);
				coverageLibFNode.addChild(coverageFunctionNode);
						
				int position = parent.childIndex;
				Tree grandpa = parent.getParent();
				int grandpakids = grandpa.getChildCount();
				andNode.addChild(parent);
				
				for (int i = 0; i < grandpakids; i++){
					if (grandpa.getChild(i).toString().equals(parent.toString()) && i == position){
						if (grandpa.getChildCount() != 0){
							grandpa.setChild(i, andNode);
						} else {						
							grandpa.addChild(andNode);
						}
					}
				}				
			}
		}		
		
	}

	private double applyOperation(double number, int attribute) {
		
		double result = number;
		
		if (attribute == 1){
			result = number + 1;
		}
		if (attribute == 2){
			result = number - 1;
		}

		return result;
	}

	private float applyOperation(float number, int attribute) {
		
		float result = number;
		
		if (attribute == 1){
			result = number + 1;
		}
		if (attribute == 2){
			result = number - 1;
		}

		return result;
	}

	private int applyOperation(int number, int attribute) {
		
		int result = number;
		
		if (attribute == 1){
			result = number + 1;
		}
		if (attribute == 2){
			result = number - 1;
		}

		return result;

	}
	
	private long applyOperation(long number, int attribute) {
		
		long result = number;
		
		if (attribute == 1){
			result = number + 1;
		}
		if (attribute == 2){
			result = number - 1;
		}

		return result;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		int positionC = queryorg.length();
		int positionCFinal = 0;
		int i = 0;
		int distance = 0;
		String number = "";
		String prevnumber = "";
		String query = queryorg.toLowerCase();

		while (i < counter) {
			for (String function : Numbers) {
				distance = query.indexOf(function);
				
				if (distance != -1) {
					distance = CheckNumber(distance, query);
					if (distance != -1) {
						number = GetNumber(distance, query);
					}
				}
				
				if (distance != -1 && positionC > distance) {
					positionC = distance;
				}
			}
			if (positionC == query.length()) { 
				for (String function : Numbers) {
					distance = query.indexOf(function);
					if (distance != -1 && positionC > distance) {
						positionC = distance;
					}
				}
				
				positionCFinal += positionC + 1 + number.length();
				query = query.substring(positionC + 1);
				positionC = query.length();
				number = "";
				prevnumber = "";
				distance = 0;
			} else {
				query = query.substring(positionC + number.length());
				if (positionCFinal == 0) {
					positionCFinal += positionC;
				} else {
					positionCFinal += positionC + prevnumber.length();
				}
				prevnumber = number;
				positionC = query.length();
				i++;
			}			
		}
		
		return positionCFinal + 1;
	}

	private String GetNumber(int distance, String query) {
		String split = " ";
		String number = "";
		String aroundquery = query.toLowerCase().substring(distance);
		aroundquery = aroundquery.substring(0, aroundquery.indexOf(split));
		
		if (aroundquery.contains("'") || aroundquery.contains(")")){
			if (aroundquery.endsWith("'") || aroundquery.startsWith("'")) {
				if (aroundquery.endsWith("'")) {
					aroundquery = aroundquery.substring(0, aroundquery.lastIndexOf("'"));
					if (isNumeric(aroundquery) || isNumericBigInteger(aroundquery)){
						number = aroundquery;
					}
				} else {
					aroundquery = aroundquery.substring(aroundquery.indexOf("'"), aroundquery.lastIndexOf("'"));
					if (isNumeric(aroundquery) || isNumericBigInteger(aroundquery)){
						number = aroundquery;
					}
				}		
			}
			if (aroundquery.contains(")")) {
				aroundquery = aroundquery.substring(0, aroundquery.indexOf(")"));
				if (isNumeric(aroundquery) || isNumericBigInteger(aroundquery)){
					number = aroundquery;
				}
			}
		}else {
			if (isNumeric(aroundquery) || isNumericBigInteger(aroundquery)){
				number = aroundquery;
			}
		}
		
		return number;
	}

	private int CheckNumber(int distance, String query) {
		
		String split = " ";
		String prevquery = query.toLowerCase().substring(distance - 1, distance);
		if (!prevquery.trim().isEmpty()) {
			if (!(prevquery.contains(",") || prevquery.contains("(") || prevquery.contains("=") || prevquery.contains("'"))) {
				distance = -1;
			}
		} else {
			String aroundquery = query.toLowerCase().substring(distance);
			aroundquery = aroundquery.substring(0, aroundquery.indexOf(split));
			
			if (aroundquery.contains("'") || aroundquery.contains(")")){
				if (aroundquery.endsWith("'") || aroundquery.startsWith("'")) {
					if (aroundquery.endsWith("'")) {
						aroundquery = aroundquery.substring(0, aroundquery.lastIndexOf("'"));
						if (!isNumeric(aroundquery) || !isNumericBigInteger(aroundquery)){
							distance = -1;
						}
					} else {
						aroundquery = aroundquery.substring(aroundquery.indexOf("'"), aroundquery.lastIndexOf("'"));
						if (!isNumeric(aroundquery) || !isNumericBigInteger(aroundquery)){
							distance = -1;
						}
					}			
				}
				if (aroundquery.contains(")")) {
					aroundquery = aroundquery.substring(0, aroundquery.indexOf(")"));
					if (!isNumeric(aroundquery) || !isNumericBigInteger(aroundquery)){
						distance = -1;
					}
				}			
			} else {
				if (!isNumeric(aroundquery) && !isNumericBigInteger(aroundquery)){
					distance = -1;
				}
			}
		}
		
		return distance;
	}
}