package operators;

import java.util.List;

import org.antlr.runtime.tree.CommonTree;


/**
 * RLOP operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.2
 * 
 * Está limitada la búsqueda del operador de tal modo que en el árbol sintáctico tienen que aparecer
 * (RLOP ... si hay algún cambio hay que indicarlo en el recuento de operandos.
 * 
 */

public class PNR implements Operator {
	
    private final String name;
    private final String[] tokens = {"PATTERN_NOT_EXPR", "PATTERN_FILTER_EXPR"};//, "MERGE_MAT", "MERGE_UNM"};
    private final String[] tokensRLOP = {"and", "EVAL_AND_EXPR", "or", "EVAL_OR_EXPR"};
    
    private final int NumNOPOperators = 1;  
    
    public PNR(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	while (query.indexOf(tokens[0]) > -1){ //PATTERN_NOT_EXPR
    		count++;
			query = query.substring(query.indexOf(tokens[0]) + tokens[0].length());
    	}
    	
    	return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumNOPOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		
		if (token.getChildren() != null){
			
			List children = token.getChildren();
	    	int size = children.size();
	    	
	    	for (int i = 0; i < size; i++){
				if (children.get(i).toString().equals(tokens[0])){ 
					thereIs = 1;
				}
			}
		}
		
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo NOP
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();
		int size = children.size();
		
		for (int i = 0; i < size; i++){
			if (children.get(i).toString().equals(tokens[0])){

				CommonTree child = (CommonTree) tree.getChild(i);
				List kids = child.getChildren();
				tree.deleteChild(i);
				tree.addChildren(kids);
			}
		}
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		int positionX = 0;
		int i = 0;
		
		positionX = queryorg.toLowerCase().indexOf("pattern");
		String query = queryorg.toLowerCase().substring(positionX);
		
		while (i < counter) {
			positionX += query.indexOf("not");
			query = query.substring(query.indexOf("not") + 3);
			i++;
		}
		
		return positionX;
	}

}