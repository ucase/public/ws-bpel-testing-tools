package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * EBTW operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.1
 * 
 * SYNTAX: test_expression [not] between begin_expression and end_expression
 * 
 */

public class RBR implements Operator {

    private final String name;
    private final String[] EBTWtoken = {"between", "NOT_BETWEEN"};
    private final int NumOperators = 3;
    
    public RBR(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	for (int i = 0; i < EBTWtoken.length; i++){
       		while (query.contains(EBTWtoken[i])){
    			query = query.substring(query.indexOf(EBTWtoken[i]) + EBTWtoken[i].length());
    			count++;
    		}
    	}

    	return count;
    }

	public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

    public int check(CommonTree token, int operand) {
		    	
		int thereIs = 0;
		
		for (int j = 0; j < EBTWtoken.length; j++){					
			if (token.toString().equals(EBTWtoken[j])){
				thereIs = 1;
			}
		}
		
		return thereIs;
	}
    
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		Tree kid1 = tree.getChild(1); //To conserve the children
		Tree kid2 = tree.getChild(2);
		Tree variable = tree.getChild(0);
		List children = tree.getChildren();
		CommonTree father = tree.parent;
		int number = 0;
		
		if (attribute == 1){
			CommonToken t = new CommonToken(162, "EVAL_AND_EXPR");
			CommonTree node = new CommonTree(t);
			
			//Children's begin
			CommonToken exprget = new CommonToken(339, ">");
			CommonTree nodeget = new CommonTree(exprget);					
			node.addChild(nodeget);
			nodeget.addChild(variable);
			nodeget.addChild(kid1);
			
			if (tree.toString().equals(EBTWtoken[1])){
				//Node begin
				CommonToken end = new CommonToken(13, "not");						
				CommonTree expend = new CommonTree(end);
				node.addChild(expend);
				
				CommonToken exprlet = new CommonToken(350, "<=");
				CommonTree nodelet = new CommonTree(exprlet);
				expend.addChild(nodelet);
				nodelet.addChild(variable);
				nodelet.addChild(kid2);
			} else {
				CommonToken exprlet = new CommonToken(350, "<=");
				CommonTree nodelet = new CommonTree(exprlet);
				node.addChild(nodelet);
				nodelet.addChild(variable);
				nodelet.addChild(kid2);
			}
			
			for (int i = 0; i < father.getChildCount(); i++){
				if (father.getChild(i).equals(tree)){
					if (father.getChild(i).getChild(0).getChild(0).getChild(0).toString().equals(variable.getChild(0).getChild(0).toString())){
						father.insertChild(i, node);
						father.deleteChild(i + 1);
					}
				}
			}
		}		
		if (attribute == 2) {
			CommonToken t = new CommonToken(162, "EVAL_AND_EXPR");
			CommonTree node = new CommonTree(t);
			
			//Children's begin
			CommonToken exprget = new CommonToken(349, ">=");
			CommonTree nodeget = new CommonTree(exprget);					
			node.addChild(nodeget);
			nodeget.addChild(variable);
			nodeget.addChild(kid1);
			
			if (tree.toString().equals(EBTWtoken[1])){
				//Node begin
				CommonToken end = new CommonToken(13, "not");						
				CommonTree expend = new CommonTree(end);
				node.addChild(expend);
				
				CommonToken exprlet = new CommonToken(338, "<");
				CommonTree nodelet = new CommonTree(exprlet);
				expend.addChild(nodelet);
				nodelet.addChild(variable);
				nodelet.addChild(kid2);
			} else {
				CommonToken exprlet = new CommonToken(338, "<");
				CommonTree nodelet = new CommonTree(exprlet);
				node.addChild(nodelet);
				nodelet.addChild(variable);
				nodelet.addChild(kid2);
			}
			
			for (int i = 0; i < father.getChildCount(); i++){
				if (father.getChild(i).equals(tree)){
					if (father.getChild(i).getChild(0).getChild(0).getChild(0).toString().equals(variable.getChild(0).getChild(0).toString())){
						father.insertChild(i, node);
						father.deleteChild(i + 1);
					}
				}
			}
		} 
		if (attribute == 3){				
			if (tree.toString().equals(EBTWtoken[0])){
				CommonToken ebtwe = new CommonToken(216, EBTWtoken[1]);
				CommonTree node = new CommonTree(ebtwe);
				node.addChildren(children);
				
				for (int i = 0; i < father.getChildCount(); i++){
					if (father.getChild(i).equals(tree)){
						if (father.getChild(i).getChild(0).getChild(0).getChild(0).toString().equals(variable.getChild(0).getChild(0).toString())){
							father.insertChild(i, node);
							father.deleteChild(i + 1);
						}
					}
				}
			}
			if (tree.toString().equals(EBTWtoken[1])){
				CommonToken ebtwe = new CommonToken(7, EBTWtoken[0]);
				CommonTree node = new CommonTree(ebtwe);
				node.addChildren(children);
				
				for (int i = 0; i < father.getChildCount(); i++){
					if (father.getChild(i).equals(tree)){
						if (father.getChild(i).getChild(0).getChild(0).getChild(0).toString().equals(variable.getChild(0).getChild(0).toString())){
							father.insertChild(i, node);
							father.deleteChild(i + 1);
						}
					}
				}
			}
		}
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}