package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * BATL operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class WBL implements Operator {
	
    private final String name;
    private final String[] BATLtokens = {"length", "length_batch"};
    private final int NumBATLOperators = 1;   
    
    public WBL(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	String checkOp = "win length";
    	
    	while (query.indexOf(checkOp) > -1){
    		count++;
    		query = query.substring(query.indexOf(checkOp) + checkOp.length());
    	}
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumBATLOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		
		if (token.getChildren() != null){
			
			List children = token.getChildren();
			int size = children.size();

			if (HasTheBrother(children)){
				
				for (int i = 0; i < size; i++){
					for (int j = 0; j < BATLtokens.length; j++){					
						if (children.get(i).toString().equals(BATLtokens[j])){
							thereIs = 1;
						}
					}
				}
			}
		}
		
		return thereIs;

	}
	
	private boolean HasTheBrother(List brothers) {
		String brother = "win";
		boolean exist = false;
		
		for (int i = 0; i < brothers.size(); i++){
			if (brothers.get(i).toString().equals(brother)){
				exist = true;
				break;
			}
		}
		return exist;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo BATL
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();
		int size = children.size();
		boolean change = false;
		attribute -= 1;
		
		for (int i = 0; i < size; i++){
			if (!change){
				if (IsTheOperator(children.get(i).toString())){
					int type = 277;
					int position = CheckOperatorInVector(children.get(i).toString());
					
					if (position <= attribute){
						attribute += 1; //It has to advance 1 position
					}
					
					CommonToken t = new CommonToken(type, BATLtokens[attribute]);
					CommonTree node = new CommonTree(t);
					tree.insertChild(i, node);
					tree.deleteChild(i + 1);
				}
			}
		}
	}

	private int CheckOperatorInVector(String operator) {

		int position = 0;
		
		for (int i = 0; i <= BATLtokens.length - 1; i++){
			if (BATLtokens[i].equals(operator)){
				position = i;
			}				
		}
		return position;
	}

	private boolean IsTheOperator(String op) {
		
		boolean exist = false;

		for (int i = 0; i < BATLtokens.length; i++){			
			if (BATLtokens[i].equals(op)){
				exist = true;
			}
		}

		return exist;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		return queryorg.indexOf("win:length");
	}

}