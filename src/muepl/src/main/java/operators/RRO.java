package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * EROR operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class RRO implements Operator {

    private final String name;
    private final String[] RROOperators = {">", "<", ">=", "<=", "EVAL_EQUALS_EXPR", "EVAL_NOTEQUALS_EXPR"};
    private final String[] CheckEROROp = {">", "<", "EVAL_EQUALS_EXPR", "EVAL_NOTEQUALS_EXPR"};
    private final int[] typeTokens = {339, 338, 349, 350, 164, 165};
    private final int NumEROROperators = 5;
    private final String[] RROPositions = {">", "<", ">=", "<=", "=", "!="};
    
    public RRO(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	int numop, countop;
    	String query = "";
    	
    	for (numop = 0; numop < CheckEROROp.length; numop++){
    		countop = 0;
    		query = source;
    		
    		while (query.indexOf(CheckEROROp[numop]) > -1){
        		query = query.substring(query.indexOf(CheckEROROp[numop]) + CheckEROROp[numop].length());
        		countop++;
        	}
    		count += countop;
    	}
    	
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumEROROperators;
    }

    public int check(CommonTree token, int operand) {
		
		int thereIs = 0;

		for (int j = 0; j < RROOperators.length; j++){
			if (token.toString().equals(RROOperators[j])){
				thereIs = 1;
			}
		}

		return thereIs;
	}

	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		attribute--;
		int position = CheckOperatorInVector(tree.toString());
		
		if (position <= attribute){
			attribute += 1; //It has to advance 1 position
		}
		
		// To include the mutation
		
		int type = InfoToken(RROOperators[attribute]);
		CommonToken t = new CommonToken(type, RROOperators[attribute]);
		CommonTree node = new CommonTree(t);
		
		// To save the children's children
		List children = tree.getChildren();
		node.addChildren(children);
		
		if (coverage) {
			// The call to coverage function: coveragePoint()
			CommonToken andT = new CommonToken(162, "EVAL_AND_EXPR");
			CommonTree andNode = new CommonTree(andT);
				
			CommonToken coverageT = new CommonToken(202, "LIB_FUNC_CHAIN");
			CommonTree coverageParent = new CommonTree(coverageT);
			andNode.addChild(coverageParent);
				
			CommonToken coverageLibF = new CommonToken(201, "LIB_FUNCTION");
			CommonTree coverageLibFNode = new CommonTree(coverageLibF);
			coverageParent.addChild(coverageLibFNode);
				
			CommonToken coverageF = new CommonToken(277, "coveragePoint ()");
			CommonTree coverageFunctionNode = new CommonTree(coverageF);
			coverageLibFNode.addChild(coverageFunctionNode);
			
			//To include the coverage function call in the query
			andNode.addChild(node);
			
			CommonTree father = tree.parent;
			int kids = father.getChildCount();
			
			for (int i = 0; i < kids; i++){
				if (father.getChild(i).equals(tree)){
					father.insertChild(i, andNode);
					father.deleteChild(i + 1);
				}
			}
		} else {
            CommonTree father = tree.parent;
            int kids = father.getChildCount();

            for (int i = 0; i < kids; i++){
                if (father.getChild(i).equals(tree)){
                    father.insertChild(i, node);
                    father.deleteChild(i + 1);
                }
            }                
		}
	}

	private int CheckOperatorInVector(String operator) {
		
		int position = 0;
		
		for (int i = 0; i < RROOperators.length; i++){
			if (RROOperators[i].equals(operator)){
				position = i;
			}				
		}
		return position;
	}

	private int InfoToken(String st) {
		int pos = 0;
		
		for (int i = 0; i < RROOperators.length; i++){
			if (RROOperators[i].equals(st)){
				pos = typeTokens[i];
			}
		}
		
		return pos;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		int positionX = queryorg.length();
		int positionXFinal = 0;
		int i = 0;
		int whatfunction = 0;
		int prevfunction = 0;
		int iteration = 0;
		int distance = 0;
		String query = queryorg.toLowerCase();

		if (query.contains(" every ")) {
			positionXFinal = query.indexOf("every");
			query = query.substring(query.indexOf("every"));
			positionXFinal += query.indexOf("(");
			query = query.substring(query.indexOf("("));
		}
		
		while (i < counter) {
			for (String function : RROPositions) {
				distance = query.indexOf(function);
				if (distance != -1 && positionX > distance) {
					positionX = distance;
					whatfunction = iteration;
				}
				iteration++;
			}
			query = query.substring(positionX + RROPositions[whatfunction].length());
			if (positionXFinal == 0) {
				positionXFinal += positionX;
				prevfunction = whatfunction;
				positionX = query.length();
			} else {
				positionXFinal += positionX + RROPositions[prevfunction].length();
				prevfunction = whatfunction;
				positionX = query.length();
			}
			i++;
			iteration = 0;
		}
		
		return positionXFinal;
	}
}