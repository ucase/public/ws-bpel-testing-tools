package operators;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * RLA operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 * 
 * SYNTAXES: test_expression [not] like pattern_expression [escape string_literal]
 * 
 */

public class RLA implements Operator {

    private final String name;
    private final String[] RLAtoken = {"like", "NOT_LIKE"};
    private final int NumOperators = 4;
    
    public RLA(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	for (int i = 0; i < RLAtoken.length; i++){
       		while (query.contains(RLAtoken[i])){
    			query = query.substring(query.indexOf(RLAtoken[i]) + RLAtoken[i].length());
    			count++;
    		}
    	}

    	return count;
    }

	public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

    public int check(CommonTree token, int operand) {
		    	
    	int thereIs = 0;
		
		for (int j = 0; j < RLAtoken.length; j++){					
			if (token.toString().equals(RLAtoken[j])){
				thereIs = 1;
			}
		}
		
		return thereIs;
	}
    
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		Tree kid = tree.getChild(1);
		String pattern = kid.toString();
		String finalkid = "";
		String[] wildcars = {"%", "_"}; 
		int length = pattern.length();
		
		if (attribute == 1){
			finalkid = pattern.substring(0, length - 1) + wildcars[0] + "'";
		} 
		if (attribute == 2){
			finalkid = pattern.substring(0, length - 1) + wildcars[1] + "'";
		}
		if (attribute == 3){
			finalkid = "'" + wildcars[0] + pattern.substring(1) + "'";
		} 
		if (attribute == 4){
			finalkid = "'" + wildcars[1] + pattern.substring(1) + "'";
		}
		
		CommonToken t = new CommonToken(277, finalkid);
		CommonTree node = new CommonTree(t);
		tree.addChild(node);
		tree.deleteChild(1);		
	}


	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}