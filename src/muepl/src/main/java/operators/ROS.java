package operators;

import java.util.List;

import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * EORDS operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.1
 * 
 * SYNTAXES: order by expression [asc | desc] [, expression [asc | desc]] [, ...]
 * 
 */

public class ROS implements Operator {

    private final String name;
    private final String ROStoken = "ORDER_BY_EXPR";
    private final int NumOperators = 1;
    
    public ROS(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	while (query.contains(ROStoken)){
    		query = query.substring(query.indexOf(ROStoken) + ROStoken.length());
    		count =+ CountChildren(query, source);
    	}
    	
    	if (count > 0){
    		count--; // It can be apply the number of children minus 1
    	}
    	
    	return count;
    }

    private int CountChildren(String query, String source) {
    	String childs = "ORDER_ELEMENT_EXPR";
		int elements = 0;
		String auxquery = query;
		
		while (auxquery.contains(childs)){
			elements++;
			auxquery = auxquery.substring(auxquery.indexOf(childs) + childs.length());
		}
		
		return elements;
	}

	public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

    public int check(CommonTree token, int operand) {
		    	
    	int thereIs = 0;
    	int numberkid = 0;
		String comparetoken = "ORDER_ELEMENT_EXPR";
		
		if (token.toString() == ROStoken){
			int size = token.getChildCount();
			
			for (int i = 0; i < size; i++){
				if (token.getChild(i).toString() == comparetoken){
					numberkid++;
				}
			}
			if (numberkid > 1){
				thereIs = 1;
			}
		}
		
		return thereIs;
	}

	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		Tree node = tree.getChild(0); //Taking the first child
		
		tree.addChild(node);
		tree.deleteChild(0);
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}