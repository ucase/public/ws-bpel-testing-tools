package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * ESIR operator 
 * 
 * Syntaxes: SINGLE_ROW_FUNC (expression...)
 * Grammar syntaxes: single_row_func (EVENT_PROP_EXPR...
 * 
 * @see The ESIRTokensC haven't children
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class RRR1 implements Operator {

    private final String name;
    private final String[] RRRTokensA = {"cast", "instanceof"};
    private final int[] typeRRRTokensA = {79, 77};
    private final String[] RRRTokensB = {"prevwindow", "prevcount"};
    private final int[] typeRRRTokensB = {72, 71};
        
    private final int NumOperators = 1;  
    
    public RRR1(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	for (int i = 0; i < RRRTokensA.length; i++){
       		while (query.contains(RRRTokensA[i])){
    			query = query.substring(query.indexOf(RRRTokensA[i]) + RRRTokensA[i].length());
    			count++;
    		}
       		query = source;
    	}

    	query = source;
    	
    	for (int i = 0; i < RRRTokensB.length; i++){
       		while (query.contains(RRRTokensB[i])){
    			query = query.substring(query.indexOf(RRRTokensB[i]) + RRRTokensB[i].length());
    			count++;
    		}
       		query = source;
    	}
    	
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

	public int check(CommonTree token, int operand) {
		
		String t = token.toString();
		int thereIs = 0;
		
		for (int i = 0; i < RRRTokensA.length; i++){
			if (t.equals(RRRTokensA[i])){
				thereIs = 1;
			}
		}
		
		if (thereIs == 0){
			for (int i = 0; i < RRRTokensB.length; i++){
				if (t.equals(RRRTokensB[i])){
					thereIs = 1;
				}
			}
		}
		
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo ESIR
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();
		
		String token = InfoToken(tree.toString());
		int type = TypeToken(token);
		CommonToken t = new CommonToken(type, token);
		CommonTree node = new CommonTree(t);
		CommonTree parent = (CommonTree) tree.getParent();
		parent.insertChild(0, node);
		
		// To save the children's children
		node.addChildren(children);
		int indextoken = tree.childIndex;
		parent.deleteChild(indextoken);

	}

	private int TypeToken(String token) {
		
		int type = 0;
		
		if (RRRTokensA[0].equals(token)){
			type = typeRRRTokensA[1];
		} else if (RRRTokensA[1].equals(token)){
			type = typeRRRTokensA[0];
		}
		
		if (RRRTokensB[0].equals(token)){
			type = typeRRRTokensB[1];
		} else if (RRRTokensB[1].equals(token)){
			type = typeRRRTokensB[0];
		}
		
		return type;
	}

	private String InfoToken(String st) {
		String token = "";
		
	if (RRRTokensA[1].equals(st)){
		token = RRRTokensA[0];
	} else if (RRRTokensA[0].equals(st)){
		token = RRRTokensA[1];
	}
	
	if (RRRTokensB[1].equals(st)){
		token = RRRTokensB[0];
	} else if (RRRTokensB[0].equals(st)){					
		token = RRRTokensB[1];
	}
	
	return token;
	
	}


	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}