package operators;

import java.util.List;

import org.antlr.runtime.tree.CommonTree;

/**
 * POC operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class POC implements Operator {

    private final String name;
    private final String POCTokens = "FOLLOWED_BY_EXPR";
    private final String[] POCSwap = {"PATTERN_FILTER_EXPR", "OBSERVER_EXPR"};
    
    private final int NumSELOperators = 1;
    
    public POC(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	while (query.indexOf(POCTokens) > -1){
    		query = query.substring(source.indexOf(POCTokens) + POCTokens.length());
    		count++;
    	}
    	
        return count; 
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumSELOperators;
    }

	public int check(CommonTree token, int operand) {
		
		String t = token.toString();
		int thereIs = 0;
		
		if (t == POCTokens){
			thereIs = 1;
		}
		
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo CEOP
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		// We have to check the kids' kids to find the tokens "PATTERN_FILTER_EXPR"
		int childA, childB;
		childA = 0;
		childB = 1;
		// Now we have the two children, but we have to deep to swap the POCSwap nodes

		CommonTree poschildA = FindSwapPosition((CommonTree) tree.getChild(childA));
		CommonTree poschildB = FindSwapPosition((CommonTree) tree.getChild(childB));		
		CommonTree parentA = poschildA.parent;
		CommonTree parentB = poschildB.parent;
		parentA.insertChild(0, poschildB);
		parentA.deleteChild(1);
		parentB.insertChild(0, poschildA);
		parentB.deleteChild(1);

	}

	private CommonTree FindSwapPosition(CommonTree tree) {
		
		List children = tree.getChildren();
		CommonTree poschild = null;
			for (int i = 0; i < children.size(); i++){
				if (IsTheChild(children.get(i).toString())){
					poschild = (CommonTree) tree.getChild(i);
					break;
				}
				else{
					poschild = FindSwapPosition((CommonTree) tree.getChild(i));
				}
			}
		
		return poschild;
	}

	private boolean IsTheChild(String op) {

		boolean exist = false;
		
		if (POCSwap[0].equals(op)){
			exist = true;
		} else {
			if (POCSwap[1].equals(op)){
				exist = true;
			}
		}

		return exist;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		int positionX = 0;
		
		positionX = queryorg.indexOf("->");
		
		return positionX;
	}
}