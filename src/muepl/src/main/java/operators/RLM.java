package operators;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * RLM operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.1
 * 
 * SYNTAXES: test_expression [not] like pattern_expression [escape string_literal]
 * 
 */

public class RLM implements Operator {

    private final String name;
    private final String[] RLMtoken = {"like", "NOT_LIKE"};
    private final int NumOperators = 2;
    private static int numoperand = 0;
    
    public RLM(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	for (int i = 0; i < RLMtoken.length; i++){
       		while (query.contains(RLMtoken[i])){
    			query = query.substring(query.indexOf(RLMtoken[i]) + RLMtoken[i].length());
    			count = HasWildcars(query);
    		}
    	}

    	return count;
    }

	private int HasWildcars(String query) {
		
		String pattern = "";
		int count = 0;
		query = query.substring(query.indexOf("'") + 1);
		pattern = query.substring(0, query.indexOf("'"));
		String pattern2 = pattern;
		
		while (pattern.contains("_")){
			pattern = pattern.substring(pattern.indexOf("_") + 1);
			count++;
		}
		
		while (pattern2.contains("%")){
			pattern2 = pattern2.substring(pattern2.indexOf("%") + 1);
			count++;
		}
		
		return count;
	}

	public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

    public int check(CommonTree token, int operand) {
		    	
		int thereIs = 0;
		
		for (int j = 0; j < RLMtoken.length; j++){					
			if (token.toString().equals(RLMtoken[j])){
				thereIs += HasWildcars(token.getChild(1).toString());
				numoperand += thereIs;
				
				if (thereIs > operand){
					thereIs = operand;
					numoperand = operand;
				}
				
			}
		}
		
		return thereIs;
	}
    
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		Tree kid = tree.getChild(1);
		String pattern = kid.toString();
		String finalkid = "";
		String[] wildcards = {"%", "_"}; 
		int count = 0;
		
		while (count != numoperand){
			int index1 = pattern.indexOf("%");
			int index2 = pattern.indexOf("_");
			
			if (index1 != -1 && index2 != -1){
				if (index1 < index2){
					count++;
					if (finalkid == ""){						
						finalkid = pattern.substring(0, pattern.indexOf(wildcards[0]) + 1);
					} else{
						finalkid = finalkid + pattern.substring(0, pattern.indexOf(wildcards[0]) + 1);
					}
					pattern = pattern.substring(pattern.indexOf(wildcards[0]) + 1);
				} else {
					count++;
					if (finalkid == ""){						
						finalkid = pattern.substring(0, pattern.indexOf(wildcards[1]) + 1);
					} else{
						finalkid = finalkid + pattern.substring(0, pattern.indexOf(wildcards[1]) + 1);
					}
					pattern = pattern.substring(pattern.indexOf(wildcards[1]) + 1);
				}
			} else {
				if (index1 != -1){
					count++;
					if (finalkid == ""){						
						finalkid = pattern.substring(0, pattern.indexOf(wildcards[0]) + 1);
					} else{
						finalkid = finalkid + pattern.substring(0, pattern.indexOf(wildcards[0]) + 1);
					}
					pattern = pattern.substring(pattern.indexOf(wildcards[0]) + 1);
				}
				if (index2 != -1){
					count++;
					if (finalkid == ""){						
						finalkid = pattern.substring(0, pattern.indexOf(wildcards[1]) + 1);
					} else{
						finalkid = finalkid + pattern.substring(0, pattern.indexOf(wildcards[1]) + 1);
					}
					pattern = pattern.substring(pattern.indexOf(wildcards[1]) + 1);
				}
			}
		}
		
		if (attribute == 1){
			finalkid = finalkid.substring(0, finalkid.length() - 1) + pattern;
			
			CommonToken t = new CommonToken(277, finalkid);
			CommonTree node = new CommonTree(t);
			tree.addChild(node);
			tree.deleteChild(1);
		} else {
			String wildcard = finalkid.substring(finalkid.length() - 1);
			
			if (wildcard.equals("%")){
				finalkid = finalkid.substring(0, finalkid.length() - 1) + "_" + pattern;
			} else {
				finalkid = finalkid.substring(0, finalkid.length() - 1) + "%" + pattern;
			}
			
			CommonToken t = new CommonToken(277, finalkid);
			CommonTree node = new CommonTree(t);
			tree.insertChild(1, node);
			tree.deleteChild(2);
		}				
	}
	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}