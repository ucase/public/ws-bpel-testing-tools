package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * EGRUA operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */


public class RGR implements Operator {

    private final String name;
    private final String RGRtoken = "GROUP_BY_EXPR";
    private final int NumOperators = 2;
    private final int[] type = {20, 21};
    private final String[] aggfunction = {"max", "min"};
    
    public RGR(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	while (query.contains(RGRtoken)){
    		query = query.substring(query.indexOf(RGRtoken) + RGRtoken.length());
    		count =+ CountChildren(query, source);
    	}
    	
    	return count;
    }

    private int CountChildren(String query, String source) {
    	String child = "EVENT_PROP_EXPR (EVENT_PROP_SIMPLE ";
		int kids = 0;
		
		while (query.indexOf(")))") > query.indexOf(child)){
			if (query.indexOf(child) != -1){
				String kid = query.substring(query.indexOf(child) + child.length());
				kid = kid.substring(0, kid.indexOf(")"));
				if (IsInQuery(kid, source, "SELECTION_EXPR")){
					query = query.substring(query.indexOf(child) + child.length());
					kids++;
				}else{
					if (IsInQuery(kid, source, "ORDER_BY_EXPR")){						
						query = query.substring(query.indexOf(child) + child.length());
						kids++;
					}
					else{
						query = query.substring(query.indexOf(child) + child.length());
					}
				}
			}
			else{
				break;
			}
		}
		return kids;
	}

	private boolean IsInQuery(String kid, String source, String token) {
		boolean exist = false;
		String group = "GROUP_BY_EXPR";
		
		int indexkid = source.indexOf(kid);
		int indexgroup = source.indexOf(group);
		int indextoken = source.indexOf(token);
		
		if (indextoken != -1){
			if (indextoken < indexkid){
				if (indexgroup > indextoken){
					if (indexkid < indexgroup){					
						exist = true;
					}
				}
			}
			else{
				String query = source.substring(indextoken);
				indexkid = query.indexOf(kid);
				if (indexkid != -1){
					exist = true;
				}
			}
		}
		
		return exist;
	}

	public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

    public int check(CommonTree token, int operand) {
		    	
		int thereIs = 0;
		String comparetoken = "EVENT_PROP_SIMPLE";
		CommonTree arbol = token;
		
		if (arbol.toString() != "EPL_EXPR"){			
			while (arbol.parent.toString() != "EPL_EXPR"){
				arbol = arbol.parent;
			}
			arbol = arbol.parent;
		}
		
		// To check if exist the token
		String source = arbol.toStringTree();
		
		if (token.getChildren() != null){
				
			List children = token.getChildren();
			int size = children.size();
			
			for (int i = 0; i < size; i++){
				if (children.get(i).toString().equals(comparetoken) && token.parent.toString().equals(RGRtoken)){
					CommonTree kid = (CommonTree) children.get(0);
					kid = (CommonTree) kid.getChild(0);
					if (IsInQuery(kid.toString(), source, "SELECTION_EXPR")){
						thereIs = 1;
					} 
					else{
						if (IsInQuery(kid.toString(), source, "ORDER_BY_EXPR")){
							thereIs = 1;
						}
					}
				}
			}			
		}
		
		return thereIs;
	}

	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		AggregateFunct(tree.getChild(0), attribute);
		Tree node = tree.getChild(0).getChild(0); //To conserve the child
		tree = tree.parent;
		List kidsGB = tree.getChildren();
		
		if (kidsGB.size() > 1){
			for (int i = 0; i < kidsGB.size(); i++){
				if (node.equals(tree.getChild(i).getChild(0).getChild(0))){
					tree.deleteChild(i);
				}
			}
		}
		else {
			
			tree = tree.parent;
			List kids = tree.getChildren();
			
			for (int i = 0; i < kids.size(); i++){
				if (kids.get(i).toString() == RGRtoken){
					tree.deleteChild(i);
				}
			}			
		}		
	}

	private void AggregateFunct(Tree tree, int attribute) {

		CommonTree arbol = (CommonTree) tree;
		
		if (arbol.toString() != "EPL_EXPR"){			
			while (arbol.parent.toString() != "EPL_EXPR"){
				arbol = arbol.parent;
			}
			arbol = arbol.parent;
		}
		
		String source = arbol.toStringTree();
		
		if (IsInQuery(tree.getChild(0).toString(), source, "SELECTION_EXPR")){
			List children = arbol.getChildren();
			for (int i = 0; i < children.size(); i++){
				if (children.get(i).toString() == "SELECTION_EXPR"){
					Tree child = arbol.getChild(i);
					int number = child.getChildCount();
					for (int j = 0; j < number; j++){
						if (child.getChild(j).getChild(0) != null && child.getChild(j).getChild(0).getChild(0) != null){
							if (tree.getChild(0).toString().equals(child.getChild(j).getChild(0).getChild(0).getChild(0).toString())){
								AddFunctionSelec(child.getChild(j).getChild(0).getChild(0).getChild(0), attribute);
							}
						}
					}
				}
			}
		}
		if (IsInQuery(tree.toString(), source, "ORDER_BY_EXPR")){
			List children = arbol.getChildren();
			for (int i = 0; i < children.size(); i++){
				if (children.get(i).toString() == "ORDER_BY_EXPR"){
					Tree child = arbol.getChild(i);
					int number = child.getChildCount();
					for (int j = 0; j < number; j++){
						if (tree.getChild(0).toString().equals(child.getChild(j).getChild(0).getChild(0).getChild(0).toString())){
							AddFunctionOrder(child.getChild(j).getChild(0).getChild(0).getChild(0), attribute);
						}
					}
				}
			}
		}		
	}

	private void AddFunctionSelec(Tree child, int attribute) {
		String parent = "SELECTION_ELEMENT_EXPR";
		CommonTree node = (CommonTree) child;
		
		while (node.parent.toString() != parent){
			node = node.parent;
		}
		node = node.parent;
		
		List children = node.getChildren();
		CommonToken function = new CommonToken(type[attribute - 1], aggfunction[attribute - 1]);
		CommonTree agfunction = new CommonTree(function);
		agfunction.addChildren(children);
		node.addChild(agfunction);
		node.deleteChild(0);
	}

	private void AddFunctionOrder(Tree child, int attribute) {
		String parent = "ORDER_ELEMENT_EXPR";
		CommonTree node = (CommonTree) child;
		
		while (node.parent.toString() != parent){
			node = node.parent;
		}
		node = node.parent;
		
		List children = node.getChildren();
		CommonToken function = new CommonToken(type[attribute - 1], aggfunction[attribute - 1]);
		CommonToken f1 = new CommonToken(202, "LIB_FUNC_CHAIN");
		CommonToken f2 = new CommonToken(201, "LIB_FUNCTION");
		CommonTree agfunction = new CommonTree(function);
		CommonTree af1 = new CommonTree(f1);
		CommonTree af2 = new CommonTree(f2);
		af2.addChildren(children);
		af1.addChild(af2);
		agfunction.addChild(af1);
		node.addChild(agfunction);
		node.deleteChild(0);
	}


	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		return queryorg.toLowerCase().indexOf("group by");
	}
}