package operators;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class which aggregates all the constants for every mutation operator.
 * Normally, these refer to the way the attribute values are understood. All
 * values are taken modulus their range, to alleviate the genetic algorithm from
 * having to learn the range of each operator's understood attribute values.
 *
 * @author Lorena Gutiérrez-Madroñal
 * 
 */
public final class OperatorConstants {

	private OperatorConstants() {}

    public static final String[] OPERATOR_NAMES    = {"PNR",
    	"POM", "PRE", "POC", "PGR", "PFP", "WLM", "WTM", "WBL", 
    	"WBT", "RLO", "RTU", "RAF", "RAO", "RRO", "RNO", "RJR", 
    	"RBR", "RGR", "RNW", "ROM", "ROS", "RLM", "RLA", "RAW", 
    	"RWB", "RRR1", "RRR2", "RSR1", "RSR2", "RSR3", "RSC", 
    	"IRC", "INC"};
    
    /* OPERATOR MAP */

	private static HashMap<String, Operator> operatorMap;

	/**
	 * Returns the map from all operator names to its Operator object.
	 * @return Map from operator name to {@link Operator} object. The
	 * elements in the map can be traversed with {@link Map#keySet()}
	 * in the same order as in {@link #OPERATOR_NAMES}.
	 * 
	 * This method is thread-safe.
	 */
	public static Map<String, Operator> getOperatorMap() {
		synchronized(OperatorConstants.class) {
			if (operatorMap == null) {
								
				// Need a LinkedHashMap to extract entries in the same order they were
				// inserted. HashMap doesn't have that property, and TreeMap is slower.
				operatorMap = new LinkedHashMap<String, Operator>();

				for (String opName : OperatorConstants.OPERATOR_NAMES) {
					operatorMap.put(opName.toLowerCase(), operators.UtilOperators.getOperator(opName));
				}
			}
			return operatorMap;
		}
	}

}