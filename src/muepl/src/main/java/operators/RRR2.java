package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * ESIR operator 
 * 
 * Syntaxes: SINGLE_ROW_FUNC (expression...)
 * Grammar syntaxes: single_row_func (EVENT_PROP_EXPR...
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class RRR2 implements Operator {

    private final String name;
    private final String[] RRRTokenscount = {"(prev ", "prevtail", "prior"};
    private final String[] RRRTokens = {"prev", "prevtail", "prior"};
    private final int[] RRRtype = {69, 70, 73};
    private final int NumOperators = 2;  
    
    public RRR2(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	for (int i = 0; i < RRRTokenscount.length; i++){
       		while (query.contains(RRRTokenscount[i])){
    			query = query.substring(query.indexOf(RRRTokenscount[i]) + RRRTokenscount[i].length());
    			count++;
    		}
       		query = source;
    	}

        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

	public int check(CommonTree token, int operand) {
		
		String t = token.toString();
		int thereIs = 0;
		
		for (int i = 0; i < RRRTokens.length; i++){
			if (t.equals(RRRTokens[i])){
				thereIs = 1;
			}
		}
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo ESIR
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		int position = CheckOperatorInVector(tree.toString());
		List children = tree.getChildren();
		
		if (position == 1){
			attribute++;
			if (attribute == RRRTokens.length){
				attribute = 0;
			}
		}
		
		if (position == 2){
			attribute--;
		}
		
		String token = RRRTokens[attribute];
		int type = RRRtype[attribute];
		CommonToken t = new CommonToken(type, token);
		CommonTree node = new CommonTree(t);
		CommonTree parent = (CommonTree) tree.getParent();
		parent.insertChild(0, node);
		node.addChildren(children);
		int indextree = tree.childIndex;
		parent.deleteChild(indextree);

	}

	private int CheckOperatorInVector(String operator) {
		int position = 0;
		
		for (int i = 0; i < RRRTokens.length; i++){
			if (RRRTokens[i].equals(operator)){
				position = i;
			}				
		}
		
		return position;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}