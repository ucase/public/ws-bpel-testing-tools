package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * EAGR operator 
 * 
 * Syntaxes: AGGREGATE_FUNC expression...
 * Grammar syntaxes: aggregate_func (EVENT_PROP_EXPR...
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */


public class RAF implements Operator {

    private final String name;
    private final String[] RAFTokens = {"max", "min", "avg", "sum", " count", "median", "stddev", "avedev"};
    private final String[] RAFTokensPosition = {"max(", "min(", "avg(", "sum(", " count(", "median(", "stddev(", "avedev("};
    private final int[] typeRAFTokens = {20, 21, 19, 18, 26, 23, 24, 25}; 
    private final String[] addtokens = {"distinct"};
    private final int[] addtypeTokens = {46};
    private final int NumSELOperators = 15; // (Keywords - 1)  
    
    public RAF(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	int countop = 0;
    	String query = source;
    	String[] Followtoken = {" (EVENT_PROP_EXPR", " distinct", " all"};
    	
    	for (int i = 0; i < RAFTokens.length; i++){
    		for (int j = 0; j < Followtoken.length; j++){    			
    			while (query.indexOf(RAFTokens[i].toString() + Followtoken[j]) > -1){
    				query = query.substring(query.indexOf(RAFTokens[i].toString() + Followtoken[j]) + RAFTokens[i].toString().length() + Followtoken[j].length());
    				countop++;
    			}
    			query = source;
    			count += countop;
    			countop = 0;
    		}
    	}

        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumSELOperators;
    }

	public int check(CommonTree token, int operand) {
		
		String t = token.toString();
		int thereIs = 0;
		
		if (token.getType() == 201){
			for (int i = 0; i < token.getChildCount() - 1; i++){
				for (int j = 0; j < RAFTokens.length; j++){
					if (token.getChild(i).toString().equals(RAFTokens[j])){
						thereIs = 1;
					}
				}
			}
		}

		return thereIs;
	}
	
	/**
	 * Función para realizar la mutación de un nodo específico de tipo EAGR
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();

		int positionOperatorInQuery = findKeywordInTree(children, RAFTokens);  // -1 si no existe el valor en el arbol
		int positionAdditionalTokenInQuery = findKeywordInTree(children, addtokens);
		
		int position = CheckOperatorInVector(children);
		
		if (CheckDistinct(children)){
			position = (RAFTokens.length) + position;
		}
		
		if (position <= attribute){
			attribute += 1;
			if (attribute == NumSELOperators + 1){
				if (position == 0){
					attribute = 1;
				} else {
					attribute = 0;
				}
			}
		}
		
		int[] type = InfoToken(attribute);
				
		CommonToken t = new CommonToken(type[0], RAFTokens[attribute % RAFTokens.length]);
		CommonTree node = new CommonTree(t);
		tree.insertChild(positionOperatorInQuery, node);
		tree.deleteChild(positionOperatorInQuery - 1);
		
		if (type[1] == -1)
		{
			if(positionAdditionalTokenInQuery != -1) // hay que eliminar el hijo correspondiente al token adicional
			{
				tree.deleteChild(positionAdditionalTokenInQuery);
			}
		} 
		else 
		{
			if(positionAdditionalTokenInQuery == -1) // En la consulta original no hay token inicial
			{
				CommonToken tadd = new CommonToken(type[1], addtokens[0]);
				CommonTree nodeadd = new CommonTree(tadd);
				tree.insertChild(positionOperatorInQuery, nodeadd);			
			}
			else //Sustituimos un token adicional por otro como en el codigo del operador
			{
				CommonToken tadd = new CommonToken(type[1], addtokens[0]);
				CommonTree nodeadd = new CommonTree(tadd);
				tree.insertChild(positionAdditionalTokenInQuery, nodeadd);
				tree.deleteChild(positionAdditionalTokenInQuery);
			}
		}		
	}

	private boolean CheckDistinct(List children) {
		
		String kid = "distinct";
		boolean exist = false;
		
		for (int i = 0; i < children.size() - 1; i++){
			if (children.get(i).toString().equals(kid)){
				exist = true;
			}
		}
		return exist;
	}

	private int CheckOperatorInVector(List children) {
		int position = -1;
		
		for (int i = 0; i < RAFTokens.length - 1; i++){
			for (int j = 0; j < children.size() - 1; j++){
				if (children.get(j).toString().equals(RAFTokens[i])){
					position = i;
				}
			}				
		}
		return position;
	}

	/**
	 * 
	 * @param attribute
	 * @return Un array donde el primer elemento es el token y el segundo es el token la palabra opcional [all | distinct] y -1 si no tiene
	 */
	private int [] InfoToken(int attribute) {
		int [] pos = new int [2];
		
		pos[0] = attribute % RAFTokens.length;
		pos[1] = (attribute / RAFTokens.length) - 1;
		
		pos[0] = typeRAFTokens[pos[0]];
		if (pos[1] != -1){
			pos[1] = addtypeTokens[0];
		}
		return pos;
	}
	
	private int findKeywordInTree(List children, String[] values)
	{
		int position = -1;
		
		for (int i = 0; i < values.length - 1; i++){
			for (int j = 0; j < children.size() - 1; j++){
				if (children.get(j).toString().equals(values[i])){
					position = j;
				}
			}				
		}
		
		if (position != -1){
			position++;
		}
		
		return position;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		
		int positionX = queryorg.length();
		int positionXFinal = 0;
		int i = 0;
		int whatfunction = 0;
		int prevfunction = 0;
		int iteration = 0;
		int distance = 0;
		String query = queryorg.toLowerCase();

		while (i < counter) {
			for (String function : RAFTokensPosition) {
				distance = query.indexOf(function);
				if (distance != -1 && positionX > distance) {
					positionX = distance;
					whatfunction = iteration;
				}
				iteration++;
			}
			query = query.substring(positionX + RAFTokensPosition[whatfunction].length());
			if (positionXFinal == 0) {
				positionXFinal += positionX;
				prevfunction = whatfunction;
				positionX = query.length();
			} else {
				positionXFinal += positionX + RAFTokensPosition[prevfunction].length();
				prevfunction = whatfunction;
				positionX = query.length();
			}
			i++;
			iteration = 0;
		}
		
		return positionXFinal;
	}
}