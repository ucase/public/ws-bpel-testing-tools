package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * OEDDP operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class POM implements Operator {
	
    private final String name;
    private final String[] tokens = {"DAY_PART", "HOUR_PART", "MINUTE_PART", "SECOND_PART", "MILLISECOND_PART"};
    private final int NumOperators = 2;   
    
    public POM(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	String checkOp = "OBSERVER_EXPR";
    	if (query.contains(checkOp)){
    		String[] parts = query.split(checkOp);
    		count = parts.length - 1;
    	}
    	
    	return count;    
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		
		if (token.getChildren() != null){
			
			if (token.getType() == 155){ //OBSERVER_EXPR
				CommonTree timerkid = (CommonTree) token.getChild(0);
				if (timerkid.getText().equals("timer")){
					thereIs = 1;
				}
			}
		}
		
		return thereIs;
	}
	
	/**
	 * Función para realizar la mutación de un nodo específico de tipo OEDDP
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		CommonTree Tperiodparent = (CommonTree) tree.getChild(2);
		List children = Tperiodparent.getChildren();
		int size = children.size();
		boolean change = false;
		
		for (int i = 0; i < size; i++){
			if (!change){
				
				if (IsTheOperator(children.get(i).toString())){
					CommonTree subtree = (CommonTree) children.get(i);
					List kids = subtree.getChildren();
					String incr = kids.get(0).toString();
					int num = Integer.parseInt(incr);
					num = AttributeOperation(num, attribute);
					incr = String.valueOf(num);
					CommonToken t = new CommonToken(355, incr);
					CommonTree node = new CommonTree(t);
					subtree.insertChild(i, node);
					subtree.deleteChild(i + 1);
					change = true;
				}
			}
		}
	}

	private int AttributeOperation(int num, int attribute) {
		if (attribute == 1){
			num++;
		} else {
			num--;
		}
		return num;
	}

	private boolean IsTheOperator(String op) {
		
		boolean exist = false;
		
		for (int i = 0; i < tokens.length; i ++){
			if (tokens[i].equals(op)){
				exist = true;
			}
		}

		return exist;
	}


	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		int positionX = 0;
		int i = 0;
		
		positionX = queryorg.toLowerCase().indexOf("pattern");
		String query = queryorg.toLowerCase().substring(positionX);
		
		while (i < counter) {
			positionX += query.indexOf("timer:interval");
			query = query.substring(query.indexOf("timer:interval") + "timer:interval".length());
			i++;
		}
		
		return positionX;
	}

}