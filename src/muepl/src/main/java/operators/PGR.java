package operators;

import java.util.List;

import org.antlr.runtime.tree.CommonTree;


/**
 * PGR operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class PGR implements Operator {
	
    private final String name;
    private final String tokenPGR = "GUARD_EXPR";
    private final String [] toRemoveTokens = {"timer", "within"};
    private final int NumOperators = 1;   
    
    public PGR(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    		
		while (query.indexOf(tokenPGR) > -1){
			query = query.substring(query.indexOf(tokenPGR) + tokenPGR.length());
			count++;
		}
    	
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		
		if (token.toString().equals(tokenPGR)){
			thereIs = 1;
		}

		return thereIs;
	}
	
	/**
	 * Función para realizar la mutación de un nodo específico de tipo RGEP
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		int indextoken = tree.childIndex;
		CommonTree parent = tree.parent;
		List children = tree.getChildren();
		int size = children.size();
		
		for (int i = 0; i < size; i++){
			if(RemoveToken(children.get(i).toString())){
				tree.deleteChild(i);
				children = tree.getChildren();
				size = children.size();
			}
		}
		
		parent.addChildren(children);
		parent.deleteChild(indextoken);
	}

	private boolean RemoveToken(String child) {
		boolean equal = false;
		
		for (int i = 0; i < toRemoveTokens.length; i++){
			if (toRemoveTokens[i].equals(child)){
				equal = true;
			}
		}
		return equal;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}

}