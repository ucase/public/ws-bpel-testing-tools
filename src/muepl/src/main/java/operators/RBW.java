package operators;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * ELKECB operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.1
 * 
 * SYNTAXES: test_expression [not] like pattern_expression [escape string_literal]
 * 
 */

public class RBW implements Operator {

    private final String name;
    private final String[] ELKECBtoken = {"like", "NOT_LIKE"};
    private final int NumOperators = 1;
    private static int numoperand = 0;
    
    public RBW(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	for (int i = 0; i < ELKECBtoken.length; i++){
       		while (query.contains(ELKECBtoken[i])){
    			query = query.substring(query.indexOf(ELKECBtoken[i]) + ELKECBtoken[i].length());
    			count = HasWildcars(query);
    		}
    	}

    	return count;
    }

	private int HasWildcars(String query) {
		
		String pattern = "";
		int count = 0;
		query = query.substring(query.indexOf("'") + 1);
		pattern = query.substring(0, query.indexOf("'"));
		String pattern2 = pattern;
		
		while (pattern.contains("_")){
			if (pattern.indexOf("_") != 0){
				if (pattern.substring(pattern.indexOf("_") - 1, pattern.indexOf("_")) != "%"){					
					pattern = pattern.substring(pattern.indexOf("_") + 1);
					count++;
				}
			} else{
				pattern = pattern.substring(pattern.indexOf("_") + 1);
			}
		}
		
		while (pattern2.contains("%")){
			if (pattern2.indexOf("%") != 0){
				if (pattern2.substring(pattern2.indexOf("%") - 1, pattern2.indexOf("%")) != "_"){					
					pattern2 = pattern2.substring(pattern2.indexOf("%") + 1);
					count++;
				}
			} else{
				pattern2 = pattern2.substring(pattern2.indexOf("%") + 1);
			}
		}
		
		return count;
	}

	public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

    public int check(CommonTree token, int operand) {
		    	
    	int thereIs = 0;
		
		for (int j = 0; j < ELKECBtoken.length; j++){					
			if (token.toString().equals(ELKECBtoken[j])){
				thereIs += HasWildcars(token.getChild(1).toString());
				numoperand += thereIs;
				
				if (thereIs > operand){
					thereIs = operand;
					numoperand = operand;
				}
				
			}
		}
		
		return thereIs;
	}
    
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		Tree kid = tree.getChild(1);
		String pattern = kid.toString();
		String finalkid = "";
		String[] wildcards = {"%", "_"}; 
		int count = 0;
		
		while (count != numoperand){
			int index1 = pattern.indexOf("%");
			int index2 = pattern.indexOf("_");
			
			while (index1 == 1 || index2 == 1){
				finalkid = pattern.substring(0, 2);
				pattern = pattern.substring(2);
				index1 = pattern.indexOf("%");
				index2 = pattern.indexOf("_");
			}
			
			if (index1 != -1 && index2 != -1){
				if (index1 < index2){
					count++;
					if (finalkid == ""){						
						finalkid = pattern.substring(0, pattern.indexOf(wildcards[0]) + 1);
					} else{
						finalkid = finalkid + pattern.substring(0, pattern.indexOf(wildcards[0]) + 1);
					}
					pattern = pattern.substring(pattern.indexOf(wildcards[0]) + 1);
				} else {
					count++;
					if (finalkid == ""){						
						finalkid = pattern.substring(0, pattern.indexOf(wildcards[1]) + 1);
					} else{
						finalkid = finalkid + pattern.substring(0, pattern.indexOf(wildcards[1]) + 1);
					}
					pattern = pattern.substring(pattern.indexOf(wildcards[1]) + 1);
				}
			} else {
				if (index1 != -1){
					count++;
					if (finalkid == ""){						
						finalkid = pattern.substring(0, pattern.indexOf(wildcards[0]) + 1);
					} else{ //TODO No lo termina de hacer bien, mirar especialmente esta parte
						finalkid = finalkid + pattern.substring(0, pattern.indexOf(wildcards[0]) + 1);
					}
					pattern = pattern.substring(pattern.indexOf(wildcards[0]) + 1);
				}
				if (index2 != -1){
					count++;
					if (finalkid == ""){						
						finalkid = pattern.substring(0, pattern.indexOf(wildcards[1]) + 1);
					} else{
						finalkid = finalkid + pattern.substring(0, pattern.indexOf(wildcards[1]) + 1);
					}
					pattern = pattern.substring(pattern.indexOf(wildcards[1]) + 1);
				}
			}
		}
		
		String wildcard = finalkid.substring(finalkid.length() - 1);
		
		finalkid = finalkid.substring(0, finalkid.length() - 2) + wildcard + pattern;
		
		CommonToken t = new CommonToken(277, finalkid);
		CommonTree node = new CommonTree(t);
		tree.addChild(node);
		tree.deleteChild(1);		
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}