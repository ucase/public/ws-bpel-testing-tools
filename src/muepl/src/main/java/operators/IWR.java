package operators;

import java.util.List;

import org.antlr.runtime.tree.CommonTree;

/**
 * AOR operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class IWR implements Operator {

    private final String name;
    private final String WCRWtoken = "WHERE_EXPR";
    private final int NumWCRWOperators = 1;
    
    public IWR(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	int indexpattern = query.indexOf("pattern");
    	
    	if (indexpattern != -1){
    		int finpattern = source.substring(indexpattern).indexOf(")");
    		query = source.substring(0, indexpattern) + source.substring(finpattern);
    	}
    	
    	while (query.contains(WCRWtoken)){
    		query = query.substring(query.indexOf(WCRWtoken) + WCRWtoken.length());
    		count++;
    	}
    	
    	return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumWCRWOperators;
    }

    public int check(CommonTree token, int operand) {
		    	
		int thereIs = 0;
		
		if (token.getChildren() != null){
				
			List children = token.getChildren();
			int size = children.size();
			
			for (int i = 0; i < size; i++){
				if (children.get(i).toString().equals(WCRWtoken)){
					thereIs = 1;
				}
			}			
		}
		
		return thereIs;
	}

	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();
		int size = children.size();
		boolean delete = false;
		
		for (int i = 0; i < size; i++){
			if (!delete){				
				if (IsTheOperator(children.get(i).toString())){
					tree.deleteChild(i);
					delete = true;
				}
			}
		}	
	}

	private boolean IsTheOperator(String op) {
		
		boolean exist = false;
		
		if (WCRWtoken.equals(op)){
			exist = true;
		}

		return exist;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		return queryorg.toLowerCase().indexOf("where");
	}
}