package operators;

import java.util.Vector;

import epl.util.Pair;

public class UtilOperators {

	public static Operator getOperator(String name) {
		
		Operator op = new DummyOperator(name);
		
		if (name == "PRE"){
			op = new PRE(name);
		}
		else{
			if (name == "POC"){
				op = new POC (name);
			}
		else{
			if (name == "RLO"){
				op = new RLO (name);
			} 
		else{
			if (name == "PNR"){
				op = new PNR (name);
			} 
		else{
			if (name == "PGR"){
				op = new PGR (name);
			}
		else{
			if (name == "POM"){
				op = new POM (name);
			} 
		else{
			if (name == "PFP"){
				op = new PFP (name);
			} 
		else{
			if (name == "WLM"){
				op = new WLM (name);
			} 
		else{
			if (name == "WTM"){
				op = new WTM (name);
			} 
		else{
			if (name == "RTU"){
				op = new RTU (name);
			} 
		else{
			if (name == "WBL"){
				op = new WBL (name);
			} 
		else{
			if (name == "WBT"){
				op = new WBT (name);
			}  
		else{
			if (name == "RAO"){
				op = new RAO (name);
			}
		else{
			if (name == "RAF"){
				op = new RAF (name);
			}
		else{
			if (name == "RRO"){
				op = new RRO (name);
			}
		else{
			if (name == "RNO"){
				op = new RNO (name);
			}
		else{
			if (name == "RJR"){
				op = new RJR (name);
			}
		else{
			if (name == "RBR"){
			op = new RBR (name);
		} 
		else{
			if (name == "RGR"){
				op = new RGR (name);
			}
		else{
			if (name == "RRR1"){
				op = new RRR1 (name);
			}
		else{
			if (name == "RRR2"){
				op = new RRR2 (name);
			}
		else{
			if (name == "RSC"){
				op = new RSC (name);
			}
		else{
			if (name == "ROS"){
				op = new ROS (name);
			}
		else{
			if (name == "ROM"){
				op = new ROM (name);
			}
		else{
			if (name == "RSR1"){
				op = new RSR1 (name);
			}
		else{
			if (name == "RSR2"){
				op = new RSR2 (name);
			}
		else{
			if (name == "RSR3"){
				op = new RSR3 (name);
			}
		else{
			if (name == "RLM"){
				op = new RLM (name);
			}
		else{
			if (name == "RBW"){
				op = new RBW (name);
			}
		else{
			if (name == "RAW"){
				op = new RAW (name);
			}
		else{
			if (name == "RLA"){
				op = new RLA (name);
			}
		else{
			if (name == "IRC"){
				op = new IWR (name);
			} 
		else{
			if (name == "INC"){
				op = new ICN (name);
			}
		else{
			if (name == "RNW"){
				op = new RNW (name);
			}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}
				
		return op;
	}
	/**
	 * Function which return the first operator according to a input string and a possible operators list
	 * @param source input string
	 * @param operators possible operators
	 * @return -1 in the Pair.second if there is not a current, in otherwise the first operator position
	 * in the string and in the Pair.first the number of the last operator in the operators vector
	 */
	
	public static Pair<Integer,Integer> getFirstOperator(String subsource, String source, String ... operators)
	{
		Vector<Integer> pos = new Vector<Integer>();
		for(String op : operators)
		{
			pos.add(subsource.indexOf(op));
		}
		
		Pair<Integer,Integer> resultado = new Pair<Integer,Integer>(0,pos.elementAt(0));
		
		for(int i = 1; i < pos.size(); i++) {
			if((pos.elementAt(i) < resultado.getSecond() &&
					pos.elementAt(i) != -1) ||(resultado.getSecond() == -1 && pos.elementAt(i) != -1)) {
				resultado.setFirst(i);
				resultado.setSecond(pos.elementAt(i));
			}
		}

		return resultado;
	}
	
	/**
	 * Function which return a vector with the pairs of operators (Num operator, Index operator)
	 * according to a input string and a possible operators list
	 * @param source Input string
	 * @param operators Possible operators
	 * @return Ascending vector according to the index. 
	 */
	
	public static Vector<Pair <Integer, Integer> > getAllOperators(String source, String ... operators)
	{
		Vector<Pair <Integer,Integer> > result = new Vector< Pair<Integer, Integer> >();
		String subsource = source;
		Pair <Integer, Integer> pairOperator = getFirstOperator(subsource, source, operators);
		
		while (pairOperator.getSecond() != -1) {

			Pair<Integer,Integer>pairOperatorModified = new Pair<Integer,Integer>();
			pairOperatorModified.setFirst(pairOperator.getFirst());
			pairOperatorModified.setSecond(pairOperator.getSecond() + source.length() - subsource.length());
			
			result.add(pairOperatorModified);
			
			subsource = subsource.substring(pairOperator.getSecond() + operators[pairOperator.getFirst()].length());
			pairOperator = getFirstOperator(subsource, source, operators);
		}
		
		return result;
	}
}
