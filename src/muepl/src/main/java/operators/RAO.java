package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;

/**
 * EAOR operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class RAO implements Operator {

    private final String name;
    private final String[] RAOOperators = {"-", "+", "*", "/", "%"} ;
    private final int NumEAOROperators = 4;
    
    public RAO(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	int numop, countop;
    	String query = source;
    	int indexpattern = query.indexOf("pattern");
    	
    	if (indexpattern != -1){
    		int finpattern = source.substring(indexpattern).indexOf(")");
    		query = source.substring(0, indexpattern) + source.substring(finpattern);
    	}
    	
    	for (numop = 0; numop < NumEAOROperators; numop++){
    		countop = 0;
    		query = source;
    	
    		// wildcard * 
    		// SELECT * || .* || count(*) || (*/15, 8:17, *, *, *) || |* || as * || *? || pattern (A B* C) /* .. */
    		
			while (query.indexOf(RAOOperators[numop]) > -1) {
				if (RAOOperators[numop] == "*") {
					int index = query.indexOf("*");
					String subquery = query.substring(index - 15, index - 1);
					String subqueryas = query.substring(index - 3, index - 1);
					String shortquerybefore = query.substring(index - 1, index);
					String shortqueryafter = query.substring(index + 1, index + 2);
					
					if (subquery.equals("SELECTION_EXPR") || subquery.equals("SUBSELECT_EXPR") || subqueryas.equals("as") || 
							shortquerybefore.equals(".") ||	shortquerybefore.equals("(") || shortquerybefore.equals("|") || 
							shortquerybefore.equals("/") ||	shortqueryafter.equals("?") || shortqueryafter.equals(",") || 
							shortqueryafter.equals("/") || shortqueryafter.equals(")")) {
						query = query.substring(index + 1);
					}
					else{						
						query = query.substring(index + RAOOperators[numop].length(), query.length());
						countop++;
					}
				}
				else {
					query = query.substring(query.indexOf(RAOOperators[numop]) + RAOOperators[numop].length(), query.length());
					countop++;
				}
			}
			count += countop;
    	}
    	
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumEAOROperators;
    }

    public int check(CommonTree token, int operand) {
		    	
		int thereIs = 0;
		
		if (token.getChildren() != null){
			
			if (!IsWildcard(token)) {
				
				List children = token.getChildren();
				String t = children.toString();
				int size = children.size();
				
				for (int i = 0; i < size; i++){
					for (int j = 0; j < RAOOperators.length; j++){
						if (children.get(i).toString().equals(RAOOperators[j])){
							thereIs = 1;
						}
					}		
				}
			}			
		}
		
		return thereIs;
	}

	private boolean IsWildcard(CommonTree token) {
		
		//TODO Añadir los que vayan saliendo...
		
		boolean wildcard = false;
		String [] fathers = {"SELECTION_EXPR", "SUBSELECT_EXPR", "ACCESS_AGG"};
		
		for (int i = 0; i < fathers.length; i++){			
			if (token.toString().equals(fathers[i])){
				wildcard = true;
			}
		}
		return wildcard;
	}

	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();
		int size = children.size();
		int type = 277;
		attribute -= 1;
		
		for (int i = 0; i < size; i++){
			if (IsTheOperator(children.get(i).toString())){
				
				int position = CheckOperatorInVector(children.get(i).toString());
				
				if (position <= attribute){
					attribute += 1; //It has to advance 1 position
				}
				
				CommonToken t = new CommonToken(type, RAOOperators[attribute]);
				CommonTree node = new CommonTree(t);
				
				// To save the children's children
				CommonTree child = (CommonTree) tree.getChild(i);
				node.addChildren(child.getChildren());
				tree.insertChild(i, node);
				tree.deleteChild(i + 1);
			}
		}	
	}
	private int CheckOperatorInVector(String operator) {
		
		int position = 0;
		
		for (int i = 0; i <= RAOOperators.length - 1; i++){
			if (RAOOperators[i].equals(operator)){
				position = i;
			}				
		}
		return position;
	}
	
	private boolean IsTheOperator(String op) {
		
		boolean exist = false;
		
		for (int i = 0; i < RAOOperators.length; i ++){
			if (RAOOperators[i].equals(op)){
				exist = true;
			}
		}
		return exist;
	}


	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		int positionX = queryorg.length();
		int positionXFinal = 0;
		int i = 0;
		int whatfunction = 0;
		int prevfunction = 0;
		int iteration = 0;
		int distance = 0;
		String query = queryorg;

		while (i < counter) {
			for (String function : RAOOperators) {
				distance = query.indexOf(function);
				if (RAOOperators[whatfunction].equals("*")) {
					distance = ScapeCharacter(distance, query);
				}
				if (distance != -1 && positionX > distance) {
					positionX = distance;
					whatfunction = iteration;
				}
				iteration++;
			}
			query = query.substring(positionX + RAOOperators[whatfunction].length());
			if (positionXFinal == 0) {
				positionXFinal += positionX;
				prevfunction = whatfunction;
				positionX = query.length();
			} else {
				positionXFinal += positionX + RAOOperators[prevfunction].length();
				prevfunction = whatfunction;
				positionX = query.length();
			}
			i++;
			iteration = 0;
		}
		
		return positionXFinal;
	}

	private int ScapeCharacter(int distance, String query) {
		
		String aroundquery = query.toLowerCase().substring(distance - 1, distance + 1);
		if (aroundquery.contentEquals("(*)")){
			distance = -1;
		}
		
		aroundquery = query.toLowerCase().substring(distance - "select  ".length(), distance);
		if (aroundquery.contains("select")) {
			distance = -1;
		}
		
		return distance;
	}
}