package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;

/**
 * SQNC operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class ICN implements Operator {

    private final String name;
    private final String WCNGtoken = "WHERE_EXPR";
    private final int NumWCNGOperators = 1;
    
    public ICN(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	int indexpattern = query.indexOf("pattern");
    	
    	if (indexpattern != -1){
    		int finpattern = source.substring(indexpattern).indexOf(")");
    		query = source.substring(0, indexpattern) + source.substring(finpattern);
    	}
    	
    	while (query.contains(WCNGtoken)){
    		query = query.substring(query.indexOf(WCNGtoken) + WCNGtoken.length());
    		count++;
    	}
    	
    	return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumWCNGOperators;
    }

    public int check(CommonTree token, int operand) {
		    	
		int thereIs = 0;
		
		if (token.getChildren() != null){
				
			List children = token.getChildren();
			int size = children.size();
			
			for (int i = 0; i < size; i++){
				if (children.get(i).toString().equals(WCNGtoken)){
					thereIs = 1;
				}
			}			
		}
		
		return thereIs;
	}

	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();
		int size = children.size();
		boolean add = false;
		
		for (int i = 0; i < size; i++){
			if (!add){				
				if (IsTheOperator(children.get(i).toString())){
					CommonToken fals = new CommonToken(13, "NOT_EXPR");
					CommonTree nodefalse = new CommonTree(fals);
					CommonTree node = (CommonTree) children.get(i);
										
					List kids = node.getChildren();
					nodefalse.addChildren(kids);
					
					while (node.getChildCount() != 0){
						node.deleteChild(0);
					}
					
					node.insertChild(0, nodefalse);
					add = true;
				}
			}
		}	
	}

	private boolean IsTheOperator(String op) {
		
		boolean exist = false;
		
		if (WCNGtoken.equals(op)){
			exist = true;
		}

		return exist;
	}


	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {    	    	
    	return queryorg.toLowerCase().indexOf("where");
	}
}