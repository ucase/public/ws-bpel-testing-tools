package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;

/**
 * ESUSBRIII operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class RSR3 implements Operator {

    private final String name;
    private final String RSR3Tokens = "EXISTS_SUBSELECT_EXPR";        
    private final int NumRSR3Operators = 1;  
    
    public RSR3(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	int countop = 0;
    	String query = source;
    	
    	for (int i = 0; i < RSR3Tokens.length(); i++){
    		while (query.indexOf(RSR3Tokens) > -1){
				query = query.substring(query.indexOf(RSR3Tokens) + RSR3Tokens.length());
				countop++;
			}
			query = source;
			count += countop;
			countop = 0;
    	}    		
    	    	
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumRSR3Operators;
    }

	public int check(CommonTree token, int operand) {
		
		String t = token.toString();
		int thereIs = 0;
		
		for (int i = 0; i < RSR3Tokens.length(); i++){
			if (t.equals(RSR3Tokens)){
				thereIs = 1;
			}
		}
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo ESUBRIII
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		CommonTree parent = (CommonTree) tree.getParent();
		List kids = parent.getChildren();

		if (parent.toString().equals("not")){
			parent = parent.parent;
			parent.deleteChild(0);
			parent.addChildren(kids);
		}
		else{
			CommonToken t = new CommonToken(13, "not");
			CommonTree node = new CommonTree(t);
			node.addChildren(kids);
			parent.insertChild(0, node);
			parent.deleteChild(1);
		}
	}


	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}