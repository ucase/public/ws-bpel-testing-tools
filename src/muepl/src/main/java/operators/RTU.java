package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * TRUN operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class RTU implements Operator {
	
    private final String name;
    private final String[] RTUtokens = {"DAY_PART", "HOUR_PART", "MINUTE_PART", "SECOND_PART", "MILLISECOND_PART"};
    private final int[] typeTokens = {210, 211, 212, 213, 214};
    private final int NumRTUOperators = 4;
    private final String[] RTUPositions = {"days", "hours", "minutes", " min", "seconds", " sec", "milliseconds"};
    		
    public RTU(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
		for (int i = 0; i < RTUtokens.length; i++){
			int contop = 0;
			while (query.indexOf(RTUtokens[i]) > -1){						
				if (query.indexOf(RTUtokens[i]) > -1){
					contop++;
					query = query.substring(query.indexOf(RTUtokens[i]) + RTUtokens[i].length());
				}
			}
			count += contop;
			query = source;
		}
		
    	return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumRTUOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		
		if (token.getChildren() != null){
			
			List children = token.getChildren();
	    	String t = children.toString();
	    	int size = children.size();
			
			for (int i = 0; i < size; i++){
				for (int j = 0; j < RTUtokens.length; j++){
					if (children.get(i).toString().equals(RTUtokens[j])){
						thereIs = 1;
					}
				}		
			}
		}
		
		return thereIs;

	}
	
	/**
	 * Función para realizar la mutación de un nodo específico de tipo TRUN
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();
		int size = children.size();
		attribute -=1;
		
		for (int i = 0; i < size; i++){
			if (IsTheOperator(children.get(i).toString())){
				
				int position = CheckOperatorInVector(children.get(i).toString());
				
				if (position <= attribute){
					attribute += 1; //It has to advance 1 position
				}
				
				int type = InfoToken(children.get(i).toString());
				CommonToken t = new CommonToken(type, RTUtokens[attribute]);
				CommonTree node = new CommonTree(t);
				
				// To save the children's children
				CommonTree child = (CommonTree) tree.getChild(i);
				node.addChildren(child.getChildren());
				tree.insertChild(i, node);
				tree.deleteChild(i + 1);		
			}			
		}
	}

	private int CheckOperatorInVector(String operator) {
		
		int position = 0;
		
		for (int i = 0; i <= RTUtokens.length - 1; i++){
			if (RTUtokens[i].equals(operator)){
				position = i;
			}				
		}
		return position;
	}

	private int InfoToken(String st) {
		int pos = 0;
		
		for (int i = 0; i < RTUtokens.length; i++){
			if (RTUtokens[i].equals(st)){
				pos = typeTokens[i];
			}
		}
		
		return pos;
	}
	private boolean IsTheOperator(String op) {
		
		boolean exist = false;
		
		for (int i = 0; i < RTUtokens.length; i ++){
			if (RTUtokens[i].equals(op)){
				exist = true;
			}
		}

		return exist;
	}
	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		int positionX = queryorg.length();
		int positionXFinal = 0;
		int i = 0;
		int whatfunction = 0;
		int prevfunction = 0;
		int iteration = 0;
		int distance = 0;
		String query = queryorg.toLowerCase();

		while (i < counter) {
			for (String function : RTUPositions) {
				distance = query.indexOf(function);
				if (RTUPositions[whatfunction].equals(" min") && !(distance == -1)) {
					distance = ScapeMin(distance, query);
				}
				if (RTUPositions[whatfunction].equals(" sec") && !(distance == -1)) {
					distance = ScapeSec(distance, query);
				}
				if (distance != -1 && positionX > distance) {
					positionX = distance;
					whatfunction = iteration;
				}
				iteration++;
			}
			query = query.substring(positionX + RTUPositions[whatfunction].length());
			if (positionXFinal == 0) {
				positionXFinal += positionX;
				prevfunction = whatfunction;
				positionX = query.length();
			} else {
				positionXFinal += positionX + RTUPositions[prevfunction].length();
				prevfunction = whatfunction;
				positionX = query.length();
			}
			i++;
			iteration = 0;
		}
		
		return positionXFinal + 2; //TODO Mirar por qué quita 2 posiciones
	}

	private int ScapeSec(int distance, String query) {
		String aroundquery = query.toLowerCase().substring(distance, distance + "seconds".length());
		if (aroundquery.contains("seconds")){
			distance = -1;
		}
		
		return distance;
	}

	private int ScapeMin(int distance, String query) {
		String aroundquery = query.toLowerCase().substring(distance, distance + "utes".length());
		if (aroundquery.contains("minutes")){
			distance = -1;
		}
		
		aroundquery = query.toLowerCase().substring(distance, distance + "min(".length());
		if (aroundquery.contains("min(")) {
			distance = -1;
		}
		
		return distance;
	}

}