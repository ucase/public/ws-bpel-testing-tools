package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * PRE operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.1
 * 
 * {@value child to erase "STREAM_EXPR", because each pattern is related to the every expressions and just appears in FROM expressions.}
 */

public class PRE implements Operator {
	
    private final String name;
    private final String tokenPRE = "PATTERN_INCL_EXPR";
    private final int NumOperators = 1;   
    
    public PRE(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    		
		while (query.indexOf(tokenPRE) > -1){
			query = query.substring(query.indexOf(tokenPRE) + tokenPRE.length());
			count++;
		}
    	
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		
		if (token.getChildren() != null){
			
			List children = token.getChildren();
			int size = children.size();
			for (int i = 0; i < size; i++){
				if (children.get(i).toString() == tokenPRE){
					thereIs = 1;
				}
			}			
		}
		
		return thereIs;
	}
	
	/**
	 * Función para realizar la mutación de un nodo específico de tipo RREP
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();
		int size = children.size();
		boolean delete = false;
		String filter = "";
		
		for (int i = 0; i < size; i++){
			if (!delete){
				if (IsTheOperator(children.get(i).toString())){
					CommonTree nodepattern = (CommonTree) children.get(i);
					while (!nodepattern.toString().toLowerCase().equals("every")){
						nodepattern = (CommonTree) nodepattern.getChild(0);
					}
					CommonTree nodefilter = (CommonTree) nodepattern.getChild(0);
					filter = nodefilter.getChild(1).getText();
					int index = nodepattern.childIndex;
					tree.deleteChild(index);
					CommonToken t = new CommonToken(139, "EVENT_FILTER_EXPR");
					CommonTree node = new CommonTree(t);
					CommonToken f = new CommonToken(277, filter);
					CommonTree nodef = new CommonTree(f);
					node.addChild(nodef);
					tree.insertChild(0, node);
					delete = true;
				}
			}
		}
	}

	private boolean IsTheOperator(String op) {
		
		boolean exist = false;
		
		if (tokenPRE.equals(op)){
			exist = true;
		}

		return exist;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		return queryorg.toLowerCase().indexOf("pattern");
	}

}