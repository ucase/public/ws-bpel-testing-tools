package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * ESUSBRI operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class RSR1 implements Operator {

    private final String name;
    private final String[] RSRTokensString = {" all (SUBSELECT", " any (SUBSELECT", " some (SUBSELECT"};
    private final String[] RSRTokens = {"all", "any", "some"};
    private final int[] RSRtype = {47, 48, 49};
    private final String[] RSR2Tokens = {"NOT_IN_SUBSELECT_EXPR", "IN_SUBSELECT_EXPR"};
    private final int[] RSR2type = {230, 229};
    private final String RSR3Tokens = "EXISTS_SUBSELECT_EXPR";
    private final int NumRSROperators = 6;  
    
    public RSR1(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	int countop = 0;
    	String query = source;
    	
    	for (int i = 0; i < RSRTokens.length; i++){
    		while (query.indexOf(RSRTokensString[i].toString()) > -1){
				query = query.substring(query.indexOf(RSRTokensString[i].toString()) + RSRTokensString[i].toString().length());
				countop++;
			}
			query = source;
			count += countop;
			countop = 0;
    	}    		
    	    	
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumRSROperators;
    }

	public int check(CommonTree token, int operand) {
		
		String t = token.toString();
		int thereIs = 0;
		
		for (int i = 0; i < RSRTokens.length; i++){
			if (t.equals(RSRTokens[i])){
				CommonTree parent = token.parent;
				int kids = parent.getChildCount();
				for (int j = 0; j < kids; j++){					
					if (parent.getChild(j).toString().contains("SUBSELECT")){
						thereIs = 1;						
					}
				}
			}
		}
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo ESUBRI
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		if (attribute == 1 || attribute == 2){
			attribute -= 1;
			int position = CheckOperatorInVector(tree.toString());
			System.out.println("position: " + position);
			
			if (position <= attribute){
				attribute += 1; //It has to advance 1 position
			}
			System.out.println("attribute: " + attribute);
			
			int type = InfoToken(RSRTokens[attribute]);
			CommonToken t = new CommonToken(type, RSRTokens[attribute]);
			CommonTree node = new CommonTree(t);
			CommonTree parent = (CommonTree) tree.getParent();
			
			for (int i = 0; i < parent.getChildCount(); i++){
				if (parent.getChild(i).toString().equals(RSRTokens[position])){				
					parent.deleteChild(i);
					parent.insertChild(i, node);
				}
			}
		}
		if (attribute == 3 || attribute == 4){
			int type = 277;
			
			if (attribute == 3){
				type = RSR2type[0];
				attribute = 0;
			}
			
			if (attribute == 4){
				type = RSR2type[1];
				attribute = 1;
			}
			
			//The new node
			CommonToken t = new CommonToken(type, RSR2Tokens[attribute]);
			CommonTree node = new CommonTree(t);
			CommonToken st = new CommonToken(231, "IN_SUBSELECT_QUERY_EXPR");
			CommonTree subnode = new CommonTree(st);
			node.addChild(subnode);

			CommonTree parent = tree.parent;
			CommonTree child = (CommonTree) parent.getChild(2);
			List kids = child.getChildren(); //To save the children
			subnode.addChildren(kids);
			Tree variable = parent.getChild(0);
			node.insertChild(0, variable);
			
			//Erase the old node
			parent = parent.parent;
			parent.deleteChild(0);
			parent.addChild(node);
		}
		if (attribute == 5 || attribute == 6){
			//The new node
			CommonTree parent = tree.parent;
			CommonTree child = (CommonTree) parent.getChild(2);
			List kids = child.getChildren();
			CommonToken existsT = new CommonToken(228, RSR3Tokens);
			CommonTree existsnode = new CommonTree(existsT);
			existsnode.addChildren(kids);
			
			parent = parent.parent;
			
			if (attribute == 5){
				parent.insertChild(0, existsnode);
				parent.deleteChild(1);
			}
			
			if (attribute == 6){
				CommonToken t = new CommonToken(13, "not");
				CommonTree node = new CommonTree(t);
				node.addChild(existsnode);
				parent.insertChild(0, node);
				parent.deleteChild(1);
			}
		}
	}

	private int CheckOperatorInVector(String operator) {
		int pos = 0;
		
		for (int i = 0; i < RSRTokens.length; i++){
			if (RSRTokens[i].equals(operator)){
				pos = i;
			}				
		}
			
		return pos;
	}
	
	private int InfoToken(String st) {
		int pos = 0;
		
		for (int i = 0; i < RSRTokens.length; i++){
			if (RSRTokens[i].equals(st)){
				pos = RSRtype[i];
			}
		}
		
		return pos;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}