package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;

/**
 * POC operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class PFP implements Operator {

    private final String name;
    private final String PFPTokens = "FOLLOWED_BY_EXPR";
    private final String[] PFPSwap = {"(", ")"};
    
    private final int NumSELOperators = 1;
    
    public PFP(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	while (query.indexOf(PFPTokens) > -1){
    		query = query.substring(source.indexOf(PFPTokens) + PFPTokens.length());
    		count++;
    	}
    	
        return count; 
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumSELOperators;
    }

	public int check(CommonTree token, int operand) {
		
		String t = token.toString();
		int thereIs = 0;
		
		if (t == PFPTokens){
			thereIs = 1;
		}
		
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo CEOP
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		// The pattern is (A -> B) with parenthesis
		if (tree.parent.toString().equals("every")){
			CommonTree grandpa = tree.parent.parent;
			CommonTree parent = tree.parent;
			grandpa.addChild(tree);
			int index = parent.childIndex;
			grandpa.deleteChild(index); //token every removed
			CommonTree first = (CommonTree) tree.getChild(0); //First FOLLOWED_BY_ITEM to include every token
			CommonTree firstchild = (CommonTree) first.getChild(0);
			CommonToken t = new CommonToken(14, "every");
			CommonTree node = new CommonTree(t);
			first.addChild(node); // Adding every token
			first.deleteChild(0);
			CommonTree every = (CommonTree) first.getChild(0);
			every.addChild(firstchild);
		}
		else { // The pattern is A -> B without parenthesis
			CommonTree first = (CommonTree) tree.getChild(0);
			CommonTree every = (CommonTree) first.getChild(0);
			CommonTree firstchild = (CommonTree) every.getChild(0); //The child which will be the child of first
			first.addChild(firstchild);
			first.deleteChild(0); // removing every
			CommonTree parent = tree.parent;
			CommonToken t = new CommonToken(14, "every");
			CommonTree node = new CommonTree(t);
			parent.addChild(node); // adding every as parent of FOLLOWED_BY_EXPR
			parent.deleteChild(0); // removing tree
			parent.getChild(0).addChild(tree); // adding the tree as child of the token every
		}

	}

	private CommonTree FindParenthesis(CommonTree tree) {
		
		List children = tree.getChildren();
		CommonTree poschild = null;
			for (int i = 0; i < children.size(); i++){
				System.out.println(children.get(i).toString());
				if (IsTheChild(children.get(i).toString())){
					poschild = (CommonTree) tree.getChild(i);
					System.out.println("IsTheChild " + children.get(i).toString());
					break;
				}
				else{
					poschild = FindParenthesis((CommonTree) tree.getChild(i));
				}
			}
		
		return poschild;
	}

	private boolean IsTheChild(String op) {

		boolean exist = false;
		
		if (PFPSwap[0].equals(op)){
			exist = true;
		} else {
			if (PFPSwap[1].equals(op)){
				exist = true;
			}
		}

		return exist;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		
		int positionX = 0;
		
		positionX = queryorg.indexOf("->");
		
		return positionX;
	}
}