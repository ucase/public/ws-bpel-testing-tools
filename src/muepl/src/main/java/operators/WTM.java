package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * TDEC operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class WTM implements Operator {
	
    private final String name;
    private final String[] tokens = {"DAY_PART", "HOUR_PART", "MINUTE_PART", "SECOND_PART", "MILLISECOND_PART"};
    private final int NumOperators = 2;   
    
    public WTM(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	String checkOp = "win time";
    	
    	while (query.indexOf(checkOp) > -1){
    		count++;
    		query = query.substring(query.indexOf(checkOp) + checkOp.length());
    	}

    	return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		
		if (token.getChildren() != null){
			
			List children = token.getChildren();
			int size = children.size();
			CommonTree parent = token.parent;
			
			if (parent != null){
				
				List brothers = parent.getChildren();
				
				if (HasTheBrother(brothers)){
					
					for (int i = 0; i < size; i++){
						for (int j = 0; j < tokens.length; j++){					
							if (children.get(i).toString().equals(tokens[j])){
								thereIs = 1;
							}
						}
					}
				}
			}
		}
		
		return thereIs;

	}
	
	private boolean HasTheBrother(List brothers) {
		String brother1 = "win";
		String brother2 = "time";
		String brother3 = "time_batch";
		boolean exist = false;
		
		for (int i = 0; i < brothers.size(); i++){
			if (brothers.get(i).toString().equals(brother1) && brothers.get(i + 1).toString().equals(brother2)){
				exist = true;
				break;
			}
			if (brothers.get(i).toString().equals(brother1) && brothers.get(i + 1).toString().equals(brother3)){
				exist = true;
				break;
			}
		}
		return exist;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo TDEC
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();
		int size = children.size();
		boolean change = false;
		
		for (int i = 0; i < size; i++){
			if (!change){
				if (IsTheOperator(children.get(i).toString())){
					int type = 277;
					CommonTree subtree = (CommonTree) children.get(i);
					List kids = subtree.getChildren();
					String tochange = kids.get(0).toString();
					boolean changed = false;
					if (isNumeric(tochange)){						
						int num = Integer.parseInt(tochange);
						if (attribute == 1){
							num++;
						} else {
							num--;
						}
						tochange = String.valueOf(num);
						changed = true;
					}
					if (isNumericFloat(tochange) && changed == false){
						float num = Float.parseFloat(tochange);
						if (attribute == 1){
							num++;
						} else {
							num--;
						}
						tochange = String.valueOf(num);
					}
					
					CommonToken t = new CommonToken(type, tochange);
					CommonTree node = new CommonTree(t);
					subtree.insertChild(i, node);
					subtree.deleteChild(i + 1);
					change = true;
				}
			}
		}
	}

	private boolean IsTheOperator(String op) {
		
		boolean exist = false;
		
		for (int i = 0; i < tokens.length; i ++){
			if (tokens[i].equals(op)){
				exist = true;
			}
		}

		return exist;
	}

	public static boolean isNumeric(String cadena){
    	try {
    		Integer.parseInt(cadena);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}
    }
    
    public static boolean isNumericFloat(String cadena) {
    	try {
    		Float.parseFloat(cadena);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}	
    }

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		return queryorg.indexOf("win:time");
	}
}