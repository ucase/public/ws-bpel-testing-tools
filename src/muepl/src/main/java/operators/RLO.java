package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * RLOP operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.2
 * 
 * Está limitada la búsqueda del operador de tal modo que en el árbol sintáctico tienen que aparecer
 * (RLOP ... si hay algún cambio hay que indicarlo en el recuento de operandos.
 * 
 */

public class RLO implements Operator {
	
    private final String name;
    private final String[] tokens = {"and", "EVAL_AND_EXPR", "or", "EVAL_OR_EXPR"};
    private final String[] tokensRLOP = {"(and ", "EVAL_AND_EXPR", "(or ", "EVAL_OR_EXPR"};
    private final String[] tokensEx = {"EVAL_AND_EXPR", "EVAL_OR_EXPR"}; 
    private final int[] typeRLOPEx = {162, 163};
    private final String[] RLOPosition = {" and ", " or "};
    
    private final int NumRLOPOperators = 1;  
    
    public RLO(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = "";
    	
    	for (int i = 0; i < tokensRLOP.length; i++){
    		int countop = 0;
    		query = source;
    		
    		while (query.indexOf(tokensRLOP[i]) > -1){   				
    				countop++;
    			query = query.substring(query.indexOf(tokensRLOP[i]) + tokensRLOP[i].length());
    		}
    		count += countop;
    	}
    	
    	return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumRLOPOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		
		if (!token.isNil()){
			for (int i = 0; i < tokens.length; i++){
				if (token.getText().equals(tokens[i])){
					thereIs = 1;
				}
			}
		}
		
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo RLOP
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {

		if (IsTheOperator(tree.getText())){
			int position = CheckOperatorInVector(tree.getText());
			int childpos = tree.childIndex;
			if (position > 1){
				attribute = 0;
			} else{
				attribute = 1;
			}
			
			CommonToken t = new CommonToken(typeRLOPEx[attribute], tokensEx[attribute]);
			CommonTree node = new CommonTree(t);
			
			if (coverage) {
				// The call to coverage function: coveragePoint()
				CommonToken andT = new CommonToken(162, "EVAL_AND_EXPR");
				CommonTree andNode = new CommonTree(andT);
				
				CommonToken coverageT = new CommonToken(202, "LIB_FUNC_CHAIN");
				CommonTree coverageParent = new CommonTree(coverageT);
				andNode.addChild(coverageParent);
				
				CommonToken coverageLibF = new CommonToken(201, "LIB_FUNCTION");
				CommonTree coverageLibFNode = new CommonTree(coverageLibF);
				coverageParent.addChild(coverageLibFNode);
				
				CommonToken coverageF = new CommonToken(277, "coveragePoint ()");
				CommonTree coverageFunctionNode = new CommonTree(coverageF);
				coverageLibFNode.addChild(coverageFunctionNode);
				
				// Adding the coverage call to the query
				
				// To save the children's children
				CommonTree parent = (CommonTree) tree.parent;
				List kids = tree.getChildren();
				node.addChildren(kids);
				andNode.addChild(node);
				
				parent.insertChild(childpos, andNode);
				parent.deleteChild(childpos + 1);
			} else {
				// To save the children's children
				CommonTree parent = (CommonTree) tree.parent;
				List kids = tree.getChildren();
				node.addChildren(kids);
				parent.insertChild(childpos, node);
				parent.deleteChild(childpos + 1);
			}
		}
	}

	private int CheckOperatorInVector(String operator) {
		
		int position = 0;
		
		for (int i = 0; i <= tokens.length - 1; i++){
			if (tokens[i].equals(operator)){
				position = i;
			}				
		}
		return position;
	}
	
	private boolean IsTheOperator(String op) {
		boolean exist = false;
		
		for (int i = 0; i < tokens.length; i ++){
			if (tokens[i].equals(op)){
				exist = true;
			}
		}
		return exist;
	}


	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		int positionX = queryorg.length();
		int positionXFinal = 0;
		int i = 0;
		int whatfunction = 0;
		int prevfunction = 0;
		int iteration = 0;
		int distance = 0;
		String query = queryorg.toLowerCase();

		while (i < counter) {
			for (String function : RLOPosition) {
				distance = query.indexOf(function);
				if (distance != -1 && positionX > distance) {
					positionX = distance;
					whatfunction = iteration;
				}
				iteration++;
			}
			query = query.substring(positionX + RLOPosition[whatfunction].length());
			if (positionXFinal == 0) {
				positionXFinal += positionX;
				prevfunction = whatfunction;
				positionX = query.length();
			} else {
				positionXFinal += positionX + RLOPosition[prevfunction].length();
				prevfunction = whatfunction;
				positionX = query.length();
			}
			i++;
			iteration = 0;
		}
		
		return positionXFinal;
	}

}