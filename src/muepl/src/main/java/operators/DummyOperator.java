package operators;

import org.antlr.runtime.tree.CommonTree;

/**
 * Dummy operator which reports no operands and doesn't change the source
 * EPL process definition at all. Useful as a placeholder while all
 * operators are being implemented.
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */
public class DummyOperator implements Operator {

    private final String name;

    public DummyOperator(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

	@Override
	public int getMaximumDistinctAttributeValue(String source)
			throws Exception {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public int getOperandCount(String source) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int check(CommonTree token, int operand) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void apply(CommonTree tree, int attribute, boolean coverage) throws Exception {
		// TODO Auto-generated method stub
		return ;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}