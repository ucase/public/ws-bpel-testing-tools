package operators;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * ESEL operator 
 * 
 * Syntaxes: SELECT [ISTREAM | IRSTREAM | RSTREAM] [DISTINCT] * | expression_list...
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.1
 */

public class RSC implements Operator {

    private final String name;
    private final String[] selTokens = {"SELECTION_EXPR", "SUBSELECT_GROUP_EXPR", "EXISTS_SUBSELECT_EXPR", "IN_SUBSELECT_QUERY_EXPR", "SUBSELECT_EXPR"};
    private final String[] tokens = {"irstream", "rstream", "distinct"};
    private final int[] typeTokens = {61, 59, 46};
    private final String[] eselTokens = {"rstream distinct", "irstream distinct", "rstream", "irstream", "distinct"}; 
    
    private final int NumSELOperators = 5; // (Keywords * 2 (select and select distinct)) - 1  
    
    public RSC(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	for (int i = 0; i < selTokens.length; i++){
    		while (query.indexOf(selTokens[i]) > -1){
    			query = query.substring(source.indexOf(selTokens[i]) + selTokens[i].length());
    			count++;
    		}
    		query = source;
    	}
    	
    	return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumSELOperators;
    }

	public int check(CommonTree token, int operand) {
		
		String t = token.toString();
		int thereIs = 0;
		
		for (int i = 0; i < selTokens.length; i++){
			if (t == selTokens[i]){
				thereIs = 1;
			}
		}
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo ESEL
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		String bf = "";
		List children = tree.getChildren();
		int size = children.size();
		
		for (int i = 0; i < size; i++){
			for (int j = 0; j < tokens.length; j++){
				if (children.get(i).toString().equals(tokens[j]))
					bf += children.get(i) + " ";
			}			
		}
		
		bf = bf.trim();
		int numwords = bf.split(" ").length;
		
		if (bf.split(" ")[0].equals("")){
			numwords = 0;
		}
		
		for (int i = numwords - 1; i >= 0; i--) {
			tree.deleteChild(i);
		}
		// Comparamos si los hijos contiene alguno de los tokens
		if (!bf.equals(eselTokens[attribute - 1])) {
			List kids = new ArrayList();
			String[] tokenMut = eselTokens[attribute - 1].split(" ");
			
			for (int i = 0; i < tokenMut.length; i++){
				int type = InfoToken(tokenMut[i]); 
				CommonToken t = new CommonToken(type, tokenMut[i]);
				CommonTree node = new CommonTree(t);
				tree.insertChild(i, node);
			}
		}

	}

	private int InfoToken(String st) {
		int pos = 0;
		
		for (int i = 0; i < tokens.length; i++){
			if (tokens[i].equals(st)){
				pos = typeTokens[i];
			}
		}
		
		return pos;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		return queryorg.toLowerCase().indexOf("select");
	}
}