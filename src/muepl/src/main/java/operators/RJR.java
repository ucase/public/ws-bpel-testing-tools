package operators;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * EJOI operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class RJR implements Operator {
	
	// JOIN = 37
    private final String name;
    private final String[] tokens = {"INNERJOIN_EXPR", "LEFT_OUTERJOIN_EXPR", "RIGHT_OUTERJOIN_EXPR", "FULL_OUTERJOIN_EXPR", "OUTERJOIN_EXPR"};
    private final int[] typeTokens = {176, 177, 178, 179, 175};
    
    private final int NumEJOINOperators = 4; // (Keywords * 2 (select and select distinct)) - 1  
    
    public RJR(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	
    	while (query.indexOf("JOIN_EXPR") > -1){
    		query = query.substring(query.indexOf("JOIN_EXPR") + "JOIN_EXPR".length());
    		count++;
    	}
    	
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumEJOINOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		
		for (int j = 0; j < tokens.length; j++){
			if (token.toString().equals(tokens[j])){
				thereIs = 1;				
			}
		}
		
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo EJOI
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
				
		int tokenindex = tree.childIndex;
		CommonTree parent = tree.parent;
		int position = CheckOperatorInVector(tree.toString());
		attribute -=1;

		if (position <= attribute){
			attribute += 1; //It has to advance 1 position
		}
		
		int type = InfoToken(tokens[attribute]);

		CommonToken t = new CommonToken(type, tokens[attribute]);
		CommonTree node = new CommonTree(t);

		// To save the children
		node.addChildren(tree.getChildren());
		parent.insertChild(tokenindex, node);
		parent.deleteChild(tokenindex + 1);	
		
	}

	private int CheckOperatorInVector(String operator) {
		
		int position = 0;
		
		for (int i = 0; i < tokens.length; i++){
			if (tokens[i].equals(operator)){
				position = i;
			}				
		}
		return position;
	}
	
	private int InfoToken(String st) {
		int pos = 0;
		
		for (int i = 0; i < tokens.length; i++){
			if (tokens[i].equals(st)){
				pos = typeTokens[i];
			}
		}
		
		return pos;
	}


	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}