package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * LDEC operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class WLM implements Operator {
	
    private final String name;
    private final String tokens = "VIEW_EXPR";
    private final int NumOperators = 2;   
    
    public WLM(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	String checkOp = "win length";
    	
    	while (query.indexOf(checkOp) > -1){
    		count++;
    		query = query.substring(query.indexOf(checkOp) + checkOp.length());
    	}
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		
		if (token.getChildren() != null){
			
			List children = token.getChildren();
			int size = children.size();

			if (HasTheBrother(children)){
				thereIs = 1;
			}
		}
		
		return thereIs;

	}
	
	private boolean HasTheBrother(List brothers) {
		String brother1 = "win";
		String brother2 = "length";
		String brother3 = "length_batch";
		boolean exist = false;
		
		for (int i = 0; i < brothers.size(); i++){
			if (brothers.get(i).toString().equals(brother1) && brothers.get(i + 1).toString().equals(brother2)){
				exist = true;
				break;
			}
			if (brothers.get(i).toString().equals(brother1) && brothers.get(i + 1).toString().equals(brother3)){
				exist = true;
				break;
			}
		}
		return exist;
	}
	
	/**
	 * Función para realizar la mutación de un nodo específico de tipo LDEC
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();
		int size = children.size();
		boolean change = false;
		int type = 355;
		
		for (int i = 0; i < size; i++){
			if (!change){
				if (isNumeric(children.get(i).toString())){
					int num = Integer.parseInt(children.get(i).toString());
					num = AttributeOperation(num, attribute);
					String modifnum = String.valueOf(num);
					CommonToken t = new CommonToken(type, modifnum);
					CommonTree node = new CommonTree(t);
					tree.insertChild(2, node);
					tree.deleteChild(3);
					change = true;
				}
			}
		}
	}
	
    private int AttributeOperation(int num, int attribute) {
		if (attribute == 1){
			num++;
		} else {
			num--;
		}
		return num;
	}

	private static boolean isNumeric(String cadena){
    	try {
    		Integer.parseInt(cadena);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}
    }

	private boolean IsTheOperator(String op) {
		
		boolean exist = false;
	
		if (tokens.equals(op)){
				exist = true;
			}

		return exist;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		return queryorg.indexOf("win:length");
	}

}