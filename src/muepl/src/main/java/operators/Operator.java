package operators;

import org.antlr.runtime.tree.CommonTree;

public interface Operator {
    /**
     * Returns the name of the operator.
     *
     * @return Name of the operator.
     */
    String getName();

    /**
     * Calculates the number of valid operands in the document.
     *
     * @param source
     *            Document where the mutation operator should be applied.
     * @return Number of valid operands to be used in the document.
     */
    int getOperandCount(String source) throws Exception;

    /**
     * Returns M in the range [1, M] of distinct (as in producing syntactically
     * different mutants) attribute integer values for some operand in the
     * document.
     */
    int getMaximumDistinctAttributeValue(String source) throws Exception;

    /**
     * Applies the operator to a particular operand in the source document.
     *
     * @param tree
     *            Node where the mutation operator should be applied.
     * @param attribute
     *            Position of the selected operand which is going to be replaced.
     *            The first operand has position 1 (not 0).
     * @param coverage 
     */
    void apply(CommonTree tree, int attribute, boolean coverage) throws Exception;

    /**
     * Check if exists the operand in the string.
     * @param ePLTree 
     * 				String where is going to be searched the operand.
     * @param operandIndex 
     * 				In some of the operators will be necessary to find the token to mutate.
     * @return
     * 			The most of the check functions will return 1 if there is one operand and 0 otherwise.
     * 			But there are some operators, that will return a number major of 1, because in a same 
     * 			token we could find more than 1 mutants.
     */
	int check(CommonTree mycommonTree, int operandIndex);

	/***
	 * Get the position Y in the query where the mutant is applied
	 * 
	 * @param eplDoc the AST of the query 
	 * @param queryorg the original query
	 * @param counter 
	 * @return the position Y of the first character where the mutant is applied
	 */
	int getColumnPosition(String eplDoc, String queryorg, int counter);
}