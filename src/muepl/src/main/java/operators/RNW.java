package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;


/**
 * RNW operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 */

public class RNW implements Operator {
	
    private final String name;
    private final String[] tokens = {"EVAL_IS_EXPR", "EVAL_ISNOT_EXPR"};
    private final int[] typeTokens = {166, 167};
    
    private final int NumENLFOperators = 1;  
    
    public RNW(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	for (int i = 0; i < tokens.length; i++){
       		while (query.indexOf(tokens[i]) > -1){
    			query = query.substring(query.indexOf(tokens[i]) + tokens[i].length());
    			count++;
    		}
       		query = source;
    	}
    	
        return count;
    }

    public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumENLFOperators;
    }

	public int check(CommonTree token, int operand) {
		
		int thereIs = 0;
		
		for (int j = 0; j < tokens.length; j++){
			if (token.toString().equals(tokens[j])){
				thereIs = 1;
			}
		}
		
		return thereIs;
	}

	/**
	 * Función para realizar la mutación de un nodo específico de tipo ENLF
	 * @param tree Nodo que se va a mutar
	 * @param attribute Número del token por el que se va a mutar
	 */
	
	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
		
		List children = tree.getChildren();
		int number = 0;
		
		if (tree.toString().equals(tokens[0])){
			number = 1;
		} else{
			number = 0;
		}
		
		CommonToken t = new CommonToken(typeTokens[number], tokens[number]);
		CommonTree node = new CommonTree(t);
		
		// To save the children's children
		node.addChildren(children);
		
		int indextoken = tree.childIndex;
		CommonTree parent = tree.parent;
		parent.insertChild(indextoken, node);
		indextoken = tree.childIndex;
		parent.deleteChild(indextoken);	
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}