package operators;

import java.util.List;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 * ROM operator 
 * 
 * @author Lorena Gutiérrez Madroñal
 * @version 1.0
 * 
 * SYNTAXES: order by expression [asc | desc] [, expression [asc | desc]] [, ...]
 * 
 */

public class ROM implements Operator {

    private final String name;
    private final String ROMtoken = "ORDER_BY_EXPR";
    private final String[] ROMOrdertokens = {"desc", "asc"};
    private final int [] type = {58, 57};
    private final int NumOperators = 2;
    
    public ROM(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public int getOperandCount(String source) throws Exception {
    	
    	int count = 0;
    	String query = source;
    	
    	while (query.contains(ROMtoken)){
    		query = query.substring(query.indexOf(ROMtoken) + ROMtoken.length());
    		count =+ CountChildren(query, source);
    	}
    	
    	return count;
    }

    private int CountChildren(String query, String source) {
		String childs = "ORDER_ELEMENT_EXPR";
		int elements = 0;
		String auxquery = query;
		
		while (auxquery.contains(childs)){
			elements++;
			auxquery = auxquery.substring(auxquery.indexOf(childs) + childs.length());
		}
		
		return elements;
	}

	public int getMaximumDistinctAttributeValue(String source) throws Exception {
    	return NumOperators;
    }

    public int check(CommonTree token, int operand) {
		    	
		int thereIs = 0;
		String comparetoken = "ORDER_ELEMENT_EXPR";
		
		if (token.toString() == comparetoken){
			thereIs = 1;
		}
		
		return thereIs;
	}

	public void apply(CommonTree tree, int attribute, boolean coverage)
			throws Exception {
				
		if (attribute == 1){
			int indextoken = tree.childIndex;
			CommonTree parent = tree.parent;
			parent.deleteChild(indextoken);
			
			if (parent.getChildCount() == 0){
				int indexparent = parent.childIndex;
				CommonTree granpa = parent.parent;
				granpa.deleteChild(indexparent);
			}
			
			if (parent.getChildCount() == 1){
				if (parent.getChild(0).toString() != "ORDER_ELEMENT_EXPR"){
					CommonTree granpa = parent.parent;
					granpa.addChild(parent.getChild(0));
					int indexparent = parent.childIndex;
					granpa.deleteChild(indexparent);
				}
			}
		}
		if (attribute == 2){
			if (!HasOrder(tree)){
				CommonToken ordtoken = new CommonToken(type[0], ROMOrdertokens[0]);
				CommonTree ordertoken = new CommonTree (ordtoken);
				tree.addChild(ordertoken);
			}
			else{
				if (OrderDesc(tree)){
					CommonToken ordtoken = new CommonToken(type[1], ROMOrdertokens[1]);
					CommonTree ordertoken = new CommonTree (ordtoken);
					tree.addChild(ordertoken);
					tree.deleteChild(1);
				}
				else {
					CommonToken ordtoken = new CommonToken(type[0], ROMOrdertokens[0]);
					CommonTree ordertoken = new CommonTree (ordtoken);
					tree.addChild(ordertoken);
					tree.deleteChild(1);
				}
			}
		}
		
	}
	
	private boolean OrderDesc(Tree tree) {
		boolean hasorder = false;
		int children = tree.getChildCount();
		
		for (int i = 0; i < children; i++){
			if (tree.getChild(i).toString().equals(ROMOrdertokens[0])){
				hasorder = true;
			}
		}
		
		return hasorder;
	}
	
	private boolean HasOrder(Tree tree) {
		boolean hasorder = false;
		int children = tree.getChildCount();
		
		for (int i = 0; i < children; i++){
			for (int j = 0; j < ROMOrdertokens.length; j++){
				if (tree.getChild(i).toString().equals(ROMOrdertokens[j])){
					hasorder = true;
				}
			}
		}
		
		return hasorder;
	}

	@Override
	public int getColumnPosition(String eplDoc, String queryorg, int counter) {
		// TODO Auto-generated method stub
		return 0;
	}
}