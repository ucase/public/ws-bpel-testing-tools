package epl.util;

import java.util.List;

public class MutantCompare{
	
	public String mutantName;
	public List<Integer> comparisions;
	public Long executionTime;
	
	public MutantCompare() {};
	
	public MutantCompare(String name, List<Integer> listcomp, Long time) {
		
		this.mutantName = name;
		this.comparisions = listcomp;
		this.executionTime = time;
	
	}
	
	public String getMutantName() {return mutantName;}
	public List<Integer> getComparisions() {return comparisions;}
	public Long getExecutionTime() {return executionTime;}
	
	public void setMutantName (String name) {mutantName = name;}
	public void setComparisions (List<Integer> listcomp) {comparisions = listcomp;}
	public void setExecutionTime (Long time) {executionTime = time;}
	
	public String MutantToString() {
		String mutantline = null;
		String values = "";
		
		for (int v = 0; v < comparisions.size(); v++) {
			values += comparisions.get(v).toString() + " ";
		}
		
		mutantline = mutantName + " " + values + "T " + executionTime;
		
		return mutantline;
	}

}
