package epl.util;

public class Pair<A, B> {

	public A first;
	public B second;
	
	public Pair() {};
	
	public Pair (A first, B second){
		this.first = first;
		this.second = second;
	}
	
	public A getFirst() { return first;}
	public B getSecond() { return second;}
	
	public void setFirst(A v) {first = v;}
	public void setSecond(B v) {second = v;}

	@Override
	public String toString() {
		return "Pair [" + first + ", " + second + "]";
	}
	
	
}
