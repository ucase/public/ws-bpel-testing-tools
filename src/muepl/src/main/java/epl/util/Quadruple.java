package epl.util;

public class Quadruple<A, B, C, D> {

	public A first;
	public B second;
	public C third;
	public D fourth;
	
	public Quadruple() {};
	
	public Quadruple (A first, B second, C third, D fourth){
		this.first = first;
		this.second = second;
		this.third = third;
		this.fourth = fourth;
	}
	
	public A getFirst() { return first;}
	public B getSecond() { return second;}
	public C getThird() {return third;}
	public D getFourth() {return fourth;}
	
	public void setFirst(A v) {first = v;}
	public void setSecond(B v) {second = v;}
	public void setThird(C v) {third = v;}
	public void setFourth(D v) {fourth = v;}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((fourth == null) ? 0 : fourth.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		result = prime * result + ((third == null) ? 0 : third.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quadruple other = (Quadruple) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (fourth == null) {
			if (other.fourth != null)
				return false;
		} else if (!fourth.equals(other.fourth))
			return false;
		if (second == null) {
			if (other.second != null)
				return false;
		} else if (!second.equals(other.second))
			return false;
		if (third == null) {
			if (other.third != null)
				return false;
		} else if (!third.equals(other.third))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Quadruple [first=" + first + ", second=" + second + ", third=" + third + ", fourth=" + fourth + "]";
	}
	
	
	
}
