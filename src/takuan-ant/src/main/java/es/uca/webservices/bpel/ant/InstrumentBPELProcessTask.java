package es.uca.webservices.bpel.ant;

import java.io.File;
import java.io.FileWriter;
import java.util.logging.Logger;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.activebpel.DeploymentArchivePackager;
import es.uca.webservices.bpel.bpelunit.CustomizedRunner;
import es.uca.webservices.bpel.daikon.decls.DeclarationsFormatter;
import es.uca.webservices.bpel.daikon.decls.DeclarationsGenerator;
import es.uca.webservices.bpel.daikon.instrument.BPELFileInstrumenter;

/**
 * <p>
 * This Ant task generates the necessary files to deploy and run the intrumented
 * BPEL process in the ActiveBPEL engine. It creates the instrumented BPEL in
 * INSPECTED_BPEL_FILENAME, the PDD in PDD_FILENAME, and the catalog in
 * CATALOG_FILENAME.
 * </p>
 * 
 * <p>
 * It assumes that subsequently the WSDL, XML Schema, BPEL file, the PDD and
 * catalog.xml will be packed in a JAR file with .bpr extension, where the XML
 * Schema and WSDL will be under the wsdl subdirectory, the BPEL process in the
 * BPEL one, proceso.pdd in the root, and catalog.xml in META-INF together with
 * a MANIFEST that has to be generated somehow. For that purpose the "jar" task
 * can be used.
 * </p>
 * 
 * <p>
 * In its last revision, now every product can be generated independently as
 * they are needed, thinking about using it directly from Java code. The Ant
 * task is still working the same way.
 * </p>
 * 
 * <p>
 * For using the intrumentation logic without getting dependencies with Ant, the
 * class BPELProcessInstrumenter can be used separately, so its methods addFile
 * and addFileList.
 * </p>
 * 
 * @author Antonio García Domínguez
 * @version 1.0
 * @see es.uca.webservices.bpel.activebpel.DeploymentArchivePackager
 * 
 */
public class InstrumentBPELProcessTask extends Task {

	static final String INSPECTED_BPEL_FILENAME = "procesoInspeccionado.bpel";
	static final String PDD_FILENAME = "proceso.pdd";
	static final String DAIKON_DECLS_FILENAME = "procesoInspeccionado.decls";
	static final String CATALOGXML_FILENAME = "catalog.xml";

	private static final Logger LOGGER = Logger
			.getLogger(InstrumentBPELProcessTask.class.getName());

	private String pathToBPEL = null;
	private String pathToBPR = null;

	/**
	 * Sets the BPEL to be instrumented
	 * 
	 * @param path
	 *            The path to the BPEL file
	 */
	public void setBPEL(String path) {
		this.pathToBPEL = path;
	}

	/**
	 * Sets the BPR where the files must be packed
	 * 
	 * @param path
	 *            The path to the BPR file
	 */
	public void setBPR(String path) {
		this.pathToBPR = path;
	}

	/**
	 * This method is called to perform the task
	 * 
	 * @throws BuildException
	 *             This exception is thrown if there was any trouble during the
	 *             instrumentation process
	 */
	@Override
	public void execute() {
		if (pathToBPEL == null) {
			throw new BuildException("bpel task attribute is mandatory");
		}

        final Thread currentThread = Thread.currentThread();
        final ClassLoader oldClassLoader = currentThread.getContextClassLoader();

        // This line is required to make XMLBeans and other things that depend on the context classloader
        // load properly from inside Ant.
        currentThread.setContextClassLoader(InstrumentBPELProcessTask.class.getClassLoader());

        try {
			final BPELProcessDefinition originalBPEL = new BPELProcessDefinition(new File(pathToBPEL));

			LOGGER.fine("Generating the instrumented WS-BPEL process definition...");
			final BPELProcessDefinition inspectedBPEL = new BPELFileInstrumenter(originalBPEL)
				.instrument(new File(INSPECTED_BPEL_FILENAME));

			// Don't print any status message here: generateBPR logs everything it does
			DeploymentArchivePackager instrumenter = new DeploymentArchivePackager(inspectedBPEL);
			if (pathToBPR != null) {
				instrumenter.generateBPR(pathToBPR);
			}

			LOGGER.fine("Generating preliminary Daikon declarations file...");
			final FileWriter fW = new FileWriter(new File(DAIKON_DECLS_FILENAME));
			try {
				new DeclarationsGenerator().generateAndPrint(
					inspectedBPEL, new DeclarationsFormatter(), fW);
			} finally {
				fW.close();
			}
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			throw new BuildException(e);
		} finally {
            currentThread.setContextClassLoader(oldClassLoader);
        }
	}
}
