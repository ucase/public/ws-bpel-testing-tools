package es.uca.webservices.bpel.ant;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.junit.Test;

public class InstrumentBPELProcessTest {

	private static final String TEST_RESOURCEDIR = "target/test-classes/";

	@Test(expected=BuildException.class)
	public void malformedBPEL() {
		InstrumentBPELProcessTask task = new InstrumentBPELProcessTask();
		task.setBPEL(TEST_RESOURCEDIR + "dummy.bpel");
		task.setBPR(TEST_RESOURCEDIR + "dummy.bpr");
		task.execute();
	}

	@Test
	public void normal() {
		final String bprPath = TEST_RESOURCEDIR + "loan.bpr";

		InstrumentBPELProcessTask task = new InstrumentBPELProcessTask();
		task.setBPEL(TEST_RESOURCEDIR + "ProcesoAprobadorPrestamo.bpel");
		task.setBPR(bprPath);
		task.execute();

		assertTrue(new File(bprPath).canRead());
	}
}
