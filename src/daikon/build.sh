#!/bin/bash
#
# Daikon build script
# Copyright (C) 2009-2011 Antonio García Domínguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Exit on error
set -e
# Use error code from last command in a pipe chain
set -o pipefail
# export all variable changes
set -o allexport

### USER CONFIGURATION ########################################################

# Path under which all downloads should be cached to save bandwidth
DL_CACHE="${HOME}/Downloads"

# URL for downloading the Daikon sources
DAIKON_HG_URL="https://code.google.com/p/daikon/"

# Path for our patch for Daikon
DAIKON_DELTA=$(readlink -f Daikon_delta_hg.diff)

# Main directory in the Daikon source distribution
DAIKON_SRC_MAINDIR="daikon"

# Classpath which will be used in the Daikon script
DAIKON_CLASSPATH="${DAIKON_SRC_MAINDIR}/java"

### AUXILIARY FUNCTIONS #######################################################

function echo_err() {
  echo "$@" >&2
}

function log() {
  if [[ "$#" < 2 ]]; then
    error "Wrong parameters: expected level and rest of message, got $@"
    return 1
  fi
  LEVEL="$1"; shift
  echo_err "`date -R`: ($LEVEL) $@"
}

function status() {
  log INFO "$@"
}

function error() {
  log ERROR "$@"
}

function push_d() {
  # Create if it doesn't exist
  if test -f "$1"; then
    error "$1 is a file"
    return 1
  fi
  mkdir -p "$1"
  pushd "$1" > /dev/null
}

function pop_d() {
  popd > /dev/null
}

### MAIN SCRIPT BODY ##########################################################

. ~/.bashrc

if test -d "$DAIKON_SRC_MAINDIR"; then
  push_d "$DAIKON_SRC_MAINDIR"
  hg revert --all
  hg pull
  pop_d
else
  rm -rf "$DAIKON_SRC_MAINDIR"
  hg clone "$DAIKON_HG_URL"
fi
push_d "$DAIKON_SRC_MAINDIR"
patch -p1 < "$DAIKON_DELTA"
pop_d

if [ -z "$JDK_HOME" ]; then
    echo "Please set JDK_HOME before running this script."
    exit 1
fi

# Add related environment variables
DAIKONDIR=$(readlink -f $DAIKON_SRC_MAINDIR)
DAIKONBIN=$DAIKONDIR/bin
DAIKONCLASS_SOURCES=1
CPADD=$DAIKONDIR/java
CLASSPATH=$CPADD:$JDK_HOME/lib/tools.jar
PATH=$DAIKONBIN:$PATH
PERLLIB=$DAIKONBIN

# Enter the Java build directory for Daikon
push_d "$DAIKON_CLASSPATH"

# Compile and generate binary distribution
LC_ALL="C" make ../daikon.jar javadoc JAVAC_XLINT="" JAVAC_ARGS="-g -target 6 -source 6" TMPDIR="/tmp" INSTALL="/bin/install"
cp ../daikon.jar ../../daikon.jar

# Generate source distribution
echo "Packaging daikon-sources.jar..."
find '(' -name "*.java" -o -name LICENSE.txt ')' -print0 | xargs -0 jar cf ../../daikon-sources.jar

# Generate javadoc distribution
find -name '*.java' -print | cut --delim='/' -f2- | xargs javadoc -d javadoc || true
push_d javadoc
echo "Packaging daikon-javadoc.jar..."
find -type f -print0 | xargs -0 jar cf ../../../daikon-javadoc.jar
pop_d

# Done!
pop_d
