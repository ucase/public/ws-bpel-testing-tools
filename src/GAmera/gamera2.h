#include <stdio.h>
#include <math.h>


#include <iostream>
#include <string> 
#include <fstream> 
#include <sstream> 
#include <algorithm>		// reverse()
#include <iterator>
#include <cstdlib>		// abs()
#include <vector>
#include <numeric>
using namespace std;


/* Bibliotecas de GALIB */
#include <ga/ga.h>
#include <ga/std_stream.h>
#include <ga/GADemeGA.h>

typedef unsigned long long CampoIndividuo;

// force instantiations for compilers that do not do auto instantiation
// for some compilers (e.g. metrowerks) this must come after any
// specializations or you will get 'multiply-defined errors when you compile.
#if !defined(GALIB_USE_AUTO_INST)
#include <ga/GA1DArrayGenome.C>
GALIB_INSTANTIATION_PREFIX GA1DArrayGenome<CampoIndividuo>;
#endif



/* Parámetros del algoritmo genético */
#define NUMOPERADORES 26

// Campos de la estructura del individuo
#define OPERADOR 0
#define INSTRUCCION 1
#define ATRIBUTO 2
#define ESTADO 3
#define TEST 4

#define VIVO 0
#define MUERTO 1
#define ERROR -2
#define NOEVALUADO -3
#define ORIG_INICIAL 1
#define ORIG_CRUCE 2
#define ORIG_MUTACION 3
#define ORIG_NUEVO 4

// REVISTA: LOS INDICES DE LAS DOS POBLACIONES

#define POBLACION 0
#define GENERADOS 1



// Determinar si un mutante potencialmente equivalente
#define equivalente(g) (g.score() == TAMPOBLACION*NUMTEST?1:0)

//Variables globales de configuración del sistema
int TAMPOBLACION, NUMTEST,NUMGENERACIONES,MUTANTESMAX;
float PMUTACION,PCRUCE,PSUSTITUIR,PNUEVO,PMUTANTES;
string BPELORIGINAL,BPELOUT,TESTSUITE,ANALIZADOR,MUTANTESCALIDAD;
string SALIDAGAMERA,ESTADISTICAS,INDIVIDUOS,BDMUTANTES,DIRECTORIOSALIDA;
int GENMUTANTES,DIFERENTE,EJEANALIZADOR,EJEBPEL;

// Definiciones de tipos para el cálculo del mcm
typedef CampoIndividuo natural;
typedef vector<natural> naturales;

/* Analizador */
void  Analizador(void);
void  Configurar(char *);
void  LeerMutantesCalidad();

/* Funciones necesarias para la definición de los individuos */
float Aptitud(GAGenome&);
int   Mutacion(GAGenome&, float); 

int Cruce(const GAGenome& , const GAGenome&, GAGenome *, GAGenome* );
void  InicializarIndividuo(GAGenome&); 
int DeterminaOperador(const GA1DArrayGenome<CampoIndividuo>&);
void  ConvierteIndividuo(const GA1DArrayGenome<CampoIndividuo>&, vector<CampoIndividuo>&);
void MostrarPoblacion (ofstream&, int, const GAPopulation&);
int ComparaMutante (vector<CampoIndividuo>&,vector<CampoIndividuo>&);
void EstablecerEstadoIndividuo(GAGenome&,  int);
void EstablecerGenIndividuo(GAGenome&,  int, int);
void EstablecerTestIndividuo(GAGenome&,  int, int);

CampoIndividuo VerGen(GAGenome& , int);
/* Vector de mutantes */
vector <float> MISMUTANTES;
// REVISTA: Los de calidad para el criterio de parada
vector <float> LOSMUTANTESDECALIDAD;
int CONTMUTANTES = 0;

/* Funciones necesarias para la definición de la población */
void  EvaluarPoblacion(GAPopulation &,GAPopulation &);
void IniciarPoblacion (GAPopulation &);

/* Funciones auxiliares */
natural mcd (natural , natural);
inline natural mcm(natural, natural);
inline natural mcmvector(naturales);

ofstream bdmutantes;

// Estructuras para el analizador: Operadores y ResumenAnalizadores

/* 
 Matriz Operadores con tres campos (la rellena el analizador)
 - El primer campo almacena el valor que le asignará el algoritmo genético. Un 0 indica que no se utiliza.
 - El segundo campo el número de instrucciones que tiene
 - El tercer campo el número de atributos que tiene
*/
struct OperadorInfo {
  CampoIndividuo operador, localidad, atributo;
};

vector <OperadorInfo> Operadores;

/* 
 Vector con el máximo de operadores y los mcm de Instrucciones y de Atributos 
*/
vector<CampoIndividuo> ResumenAnalizador(3); 

// Definimos la clase de nuestro algoritmo genético. 
// Hereda los mismos procedimientos que la clase de Estado-Permanente de la biblioteca.
ofstream f;
// REVISTA llevaremos dos poblaciones
class GAMERA : public GADemeGA {//SteadyStateGA {
 public:
   GADefineIdentity("GAMERA", 200);
   GAMERA(const GAPopulation& p) : GADemeGA(p) {};
   virtual ~GAMERA() {};
   virtual void step();
   GAMERA & operator++() { step(); return *this; };

   void initialize(unsigned int seed) {
     GARandomSeed(seed);

     deme[0]->initialize();
     deme[0]->evaluate(gaTrue);
     pstats[0].reset(*deme[0]);
     //pop->individual(0).copy(deme[0]->best());
     //pop->touch();
     //stats.reset(*pop);

     if(!scross) GAErr(GA_LOC, className(), "initialize", gaErrNoSexualMating);
   }

};


// En cada paso hacemos un cruce o una mutación

void GAMERA::step()//const GAPopulation& pop, const GAPopulation& generados)
 { 
   int i,j;
   GAGenome *mama, *papa;
   float n = ((PNUEVO*(float)tmppop->size() < 1) ? 1 : PNUEVO*(float)tmppop->size());
   vector <CampoIndividuo>m1(3),m2(3);
   // Generamos un porcentaje aleatoriamente de individuos nuevos
   for (i=0; i<n; i++) {
       mama = &(deme[POBLACION]->individual(0));
       tmppop->individual(i).copy(*mama);
       InicializarIndividuo(tmppop->individual(i));
       EstablecerEstadoIndividuo(tmppop->individual(i),ORIG_NUEVO); // REVISTA: CONOCER ORIGEN

	   cout <<"NUEVO: " << i << "\t"<< VerGen(tmppop->individual(i), OPERADOR) << "\t" <<  VerGen(tmppop->individual(i), INSTRUCCION)  << "\t"
		<<  VerGen(tmppop->individual(i), ATRIBUTO)  << endl;
   }
   // Completamos con cruces y mutaciones
   for(i=n; i<tmppop->size()-1; i++){	// takes care of odd population
     mama = &(deme[POBLACION]->select());
     papa = &(deme[POBLACION]->select());
     pstats[POBLACION].numsel += 2;		// keep track of number of selections
     if(GAFlipCoin(pCrossover())) {// realizar un cruce {
	 tmppop->individual(i).copy(*papa);
	 tmppop->individual(i+1).copy(*mama);
	 if (GAFlipCoin(0.5) == 0) {
	     CampoIndividuo aux1=VerGen(tmppop->individual(i),INSTRUCCION);
	     CampoIndividuo aux2=VerGen(tmppop->individual(i),ATRIBUTO);
	     
	     EstablecerGenIndividuo(tmppop->individual(i),INSTRUCCION,VerGen(tmppop->individual(i+1),INSTRUCCION));
	     EstablecerGenIndividuo(tmppop->individual(i),ATRIBUTO,VerGen(tmppop->individual(i+1),ATRIBUTO));
	     EstablecerGenIndividuo(tmppop->individual(i+1),INSTRUCCION, aux1);
	     EstablecerGenIndividuo(tmppop->individual(i+1),ATRIBUTO,aux2);
	 } else {
	     CampoIndividuo aux2=VerGen(tmppop->individual(i),ATRIBUTO);
	     EstablecerGenIndividuo(tmppop->individual(i),ATRIBUTO,VerGen(tmppop->individual(i+1),ATRIBUTO));
	     EstablecerGenIndividuo(tmppop->individual(i+1),ATRIBUTO,aux2);
	 }
  cout <<"CRUCE1: " << i << "\t"<< VerGen(tmppop->individual(i), OPERADOR) << "\t" <<  VerGen(tmppop->individual(i), INSTRUCCION)  << "\t"
		<<  VerGen(tmppop->individual(i), ATRIBUTO)  << endl;
  cout <<"CRUCE2: " << i << "\t"<< VerGen(tmppop->individual(i+1), OPERADOR) << "\t" <<  VerGen(tmppop->individual(i+1), INSTRUCCION)  << "\t"
		<<  VerGen(tmppop->individual(i+1), ATRIBUTO)  << endl;
	 stats.numcro += 1;
//	 stats.numcro += (*scross)(*papa, *mama, &tmppop->individual(i), &tmppop->individual(i+1));
	 EstablecerEstadoIndividuo(tmppop->individual(i),ORIG_CRUCE); // REVISTA: CONOCER ORIGEN
	 EstablecerEstadoIndividuo(tmppop->individual(i+1),ORIG_CRUCE); // REVISTA: CONOCER ORIGEN
	 i++;
     } else { // o una mutación 
	 tmppop->individual(i).copy(*papa);
	 pstats[POBLACION].nummut += tmppop->individual(i).mutate(1);//pMutation());
  cout <<"MUTAR: " << i << "\t"<< VerGen(tmppop->individual(i), OPERADOR) << "\t" <<  VerGen(tmppop->individual(i), INSTRUCCION)  << "\t"
		<<  VerGen(tmppop->individual(i), ATRIBUTO)  << endl;
     }
   }
   if(i < tmppop->size()) { // Falta un individuo por meter
       if(GARandomBit())
	   tmppop->individual(i).copy(*mama);
       else
	   tmppop->individual(i).copy(*papa);
       
       pstats[POBLACION].nummut += tmppop->individual(tmppop->size()-1).mutate(1);
   }
 
// Comprobamos si los generados son nuevos o repetidos
   
   if (DIFERENTE == 1) { // Exigimos individuos diferentes
       for (i=0; i<tmppop->size();i++) {
	   ConvierteIndividuo((GA1DArrayGenome<CampoIndividuo>&)tmppop->individual(i),m1);
	   for (j=0; j<MISMUTANTES.size();j++) {
	       if (MISMUTANTES[j] == m1[0]*10000+m1[1]*100+m1[2]*1) {
		   InicializarIndividuo(tmppop->individual(i));
		   ConvierteIndividuo((GA1DArrayGenome<CampoIndividuo>&)tmppop->individual(i),m1);
		   j=-1; // Volvemos a iniciar el bucle
	       }
	   }
       }

   }

   // Añadimos los nuevos individuos creados
   deme[POBLACION] = tmppop;
   EvaluarPoblacion(*deme[POBLACION],*deme[GENERADOS]); // REVISTA: siguiente evaluación

   deme[POBLACION]->scale();                  // se escala para la ruleta que se emplea en la selección de cruces

   pstats[POBLACION].numrep += tmppop->size();//nrepl[ii];
   pstats[POBLACION].update(*deme[POBLACION]);		// update the statistics by one generation
}
