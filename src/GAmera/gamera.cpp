/* ----------------------------------------------------------------------------
  GAMERA: generadormutantes.C
 
 ---------------------------------------------------------------------------- */
#include <cstdlib>
#include "gamera2.h"

time_t tiempoinicial;


/*----------------------------
  main

  Argumentos:
      - seed numero --> semillas aleatorias
      - file fichero --> Fichero de configuración con el resto de opciones
      - Resto del algoritmo genético (ver ayuda GALIB)
  -------------------------------*/
unsigned generacion; // variable global contador de generaciones

int
main(int argc, char** argv)
{
    ofstream outfile;
    unsigned int seed = time(NULL); // Semilla aleatoria
    string llamada;
    char cadena[50];// = "gamera.conf"; // Fichero de configuración por omisión

    strcpy(cadena, "gamera.conf");
// Buscar semilla aleatoria especificada en la línea de argumentos
// y el fichero de configuración

  for(int ii=1; ii<argc; ii++) {
    if(strcmp(argv[ii],"seed") == 0) {
      seed = atoi(argv[ii+1]);
    }
    if(strcmp(argv[ii],"file") == 0) {
	strcpy(cadena,argv[ii+1]);
    }
  }
  //  srand(seed);
  // Configurar GAMERA
  cout << "Leyendo configuración ...."<<endl; cout.flush();
  NUMGENERACIONES=1000;
  Configurar(cadena);
  LeerMutantesCalidad();

  // Crear estructura
  llamada= "mkdir -p " + DIRECTORIOSALIDA + "/mutantes";
  system (llamada.c_str());

  // Abrir bd de mutantes
  bdmutantes.open("bdmutantes.dat", (STD_IOS_OUT | STD_IOS_TRUNC));


// TIEMPO INICIAL;
  tiempoinicial = time (NULL);


  // Llamar al analizador
  cout << "Analizando fichero original "<<BPELORIGINAL<<endl; cout.flush();

  Analizador();
  

  cout <<"ResumenAnalizador:"<<  ResumenAnalizador[0]<<"-"<<  ResumenAnalizador[1]<<"-"<< ResumenAnalizador[2]<<"-"<<  endl;cout.flush();

  // Activar el sistema de ejecución de mutantes

  llamada="ActiveBPEL.sh start";
  system (llamada.c_str());

//Obtiene la salida de la composición original

  if (EJEBPEL == 0) {
 llamada="mubpel run " + TESTSUITE + " " + BPELORIGINAL + " > " +  BPELOUT;
 cout << "Ejecutando '" << llamada << "'" << endl;
 system (llamada.c_str());}

/* ------
    Definimos las caracteríticas del AG
    ------------*/
	  
// Individuos: vector de 3 elementos enteros: operador,instruccion y atributo 
// REVISTA: Metemos el individuo con más campos, uno para su estado, y el resto para el resultado de su ejecución
  GA1DArrayGenome<CampoIndividuo> genome(3 + NUMTEST + 1, Aptitud);
  genome.initializer(InicializarIndividuo);
  genome.mutator(Mutacion); // Establecemos el método de mutación
//  genome.crossover(Cruce); // Establecemos el método de mutación

  // Inicializamos la población y especificamos su evaluación
  GAPopulation poblacion(genome,TAMPOBLACION);
  poblacion.initializer(IniciarPoblacion);

// Inicializamos el algoritmo genético
  GAMERA ga(poblacion);
  GARandomSeed(seed);
  ga.populationSize(POBLACION,TAMPOBLACION);
  ga.populationSize(GENERADOS,MUTANTESMAX);

  ga.nGenerations(NUMGENERACIONES);
  ga.pMutation(PMUTACION);
  ga.pCrossover(PCRUCE);
  ga.nReplacement(POBLACION,TAMPOBLACION*PSUSTITUIR);
  ga.scoreFilename("gamera_estadisticas.dat");	// name of file for scores
  ga.scoreFrequency(1);	// keep the scores of every 10th generation
  ga.flushFrequency(100);	// specify how often to write the score to disk
  ga.selectScores(GAStatistics::AllScores);
  //ga.parameters(argc, argv, gaTrue); // parse commands, complain if bogus args

  cout << "Creando la primera población...\n"; cout.flush();
  ga.initialize(seed);

// Abrir fichero de salida
  outfile.open("mutantes.dat", (STD_IOS_OUT | STD_IOS_TRUNC));

// Mostrar parámetros del algoritmo
  outfile << ga.parameters();

// Mostrar población
  MostrarPoblacion(outfile, ga.generation(), ga.population(POBLACION));

  // Inicio de las generaciones
  cout << "Empezando las generaciones"; cout.flush();
  generacion = 1;
  while(MISMUTANTES.size() < GENMUTANTES){
      ga.step();

      // Mostrar población
      MostrarPoblacion(outfile, generacion, ga.population(POBLACION));

      cout << "."   << generacion++
           << "..." << LOSMUTANTESDECALIDAD.size()
           << "-"   << GENMUTANTES
           << "-"   << MISMUTANTES.size()
           << endl;
  }
  ga.flushScores();

  outfile.close();
  bdmutantes.close();

  // Guardar ficheros de resultados
  llamada = "mv mutantes.dat " + DIRECTORIOSALIDA + "/" + SALIDAGAMERA;
  system (llamada.c_str());
  llamada = "mv bdmutantes.dat "  + DIRECTORIOSALIDA + "/" + BDMUTANTES;
  system (llamada.c_str());
  llamada = "mv gamera_estadisticas.dat "  + DIRECTORIOSALIDA + "/" + ESTADISTICAS;
  system (llamada.c_str());

  return 0;
}
 

/* -------------------------
   Función EvaluarPoblacion
 - Redefinimos la que está establecida para una vez evaluado los individuos
   calcular la matriz de ejecución
---------------------------*/
// Redefinimos la evaluación de la población
void EvaluarPoblacion(GAPopulation &pob, GAPopulation &generados){

 

/* Estructuras para calcular la aptitud del individuo */
  int MatrizEjecucion[pob.size()][NUMTEST];
  int i,j,k,igual,uno,test;
  vector <CampoIndividuo>repetidos(pob.size());
  vector <CampoIndividuo>m1(3), m2(3);

 
    for (i=0; i<pob.size();i++){
	igual = 0;
	ConvierteIndividuo((GA1DArrayGenome<CampoIndividuo>&)pob.individual(i),m1);

        // Comprobamos que no está ya en la población actual
	for (j=0; (igual == 0) & (j<i); j++) {
	    ConvierteIndividuo((GA1DArrayGenome<CampoIndividuo>&)pob.individual(j),m2);
	    igual = ComparaMutante(m1,m2);
	}
	
	if (igual == 0) { // Comprobamos que no ha sido generado anteriormente
	    for (j=0, igual = 0; (igual == 0) & (j<MISMUTANTES.size());j++) {
		if (MISMUTANTES[j] == m1[0]*10000+m1[1]*100+m1[2]*1) 
		    igual = 1;
	    }
	}


	if (igual == 0) { // Nuevo mutante
	    pob.individual(i).evaluate();
	    repetidos[i]=0;
	} else { // Es repetido
	    repetidos[i]=1;
	}
	ifstream infile("salida.out");
	for (j=0, uno=0, test=-1 ; j<NUMTEST; j++){
	    if ((igual == 1)) {// REVISTA: SI ES REPETIDO || (uno == 1)|| (uno == 2)) {	
		MatrizEjecucion[i][j]=0; // Si es repetido o ya ha sido muerto
	    } else {
		infile>> MatrizEjecucion[i][j];
		if (MatrizEjecucion[i][j] ==1){ // Es muerto
		    EstablecerTestIndividuo(pob.individual(i),j+TEST,MatrizEjecucion[i][j]); //REVISTA:
		    uno=1;
                    test=j; 
		}
		if (MatrizEjecucion[i][j] ==2){ // Produce error en la ejecución
		    MatrizEjecucion[i][j]=0;
		    EstablecerTestIndividuo(pob.individual(i),j+TEST,0); //REVISTA:
		    repetidos[i]=1; // Para que su aptitud sea 0
		    uno=2;
		}

	    }
	}
	infile.close();
	int calidad =0;
	if (igual == 0) { // Meterlo en la base de datos de mutantes
	    // REVISTA: meterlo en la población de generados
	    generados.individual(MISMUTANTES.size()).copy(pob.individual(i));
	    MISMUTANTES.push_back( m1[0]*10000+m1[1]*100+m1[2]*1);
	    for (k=0; k<LOSMUTANTESDECALIDAD.size(); k++) {
		if (LOSMUTANTESDECALIDAD[k] == m1[0]*10000+m1[1]*100+m1[2]*1) {
		    GENMUTANTES++;
		    calidad=1;
		}
	    }
	    time_t tiempo = time (NULL) - tiempoinicial;

	    bdmutantes << generacion << ":"<<calidad<<":"<<tiempo<<":"<<m1[0] << "\t"<<m1[1] << "\t"<<m1[2] << "\tESTADO: "<<uno<< "\tTEST: ";
	    for (k=0;k<NUMTEST;k++) {
		bdmutantes << VerGen(pob.individual(i),TEST+k);
	    }
           bdmutantes << endl;// ACTUALIZAR EL FICHERO DE LA BD
	}

    }

    // Calculamos el fitness para cada individuo
    int suma;
    for (i=0; i<pob.size(); i++){
	if (repetidos[i] == 0) { // Es original // REVISTA:CORREGIR
	    suma=0;
	    for(j=0; j<NUMTEST; j++) {
		if(MatrizEjecucion[i][j]==1){
		    for (k=0; k<pob.size();k++){ // REVISTA: RECORRER POBLACION ACTUAL
			suma=suma+MatrizEjecucion[k][j];
		    }
		    for (k=0; k<MISMUTANTES.size(); k++){ // REVISTA: RECORRER POBLACION GENERADA
			suma=suma+VerGen(generados.individual(k),j+TEST);
		    }
		}
	    }
	    pob.individual(i).score(
		(MISMUTANTES.size() + pob.size()) * NUMTEST - suma);
	} else { // Es repetido o erróneo -> fitness = 0
	    pob.individual(i).score(0);
	}
    }

}

/***************************
  Nuevas funciones definidas
***********************/
/* itos() - convierte el entero n en un string */

string itos(CampoIndividuo n)
{
  string s;
  bool signo = n < 0;		// recuerda el signo
  n = __gnu_cxx::abs(n);
  do {				// genera dígitos de derecha a izquierda
    s += n % 10 + '0';		// obtiene el siguiente dígito, lo guarda
  } while (n /= 10);		// y lo quita
  if (signo) s += '-';		// añade el signo si es negativo
  reverse(s.begin(), s.end());	// invierte la cadena
  return s;			// y la devuelve por valor
}

/********************
  APTITUD
  Calcula la aptitud del individuo
***********************/
float Aptitud(GAGenome& g)
{
    GA1DArrayGenome<CampoIndividuo>& genome = (GA1DArrayGenome<CampoIndividuo>&)g;
    vector<CampoIndividuo> mutante(3);
    string llamada;

    // Convertimos el individuo al mutante
    ConvierteIndividuo(genome, mutante);

    // Preparamos la llamada al sistema de ejecución de mutantes
    llamada = "mubpel apply " + BPELORIGINAL  + " " + itos(mutante[OPERADOR]) + " " + itos(mutante[INSTRUCCION]) + " " + itos(mutante[ATRIBUTO]) + " > mutante.bpel";
    cout << "Ejecutando '" << llamada << "'" << endl;
    system(llamada.c_str());

    // Copiamos el mutante al directorio de salida
    llamada = "cp mutante.bpel " + DIRECTORIOSALIDA + "/mutantes/m-" + itos(mutante[OPERADOR]) + "-" + itos(mutante[INSTRUCCION]) + "-" + itos(mutante[ATRIBUTO]) + ".bpel";
    system(llamada.c_str());

    // Llamamos al sistema de ejecución
    // Obtenemos la fila de la matriz de ejecución comparando su salida
    llamada="mubpel comparefull " + TESTSUITE + " " + BPELORIGINAL + " " +  BPELOUT + " mutante.bpel > salida.out";
    cout <<"Ejecutando '" << llamada << "'" << endl;
    system(llamada.c_str());

    return 1;
}

static CampoIndividuo GenerarCampoAleatorio(CampoIndividuo min, CampoIndividuo max) {
  return min + (CampoIndividuo(rand()) * CampoIndividuo(rand())) % (max - min + 1);
}

/* -------------------------
   Función InicializarIndividuo
 - Redefinimos la que está establcida para inicializar los individuos en el rango permitido
---------------------------*/
void
InicializarIndividuo(GAGenome& g)
{
  GA1DArrayGenome<CampoIndividuo>& genome = (GA1DArrayGenome<CampoIndividuo>&)g;
  genome.gene(OPERADOR, GenerarCampoAleatorio(1, ResumenAnalizador[OPERADOR]));
  genome.gene(INSTRUCCION, GenerarCampoAleatorio(1, ResumenAnalizador[INSTRUCCION]));
  genome.gene(ATRIBUTO, GenerarCampoAleatorio(1, ResumenAnalizador[ATRIBUTO]));
  genome.gene(ESTADO, ORIG_INICIAL); // REVISTA: NO LO HEMOS EJECUTADO
}
/* -------------------------
   Función InicializarIndividuo
 - Redefinimos la que está establcida para inicializar los individuos en el rango permitido
---------------------------*/
CampoIndividuo
VerGen(GAGenome& g, int valor)
{
  GA1DArrayGenome<CampoIndividuo>& genome = (GA1DArrayGenome<CampoIndividuo>&)g;
  return (CampoIndividuo) genome.gene(valor);
}
/* -------------------------
   Función InicializarIndividuo
 - Redefinimos la que está establcida para inicializar los individuos en el rango permitido
---------------------------*/
void
EstablecerEstadoIndividuo(GAGenome& g, int estado)
{
  GA1DArrayGenome<CampoIndividuo>& genome = (GA1DArrayGenome<CampoIndividuo>&)g;
  genome.gene(ESTADO, estado);
}

void
EstablecerGenIndividuo(GAGenome& g, int gen, int estado)
{
  GA1DArrayGenome<CampoIndividuo>& genome = (GA1DArrayGenome<CampoIndividuo>&)g;
  genome.gene(gen, estado);
}

/* -------------------------
   Función InicializarIndividuo
 - Redefinimos la que está establcida para inicializar los individuos en el rango permitido
---------------------------*/
void
EstablecerTestIndividuo(GAGenome& g, int test, int resultado)
{
  GA1DArrayGenome<CampoIndividuo>& genome = (GA1DArrayGenome<CampoIndividuo>&)g;
  genome.gene(test, resultado);
}

/* -------------------------
   Función Mutacion
   - Redefinimos la que está establecida para poner la nuestra
   ---------------------------*/
int
Mutacion(GAGenome& g, float pmut)
{
  GA1DArrayGenome<CampoIndividuo>& genome = (GA1DArrayGenome<CampoIndividuo>&)g;
  int nmut = 0;
  int posicionmutada,valor;
  int op;
  // Determinar elemento del individuo a cambiar
  posicionmutada = GARandomInt(0,2);

  // Determinamos el operador sobre el que actuamos
  op = DeterminaOperador(genome);


// Calcular valor aleatorio dentro del rango permitido, en función de la probabilidad de mutación
  valor = (GenerarCampoAleatorio(1, (1-PMUTACION)*10)  % ResumenAnalizador[posicionmutada]) + 1 ;

  // Establecer nuevo valor
  genome.gene(posicionmutada, valor);
  // REVISTA: NO EVALUADO
  genome.gene(ESTADO, ORIG_MUTACION);
  nmut = 1;
  
  return nmut;
}

/* -------------------------
   Función Analizador 
 - Recibe como argumento el nombre del fichero bpel original
 - Completa la estructura Operadores con la información dada por el analizador
 - Devuelve el número de operadores de mutación que pueden aplicarse en el fichero bpel
---------------------------*/
void Analizador() {
   string operador, bufer; // para leer cada línea
   string llamada, fichanalizador;
   int i, cont;
   int mutantesmax=0;
   naturales numinstrucciones,numatributos;
   char *fichanalisis;

   // Abrir fichero resultado del analizador
   fichanalizador = ANALIZADOR ;
   fichanalisis = (char *)fichanalizador.c_str();

   // Llamar al analizador
   if (EJEANALIZADOR == 0) {
   llamada = "mubpel analyze " + BPELORIGINAL + " > " + fichanalizador;
   system(llamada.c_str());}

   // fichanalisis = (char *)malloc(fichanalizador.length());
   // fichanalizador.copy(fichanalisis, fichanalizador.length(),0);
   // cout << fichanalizador.length() << "-"
   //      << fichanalizador << "-"
   //      << fichanalisis
   //      << endl;

   ifstream ficheroanalisis(fichanalisis);

   ResumenAnalizador[OPERADOR] = 1;
   // Bucle lectura línea a línea
   while (getline (ficheroanalisis, bufer)) {
       // Crear flujo en memoria sobre bufer
       istringstream linea(bufer);
       // leemos operador y sus atributos
       OperadorInfo info;
       linea >> operador >> info.localidad >> info.atributo;
       if (info.localidad > 0) {
           info.operador = ResumenAnalizador[OPERADOR];
           ResumenAnalizador[OPERADOR]++;
       } else {
           info.operador = 0;
       }
       Operadores.push_back(info);
   }

   // Determinamos el número de operadores que pueden aplicarse
   ResumenAnalizador[OPERADOR]--;

// Calculamos los mcm de Instrucciones y Atributos
   for (i=0,cont=0; i<Operadores.size();i++)
       if (Operadores[i].operador != 0)  {
	  numinstrucciones.push_back(Operadores[i].localidad);
	  numatributos.push_back(Operadores[i].atributo);
	  mutantesmax += Operadores[i].localidad * Operadores[i].atributo;
       }
   ResumenAnalizador[INSTRUCCION] = (CampoIndividuo) mcmvector(numinstrucciones);
   ResumenAnalizador[ATRIBUTO] = (CampoIndividuo) mcmvector(numatributos);
   GENMUTANTES = (int)((PMUTANTES * (float)mutantesmax < 1)
                       ? 1 : PMUTANTES*(float)mutantesmax);
   MUTANTESMAX = mutantesmax;
}
/**********
 MCD de dos números naturales
****************/
natural mcd (natural a, natural b)
{
    while (b!= 0) {
	natural t=a%b;
	a=b;
	b=t;
    }
    return a;
}

/************
 MCM de dos números naturales
*************/

inline natural mcm (natural a, natural b)
{
    return a*(b/mcd(a,b));
}

/***************
 MCMVECTOR mcm de un vector de naturales
***************/

inline natural mcmvector (naturales v) 
{
    return accumulate (v.begin(), v.end(), 1, mcm);
}


/*--------------------------------------
  CONFIGURAR
  Lectura del fichero de configuración gamera.conf
  para introducir los valores de las variables globales

  ---------------------------------*/
void Configurar (char *fichero) 
{
    // Variable para almacenar las líneas del fichero
    string bufer,caracteristica,cadena;
    float valor; 

    // Abrir fichero de configuración
    ifstream ficheroconf((char *)fichero);//"gamera.conf");
    if (!ficheroconf) {
	cerr << "Error al leer fichero " << fichero << endl;//gamera.conf";
	exit(1);   
    }  

    // Bucle lectura línea a línea
    while (getline (ficheroconf, bufer)) {
	// Crear flujo en memoria sobre bufer
	istringstream linea(bufer);
	// leemos característica y su valor
	linea >> caracteristica >> valor >> cadena;
	if (caracteristica.compare("TAMPOBLACION") == 0)
	    TAMPOBLACION = (int) valor;
	else if (caracteristica.compare("NUMTEST") == 0)
	    NUMTEST = (int) valor;
	else if (caracteristica.compare("NUMGENERACIONES") == 0)
	    NUMGENERACIONES = (int) valor;
	else if (caracteristica.compare("PMUTACION") == 0)
	    PMUTACION = valor;
	else if (caracteristica.compare("PCRUCE") == 0)
	    PCRUCE = valor;
	else if (caracteristica.compare("PSUSTITUIR") == 0)
	    PSUSTITUIR = valor;
	else if (caracteristica.compare("PNUEVO") == 0)
	    PNUEVO = valor;
	else if (caracteristica.compare("BPELORIGINAL") == 0)
	    BPELORIGINAL = cadena;
	else if (caracteristica.compare("BPELOUT") == 0){
	    EJEBPEL=valor;
	    BPELOUT = cadena;
	}else if (caracteristica.compare("TESTSUITE") == 0)
	    TESTSUITE = cadena;
	else if (caracteristica.compare("SALIDAGAMERA") == 0)
	    SALIDAGAMERA = cadena;
	else if (caracteristica.compare("ESTADISTICAS") == 0)
	    ESTADISTICAS = cadena;
	else if (caracteristica.compare("PMUTANTES") == 0)
	    PMUTANTES = valor;
	else if (caracteristica.compare("DIFERENTES") == 0)
	    DIFERENTE = valor;
	else if (caracteristica.compare("BDMUTANTES") == 0)
	    BDMUTANTES = cadena;
	else if (caracteristica.compare("DIRECTORIOSALIDA") == 0)
	    DIRECTORIOSALIDA = cadena;
	else if (caracteristica.compare("MUTANTESCALIDAD") == 0)
	    MUTANTESCALIDAD = cadena;
	else if (caracteristica.compare("ANALIZADOR") == 0){
	    EJEANALIZADOR=valor;
	    ANALIZADOR = cadena;
	}}
    ficheroconf.close();
}


/*---------------------
LEERMUTANTESCALIDAD
  Lectura de los mutantes de calidad
----------------------------*/
void LeerMutantesCalidad() {
    string fichmutantescalidad, bufer, operador;
    char *ficheromutantescalidad;
    OperadorInfo info;
    fichmutantescalidad = MUTANTESCALIDAD;
    ficheromutantescalidad = (char *)fichmutantescalidad.c_str();

    ifstream ficherocalidad(ficheromutantescalidad);
    while (getline (ficherocalidad, bufer)) {
	istringstream linea(bufer);
	linea >> info.operador >> info.localidad >> info.atributo;
	LOSMUTANTESDECALIDAD.push_back (info.operador*10000+info.localidad*100+info.atributo*1);
    }
}

/*--------------------------------------
  CONVIERTEINDIVIDUO

  Convierte un individuo a mutante

  ---------------------------------*/
void ConvierteIndividuo(const GA1DArrayGenome<CampoIndividuo>& genome, vector<CampoIndividuo>& mutante) {

    // Determinamos el operador
    mutante[OPERADOR] = DeterminaOperador(genome);

    // Transformamos el campo instrucción

    mutante[INSTRUCCION]= 
	ceil((genome.gene(INSTRUCCION)
	      * Operadores[mutante[OPERADOR]-1].localidad
		 )
	     / (double)ResumenAnalizador[INSTRUCCION]);

    // Transformamos el campo atributo
    mutante[ATRIBUTO] = 
	ceil((genome.gene(ATRIBUTO)
	      * Operadores[mutante[OPERADOR]-1].atributo
		 )
	     / (double)ResumenAnalizador[ATRIBUTO]);
        
}

/*--------------------------------------
  MUESTRAPOBLACION

  Muestra la población del algoritmo genético en el fichero especificado

  ---------------------------------*/
void MostrarPoblacion (ofstream& fic,  int generacion,const  GAPopulation& pob)
{

  vector<CampoIndividuo> mutante(3);

  GA1DArrayGenome<CampoIndividuo>& genome = (GA1DArrayGenome<CampoIndividuo>&) pob.individual(0);

  fic << "----------GENERACION " << generacion <<"----------\n";

  for(int i=0; i<pob.size(); i++){

      genome = (GA1DArrayGenome<CampoIndividuo>&) pob.individual(i);

      // Traducimos individuo 
      ConvierteIndividuo(genome, mutante);

      // Mostramos en el fichero sus valores
      fic << mutante[OPERADOR] << "\t" << mutante[INSTRUCCION] << "\t"
	  << mutante[ATRIBUTO] << "\t" << equivalente(genome) << "\t" 
	  << genome.score() << endl;

  }
}
/*--------------------------------------
  DETERMINAOPERADOR

  Transforma la codificación de operador del algoritmo genético a los operadores de mutación que disponemos en el sistema

  ---------------------------------*/
int DeterminaOperador(const GA1DArrayGenome<CampoIndividuo>& genome) {

  for (int i=0; i<Operadores.size(); i++){
    if (Operadores[i].operador==genome.gene(OPERADOR)) {
      return i+1;
    }
  }
  cerr<<"ERROR GRAVE, NO ENCUENTRO OPERADOR"<<endl;
  exit(1);
}

/*--------------------------------------
  COMPARAMUTANTE

  Compara si dos mutantes son iguales devolviendo 1 en caso de igualdad
  ---------------------------------*/

int ComparaMutante(vector<CampoIndividuo>& m1,vector<CampoIndividuo>& m2) {
    if ((m1[0] == m2[0]) && (m1[1] == m2[1]) && (m1[2] == m2[2]))
	return 1;
    else
	return 0;
}

/*--------------------------------------
  INICIARPOBLACION

  Iniciamos una población de individuos 
  ---------------------------------*/
 
void IniciarPoblacion (GAPopulation & p){
    vector <CampoIndividuo> m1(3),m2(3);
    int i,j,igual;

    p.individual(0).initialize();

    GA1DArrayGenome<CampoIndividuo>& ind = (GA1DArrayGenome<CampoIndividuo>&)p.individual(0);
    ConvierteIndividuo(ind, m1);

    for(i=1; i<p.size(); i++) {
	p.individual(i).initialize();
	if (DIFERENTE == 1) { // si queremos población de distintos individuos
	    ConvierteIndividuo((GA1DArrayGenome<CampoIndividuo>&)p.individual(i),m1);
	    for (j=0, igual=0; (igual == 0) & (j<i); j++) { // Aseguramos población diferente de mutantes
		ConvierteIndividuo((GA1DArrayGenome<CampoIndividuo>&)p.individual(j),m2);
		igual = ComparaMutante(m1,m2);
	    }
	    if (igual == 1) {
		    i--; // Volvemos a Generar el mismo individuo de nuevo para que sea diferente
	    }
	}
    }
}

  
   
