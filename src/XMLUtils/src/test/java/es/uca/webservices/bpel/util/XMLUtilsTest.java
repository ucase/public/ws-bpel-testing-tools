package es.uca.webservices.bpel.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests for the {@link XMLUtils} class.
 *
 * @author Antonio García-Domínguez
 */
public class XMLUtilsTest {

	@Test
	public void stripEmptyIsEmpty() {
		assertStrippedDeclsEquals("", "");
	}

	@Test
	public void stripTextIsText() {
		assertStrippedDeclsEquals("raw text", "raw text");
	}

	@Test(expected=NullPointerException.class)
	public void stripNullIsNull() {
		assertStrippedDeclsEquals(null, null);
	}

	@Test
	public void stripValidXMLWithoutDeclaration() {
		assertStrippedDeclsEquals("<document/>", "<document/>");
	}

	@Test
	public void stripValidXMLWithDeclaration() {
		assertStrippedDeclsEquals("<?xml version=\"1.0\"?><document/>", "<document/>");
	}

	@Test
	public void onlyXMLDeclaration() {
		assertStrippedDeclsEquals("<?xml version=\"1.0\"?>", "");
	}

	private void assertStrippedDeclsEquals(final String source, final String expected) {
		final String result = XMLUtils.stripDeclarations(source);
		assertEquals(expected, result);
	}
}
