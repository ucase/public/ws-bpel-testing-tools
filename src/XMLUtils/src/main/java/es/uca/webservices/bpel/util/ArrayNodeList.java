package es.uca.webservices.bpel.util;

import java.util.ArrayList;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Simple NodeList implementation, backed by an ArrayList.
 * 
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class ArrayNodeList extends ArrayList<Node> implements NodeList {
	private static final long serialVersionUID = 1L;

	/**
     * Default implementation. Creates an empty list.
     */
    public ArrayNodeList() {}

    /**
     * Creates an instance from another node list.
     */
    public ArrayNodeList(NodeList nl) {
    	for (int i = 0; i < nl.getLength(); ++i) {
    		add(nl.item(i));
    	}
    }

    @Override
    public int getLength() {
        return size();
    }

    @Override
    public Node item(int index) {
        return index < size() ? get(index) : null;
    }
}
