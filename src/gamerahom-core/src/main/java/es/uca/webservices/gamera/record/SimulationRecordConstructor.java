package es.uca.webservices.gamera.record;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.constructor.AbstractConstruct;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.MappingNode;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.Tag;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.GAIndividual;

public class SimulationRecordConstructor extends Constructor {

	private class GAIndividualConstructor extends AbstractConstruct {
		@SuppressWarnings({ "unchecked" })
		@Override
		public Object construct(Node node) {
			final Map<?, ?> fields = constructMapping((MappingNode)node);

			final List<Integer> loadedOperator = (ArrayList<Integer>)fields.get(SimulationRecordRepresenter.GAIND_OPERATOR);
			final List<Integer> loadedLocation = (ArrayList<Integer>)fields.get(SimulationRecordRepresenter.GAIND_LOCATION);
			final List<Integer> loadedAttribute = (ArrayList<Integer>)fields.get(SimulationRecordRepresenter.GAIND_ATTRIBUTE);

			Integer order = (Integer)fields.get(SimulationRecordRepresenter.GAIND_ORDER);
			if (order == null) {
				order = 1;
			}

			Boolean normalized = (Boolean)fields.get(SimulationRecordRepresenter.GAIND_NORMALIZED);
			if (normalized == null) {
				normalized = false;
			}
			
			Object rawFitness = fields.get(SimulationRecordRepresenter.GAIND_FITNESS);
			Double fitness = null;
			if (rawFitness instanceof Number) {
				fitness = ((Number) rawFitness).doubleValue();
			}

			final BigInteger[] operator = toBigIntegerArray(loadedOperator);
			final BigInteger[] location = toBigIntegerArray(loadedLocation);
			final BigInteger[] attribute = toBigIntegerArray(loadedAttribute);

			GAIndividual ind = new GAIndividual(operator, location, attribute, order, normalized);
			ind.setFitness(fitness);
			return ind;
		}

		private BigInteger[] toBigIntegerArray(final List<Integer> l) {
			final BigInteger[] operator = new BigInteger[l.size()];
			for (int i = 0; i < l.size(); ++i) {
				if (l.get(i) != null) {
					operator[i] = BigInteger.valueOf(l.get(i));
				}
			}
			return operator;
		}
	}

	public SimulationRecordConstructor() {
		this(SimulationRecord.class);
	}

	public SimulationRecordConstructor(Class<?> rootType) {
		super(rootType);

		final TypeDescription descGAI = new TypeDescription(GAIndividual.class);
		descGAI.setTag(SimulationRecordRepresenter.GAIND_TAG);
		addTypeDescription(descGAI);

		final TypeDescription descResultsCR = new TypeDescription(ComparisonSimulationRecord.class);
		descResultsCR.putListPropertyType("results", ComparisonResult.class);
		addTypeDescription(descResultsCR);

		final TypeDescription descCompRecords = new TypeDescription(SimulationRecord.class);
		descCompRecords.putMapPropertyType("comparisonRecords", GAIndividual.class, ComparisonSimulationRecord.class);
		addTypeDescription(descCompRecords);

		final GAIndividualConstructor indCtor = new GAIndividualConstructor();
		this.yamlConstructors.put(new Tag(SimulationRecordRepresenter.GAIND_TAG), indCtor);
		this.yamlConstructors.put(new Tag("tag:yaml.org,2002:" + GAIndividual.class.getName()), indCtor);
	}

}
