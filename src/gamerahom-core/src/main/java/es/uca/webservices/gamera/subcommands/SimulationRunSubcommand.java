package es.uca.webservices.gamera.subcommands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.conf.Configuration;
import es.uca.webservices.gamera.exec.SimulatedExecutor;
import es.uca.webservices.gamera.term.PercentStrongMutantsCondition;

/**
 * Subclass of {@link ExecutionSubcommand} that implicitly replaces the
 * specified {@link GAExecutor} with a {@link SimulatedExecutor}. It also
 * provides some facilities for specifying additional termination conditions
 * that do not make much sense outside simulations.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
abstract class SimulationRunSubcommand extends ExecutionSubcommand {

	private static final String EXCLUDE_TESTS_OPTION = "exclude-tests";
	private static final String EXCLUDE_TESTS_FROM_FILE_OPTION = "exclude-tests-from";
	private static final String INCLUDE_TESTS_OPTION = "include-tests";
	private static final String INCLUDE_TESTS_FROM_FILE_OPTION = "include-tests-from";
	public static final String STRONG_OPTION = "strong";

	private Double percentStrong;
	private String includedTests, excludedTests;
	private File includedTestsFrom, excludedTestsFrom;

	public SimulationRunSubcommand(String name, String description) {
		super(2, 2, name, "config.yaml simrecord.yaml", description);
	}

	public Double getPercentStrong() {
		return percentStrong;
	}

	public void setPercentStrong(Double percentStrong) {
		this.percentStrong = percentStrong;
	}

	public String getIncludedTests() {
		return includedTests;
	}

	public void setIncludedTests(String includedTests) {
		this.includedTests = includedTests;
	}

	public String getExcludedTests() {
		return excludedTests;
	}

	public void setExcludedTests(String excludedTests) {
		this.excludedTests = excludedTests;
	}

	public File getIncludedTestsFrom() {
		return includedTestsFrom;
	}

	public void setIncludedTestsFrom(File includedTestsFrom) {
		this.includedTestsFrom = includedTestsFrom;
	}

	public File getExcludedTestsFrom() {
		return excludedTestsFrom;
	}

	public void setExcludedTestsFrom(File excludedTestsFrom) {
		this.excludedTestsFrom = excludedTestsFrom;
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		if (options.has(STRONG_OPTION)) {
			setPercentStrong((Double)options.valueOf(STRONG_OPTION));
		}
		if (options.has(INCLUDE_TESTS_OPTION)) {
			setIncludedTests((String)options.valueOf(INCLUDE_TESTS_OPTION));
		}
		if (options.has(EXCLUDE_TESTS_OPTION)) {
			setExcludedTests((String)options.valueOf(EXCLUDE_TESTS_OPTION));
		}
		if (options.has(INCLUDE_TESTS_FROM_FILE_OPTION)) {
			setIncludedTestsFrom(new File((String)options.valueOf(INCLUDE_TESTS_FROM_FILE_OPTION)));
		}
		if (options.has(EXCLUDE_TESTS_FROM_FILE_OPTION)) {
			setExcludedTestsFrom(new File((String)options.valueOf(EXCLUDE_TESTS_FROM_FILE_OPTION)));
		}
	}

	@Override
	protected OptionParser createOptionParser() {
		final OptionParser optionParser = super.createOptionParser();
		optionParser
			.accepts(STRONG_OPTION, "Stops when P * 100 % of all strong mutants are generated")
			.withRequiredArg().ofType(Double.class).describedAs("P");
		optionParser
			.accepts(INCLUDE_TESTS_OPTION,
					"Uses the tests in I, which is a comma-separated list of one or more " +
					"positions and ranges. Positions are 1-based indices over all the available" +
					" tests, or the name of a test. Ranges are of the form 'X-Y', where X and Y" +
					" are positions. By default, all tests are included.")
			.withRequiredArg().describedAs("I").ofType(String.class);
		optionParser
			.accepts(EXCLUDE_TESTS_OPTION,
					"Removes the tests in E, which follows the same format as the argument of " +
					"'include-tests'. 'exclude-tests' takes precedence over 'include-tests': if " +
					"a test is both included and excluded, it will be excluded. By default, no " +
					"tests are excluded.")
			.withRequiredArg().describedAs("E").ofType(String.class);
		optionParser
			.accepts(INCLUDE_TESTS_FROM_FILE_OPTION,
					"Like '" + INCLUDE_TESTS_OPTION + "', but reads the list positions and " +
					"ranges from the non-empty lines of the file at P. Takes precedence over '" +
					INCLUDE_TESTS_OPTION + "'.")
			.withRequiredArg().describedAs("P").ofType(String.class);
		optionParser
			.accepts(EXCLUDE_TESTS_FROM_FILE_OPTION,
					"Like '" + EXCLUDE_TESTS_OPTION + "', but reads the list positions and " +
					"ranges from the non-empty lines of the file at P. Takes precedence over '" +
					EXCLUDE_TESTS_OPTION + "'.")
			.withRequiredArg().describedAs("P").ofType(String.class);
		return optionParser;
	}

	@Override
	protected Configuration loadConfiguration(File file)
			throws FileNotFoundException, ClassNotFoundException,
			InvalidConfigurationException {
		final Configuration conf = super.loadConfiguration(file);

		final File recordSource = new File(getNonOptionArgs().get(1));
		SimulatedExecutor exec;
		try {
			exec = new SimulatedExecutor(recordSource);

			if (includedTestsFrom != null) {
				includedTests = loadPatternsFrom(includedTestsFrom);
			}
			if (excludedTestsFrom != null) {
				excludedTests = loadPatternsFrom(excludedTestsFrom);
			}
			exec.setIncludedTests(includedTests);
			exec.setExcludedTests(excludedTests);
		} catch (Exception ex) {
			throw new InvalidConfigurationException("Provided simulation record is not valid", ex);
		}

		if (conf.getMaxOrder() > exec.getRecord().getMaxOrder()) {
			throw new InvalidConfigurationException(String.format(
				"Maximum order in the run configuration (%d) is higher than that in the simulation record (%d)",
				conf.getMaxOrder(), exec.getRecord().getMaxOrder()));
		}
		conf.setExecutor(exec);

		if (getPercentStrong() != null) {
			final PercentStrongMutantsCondition cond = new PercentStrongMutantsCondition();
			cond.setPercent(getPercentStrong());
			cond.setSimulationRecord(exec.getRecord());
			conf.addTerminationCondition(cond);
		}
		return conf;
	}

	private String loadPatternsFrom(File f) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(f));
		try {
			StringBuffer patterns = new StringBuffer();
			boolean bFirst = true;
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				line = line.trim();
				if ("".equals(line)) {
					// Skip empty lines
					continue;
				}
	
				if (!bFirst) {
					patterns.append(',');
				}
				else {
					bFirst = false;
				}
				patterns.append(line);
			}
			return patterns.toString();
		} finally {
			reader.close();
		}
	}

}
