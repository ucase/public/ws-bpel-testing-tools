package es.uca.webservices.gamera.log;

import java.util.List;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.log.GALogger;

/**
 * Logger which forwards all events to a list of loggers.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public class AggregateLogger implements GALogger {

    private List<GALogger> loggers;

    /**
     * Creates a new aggregate logger.
     * 
     * @param loggers
     *            List of loggers which should be notified of each event. The
     *            list is not copied: if it is modified elsewhere, it will
     *            affect this aggregate logger. This class is <emph>not</emph>
     *            thread safe!
     */
    public AggregateLogger(List<GALogger> loggers) {
        this.loggers = loggers;
    }

    @Override
    public void finished(GAState state) {
        for (GALogger logger : loggers) {
            logger.finished(state);
        }
    }

    @Override
    public void finishedAnalysis(GAState state) {
        for (GALogger logger : loggers) {
            logger.finishedAnalysis(state);
        }
    }

    @Override
    public void finishedComparison(GAState state, GAPopulation population,
            GAIndividual individual, ComparisonResults comparison) {
        for (GALogger logger : loggers) {
            logger.finishedComparison(state, population, individual,
                            comparison);
        }
    }

    @Override
    public void finishedComparison(GAState state, GAPopulation population) {
        for (GALogger logger : loggers) {
            logger.finishedComparison(state, population);
        }
    }

    @Override
    public void finishedEvaluation(GAState state, GAPopulation population) {
        for (GALogger logger : loggers) {
            logger.finishedEvaluation(state, population);
        }
    }

    @Override
    public void newGeneration(GAState state, GAPopulation population) {
        for (GALogger logger : loggers) {
            logger.newGeneration(state, population);
        }
    }

    @Override
    public void started(GAState state) {
        for (GALogger logger : loggers) {
            logger.started(state);
        }
    }

    @Override
    public void startedAnalysis(GAState state) {
        for (GALogger logger : loggers) {
            logger.startedAnalysis(state);
        }
    }

    @Override
    public void startedComparison(GAState state, GAPopulation population) {
        for (GALogger logger : loggers) {
            logger.startedComparison(state, population);
        }
    }

    @Override
    public void startedComparison(GAState state, GAPopulation population,
            GAIndividual individual) {
        for (GALogger logger : loggers) {
            logger.startedComparison(state, population, individual);
        }
    }

    @Override
    public void startedEvaluation(GAState state, GAPopulation population) {
        for (GALogger logger : loggers) {
            logger.startedEvaluation(state, population);
        }
    }

	@Override
	public void appliedGeneticOperator(GAState state, String geneticOperatorName, 
			List<GAIndividual> parents, List<GAIndividual> children) {
        for (GALogger logger : loggers) {
            logger.appliedGeneticOperator(state, geneticOperatorName, parents, children);
        }
	}
	
    @Override
    public void validate() throws InvalidConfigurationException {
        if (loggers == null || loggers.isEmpty()) {
            throw new InvalidConfigurationException(
                    "Aggregate logger should contain at least one logger");
        }
    }

}
