package es.uca.webservices.gamera.conf;

import java.io.FileNotFoundException;
import java.io.InputStream;

import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.generate.GAIndividualGenerator;
import es.uca.webservices.gamera.api.generate.GAIndividualGeneratorOptions;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperator;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.api.select.GASelectionOperator;
import es.uca.webservices.gamera.api.select.GASelectionOperatorOptions;
import es.uca.webservices.gamera.api.term.GATerminationCondition;
import es.uca.webservices.gamera.generate.KnowledgeBasedGenerator;

/**
 * Loader for configuration files.
 *
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class ConfigurationLoader {

    static final String CLASS_TAG_PREFIX = "es.uca.webservices.gamera";

    /**
     * Loads the contents of a YAML file into this bean. Validation should be invoked separately.
     *
     * @throws FileNotFoundException
     *             The specified file does not exist.
     * @throws ClassNotFoundException
     *             {@link es.uca.webservices.gamera.conf.Configuration} was not found in the classpath.
     * @throws InvalidConfigurationException
     *             The YAML file was correctly parsed, but the loaded configuration is not valid.
     */
    public Configuration parse(InputStream in) throws FileNotFoundException, ClassNotFoundException, InvalidConfigurationException {
        Yaml parser = new Yaml(getConstructor());
        return (Configuration)parser.load(in);
    }

    private Constructor getConstructor() throws ClassNotFoundException {
        PackagePrefixConstructor ctor
            = new PackagePrefixConstructor(Configuration.class.getName(), CLASS_TAG_PREFIX);

        TypeDescription typeDesc = new TypeDescription(Configuration.class);
        typeDesc.putMapPropertyType("geneticOperators",
            GAGeneticOperator.class, GAGeneticOperatorOptions.class);
        typeDesc.putMapPropertyType("individualGenerators",
            GAIndividualGenerator.class, GAIndividualGeneratorOptions.class);
        typeDesc.putMapPropertyType("selectionOperators",
            GASelectionOperator.class, GASelectionOperatorOptions.class);
        typeDesc.putListPropertyType("terminationConditions", GATerminationCondition.class);
        typeDesc.putListPropertyType("loggers", GALogger.class);
        ctor.addTypeDescription(typeDesc);

        TypeDescription kbgTypeDesc = new TypeDescription(KnowledgeBasedGenerator.class);
        kbgTypeDesc.putMapPropertyType("ratios", String.class, Double.class);
        ctor.addTypeDescription(kbgTypeDesc);

        return ctor;
    }

}
