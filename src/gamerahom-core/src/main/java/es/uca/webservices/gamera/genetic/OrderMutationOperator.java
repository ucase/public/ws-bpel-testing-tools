package es.uca.webservices.gamera.genetic;

import java.util.Arrays;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.log.GALogger;

/**
 * Implements a mutation operator which modifies the individual order.
 * 
 * @author Antonio García-Domínguez, Juan José Domínguez-Jiménez, Emma
 *         Blanco-Muñoz
 * @version 1.0
 */

@Option(description="Implements a mutation operator which modifies the individual order.")
public class OrderMutationOperator extends AbstractMutationOperator {

	@Override
	public void apply(GAState state, GAPopulation source, GAPopulation destination, GALogger logger, boolean mutateAttribute, boolean createTree) {
		final GAIndividual individual = getSelector().select(source).denormalize(state.getAnalysisResults());

		// Range of the mutation 
		int range;
		if (getRandomRange() != null) {
			range = (int)Math.ceil(getRandomRange() * (1 - getScaleFactor()));
		} else {
			range = (int)Math.ceil(individual.getMaxOrder() * getRandomRangePercent() * (1 - getScaleFactor()));
		}

		// Magnitude of the current mutation (must be nonzero)
		int delta;
		do {
			delta = prng.nextInt(range);
		} while (range > 1 && delta == 0);

		// The current order will be between 1 and maxOrder
		final int currentOrder = individual.getOrder();
		final int newOrder = 1 + ((currentOrder + delta) % individual.getMaxOrder()); 

		final GAIndividual child = doMutation(individual, newOrder);
		
		logger.appliedGeneticOperator(state, OrderMutationOperator.class.getName(),
				Arrays.asList(individual), Arrays.asList(child));
		
		destination.addIndividual(child);
	}

	GAIndividual doMutation(final GAIndividual individual, int newOrder) {
		return new GAIndividual(individual, Math.max(1,Math.min(newOrder, individual.getMaxOrder())));
	}
}
