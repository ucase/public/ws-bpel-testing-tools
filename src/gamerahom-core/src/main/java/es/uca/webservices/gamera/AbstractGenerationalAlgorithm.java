package es.uca.webservices.gamera;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAFitnessFunction;
import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.api.term.GATerminationCondition;
import es.uca.webservices.gamera.api.util.ranges.StringArrayRangeListFilter;
import es.uca.webservices.gamera.conf.Configuration;
import es.uca.webservices.gamera.exec.SimulatedExecutor;

/**
 * Superclass for all the generational algorithms implemented in GAmera.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
public abstract class AbstractGenerationalAlgorithm {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractGenerationalAlgorithm.class);
	private Configuration configuration = new Configuration();

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	/**
	 * Runs the generational algorithm, returning its final state.
	 *
	 * @throws InvalidConfigurationException
	 *             The generator has not been correctly configured.
	 * @throws AnalysisException
	 *             There was a problem while analyzing the original program.
	 * @throws GenerationException
	 *             There was a problem while generating individuals.
	 * @throws ComparisonException
	 *             There was a problem while comparing the output of the
	 *             original program and the mutant again the test suite.
	 * @throws PreparationException
	 *             There was a problem while setting up the executor-
	 */
	public GAState run() throws InvalidConfigurationException, AnalysisException,
			GenerationException, ComparisonException, PreparationException {
		validate();

		final Configuration configuration = getConfiguration();
		final GAState state = new GAState();
		final GALogger aggregateLogger = configuration.getAggregateLogger();
		final GAExecutor executor = configuration.getExecutor();
		final List<GATerminationCondition> terminationConditions = configuration.getTerminationConditions();
		Set<GAIndividual> newIndAddedToHOF = new HashSet<GAIndividual>();
		
		prepare(state, aggregateLogger, executor);
		try {
			analyze(state, aggregateLogger, executor);
			if (configuration.getPopulationSizeAsPercentOfTotalMutants() != null) {
				final double percentTotal = configuration.getPopulationSizeAsPercentOfTotalMutants();
				final int popSize = (int)Math.ceil(percentTotal * state.getTotalNumberOfMutants());
				configuration.setPopulationSize(popSize);
				state.setSizePopulation(popSize);
			}

			/* FIRST GENERATION */

			// The first generation by default is numbered zero, and its "previous" generation is always empty
			GAPopulation currentPopulation = new GAPopulation();
			
			// Now, all individuals of the initial population are created
			generateFirstGeneration(state, currentPopulation);
			aggregateLogger.newGeneration(state, currentPopulation);
			
			// It is necessary to update the Hall of Fame (HOF) in order to
			// record all the individuals created and their execution rows
			
			// The result will be a set of the normalized individuals which have been added to the HOF

			newIndAddedToHOF = updateHOF(executor, currentPopulation, state, aggregateLogger);

			/* FOLLOWING GENERATIONS */

			// Two populations are maintained along the generations
			GAPopulation prevPopulation;

			// If the state is true, the algorithm should terminate
			while (!checkTerminationConditions(terminationConditions, state)) {
				evaluate(currentPopulation, newIndAddedToHOF, executor, state, aggregateLogger);

				// Switch to the next generation
				prevPopulation = currentPopulation;
				currentPopulation = new GAPopulation();
				state.setCurrentGeneration(state.getCurrentGeneration() + 1);
				
				// The Hall of Fame is shared by all generations
				nextGeneration(state, currentPopulation, prevPopulation, aggregateLogger);
				

				// Before finishing the current generation, HOF is updated
				newIndAddedToHOF = updateHOF(executor, currentPopulation, state, aggregateLogger);

				aggregateLogger.newGeneration(state, prevPopulation);
			}
					
			return state;
		} finally {
			cleanup(state, aggregateLogger, executor);
		}
	}

	protected AnalysisResults analyze(final GAState state, final GALogger aggregateLogger, final GAExecutor executor) throws AnalysisException {
		// Analyzes the original program and reports the required parameters for
		// the genetic algorithm. Results can be filtered by the available
		// configuration.
		aggregateLogger.startedAnalysis(state);
		AnalysisResults analysis = executor.analyze(null);
		analysis = applyOperatorFilters(executor, analysis);

		state.setAnalysisResults(analysis);
		if (executor instanceof SimulatedExecutor) {
			state.addCachedNanos(((SimulatedExecutor) executor).getRecord()
					.getAnalysisRecord().getTotalWallNanos());
		}
		aggregateLogger.finishedAnalysis(state);

		// Calculates the total number of mutants
		state.setTotalNumberOfMutants(calculateTotalNumberOfMutants(analysis,
				configuration.getMaxOrder()).longValue());

		// Creates the Hall of Fame with the analysis calculated before
		state.setHof(new GAHof(analysis));
		
		// Set the value for the population size used in the fitness
		state.setSizePopulation(configuration.getPopulationSize());
		
		return analysis;
	}

	protected void prepare(final GAState state, final GALogger aggregateLogger,
			final GAExecutor executor) throws PreparationException {
		executor.prepare();
		
		final Configuration configuration = getConfiguration();
		//Added for TCE
		if(configuration.isTceEquivalent()){
			FileReader fR = null;
			BufferedReader bR = null;
			try {
				fR = new FileReader(configuration.getTcefEquiv());
				bR = new BufferedReader(fR);

				String line;
				while ((line = bR.readLine()) != null) {
					state.getEquivalentMutants().add(line);
				}
				
			} catch (Exception e) {
				throw new PreparationException("Not possible to read the file " + configuration.getTcefEquiv().getPath());
			} finally {
				try {
					fR.close();
				} catch (Exception e) {
					throw new PreparationException("Not possible to close the file " + configuration.getTcefEquiv().getPath());
				}
			}
		}
		
		if(configuration.isTceDuplicate()){
			FileReader fR = null;
			BufferedReader bR = null;
			try {
				fR = new FileReader(configuration.getTcefDupl());
				bR = new BufferedReader(fR);

				String line;
				while ((line = bR.readLine()) != null) {
					String[] mutants = line.split(" ");
					List<String> mutantsLine = new ArrayList<String>();
					for(String s : mutants){
						//Add mutants that are duplicate among them 
						mutantsLine.add(s);
					}
					//Finally, add this new list of duplicate mutants to the complete list of duplicate mutants
					state.getDuplicateMutants().add(mutantsLine);
				}
				
			} catch (Exception e) {
				throw new PreparationException("Not possible to read the file " + configuration.getTcefDupl().getPath());
			} finally {
				try {
					fR.close();
				} catch (Exception e) {
					throw new PreparationException("Not possible to close the file " + configuration.getTcefDupl().getPath());
				}
			}
		}
		
		if (executor instanceof SimulatedExecutor) {
			state.addCachedNanos(((SimulatedExecutor) executor).getRecord()
					.getPrepareWallNanos());
		}
		aggregateLogger.started(state);
	}

	protected void cleanup(final GAState state, final GALogger aggregateLogger,	final GAExecutor executor) throws PreparationException {
		executor.cleanup();
		if (executor instanceof SimulatedExecutor) {
			state.addCachedNanos(((SimulatedExecutor) executor).getRecord()
					.getCleanupWallNanos());
		}

		// We should recompute the fitness of each individual before the last
		// call to the GALogger, as it might have changed over the generations
		final GAHof hof = state.getHof();
		for (GAIndividual i : hof.keySet()) {
			assignFitness(i, state);
		}

		// Done!
		aggregateLogger.finished(state);
	}

	protected ComparisonResults[] compare(GAExecutor executor,
			GAPopulation population, GAState state, GALogger aggregateLogger,
			GAIndividual[] ind2HOF) throws ComparisonException {
		if (aggregateLogger != null) {
			aggregateLogger.startedComparison(state, population);
		}

		ComparisonResults[] comparisons = executor.compare(null, ind2HOF);
		if (executor instanceof SimulatedExecutor) {
			// SimulatedExecutors always used cached times instead of their own.
			// The rest are all assumed to *really* run the mutants, so we do not
			// need to do this. We need to use the results from the comparison
			// results and not the simulation records, as the user may have set
			// stopAtFirstDiff to true, and only the executor itself honors that
			// flag.
			for (ComparisonResults cr : comparisons) {
				state.addCachedNanos(cr.getTotalNanos());
			}
		}

		if (aggregateLogger != null) {
			aggregateLogger.finishedComparison(state, population);
		}
		return comparisons;
	}

	/**
	 * Validates that the generator has been correctly configured.
	 * 
	 * @throws InvalidConfigurationException
	 *             The generator has not been correctly configured.
	 */
	protected void validate() throws InvalidConfigurationException {
		if (configuration == null) {
			throw new InvalidConfigurationException(
					"The generator must be configured before being run.");
		}
	}

	/**
	 * It evaluates if the genetic algorithm should terminate depending on the
	 * termination conditions or other factors.
	 * 
	 * @param terminationConditions
	 *            Termination conditions
	 * @param state
	 *            State of the genetic algorithm
	 * @return <code>true</code> if the algorithm should terminate its
	 *         execution, or <code<false</code> if it should continue.
	 */
	protected boolean checkTerminationConditions(List<GATerminationCondition> terminationConditions, GAState state) {
		// The condition of every termination conditions in order to verify if
		// the algorithm should terminate or not
		for (GATerminationCondition cond : terminationConditions) {
			// If it is true, the condition is met
			if (cond.evaluate(state)) {
				LOGGER.info("Termination condition {} is true: done", cond);
				
				//Nuevo: si ha puesto el árbol, mostrar
				if(configuration.isCreateTree()){	
					if(!state.getAncestors().isEmpty()){
						
						int lineTree = 1;
						PrintStream ps2;
				
						try {
							ps2 = new PrintStream(new File(configuration.getTreeResults().toString()));
						
							ps2.flush();
							
							for (String key : state.getAncestors().keySet()) {
								ps2.print(lineTree);
								lineTree++;
								ps2.print("\t-\t");
								ps2.print(key);
								ps2.print("\t");
								ps2.print("ANC1");
								ps2.print("\t");
								
								ps2.print(state.getAncestors().get(key).get(0));
								
								ps2.print("\t");
								ps2.print("ANC2");
								ps2.print("\t");
								
								if(state.getAncestors().get(key).size() > 1)
									ps2.print(state.getAncestors().get(key).get(1));
								
								ps2.println();
							}
						    
							ps2.close();
							
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
					}
				}
				
				return true;
			}
			// Otherwise, we continue looking for until a condition met
		}
		// If there is no met condition, we return false
		return false;
	}

	/**
	 * Creates the next generation from the previous one and the analysis
	 * results. The method may use some of the components registered in the
	 * configuration, or not.
	 */
	protected abstract void nextGeneration(GAState state,
			GAPopulation currentPopulation, GAPopulation prevPopulation,
			GALogger logger)
			throws ComparisonException, GenerationException;

	/**
	 * Creates the first generation from the analysis results. It may use the
	 * individual generators registered in the configuration, or not.
	 */
	protected abstract void generateFirstGeneration(GAState state,
			GAPopulation destination);

	private AnalysisResults applyOperatorFilters(final GAExecutor executor, AnalysisResults analysis) throws AnalysisException {
		if (configuration.getExcludedOperators() == null && configuration.getIncludedOperators() == null) {
			return analysis;
		}

		try {
			final StringArrayRangeListFilter filter = new StringArrayRangeListFilter(
					Arrays.asList(analysis.getOperatorNames()),
					configuration.getIncludedOperators(),
					configuration.getExcludedOperators());
			if (executor instanceof SimulatedExecutor) {
				((SimulatedExecutor)executor).getRecord().filterOperators(filter);
				analysis = executor.analyze(null);
			}
			else {
				analysis = analysis.filterOperators(filter);
			}
		} catch (IllegalArgumentException ex) {
			throw new AnalysisException("There was an error while filtering the operators", ex);
		}

		return analysis;
	}

	/**
	 * For each X-operator with Y-locations and Z-attributes, we can get Y*Z
	 * mutants. But faced with a maximum order N, these possible mutants are
	 * combinable for each order. So, if we calculate the sum of these
	 * combinations, the total number of mutants will be represented by the
	 * geometric progression: M + M² + M³ + ... + M^N, M means the sum of the
	 * combinations and N the maximum order.
	 * 
	 * @param analysis
	 *            Analysis Results
	 * @param maxOrder
	 *            Maximum Order of the individuals
	 */
	private BigInteger calculateTotalNumberOfMutants(AnalysisResults analysis, int maxOrder) {
		BigInteger combinations = BigInteger.ZERO;

		final int maxOp = analysis.getMaxOp().intValue();
		for (int i = 0; i < maxOp; i++) {
			combinations = combinations.add(analysis.getFilteredLoc()[i].multiply(analysis.getFilteredAtt()[i]));
		}

		return combinations.multiply((BigInteger.ONE.subtract(combinations.pow(maxOrder))).divide(BigInteger.ONE.subtract(combinations)));
	}

	/**
	 * Update the HOF of the population adding new individuals and recalculating
	 * the fitness of the individuals
	 * 
	 * @param executor
	 *            It generates and runs mutants
	 * @param population
	 *            Current population
	 * @param state
	 *            Overall state of the algorithm
	 * @param aggregateLogger
	 *            Logger which forwards all events to a list of loggers. It can
	 *            be null because is called by an auxiliary operation in the
	 *            genetic operator.
	 * @throws GenerationException
	 *             There was a problem while generating individuals.
	 * @throws ComparisonException
	 *             There was a problem while comparing the output of the
	 *             original program and the mutant again the test suite.
	 * @return A set of the new individuals added to the HOF.
	 */
	private Set<GAIndividual> updateHOF(GAExecutor executor, GAPopulation population,
			GAState state, GALogger aggregateLogger)
			throws GenerationException, ComparisonException
	{
		final List<GAIndividual> individuals = population.getIndividuals();
		final Set<GAIndividual> individuals2HOF = new HashSet<GAIndividual>();
		final GAHof hof = state.getHof();
		final AnalysisResults analysis = state.getAnalysisResults();

		// We check the current population in order to identify the unique individuals
		// to add in the HOF which are not repeated neither the HOF nor in the population
		for (GAIndividual ind : individuals) {
			if (!hof.containsKey(ind)) {
				if (!individuals2HOF.contains(ind.normalize(analysis))) {
					individuals2HOF.add(ind.normalize(analysis));
				}
			} else {
				//System.out.println("Already in HOF: " + ind);
			}
		}

		// If there is at least an individual that has to be included in the HOF...
		if (!individuals2HOF.isEmpty()) {
			// The type of the parameter of the individuals to include in the
			// HOF has to be adapted from List<GAIndividual> to GAIndividual[]
			// in order to applying the varargs
			GAIndividual[] ind2HOF = new GAIndividual[individuals2HOF.size()];
			ind2HOF = (GAIndividual[]) individuals2HOF.toArray(ind2HOF);

			// Run all mutants at the same time, to take advantage of parallelism
			ComparisonResults[] comparisons = compare(executor, population,	state, aggregateLogger, ind2HOF);

			// Add the new results to the existing HOF and finally, update the execution matrix with the
			// execution results of the new population and the results of the individual's HOF
			hof.concat2HOF(ind2HOF, comparisons);
			state.updateExecutionMatrix(hof, population);
		}
		
		return individuals2HOF;
	}

	private void evaluate(GAPopulation population, Set<GAIndividual> newIndAddedToHOF, GAExecutor executor, GAState state, GALogger aggregateLogger) {
		//final GAFitnessFunction fit = configuration.getFitnessFunction();

		aggregateLogger.startedEvaluation(state, population);
		
		for (GAIndividual ind : population.getIndividuals()) {
			GAIndividual normalizedInd = ind.normalize(state.getAnalysisResults());
			// The fitness only will be calculated if the individual of the current population
			// fits in with one of the last individuals added to the HOF.
			// Attention, these individuals are normalized
			if(newIndAddedToHOF.contains(normalizedInd)) {
				
				//ind.setFitness(fit.computeFitness(ind, state));
				assignFitness(ind, state);
				
				// It is necessary remove this last individual added to the HOF,
				// just in case there is another identical individual in the population
				// (its fitness will be zero)
				newIndAddedToHOF.remove(normalizedInd);
			} else {
				// If there is no elements in lastOnesAddedToHof the fitness of the population
				// individuals will be zero for all of them.
				ind.setFitness(0);
			}
		}
		aggregateLogger.finishedEvaluation(state, population);
	}
	
	/* Asigns a fitness to an individual checking whether the mutant is TCE equivalent or not */
	
	private void assignFitness(GAIndividual ind, GAState state){
		final GAFitnessFunction fit = configuration.getFitnessFunction();
		
		//isTceDuplicate = false and isTceEquivalente = false
		if(!configuration.isTceEquivalent() && !configuration.isTceDuplicate())
			ind.setFitness(fit.computeFitness(ind, state));
		else{

			GAIndividual normalizedInd = ind.normalize(state.getAnalysisResults());
			String mutantName = "m" + state.normalizeCode(normalizedInd.getOperator().get(0)) + "_" + normalizedInd.getLocation().get(0) + "_" + normalizedInd.getAttribute().get(0);
			
			if(configuration.isTceEquivalent()){
				//isTceEquivalent = true => Check whether it is equivalent or not. 
				//Apply lowest fitness to those mutants that are TCE equivalent
				//if(state.getEquivalentMutants().contains(executor.mutantName(normalizedInd)))
				if(state.getEquivalentMutants().contains(mutantName))
					ind.setFitness(0);
				else{
					//If it is not equivalent, check whether it is duplicate or not
					//Apply lowest fitness to those mutants that are TCE duplicate
					if(configuration.isTceDuplicate()){
						//isTceDuplicate = true and isTceEquivalent = true
						if(state.isDuplicateMutant(mutantName)){
							ind.setFitness(0);
						}
						else
							ind.setFitness(fit.computeFitness(ind, state));
					}
					else{
						//isTceDuplicate = false and isTceEquivalent = true
						ind.setFitness(fit.computeFitness(ind, state));
					}
				}	
			}
			else{
				//isTceDuplicate = true and isTceEquivalent = false
				if(state.isDuplicateMutant(mutantName)){
					ind.setFitness(0);
				}
				else
					ind.setFitness(fit.computeFitness(ind, state));
			}
		}
	}
}
