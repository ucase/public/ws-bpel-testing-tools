package es.uca.webservices.gamera.subcommands;

import java.io.File;
import java.io.FileNotFoundException;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.conf.Configuration;
import es.uca.webservices.gamera.log.HofLogger;
import es.uca.webservices.gamera.term.PercentAllMutantsCondition;

/**
 * Base class for all subcommands that involve running the genetic
 * algorithm or simulating all or parts of it. Some of the shared
 * options allow for quickly adding certain termination conditions
 * or loggers, without editing the original .yaml file.
 *
 * @author Antonio García-Domínguez
 */
abstract class ExecutionSubcommand extends AbstractSubcommand {

	private static final String EXCLUDE_OPS_OPTION = "exclude-ops";
	private static final String INCLUDE_OPS_OPTION = "include-ops";
	private static final String HOF_OPTION = "hof";
	private static final String SEED_OPTION = "seed";
	public static final String ALL_OPTION = "all";
	private static final String POPULATION_SIZE_OPTION = "popsize";

	private Double percentAll;
	private String excludedOperators, includedOperators;
	private String hofReportPath;
	private String seed;
	private Integer populationSize;

	public ExecutionSubcommand(
			int minArgumentCount, int maxArgumentCount,
			String name, String usage, String description)
	{
		super(minArgumentCount, maxArgumentCount, name, usage, description);
	}

	public Double getPercentAll() {
		return percentAll;
	}

	public void setPercentAll(Double percentAll) {
		this.percentAll = percentAll;
	}

	public String getExcludedOperators() {
		return excludedOperators;
	}

	public void setExcludedOperators(String excludedOperators) {
		this.excludedOperators = excludedOperators;
	}

	public String getIncludedOperators() {
		return includedOperators;
	}

	public void setIncludedOperators(String includedOperators) {
		this.includedOperators = includedOperators;
	}

	public String getHofReportPath() {
		return hofReportPath;
	}

	public void setHofReportPath(String path) {
		this.hofReportPath = path;
	}

	public String getSeed() {
		return seed;
	}

	public void setSeed(String seed) {
		this.seed = seed;
	}

	public Integer getPopulationSize() {
		return populationSize;
	}

	public void setPopulationSize(Integer populationSize) {
		this.populationSize = populationSize;
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);

		if (options.has(ALL_OPTION)) {
			setPercentAll((Double)options.valueOf(ALL_OPTION));
		}
		if (options.has(INCLUDE_OPS_OPTION)) {
			setIncludedOperators((String)options.valueOf(INCLUDE_OPS_OPTION));
		}
		if (options.has(EXCLUDE_OPS_OPTION)) {
			setExcludedOperators((String)options.valueOf(EXCLUDE_OPS_OPTION));
		}
		if (options.has(HOF_OPTION)) {
			setHofReportPath((String)options.valueOf(HOF_OPTION));
		}
		if (options.has(SEED_OPTION)) {
			setSeed((String)options.valueOf(SEED_OPTION));
		}
		if (options.has(POPULATION_SIZE_OPTION)) {
			setPopulationSize((Integer)options.valueOf(POPULATION_SIZE_OPTION));
		}
	}

	@Override
	protected OptionParser createOptionParser() {
		final OptionParser parser = super.createOptionParser();
		parser
			.accepts(ALL_OPTION, "Stops when P * 100 % of all mutants have been generated")
			.withRequiredArg().describedAs("P").ofType(Double.class);
		parser
			.accepts(INCLUDE_OPS_OPTION,
					"Only use the operators listed in I, which is a comma-separated list " +
					"of positions and ranges. Ranges are of the form 'X-Y', where X and Y " +
					"are positions. Positions can be 1-based indices in the list of " +
					"operators produced by the analyzer, or the names of the operators at " +
					"those indices themselves. By default, all operators are used.")
			.withRequiredArg().describedAs("I").ofType(String.class);
		parser
			.accepts(EXCLUDE_OPS_OPTION,
					"Do not use the operators listed in E, which has the same format as in " +
					"'include-ops'. Takes precedence over 'include-ops': if an operator is b" +
					"oth excluded and included, it will be excluded. By default, no " +
					"operators are excluded.")
			.withRequiredArg().describedAs("E").ofType(String.class);
		parser
			.accepts(HOF_OPTION,
					"Registers a new HofLogger that writes the Hall of Fame produced by this " +
					"algorithm to a new file located at the relative or absolute path P.")
			.withRequiredArg().describedAs("P").ofType(String.class);
		parser
			.accepts(SEED_OPTION,
					"Changes the seed to the bytes of the specified string.")
			.withRequiredArg().describedAs("S").ofType(String.class);
		parser
			.accepts(POPULATION_SIZE_OPTION, "Changes the population size.")
			.withRequiredArg().describedAs("N").ofType(Integer.class);

		return parser;
	}

	@Override
	protected Configuration loadConfiguration(File file)
			throws FileNotFoundException, ClassNotFoundException,
			InvalidConfigurationException
	{
		final Configuration config = super.loadConfiguration(file);
		if (getPercentAll() != null) {
			final PercentAllMutantsCondition cond = new PercentAllMutantsCondition();
			cond.setPercent(getPercentAll());
			config.addTerminationCondition(cond);
		}
		config.setIncludedOperators(getIncludedOperators());
		config.setExcludedOperators(getExcludedOperators());
		if (getHofReportPath() != null) {
			final HofLogger logger = new HofLogger();
			logger.setConsole(false);
			logger.setFile(getHofReportPath());
			config.getLoggers().add(logger);
		}
		if (getSeed() != null) {
			config.setSeed(getSeed());
		}
		if (getPopulationSize() != null) {
			config.setPopulationSize(getPopulationSize());
		}
		return config;
	}
}
