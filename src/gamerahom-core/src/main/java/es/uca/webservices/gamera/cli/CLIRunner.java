package es.uca.webservices.gamera.cli;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.subcommands.ExecutionTableSubcommand;
import es.uca.webservices.gamera.subcommands.FitnessDistanceTableSubcommand;
import es.uca.webservices.gamera.subcommands.FitnessMapSubcommand;
import es.uca.webservices.gamera.subcommands.FitnessTableSubcommand;
import es.uca.webservices.gamera.subcommands.GARunSubcommand;
import es.uca.webservices.gamera.subcommands.GASimulationRunSubcommand;
import es.uca.webservices.gamera.subcommands.ISubcommand;
import es.uca.webservices.gamera.subcommands.MixRunSimulationRunSubcommand;
import es.uca.webservices.gamera.subcommands.MixRunSubcommand;
import es.uca.webservices.gamera.subcommands.RandomRunSubcommand;
import es.uca.webservices.gamera.subcommands.RandomSimulationRunSubcommand;
import es.uca.webservices.gamera.subcommands.SimulationMetricsSubcommand;
import es.uca.webservices.gamera.subcommands.SimulationRecordSubcommand;
import es.uca.webservices.gamera.subcommands.SubsumingMutantsSubcommand;

/**
 * Command-line launcher for GAmera.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public final class CLIRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(CLIRunner.class);
	
    private static Properties props;
	static final int BAD_USAGE = 1;
    private static final int FILE_NOT_FOUND = BAD_USAGE + 1;
    private static final int CLASS_NOT_FOUND = FILE_NOT_FOUND + 1;
    static final int INVALID_CONFIGURATION = CLASS_NOT_FOUND + 1;
    private static final int ERROR_ANALYZING = INVALID_CONFIGURATION + 1;
    private static final int ERROR_COMPARING = ERROR_ANALYZING + 1;
    private static final int ERROR_PREPARING = ERROR_COMPARING + 1;
    private static final int ERROR_GENERATING = ERROR_PREPARING + 1;
    private static final int ERROR_IO = ERROR_GENERATING + 1;
    private static final int FATAL_ERROR = 254;

    // This line follows a Sonar advice: Hide Utility Class Constructor
    private CLIRunner(){}
    
    /**
     * Entry point to GAmera from the command line.
     * 
     * @param args
     *            Command line arguments: currently, only the path to the YAML
     *            configuration file is needed.
     */
    public static void main(String[] args) throws ComparisonException, GenerationException {
        int statusCode = mainMethod(args);
        if (statusCode != 0) {
            System.exit(statusCode);
        }
    }

    public static String getVersion() throws IOException {
		return getProperty("gamera.version");
	}

	/**
     * Main method of this class. Useful for unit testing:
     * {@link System#exit(int)} cannot be used there, and we cannot directly
     * access what was printed to the standard error stream.
     * 
     * @param err
     *            {@link java.io.PrintStream} where errors in the main launcher
     *            should be printed to. To redirect the output of the algorithm
     *            itself, use the appropriate GALogger.
     * @param args
     *            Command line arguments.
     */
    static int mainMethod(String[] args) {
        try {
        	final Map<String, ISubcommand> subcmdMap = getSubcommandMap();

        	// Split into subcommand + subcommand args
        	if (args.length == 0) {
        		return printUsage(subcmdMap);
        	}
        	ISubcommand cmd = subcmdMap.get(args[0]);
        	List<String> cmdArgs;
        	if (cmd == null) {
        		if (args.length == 1) {
        			cmd = new GARunSubcommand();
        			cmdArgs = Arrays.asList(args);
        		} else {
        			return printUsage(subcmdMap);
        		}
        	}
        	else {
        		cmdArgs = Arrays.asList(args).subList(1, args.length);
        	}

        	try {
        		cmd.parseArgs(cmdArgs.toArray(new String[cmdArgs.size()]));
        	} catch (IllegalArgumentException ex) {
        		LOGGER.error(ex.getMessage());
        		return printUsage(subcmdMap);
        	}
        	
        	cmd.run();
            return 0;
        } catch (FileNotFoundException e) {
        	LOGGER.error("File not found", e);
            return FILE_NOT_FOUND;
        } catch (ClassNotFoundException e) {
        	LOGGER.error("Class not found", e);
             return CLASS_NOT_FOUND;
        } catch (InvalidConfigurationException e) {
        	LOGGER.error("Invalid configuration", e);
            return INVALID_CONFIGURATION;
        } catch (AnalysisException e) {
        	LOGGER.error("Error while analyzing original program", e);
            return ERROR_ANALYZING;
        } catch (GenerationException e) {
        	LOGGER.error("Error while generating a mutant", e);
        	return ERROR_GENERATING;
		} catch (ComparisonException e) {
			LOGGER.error("Error while comparing a mutant with the original program", e);
			return ERROR_COMPARING;
		} catch (PreparationException e) {
			LOGGER.error("Error while preparing the executor", e);
			return ERROR_PREPARING;
		} catch (IOException e) {
			LOGGER.error("I/O error", e);
			return ERROR_IO;
		} catch (Exception e) {
			LOGGER.error("Unknown error", e);
			return FATAL_ERROR;
		}
    }

	public static Map<String, ISubcommand> getSubcommandMap() {
		final Map<String, ISubcommand> subcmdMap = new TreeMap<String, ISubcommand>();
		final ISubcommand[] subcmds = new ISubcommand[] {
			new GARunSubcommand(),
			new RandomRunSubcommand(),
			new MixRunSubcommand(),
			new GASimulationRunSubcommand(),
			new RandomSimulationRunSubcommand(),
			new MixRunSimulationRunSubcommand(),
			new SimulationRecordSubcommand(),
			new SimulationMetricsSubcommand(),
			new FitnessMapSubcommand(),
			new FitnessDistanceTableSubcommand(),
			new FitnessTableSubcommand(),
			new ExecutionTableSubcommand(),
			new SubsumingMutantsSubcommand()
		};
		for (ISubcommand subcmd : subcmds) {
			subcmdMap.put(subcmd.getName(), subcmd);
		}
		return subcmdMap;
	}

	private static int printUsage(Map<String, ISubcommand> subcmdMap) {
		try {
			System.err.println("GAmera version " +  getVersion());
			System.err.println();
			System.err.println("Available subcommands:\n");

			for (Map.Entry<String, ISubcommand> entry : subcmdMap.entrySet()) {
				System.err.println(" * " + entry.getKey() + " " + entry.getValue().getUsage());
			}
			System.err.println();

			System.err.println("For more help about a subcommand, use '" + getProperty("launcher.name") + " subcmd -h'");
		    return BAD_USAGE;
		} catch (IOException e) {
		    LOGGER.error("Fatal error: cannot read main properties file");
		    return FATAL_ERROR;
		}
	}

    private static synchronized String getProperty(String property) throws IOException {
        if (props == null) {
            props = new Properties();
            props.load(CLIRunner.class
                    .getResourceAsStream("/gamera.properties"));
        }
        return props.getProperty(property);
    }
}
