package es.uca.webservices.gamera.log;

import java.io.File;
import java.io.PrintStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Structure which has the stream to get the impression by console or to a file
 *
 * @author Emma Blanco-Muñoz
 * @version 1.0
 */
public class PrintingByConsoleOrFile {

	static final Logger LOGGER = LoggerFactory.getLogger(PrintingByConsoleOrFile.class);
	 
	private PrintStream ps = null;

	/**
	 * Prints a message by console or to a file  with a timestamp
	 *  
	 * @param s
	 * 		String to print
	 * @param elapsed
	 *      Elapsed time since the test was started
	 * @param console
	 * 		Boolean which shows if the console is enabled or not
	 * @param file
	 * 		File path 
	 */
    public void printMsgByConsoleFile(String s, BigInteger elapsed, boolean console, String file) {
    	if (ps == null) {
	    	createStream(console, file);
    	}
    	ps.println(String.format("%-20s [%6s ms] %s", now(), elapsed.toString(), s));
    }

    /**
     * Create the stream to print by console or to a file
     * 
     * @param console
     * @param file
     */
	public void createStream(boolean console, String file) {
		if(console) {
			ps = System.out;
		} else {
			try {
				ps = new PrintStream(new File(file));
			} catch (Exception e) {
				LOGGER.error("File not found", e);
			}
		}
	}
    
	/**
	 * Gets the date and the time with a defined format
	 *  
	 * @return String with the datetime 
	 */
    private String now() {
    	Calendar c = Calendar.getInstance();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss  ");
    	return sdf.format(c.getTime());
    }
    
    /**
	 * Returns the stream to printing by console or in a file
	 */
	public PrintStream getPs() {
		return ps;
	}

    /**
	 * Changes the stream to printing by console or in a file
	 */
	public void setPs(PrintStream ps) {
		this.ps = ps;
	}


}