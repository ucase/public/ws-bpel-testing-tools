package es.uca.webservices.gamera.term;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.term.GATerminationCondition;

/**
 * Termination condition which is true when the minimal test suite needs
 * a number of test cases.
 *
 * @author Pedro Delgado-Pérez
 * @version 1.0
 */
@Option(description="Termination condition which is true when obtained a number of " +
		"test cases in the minimal test suite.")
public class MinimalTestSuiteCondition implements GATerminationCondition {

    private int tests;
    private String minimalTestSuiteDirectory=null;
    
    public void setTests(int tests) {
        this.tests = tests;
    }

    public int getTests() {
        return tests;
    }
    
	public void setMinimalTestSuiteDirectory(String minimalTestSuiteDirectory){
		this.minimalTestSuiteDirectory=minimalTestSuiteDirectory;
	}
	
	public String getMinimalTestSuiteDirectory(){
		return minimalTestSuiteDirectory;
	}

    @Override
    public boolean evaluate(GAState state) {
    	String line = null;
    	
		try{
			ProcessBuilder pb = new ProcessBuilder();
			List<String> args = new ArrayList<String>();
			//Pasamos el script
			args.add("./minimalTestSuite.sh");
			//args.add(Integer.toString(getNumber()));
			pb.command(args).directory(new File(minimalTestSuiteDirectory));
			Process p = pb.start();
			p.waitFor();
			InputStreamReader tempReader = new InputStreamReader(new BufferedInputStream(p.getInputStream()));
			BufferedReader reader = new BufferedReader(tempReader);
		
			//En la línea que lea tiene que devolver el número de tests en el test suite
			line = reader.readLine();
			
			//NUEVO: Vamos a enviar los mutantes generados a un fichero externo; 
			PrintStream ps = System.out;
			ps.println(String.format("El número de tests en el conjunto adecuado y mínimo es: %s", line));
			
		}catch(Exception e){
			try {
				throw new AnalysisException(e);
			} catch (AnalysisException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return Integer.parseInt(line) == tests;
    }

    @Override
    public void validate() throws InvalidConfigurationException {
        if (getTests() <= 0) {
            throw new InvalidConfigurationException(
                    "The number of test cases must be greater than 0");
        }
    }

}
