package es.uca.webservices.gamera.record;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.uca.webservices.gamera.api.GAIndividual;

/**
 * Represents the results of computing mutant subsumption relationships within
 * a {@link SimulationRecord}. Live and duplicated mutants are recorded as well,
 * as they are removed prior to computing subsumed mutants.
 */
public class MutantSubsumptionRecord {

	public static class SubsumingMutant {
		private GAIndividual subsuming;
		private Map<List<Integer>, GAIndividual> subsumed = new HashMap<>();

		public GAIndividual getSubsuming() {
			return subsuming;
		}

		public void setSubsuming(GAIndividual subsuming) {
			this.subsuming = subsuming;
		}

		public Map<List<Integer>, GAIndividual> getSubsumed() {
			return subsumed;
		}

		public void setSubsumed(Map<List<Integer>, GAIndividual> subsumed) {
			this.subsumed = subsumed;
		}
	}

	private Set<GAIndividual> liveMutants = new HashSet<>();
	private Map<List<Integer>, Set<GAIndividual>> duplicateMutants = new HashMap<>();
	private Map<List<Integer>, SubsumingMutant> subsumingMutants = new HashMap<>();

	public Set<GAIndividual> getLiveMutants() {
		return liveMutants;
	}

	public void setLiveMutants(Set<GAIndividual> liveMutants) {
		this.liveMutants = liveMutants;
	}

	public Map<List<Integer>, Set<GAIndividual>> getDuplicateMutants() {
		return duplicateMutants;
	}

	public void setDuplicateMutants(Map<List<Integer>, Set<GAIndividual>> duplicateMutants) {
		this.duplicateMutants = duplicateMutants;
	}

	public Map<List<Integer>, SubsumingMutant> getSubsumingMutants() {
		return subsumingMutants;
	}

	public void setSubsumingMutants(Map<List<Integer>, SubsumingMutant> subsumingMutants) {
		this.subsumingMutants = subsumingMutants;
	}

}
