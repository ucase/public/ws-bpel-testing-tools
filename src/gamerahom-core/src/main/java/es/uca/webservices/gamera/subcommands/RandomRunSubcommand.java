package es.uca.webservices.gamera.subcommands;

import java.io.File;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import es.uca.webservices.gamera.RandomSelectionAlgorithm;

/**
 * Subcommand that performs random selection on the set of all mutants. Useful
 * as a contrast against EMT's genetic algorithm.
 * 
 * {@link RandomRunSubcommand} and {@link RandomSimulationRunSubcommand} share
 * most of their code. Since Java does not have multiple inheritance, we mix in
 * the options related to random selection through the {@link RandomSelectionAlgorithm}
 * class.
 * 
 * @author Antonio García-Domínguez
 */
public class RandomRunSubcommand extends ExecutionSubcommand {

	private RandomSelectionAlgorithm selector = new RandomSelectionAlgorithm();

	static final String DESCRIPTION =
		"Performs random selection using most of the components of a YAML\n" +
		"configuration file, instead of the genetic algorithm. It tries to\n" +
		"mimic the genetic algorithm as much as possible. The set of all\n" +
		"normalized individuals are shuffled and then are executed sequentially\n" +
		"in order in batches as large as populationSize, as if they were\n" +
		"generations of a hypothetic genetic algorithm.\n" +
		"\n" +
		"Execution stops when one of the termination conditions is met."
		;

	public RandomRunSubcommand() {
		super(1, 1, "randrun", "config.yaml", DESCRIPTION);
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		selector.parseOptions(options);
	}

	@Override
	protected OptionParser createOptionParser() {
		return selector.extendOptionParser(super.createOptionParser());
	}

	@Override
	protected void runCommand() throws Exception {
		final File yaml = new File(getNonOptionArgs().get(0));
		selector.setConfiguration(loadConfiguration(yaml));
		selector.run();
	}

}
