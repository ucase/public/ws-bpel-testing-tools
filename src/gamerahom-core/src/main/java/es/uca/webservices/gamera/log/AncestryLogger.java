package es.uca.webservices.gamera.log;

import java.util.List;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;

/**
 * Logger which prints who are the parents of the new individuals produced
 * by applying a genetic operator (mutation, crossover, ...).
 *
 * @author Emma Blanco-Muñoz
 * @version 1.0
 */

@Option(description="Logger which prints who are the parents of the new individuals produced" +
		" by applying a genetic operator (mutation, crossover, ...).")
public class AncestryLogger extends AbstractLogger {

	@Override
	public void appliedGeneticOperator(GAState state, String geneticOperatorName, 
			List<GAIndividual> parents, List<GAIndividual> children) {
		StringBuilder text = new StringBuilder("The genetic operator " + 
				geneticOperatorName.substring(geneticOperatorName.lastIndexOf(".")+1, geneticOperatorName.length()) + " has produced ");
		
		for (GAIndividual child : children) {
			text.append("{" + child.getOrder() + " ");
			printHOM(state.getAnalysisResults().getOperatorNames(), text, child.normalize(state.getAnalysisResults()));
			text.append("}, ");
		}
		text = new StringBuilder(text.substring(0, text.lastIndexOf(", "))).append(" from the parents ");
		
		for (GAIndividual parent : parents) {
			text.append("{" + parent.getOrder() + " ");
			printHOM(state.getAnalysisResults().getOperatorNames(), text, parent.normalize(state.getAnalysisResults()));
			text.append("}, ");
		}

		text = new StringBuilder(text.substring(0, text.lastIndexOf(", ")));
		getStream().printMsgByConsoleFile(text.toString(), state.getElapsedMillis(), isConsole(), getFile());
	}

}
