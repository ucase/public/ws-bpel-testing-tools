package es.uca.webservices.gamera.generate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Random;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.generate.GAIndividualGenerator;
import es.uca.webservices.gamera.api.annotations.Option;

/**
 * Randomly generates new individuals, using an uniform distribution in each
 * field. The number of individuals generated is a percentage of the number of
 * the individuals of the original population.
 *
 * @author Antonio García Domínguez, Juan José Domínguez-Jiménez, Emma
 *         Blanco-Muñoz
 * @version 1.1
 */
@Option(description="Randomly " +
		"generates new individuals, using an uniform distribution in each field. " +
		"The number of individuals generated is a percentage of the number of the individuals " +
		"of the original population.")
public class UniformGenerator implements GAIndividualGenerator {

    // Number of individuals to be generated, expressed as a proportion in the
    // range [0,1] of the size of the source population
    private double percent;
    protected Random prng;
    
    public void setPercent(double percent) {
        this.percent = percent;
    }

    public double getPercent() {
        return percent;
    }
    
    @Override
    public void generate(GAState state, int maxOrder, 
    		GAPopulation source, GAPopulation destination) {

        if (maxOrder < 1) {
            throw new IllegalArgumentException("The maximum order of the " +
            		"individual should be greater or equal than one");
        }

        GAIndividual a = createIndividual(state, maxOrder);
        destination.addIndividual(a);
    }

    @Override
    public void validate() throws InvalidConfigurationException {
        if (getPercent() < 0.0 || getPercent() > 1.0) {
            throw new InvalidConfigurationException(
                    "Percent of all mutants to be produced by the uniform "
                    + "generator must lie in [0, 1]");
        }
        if (prng == null) {
        	throw new InvalidConfigurationException("No PRNG has been set");
        }
    }
    
    protected GAIndividual createIndividual(GAState state, int maxOrder) {
        // Current order of the individual, [1, maxOrder]
        final int randOrder =  prng.nextInt(maxOrder) + 1;

        BigInteger[] randOperator = new BigInteger[maxOrder];
        BigInteger[] randLocation = new BigInteger[maxOrder];
        BigInteger[] randAttribute = new BigInteger[maxOrder];

        // Complete individual mutants with random numbers (starting with 1) which depend on its maximum values
        AnalysisResults analysis = state.getAnalysisResults();
        final BigDecimal operatorRange = new BigDecimal(analysis.getMaxOp()).subtract(BigDecimal.ONE);
        final BigDecimal locationRange = new BigDecimal(analysis.getMaxLoc()).subtract(BigDecimal.ONE);
        final BigDecimal attributeRange = new BigDecimal(analysis.getMaxAtt()).subtract(BigDecimal.ONE);
        for (int i = 0; i < maxOrder; i++) {
			randOperator[i] = operatorRange.multiply(new BigDecimal(prng.nextDouble())).toBigInteger().add(BigInteger.ONE);
            randLocation[i] = locationRange.multiply(new BigDecimal(prng.nextDouble())).toBigInteger().add(BigInteger.ONE);
            randAttribute[i] = attributeRange.multiply(new BigDecimal(prng.nextDouble())).toBigInteger().add(BigInteger.ONE);
        }

        return new GAIndividual(randOperator, randLocation, randAttribute, randOrder);
    }

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}
}