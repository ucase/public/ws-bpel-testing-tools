package es.uca.webservices.gamera.subcommands;

import java.io.File;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import es.uca.webservices.gamera.RandomSelectionAlgorithm;

/**
 * Subcommand that performs random selection on the set of all mutants
 * <em>and</em> replaces the {@link GAExecutor} on the fly with a
 * {@link SimulatedExecutor}. Useful as a contrast against EMT's genetic
 * algorithm.
 * 
 * {@link RandomRunSubcommand} and {@link RandomSimulationRunSubcommand} share
 * most of their code. Since Java does not have multiple inheritance, we mix in
 * the options related to random selection through the {@link RandomSelectionAlgorithm}
 * class.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
public class RandomSimulationRunSubcommand extends SimulationRunSubcommand {

	private RandomSelectionAlgorithm selector = new RandomSelectionAlgorithm();

	private static final String DESCRIPTION =
		RandomRunSubcommand.DESCRIPTION + "\n" +
		"\n" +
		"In addition, the executor in the YAML configuration is replaced by a\n" +
		"simulation executor that extracts its analysis and results from the\n" +
		"specified simulation record. The maximum order of the simulation record\n" +
		"must be greater than or equal than the maximum order of the YAML\n" +
		"configuration in order for it to work."
		;

	public RandomSimulationRunSubcommand() {
		super("randsimrun", DESCRIPTION);
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		selector.parseOptions(options);
	}

	@Override
	protected OptionParser createOptionParser() {
		return selector.extendOptionParser(super.createOptionParser());
	}

	@Override
	protected void runCommand() throws Exception {
		final File yaml = new File(getNonOptionArgs().get(0));
		selector.setConfiguration(loadConfiguration(yaml));
		selector.run();
	}

}
