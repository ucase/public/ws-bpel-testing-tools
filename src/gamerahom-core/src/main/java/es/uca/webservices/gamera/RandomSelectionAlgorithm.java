package es.uca.webservices.gamera;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.ValidIndividualsList;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.GAStoppableAfterFirstDiffExecutor;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.generate.GAIndividualGenerator;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.api.term.GATerminationCondition;
import es.uca.webservices.gamera.conf.Configuration;
import es.uca.webservices.gamera.conf.ConfigurationValidationMode;
import es.uca.webservices.gamera.fitness.ZeroFitnessFunction;
import es.uca.webservices.gamera.generate.KnowledgeBasedGenerator;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * <p>
 * Implements random selection on top of the component-based architecture used
 * by GAmera.
 * </p>
 * 
 * <p>
 * Our implementation of random selection is a generational algorithm that
 * starts by listing all possible individuals, and then shuffling the list.
 * Generations are produced by simply iterating over the shuffled list, until no
 * more individuals are left and the algorithm terminates. The algorithm may
 * terminate before that, depending on the registered
 * {@link GATerminationConditions}.
 * </p>
 * 
 * <p>
 * To emulate traditional mutant sampling, the population size should be set to
 * 1 and the executor should stop comparisons after the first difference.
 * </p>
 * 
 * <p>
 * Since the algorithm ignores fitness values, you may want to use the
 * {@link ZeroFitnessFunction} to save time when running this algorithm, unless
 * you're interested in the fitness values of the individuals.
 * </p>
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
public class RandomSelectionAlgorithm extends AbstractGenerationalAlgorithm {

	private static final class KnowledgeBasedIndividualComparator implements Comparator<GAIndividual> {
		private final AnalysisResults analysis;
		private final Configuration configuration;

		private KnowledgeBasedIndividualComparator(AnalysisResults analysis, Configuration configuration) {
			this.analysis = analysis;
			this.configuration = configuration;
		}

		@Override
		public int compare(GAIndividual o1, GAIndividual o2) {
			for (GAIndividualGenerator gen: configuration.getIndividualGenerators().keySet()) {
				if (gen instanceof KnowledgeBasedGenerator) {
					final KnowledgeBasedGenerator kbg = (KnowledgeBasedGenerator)gen;
					final double ratio1 = getOperatorStrongRatio(analysis, kbg, o1);
					final String op2 = analysis.getOperatorNames()[o2.getThOperator(1).intValue() - 1];
					final double ratio2 = kbg.getRatios().get(op2);

					// We want to sort from highest to lowest (hence the negation)
					return -Double.compare(ratio1, ratio2);
				}
			}

			return 0;
		}

		private double getOperatorStrongRatio(final AnalysisResults analysis, final KnowledgeBasedGenerator kbg, GAIndividual o1) {
			final String op1 = analysis.getOperatorNames()[o1.getThOperator(1).intValue() - 1];
			final double ratio1 = kbg.getRatios().get(op1);
			return ratio1;
		}
	}

	public static final String STOP_AT_FIRST_DIFF_OPTION = "stop-at-first-diff";
	private static final String SORT_OPERATORS_OPTION = "sort-by-op-ratios";

	private boolean stopAtFirstDiff = false;
	private boolean sortByOperators = false;

	protected List<GAIndividual> allIndividuals;
	private Iterator<GAIndividual> iterIndividual;

	public void parseOptions(OptionSet options) {
		setStopAtFirstDiff(options.has(STOP_AT_FIRST_DIFF_OPTION));
		setSortByOperators(options.has(SORT_OPERATORS_OPTION));
	}

	public OptionParser extendOptionParser(OptionParser parser) {
		parser.accepts(
			STOP_AT_FIRST_DIFF_OPTION,
			"Stops the test suite after the first test case with a different" +
			" output (traditional mutant sampling)." +
			" By default, all test cases are run.");
		parser.accepts(SORT_OPERATORS_OPTION,
			"Sorts the shuffled individuals by operator, from the ones that "
			+ "have the highest observed strong mutant ratios in other programs "
			+ "(as indicated in the KnowledgeBasedGenerator ratios), to the lowest.");
		return parser;
	}

	public boolean getStopAtFirstDiff() {
		return stopAtFirstDiff;
	}

	public void setStopAtFirstDiff(boolean stopAtFirstDiff) {
		this.stopAtFirstDiff = stopAtFirstDiff;
	}

	public boolean isSortByOperators() {
		return sortByOperators;
	}

	public void setSortByOperators(boolean sortByOperators) {
		this.sortByOperators = sortByOperators;
	}

	@Override
	protected void validate() throws InvalidConfigurationException {
		super.validate();
		getConfiguration().validate(ConfigurationValidationMode.RANDOM_SELECTION);

		if (stopAtFirstDiff) {
			final GAExecutor executor = getConfiguration().getExecutor();
			if (executor instanceof GAStoppableAfterFirstDiffExecutor) {
				((GAStoppableAfterFirstDiffExecutor)executor).setStopAtFirstDiff(true);
			}
			else {
				throw new InvalidConfigurationException("The selected executor " + executor + " cannot stop at the first difference");
			}
		}
	}

	@Override
	protected void nextGeneration(GAState state, GAPopulation currentPopulation, 
			GAPopulation prevPopulation, GALogger logger) throws ComparisonException, GenerationException {
		pickNextGeneration(state.getAnalysisResults(), getConfiguration(), currentPopulation);
	}

	@Override
	protected void generateFirstGeneration(GAState state, GAPopulation destination) {
		final Configuration configuration = getConfiguration();
		pickNextGeneration(state.getAnalysisResults(), configuration, destination);
	}

	@Override
	protected boolean checkTerminationConditions(List<GATerminationCondition> terminationConditions, GAState state) {
		return super.checkTerminationConditions(terminationConditions, state) || !iterIndividual.hasNext();
	}

	protected void pickNextGeneration(AnalysisResults analysis, final Configuration configuration, GAPopulation destination) {
		if (iterIndividual == null) {
			// Initialize the list with every individual
			allIndividuals = new ValidIndividualsList(analysis, configuration.getMaxOrder()).all();
			orderIndividuals(analysis, configuration);
			iterIndividual = allIndividuals.iterator();
		}

		for (int i = 0; i < configuration.getPopulationSize() && iterIndividual.hasNext(); ++i) {
			destination.addIndividual(iterIndividual.next());
		}
	}

	protected void orderIndividuals(final AnalysisResults analysis, final Configuration configuration) {
		Collections.shuffle(allIndividuals, configuration.getPRNG());
		if (sortByOperators) {
			Collections.sort(allIndividuals, new KnowledgeBasedIndividualComparator(analysis, configuration));
		}
	}
}
