package es.uca.webservices.gamera.record;

import java.beans.IntrospectionException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.yaml.snakeyaml.introspector.Property;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Represent;
import org.yaml.snakeyaml.representer.Representer;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.exec.SimulatedExecutor;

/**
 * Representer which controls the properties that should be dumped from the
 * simulation records.
 *
 * @author Antonio García-Domínguez
 */
public class SimulationRecordRepresenter extends Representer {

	private final String[] operatorNames;

	static final String GAIND_FITNESS = "fitness";
	static final String GAIND_ORDER = "order";
	static final String GAIND_ATTRIBUTE = "attribute";
	static final String GAIND_LOCATION = "location";
	static final String GAIND_OPERATOR = "operator";
	static final String GAIND_NORMALIZED = "normalized";
	static final String GAIND_OPERATOR_NAMES = "operatorNames";
	static final String GAIND_TAG = "!gaind";

	private class GAIndividualRepresenter implements Represent {
		@Override
		public Node representData(Object data) {
			final GAIndividual i = (GAIndividual)data;

			final Map<String, Object> values = new HashMap<String, Object>();
			values.put(GAIND_OPERATOR, i.getOperator().toArray());
			values.put(GAIND_LOCATION, i.getLocation().toArray());
			values.put(GAIND_ATTRIBUTE, i.getAttribute().toArray());
			values.put(GAIND_ORDER, i.getOrder());
			values.put(GAIND_FITNESS, i.getFitness());
			values.put(GAIND_NORMALIZED, i.isNormalized());

			if (operatorNames != null) {
				// In some cases, we may want to add operator names to the individuals
				// (e.g. when producing subsuming records)
				String[] individualOperatorNames = new String[i.getOperator().size()];
				for (int iOp = 0; iOp < i.getOperator().size(); iOp++) {
					individualOperatorNames[iOp] = operatorNames[i.getOperator().get(iOp).intValue() - 1];
				}
				values.put(GAIND_OPERATOR_NAMES, individualOperatorNames);
			}

			return representMapping(new Tag(GAIND_TAG), values, null);
		}
	}

	public SimulationRecordRepresenter() {
		this(null);
	}

	public SimulationRecordRepresenter(String[] operatorNames) {
		this.representers.put(GAIndividual.class, new GAIndividualRepresenter());
		this.operatorNames = operatorNames;
	}

	@Override
	protected Set<Property> getProperties(Class<? extends Object> type) throws IntrospectionException {
		final Set<Property> set = super.getProperties(type);
		if (type.equals(ComparisonSimulationRecord.class) || type.equals(AnalysisSimulationRecord.class)) {
			return filterByName(set, "totalWallMillis");
		}
		else if (type.equals(SimulationRecord.class)) {
			return filterByName(set, "cleanupWallMillis", "prepareWallMillis");
		}
		else if (type.equals(SimulatedExecutor.class)) {
			return filterByName(set, "record");
		}
		else {
			return set;
		}
	}

	private Set<Property> filterByName(final Set<Property> properties, String... vToRemove) {
		final Set<String> toRemove = new TreeSet<String>(Arrays.asList(vToRemove));
		final Set<Property> filtered = new TreeSet<Property>();
		for (Property p : properties) {
			if (!toRemove.contains(p.getName())) {
				filtered.add(p);
			}
		}
		return filtered;
	}

}
