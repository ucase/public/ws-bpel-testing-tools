package es.uca.webservices.gamera.genetic;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.api.util.Pair;

/**
 * Implements a crossover operator which exchanges the operator, location or
 * attribute of a random mutant of two individuals selected.
 * 
 * @author Antonio García-Domínguez, Juan José Domínguez-Jiménez, 
 *         Emma Blanco-Muñoz
 * @version 1.2
 */

@Option(description="Implements a crossover operator which exchanges the operator," +
		" location or attribute of a random mutant of two individuals selected.")
public class IndividualCrossoverOperator extends AbstractGeneticOperator {

	private static final int XOVER_OPERATOR = 1;
    private static final int XOVER_LOCATION = 2;
	private static final int XOVER_ATTRIBUTE = 3;
	
	@Override
	public void apply(GAState state, GAPopulation source, GAPopulation destination, GALogger logger, boolean mutateAttribute, boolean createTree) {
		// Select two different individuals with the selector defined in the configuration
		final GAIndividual dad = getSelector().select(source).denormalize(state.getAnalysisResults());
		GAIndividual mom = getSelector().select(source).denormalize(state.getAnalysisResults());

		// We choose the lowest individual order and generates a random number
		// between one and the minimum order of the parents.
		final int crossoverPoint = 1 + prng.nextInt(Math.min(dad.getOrder(), mom.getOrder()));

		// Now, the value to be modified has to be chosen randomly too [1, 3]
		final int mutatedField;
		if(mutateAttribute)
			mutatedField = XOVER_OPERATOR + prng.nextInt(XOVER_ATTRIBUTE - XOVER_OPERATOR + 1);
		else
			mutatedField = XOVER_OPERATOR + prng.nextInt(XOVER_LOCATION - XOVER_OPERATOR + 1);
		
		final Pair<GAIndividual, GAIndividual> children = doCrossover(dad, mom, crossoverPoint, mutatedField);
		
		logger.appliedGeneticOperator(state, IndividualCrossoverOperator.class.getName(),
				Arrays.asList(dad, mom), Arrays.asList(children.getLeft(), children.getRight()));
				
		destination.addIndividual(children.getLeft());
		destination.addIndividual(children.getRight());
		
        //NUEVO: asociar dos nuevos mutantes generados con sus dos ancestros
		List<GAIndividual> ancs = new ArrayList<GAIndividual>();
    	ancs.add(dad);
    	ancs.add(mom);
    	
    /*    GAIndividual normalizedChild = children.getLeft().normalize(state.getAnalysisResults());
        String childName = "m" + state.normalizeCode(normalizedChild.getOperator().get(0)) + "_" + normalizedChild.getLocation().get(0) + "_" + normalizedChild.getAttribute().get(0);
        
        GAIndividual normalizedChild2 = children.getRight().normalize(state.getAnalysisResults());
        String childName2 = "m" + state.normalizeCode(normalizedChild2.getOperator().get(0)) + "_" + normalizedChild2.getLocation().get(0) + "_" + normalizedChild2.getAttribute().get(0);
      */  
        if(createTree){
	        if(!state.isMutantAlreadyInTree(children.getLeft())){
	        	state.addMutantToTree(children.getLeft(), ancs);
	        }
	        
	        if(!state.isMutantAlreadyInTree(children.getRight())){
	        	state.addMutantToTree(children.getRight(), ancs);
	        }
        }
	}

	Pair<GAIndividual, GAIndividual> doCrossover(final GAIndividual dad, final GAIndividual mom, final int crossoverPoint, final int mutatedField) {
		
		if(crossoverPoint < 1 || crossoverPoint > dad.getLocation().size()
				|| crossoverPoint > Math.min(dad.getOrder(), mom.getOrder())) {
			throw new IllegalArgumentException(
					"The crossover point selected should be greater than zero, "
                     + "lower than the maximum order and be the lowest order" 
                     + " of their parents");
         }
		
		if(mutatedField < XOVER_OPERATOR || mutatedField > XOVER_ATTRIBUTE) {
			throw new IllegalArgumentException(
					"The mutated field should be 1 (operator), 2 (location) " +
					"or 3 (attribute)");
         }

		final BigInteger[] operatorsDad  = (BigInteger[])dad.getOperator().toArray();
		final BigInteger[] locationsDad  = (BigInteger[])dad.getLocation().toArray();
		final BigInteger[] attributesDad = (BigInteger[])dad.getAttribute().toArray();

		final BigInteger[] operatorsMom  = (BigInteger[])mom.getOperator().toArray();
		final BigInteger[] locationsMom  = (BigInteger[])mom.getLocation().toArray();
		final BigInteger[] attributesMom = (BigInteger[])mom.getAttribute().toArray();

		switch (mutatedField) {
			case XOVER_OPERATOR:
				swap(operatorsDad, operatorsMom, crossoverPoint - 1);
				break;
			case XOVER_LOCATION:
				swap(locationsDad, locationsMom, crossoverPoint - 1);
				break;
			case XOVER_ATTRIBUTE:
				swap(attributesDad, attributesMom, crossoverPoint - 1);
				break;
			default:
				break;
		}

		return new Pair<GAIndividual, GAIndividual>(
			new GAIndividual(operatorsDad, locationsDad, attributesDad, dad.getOrder()),
			new GAIndividual(operatorsMom, locationsMom, attributesMom, mom.getOrder())
		);
	}

	private void swap(final BigInteger[] left, final BigInteger[] right, final int pos) {
		final BigInteger tmp = left[pos];
		left[pos] = right[pos];
		right[pos] = tmp;
	}
}
