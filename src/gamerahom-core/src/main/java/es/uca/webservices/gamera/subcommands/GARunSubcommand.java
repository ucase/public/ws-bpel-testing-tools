package es.uca.webservices.gamera.subcommands;

import java.io.File;

import es.uca.webservices.gamera.AbstractGenerationalAlgorithm;
import es.uca.webservices.gamera.GeneticAlgorithm;

/**
 * Subcommand that runs the genetic algorithm using the specified YAML
 * configuration file.
 */
public class GARunSubcommand extends ExecutionSubcommand {

	private static final String DESCRIPTION =
		"Loads the specified .yaml file and runs the genetic algorithm using\n" +
		"the components with the specified configuration. In addition, it provides\n" +
		"some minor facilities for quickly customizing the configuration on the fly."
		;

	public GARunSubcommand() {
		super(1, 1, "run", "config.yaml", DESCRIPTION);
	}

	@Override
	protected void runCommand() throws Exception {
		final File yaml = new File(getNonOptionArgs().get(0));
		AbstractGenerationalAlgorithm generator = new GeneticAlgorithm();
		generator.setConfiguration(loadConfiguration(yaml));
		generator.run();
	}

}
