package es.uca.webservices.gamera.conf;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.uncommons.maths.random.MersenneTwisterRNG;

import es.uca.webservices.gamera.api.GAFitnessFunction;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionType;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.generate.GAIndividualGenerator;
import es.uca.webservices.gamera.api.generate.GAIndividualGeneratorOptions;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperator;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.api.select.GASelectionOperator;
import es.uca.webservices.gamera.api.select.GASelectionOperatorOptions;
import es.uca.webservices.gamera.api.term.GATerminationCondition;
import es.uca.webservices.gamera.fitness.MutantStrengthFitnessFunction;
import es.uca.webservices.gamera.log.AggregateLogger;

/**
 * JavaBean which validates and stores the GAmera configuration for the genetic
 * algorithm or the random selection algorithm.
 *
 * To load it, please use {@link ConfigurationLoader#parse}. Before using an
 * instance, it should be validated through
 * {@link #validate(ConfigurationValidationMode)}.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.1
 */
public class Configuration {
	
	// Maximum order for every individual
    private int maxOrder = 1; 
    private Integer populationSize;
    private Double populationSizeAsPercentOfTotalMutants;
    private Random prng = null;
    private GAExecutor executor;
    private String includedOperators, excludedOperators;
    private GAFitnessFunction fitnessFunction = new MutantStrengthFitnessFunction();
    private boolean mutateAttribute = true;

    private boolean tceEquivalent = false;
    private File tcefEquiv = null;
    private boolean tceDuplicate = false;
    private File tcefDupl = null;
    
    private boolean createTree = false;	//Nuevo.
    private File treeResults = null;
    
    private Map<GAGeneticOperator, GAGeneticOperatorOptions>
        geneticOperators = new LinkedHashMap<GAGeneticOperator, GAGeneticOperatorOptions>();
    private Map<GAIndividualGenerator, GAIndividualGeneratorOptions>
        individualGenerators = new LinkedHashMap<GAIndividualGenerator, GAIndividualGeneratorOptions>();
    private Map<GASelectionOperator, GASelectionOperatorOptions>
        selectionOperators = new LinkedHashMap<GASelectionOperator, GASelectionOperatorOptions>();

    private List<GATerminationCondition> terminationConditions = new ArrayList<GATerminationCondition>();
    private List<GALogger>               loggers               = new ArrayList<GALogger>();

    public void setMaxOrder(int maxOrder) {
        this.maxOrder = maxOrder;
    }

    public int getMaxOrder() {
        return maxOrder;
    }

    public void setPopulationSize(Integer populationSize) {
        this.populationSize = populationSize;
    }

    public Integer getPopulationSize() {
        return populationSize;
    }

    public Double getPopulationSizeAsPercentOfTotalMutants() {
		return populationSizeAsPercentOfTotalMutants;
	}

	public void setPopulationSizeAsPercentOfTotalMutants(Double percent) {
		this.populationSizeAsPercentOfTotalMutants = percent;
	}

	public void setExecutor(GAExecutor executor) {
        this.executor = executor;
    }

    public GAExecutor getExecutor() {
        return executor;
    }

    public Random getPRNG() {
    	if (prng == null) {
    		prng = new MersenneTwisterRNG();
    	}
		return prng;
    }

    public void setSeed(String seed) {
    	setSeedBytes(seed.getBytes());
    }
    
	public void setSeedBytes(byte[] seed) {
		prng = new MersenneTwisterRNG(seed);
	}

	public boolean isMutateAttribute() {
		return mutateAttribute;
	}

	public void setMutateAttribute(boolean mutateAttribute) {
		this.mutateAttribute = mutateAttribute;
	}
	
	public boolean isCreateTree() {
		return createTree;
	}

	public void setCreateTree(boolean createTree) {
		this.createTree = createTree;
	}

	
	public boolean isTceEquivalent() {
		return tceEquivalent;
	}

	public void setTceEquivalent(boolean tceEquivalent) {
		this.tceEquivalent = tceEquivalent;
	}
	
	@Option(type=OptionType.PATH_LOAD, fileExtensions={"txt"})
	public void setTcefEquiv(String tcefEquiv) {
		this.tcefEquiv = tcefEquiv != null ? new File(tcefEquiv) : null;
	}
	
	public File getTcefEquiv() {
		return tcefEquiv;
	}

	public void setTcefEquiv(File f) {
		this.tcefEquiv = f;
	}
	
	public boolean isTceDuplicate() {
		return tceDuplicate;
	}

	public void setTceDuplicate(boolean tceDuplicate) {
		this.tceDuplicate = tceDuplicate;
	}
	
	@Option(type=OptionType.PATH_LOAD, fileExtensions={"txt"})
	public void setTcefDupl(String tcefDupl) {
		this.tcefDupl = tcefDupl != null ? new File(tcefDupl) : null;
	}
	
	public File getTcefDupl() {
		return tcefDupl;
	}

	public void setTcefDupl(File f) {
		this.tcefDupl = f;
	}
	
	@Option(type=OptionType.PATH_LOAD, fileExtensions={"txt"})
	public void setTreeResults(String treeResults) {
		this.treeResults = treeResults != null ? new File(treeResults) : null;
	}

	public File getTreeResults() {
		return treeResults;
	}
	
	public void setTreeResults(File f) {
		this.treeResults = f;
	}
	
    public void setGeneticOperators(Map<GAGeneticOperator, GAGeneticOperatorOptions> geneticOperators) {
        this.geneticOperators = geneticOperators;
    }

    public Map<GAGeneticOperator, GAGeneticOperatorOptions> getGeneticOperators() {
        return geneticOperators;
    }

    public void setIndividualGenerators(Map<GAIndividualGenerator, GAIndividualGeneratorOptions> individualGenerators) {
        this.individualGenerators = individualGenerators;
    }

    public Map<GAIndividualGenerator, GAIndividualGeneratorOptions> getIndividualGenerators() {
        return individualGenerators;
    }

    public void setSelectionOperators(
            Map<GASelectionOperator, GASelectionOperatorOptions> selectionOperators) {
        this.selectionOperators = selectionOperators;
    }

    public Map<GASelectionOperator, GASelectionOperatorOptions> getSelectionOperators() {
        return selectionOperators;
    }

    public void setTerminationConditions(
            List<GATerminationCondition> terminationConditions) {
        this.terminationConditions = terminationConditions;
    }

    public List<GATerminationCondition> getTerminationConditions() {
        return terminationConditions;
    }

    public void addTerminationCondition(GATerminationCondition tc) {
    	if (terminationConditions == null) {
    		terminationConditions = new ArrayList<>();
    	}
    	terminationConditions.add(tc);
    }

    public void setLoggers(List<GALogger> loggers) {
        this.loggers = loggers;
    }

    public List<GALogger> getLoggers() {
        return loggers;
    }

    /**
     * Returns a {@link GALogger} which aggregates all the loggers in
     * {@link #getLoggers()}.
     */
    public GALogger getAggregateLogger() {
        return new AggregateLogger(loggers);
    }

    /**
	 * <p>
	 * Returns the ranges of operators that will be used by the algorithm. If
	 * <code>null</code> (the default), all operators will be available.
	 * </p>
	 *
	 * <p>
	 * The accepted format for this option is a string with a comma-separated
	 * list of positions and ranges, ignoring trailing and leading whitespace
	 * around commas, positions and ranges. A position is a 1-based index into
	 * the result of AnalysisResults#getOperatorNames(), or the name of
	 * the operator itself, which will be resolved as its position in the array.
     * Ranges are a pair of positions, in which the second one must be greater
     * than or equal to the first one.
	 * </p>
	 *
	 * <p>
	 * At least one range should be provided for this option. In summary:
	 * </p>
	 *
	 * <ul>
	 * <li><code>null</code> is valid: all operators will be included (the
	 * default).</li>
	 * <li><code>""</code> is not valid, as it does not mention any range.</li>
	 * <li><code>"2-1"</code> is not valid, as the <code>[2, 1]</code> range is
	 * empty.</li>
	 * <li><code>"1-5"</code> is valid: the first 5 operators will be included.</li>
	 * <li><code>"a-c,d"</code> will include the operators between "a" and "c",
	 * and the operator "d".</li>
	 * </ul>
	 *
	 * @see #getExcludedOperators()
	 * @see #setIncludedOperators(String)
	 */
    @Option(operators=true)
    public String getIncludedOperators() {
		return includedOperators;
	}

    /**
     * Changes the operator ranges that will be used by the algorithm.
     * @see #getIncludedOperators()
     */
    @Option(operators=true)
	public void setIncludedOperators(String includedOperators) {
		this.includedOperators = includedOperators;
	}

	/**
	 * Returns the operator ranges that will not be used by the algorithm. These
	 * ranges will be removed from those specified in
	 * {@link #getIncludedOperators()}.
	 *
	 * Most of the details about the valid values for this option are the same
	 * as in {@link #getIncludedOperators()}. However, in this case the default
	 * value (also <code>null</code>) means that <emph>no</emph> operators will
	 * be excluded. Therefore, when using the default values for both
	 * {@link #getIncludedOperators()} and {@link #getExcludedOperators()}, all
	 * operators will be used.
	 *
	 * In addition, it is valid to specify an empty string here, meaning that no
	 * operators will be removed from those listed by
	 * {@link #getIncludedOperators()}.
	 *
	 * @see #getIncludedOperators()
	 * @see #setExcludedOperators(String)
	 */
    @Option(operators=true)
	public String getExcludedOperators() {
		return excludedOperators;
	}

	/**
	 * Changes the operator ranges that will be excluded from those returned by
	 * {@link #getIncludedOperators()}.
	 */
    @Option(operators=true)
	public void setExcludedOperators(String excludedOperators) {
		this.excludedOperators = excludedOperators;
	}

	/**
	 * Returns the fitness function that will be used to evaluate the fitness of every
	 * individual in the generational algorithm.
	 */
	public GAFitnessFunction getFitnessFunction() {
		return fitnessFunction;
	}

	/**
	 * Changes the fitness function that will be used to evaluate the fitness of
	 * every individual in the generational algorithm.
	 */
	public void setFitnessFunction(GAFitnessFunction fitnessFunction) {
		this.fitnessFunction = fitnessFunction;
	}

	/**
	 * Validates that the contents of this configuration are correct and ensures
	 * every randomized component uses the same PRNG.
	 * 
	 * @param mode
	 *            Validation mode: {@link ConfigurationValidationMode#FULL} for validating
	 *            a configuration for the full GA, or any other value for validating the
	 *            common subset used by all generational algorithms. 
	 * 
	 * @throws InvalidConfigurationException
	 *             The configuration is not valid.
	 */
    public void validate(ConfigurationValidationMode mode) throws InvalidConfigurationException {
        basicValidation();

        if (mode == ConfigurationValidationMode.FULL) {
        	// Only the full GA requires this: random selection does not
        	// require genetic operators, individual generators and selection
        	// operators
        	if (geneticOperators == null || geneticOperators.isEmpty()) {
        		throw new InvalidConfigurationException(
                    "No genetic operators have been defined.");
        	}
        	else if (individualGenerators == null || individualGenerators.isEmpty()) {
        		throw new InvalidConfigurationException(
                    "No individual generators have been defined.");
        	}
        	else if (selectionOperators == null) {
            	// It is OK for there not to be any selection operators - GAmera
            	// did that for the IST study.
                throw new InvalidConfigurationException(
                        "The list of selection operators must not be null.");
            }
        }

        if (terminationConditions == null || terminationConditions.isEmpty()) {
            throw new InvalidConfigurationException(
                    "No termination operators have been defined.");
        }
        if (includedOperators != null && includedOperators.trim().equals("")) {
        	throw new InvalidConfigurationException(
        			"If includedOperators is used, it should included at least one position or range");
        }
        executor.validate();
        deepValidation(mode);
    }

    private void basicValidation() throws InvalidConfigurationException {
        if (getMaxOrder() <= 0) {
            throw new InvalidConfigurationException(
                    "Order must be strictly greater than zero.");
        }
        else if (getPopulationSize() != null && getPopulationSize() <= 0) {
        	throw new InvalidConfigurationException(
                   "Population size must be strictly greater than zero.");
        }
        else if (getPopulationSizeAsPercentOfTotalMutants() != null && (getPopulationSizeAsPercentOfTotalMutants() <= 0 || getPopulationSizeAsPercentOfTotalMutants() > 1)) {
        	throw new InvalidConfigurationException(
        			"Population size as percentage of the total number of mutants must be within the (0, 1] range.");
        }
        else if (getPopulationSize() == null && getPopulationSizeAsPercentOfTotalMutants() == null) {
        	throw new InvalidConfigurationException(
        			"Either populationSize or populationSizeAsPercentOfTotalMutants must be used");
        }
        else if (getPopulationSize() != null && getPopulationSizeAsPercentOfTotalMutants() != null) {
        	throw new InvalidConfigurationException(
        			"Only one of populationSize or populationSizeAsPercentOfTotalMutants should be used");
        }
        else if (executor == null) {
            throw new InvalidConfigurationException(
                    "No executor has been defined.");
        }
        else if (loggers == null || loggers.isEmpty()) {
            throw new InvalidConfigurationException(
                    "No loggers have been defined.");
        }
        else if (fitnessFunction == null) {
        	throw new InvalidConfigurationException("No fitness function has been defined.");
        }
    }

    private void deepValidation(ConfigurationValidationMode mode) throws InvalidConfigurationException {
    	if (mode == ConfigurationValidationMode.FULL) {
			for (Map.Entry<GAGeneticOperator, GAGeneticOperatorOptions> op : geneticOperators.entrySet()) {
				op.getKey().setPRNG(prng);
				op.getKey().validate();
				op.getValue().validate();
			}
			for (Map.Entry<GAIndividualGenerator, GAIndividualGeneratorOptions> gen : individualGenerators.entrySet()) {
				gen.getKey().setPRNG(prng);
				gen.getKey().validate();
				gen.getValue().validate();
			}
			for (Map.Entry<GASelectionOperator, GASelectionOperatorOptions> op : selectionOperators.entrySet()) {
				op.getKey().setPRNG(prng);
				op.getKey().validate();
				op.getValue().validate();
			}
    	}
        for (GATerminationCondition cond : terminationConditions) {
            cond.validate();
        }
        for (GALogger logger : loggers) {
            logger.validate();
        }
    }

}
