package es.uca.webservices.gamera.subcommands;

import java.io.File;

import es.uca.webservices.gamera.conf.Configuration;
import es.uca.webservices.gamera.record.SimulationRecord;

/**
 * Subcommand which generates a simulation record using the GAExecutor set
 * in a certain YAML configuration file.
 *
 * @author Antonio García-Domínguez
 */
public class SimulationRecordSubcommand extends AbstractSubcommand {

	private static final String DESCRIPTION =
		"This command creates a simulation record in YAML format and dumps it\n" +
		"to the standard output stream. The simulation record can be used in\n" +
		"place of the original program and all its FOMs, second-order HOMs,\n" +
		"..., up to its n-order HOMs, where 'n' is the maxOrder set in the\n" +
		"provided YAML file." +
		"\n" +
		"The simulation record lists all the comparison results and test times,\n" +
		"in addition to the preparation, cleanup and analysis times. It can be\n" +
		"used to drive the genetic algorithm with 'simrun' and to drive mutant\n" +
		"sampling with 'randsimrun'."
		;

	public SimulationRecordSubcommand() {
		super(2, 2, "simrecord", "config.yaml simrecord.yaml", DESCRIPTION);
	}

	@Override
	protected void runCommand() throws Exception {
		final File configYAML = new File(getNonOptionArgs().get(0));
		final File recordDestination = new File(getNonOptionArgs().get(1));

		final Configuration conf = loadConfiguration(configYAML);
		final SimulationRecord rec = SimulationRecord.buildFrom(conf.getMaxOrder(), conf.getExecutor());
		rec.saveTo(recordDestination);
	}

}
