package es.uca.webservices.gamera.subcommands;

import java.io.File;

import es.uca.webservices.gamera.AbstractGenerationalAlgorithm;
import es.uca.webservices.gamera.GeneticAlgorithm;
import es.uca.webservices.gamera.RandomSelectionAlgorithm;
import es.uca.webservices.gamera.ThresholdBasedHybridAlgorithm;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * Subclass of {@link SimulationRunSubcommand} that runs the genetic algorithm
 * against the loaded configuration.
 *
 * @author Antonio García-Domínguez
 */
public class MixRunSimulationRunSubcommand extends SimulationRunSubcommand {

	private static final String DESCRIPTION = "Runs a hybrid approach, starting with the GA and switching to random after\n"
			+ "a certain ratio of all mutants has been executed.\n" + "\n" + "It uses the provided simulation record\n"
			+ "to drive the algorithm. This can be useful when studying the effectiveness\n"
			+ "of EMT or selecting appropriate configuration values.\n" + "\n"
			+ "The command can also do some limited changes in the configuration file on the\n"
			+ "fly. Please check the options listed below.";

	private static final String THRESHOLD_OPTION = "threshold";
	private static final double THRESHOLD_DEFAULT = 0.5d;
	private double threshold = THRESHOLD_DEFAULT;

	private static final String START_RANDOM_OPTION = "random-start";
	private boolean startRandom = false;

	public MixRunSimulationRunSubcommand() {
		super("simmixrun", DESCRIPTION);
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		if (options.has(THRESHOLD_OPTION)) {
			threshold = (Double) options.valueOf(THRESHOLD_OPTION);
		}
		startRandom = options.has(START_RANDOM_OPTION);
	}

	@Override
	protected OptionParser createOptionParser() {
		OptionParser parser = super.createOptionParser();
		parser.accepts(THRESHOLD_OPTION, "0-1 ratio after which execution should switch to random selection")
				.withRequiredArg().describedAs("R").ofType(Double.class).defaultsTo(THRESHOLD_DEFAULT);
		parser.accepts(START_RANDOM_OPTION, "Start with a random search and switch to GA after threshold. "
				+ "The default is the other way around: start with GA and give up after the threshold, "
				+ "going into a random search.");
		return parser;
	}

	public double getThreshold() {
		return threshold;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	public boolean isStartRandom() {
		return startRandom;
	}

	public void setStartRandom(boolean startRandom) {
		this.startRandom = startRandom;
	}

	@Override
	protected void runCommand() throws Exception {
		final File yaml = new File(getNonOptionArgs().get(0));
		AbstractGenerationalAlgorithm mixgenerator;
		if (startRandom) {
			mixgenerator = new ThresholdBasedHybridAlgorithm(
					new RandomSelectionAlgorithm(), new GeneticAlgorithm(),
					threshold);
		} else {
			mixgenerator = new ThresholdBasedHybridAlgorithm(
					new GeneticAlgorithm(), new RandomSelectionAlgorithm(),
					threshold);
		}
		mixgenerator.setConfiguration(loadConfiguration(yaml));
		mixgenerator.run();
	}

}
