package es.uca.webservices.gamera;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.generate.GAIndividualGenerator;
import es.uca.webservices.gamera.api.generate.GAIndividualGeneratorOptions;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperator;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.api.select.GASelectionOperator;
import es.uca.webservices.gamera.api.select.GASelectionOperatorOptions;
import es.uca.webservices.gamera.conf.Configuration;
import es.uca.webservices.gamera.conf.ConfigurationValidationMode;

/**
 * Genetic algorithm for generating a subset of the high-quality mutants of a
 * program. The algorithm is decoupled from the specific components used: these
 * are picked up by the {@link Configuration} instance and hidden behind several
 * interfaces.
 *
 * @see es.uca.webservices.gamera.conf.Configuration
 * @see es.uca.webservices.gamera.api.generate.GAIndividualGenerator
 * @see es.uca.webservices.gamera.api.genetic.GAGeneticOperator
 * @see es.uca.webservices.gamera.api.log.GALogger
 * @see es.uca.webservices.gamera.api.select.GASelectionOperator
 * @see es.uca.webservices.gamera.api.term.GATerminationCondition
 * @author Antonio García-Domínguez, Juan José Domínguez-Jiménez, Emma Blanco-Muñoz
 * @version 1.1
 */
public class GeneticAlgorithm extends AbstractGenerationalAlgorithm {

	@Override
	protected void nextGeneration(GAState state, GAPopulation currentPopulation, 
			GAPopulation prevPopulation, GALogger logger) throws ComparisonException, GenerationException
	{
		final Configuration configuration = getConfiguration();
		final Map<GAIndividualGenerator, GAIndividualGeneratorOptions> individualGenerators = configuration.getIndividualGenerators();
		final Map<GASelectionOperator, GASelectionOperatorOptions> selectionOperators = configuration.getSelectionOperators();
		final Map<GAGeneticOperator, GAGeneticOperatorOptions> geneticOperators = configuration.getGeneticOperators();
		final int maxPopulation = configuration.getPopulationSize();
		final int maxOrder = configuration.getMaxOrder();
		final boolean mutateAttribute = configuration.isMutateAttribute();
		final boolean createTree = configuration.isCreateTree();

		// --- SELECTION ---
		selectFromPreviousPopulation(maxPopulation, currentPopulation, prevPopulation, selectionOperators);

		// --- RANDOM GENERATION ---
		generateNewIndividuals(maxPopulation, currentPopulation, prevPopulation, state,	maxOrder, individualGenerators);

		// --- MUTATION & CROSSOVER ---
		applyGeneticOperators(maxPopulation, currentPopulation, prevPopulation, state, logger, geneticOperators, mutateAttribute, createTree);
	}

	@Override
	protected void generateFirstGeneration(GAState state, GAPopulation destination) {
		final Configuration configuration = getConfiguration();
		final Map<GAIndividualGenerator, GAIndividualGeneratorOptions> individualGenerators = configuration.getIndividualGenerators();
		final int maxOrder = configuration.getMaxOrder();
		final int populationSize = configuration.getPopulationSize();

		// Sonar advises not to modify a parameter
		int individualsToGenerate = populationSize;
		final Set<GAIndividualGenerator> generators = individualGenerators.keySet();
		Iterator<GAIndividualGenerator> it = generators.iterator();
		
		// While the destination population isn't complete, every individual
		//generator are called to generate an individual and so on
		while (individualsToGenerate > 0) {
			if(it.hasNext()) {
				it.next().generate(state, maxOrder, null, destination);				
				individualsToGenerate--;
			} else { 
				// If the number of the individuals to generate is not
				// zero yet, we repeat the generation with the first generator
				it = generators.iterator();
			}
		}
	}
	
	/**
	 * It selects individuals from the previous population by applying the
	 * selection operators
	 * 
	 * @param maxPopulation
	 * 		       Maximum individuals per population
	 * @param currentPopulation
	 * 		       New population where new individuals are going to be added
	 * @param prevPopulation
	 * 			   Previous population which can be useful to generate new
	 * 			   individuals
	 * @param selectionOperators
	 * 			   Operators of selection
	 * 
	 */	
	private void selectFromPreviousPopulation(int maxPopulation, 
			GAPopulation currentPopulation, 
			GAPopulation prevPopulation, 
			Map<GASelectionOperator, 
			GASelectionOperatorOptions> selectionOperators)
	{
		for(Entry<GASelectionOperator, GASelectionOperatorOptions>	selectionPair : selectionOperators.entrySet()) {
			// We obtain the selection operator and the percent about
			// the number of individuals to generate with this operator
			final GASelectionOperator op = selectionPair.getKey();
			final double percent = selectionPair.getValue().getPercent();
				
			// Calculates how many individuals we have to select
			int numIndividuals = (int) Math.round(percent * maxPopulation);
				
			while(numIndividuals > 0) {
				final GAIndividual i = op.select(prevPopulation);
				currentPopulation.addIndividual(i);
				numIndividuals--;
			}
		}
	}
	
	/**
	 * It generate new individuals to the destination population by applying the
	 * individual generators
	 * 
	 * @param maxPopulation
	 * 		       Maximum individuals per population
	 * @param currentPopulation
	 * 		       New population where new individuals are going to be added
	 * @param prevPopulation
	 * 			   Previous population which can be useful to generate new
	 * 			   individuals
	 * @param state
	 * 			   Results produced after analyzing the original program.
	 * @param maxOrder
	 *             Maximum order defined in the configuration
	 * @param individualGenerators
	 * 			   Generators of individuals
	 * 
	 */
	private void generateNewIndividuals(int maxPopulation, 
			GAPopulation currentPopulation, 
			GAPopulation prevPopulation,
			GAState state, int maxOrder, 
			Map<GAIndividualGenerator, GAIndividualGeneratorOptions> individualGenerators)
	{
		for (Entry<GAIndividualGenerator, GAIndividualGeneratorOptions> generatorPair : individualGenerators.entrySet()) {
			// We obtain the individual generator and the percent about
			// the number of individuals to generate with this operator
			final GAIndividualGenerator op = generatorPair.getKey();
			final double percent = generatorPair.getValue().getPercent();

			// Calculates how many individuals we have to generate
			int numIndividuals = (int) Math.round(percent * maxPopulation);
				
			while(numIndividuals > 0) {
				op.generate(state, maxOrder, prevPopulation, currentPopulation);
				numIndividuals--;
			}
		}
	}

	/**
	 * It generates new individuals by applying the genetic operators
	 * 
	 * @param maxPopulation
	 * 		       Maximum individuals per population
	 * @param currentPopulation
	 * 		       New population where new individuals are going to be added
	 * @param prevPopulation
	 * 			   Previous population which can be useful to generate new
	 * 			   individuals
	 * @param state
	 * 			   Overall state of the algorithm
	 * @param analysis
	 * 			   Results produced after analyzing the original program.
	 * @param executor
	 *            It generates and runs mutants
	 * @param geneticOperators
	 * 			   Genetic operators like mutation or crossover
	 * @param mutateAttribute
	 * 			   Whether the attribute field is considered or not when applying genetic operators
	 * @param createTree
	 * 				Whether the tree of ancestors is generated or not
	 * @throws GenerationException
	 *             There was a problem while generating individuals.
	 * @throws ComparisonException
	 *             There was a problem while comparing the output of the original
	 *             program and the mutant again the test suite.
	 *             
	 */	
	private void applyGeneticOperators(int maxPopulation, 
			GAPopulation currentPopulation, 
			GAPopulation prevPopulation,
			GAState state,
			GALogger logger,
			Map<GAGeneticOperator, GAGeneticOperatorOptions> geneticOperators, 
			boolean mutateAttribute,
			boolean createTree)
			throws ComparisonException, GenerationException
	{
		
		// This loop will be produced while there's room in the population
		while (currentPopulation.getPopulationSize() < maxPopulation) {
			// With a random number, we'll apply a genetic operator depending on its probability
			double rand = getConfiguration().getPRNG().nextDouble();
			
			// Apply every genetic operator
			for(Entry<GAGeneticOperator, GAGeneticOperatorOptions> geneticPair : geneticOperators.entrySet()) {
				// We obtain the genetic operator and the percent about
				// the number of individuals to generate with this operator
				final GAGeneticOperator op = geneticPair.getKey();
				final double probability = geneticPair.getValue().getProbability();
					
				// If the condition is met, we apply the operator
				// Otherwise, we'll check the other operators
				if(rand <= probability) {
					final GAPopulation aux = new GAPopulation();
					op.apply(state, prevPopulation, aux, logger, mutateAttribute, createTree);
					for (Iterator<GAIndividual> iAux = aux.getIndividuals().iterator(); iAux.hasNext() &&
							currentPopulation.getPopulationSize() < maxPopulation; ) {
						currentPopulation.addIndividual(iAux.next());
					}
					break;
				} else {
					rand -= probability;
				}
			}
		}
	}

	@Override
	protected void validate() throws InvalidConfigurationException {
		super.validate();
		getConfiguration().validate(ConfigurationValidationMode.FULL);
	}
}
