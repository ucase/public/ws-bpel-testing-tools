package es.uca.webservices.gamera.subcommands;

import java.io.File;

import es.uca.webservices.gamera.AbstractGenerationalAlgorithm;
import es.uca.webservices.gamera.GeneticAlgorithm;

/**
 * Subclass of {@link SimulationRunSubcommand} that runs the genetic algorithm
 * against the loaded configuration.
 *
 * @author Antonio García-Domínguez
 */
public class GASimulationRunSubcommand extends SimulationRunSubcommand {

	private static final String DESCRIPTION =
		"Runs the genetic algorithm against the specified configuration. However,\n" +
		"instead of the specified GAExecutor, it uses the provided simulation record\n" +
		"to drive the algorithm. This can be useful when studying the effectiveness\n" +
		"of EMT or selecting appropriate configuration values.\n" +
		"\n" +
		"The command can also do some limited changes in the configuration file on the\n" +
		"fly. Please check the options listed below."
		;

	public GASimulationRunSubcommand() {
		super("simrun", DESCRIPTION);
	}

	@Override
	protected void runCommand() throws Exception {
		final File yaml = new File(getNonOptionArgs().get(0));
		AbstractGenerationalAlgorithm generator = new GeneticAlgorithm();
		generator.setConfiguration(loadConfiguration(yaml));
		generator.run();
	}

}
