package es.uca.webservices.gamera.exec;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionType;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAProgressMonitor;
import es.uca.webservices.gamera.api.exec.GAStoppableAfterFirstDiffExecutor;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.api.predicates.IIntegerPredicate;
import es.uca.webservices.gamera.api.util.ranges.StringArrayRangeListFilter;
import es.uca.webservices.gamera.record.AnalysisSimulationRecord;
import es.uca.webservices.gamera.record.ComparisonSimulationRecord;
import es.uca.webservices.gamera.record.SimulationRecord;
import java.util.Objects;

public class SimulatedExecutor implements GAStoppableAfterFirstDiffExecutor {

	private File fSimRecord;
	private SimulationRecord simRecord;
	private int nTestCases = 0;
	private boolean stopAtFirstDiff = false;
	private String includedTests, excludedTests;

	public SimulatedExecutor() {
		// Empty constructor for SnakeYAML
	}

	public SimulatedExecutor(SimulationRecord record) {
		setRecord(record);
	}

	public SimulatedExecutor(File recordSource) throws Exception {
		setFile(recordSource);
	}

	public File getFile() {
		return fSimRecord;
	}

    @Option(type=OptionType.PATH_LOAD, fileExtensions={"yaml", "raw"})
    public void setFile(String recordSource) throws Exception {
        setFile(new File(recordSource));
    }

	public void setFile(File recordSource) throws Exception {
		fSimRecord = recordSource;
		if (recordSource != null) {
			setRecord(SimulationRecord.loadFrom(recordSource));
		} else {
			setRecord(null);
		}
	}

	public SimulationRecord getRecord() {
		return simRecord;
	}

	public void setRecord(SimulationRecord r) {
		this.simRecord = r;
	}

	public String getIncludedTests() {
		return includedTests;
	}

	public void setIncludedTests(String includedTests) {
		this.includedTests = includedTests;
	}

	public String getExcludedTests() {
		return excludedTests;
	}

	public void setExcludedTests(String excludedTests) {
		this.excludedTests = excludedTests;
	}

	@Override
	public void prepare() throws PreparationException {
		if (includedTests != null || excludedTests != null) {
			List<String> values = simRecord.getTestCaseNames();
			if (values == null) {
				nTestCases = simRecord.getTestCaseCount();
				values = new ArrayList<String>(nTestCases);
				for (int i = 1; i <= nTestCases; ++i) {
					values.add("test" + i);
				}
			}
			final IIntegerPredicate filter = new StringArrayRangeListFilter(values, includedTests, excludedTests);
			simRecord.filterTests(filter);
		}
		nTestCases = simRecord.getTestCaseCount();
	}

	@Override
	public void cleanup() throws PreparationException {
		// nothing to do!
	}

	@Override
	public AnalysisResults analyze(GAProgressMonitor monitor) throws AnalysisException {
		final AnalysisSimulationRecord aR = simRecord.getAnalysisRecord();
		return new AnalysisResults(
			aR.getOperatorNames(),
			aR.getLocationCounts(),
			aR.getFieldRanges()
		);
	}

	@Override
	public ComparisonResults[] compare(GAProgressMonitor monitor, GAIndividual... individuals) throws ComparisonException {
		final ComparisonResults[] results = new ComparisonResults[individuals.length];

		for (int i = 0; i < results.length; ++i) {
			final GAIndividual ind = individuals[i];
			final ComparisonSimulationRecord comparisonRecord = simRecord.getComparisonRecords().get(ind);
			if (comparisonRecord == null) {
				// The individual is not listed in the simulation record - what to do?
				if (simRecord.getMissingIndividualsAreInvalid()) {
					// We should report it as invalid
					final ComparisonResult[] row = new ComparisonResult[nTestCases];
					for (int j = 0; j < row.length; ++j) {
						row[j] = ComparisonResult.INVALID;
					}
					results[i] = new ComparisonResults(ind, row);
				} else {
					// We should abort the process
					throw new ComparisonException(
						"Individual " + ind + " is missing in the simulation record and cannot be considered invalid");
				}
			}
			else {
				final ComparisonResult[] row = comparisonRecord.getComparisonResults();
				BigInteger[] testWallNanos = comparisonRecord.getTestWallNanos();

				if (stopAtFirstDiff) {
					final int firstDiff = comparisonRecord.getFirstTestWithDifferentOutput();
					for (int j = firstDiff + 1; j < row.length; ++j) {
						row[j] = ComparisonResult.SAME_OUTPUT;
					}
					if (testWallNanos != null) {
						for (int j = firstDiff + 1; j < row.length; ++j) {
							testWallNanos[j] = BigInteger.ZERO;
						}
					}
				}

				results[i] = new ComparisonResults(ind, row);
				results[i].setTestWallNanos(testWallNanos);
			}
		}

		return results;
	}

	@Override
	public void loadDataFile(File dataFile) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public File getOriginalProgramFile() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public File getMutantFile(GAIndividual individual)	throws UnsupportedOperationException, GenerationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (simRecord == null) {
			throw new InvalidConfigurationException("No simulation record has been set.");
		}
	}

	@Override
	public boolean getStopAtFirstDiff() {
		return stopAtFirstDiff;
	}

	@Override
	public void setStopAtFirstDiff(boolean stopAtFirstDiff) {
		this.stopAtFirstDiff = stopAtFirstDiff;
	}

	@Override
	public List<String> getTestCaseNames() throws AnalysisException {
		return simRecord.getTestCaseNames();
	}

	@Override
	public List<String> getMutationOperatorNames() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SimulatedExecutor that = (SimulatedExecutor) o;
		return nTestCases == that.nTestCases &&
				stopAtFirstDiff == that.stopAtFirstDiff &&
				Objects.equals(fSimRecord, that.fSimRecord) &&
				Objects.equals(simRecord, that.simRecord) &&
				Objects.equals(includedTests, that.includedTests) &&
				Objects.equals(excludedTests, that.excludedTests);
	}

	@Override
	public int hashCode() {
		return Objects.hash(fSimRecord, simRecord, nTestCases, stopAtFirstDiff, includedTests, excludedTests);
	}
}
