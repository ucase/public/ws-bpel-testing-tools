package es.uca.webservices.gamera.subcommands;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.conf.Configuration;
import es.uca.webservices.gamera.conf.ConfigurationLoader;

/**
 * Abstract base class for every supported subcommand.
 * 
 * @author Antonio García-Domínguez
 */
abstract class AbstractSubcommand implements ISubcommand {

	private static final String HELP_OPTION = "help";

	private int fMinArgumentCount;
	private int fMaxArgumentCount;
	private String fUsage;
	private String fName;
	private String fDescription;
	private List<String> fNonOptionArgs;
	private boolean fRequestedHelp = false;
	private PrintStream fPrintStream = System.out;

	/** If used in the constructor, it means that it can receive an unlimited
	 *  number of arguments. */
	public static final int NARGS_UNLIMITED = 0;

	/**
	 * Creates a new instance.
	 * 
	 * @param minArgumentCount
	 *            Minimum number of required non-option arguments.
	 * @param maximumArgumentCount
	 *            Maximum number of required non-options arguments (
	 *            {@link #NARGS_UNLIMITED} for unlimited).
	 * @param name
	 *            Name of the subcommand (as in 'run' or 'compare')
	 * @param usage
	 *            Usage string (without the 'cmd subcmd' part)
	 * @param description
	 *            Long description (spanning over several lines, if necessary).
	 */
	public AbstractSubcommand(int minArgumentCount, int maxArgumentCount,
			String name, String usage, String description) {
		this.fMinArgumentCount = minArgumentCount;
		this.fMaxArgumentCount = maxArgumentCount;
		this.fName = name;
		this.fUsage = usage;
		this.fDescription = description;
	}

	public final String getName() {
		return fName;
	}

	public final String getUsage() {
		return fUsage;
	}

	public final String getDescription() {
		return fDescription;
	}

	public final void parseArgs(String... args) {
		OptionParser parser = createOptionParser();
		try {
			OptionSet options = parser.parse(args);
			fNonOptionArgs = options.nonOptionArguments();

			if (options.has(HELP_OPTION)) {
				this.fRequestedHelp = true;
			} else if (getNonOptionArgs().size() < fMinArgumentCount
					|| (fMaxArgumentCount > 0 && getNonOptionArgs().size() > fMaxArgumentCount)) {
				throw new IllegalArgumentException("Wrong number of arguments");
			} else {
				parseOptions(options);
			}
		} catch (OptionException ex) {
			throw new IllegalArgumentException(ex.getLocalizedMessage(), ex);
		}
	}

	public final void run() throws Exception {
		if (fRequestedHelp) {
			printHelp(createOptionParser());
		} else {
			runCommand();
		}
	}

	/**
	 * Sets the output stream to which the XML results will be dumped. The
	 * output stream will be internally wrapped with a PrintStream.
	 */
	public final void setOutputStream(OutputStream fOutputStream) {
		this.fPrintStream = new PrintStream(fOutputStream);
	}

	/**
	 * Returns the output stream to which the XML results will be dumped.
	 */
	public final PrintStream getOutputStream() {
		return fPrintStream;
	}

	/* PROTECTED METHODS */

	/**
	 * Runs the command itself. This should be called after everything has been
	 * properly configured either through {@link #parseArgsAndRun} or by
	 * manually setting the proper options.
	 */
	protected abstract void runCommand() throws Exception;

	/**
	 * Parses the options given by the user, setting various values required by
	 * the subcommand. The default implementation is empty: --help is handled in
	 * {@link #parseArgs(String...)} and {@link #run()}.
	 */
	protected void parseOptions(OptionSet options) {
		// nothing here for now
	}

	/**
	 * Returns the option parser. Please do not forget to reuse the OptionParser
	 * created by the superclass in your subclasses!
	 */
	protected OptionParser createOptionParser() {
		OptionParser parser = new OptionParser();
		parser.accepts(HELP_OPTION, "Provides help on the subcommand");
		return parser;
	}

	/**
	 * Returns the list of all non-option arguments.
	 */
	protected final List<String> getNonOptionArgs() {
		return fNonOptionArgs;
	}

	/**
	 * Loads a YAML configuration file.
	 */
	protected 	Configuration loadConfiguration(File file)
			throws FileNotFoundException, ClassNotFoundException,
			InvalidConfigurationException {
        ConfigurationLoader loader = new ConfigurationLoader();
        return loader.parse(new FileInputStream(file));
	}

	/* PRIVATE METHODS */

	private void printHelp(OptionParser parser) throws IOException {
		System.err.println("Usage: gamerahom " + getName() + " " + getUsage());
		System.err.println("Description:\n");
		System.err.println(getDescription() + "\n");
		parser.printHelpOn(System.err);
	}

}
