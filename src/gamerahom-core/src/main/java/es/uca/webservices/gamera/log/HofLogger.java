package es.uca.webservices.gamera.log;

import java.util.Iterator;
import java.util.Map.Entry;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;

/**
 * Logger which prints messages about the Hall of Fame to console and/or to a file.
 *
 * @author Emma Blanco-Muñoz, Antonio García-Domínguez
 * @version 1.1
 */

@Option(description="Logger which prints messages about the Hall of Fame to console and/or to a file.")
public class HofLogger extends AbstractLogger {

	private static final String HOF_HEADER =
		"MUTANT_N {ORDER (OPERATOR, LOCATION, ATTRIBUTE)+}, FITNESS, [COMPARISON RESULTS]";

	@Override
	public void finished(GAState state) {
		GAHof hof = state.getHof();		
		AnalysisResults analysis = hof.getAnalysis();
		String[] names = analysis.getOperatorNames();

		int indCount = 0;
		int testCasesNum = 0;

		final Iterator<ComparisonResults> itHOFEntries = hof.values().iterator();
		final ComparisonResults firstEntry = itHOFEntries.hasNext() ? itHOFEntries.next() : null;
		if (firstEntry != null) {
			testCasesNum = firstEntry.getRow().length;
		}
		getStream().printMsgByConsoleFile(HOF_HEADER, state.getElapsedMillis(), isConsole(), getFile());

		for(Entry<GAIndividual, ComparisonResults> pair : hof.entrySet()) {
			StringBuilder sb = new StringBuilder();
			GAIndividual ind = (GAIndividual) pair.getKey();				

			sb.append("MUTANT_");
			sb.append(indCount);
			sb.append("{");
			sb.append(ind.getOrder());
			sb.append(' ');

			// Print all mutants of the individual, remove the last comma and put a bracket
			printHOM(names, sb, ind);
			sb.append("}, ");

			// Fitness
			sb.append(ind.getFitness());
			sb.append(", ");

			// Comparison results
			sb.append('[');
			ComparisonResult[] executionRow = pair.getValue().getRow();
			if (testCasesNum > 0) {
				// This value will be:
				//     0 (same output), 
				//     1 (different output) or 
				//     2 (invalid)
				sb.append(executionRow[0].getValue());
				for(int t = 1; t < testCasesNum; t++) { 
					sb.append(' ');
					sb.append(executionRow[t].getValue());
				}
			}
			sb.append(']');
			
			getStream().printMsgByConsoleFile(sb.toString(), state.getElapsedMillis(), isConsole(), getFile());
			indCount++;
		}
	}

}
