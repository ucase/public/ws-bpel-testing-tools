package es.uca.webservices.gamera.log;

import java.math.BigInteger;
import java.util.List;

import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.log.GALogger;

/**
 * Abstract class for the loggers.
 *
 * @author Emma Blanco-Muñoz
 * @version 1.0
 */
public class AbstractLogger implements GALogger {

    private boolean console = true;
    private String file;

    private PrintingByConsoleOrFile stream = new PrintingByConsoleOrFile();
    
    @Override
    public void finished(GAState state) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
    }

    @Override
    public void finishedAnalysis(GAState state) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
    }

    @Override
    public void finishedComparison(GAState state, GAPopulation population,
            GAIndividual individual, ComparisonResults comparison) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
    }

    @Override
    public void finishedComparison(GAState state, GAPopulation population) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
    }

    @Override
    public void finishedEvaluation(GAState state, GAPopulation population) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
    }

    @Override
    public void newGeneration(GAState state, GAPopulation population) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
    }

    @Override
    public void started(GAState state) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
    }

    @Override
    public void startedAnalysis(GAState state) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
    }

    @Override
    public void startedComparison(GAState state, GAPopulation population) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
    }

    @Override
    public void startedComparison(GAState state, GAPopulation population,
            GAIndividual individual) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
    }

    @Override
    public void startedEvaluation(GAState state, GAPopulation population) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
    }

	@Override
	public void appliedGeneticOperator(GAState state, String geneticOperatorName, 
			List<GAIndividual> parent, List<GAIndividual> child) {
    	if(stream.getPs() == null) {
    		stream.createStream(console, file);
    	}
	}

    // VALIDATION /////////////////////////////////////////

    @Override
    public void validate() throws InvalidConfigurationException {
        if (!isConsole() && getFile() == null) {
            throw new InvalidConfigurationException(
                    "At least the console or file output should be enabled. " +
                    "If you do not want any output, please use the " +
                    "NullLogger instead.");
        } else if (isConsole() && getFile() != null) {
            throw new InvalidConfigurationException(
                    "Only an output should be enabled. Please choose the console" +
                    " or file output.");
        }
    }
    
    // BEAN PROPERTIES ////////////////////////////////////

    public void setConsole(boolean console) {
        this.console = console;
    }

    public boolean isConsole() {
        return console;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFile() {
        return file;
    }
    

	public void printHOM(String[] operatorNames, StringBuilder sb, GAIndividual individual) {
		if (individual.getOrder() > 0) {
			// We write information about every mutant for each individual
			printHOMComponent(1, operatorNames, sb, individual);
			for(int i = 2; i <= individual.getOrder(); i++) {
				sb.append(", ");
				printHOMComponent(i, operatorNames, sb, individual);
			}
		}
	}

	private void printHOMComponent(int i, String[] operatorNames, StringBuilder sb, GAIndividual individual) {
		final BigInteger[] mutant = individual.getMutant(i);
		final BigInteger o = mutant[0];
		final BigInteger l = mutant[1];
		final BigInteger a = mutant[2];

		// Operator
		// The first element in the array is zero, for this reason
		// the value has to be decremented
		sb.append("(");
		sb.append(operatorNames[o.intValue() - 1]);
		sb.append(", ");

		// Location
		sb.append(l);
		sb.append(", ");

		// Attribute
		sb.append(a);
		sb.append(")");
	}
	
    // GETTERS & SETTERS //////////////////////////////////
    
    @Option(hidden=true)
	public PrintingByConsoleOrFile getStream() {
		return stream;
	}

    @Option(hidden=true)
	public void setStream(PrintingByConsoleOrFile stream) {
		this.stream = stream;
	}   

}