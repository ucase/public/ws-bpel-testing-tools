package es.uca.webservices.gamera;

import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.conf.Configuration;
import es.uca.webservices.gamera.conf.ConfigurationValidationMode;

public class ThresholdBasedHybridAlgorithm extends AbstractGenerationalAlgorithm {

	private final AbstractGenerationalAlgorithm firstAlgorithm;
	private final AbstractGenerationalAlgorithm secondAlgorithm;
	private final double threshold;
	private AbstractGenerationalAlgorithm current;

	public ThresholdBasedHybridAlgorithm(AbstractGenerationalAlgorithm first, AbstractGenerationalAlgorithm second, double threshold) {
		this.firstAlgorithm = first;
		this.secondAlgorithm = second;
		this.threshold = threshold;

		this.current = this.firstAlgorithm;
	}

	@Override
	protected void nextGeneration(GAState state, GAPopulation currentPopulation, 
			GAPopulation prevPopulation, GALogger logger) throws ComparisonException, GenerationException
	{
		final double ratio = (double) state.getHof().size() / state.getTotalNumberOfMutants();
		if (ratio >= threshold) {
			this.current = this.secondAlgorithm;
		}
		
		current.nextGeneration(state, currentPopulation, prevPopulation, logger);
	}

	@Override
	protected void generateFirstGeneration(GAState state, GAPopulation destination) {
		current.generateFirstGeneration(state, destination);
	}

	@Override
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);
		firstAlgorithm.setConfiguration(configuration);
		secondAlgorithm.setConfiguration(configuration);
	}

	@Override
	protected void validate() throws InvalidConfigurationException {
		super.validate();
		getConfiguration().validate(ConfigurationValidationMode.FULL);
	}
}
