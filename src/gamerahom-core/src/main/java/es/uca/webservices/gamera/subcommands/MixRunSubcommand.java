package es.uca.webservices.gamera.subcommands;

import java.io.File;

import es.uca.webservices.gamera.AbstractGenerationalAlgorithm;
import es.uca.webservices.gamera.GeneticAlgorithm;
import es.uca.webservices.gamera.RandomSelectionAlgorithm;
import es.uca.webservices.gamera.ThresholdBasedHybridAlgorithm;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * Subcommand that runs the genetic algorithm and the random selection using the
 * specified YAML configuration file.
 */

public class MixRunSubcommand extends ExecutionSubcommand {

	private static final String DESCRIPTION =
			"Loads the specified .yaml file and runs the genetic algorithm, switching to\n"
			+ "random selection after a percentage of all mutants have been run, using the\n"
			+ "components with the specified configuration.\n";

	private static final String THRESHOLD_OPTION = "threshold";
	private static final double THRESHOLD_DEFAULT = 0.5d;
	private double threshold = THRESHOLD_DEFAULT;

	public MixRunSubcommand() {
		super(1, 1, "mixrun", "config.yaml", DESCRIPTION);
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		if (options.has(THRESHOLD_OPTION)) {
			threshold = (Double) options.valueOf(THRESHOLD_OPTION);
		}
	}

	@Override
	protected OptionParser createOptionParser() {
		OptionParser parser = super.createOptionParser();
		parser
			.accepts(THRESHOLD_OPTION,
					"0-1 ratio after which execution should switch to random selection")
			.withRequiredArg().describedAs("R")
			.ofType(Double.class).defaultsTo(THRESHOLD_DEFAULT);
		return parser;
	}

	public double getThreshold() {
		return threshold;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	@Override
	protected void runCommand() throws Exception {
		final File yaml = new File(getNonOptionArgs().get(0));
		AbstractGenerationalAlgorithm mixgenerator = new ThresholdBasedHybridAlgorithm(
				new GeneticAlgorithm(),
				new RandomSelectionAlgorithm(),
				threshold);
		mixgenerator.setConfiguration(loadConfiguration(yaml));
		mixgenerator.run();
	}
}
