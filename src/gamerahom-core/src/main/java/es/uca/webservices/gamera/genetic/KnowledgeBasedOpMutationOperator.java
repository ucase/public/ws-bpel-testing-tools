package es.uca.webservices.gamera.genetic;

import java.math.BigInteger;
import java.util.Collections;
import java.util.Map;
import java.util.Random;

import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.util.kbg.KnowledgeBasedOperatorRoulette;

/**
 * Tweaks the {@link IndividualMutationOperator} so mutating the operator
 * samples the estimated distribution of strong mutants by operator in the HOF
 * since the last generation.
 *
 * This class is not thread-safe.
 */
public class KnowledgeBasedOpMutationOperator extends IndividualMutationOperator {

	private GAState lastSeenState;
	private KnowledgeBasedOperatorRoulette roulette = new KnowledgeBasedOperatorRoulette(Collections.emptyMap());

	public Map<String, Double> getRatios() {
		return roulette.getRatios();
	}

	public void setRatios(Map<String, Double> ratios) {
		this.roulette = new KnowledgeBasedOperatorRoulette(ratios);
		roulette.setPrng(prng);
	}

	public double getKnowledgeRewardScale() {
		return roulette.getRewardScale();
	}

	public void setKnowledgeRewardScale(double scale) {
		roulette.setRewardScale(scale);
	}

	@Override
	public void apply(GAState state, GAPopulation source, GAPopulation destination, GALogger logger,
			boolean mutateAttribute, boolean createTree) {
		lastSeenState = state;
		super.apply(state, source, destination, logger, mutateAttribute, createTree);
	}

	@Override
	protected BigInteger computeMutatedValue(int mutatedField, BigInteger currentValue, BigInteger maxValue,
			BigInteger maxDelta) {
		if (mutatedField == MUT_OPERATOR) {
			return roulette.chooseOperator(lastSeenState);
		} else {
			return super.computeMutatedValue(mutatedField, currentValue, maxValue, maxDelta);
		}
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
		roulette.setPrng(prng);
	}
}
