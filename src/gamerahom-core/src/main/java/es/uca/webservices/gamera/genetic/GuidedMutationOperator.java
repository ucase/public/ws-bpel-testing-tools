package es.uca.webservices.gamera.genetic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.log.GALogger;

/**
 * Implementation of the guided mutation operator in:
 *
 * An Evolutionary Algorithm With Guided Mutation for the Maximum Clique Problem
 * IEEE TRANSACTIONS ON EVOLUTIONARY COMPUTATION, VOL. 9, NO. 2, APRIL 2005 
 */
public class GuidedMutationOperator extends AbstractGeneticOperator {

	private GAPopulation lastPopulation;
	private GuidedBitMutator bmOperator = new GuidedBitMutator(),
			bmLocation = new GuidedBitMutator(),
			bmAttribute = new GuidedBitMutator();

	@Override
	public void apply(GAState state, GAPopulation source, GAPopulation destination, GALogger logger, boolean mutateAttribute, boolean createTree) {
		if (source != lastPopulation) {
			updateBitMutators(state, source);
		}

		final GAIndividual srcIndividual = getSelector().select(source).denormalize(state.getAnalysisResults());
		final AnalysisResults analysisResults = state.getAnalysisResults();
		final GAIndividual dstIndividual = new GAIndividual(
			Math.max(1,
					bmOperator.mutate(srcIndividual.getThOperator(1).intValue(), prng) % analysisResults.getMaxOp().intValue()),
			Math.max(1,
					bmLocation.mutate(srcIndividual.getThLocation(1).intValue(), prng) % analysisResults.getMaxLoc().intValue()),
			Math.max(1,
					bmAttribute.mutate(srcIndividual.getThAttribute(1).intValue(), prng) % analysisResults.getMaxAtt().intValue())
		);

		logger.appliedGeneticOperator(state, GuidedMutationOperator.class.getName(),
				Arrays.asList(srcIndividual), Arrays.asList(dstIndividual));
		destination.addIndividual(dstIndividual.denormalize(analysisResults));
	}

	private void updateBitMutators(GAState state, GAPopulation source) {
		List<Integer> desiredOperators = new ArrayList<>(source.getPopulationSize());
		List<Integer> desiredLocations = new ArrayList<>(source.getPopulationSize());
		List<Integer> desiredAttributes = new ArrayList<>(source.getPopulationSize());

		for (GAIndividual ind : source.getIndividuals()) {
			GAIndividual normalized = ind.normalize(state.getAnalysisResults());
			ComparisonResults cmp = state.getHof().get(ind);
			if (cmp.isAlive()) {
				desiredOperators.add(normalized.getThOperator(1).intValue());
				desiredLocations.add(normalized.getThLocation(1).intValue());
				desiredAttributes.add(normalized.getThAttribute(1).intValue());
			}
		}

		bmOperator.update(desiredOperators.toArray(new Integer[desiredOperators.size()]));
		bmLocation.update(desiredLocations.toArray(new Integer[desiredLocations.size()]));
		bmAttribute.update(desiredAttributes.toArray(new Integer[desiredAttributes.size()]));

		lastPopulation = source;
	}

	public double getLearningFactor() {
		return bmOperator.getLearningFactor();
	}

	public void setLearningFactor(double x) {
		bmOperator.setLearningFactor(x);
		bmLocation.setLearningFactor(x);
		bmAttribute.setLearningFactor(x);
	}

	public double getSamplingRate() {
		return bmOperator.getSamplingRate();
	}

	public void setSamplingRate(double x) {
		bmOperator.setSamplingRate(x);
		bmLocation.setSamplingRate(x);
		bmAttribute.setSamplingRate(x);
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		super.validate();
		if (getLearningFactor() < 0 || getLearningFactor() > 1) {
			throw new InvalidConfigurationException("Learning factor must be within [0, 1]");
		}
		if (getSamplingRate() < 0 || getSamplingRate() > 1) {
			throw new InvalidConfigurationException("Sampling rate must be within [0, 1]");
		}
	}

}
