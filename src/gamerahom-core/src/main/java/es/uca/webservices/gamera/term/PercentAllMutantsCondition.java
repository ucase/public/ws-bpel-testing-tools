package es.uca.webservices.gamera.term;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Set;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionType;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.term.GATerminationCondition;
import es.uca.webservices.gamera.record.SimulationRecord;

/**
 * Termination condition which is true when a certain percentage of all mutants
 * have been generated.
 *
 * @author Antonio García-Domínguez
 * @version 1.0
 */
@Option(description="Termination condition which is true when a certain percentage " +
		"of all mutants have been generated.")
public class PercentAllMutantsCondition implements GATerminationCondition {

    private double percent;
	private File fileMutants1 = null;
	private SimulationRecord simRecord1;
	private File fSimRecord1;
	
	private Set<GAIndividual> mutantsGenerated;
    
    public void setPercent(double percent) {
        this.percent = percent;
    }

    public double getPercent() {
        return percent;
    }
    
	public Set<GAIndividual> getMutantsGenerated() {
		return mutantsGenerated;
	}

	public void setMutantsGenerated(Set<GAIndividual> mutantsGenerated) {
		this.mutantsGenerated = mutantsGenerated;
	}
	
	@Option(type=OptionType.PATH_LOAD, fileExtensions={"txt"})
	public void setFileMutants1(String fileMutants) {
		this.fileMutants1 = fileMutants != null ? new File(fileMutants) : null;
	}

	public File getfSimRecord1() {
		return fSimRecord1;
	}

	public void setfSimRecord1(File fSimRecord1) throws Exception {
		this.fSimRecord1 = fSimRecord1;
		setSimRecord1(SimulationRecord.loadFrom(fSimRecord1));
	}
	
	public SimulationRecord getSimRecord1() {
		return simRecord1;
	}

	public void setSimRecord1(SimulationRecord simRecord1) {
		this.simRecord1 = simRecord1;
	}

    @Override
    public boolean evaluate(GAState state) {
        final long generated = state.getHof().size();
        final long total = state.getTotalNumberOfMutants();
        final double generatedPercent = generated / (double) total;
       
        //Modified
        //--------
        if(generatedPercent >= percent){
        	if(fileMutants1 != null){
	        
	        	try {
	        		mutantsGenerated = state.getHof().keySet();
	        		
	        		PrintStream ps;
	        		ps = new PrintStream(new File(fileMutants1.toString()));
	        		ps.flush();
			    
	        		for (Iterator<GAIndividual> mg = mutantsGenerated.iterator(); mg.hasNext(); ) {
				    
						final GAIndividual sm = mg.next();
						if (simRecord1.getComparisonRecords().containsKey(sm)) {
							ps.println("m" + sm.getOperator() + "_" + sm.getLocation() + "_" + sm.getAttribute());
						}
						//System.out.println("m" + sm.getOperator() + "_" + sm.getLocation() + "_" + sm.getAttribute());
	        		}
				    
					ps.close();
	        	} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
	        }
        }
        //------
        
        return generatedPercent >= percent;
    }

    @Override
    public void validate() throws InvalidConfigurationException {
        if (getPercent() <= 0 || getPercent() > 1) {
            throw new InvalidConfigurationException(
                    "The proportion of all mutants to be generated must "
                            + "lie in the [0,1) range");
        }
    }


	
}
