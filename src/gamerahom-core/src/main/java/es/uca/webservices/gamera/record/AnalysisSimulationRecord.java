package es.uca.webservices.gamera.record;

import java.math.BigInteger;
import java.util.Arrays;

import es.uca.webservices.gamera.api.AnalysisResults;

/**
 * Records all the information to simulate a previous analysis using some other
 * {@link GAExecutor}
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
public class AnalysisSimulationRecord {

	private static final BigInteger RATIO_NANOS_MILLIS = BigInteger.valueOf(1000000);
	private String[] operatorNames;
	private BigInteger[] locationCounts;
	private BigInteger[] fieldRanges;
	private BigInteger totalWallNanos;

	public AnalysisSimulationRecord() {
		// empty ctor for SnakeYAML
	}

	public AnalysisSimulationRecord(AnalysisResults results, BigInteger totalWallNanos) {
		this.operatorNames = results.getOperatorNames();
		this.locationCounts = results.getLocationCounts();
		this.fieldRanges = results.getFieldRanges();
		this.totalWallNanos = totalWallNanos;
	}

	public String[] getOperatorNames() {
		return operatorNames;
	}

	public void setOperatorNames(String[] operatorNames) {
		this.operatorNames = operatorNames;
	}

	public BigInteger[] getLocationCounts() {
		return locationCounts;
	}

	public void setLocationCounts(BigInteger[] locationCounts) {
		this.locationCounts = locationCounts;
	}

	public BigInteger[] getFieldRanges() {
		return fieldRanges;
	}

	public void setFieldRanges(BigInteger[] fieldRanges) {
		this.fieldRanges = fieldRanges;
	}

	public long getTotalWallMillis() {
		return getTotalWallNanos().divide(RATIO_NANOS_MILLIS).longValue();
	}

	public void setTotalWallMillis(long totalWallMillis) {
		setTotalWallNanos(BigInteger.valueOf(totalWallMillis).multiply(RATIO_NANOS_MILLIS));
	}

	public BigInteger getTotalWallNanos() {
		return totalWallNanos;
	}

	public void setTotalWallNanos(BigInteger totalWallNanos) {
		this.totalWallNanos = totalWallNanos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(fieldRanges);
		result = prime * result + Arrays.hashCode(locationCounts);
		result = prime * result + Arrays.hashCode(operatorNames);
		result = prime * result
				+ ((totalWallNanos == null) ? 0 : totalWallNanos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AnalysisSimulationRecord other = (AnalysisSimulationRecord) obj;
		if (!Arrays.equals(fieldRanges, other.fieldRanges)) {
			return false;
		}
		if (!Arrays.equals(locationCounts, other.locationCounts)) {
			return false;
		}
		if (!Arrays.equals(operatorNames, other.operatorNames)) {
			return false;
		}
		if (totalWallNanos == null) {
			if (other.totalWallNanos != null) {
				return false;
			}
		} else if (!totalWallNanos.equals(other.totalWallNanos)) {
			return false;
		}
		return true;
	}

}
