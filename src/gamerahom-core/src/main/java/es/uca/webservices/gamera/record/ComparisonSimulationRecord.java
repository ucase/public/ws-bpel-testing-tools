package es.uca.webservices.gamera.record;

import java.math.BigInteger;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;

/**
 * Records all the information required of a certain comparison in order to
 * simulate it from a data file.
 *
 * @author Antonio García-Domínguez
 */
public class ComparisonSimulationRecord {

	private static final BigInteger NANOS_TO_MILLIS = BigInteger.valueOf(1000000);
	private static final Logger LOGGER = LoggerFactory.getLogger(ComparisonSimulationRecord.class);
	private Integer[] row;
	private BigInteger[] testWallNanos;
	private BigInteger totalWallNanos;

	public ComparisonSimulationRecord() {
		// empty ctor for SnakeYAML
	}

	public ComparisonSimulationRecord(ComparisonResults results, BigInteger totalWallNanos) {
		final ComparisonResult[] comparisons = results.getRow();
		row = new Integer[comparisons.length];
		for (int i = 0; i < row.length; ++i) {
			row[i] = comparisons[i].getValue();
		}
		this.totalWallNanos = totalWallNanos;
		this.testWallNanos = results.getTestWallNanos();
	}

	public ComparisonSimulationRecord(Integer[] row, BigInteger totalWallNanos) {
		this.row = row;
		this.totalWallNanos = totalWallNanos;
	}

	public Integer[] getRow() {
		return row;
	}

	public ComparisonResult[] getComparisonResults() {
		ComparisonResult[] resultRow = new ComparisonResult[row.length];
		for (int i = 0; i < row.length; i++) {
			resultRow[i] = ComparisonResult.getObject(row[i]);
		}
		return resultRow;
	}

	public boolean isValid() {
		for (ComparisonResult v : getComparisonResults()) {
			if (!v.isValid()) {
				return false;
			}
		}
		return true;
	}

	public boolean isAlive() {
		for (ComparisonResult v : getComparisonResults()) {
			if (!v.isAlive()) {
				return false;
			}
		}
		return true;
	}

	public boolean isKilled() {
		return !isAlive();
	}

	public void setRow(Integer[] row) {
		this.row = row;
	}

	public long getTotalWallMillis() {
		return totalWallNanos.divide(NANOS_TO_MILLIS).longValue();
	}

	public void setTotalWallMillis(long totalWallMillis) {
		this.totalWallNanos = BigInteger.valueOf(totalWallMillis).multiply(NANOS_TO_MILLIS);
	}

	public BigInteger getTotalWallNanos() {
		return totalWallNanos;
	}

	public void setTotalWallNanos(BigInteger totalWallMillis) {
		this.totalWallNanos = totalWallMillis;
	}

	public BigInteger[] getTestWallNanos() {
		return testWallNanos;
	}

	public void setTestWallNanos(BigInteger[] testWallNanos) {
		this.testWallNanos = testWallNanos;
	}

	/**
	 * Returns the number of test cases run.
	 */
	public int getTestCaseCount() {
		return row.length;
	}

	/**
	 * Returns the sum of the nanoseconds required by every test case up to the
	 * first one that killed the mutant. If the mutant survived, it will return
	 * the sum of the times of all test cases.
	 */
	public BigInteger getNanosUpToFirstDeath() {
		if (testWallNanos == null) {
			LOGGER.warn("Test by test times are not available: returning the total time");
			return totalWallNanos;
		}

		final int upperBound = Math.min(row.length, getFirstTestWithDifferentOutput() + 1);
		BigInteger sum = BigInteger.ZERO;
		for (int i = 0; i < upperBound; ++i) {
			sum = sum.add(testWallNanos[i]);
		}
		return sum;
	}

	/**
	 * Returns the position (from 0 to <code>getRow().length - 1</code>) of the
	 * first test case that kills this individual. If the individual stayed
	 * alive, returns <code>getRow().length</code>.
	 */
	public int getFirstTestWithDifferentOutput() {
		int firstDeath = 0;
		ComparisonResult[] results = getComparisonResults();
		for (ComparisonResult result : results) {
			if (!result.isAlive()) {
				return firstDeath;
			}
			++firstDeath;
		}
		return firstDeath;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(row);
		result = prime * result + Arrays.hashCode(testWallNanos);
		result = prime * result
				+ ((totalWallNanos == null) ? 0 : totalWallNanos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ComparisonSimulationRecord other = (ComparisonSimulationRecord) obj;
		if (!Arrays.equals(row, other.row)) {
			return false;
		}
		if (!Arrays.equals(testWallNanos, other.testWallNanos)) {
			return false;
		}
		if (totalWallNanos == null) {
			if (other.totalWallNanos != null) {
				return false;
			}
		} else if (!totalWallNanos.equals(other.totalWallNanos)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ComparisonSimulationRecord ["
				+ (row != null ? "row=" + Arrays.toString(row) + ", " : "")
				+ (testWallNanos != null ? "testWallNanos=" + Arrays.toString(testWallNanos) + ", " : "")
				+ (totalWallNanos != null ? "totalWallNanos=" + totalWallNanos : "")
				+ "]";
	}

	/**
	 * Returns {@code true} iff this comparison record subsumes {@code other}. For the definition
	 * of subsumption, we use the dynamic subsumption definition from:
	 * 
	 * <pre>
	 * P. Ammann, M. E. Delamaro, and J. Offutt, ‘Establishing Theoretical Minimal Sets of Mutants’,
	 * in Verification and Validation 2014 IEEE Seventh International Conference on Software Testing,
	 * Mar. 2014, pp. 21–30. doi: 10.1109/ICST.2014.13.
	 * </pre>
	 *
	 * The definition is as follows, where S(i, j) means "test i killed mutant j":
	 * 
	 * <pre>
	 * Definition 4: Dynamic subsumption. If mutant x is not live and S(i, x) \implies S(i, y),
	 * i=1..|T|, we say that mutant x dynamically subsumes mutant y with respect to T .
	 * </pre>
	 */
	public boolean subsumes(ComparisonSimulationRecord other) {
		if (getTestCaseCount() != other.getTestCaseCount()) {
			throw new IllegalArgumentException(String.format(
				"Test case counts are different: %d != %d",
				getTestCaseCount(), other.getTestCaseCount()));
		}

		// "If mutant x is not live"
		if (isKilled()) {
			ComparisonResult[] results = getComparisonResults();
			ComparisonResult[] otherResults = other.getComparisonResults();

			for (int i = 0; i < results.length; ++i) {
				if (results[i].isDead()) {
					if (!otherResults[i].isDead()) {
						/*
						* Found a test case where this mutant is dead and the other one is not dead:
						* this mutant does not subsume the other one.
						*/
						return false;
					}
				} else if (results[i].isExecuted()) {
					if (!otherResults[i].isDead() && !otherResults[i].isExecuted()) {
						/*
						* This is an extension of the original definition from Papadakis to handle
						* coverage. This would be a case where this mutant is covered-but-not-killed
						* and the other mutant is neither killed nor covered.
						*/
						return false;
					}
				}
			}
			return true;
		}

		return false;
	}
}
