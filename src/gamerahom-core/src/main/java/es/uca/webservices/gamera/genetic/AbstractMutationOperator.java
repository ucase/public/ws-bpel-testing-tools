package es.uca.webservices.gamera.genetic;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Abstract class for the mutation operators.
 * 
 * All mutation operators have some parameters which are used to apply the
 * mutation operator. The value of these parameters are changeable since the 
 * configuration file.
 * 
 * This class avoids the code duplication between all mutation operators.
 * 
 * @author Emma Blanco-Muñoz, Antonio García-Domínguez
 * @version 1.1
 */
public abstract class AbstractMutationOperator extends AbstractGeneticOperator {

	// Maximum magnitude of the mutation (+/-) when the scaleFactor is set to 1
	private Integer randomRange;

	// Alternative version of randomRange, expressed as a percentage of the total available range
	private Double randomRangePercent;

	// It represents the probability of the individual mutation operator. The
	// more it is used, the less drastic that mutations should be.
	private Double scaleFactor;

    @Override
    public void validate() throws InvalidConfigurationException {
    	super.validate();
        if (getScaleFactor() == null || getScaleFactor() <= 0 || getScaleFactor() >= 1) {
            throw new InvalidConfigurationException("Mutation probability (scaleFactor) should lie in (0, 1)");
        }
        if ((getRandomRange() == null) == (getRandomRangePercent() == null)) {
        	throw new InvalidConfigurationException("Exactly one of randomRange or randomRangePercent should be used");
        }
        if (getRandomRangePercent() != null && (getRandomRangePercent() <= 0 || getRandomRangePercent() > 1)) {
        	throw new InvalidConfigurationException("randomRangePercent should lie in (0, 1]");
        }
        if (getRandomRange() != null && getRandomRange() < 0) {
            throw new InvalidConfigurationException("randomRange should be > 0");
        }
    }

	public Integer getRandomRange() {
		return randomRange;
	}

	public void setRandomRange(Integer randomRange) {
		this.randomRange = randomRange;
	}

	public Double getRandomRangePercent() {
		return randomRangePercent;
	}

	public void setRandomRangePercent(Double randomRangePercent) {
		this.randomRangePercent = randomRangePercent;
	}

	public Double getScaleFactor() {
		return scaleFactor;
	}

	public void setScaleFactor(Double scaleFactor) {
		this.scaleFactor = scaleFactor;
	}
}