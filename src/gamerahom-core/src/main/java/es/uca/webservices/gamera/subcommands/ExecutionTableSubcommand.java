package es.uca.webservices.gamera.subcommands;

import java.io.File;
import java.io.PrintStream;
import java.util.Map.Entry;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.record.ComparisonSimulationRecord;
import es.uca.webservices.gamera.record.SimulationRecord;

/**
 * Produces a table to use with R to find surviving not-covered, surviving
 * covered and killed mutants.
 */
public class ExecutionTableSubcommand extends AbstractSubcommand {

	private static final String DESCRIPTION =
		"Prints to the standard output stream a table with each individual\n" +
		"and its execution table row, for later use with R."
	;

	public ExecutionTableSubcommand() {
		super(1, 1, "exectable", "simrecord.(raw|yaml)", DESCRIPTION);
	}

	@Override
	protected void runCommand() throws Exception {
		final File simRecordFile = new File(getNonOptionArgs().get(0));
		final SimulationRecord sR = SimulationRecord.loadFrom(simRecordFile); 
		renderReport(simRecordFile, sR);
	}

	private void renderReport(File simRecordFile, SimulationRecord sR) throws Exception {
		PrintStream ps = getOutputStream();
		
		for (int iOrder = 0; iOrder < sR.getMaxOrder(); iOrder++) {
			if (iOrder > 0) {
				ps.print("\t");
			}
			ps.print(String.format("op%d\tloc%d\tattr%d", iOrder, iOrder, iOrder));
		}
		for (int iTest = 0; iTest < sR.getTestCaseCount(); iTest++) {
			ps.print(String.format("\ttest%d", iTest));
		}
		ps.println();

		for (Entry<GAIndividual, ComparisonSimulationRecord> entry : sR.getComparisonRecords().entrySet()) {
			final GAIndividual ind = entry.getKey();
			final ComparisonSimulationRecord cmp = entry.getValue();

			for (int iOrder = 1; iOrder <= sR.getMaxOrder(); iOrder++) {
				if (iOrder > 1) {
					ps.print("\t");
				}
				ps.print(String.format("%d\t%d\t%d",
					ind.getThOperator(iOrder),
					ind.getThLocation(iOrder),
					ind.getThAttribute(iOrder)));
			}

			for (ComparisonResult cmpResult : cmp.getComparisonResults()) {
				ps.print(String.format("\t%d", cmpResult.getValue()));
			}
			ps.println();
		}
	}

}
