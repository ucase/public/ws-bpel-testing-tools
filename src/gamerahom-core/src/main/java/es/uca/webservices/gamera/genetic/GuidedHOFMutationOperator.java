package es.uca.webservices.gamera.genetic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.log.GALogger;

/**
 * Variant of the {@link GuidedMutationOperator} which always considers the full
 * surviving HOF. No learning factor involved here.
 */
public class GuidedHOFMutationOperator extends AbstractGeneticOperator {

	private GAPopulation lastPopulation;
	private GuidedBitMutator bmOperator = new GuidedBitMutator(),
			bmLocation = new GuidedBitMutator(),
			bmAttribute = new GuidedBitMutator();

	public GuidedHOFMutationOperator() {
		bmOperator.setLearningFactor(1);
		bmLocation.setLearningFactor(1);
		bmAttribute.setLearningFactor(1);
	}

	@Override
	public void apply(GAState state, GAPopulation source, GAPopulation destination, GALogger logger, boolean mutateAttribute, boolean createTree) {
		if (source != lastPopulation) {
			updateBitMutators(state, source);
		}

		final GAIndividual srcIndividual = getSelector().select(source).denormalize(state.getAnalysisResults());
		final AnalysisResults analysisResults = state.getAnalysisResults();
		final GAIndividual dstIndividual = new GAIndividual(
			Math.max(1,
					bmOperator.mutate(srcIndividual.getThOperator(1).intValue(), prng) % analysisResults.getMaxOp().intValue()),
			Math.max(1,
					bmLocation.mutate(srcIndividual.getThLocation(1).intValue(), prng) % analysisResults.getMaxLoc().intValue()),
			Math.max(1,
					bmAttribute.mutate(srcIndividual.getThAttribute(1).intValue(), prng) % analysisResults.getMaxAtt().intValue())
		);

		logger.appliedGeneticOperator(state, GuidedMutationOperator.class.getName(),
				Arrays.asList(srcIndividual), Arrays.asList(dstIndividual));
		destination.addIndividual(dstIndividual.denormalize(analysisResults));
	}

	private void updateBitMutators(GAState state, GAPopulation source) {
		List<Integer> desirableOperators = new ArrayList<>();
		List<Integer> desirableLocations = new ArrayList<>();
		List<Integer> desirableAttributes = new ArrayList<>();

		for (Entry<GAIndividual, ComparisonResults> entry : state.getHof().entrySet()) {
			if (entry.getValue().isAlive()) {
				desirableOperators.add(entry.getKey().getThOperator(1).intValue());
				desirableLocations.add(entry.getKey().getThLocation(1).intValue());
				desirableAttributes.add(entry.getKey().getThAttribute(1).intValue());
			}
		}

		bmOperator.update(desirableOperators.toArray(new Integer[desirableOperators.size()]));
		bmLocation.update(desirableLocations.toArray(new Integer[desirableLocations.size()]));
		bmAttribute.update(desirableAttributes.toArray(new Integer[desirableAttributes.size()]));
	}

	public double getSamplingRate() {
		return bmOperator.getSamplingRate();
	}

	public void setSamplingRate(double x) {
		bmOperator.setSamplingRate(x);
		bmLocation.setSamplingRate(x);
		bmAttribute.setSamplingRate(x);
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		super.validate();
		if (getSamplingRate() < 0 || getSamplingRate() > 1) {
			throw new InvalidConfigurationException("Sampling rate must be within [0, 1]");
		}
	}
}
