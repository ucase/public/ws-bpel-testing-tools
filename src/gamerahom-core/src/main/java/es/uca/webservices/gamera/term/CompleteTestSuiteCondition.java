package es.uca.webservices.gamera.term;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionType;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.term.GATerminationCondition;
import es.uca.webservices.gamera.record.ComparisonSimulationRecord;
import es.uca.webservices.gamera.record.SimulationRecord;

/**
 * Termination condition which is true when the minimal test suite needs
 * a number of test cases.
 *
 * @author Pedro Delgado-Pérez
 * @version 1.0
 */
@Option(description="Termination condition which is true when obtained a number of " +
		"test cases in the minimal test suite.")
public class CompleteTestSuiteCondition implements GATerminationCondition {

    private double percentTests;
    private int tests;
	private File fileMutants1 = null;
	private File fileMutants2 = null;
    private String minimalTestSuiteDirectory=null;
	private SimulationRecord simRecord1, simRecord2;
	private File fSimRecord1, fSimRecord2;
	private Boolean fileMutants2_used = false;
	
	private Set<GAIndividual> mutantsGenerated;
    
	public double getPercentTests() {
		return percentTests;
	}

	public void setPercentTests(double percentTests) {
		this.percentTests = percentTests;
	}
	
    public void setTests(int tests) {
        this.tests = tests;
    }

    public int getTests() {
        return tests;
    }
    
	@Option(type=OptionType.PATH_LOAD, fileExtensions={"txt"})
	public void setFileMutants1(String fileMutants) {
		this.fileMutants1 = fileMutants != null ? new File(fileMutants) : null;
	}
	
	@Option(type=OptionType.PATH_LOAD, fileExtensions={"txt"})
	public void setFileMutants2(String fileMutants) {
		this.fileMutants2 = fileMutants != null ? new File(fileMutants) : null;
	}
    
	public void setMinimalTestSuiteDirectory(String minimalTestSuiteDirectory){
		this.minimalTestSuiteDirectory=minimalTestSuiteDirectory;
	}
	
	public String getMinimalTestSuiteDirectory(){
		return minimalTestSuiteDirectory;
	}
	
	public File getfSimRecord1() {
		return fSimRecord1;
	}

	public void setfSimRecord1(File fSimRecord1) throws Exception {
		this.fSimRecord1 = fSimRecord1;
		setSimRecord1(SimulationRecord.loadFrom(fSimRecord1));
	}
	
	public SimulationRecord getSimRecord1() {
		return simRecord1;
	}

	public void setSimRecord1(SimulationRecord simRecord1) {
		this.simRecord1 = simRecord1;
	}

	public File getfSimRecord2() {
		return fSimRecord2;
	}

	public void setfSimRecord2(File fSimRecord2) throws Exception {
		this.fSimRecord2 = fSimRecord2;
		setSimRecord2(SimulationRecord.loadFrom(fSimRecord2));
	}

	public SimulationRecord getSimRecord2() {
		return simRecord2;
	}

	public void setSimRecord2(SimulationRecord simRecord2) {
		this.simRecord2 = simRecord2;
	}

	
	public Set<GAIndividual> getMutantsGenerated() {
		return mutantsGenerated;
	}

	public void setMutantsGenerated(Set<GAIndividual> mutantsGenerated) {
		this.mutantsGenerated = mutantsGenerated;
	}

    @Override
    public boolean evaluate(GAState state) {
    	String line = null;
    	
		try{
			//Individuos generados hasta el momento
			mutantsGenerated = state.getHof().keySet();
			
		    //Vamos a enviar los mutantes generados a un fichero externo; 
			PrintStream ps = new PrintStream(new File(fileMutants1.toString()));
		    ps.flush();
		    for (Iterator<GAIndividual> mg = mutantsGenerated.iterator(); mg.hasNext(); ) {
		    	//Tengo el individuo
				final GAIndividual sm = mg.next();
				
				//Ahora tengo que sacar el nombre del mutante del simulation record
				if (simRecord1.getComparisonRecords().containsKey(sm)) {
					ps.println("m" + sm.getOperator() + "_" + sm.getLocation() + "_" + sm.getAttribute());
				}
			}
		    
			ps.close();
			
			//La primera vez, también tenemos que generar la matriz completa
			if(!fileMutants2_used){
				PrintStream ps2 = new PrintStream(new File(fileMutants2.toString()));
			    ps2.flush();
			    
			    final Set<Entry<GAIndividual, ComparisonSimulationRecord>> entry = simRecord2.getComparisonRecords().entrySet();
			    
			    for(Entry<GAIndividual, ComparisonSimulationRecord> x : entry){
			   
			    	//Tengo el individuo, así que mostramos el nombre Y SU RESULTADO -> ESTAMOS CONSTRUYENDO LA MATRIZ DE EJECUCIÓN
					final GAIndividual sm = x.getKey();
					ps2.print("m" + sm.getOperator() + "_" + sm.getLocation() + "_" + sm.getAttribute());
					
					Integer[] results = simRecord2.getComparisonRecords().get(sm).getRow();
					for(Integer i : results){
						ps2.print(" " + i.toString());
					}
					ps2.println();
				}
			    
				ps2.close();
				fileMutants2_used = true;
				
				//Calculamos el número de tests mínimo que necesitamos para esta matriz
				ProcessBuilder pb = new ProcessBuilder();
				List<String> args = new ArrayList<String>();
				args.add("./minimalTestSuiteComplete.sh");
				pb.command(args).directory(new File(minimalTestSuiteDirectory));
				Process p = pb.start();
				p.waitFor();
				InputStreamReader tempReader = new InputStreamReader(new BufferedInputStream(p.getInputStream()));
				BufferedReader reader = new BufferedReader(tempReader);
			
				//En la línea que lea tiene que devolver el número de tests en el test suite
				line = reader.readLine();
				
				//Calculamos el número de tests necesarios
				tests = (int) (Integer.parseInt(line) * getPercentTests());
				PrintStream psResult = System.out;
				psResult.println(String.format("El número de tests necesarios en el conjunto completo es: %s, y el necesario con porcentaje %s es: %s", line, String.valueOf(getPercentTests()), String.valueOf(tests)));
			}
			
			ProcessBuilder pb = new ProcessBuilder();
			List<String> args = new ArrayList<String>();
			//Pasamos el script
			args.add("./minimalTestSuite.sh");
			pb.command(args).directory(new File(minimalTestSuiteDirectory));
			Process p = pb.start();
			p.waitFor();
			InputStreamReader tempReader = new InputStreamReader(new BufferedInputStream(p.getInputStream()));
			BufferedReader reader = new BufferedReader(tempReader);
		
			//En la línea que lea tiene que devolver el número de tests en el test suite
			line = reader.readLine();
			
			//NUEVO: Vamos a enviar los mutantes generados a un fichero externo; 
			PrintStream psResult = System.out;
			psResult.println(String.format("El número de tests en el conjunto adecuado y mínimo es: %s", line));
			
			
		}catch(Exception e){
			try {
				throw new AnalysisException(e);
			} catch (AnalysisException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return Integer.parseInt(line) >= tests;
    }

    @Override
    public void validate() throws InvalidConfigurationException {
        if (getPercentTests() <= 0) {
            throw new InvalidConfigurationException(
                    "The percentage of test cases must be greater than 0");
        }
    }

}
