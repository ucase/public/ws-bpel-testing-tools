package es.uca.webservices.gamera.log;

import es.uca.webservices.gamera.api.annotations.Option;

/**
 * Dummy logger which does not do anything.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */

@Option(hidden=true)
public class NullLogger extends AbstractLogger {
	// do nothing
}