package es.uca.webservices.gamera.genetic;

import java.math.BigInteger;
import java.util.Arrays;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.api.util.Pair;

/**
 * Implements a crossover operator which generates two individuals from the
 * values of its parents according to a randomly crossover point.
 * 
 * @author Antonio García-Domínguez, Juan José Domínguez-Jiménez,
 *         Emma Blanco-Muñoz
 * @version 1.1
 */

@Option(description="Implements a crossover operator which generates " +
		"two individuals from the values of its parents according to a " +
		"randomly crossover point.")
public class OrderCrossoverOperator extends AbstractGeneticOperator {
	private static final int CROSS_OPERATOR = 1;
	private static final int CROSS_LOCATION = CROSS_OPERATOR + 1;
	private static final int CROSS_ATTRIBUTE = CROSS_LOCATION + 1;

	@Override
	public void apply(GAState state, GAPopulation source, GAPopulation destination, GALogger logger, boolean mutateAttribute, boolean createTree) {
		// Select two different individuals with the selector defined in the configuration
		final GAIndividual dad = getSelector().select(source).denormalize(state.getAnalysisResults());
		GAIndividual mom = getSelector().select(source).denormalize(state.getAnalysisResults());
		
        // Now, fields to be crossed are chosen by a crossover point. [1, 2]
		// If this value was 3 (the last field), the children would be equals to their parents.
		final int crossoverPoint;
		if(mutateAttribute)
			crossoverPoint = CROSS_OPERATOR + prng.nextInt(CROSS_LOCATION - CROSS_OPERATOR + 1);
		else
			crossoverPoint = CROSS_OPERATOR;
		
		final Pair<GAIndividual, GAIndividual> children = doCrossover(dad, mom, crossoverPoint);

		logger.appliedGeneticOperator(state, OrderCrossoverOperator.class.getName(),
				Arrays.asList(dad, mom), Arrays.asList(children.getLeft(), children.getRight()));
		
		// Add the two individuals modified by the order crossover operator
		destination.addIndividual(children.getLeft());
		destination.addIndividual(children.getRight());
	}

	Pair<GAIndividual, GAIndividual> doCrossover(final GAIndividual dad, final GAIndividual mom, final int crossoverPoint) {
		// The crossover point can't be the value 3 (field attribute) because the children would be the same as their parents
		if(crossoverPoint < 1 || crossoverPoint >= CROSS_ATTRIBUTE) {
			throw new IllegalArgumentException(
					"The crossover point selected should be greater than zero and "
                     + "lower than the number of fields of an individual");
         }
		// We assume that all arrays have the same length, and that it is equal to the maximum order defined in the configuration.
		final BigInteger[] operatorsDad  = (BigInteger[])dad.getOperator().toArray();
		final BigInteger[] locationsDad  = (BigInteger[])dad.getLocation().toArray();
		final BigInteger[] attributesDad = (BigInteger[])dad.getAttribute().toArray();

		final BigInteger[] operatorsMom  = (BigInteger[])mom.getOperator().toArray();
		final BigInteger[] locationsMom  = (BigInteger[])mom.getLocation().toArray();
		final BigInteger[] attributesMom = (BigInteger[])mom.getAttribute().toArray();

		// The fields to modify are the fields from the crossover point
        switch(crossoverPoint) {
            case CROSS_OPERATOR:
            	swap(locationsDad, locationsMom);
            	swap(attributesDad, attributesMom);
                break;
            case CROSS_LOCATION:
            	swap(attributesDad, attributesMom);
                break;
            default:
                break;
        }

		return new Pair<GAIndividual, GAIndividual>(
			new GAIndividual(operatorsDad, locationsDad, attributesDad, dad.getOrder()),
			new GAIndividual(operatorsMom, locationsMom, attributesMom, mom.getOrder())
		);
	}

	private void swap(final BigInteger[] left, final BigInteger[] right) {
		for (int pos = 0; pos < left.length; pos++) {
			final BigInteger tmp = left[pos];
			left[pos] = right[pos];
			right[pos] = tmp;
		}
	}
}
