package es.uca.webservices.gamera.fitness;

import es.uca.webservices.gamera.api.GAFitnessFunction;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;

/**
 * Dummy fitness function that always returns zero.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
public class ZeroFitnessFunction implements GAFitnessFunction {

	@Override
	public double computeFitness(GAIndividual i, GAState state) {
		return 0;
	}

}
