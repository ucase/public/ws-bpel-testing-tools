package es.uca.webservices.gamera.subcommands;

import java.io.File;
import java.io.FileWriter;

import org.yaml.snakeyaml.Yaml;

import es.uca.webservices.gamera.record.MutantSubsumptionRecord;
import es.uca.webservices.gamera.record.SimulationRecord;
import es.uca.webservices.gamera.record.SimulationRecordRepresenter;

/**
 * Subcommand which computes a set of subsuming mutants for a given simulation record,
 * using the greedy algorithm from Papadakis.
 */
public class SubsumingMutantsSubcommand extends AbstractSubcommand {

	private static final String DESCRIPTION =
		"This command reads a simulation record in YAML format and produces a YAML file with a set of subsuming mutants."
		;

    public SubsumingMutantsSubcommand() {
        super(2, 2, "subsuming", "simrecord.yaml subsumption.yaml", DESCRIPTION);
    }

    @Override
    protected void runCommand() throws Exception {
        final File recordSource = new File(getNonOptionArgs().get(0));
        final File targetFile = new File(getNonOptionArgs().get(1));
        
        final SimulationRecord record = SimulationRecord.loadFrom(recordSource);
        final MutantSubsumptionRecord mutSubRecord = record.computeSubsumingMutants();

        Yaml yaml = new Yaml(new SimulationRecordRepresenter(record.getAnalysisRecord().getOperatorNames()));
        try (FileWriter fw = new FileWriter(targetFile)) {
        	yaml.dump(mutSubRecord, fw);
        }
    }
    
}
