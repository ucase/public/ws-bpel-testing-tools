package es.uca.webservices.gamera.fitness;

import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAFitnessFunction;
import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;

/**
 * <p>
 * Fitness function based on how specific the tests need to be to kill a certain
 * mutant. If there are M mutants and T test cases, surviving mutants have M*T
 * fitness. From this perfect score, we subtract the number of mutants killed by
 * each test case that kills this mutant.
 * </p>
 * 
 * <p>
 * The fitness of the individual is computed according to the current hall of
 * fame with the history of the previous generations. More formally, the fitness
 * of the individual is defined through the following formula, after discarding
 * all invalid mutants:
 * </p>
 * 
 * <pre>
 * f(I) = M · T - Sum {j = 1..T} (m_Ij · Sum{i = 1..M} (m_ij) )
 * </pre>
 * 
 * <p>
 * where M is the number of mutants in a generation, T represents the cases
 * number of the test suite and m shows the execution row of an individual I:
 * m_ij is 0 if the mutant survived or 1 if the mutant was killed.
 * </p>
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
public class MutantStrengthFitnessFunction implements GAFitnessFunction {

	private int coverageModifierFactor = -1;

	@Override
	public double computeFitness(GAIndividual ind, GAState state) {
		final GAHof hof = state.getHof();
		final ComparisonResults results =  hof.get(ind);
		return computeFitness(results.getRow(), results.getRow().length, 
				(int) (state.getSizePopulation() + hof.getValidMutants()), state.getSumExecutionMatrix());
	}

	/**
	 * Convenience version of {@link #computeFitness(GAIndividual, GAState)}
	 * that does not require building a complete state.
	 */
	public double computeFitness(final ComparisonResult[] individualCurrentRow, final Integer t, final Integer m, final double[] sumExecutionMatrix) {
		if (individualCurrentRow[0].isValid()) {	
			// M is the size of the current population and the size of the hof with the new individuals added
			// T is the total number of test in the test suite
			double fitness = m * t;
			for (int iTestCase = 0; iTestCase < t; iTestCase++) {
				// If the mutant was executed, we consider it to be "half-dead" (this is only possible
				// if the executor reports covered mutations)
				final ComparisonResult result = individualCurrentRow[iTestCase];
				final double columnSum = sumExecutionMatrix[iTestCase];
				final double modifier = result.isDead() ? -columnSum : (result.isExecuted() ? coverageModifierFactor * columnSum/2 : 0);

				fitness = fitness + modifier;
			}

			// Fitness cannot be negative: if it is negative due to the impact of the coverage modifier factor, clip it to zero
			return Math.max(0, fitness);
		} else {
			// Invalid individual: fitness is always zero
			return 0;
		}
	}

	public int getCoverageModifierFactor() {
		return coverageModifierFactor;
	}

	public void setCoverageModifierFactor(int coverageModifierFactor) {
		this.coverageModifierFactor = coverageModifierFactor;
	}

}
