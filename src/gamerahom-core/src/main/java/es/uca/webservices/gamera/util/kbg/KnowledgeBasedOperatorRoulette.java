package es.uca.webservices.gamera.util.kbg;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;

/**
 * Scores operators based on the estimated probability of
 * having strong mutants, based on previously run programs
 * and the currently ongoing search.
 */
public class KnowledgeBasedOperatorRoulette {

	private long scoredGeneration = -1;
	private double[] operatorScores;
	private double operatorScoresSum;
	private Random prng;
	private double rewardScale = 1;

    private Map<String, Double> ratios = new HashMap<>();
    
    public KnowledgeBasedOperatorRoulette(Map<String, Double> ratios) {
    	this.ratios = ratios;
    }

    public BigInteger chooseOperator(GAState state) {
		if (scoredGeneration != state.getCurrentGeneration()) {
			scoredGeneration = state.getCurrentGeneration();
			operatorScores = new double[state.getAnalysisResults().getMaxOp().intValue()];
			operatorScoresSum = 0;

			for (int i = 0; i < operatorScores.length; i++) {
				operatorScores[i] = scoreOperator(state, state.getAnalysisResults().getFilteredOper()[i].intValue());
				operatorScoresSum += operatorScores[i];
			}
		}
		
		double rouletteTurn = operatorScoresSum * prng.nextDouble();
		for (int i = 0; i < operatorScores.length; i++) {
			if (rouletteTurn < operatorScores[i]) {
				BigInteger bi = BigInteger.valueOf(i + 1);
				return bi;
			} else {
				rouletteTurn -= operatorScores[i];
			}
		}

		throw new IllegalStateException("The roulette turn should have found one match");
    }

	private double scoreOperator(GAState state, int oneBasedOperator) {
		final AnalysisResults analysis = state.getAnalysisResults();
		
		/*
		 * Formula: 100/nOperators + nMutants(operator) * knownRatio +
		 * nMutants(operator) * observedRatio,
		 * 
		 * Simplified to 100/nOperators + nMutants(operator) * (knownRatio +
		 * observedRatio).
		 */

		// first factor of the formula
		final BigInteger nOperators = analysis.getMaxOp();
		final BigDecimal baseFactor = new BigDecimal("100").divide(new BigDecimal(nOperators), RoundingMode.DOWN); 

		// second factor, left side
		final BigInteger nLocations = analysis.getLocationCounts()[oneBasedOperator - 1];
		final BigInteger fieldRange = analysis.getFieldRanges()[oneBasedOperator - 1];
		final BigInteger nMutantsOperator = nLocations.multiply(fieldRange);

		// second factor, right side
		final String operatorName = analysis.getOperatorNames()[oneBasedOperator - 1];
		final BigDecimal knownRatio = new BigDecimal(ratios.get(operatorName));

		int knownAlive = 0;
		for (Entry<GAIndividual, ComparisonResults> entry : state.getHof().entrySet()) {
			if (entry.getKey().getThOperator(1).intValue() == oneBasedOperator && entry.getValue().isAlive()) {
				++knownAlive;
			}
		}
		final BigDecimal observedRatio =
			nMutantsOperator.compareTo(BigInteger.ZERO) > 0
			? new BigDecimal(knownAlive).divide(new BigDecimal(nMutantsOperator), RoundingMode.DOWN) : BigDecimal.ZERO;

		// second factor
		final BigDecimal rewardFactor = new BigDecimal(nMutantsOperator)
			.multiply(knownRatio.add(observedRatio)).multiply(new BigDecimal(rewardScale));
		
		// total score for mutation operator
		final BigDecimal totalScore = baseFactor.add(rewardFactor);

		return totalScore.doubleValue();
	}

	public Random getPrng() {
		return prng;
	}

	public Map<String, Double> getRatios() {
		return ratios;
	}

	public void setPrng(Random prng) {
		this.prng = prng;
	}

	public double getRewardScale() {
		return rewardScale;
	}

	public void setRewardScale(double rewardScale) {
		this.rewardScale = rewardScale;
	}
}
