package es.uca.webservices.gamera.subcommands;

/**
 * Interface for a supported subcommand.
 * 
 * @author Antonio García-Domínguez
 */
public interface ISubcommand {

	/**
	 * Returns the name of the subcommand.
	 */
	String getName();

	/**
	 * Parses the arguments given in the command line. If the wrong options are
	 * used, not enough arguments are given, or the user requests it, it will
	 * print help about the subcommand and its usage. Otherwise, it will run the
	 * subcommand.
	 * 
	 * @throws IllegalArgumentException
	 *             Wrong number of non-option arguments, unknown option, or
	 *             wrong value used as argument to some option.
	 */
	void parseArgs(String... args);

	/**
	 * Runs the subcommand. If the user has requested help, it will be printed
	 * to stderr. This method should not be overridden.
	 * 
	 * @throws Exception
	 *             There was a problem running the subcommand.
	 */
	void run() throws Exception;

	/**
	 * Returns a quick summary of the basic usage of this command.
	 */
	String getUsage();

	/**
	 * Returns a long description of this command.
	 */
	String getDescription();

}