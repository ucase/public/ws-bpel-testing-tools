package es.uca.webservices.gamera.genetic;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.log.GALogger;

/**
 * Implements a mutation operator which modifies the operator, location or
 * attribute of a mutant in the provided population. 
 *
 * @author Antonio García-Domínguez, Juan José Domínguez-Jiménez, Emma Blanco-Muñoz
 * @version 1.2
 */

@Option(description="Implements a mutation operator which modifies the operator, location or " +
		"attribute of a mutant in the provided population.")
public class IndividualMutationOperator extends AbstractMutationOperator {
	protected static final int MUT_OPERATOR = 1;
	protected static final int MUT_LOCATION = 2;
	protected static final int MUT_ATTRIBUTE = 3;

	private boolean clip = false;
	private boolean useOperatorSteps = false;

	/**
	 * Indicates whether the mutation range is interpreted for the location and
	 * attribute fields as units of the denormalized space (<code>false</code>),
	 * or as units of the normalized space of the operator of the individual (
	 * <code>true</code>). By default, this field is set to <code>false</code>.
	 */
	public boolean isUseOperatorSteps() {
		return useOperatorSteps;
	}

	/**
	 * Returns whether the denormalized space (<code>false</code>) or the
	 * normalized space of the operator of the individual (<code>true</code>)
	 * will be used to mutate the location and attribute fields.
	 */
	public void setUseOperatorSteps(boolean useOperatorSteps) {
		this.useOperatorSteps = useOperatorSteps;
	}

	/**
	 * Indicates whether clipping (<code>true</code>) or modulo arithmetic (
	 * <code>false</code>) will be used to bring the mutated values back into
	 * the valid range.
	 */
	public boolean isClip() {
		return clip;
	}

	/**
	 * Sets whether clipping (<code>true</code>) or modulo arithmetic (
	 * <code>false</code>) will be used to bring the mutated values back into
	 * the valid range.
	 */
	public void setClip(boolean clip) {
		this.clip = clip;
	}

	@Override
    public void apply(GAState state, GAPopulation source, GAPopulation destination, GALogger logger, boolean mutateAttribute, boolean createTree) {
        // We have to choose the mutant to be modified randomly, but taking into
        // account the order of the individual. [1, order]
    	final GAIndividual individual = getSelector().select(source).denormalize(state.getAnalysisResults());
        final int mutant = 1 + prng.nextInt(individual.getOrder());

        // Now, the value to be modified has to be chosen randomly too. [1, 3]
		final int mutatedField;
		if(mutateAttribute)
			mutatedField = MUT_OPERATOR + prng.nextInt(MUT_ATTRIBUTE - MUT_OPERATOR + 1);
		else
			mutatedField = MUT_OPERATOR + prng.nextInt(MUT_LOCATION - MUT_OPERATOR + 1);

        final GAIndividual child = doMutation(state.getAnalysisResults(), individual, mutant, mutatedField);

        logger.appliedGeneticOperator(state, IndividualMutationOperator.class.getName(),
				Arrays.asList(individual), Arrays.asList(child));
		
        destination.addIndividual(child);
        
        //NUEVO: asociar mutante generado con su ancestro
    /*    GAIndividual normalizedChild = child.normalize(state.getAnalysisResults());
        String childName = "m" + state.normalizeCode(normalizedChild.getOperator().get(0)) + "_" + normalizedChild.getLocation().get(0) + "_" + normalizedChild.getAttribute().get(0);*/
        if(createTree){        
	        if(!state.isMutantAlreadyInTree(child)){
	        	List<GAIndividual> ancs = new ArrayList<GAIndividual>();
	        	ancs.add(individual);
	        	state.addMutantToTree(child, ancs);
	        }
        }
    }

	GAIndividual doMutation(AnalysisResults analysis, final GAIndividual individual, final int mutant, final int mutatedField) {
		checkValidParameters(individual, mutant, mutatedField);
		// Denormalized value to be mutated
		BigInteger currentValue; 
		// Maximum denormalized value for the field to be mutated (minimum value is fixed to 1)
		BigInteger maxValue;     
		// Maximum mutation (normalized if isUseOperatorSteps() is true, denormalized otherwise)
		BigInteger maxDelta;    
		
        switch(mutatedField) {
            case MUT_OPERATOR: 
            	currentValue = individual.getThOperator(mutant);
            	maxValue = maxDelta = analysis.getMaxOp();
                break;
            case MUT_LOCATION: 
            	currentValue = individual.getThLocation(mutant);
            	maxValue = maxDelta = analysis.getMaxLoc();
            	if (isUseOperatorSteps()) {
            		final BigInteger thOperator = individual.getThOperator(mutant);
					maxDelta = analysis.getFilteredLoc()[thOperator.intValue() - 1];
            	}
                break;
            default:
            case MUT_ATTRIBUTE:
            	currentValue = individual.getThAttribute(mutant);
        		maxValue = maxDelta = analysis.getMaxAtt();
            	if (isUseOperatorSteps()) {
            		final BigInteger thOperator = individual.getThOperator(mutant);
					maxDelta = analysis.getFilteredAtt()[thOperator.intValue() - 1];
            	}
                break;
        }

        BigInteger validNewValue = computeMutatedValue(mutatedField, currentValue, maxValue, maxDelta);

        // Create the mutated GAIndividual
        final BigInteger[] operator  = (BigInteger[])individual.getOperator().toArray();
        final BigInteger[] location  = (BigInteger[])individual.getLocation().toArray();
        final BigInteger[] attribute = (BigInteger[])individual.getAttribute().toArray();

        // If the new calculated value is greater than the maximum, the new
		// order will be the maximum allowed.
        switch(mutatedField) {
        	case MUT_OPERATOR:
        		operator[mutant - 1] = validNewValue;
                break;
            case MUT_LOCATION:
        		location[mutant - 1] = validNewValue;
                break;
            case MUT_ATTRIBUTE:
        		attribute[mutant - 1] = validNewValue;
                break;
            default:
            	break;
        }

        return new GAIndividual(operator, location, attribute, individual.getOrder());
	}

	protected BigInteger computeMutatedValue(int mutatedField, BigInteger currentValue, BigInteger maxValue, BigInteger maxDelta) {
        // To calculate the new value we use the next rule:
        // v = currentValue + rand(-randomRange, randomRange) * (1 - scaleFactor)
        //   = currentValue + rand(-1, 1) * randomRange * (1 - scaleFactor)

        // Compute the range using a fixed value (randomRange) or a percentage of the maximum delta (randomRangePercent) and the scaleFactor
        BigDecimal range;
        if (getRandomRange() != null) {
            range = new BigDecimal(getRandomRange() * (1 - getScaleFactor())).max(BigDecimal.ONE);
        }
        else {
            range = new BigDecimal(maxDelta).multiply(new BigDecimal(getRandomRangePercent() * (1 - getScaleFactor()))).max(BigDecimal.ONE);
        }

        // The change should be between -range and +range
        final double rangePos = prng.nextDouble() * 2 - 1;
        BigInteger delta = range.multiply(new BigDecimal(rangePos)).toBigInteger();
        if (isUseOperatorSteps()) {
            // Denormalize the delta value
            delta = delta.multiply(maxValue).divide(maxDelta);
        }

        // Add the delta to the new value and ensure it is within the valid range
        final BigInteger newValue = currentValue.add(delta);
        BigInteger validNewValue;
        if (!isClip()) {
            validNewValue = newValue.mod(maxValue).add(BigInteger.ONE);
        } else {
            validNewValue = newValue.min(maxValue).max(BigInteger.ONE);
        }

        return validNewValue;
	}

	private void checkValidParameters(final GAIndividual individual,
			final int mutant, final int mutatedField) {
		if(mutant < 1 || mutant > individual.getMaxOrder()) {
			throw new IllegalArgumentException(
					"The mutant selected should be greater than zero"
                     + " and lower than the maximum order");
        }
		
		if(mutatedField < MUT_OPERATOR || mutatedField > MUT_ATTRIBUTE) {
			throw new IllegalArgumentException(
					"The mutated field should be 1 (operator), 2 (location) " +
					"or 3 (attribute)");
        }
	}

}
