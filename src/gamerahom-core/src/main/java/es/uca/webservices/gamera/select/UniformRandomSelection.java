package es.uca.webservices.gamera.select;

import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.select.GASelectionOperator;

/**
 * Simple selection operator which picks an individual at random
 * using an uniform probability distribution.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */

@Option(description="Simple selection operator which picks an individual at random " +
		"using an uniform probability distribution.")
public class UniformRandomSelection implements GASelectionOperator {

	private Random prng;

	@Override
	public GAIndividual select(GAPopulation source) {
		final List<GAIndividual> individuals = source.getIndividuals();
		final int nIndividuals = individuals.size();
		final int rndIndividualIndex = prng.nextInt(nIndividuals);
		return individuals.get(rndIndividualIndex);
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (prng == null) {
			throw new InvalidConfigurationException("No PRNG has been set");
		}
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}

}
