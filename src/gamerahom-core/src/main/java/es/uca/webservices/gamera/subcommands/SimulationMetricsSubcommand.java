package es.uca.webservices.gamera.subcommands;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.record.ComparisonSimulationRecord;
import es.uca.webservices.gamera.record.SimulationRecord;

/**
 * Subcommand which reads a simulation record produced by
 * {@link SimulationRunSubcommand} and produces various metrics.
 * 
 * @author Antonio García-Domínguez
 */
public class SimulationMetricsSubcommand extends AbstractSubcommand {

	private static class Column<K> {
		private final String description;
		private final Map<K, Integer> values;

		public Column(String desc, Map<K, Integer> values) {
			this.description = desc;
			this.values = values;
		}

		public String getDescription() {
			return description;
		}

		public Integer getValue(K key) {
			return values.get(key);
		}

		public int getTotal() {
			int total = 0;
			for (Integer x : values.values()) {
				if (x != null) {
					total += x;
				}
			}
			return total;
		}
	}

	private static final String DESCRIPTION =
		"This command reads a simulation record in YAML format and produces various metrics (e.g. mutation score and mutant counts)."
		;

	public SimulationMetricsSubcommand() {
		super(1, 1, "simmetrics", "simrecord.yaml", DESCRIPTION);
	}

	@Override
	protected void runCommand() throws Exception {
		final File recordSource = new File(getNonOptionArgs().get(0));
		final SimulationRecord simRecord = SimulationRecord.loadFrom(recordSource);

		final Entry<GAIndividual, ComparisonSimulationRecord> firstEntry = simRecord.getComparisonRecords().entrySet().iterator().next();
		final int nTestCases = firstEntry.getValue().getRow().length;
		final String[] operators = simRecord.getAnalysisRecord().getOperatorNames();

		final Set<GAIndividual> alive = new HashSet<>();
		final Set<GAIndividual> killed = new HashSet<>();
		final Set<GAIndividual> invalid = new HashSet<>();
		simRecord.getMutants(alive, killed, invalid);

		final Map<String, Integer> invalidByOperator = countByOperator(operators, invalid);
		final Map<String, Integer> aliveByOperator = countByOperator(operators, alive);
		final Map<String, Integer> killedByOperator = countByOperator(operators, killed);

		final int nAlive = alive.size();
		final int nKilled = killed.size();
		final int nValid  = nAlive + nKilled;
		final double mutationScore = (double)nKilled/nValid;

		System.out.println(String.format("Estimated mutation score: (%d - %d)/%d = %d/%d = %g", nValid, nAlive, nValid, nKilled, nValid, mutationScore));
		System.out.println("Test cases: " + nTestCases);
		System.out.println();

		printTable("All mutants", "Operator", Arrays.asList(operators),
				new Column<>("Alive", aliveByOperator),
				new Column<>("Killed", killedByOperator),
				new Column<>("Invalid", invalidByOperator));
		System.out.println();

		final Set<GAIndividual> potEquiv = new HashSet<>();
		final Set<GAIndividual> hardToKill = new HashSet<>();
		simRecord.getStrongMutants(potEquiv, hardToKill);
		final Map<String, Integer> potEquivByOperator = countByOperator(operators, potEquiv);
		final Map<String, Integer> hardToKillByOperator = countByOperator(operators, hardToKill);

		printTable("Strong mutants", "Operator", Arrays.asList(operators),
				new Column<>("PotEquiv", potEquivByOperator),
				new Column<>("HardToKill", hardToKillByOperator));
	}

	@SafeVarargs
	private final <K> void printTable(String title, String keyColumnLabel, Iterable<K> operators, Column<K>... columns) {
		int colWidth = Math.max(keyColumnLabel.length(), "Total".length());
		for (K key : operators) {
			colWidth = Math.max(colWidth, key.toString().length());
			for (Column<K> col : columns) {
				colWidth = Math.max(colWidth, col.getDescription().length());
				colWidth = Math.max(colWidth, (col.getTotal() + "").length());
			}
		}
		final int titleWidth = colWidth * (columns.length + 1) + columns.length;
		final String cellFormat = "%" + colWidth + "s";

		final int spacing = (titleWidth - title.length())/2;
		printCharacter(spacing, ' ');
		System.out.print(title);
		System.out.println();
		printCharacter(titleWidth, '=');
		System.out.println();

		System.out.print(String.format(cellFormat, keyColumnLabel));
		for (Column<K> col : columns) {
			System.out.print(" ");
			System.out.print(String.format(cellFormat, col.getDescription()));
		}
		System.out.println();

		for (K key : operators) {
			System.out.print(String.format(cellFormat, key.toString()));
			for (Column<K> col : columns) {
				System.out.print(" ");
				System.out.print(String.format(cellFormat, col.getValue(key)));
			}
			System.out.println();
		}

		System.out.print(String.format(cellFormat, "Total"));
		for (Column<K> col : columns) {
			System.out.print(" ");
			System.out.print(String.format(cellFormat, col.getTotal()));
		}
		System.out.println();
	}

	private void printCharacter(final int count, final char c) {
		for (int i = 0; i < count; i++) {
			System.out.print(c);
		}
	}

	private Map<String, Integer> countByOperator(final String[] operators, final Set<GAIndividual> individuals) {
		final Map<String, Integer> counts = new LinkedHashMap<>();

		for (String op : operators) {
			counts.put(op, 0);
		}

		for (GAIndividual ind : individuals) {
			final String sOperator = operators[ind.getThOperator(1).intValue() - 1];
			counts.put(sOperator, counts.get(sOperator) + 1);
		}

		return counts;
	}
}
