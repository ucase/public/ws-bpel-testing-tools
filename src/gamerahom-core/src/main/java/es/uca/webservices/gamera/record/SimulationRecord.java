package es.uca.webservices.gamera.record;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.ValidIndividualsList;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.api.predicates.IIntegerPredicate;
import es.uca.webservices.gamera.cli.CLIRunner;
import es.uca.webservices.gamera.record.MutantSubsumptionRecord.SubsumingMutant;

/**
 * <p>
 * Stores all the timing, analysis and comparison information required to
 * simulate the execution of some {@link GAExecutor}.
 * </p>
 *
 * <p>
 * Instances can be created manually or using a {@link GAExecutor} through the
 * {@link #buildFrom(int, GAExecutor)} method. Later on, they can be saved to a
 * YAML file ({@link #saveToYAML(File)} or a text file using an ad-hoc format
 * that requires less memory to be read, but stores less information (
 * {@link #saveToRaw(File)}. Likewise, simulation records can be loaded from
 * both formats, through the respective {@link #loadFromYAML(File)} or
 * {@link #loadFromRaw(File)}.
 * </p>
 *
 * <p>
 * The class also provides facilities for performing in-place filtering of the
 * available test cases. This allows reusing the same simulation record with
 * several subsets of test cases.
 * </p>
 *
 * <p>
 * The class also provides methods for deciding which kind of serialization to
 * use depending on the filename extension: {@link #saveTo(File)} and
 * {@link #loadFrom(File)}.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */
public class SimulationRecord {
	
	private static final int MAGIC_NUMBER_HASH1 = 1231;
	private static final int MAGIC_NUMBER_HASH2 = 1237;
	private static final Logger LOGGER = LoggerFactory.getLogger(SimulationRecord.class);
	private static final BigInteger RATIO_NANOS_MILLIS = BigInteger.valueOf(1000000);

	/** Version number for the current format, in case we break backwards-compatibility.*/
	public static final int CURRENT_FORMAT_VERSION = 3;

	private int formatVersion = CURRENT_FORMAT_VERSION;

	// Software (name + version) used to generate the record
	private String recorder;

	// Generation timestamp
	private Date timestamp = new Date();

	// Executor used to run the tests
	private GAExecutor executor;

	// Times required to prepare, clean up and analyze
	private BigInteger prepareWallNanos = BigInteger.ZERO, cleanupWallNanos = BigInteger.ZERO;

	// Maximum order recorded
	private int maxOrder;

	/**
	 * Should missing individuals in the comparison records be considered
	 * invalid (<code>true</code>), or should we raise an exception (
	 * <code>false</code>)?
	 */
	private boolean missingIndividualsAreInvalid = false;

	// Time and results of the analysis
	private AnalysisSimulationRecord analysisRecord;

	// Times and results for each comparison
	private Map<GAIndividual, ComparisonSimulationRecord> comparisonRecords = new LinkedHashMap<GAIndividual, ComparisonSimulationRecord>();

	// Names of each test case, in the same order as listed in the comparison records (optional)
	private List<String> testCaseNames;

	public SimulationRecord() {
		// Empty ctor (for SnakeYAML)
	}

	public int getFormatVersion() {
		return formatVersion;
	}

	public void setFormatVersion(int formatVersion) {
		this.formatVersion = formatVersion;
	}

	public String getRecorder() {
		return recorder;
	}

	public void setRecorder(String recorder) {
		this.recorder = recorder;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public GAExecutor getExecutor() {
		return executor;
	}

	public void setExecutor(GAExecutor executor) {
		this.executor = executor;
	}

	public long getPrepareWallMillis() {
		return getPrepareWallNanos().divide(RATIO_NANOS_MILLIS).longValue(); 
	}

	public void setPrepareWallMillis(long wallMillis) {
		setPrepareWallNanos(BigInteger.valueOf(wallMillis).multiply(RATIO_NANOS_MILLIS));
	}

	public BigInteger getPrepareWallNanos() {
		return prepareWallNanos;
	}

	public void setPrepareWallNanos(BigInteger prepareWallNanos) {
		this.prepareWallNanos = prepareWallNanos;
	}

	public long getCleanupWallMillis() {
		return getCleanupWallNanos().divide(RATIO_NANOS_MILLIS).longValue();
	}

	public void setCleanupWallMillis(long wallMillis) {
		setCleanupWallNanos(BigInteger.valueOf(wallMillis).multiply(RATIO_NANOS_MILLIS));
	}

	public BigInteger getCleanupWallNanos() {
		return cleanupWallNanos;
	}

	public void setCleanupWallNanos(BigInteger cleanupWallNanos) {
		this.cleanupWallNanos = cleanupWallNanos;
	}

	public int getMaxOrder() {
		return maxOrder;
	}

	public void setMaxOrder(int maxOrder) {
		this.maxOrder = maxOrder;
	}

	/**
	 * Returns whether missing individuals in the comparison records be
	 * considered to have an INVALID row (<code>true</code>), or should we raise
	 * an exception ( <code>false</code>).
	 */
	public boolean getMissingIndividualsAreInvalid() {
		return missingIndividualsAreInvalid;
	}

	/**
	 * Changes whether missing individuals in the comparison records be
	 * considered to have an INVALID row (<code>true</code>), or should we raise
	 * an exception ( <code>false</code>).
	 */
	public void setMissingIndividualsAreInvalid(boolean missingIndividualsAreInvalid) {
		this.missingIndividualsAreInvalid = missingIndividualsAreInvalid;
	}

	public AnalysisSimulationRecord getAnalysisRecord() {
		return analysisRecord;
	}

	public void setAnalysisRecord(AnalysisSimulationRecord analysisRecord) {
		this.analysisRecord = analysisRecord;
	}

	public Map<GAIndividual, ComparisonSimulationRecord> getComparisonRecords() {
		return comparisonRecords;
	}

	public void setComparisonRecords(Map<GAIndividual, ComparisonSimulationRecord> comparisonRecords) {
		this.comparisonRecords = comparisonRecords;
	}

	public void addComparisonRecord(ComparisonResults results, BigInteger wallNanos) {
		comparisonRecords.put(results.getIndividual(), new ComparisonSimulationRecord(results, wallNanos));
	}

	public List<String> getTestCaseNames() {
		return testCaseNames;
	}

	public void setTestCaseNames(List<String> testNames) {
		this.testCaseNames = testNames;
	}

	/**
	 * Saves the simulation record to a file. The serialization format depends
	 * on the file extension: <code>.yaml</code> or <code>.yml</code> files
	 * (case insensitive) will use YAML serialization, and any other file will
	 * use the raw serialization. Raw serialization is more efficient for large
	 * records, but does not store all the information.
	 */
	public void saveTo(File f) throws IOException {
		if (isYAMLFile(f)) {
			saveToYAML(f);
		}
		else {
			saveToRaw(f);
		}
	}

	public void saveToRaw(File f) throws IOException {
		FileWriter fW = new FileWriter(f);
		try {
			PrintWriter pW = new PrintWriter(fW);
			for (RawField rf : RawField.values()) {
				rf.saveTo(this, pW);
			}
		} finally {
			fW.close();
		}
	}

	public void saveToYAML(File f) throws IOException {
		final FileWriter fW = new FileWriter(f);
		try {
			getYaml().dump(this, fW);
		} finally {
			fW.close();
		}
	}

	/**
	 * Loads the file using a serialization type dependent on its filename
	 * extension.
	 * @see #saveTo(File)
	 */
	public static SimulationRecord loadFrom(File f) throws Exception {
		if (isYAMLFile(f)) {
			return loadFromYAML(f);
		}
		else {
			return loadFromRaw(f);
		}
	}

	public static SimulationRecord loadFromRaw(File f) throws Exception {
		FileReader fR = new FileReader(f);
		try {
			final SimulationRecord record = new SimulationRecord();
			record.setAnalysisRecord(new AnalysisSimulationRecord());
			record.setComparisonRecords(new HashMap<GAIndividual, ComparisonSimulationRecord>());
			BufferedReader bR = new BufferedReader(fR);

			String line;
			while ((line = bR.readLine()) != null) {
				RawField.read(record, line, bR);
			}

			return record;
		} finally {
			try {
				fR.close();
			} catch (IOException e) {
				LOGGER.error("I/O exception while closing file", e);
			}
		}
	}

	public static SimulationRecord loadFromYAML(File f) throws FileNotFoundException {
		return (SimulationRecord) getYaml().load(new FileReader(f));
	}

	public static SimulationRecord buildFrom(final int maxOrder, final GAExecutor exec)
			throws InvalidConfigurationException, PreparationException, IOException, AnalysisException, ComparisonException
	{
		exec.validate();

		final SimulationRecord r = new SimulationRecord();
		r.setMaxOrder(maxOrder);
		r.setRecorder("GAmeraHOM v" + CLIRunner.getVersion());
		r.setExecutor(exec);

		{
			final long start = System.nanoTime();
			exec.prepare();
			r.setPrepareWallNanos(BigInteger.valueOf(System.nanoTime() - start));
			r.setTestCaseNames(exec.getTestCaseNames());
		}

		AnalysisResults analysis;
		{
			final long start = System.nanoTime();
			analysis = exec.analyze(null);
			r.setAnalysisRecord(new AnalysisSimulationRecord(analysis, BigInteger.valueOf(System.nanoTime() - start)));
		}

		for (GAIndividual ind : new ValidIndividualsList(analysis, maxOrder)) {
			final long start = System.nanoTime();
			final ComparisonResults results = exec.compare(null, ind)[0];
			r.addComparisonRecord(results, BigInteger.valueOf(System.nanoTime() - start));
		}

		{
			final long start = System.nanoTime();
			exec.cleanup();
			r.setCleanupWallNanos(BigInteger.valueOf(System.nanoTime() - start));
		}

		return r;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((analysisRecord == null) ? 0 : analysisRecord.hashCode());
		result = prime
				* result
				+ ((cleanupWallNanos == null) ? 0 : cleanupWallNanos.hashCode());
		result = prime
				* result
				+ ((comparisonRecords == null) ? 0 : comparisonRecords
						.hashCode());
		result = prime * result
				+ ((executor == null) ? 0 : executor.hashCode());
		result = prime * result + formatVersion;
		result = prime * result + maxOrder;
		result = prime * result + (missingIndividualsAreInvalid ? MAGIC_NUMBER_HASH1 : MAGIC_NUMBER_HASH2);
		result = prime
				* result
				+ ((prepareWallNanos == null) ? 0 : prepareWallNanos.hashCode());
		result = prime * result
				+ ((recorder == null) ? 0 : recorder.hashCode());
		result = prime * result
				+ ((testCaseNames == null) ? 0 : testCaseNames.hashCode());
		result = prime * result
				+ ((timestamp == null) ? 0 : timestamp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SimulationRecord other = (SimulationRecord) obj;
		if (analysisRecord == null) {
			if (other.analysisRecord != null) {
				return false;
			}
		} else if (!analysisRecord.equals(other.analysisRecord)) {
			return false;
		}
		if (cleanupWallNanos == null) {
			if (other.cleanupWallNanos != null) {
				return false;
			}
		} else if (!cleanupWallNanos.equals(other.cleanupWallNanos)) {
			return false;
		}
		if (comparisonRecords == null) {
			if (other.comparisonRecords != null) {
				return false;
			}
		} else if (!comparisonRecords.equals(other.comparisonRecords)) {
			return false;
		}
		if (executor == null) {
			if (other.executor != null) {
				return false;
			}
		} else if (!executor.equals(other.executor)) {
			return false;
		}
		if (formatVersion != other.formatVersion) {
			return false;
		}
		if (maxOrder != other.maxOrder) {
			return false;
		}
		if (missingIndividualsAreInvalid != other.missingIndividualsAreInvalid){
			return false;
		}
		if (prepareWallNanos == null) {
			if (other.prepareWallNanos != null) {
				return false;
			}
		} else if (!prepareWallNanos.equals(other.prepareWallNanos)) {
			return false;
		}
		if (recorder == null) {
			if (other.recorder != null) {
				return false;
			}
		} else if (!recorder.equals(other.recorder)) {
			return false;
		}
		if (testCaseNames == null) {
			if (other.testCaseNames != null) {
				return false;
			}
		} else if (!testCaseNames.equals(other.testCaseNames)) {
			return false;
		}
		if (timestamp == null) {
			if (other.timestamp != null) {
				return false;
			}
		} else if (!timestamp.equals(other.timestamp)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SimulationRecord [formatVersion="
				+ formatVersion + ", "
				+ (recorder != null ? "recorder=" + recorder + ", " : "")
				+ (timestamp != null ? "timestamp=" + timestamp + ", " : "")
				+ (executor != null ? "executor=" + executor + ", " : "")
				+ (prepareWallNanos != null ? "prepareWallNanos="
						+ prepareWallNanos + ", " : "")
				+ (cleanupWallNanos != null ? "cleanupWallNanos="
						+ cleanupWallNanos + ", " : "")
				+ "maxOrder=" + maxOrder
				+ ", missingIndividualsAreInvalid=" + missingIndividualsAreInvalid + ", "
				+ (analysisRecord != null ? "analysisRecord=" + analysisRecord + ", " : "")
				+ (comparisonRecords != null ? "comparisonRecords=" + comparisonRecords : "")
				+ "]";
	}

	public void filterOperators(IIntegerPredicate operatorFilter) {
		assert analysisRecord != null : "The analysis record should have been initialized";
		assert comparisonRecords != null : "The comparison records should have been initialized";

		final int nOperators = analysisRecord.getOperatorNames().length;
		for (int i = 0; i < nOperators; ++i) {
			if (!operatorFilter.evaluate(1 + i)) {
				analysisRecord.getLocationCounts()[i] = BigInteger.ZERO;
			}
		}

		final Iterator<Entry<GAIndividual, ComparisonSimulationRecord>> it = comparisonRecords.entrySet().iterator();
		outer:
		while (it.hasNext()) {
			final GAIndividual ind = it.next().getKey();
			for (int iOrder = 1; iOrder <= ind.getOrder(); ++iOrder) {
				if (!operatorFilter.evaluate(ind.getThOperator(iOrder).intValue())) {
					it.remove();
					continue outer;
				}
			}
		}
	}

	/**
	 * Performs an in-place filtering of all comparison records, removing those
	 * tests which aren't accepted by the provided filter. Tests are numbered
	 * starting at 1. Test times are subtracted from the total comparison times
	 * if available.
	 */
	public void filterTests(IIntegerPredicate filter) {
		final int nTests = getTestCaseCount();

		for (ComparisonSimulationRecord cr : comparisonRecords.values()) {
			final Integer[] oldRow = cr.getRow();
			final BigInteger[] oldNanos = cr.getTestWallNanos();
			final List<Integer> lNewRow = new ArrayList<Integer>();
			final List<BigInteger> lNewNanos = oldNanos != null ? new ArrayList<BigInteger>() : null;
			BigInteger totalNanos = cr.getTotalWallNanos();

			for (int i = 0; i < nTests; ++i) {
				if (filter.evaluate(1 + i)) {
					lNewRow.add(oldRow[i]);
					if (lNewNanos != null) {
						lNewNanos.add(oldNanos[i]);
					}
				}
				else if (cr.getTestWallNanos() != null) {
					totalNanos = totalNanos.subtract(oldNanos[i]);
				}
			}

			cr.setRow(lNewRow.toArray(new Integer[lNewRow.size()]));
			cr.setTestWallNanos(lNewNanos != null ? lNewNanos.toArray(new BigInteger[lNewNanos.size()]) : null);
			cr.setTotalWallNanos(totalNanos);
		}
	}

	/**
	 * Convenience version of {@link #getStrongMutants(Set, Set)} when we are not interested
	 * in splitting them on potentially equivalent and hard to kill.
	 */
	public Set<GAIndividual> getStrongMutants() {
		final Set<GAIndividual> strong = new HashSet<>();
		getStrongMutants(strong, strong);
		return strong;
	}

	/**
	 * Adds to <code>potEquiv</code> and <code>hardToKill</code> the set of all
	 * strong mutants from the simulation record, divided into potentially
	 * equivalent and hard to kill mutants. A mutant is said to be strong if it is
	 * alive or if it is only killed by a single test case that does not kill any
	 * other mutant (row and column sums are equal to 1).
	 */
	public void getStrongMutants(Set<GAIndividual> potEquiv, Set<GAIndividual> hardToKill) {
		final int[] columnSums = getColumnSums();

		individualLoop:
		for (Map.Entry<GAIndividual, ComparisonSimulationRecord> entry : comparisonRecords.entrySet()) {
			final GAIndividual ind = entry.getKey();

			final ComparisonResult[] results = entry.getValue().getComparisonResults();
			int sum = 0;
			for (int col = 0; sum <= 1 && col < columnSums.length; ++col) {
				if (results[col].isDead() && columnSums[col] > 1) {
					continue individualLoop;
				}
				sum += results[col].isDead() ? 1 : 0;
			}

			switch (sum) {
			case 0: if (potEquiv != null) potEquiv.add(ind); break;
			case 1: if (hardToKill != null) hardToKill.add(ind); break;
			default: /* skip */ break;
			}
		}
	}

	public int[] getColumnSums() {
		final int nTestCases = getTestCaseCount();
		final int[] columnSums = new int[nTestCases];

		// Sum the columns of the matrix: if a test case dies in a test case whose column sum is 1, it is still strong.
		// We skip over cells marked as invalid.
		for (ComparisonSimulationRecord x : comparisonRecords.values()) {
			final ComparisonResult[] row = x.getComparisonResults();
			for (int col = 0; col < nTestCases; ++col) {
				columnSums[col] += row[col].isValid() && row[col].isDead() ? 1 : 0;
			}
		}

		return columnSums;
	}

	public int getTestCaseCount() {
		return !comparisonRecords.isEmpty()
			? comparisonRecords.values().iterator().next().getTestCaseCount()
			: 0;
	}

	private static Yaml getYaml() {
		return new Yaml(new SimulationRecordConstructor(), new SimulationRecordRepresenter());
	}

	private static boolean isYAMLFile(File f) {
		if (f == null) {
			return false;
		}
		final String name = f.getName().toLowerCase();
		return (name.endsWith(".yaml") || name.endsWith(".yml"));
	}

	/**
	 * Adds all the surviving valid mutants to the <code>alive</code> set, all the
	 * dead valid mutants to the <code>dead</code> set, and all the invalid mutants
	 * to the <code>invalid</code> set. If you want a set of all the mutants, just
	 * pass the same set for all arguments.
	 */
	public void getMutants(Set<GAIndividual> alive, Set<GAIndividual> killed, Set<GAIndividual> invalid) {
		for (Entry<GAIndividual, ComparisonSimulationRecord> entry : comparisonRecords.entrySet()) {
			final GAIndividual ind = entry.getKey();
			final ComparisonSimulationRecord cmp = entry.getValue();

			if (cmp.isAlive()) {
				alive.add(ind);
			} else if (cmp.isKilled()) {
				killed.add(ind);
			} else if (!cmp.isValid()) {
				invalid.add(ind);
			} else {
				throw new IllegalStateException("Individual should be alive, killed or invalid");
			}
		}
	}

	/**
	 * Computes a set of subsuming mutants out of this simulation record. Uses the
	 * greedy algorithm from:
	 * 
	 * <pre>
	 * M. Papadakis, C. Henard, M. Harman, Y. Jia, and Y. Le Traon, ‘Threats to
	 * the validity of mutation-based test assessment’, in Proceedings of the
	 * 25th International Symposium on Software Testing and Analysis, in ISSTA
	 * 2016. New York, NY, USA: Association for Computing Machinery, Jul. 2016,
	 * pp. 354–365. doi: 10.1145/2931037.2931040.
	 * </pre>
	 */
	public MutantSubsumptionRecord computeSubsumingMutants() {
		Map<GAIndividual, ComparisonSimulationRecord> pending = new HashMap<>(comparisonRecords);

		MutantSubsumptionRecord subsumingRecord = new MutantSubsumptionRecord();		
		subsumingRecord.setLiveMutants(removeLiveMutants(pending));
		subsumingRecord.setDuplicateMutants(removeDuplicateMutants(pending));

		while (!pending.isEmpty()) {
			int maxSubsumed = 0;
			Map<List<Integer>, GAIndividual> subsumedMut = null;
			GAIndividual maxMutSubsuming = null;
			List<Integer> maxMutSubsumingRow = null;

			// Select the most subsuming mutant (note: a dead mutant subsumes itself) 
			for (Entry<GAIndividual, ComparisonSimulationRecord> entry : pending.entrySet()) {
				Map<List<Integer>, GAIndividual> subsumedEntries = pending.entrySet().stream()
					.filter(pe -> entry.getValue().subsumes(pe.getValue()))
					.collect(Collectors.toMap(
						pe -> Arrays.asList(pe.getValue().getRow()),
						pe -> pe.getKey()));

				if (subsumedEntries.size() > maxSubsumed) {
					maxSubsumed = subsumedEntries.size();
					maxMutSubsuming = entry.getKey();
					subsumedMut = subsumedEntries;
					maxMutSubsumingRow = Arrays.asList(entry.getValue().getRow());
				}
			}

			subsumedMut.values().forEach(pending::remove);

			// Remove the subsuming mutant from the set of mutants it subsumes, for reporting
			subsumedMut.remove(maxMutSubsumingRow);

			SubsumingMutant subsumingMutant = new SubsumingMutant();
			subsumingMutant.setSubsumed(subsumedMut);
			subsumingMutant.setSubsuming(maxMutSubsuming);
			subsumingRecord.getSubsumingMutants().put(maxMutSubsumingRow, subsumingMutant);
		}

		return subsumingRecord;
	}

	protected Map<List<Integer>, Set<GAIndividual>> removeDuplicateMutants(Map<GAIndividual, ComparisonSimulationRecord> pending) {
		/*
		 * Remove duplicate mutants: first group by row, then remove all entries for which
		 * we have more than 1 individual.
		 */
		Map<List<Integer>, Set<GAIndividual>> rowToIndividuals = new HashMap<>();
		for (Iterator<Map.Entry<GAIndividual, ComparisonSimulationRecord>> itPending = pending.entrySet().iterator();
			 itPending.hasNext();
		) {
			Entry<GAIndividual, ComparisonSimulationRecord> entry = itPending.next();
			List<Integer> rowList = Arrays.asList(entry.getValue().getRow());
			rowToIndividuals.computeIfAbsent(rowList, k -> new HashSet<>()).add(entry.getKey());
		}
		for (Iterator<Entry<List<Integer>, Set<GAIndividual>>> itRowToIndividuals = rowToIndividuals.entrySet().iterator();
			 itRowToIndividuals.hasNext();
		) {
			Entry<List<Integer>, Set<GAIndividual>> entry = itRowToIndividuals.next();
			if (entry.getValue().size() > 1) {
				// Duplicated: remove from pending
				entry.getValue().forEach(pending::remove);
			} else {
				// Not duplicated: remove from the map that will eventually only have duplicated mutants
				itRowToIndividuals.remove();
			}
		}

		return rowToIndividuals;
	}

	protected Set<GAIndividual> removeLiveMutants(Map<GAIndividual, ComparisonSimulationRecord> pending) {
		Set<GAIndividual> liveMutants = new HashSet<>();

		for (Iterator<Map.Entry<GAIndividual, ComparisonSimulationRecord>> itPending = pending.entrySet().iterator();
			 itPending.hasNext();
		) {
			Entry<GAIndividual, ComparisonSimulationRecord> entry = itPending.next();
			if (entry.getValue().isAlive()) {
				itPending.remove();
				liveMutants.add(entry.getKey());
			}
		}

		return liveMutants;
	}

}
