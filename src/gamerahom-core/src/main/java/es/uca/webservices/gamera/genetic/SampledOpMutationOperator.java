package es.uca.webservices.gamera.genetic;

import java.math.BigInteger;
import java.util.Map.Entry;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.log.GALogger;

/**
 * Tweaks the {@link IndividualMutationOperator} so mutating the operator
 * samples the estimated distribution of strong mutants by operator in the HOF
 * since the last generation.
 */
public class SampledOpMutationOperator extends IndividualMutationOperator {

	private double samplingRate = 0.9;
	private GAPopulation lastPopulation;
	private double[] strongProbabilities;

	public double getSamplingRate() {
		return samplingRate;
	}

	public void setSamplingRate(double samplingFactor) {
		this.samplingRate = samplingFactor;
	}

	@Override
	public void apply(GAState state, GAPopulation source, GAPopulation destination, GALogger logger, boolean mutateAttribute, boolean createTree) {
		if (lastPopulation != source) {
			lastPopulation = source;

			final AnalysisResults analysis = state.getAnalysisResults();
			strongProbabilities = new double[analysis.getMaxOp().intValue()];
			int totalAlive = 0;
			for (Entry<GAIndividual, ComparisonResults> entry : state.getHof().entrySet()) {
				final GAIndividual ind = entry.getKey().denormalize(analysis);
				final ComparisonResults cmp = entry.getValue();
				if (cmp.isAlive()) {
					strongProbabilities[ind.getThOperator(1).intValue() - 1]++;
					totalAlive++;
				}
			}
			for (int i = 0; i < strongProbabilities.length; i++) {
				strongProbabilities[i] /= totalAlive;
			}
		}

		super.apply(state, source, destination, logger, mutateAttribute, createTree);
	}

	@Override
	protected BigInteger computeMutatedValue(int mutatedField, BigInteger currentValue, BigInteger maxValue, BigInteger maxDelta) {
		if (mutatedField == MUT_OPERATOR && prng.nextDouble() < samplingRate) {

			final double roll = prng.nextDouble();
			double probSoFar = 0;
			for (int i = 0; i < strongProbabilities.length; i++) {
				probSoFar += strongProbabilities[i];
				if (roll < probSoFar) {
					return BigInteger.valueOf(i).add(BigInteger.ONE);
				}
			}

			// Fallback: return the maximum value
			return BigInteger.valueOf(strongProbabilities.length);
		} else {
			return super.computeMutatedValue(mutatedField, currentValue, maxValue, maxDelta);
		}
	}

}
