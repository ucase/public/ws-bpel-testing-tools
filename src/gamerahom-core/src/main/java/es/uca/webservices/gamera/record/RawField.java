package es.uca.webservices.gamera.record;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.exec.GAExecutor;

/**
 * Available fields for the raw file format, in the expected order for saving.
 * Please do not change the order of the values unless you <em>really</em> know
 * what you're doing!
 * 
 * @author Antonio García-Domínguez
 */
enum RawField {
	FORMAT("format") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), Integer.toString(record.getFormatVersion()));
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) throws Exception {
			record.setFormatVersion(Integer.valueOf(firstLineFields[1]));
		}
	},
	RECORDER("recorder") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), record.getRecorder());
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) throws Exception {
			// TODO Auto-generated method stub
			record.setRecorder(readOptionalField(firstLineFields[1]));
		}
	},
	TIMESTAMP("timestamp") {
		private final DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS z");

		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), df.format(record.getTimestamp()));
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) throws Exception {
			record.setTimestamp(df.parse(firstLineFields[1]));
		}
	},
	MAX_ORDER("maxOrder") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), Integer.toString(record.getMaxOrder()));
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) {
			record.setMaxOrder(Integer.valueOf(firstLineFields[1]));
		}
	},
	MISSING_ARE_INVALID("missingAreInvalid") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), Boolean.toString(record.getMissingIndividualsAreInvalid()));
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) {
			record.setMissingIndividualsAreInvalid(Boolean.valueOf(firstLineFields[1]));
		}
	},
	ANALYSIS_ATTRIBUTES("attributeRanges") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), record.getAnalysisRecord().getFieldRanges());
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) {
			final List<BigInteger> bis = new ArrayList<BigInteger>();
			for (int i = 1; i < firstLineFields.length; ++i) {
				bis.add(new BigInteger(firstLineFields[i]));
			}

			record.getAnalysisRecord().setFieldRanges(bis.toArray(new BigInteger[bis.size()]));
		}
	},
	ANALYSIS_OPERANDS("locationCounts") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), record.getAnalysisRecord().getLocationCounts());
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) {
			final List<BigInteger> bis = new ArrayList<BigInteger>();
			for (int i = 1; i < firstLineFields.length; ++i) {
				bis.add(new BigInteger(firstLineFields[i]));
			}

			record.getAnalysisRecord().setLocationCounts(bis.toArray(new BigInteger[bis.size()]));
		}
	},
	ANALYSIS_OPERATORS("operatorNames") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), record.getAnalysisRecord().getOperatorNames());
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) {
			record.getAnalysisRecord().setOperatorNames(Arrays.copyOfRange(firstLineFields, 1, firstLineFields.length));
		}
	},
	ANALYZE_NANOS("analyzeWallNanos") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), record.getAnalysisRecord().getTotalWallNanos());
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) {
			record.getAnalysisRecord().setTotalWallNanos(new BigInteger(firstLineFields[1]));
		}
	},
	EXECUTOR("executorClassName") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			final GAExecutor exec = record.getExecutor();
			printlnFields(pW, getName(), exec != null ? exec.getClass().getName() : null);
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) throws Exception {
			final String className = readOptionalField(firstLineFields[1]);
			if (className != null) {
				record.setExecutor((GAExecutor)Class.forName(className).newInstance());
			} else {
				record.setExecutor(null);
			}
		}
	},
	CLEANUP_NANOS("cleanupWallNanos") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), record.getCleanupWallNanos());
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) {
			record.setCleanupWallNanos(new BigInteger(firstLineFields[1]));
		}
	},
	PREPARE_NANOS("prepareWallNanos") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), record.getPrepareWallNanos());
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) {
			record.setPrepareWallNanos(new BigInteger(firstLineFields[1]));
		}
	},
	TEST_CASE_NAMES("testCaseNames") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			final List<String> lCaseNames = record.getTestCaseNames();
			if (lCaseNames != null) {
				final String[] vCaseNames = lCaseNames.toArray(new String[lCaseNames.size()]);
				printlnFields(pW, getName(), vCaseNames);
			}
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) throws Exception {
			final List<String> fields = Arrays.asList(firstLineFields);
			record.setTestCaseNames(fields.subList(1, fields.size()));
		}
	},
	COMPARISONS("comparisons") {
		@Override
		public void saveTo(SimulationRecord record, PrintWriter pW) {
			printlnFields(pW, getName(), record.getComparisonRecords().size());

			for (Map.Entry<GAIndividual, ComparisonSimulationRecord> entry : record.getComparisonRecords().entrySet()) {
				final GAIndividual ind = entry.getKey();
				final ComparisonSimulationRecord cR = entry.getValue();

				printFields(pW, ind.isNormalized() ? RAW_NORMALIZED_MARKER : RAW_DENORMALIZED_MARKER, ind.getOrder());
				for (int i = 1; i <= ind.getOrder(); ++i) {
					pW.print(RAW_FIELD_SEPARATOR);
					pW.print(ind.getThOperator(i));
					pW.print(RAW_FIELD_SEPARATOR);
					pW.print(ind.getThLocation(i));
					pW.print(RAW_FIELD_SEPARATOR);
					pW.print(ind.getThAttribute(i));
				}

				pW.print(RAW_FIELD_SEPARATOR);
				pW.print(cR.getRow().length);
				for (int c : cR.getRow()) {
					pW.print(RAW_FIELD_SEPARATOR);
					pW.print(c);
				}

				pW.print(RAW_FIELD_SEPARATOR);
				if (cR.getTestWallNanos() != null) {
					pW.print(cR.getTestWallNanos().length);
					for (BigInteger nano : cR.getTestWallNanos()) {
						pW.print(RAW_FIELD_SEPARATOR);
						pW.print(nano);
					}
				}
				else {
					pW.print(RAW_NULL_MARKER);
				}

				pW.print(RAW_FIELD_SEPARATOR);
				pW.print(cR.getTotalWallNanos());
				pW.println();
			}
		}

		@Override
		public void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) throws Exception {
			final int nComparisons = Integer.valueOf(firstLineFields[1]);

			String line;
			for (int i = 0; i < nComparisons && (line = bR.readLine()) != null; ++i) {
				String[] fields = line.split(RAW_FIELD_SEPARATOR);
				int fieldPosition = 0;
				final boolean normalized = RAW_NORMALIZED_MARKER.equals(fields[fieldPosition++]);
				final int order = Integer.valueOf(fields[fieldPosition++]);

				final BigInteger[] operators = new BigInteger[order];
				final BigInteger[] locations = new BigInteger[order];
				final BigInteger[] attributes = new BigInteger[order];
				for (int iOrder = 0; iOrder < order; ++iOrder) {
					operators[iOrder] = new BigInteger(fields[fieldPosition++]);
					locations[iOrder] = new BigInteger(fields[fieldPosition++]);
					attributes[iOrder] = new BigInteger(fields[fieldPosition++]);
				}
				final GAIndividual ind = new GAIndividual(operators, locations, attributes, order, normalized);

				final int tests = Integer.valueOf(fields[fieldPosition++]);
				final Integer[] row = new Integer[tests];
				for (int iTest = 0; iTest < tests; ++iTest) {
					row[iTest] = Integer.valueOf(fields[fieldPosition++]);
				}

				final String sNanosCount = readOptionalField(fields[fieldPosition++]);
				BigInteger[] nanos = null;
				if (sNanosCount != null) {
					final int nanosCount = Integer.valueOf(sNanosCount);
					nanos = new BigInteger[nanosCount];
					for (int iNanos = 0; iNanos < nanosCount; ++iNanos) {
						nanos[iNanos] = new BigInteger(fields[fieldPosition++]);
					}
				}

				final BigInteger totalNanos = new BigInteger(fields[fieldPosition++]);
				final ComparisonSimulationRecord cR = new ComparisonSimulationRecord(row, totalNanos);
				cR.setTestWallNanos(nanos);
				record.getComparisonRecords().put(ind, cR);
			}
		}
	};

	private static final Map<String, RawField> STRING_TO_FIELD;
	static {
		STRING_TO_FIELD = new HashMap<String, RawField>();
		for (RawField f : RawField.values()) {
			STRING_TO_FIELD.put(f.getName(), f);
		}
	}

	private static final String RAW_FIELD_SEPARATOR = "\t";
	private static final String RAW_NORMALIZED_MARKER = "N";
	private static final String RAW_DENORMALIZED_MARKER = "D";
	private static final String RAW_NULL_MARKER = "Z";
	private final String name;

	private RawField(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public abstract void saveTo(SimulationRecord record, PrintWriter pW);

	public abstract void readFrom(SimulationRecord record, String[] firstLineFields, BufferedReader bR) throws Exception;

	public static RawField fromName(String name) {
		return STRING_TO_FIELD.get(name);
	}

	public static void read(SimulationRecord record, String line, BufferedReader bR) throws Exception {
		final String[] fields = line.split(RAW_FIELD_SEPARATOR);
		final RawField rf = fromName(fields[0]);
		if (rf != null) {
			rf.readFrom(record, fields, bR);
		}
		else {
			throw new IllegalArgumentException("Unknown field '" + fields[0] + "'");
		}
	}

	protected static String readOptionalField(String field) {
		if (RAW_NULL_MARKER.equals(field)) {
			return null;
		}
		return field;
	}

	protected static void printField(PrintWriter pW, final Object field) {
		if (field instanceof Object[]) {
			printFields(pW, (Object[])field);
		} else {
			pW.print(field != null ? field.toString() : RAW_NULL_MARKER);
		}
	}

	protected static void printFields(PrintWriter pW, Object... fields) {
		if (fields.length > 0) {
			printField(pW, fields[0]);
			for (int i = 1; i < fields.length; ++i) {
				pW.print(RAW_FIELD_SEPARATOR);
				printField(pW, fields[i]);
			}
		}
	}

	protected static void printlnFields(PrintWriter pW, Object... fields) {
		printFields(pW, fields);
		pW.println();
	}
}