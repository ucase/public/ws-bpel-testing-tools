package es.uca.webservices.gamera.subcommands;

import java.io.File;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.ValidIndividualsList;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.exec.SimulatedExecutor;
import es.uca.webservices.gamera.fitness.MutantStrengthFitnessFunction;
import es.uca.webservices.gamera.record.SimulationRecord;

public class FitnessMapSubcommand extends AbstractSubcommand {

	public static class Counter implements Iterable<Integer> {
		private final int min, max;

		public Counter(int min, int max) {
			this.min = min;
			this.max = max;
		}

		@Override
		public Iterator<Integer> iterator() {
			return new Iterator<Integer>() {
				private int current = min;

				@Override
				public boolean hasNext() {
					return current <= max;
				}

				@Override
				public Integer next() {
					return hasNext() ? current++ : null;
				}

				@Override
				public void remove() {
					throw new UnsupportedOperationException();
				}
			};
		}
	}
	
	private static final VelocityEngine VELOCITY_ENGINE= new VelocityEngine();

	static {
		VELOCITY_ENGINE.setProperty("resource.loader", "class");
		VELOCITY_ENGINE.setProperty(
			"class.resource.loader.class",
			ClasspathResourceLoader.class.getName());
		VELOCITY_ENGINE.setProperty("foreach.provide.scope.control", "true");
		VELOCITY_ENGINE.init();
	}

	private static final String DESCRIPTION =
		"Produces a detailed report from a simulation record, listing several\n" +
		"statistics about the fitness distribution through the entire space of\n" +
		" all first-order individuals.\n" +
		"\n" +
		"The report is dumped as a LaTeX document to the standard output stream,\n" +
		"and should be compiled with a recent TeXLive distribution and the latest\n" +
		"versions of the pgfplots and pgfplotstable packages."
		;

	public FitnessMapSubcommand() {
		super(1, 1, "fitnessmap", "simrecord.(raw|yaml)", DESCRIPTION);
	}

	@Override
	protected void runCommand() throws Exception {
		final OutputStreamWriter w = new OutputStreamWriter(getOutputStream());
		try {
			final File simRecordFile = new File(getNonOptionArgs().get(0));
			final SimulationRecord sR = SimulationRecord.loadFrom(simRecordFile); 
			renderReport(simRecordFile, sR, w);
		} finally {
			w.close();
		}
	}

	private void renderReport(final File simRecordFile, final SimulationRecord sR, final Writer writer) throws AnalysisException, ComparisonException, NoSuchMethodException {
		final VelocityContext ctx = new VelocityContext();
		ctx.put("recordFilename", simRecordFile.getName());

		final SimulatedExecutor sE = new SimulatedExecutor(sR);
		final AnalysisResults aR = sE.analyze(null);
		final BigInteger maxOp = aR.getMaxOp();
		final BigInteger maxLoc = aR.getMaxLoc();
		final BigInteger maxAtt = aR.getMaxAtt();
		ctx.put("analysis", aR);
		ctx.put("maxOp", maxOp);
		ctx.put("maxLoc", maxLoc);
		ctx.put("maxAtt", maxAtt);

		final List<GAIndividual> allNormalizedIndividuals = new ArrayList<GAIndividual>();
		final GAHof hof = new GAHof(aR);
		ctx.put("normalizedIndividuals", allNormalizedIndividuals);
		ctx.put("strongIndividuals", sR.getStrongMutants());
		ctx.put("hof", hof);
		ctx.put("diCreator", GAIndividual.class.getConstructor(BigInteger.class, BigInteger.class, BigInteger.class));
		ctx.put("iCreator", GAIndividual.class.getConstructor(int.class, int.class, int.class, boolean.class));
		ctx.put("counterCreator",  Counter.class.getConstructor(int.class, int.class));

		long nTestCases = 0;
		for (GAIndividual ni : new ValidIndividualsList(aR, 1)) {
			final ComparisonResults cmp = sE.compare(null, ni)[0];
			nTestCases = cmp.getRow().length;
			hof.put(cmp);
			allNormalizedIndividuals.add(ni);
		}
		ctx.put("numTestCases", nTestCases);

		final GAPopulation population = new GAPopulation();
		population.setIndividuals(allNormalizedIndividuals);

		final GAState state = new GAState();
		state.setHof(hof);
		state.setAnalysisResults(hof.getAnalysis());
		state.setTotalNumberOfMutants(hof.size());
		state.updateExecutionMatrix(hof, population);
		ctx.put("state", state);

		final MutantStrengthFitnessFunction fit = new MutantStrengthFitnessFunction();
		for (GAIndividual nInd : allNormalizedIndividuals) {
			nInd.setFitness(fit.computeFitness(nInd, state));
		}
        ctx.put("fitness", fit);
		ctx.put("minFitness", 0);
		ctx.put("maxFitness", nTestCases * hof.getValidMutants());

		Template t = VELOCITY_ENGINE.getTemplate("fitnessmap.vm");
		t.merge(ctx, writer);
	}

}
