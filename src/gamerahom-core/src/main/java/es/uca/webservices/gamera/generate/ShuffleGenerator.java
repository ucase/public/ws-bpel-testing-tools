package es.uca.webservices.gamera.generate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.ValidIndividualsList;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.generate.GAIndividualGenerator;

/**
 * Randomly generates new individuals, cycling through a shuffled list of all
 * the possible individuals. The number of individuals generated is a percentage
 * of the number of the individuals of the original population.
 *
 * @author Antonio García Domínguez
 * @version 1.1
 */
@Option(description = "Randomly " + "generates new individuals, cycling through a shuffled list of all individuals. "
		+ "The number of individuals generated is a percentage of the number of the individuals "
		+ "of the original population.")
public class ShuffleGenerator implements GAIndividualGenerator {
	// Number of individuals to be generated, expressed as a proportion in the
	// range [0,1] of the size of the source population
	private double percent;
	private Random prng;

	private AnalysisResults lastAnalysis;
	private List<GAIndividual> shuffledIndividuals;
	private Iterator<GAIndividual> itIndividual;

	public void setPercent(double percent) {
		this.percent = percent;
	}

	public double getPercent() {
		return percent;
	}

	@Override
	public void generate(GAState state, int maxOrder, GAPopulation source, GAPopulation destination) {

		if (maxOrder != 1) {
			throw new IllegalArgumentException(
					"The maximum order of the individual should be equal to one");
		}

		final AnalysisResults analysis = state.getAnalysisResults();
		if (lastAnalysis != analysis) {
			lastAnalysis = analysis;
			shuffledIndividuals = new ArrayList<>();

			ValidIndividualsList validIndividuals = new ValidIndividualsList(analysis, 1);
			validIndividuals.addAllTo(shuffledIndividuals);
			Collections.shuffle(shuffledIndividuals, prng);

			itIndividual = shuffledIndividuals.iterator();
			if (!itIndividual.hasNext()) {
				throw new IllegalArgumentException("No individuals are available");
			}
		}

		GAIndividual a = createIndividual(analysis);
		destination.addIndividual(a);
	}

	private GAIndividual createIndividual(AnalysisResults analysis) {
		if (!itIndividual.hasNext()) {
			itIndividual = shuffledIndividuals.iterator();
		}
		return itIndividual.next().denormalize(analysis);
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (getPercent() < 0.0 || getPercent() > 1.0) {
			throw new InvalidConfigurationException(
					"Percent of all mutants to be produced by the uniform " + "generator must lie in [0, 1]");
		}
		if (prng == null) {
			throw new InvalidConfigurationException("No PRNG has been set");
		}
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}

}
