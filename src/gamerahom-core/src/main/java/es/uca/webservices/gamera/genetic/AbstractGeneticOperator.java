package es.uca.webservices.gamera.genetic;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperator;
import es.uca.webservices.gamera.api.select.GASelectionOperator;
import es.uca.webservices.gamera.select.RouletteSelection;

/**
 * Abstract base class for the genetic operators.
 * 
 * All genetic operators based on this class use roulette selection to pick out
 * individuals from the population, but this can be changed using the
 * {@link #setSelector(GASelectionOperator)} method.
 * 
 * The seed for the PRNG used in one of these operators is shared with every
 * other component of the algorithm through the shared {@link Configuration} object.
 * 
 * This class uses late initialization to avoid creating more objects than it
 * should. Therefore, its fields should never be accessed directly: instead,
 * please use the available methods.
 */
public abstract class AbstractGeneticOperator implements GAGeneticOperator {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractGeneticOperator.class);

	protected Random prng;
	private GASelectionOperator selector;

	/**
	 * Returns the {@link GASelectionOperator} used to select the individual to
	 * be mutated.
	 */
	public synchronized GASelectionOperator getSelector() {
		if (selector == null) {
			selector = new RouletteSelection();
			selector.setPRNG(prng);
		}
		LOGGER.debug("Using {} as selector", selector);
		return selector;
	}

	/**
	 * Changes the {@link GASelectionOperator} used to select the individual to
	 * be mutated.
	 */
	public void setSelector(GASelectionOperator selector) {
		this.selector = selector;
		selector.setPRNG(prng);
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
		if (selector != null) {
			selector.setPRNG(prng);
		}
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (prng == null) {
			throw new InvalidConfigurationException("No PRNG has been set");
		}
	}
}