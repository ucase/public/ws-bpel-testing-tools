package es.uca.webservices.gamera.term;

import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.term.GATerminationCondition;

/**
 * Termination condition that is true when a certain number of generations have
 * been produced.
 *
 * @author Antonio García-Domínguez
 * @version 1.0
 */

@Option(description="Termination condition that is true when a certain " +
		"number of generations have been produced.")
public class GenerationCountCondition implements GATerminationCondition {

    private int count;

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean evaluate(GAState state) {
        return state.getCurrentGeneration() >= count;
    }

    @Override
    public void validate() throws InvalidConfigurationException {
        if (getCount() <= 0) {
            throw new InvalidConfigurationException(
                    "The maximum number of generations should be greater than 0");
        }
    }

}
