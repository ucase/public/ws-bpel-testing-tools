package es.uca.webservices.gamera.genetic;

import java.math.BigInteger;
import java.util.Arrays;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.log.GALogger;

/**
 * Implements a mutation operator which modifies the operator, location or
 * attribute of a mutant in the provided population which doesn't use
 * the original value of the field to mutate. 
 *
 * @author Antonio García-Domínguez, Juan José Domínguez-Jiménez, Emma Blanco-Muñoz
 * @version 1.0
 */

@Option(description="Implements a mutation operator which modifies the operator, " +
		"location or attribute of a mutant in the provided population which doesn't " +
		"use the original value of the field to mutate.")
public class RandomFieldMutationOperator extends AbstractMutationOperator {
	private static final int MUT_OPERATOR = 1;
	private static final int MUT_LOCATION = 2;
	private static final int MUT_ATTRIBUTE = 3;

	@Override
	public void apply(GAState state, GAPopulation source, GAPopulation destination, GALogger logger, boolean mutateAttribute, boolean createTree) {
		// We have to choose the mutant to be modified randomly, but taking into
		// account the order of the individual. [1, order]
		final GAIndividual individual = getSelector().select(source).denormalize(state.getAnalysisResults());
		final int mutant = 1 + prng.nextInt(individual.getOrder());

		// Now, the value to be modified has to be chosen randomly too. [1, 3]
		final int mutatedField;
		if(mutateAttribute)
			mutatedField = MUT_OPERATOR + prng.nextInt(MUT_ATTRIBUTE - MUT_OPERATOR + 1);
		else
			mutatedField = MUT_OPERATOR + prng.nextInt(MUT_LOCATION - MUT_OPERATOR + 1);

		final GAIndividual child = doMutation(state.getAnalysisResults(), individual, mutant, mutatedField);
		
		logger.appliedGeneticOperator(state, RandomFieldMutationOperator.class.getName(),
				Arrays.asList(individual), Arrays.asList(child));
		
		destination.addIndividual(child);
	}

	GAIndividual doMutation(AnalysisResults analysis, final GAIndividual individual, final int mutant, final int mutatedField) {
		checkValidParameters(individual, mutant, mutatedField);

		// Maximum denormalized value for the field to be mutated (minimum value is fixed to 1)
		BigInteger maxValue;

		switch(mutatedField) {
		case MUT_OPERATOR:
			maxValue = analysis.getMaxOp();
			break;
		case MUT_LOCATION:
			maxValue = analysis.getMaxLoc();
			break;
		default:
		case MUT_ATTRIBUTE:
			maxValue = analysis.getMaxAtt();
			break;
		}

		// To calculate the new value we use the next rule v = generateRandom(min, max) % maxValueAnalysis + 1
		//     where min = 1 and max = (1 - scaleFactor) * randomRange
		//   = (1 + random % max) % maxValueAnalysis + 1 

		final BigInteger max;
		// The minimum value for the random number is one, so the maximum value at least has to be one
		if (getRandomRange() != null) {
			max = BigInteger.valueOf((long) ((1 - getScaleFactor()) * getRandomRange())).max(BigInteger.ONE);
		} else {
			max = BigInteger.valueOf((long) ((1 - getScaleFactor()) * getRandomRangePercent())).multiply(maxValue).max(BigInteger.ONE);
		}

		final BigInteger aux = BigInteger.ONE.add(BigInteger.valueOf(prng.nextLong())).mod(max);
		final BigInteger validNewValue = aux.mod(maxValue).add(BigInteger.ONE);

		// Create the mutated GAIndividual
		final BigInteger[] operator  = (BigInteger[])individual.getOperator().toArray();
		final BigInteger[] location  = (BigInteger[])individual.getLocation().toArray();
		final BigInteger[] attribute = (BigInteger[])individual.getAttribute().toArray();

		// If the new calculated value is greater than the maximum, the new
		// order will be the maximum allowed.
		switch(mutatedField) {
		case MUT_OPERATOR:
			operator[mutant - 1] = validNewValue;
			break;
		case MUT_LOCATION:
			location[mutant - 1] = validNewValue;
			break;
		case MUT_ATTRIBUTE:
			attribute[mutant - 1] = validNewValue;
			break;
		default:
			break;
		}

		return new GAIndividual(operator, location, attribute, individual.getOrder());
	}

	private void checkValidParameters(final GAIndividual individual,
			final int mutant, final int mutatedField) {
		if(mutant < 1 || mutant > individual.getMaxOrder()) {
			throw new IllegalArgumentException(
					"The mutant selected should be greater than zero"
							+ " and lower than the maximum order");
		}

		if(mutatedField < MUT_OPERATOR || mutatedField > MUT_ATTRIBUTE) {
			throw new IllegalArgumentException(
					"The mutated field should be 1 (operator), 2 (location) " +
					"or 3 (attribute)");
		}
	}

}
