package es.uca.webservices.gamera.subcommands;

import java.io.File;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.ValidIndividualsList;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.exec.SimulatedExecutor;
import es.uca.webservices.gamera.fitness.MutantStrengthFitnessFunction;
import es.uca.webservices.gamera.record.SimulationRecord;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import es.uca.webservices.gamera.api.util.EuclideanDistance;

/**
 * Produces a table to use with R to analyse fitness-distance within a certain
 * problem space.
 */
public class FitnessDistanceTableSubcommand extends AbstractSubcommand {

	private static final String DIST_SCALE_OPTION = "dist-scale";
	private static final int DIST_SCALE_DEFAULT = 4;
	private static final String PRINT_OPTIMA_OPTION = "print-optima";
	private static final boolean PRINT_OPTIMA_DEFAULT = false; 

	private static final class FitnessDescendingComparator implements Comparator<GAIndividual> {
		@Override
		public int compare(GAIndividual o1, GAIndividual o2) {
			return -Double.compare(o1.getFitness(), o2.getFitness());
		}
	}

	private static final String DESCRIPTION =
		"Prints to the standard output stream a table with individual, global\n" +
		"optima, individual fitness (assuming full HoF) and individual distance\n" +
		"to optima, for later use with R to check for fitness-distance correlation."
	;

	private int distanceScale = DIST_SCALE_DEFAULT;
	private boolean printOptima = PRINT_OPTIMA_DEFAULT;

	public int getDistanceScale() {
		return distanceScale;
	}

	public void setDistanceScale(int distanceScale) {
		if (distanceScale < 0) { 
			throw new IllegalArgumentException("Distance scale must be positive");
		}
		this.distanceScale = distanceScale;
	}

	public boolean isPrintOptima() {
		return printOptima;
	}

	public void setPrintOptima(boolean printOptima) {
		this.printOptima = printOptima;
	}

	public FitnessDistanceTableSubcommand() {
		super(1, 1, "fdtable", "simrecord.(raw|yaml)", DESCRIPTION);
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		if (options.has(DIST_SCALE_OPTION)) {
			setDistanceScale((int) options.valueOf(DIST_SCALE_OPTION));
		}
		if (options.has(PRINT_OPTIMA_OPTION)) {
			setPrintOptima((boolean) options.valueOf(PRINT_OPTIMA_OPTION));
		}
	}

	@Override
	protected OptionParser createOptionParser() {
		final OptionParser parser = super.createOptionParser();
		parser.accepts(DIST_SCALE_OPTION, "Scale used for computing the square root in the Euclidean distance")
			.withRequiredArg().ofType(Integer.class).describedAs("S")
			.defaultsTo(DIST_SCALE_DEFAULT);
		parser.accepts(PRINT_OPTIMA_OPTION, "Whether to print the optima in the listing or not")
			.withRequiredArg().ofType(Boolean.class).defaultsTo(PRINT_OPTIMA_DEFAULT);

		return parser;
	}

	@Override
	protected void runCommand() throws Exception {
		final File simRecordFile = new File(getNonOptionArgs().get(0));
		final SimulationRecord sR = SimulationRecord.loadFrom(simRecordFile); 
		renderReport(simRecordFile, sR);
	}

	private void renderReport(File simRecordFile, SimulationRecord sR) throws Exception {
		final List<GAIndividual> allNormalizedIndividuals = computeFitnesses(sR);
		final List<GAIndividual> globalOptima = extractGlobalOptima(allNormalizedIndividuals);
		renderTable(allNormalizedIndividuals, globalOptima);
	}

	private void renderTable(final List<GAIndividual> allNormalizedIndividuals, final List<GAIndividual> globalOptima) {
		PrintStream ps = getOutputStream();
		ps.println("Individual,Optimum,Distance,Fitness");
		for (GAIndividual ind : allNormalizedIndividuals) {
			for (GAIndividual go : globalOptima) {
				BigDecimal distance = EuclideanDistance.distance(ind, go);
				ps.println(String.format("\"%s\",\"%s\",%s,%g",
					ind.individualFieldsToString(), go.individualFieldsToString(),
					distance.toString(), ind.getFitness()));
			}
		}
	}

	private List<GAIndividual> extractGlobalOptima(final List<GAIndividual> allNormalizedIndividuals) {
		// Sort by fitness in descending order, then extract list of global optima
		Collections.sort(allNormalizedIndividuals, new FitnessDescendingComparator());
		List<GAIndividual> globalOptima = new ArrayList<>();
		final double maxFitness = allNormalizedIndividuals.isEmpty() ? 0 : allNormalizedIndividuals.get(0).getFitness();
		for (Iterator<GAIndividual> itIndividual = allNormalizedIndividuals.iterator(); itIndividual.hasNext(); ) {
			GAIndividual ind = itIndividual.next();
			if (ind.getFitness() < maxFitness) {
				break;
			}
			if (!printOptima) {
				itIndividual.remove();
			}
			globalOptima.add(ind);
		}
		return globalOptima;
	}

	private List<GAIndividual> computeFitnesses(SimulationRecord sR) throws AnalysisException, ComparisonException {
		final SimulatedExecutor sE = new SimulatedExecutor(sR);
		final AnalysisResults aR = sE.analyze(null);

		final List<GAIndividual> allNormalizedIndividuals = new ArrayList<GAIndividual>();
		final GAHof hof = new GAHof(aR);
		for (GAIndividual ni : new ValidIndividualsList(aR, 1)) {
			final ComparisonResults cmp = sE.compare(null, ni)[0];
			hof.put(cmp);
			allNormalizedIndividuals.add(ni);
		}

		final GAPopulation population = new GAPopulation();
		population.setIndividuals(allNormalizedIndividuals);

		final GAState state = new GAState();
		state.setHof(hof);
		state.setAnalysisResults(hof.getAnalysis());
		state.setTotalNumberOfMutants(hof.size());
		state.updateExecutionMatrix(hof, population);

		final MutantStrengthFitnessFunction fit = new MutantStrengthFitnessFunction();
		for (GAIndividual nInd : allNormalizedIndividuals) {
			final double fitness = fit.computeFitness(nInd, state);
			nInd.setFitness(fitness);
		}

		return allNormalizedIndividuals;
	}

}
