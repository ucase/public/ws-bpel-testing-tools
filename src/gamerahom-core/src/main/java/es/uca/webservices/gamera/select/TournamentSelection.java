package es.uca.webservices.gamera.select;

import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.select.GASelectionOperator;

/**
 * <p>
 * Performs k-sized tournament selection with replacement. By default, it will
 * use binary tournament selection, but it can be customized through
 * {@link #setTournamentSize(int)}. Uniform random selection can be emulated by
 * setting the tournament size to 1.
 * </p>
 * 
 * <p>
 * According to [1], doing tournament selection without replacement would not
 * really change the results for typical GA configurations. For this reason, this
 * class doess not provide an option for disabling replacement.
 * </p>
 * 
 * <p>
 * [1] H. Xie, M. Zhang, P. Andreae, y M. Johnston, «An analysis of
 * multi-sampled issue and no-replacement tournament selection», in In
 * Proceedings of Genetic and Evolutionary Computation Conference (2008), ACM,
 * pp. 1323-1330.
 * </p>
 * 
 * @author Antonio García-Domínguez
 */

@Option(description="Performs k-sized tournament selection with replacement. " +
		"By default, it will use binary tournament selection, but it can be customized " +
		"through #setTournamentSize(int). Uniform random selection can be emulated by " +
		"setting the tournament size to 1.")
public class TournamentSelection implements GASelectionOperator {

	private Random prng;
	private int tournamentSize = 2;

	@Override
	public GAIndividual select(GAPopulation source) {
		final List<GAIndividual> individuals = source.getIndividuals();
		final Integer populationSize = source.getPopulationSize();
		if (source.getIndividuals().isEmpty()) {
			return null;
		}

		GAIndividual bestIndividual = null;
		int bestFitness = Integer.MIN_VALUE;
		for (int iRound = 0; iRound < tournamentSize; ++iRound) {
			final int iIndividual = prng.nextInt(populationSize);
			final GAIndividual ind = individuals.get(iIndividual);
			if (ind.getFitness() > bestFitness) {
				bestIndividual = ind;
			}
		}

		return bestIndividual;
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (tournamentSize < 1) {
			throw new InvalidConfigurationException("Tournament size must be 1 or higher.");
		}
		if (prng == null) {
			throw new InvalidConfigurationException("No PRNG has been set.");
		}
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}

	public int getTournamentSize() {
		return tournamentSize;
	}

	public void setTournamentSize(int tournamentSize) {
		this.tournamentSize = tournamentSize;
	}
}
