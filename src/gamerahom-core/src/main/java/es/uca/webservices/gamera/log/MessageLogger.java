package es.uca.webservices.gamera.log;

import java.util.Arrays;

import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;

/**
 * Simple logger which prints messages to console or to a file.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */

@Option(description="Simple logger which prints messages to console or to a file.")
public class MessageLogger extends AbstractLogger {

    // OVERRIDEN METHODS ///////////////////////////////////

	@Override
	public void newGeneration(GAState state, GAPopulation population) {
		getStream().printMsgByConsoleFile("The algorithm has produced the generation " +
				state.getCurrentGeneration() + ".", state.getElapsedMillis(), isConsole(), getFile());
	}

	@Override
	public void started(GAState state) {
		getStream().printMsgByConsoleFile("The algorithm has started its execution.",
				state.getElapsedMillis(), isConsole(), getFile());
	}

	@Override
	public void finished(GAState state) {
		getStream().printMsgByConsoleFile("The algorithm has finished its execution (" +
				state.getCurrentGeneration() + " generations, " +
				state.getHof().size() + " mutants out of " +
				state.getTotalNumberOfMutants() + ").",
				state.getElapsedMillis(), isConsole(), getFile());
	}

	@Override
	public void startedAnalysis(GAState state) {
		getStream().printMsgByConsoleFile("The algorithm has started to analyze the " +
				"original program.",
				state.getElapsedMillis(), isConsole(), getFile());
	}
	
	@Override
	public void finishedAnalysis(GAState state) {
		getStream().printMsgByConsoleFile("The algorithm has finished analyzing the " +
				"original program.", state.getElapsedMillis(), isConsole(), getFile());
		getStream().printMsgByConsoleFile(
			"Active operators: " + Arrays.toString(state.getAnalysisResults().getOperatorNames()),
			state.getElapsedMillis(), isConsole(), getFile());
	}

	@Override
	public void startedComparison(GAState state, GAPopulation population) {
		getStream().printMsgByConsoleFile(
			"The algorithm has started to compare the " + population.getPopulationSize() +
			" individuals in a new generation with the original program.",
			state.getElapsedMillis(), isConsole(), getFile());
	}

	@Override
	public void finishedComparison(GAState state, GAPopulation population) {
		getStream().printMsgByConsoleFile("The algorithm has finished comparing the " +
				population.getPopulationSize() +
				" individuals in the current generation.",
				state.getElapsedMillis(), isConsole(), getFile());
	}

	@Override
	public void finishedEvaluation(GAState state, GAPopulation population) {
		getStream().printMsgByConsoleFile("The algorithm has finished evaluating the fitness " +
				"of the individuals in the current generation.",
				state.getElapsedMillis(), isConsole(), getFile());
	}

	@Override
	public void startedEvaluation(GAState state, GAPopulation population) {
		getStream().printMsgByConsoleFile("The algorithm has started evaluating the fitness " +
				"of the individuals in the current generation.",
				state.getElapsedMillis(), isConsole(), getFile());
	}
}
