package es.uca.webservices.gamera.term;

import java.io.File;
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.term.GATerminationCondition;
import es.uca.webservices.gamera.record.SimulationRecord;

/**
 * <p>Termination condition that stops the algorithm when a certain percentage of all
 * strong mutants has been generated. By default, it will stop when all strong
 * mutants have been generated. Strong mutants are read from a simulation record.
 * A mutant is "strong" if it does not die or if it is only killed by a test case
 * that does not kill any other mutant.</p>
 *
 * @author Antonio García-Domínguez
 */

@Option(description="Termination condition that stops the algorithm when a " +
		"certain percentage of all strong mutants has been generated. By default, " +
		"it will stop when all strong mutants have been generated. Strong mutants are " +
		"read from a simulation record. A mutant is 'strong' if it does not die or if " +
		"it is only killed by a test case that does not kill any other mutant.")
public class PercentStrongMutantsCondition implements GATerminationCondition {

	private static final Logger LOGGER = LoggerFactory.getLogger(PercentStrongMutantsCondition.class);

	private int totalStrongMutants;
	private double percent = 1;
	private SimulationRecord simRecord;
	private File fSimRecord;
	private Set<GAIndividual> remainingStrongMutants;

	public double getPercent() {
		return percent;
	}

	public void setPercent(double percent) {
		this.percent = percent;
	}

	public File getFile() {
		return fSimRecord;
	}

	public void setFile(File fSimRecord) throws Exception {
		this.fSimRecord = fSimRecord;
		setSimulationRecord(SimulationRecord.loadFrom(fSimRecord));
	}

	public SimulationRecord getSimulationRecord() {
		return simRecord;
	}

	public void setSimulationRecord(SimulationRecord simRecord) {
		this.simRecord = simRecord;

		// Invalidate the remaining strong mutant cache
		remainingStrongMutants = null;
		totalStrongMutants = -1;
	}

	@Override
	public synchronized boolean evaluate(GAState state) {
		if (remainingStrongMutants == null) {
			/*
			 * We need to initialize this field as late as possible, as the set
			 * of strong mutants may have been filtered during the analysis step
			 */
			remainingStrongMutants = simRecord.getStrongMutants();
			totalStrongMutants = remainingStrongMutants.size();
			final double threshold = percent * totalStrongMutants;
			LOGGER.info("Stopping when {} strong mutants are found", Math.ceil(threshold));
		}

		for (Iterator<GAIndividual> iSM = remainingStrongMutants.iterator(); iSM.hasNext(); ) {
			final GAIndividual sm = iSM.next();
			if (state.getHof().containsKey(sm)) {
				iSM.remove();
			}
		}

		final int foundStrongMutants = totalStrongMutants - remainingStrongMutants.size();
		final int goal = (int)(totalStrongMutants * percent);
		return foundStrongMutants >= goal;
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (percent < 0 || percent > 1) {
			throw new InvalidConfigurationException("percent must be within the [0, 1] range");
		}
		if (simRecord == null) {
			throw new InvalidConfigurationException("No simulation record has been set");
		}
	}

}