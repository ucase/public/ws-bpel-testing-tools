package es.uca.webservices.gamera.subcommands;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.ValidIndividualsList;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.exec.SimulatedExecutor;
import es.uca.webservices.gamera.fitness.MutantStrengthFitnessFunction;
import es.uca.webservices.gamera.record.SimulationRecord;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * Produces a table to use with R to analyse fitness-distance within a certain
 * problem space.
 */
public class FitnessTableSubcommand extends AbstractSubcommand {

	private static final int DEFAULT_COVERAGE_MODIFIER = 0;

	private static final String DESCRIPTION =
		"Prints to the standard output stream a table with each individual\n" +
		"and its fitness (assuming full HoF), for later use with R to compute\n" +
		"individual fitness histograms."
	;

	private static final String COVMF_OPTION = "covmod";
	private int coverageModifierFactor = DEFAULT_COVERAGE_MODIFIER;

	public int getCoverageModifierFactor() {
		return coverageModifierFactor;
	}

	public void setCoverageModifierFactor(int coverageModifierFactor) {
		this.coverageModifierFactor = coverageModifierFactor;
	}

	public FitnessTableSubcommand() {
		super(1, 1, "ftable", "simrecord.(raw|yaml)", DESCRIPTION);
	}

	@Override
	protected void parseOptions(OptionSet options) {
		super.parseOptions(options);
		if (options.has(COVMF_OPTION)) {
			setCoverageModifierFactor((int) options.valueOf(COVMF_OPTION));
		}
	}

	@Override
	protected OptionParser createOptionParser() {
		final OptionParser parser = super.createOptionParser();
		parser.accepts(COVMF_OPTION, "Factor applied to surviving cases where mutation is covered")
			.withRequiredArg().ofType(Integer.class).describedAs("F")
			.defaultsTo(DEFAULT_COVERAGE_MODIFIER);

		return parser;
	}

	@Override
	protected void runCommand() throws Exception {
		final File simRecordFile = new File(getNonOptionArgs().get(0));
		final SimulationRecord sR = SimulationRecord.loadFrom(simRecordFile); 
		renderReport(simRecordFile, sR);
	}

	private void renderReport(File simRecordFile, SimulationRecord sR) throws Exception {
		final List<GAIndividual> allNormalizedIndividuals = computeFitnesses(sR);
		renderTable(allNormalizedIndividuals);
	}

	private void renderTable(final List<GAIndividual> allNormalizedIndividuals) {
		PrintStream ps = getOutputStream();
		ps.println("Individual\tFitness");
		for (GAIndividual ind : allNormalizedIndividuals) {
			ps.println(String.format("%s\t%g",
				ind.individualFieldsToString(), ind.getFitness()));
		}
	}

	private List<GAIndividual> computeFitnesses(SimulationRecord sR) throws AnalysisException, ComparisonException {
		final SimulatedExecutor sE = new SimulatedExecutor(sR);
		final AnalysisResults aR = sE.analyze(null);

		final List<GAIndividual> allNormalizedIndividuals = new ArrayList<GAIndividual>();
		final GAHof hof = new GAHof(aR);
		for (GAIndividual ni : new ValidIndividualsList(aR, 1)) {
			final ComparisonResults cmp = sE.compare(null, ni)[0];
			hof.put(cmp);
			allNormalizedIndividuals.add(ni);
		}

		final GAPopulation population = new GAPopulation();
		population.setIndividuals(allNormalizedIndividuals);

		final GAState state = new GAState();
		state.setHof(hof);
		state.setAnalysisResults(hof.getAnalysis());
		state.setTotalNumberOfMutants(hof.size());
		state.updateExecutionMatrix(hof, population);

		final MutantStrengthFitnessFunction fit = new MutantStrengthFitnessFunction();
		fit.setCoverageModifierFactor(coverageModifierFactor);
		for (GAIndividual nInd : allNormalizedIndividuals) {
			final double fitness = fit.computeFitness(nInd, state);
			nInd.setFitness(fitness);
		}

		return allNormalizedIndividuals;
	}

}
