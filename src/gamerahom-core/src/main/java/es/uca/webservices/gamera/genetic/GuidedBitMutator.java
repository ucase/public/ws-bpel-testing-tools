package es.uca.webservices.gamera.genetic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Utility class designed to learn bit probabilities as in the IEEE Transactions
 * on Evolutionary Computation paper.
 */
public class GuidedBitMutator {
	private List<Double> bitProbabilities;

	// Learning factor (0 - keep previous values, 1 - ignore previous values)
	private double learningFactor = 0.7;

	// Sampling rate (0 - keep original value, 1 - always sample from probabilities)
	private double samplingRate = 0.9;

	/**
	 * Updates the bit probabilities based on the next batch of "desirable" values.
	 */
	public void update(Integer... desirableValues) {
		List<Double> newProbabilities = new ArrayList<>();

		int remainingNonZero;
		do {
			remainingNonZero = 0;

			int count = 0;
			for (int i = 0; i < desirableValues.length; i++) {
				if ((desirableValues[i] & 1) == 1) {
					count++;
				}

				desirableValues[i] >>= 1;
				if (desirableValues[i] > 0) {
					remainingNonZero++;
				}
			}

			newProbabilities.add((double)count/desirableValues.length);
		} while (remainingNonZero > 0);

		// no merge yet
		if (learningFactor == 0 || bitProbabilities == null) {
			bitProbabilities = newProbabilities;
		} else {
			final int newLength = Math.max(bitProbabilities.size(), newProbabilities.size());
			final ArrayList<Double> mergedProbabilities = new ArrayList<>(newLength);
			for (int i = 0; i < newLength; i++) {
				double oldProb = 0, newProb = 0;
				if (i < bitProbabilities.size()) {
					oldProb = bitProbabilities.get(i);
				}
				if (i < newProbabilities.size()) {
					newProb = newProbabilities.get(i);
				}

				mergedProbabilities.add((1-learningFactor) * oldProb + learningFactor * newProb);
			}
			bitProbabilities = mergedProbabilities;
		}
	}

	public double getLearningFactor() {
		return learningFactor;
	}

	public void setLearningFactor(double learningFactor) {
		this.learningFactor = learningFactor;
	}

	public int mutate(int original, Random prng) {
		int result = 0;

		int position = 0;
		for (int mask = 1; mask < original; mask <<= 1, position++) {
			if (prng.nextDouble() < samplingRate) {
				if (prng.nextDouble() < getProbability(position)) {
					result = result | mask;
				}
			} else {
				result = result | (original & mask);
			}
		}

		return result;
	}

	public double getSamplingRate() {
		return samplingRate;
	}

	public void setSamplingRate(double samplingRate) {
		this.samplingRate = samplingRate;
	}

	/**
	 * Returns the probability of the bit at <code>position</code> to be a 1, based
	 * on prior "desirable" results. Position 0 is the least significant bit.
	 */
	public double getProbability(int position) {
		return bitProbabilities  != null && position < bitProbabilities.size() ? bitProbabilities.get(position) : 0;
	}

	@Override
	public String toString() {
		return "BitCounter [bitProbabilities=" + bitProbabilities + ", learningFactor=" + learningFactor + "]";
	}
}