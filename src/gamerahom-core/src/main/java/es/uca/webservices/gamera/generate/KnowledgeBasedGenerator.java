package es.uca.webservices.gamera.generate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Map;
import java.util.Random;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.util.kbg.KnowledgeBasedOperatorRoulette;

@Option(description="Randomly generates new individuals, using a "
		+ "weighted distribution in the operator field combining "
		+ "previous and new knowledge, and uniform distributions in the others. " +
		"The number of individuals generated is a percentage of the number of the individuals " +
		"of the original population.")
public class KnowledgeBasedGenerator extends UniformGenerator {

	private KnowledgeBasedOperatorRoulette roulette = new KnowledgeBasedOperatorRoulette(Collections.emptyMap());
	
    public Map<String, Double> getRatios() {
		return roulette.getRatios();
	}

	public void setRatios(Map<String, Double> ratios) {
		this.roulette = new KnowledgeBasedOperatorRoulette(ratios);
		roulette.setPrng(prng);
	}

	public double getKnowledgeRewardScale() {
		return roulette.getRewardScale();
	}

	public void setKnowledgeRewardScale(double scale) {
		roulette.setRewardScale(scale);
	}
    
	@Override
    protected GAIndividual createIndividual(GAState state, int maxOrder) {
    	 // Current order of the individual, [1, maxOrder]
        final int randOrder =  prng.nextInt(maxOrder) + 1;

        BigInteger[] randOperator = new BigInteger[maxOrder];
        BigInteger[] randLocation = new BigInteger[maxOrder];
        BigInteger[] randAttribute = new BigInteger[maxOrder];

        // Complete individual mutants with random numbers (starting with 1) which depend on its maximum values
        final AnalysisResults analysis = state.getAnalysisResults();
        final BigDecimal locationRange = new BigDecimal(analysis.getMaxLoc()).subtract(BigDecimal.ONE);
        final BigDecimal attributeRange = new BigDecimal(analysis.getMaxAtt()).subtract(BigDecimal.ONE);
        for (int i = 0; i < maxOrder; i++) {
			randOperator[i] = roulette.chooseOperator(state);
            randLocation[i] = locationRange.multiply(new BigDecimal(prng.nextDouble())).toBigInteger().add(BigInteger.ONE);
            randAttribute[i] = attributeRange.multiply(new BigDecimal(prng.nextDouble())).toBigInteger().add(BigInteger.ONE);
        }

        return new GAIndividual(randOperator, randLocation, randAttribute, randOrder);
    }

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
		roulette.setPrng(prng);
	}
}