package es.uca.webservices.gamera.conf;

/**
 * Operation modes for the {@link Configuration#validate} method.
 *
 * @author Antonio García Domínguez
 */
public enum ConfigurationValidationMode {
	/** All components should be validated. */
	FULL,
	/** Only the components required by random selection/mutant sampling should be validated. */
	RANDOM_SELECTION
}
