package es.uca.webservices.gamera.select;

import java.util.List;
import java.util.Random;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.select.GASelectionOperator;

/**
 * Selection operator which implements the roulette method. This method consists
 * of choosing an individual from the source population for the new population,
 * summing the fitness of individuals from the individual Z, which is a random
 * individual of the source population, while this sum is lower than Y, which is
 * a random number between zero and the total fitness of the population
 * individuals. The last Z will be the individual selected.
 * 
 * @author Antonio García-Domínguez, Juan José Domínguez-Jiménez, 
 *         Emma Blanco-Muñoz
 */
@Option(description="Selection operator which implements the roulette method." +
		" This method consists of choosing an individual from the source population " +
		"for the new population, summing the fitness of individuals from the individual Z, " +
		"which is a random individual of the source population, while this sum is lower than Y, " +
		"which is a random number between zero and the total fitness of the population individuals. " +
		"The last Z will be the individual selected.")
public class RouletteSelection implements GASelectionOperator {

	private Random prng;

	@Override
	public GAIndividual select(GAPopulation source) {
		// We get the total sum of every individual.
		int totalFitness = 0;
		final List<GAIndividual> individuals = source.getIndividuals();
		
		// Calculate the sum of every individual fitness
		for (GAIndividual ind : individuals) {
			totalFitness += ind.getFitness();
		}
		
		// Random number between 0 and totalFitness
		long y = Math.abs(prng.nextLong() % (totalFitness + 1));
		// Random number between 0 and population size-1
		int z = prng.nextInt(source.getPopulationSize());

		return doRoulette(individuals, y, z);
	}

	GAIndividual doRoulette(final List<GAIndividual> individuals, final long rouletteResult, final int startingPosition) {
		if(startingPosition == individuals.size()) {
			throw new IllegalArgumentException("The starting position should be" +
					"in the [0, population size) range");
		}
		if (rouletteResult == 0) {
			// If the rouletteResult is zero, returns the individual of the 
			// starting position 
			return individuals.get(startingPosition);
		} else {
			// This loop will be produced while the count isn't equal or greater
			// than Y. When it finish, X-1 will be the individual selected.
			final int nIndividuals = individuals.size();

			int x, count = 0;
			for (x = startingPosition; count < rouletteResult; x++) {
				if (x == nIndividuals) {
					x = 0;
				}
				count += individuals.get(x).getFitness();
			}

			// Undo the final increment of the loop
			return individuals.get(x-1);
		}
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		if (prng == null) {
			throw new InvalidConfigurationException("No PRNG has been set yet");
		}
	}

	@Override
	public void setPRNG(Random prng) {
		this.prng = prng;
	}

}
