package es.uca.webservices.gamera.select;

import static org.junit.Assert.assertNotNull;

import java.math.BigInteger;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.generate.UniformGenerator;

/**
 * Tests for the {@link UniformRandomSelection} class.
 * 
 * @author Emma Blanco-Muñoz, Antonio García-Domínguez
 * @version 1.1
 */
public class UniformRandomSelectionTest {

	private UniformRandomSelection selection;
	private UniformGenerator gen;
    private AnalysisResults analysis;
    private GAPopulation population;
	private Random r;
	private GAState state;
    
    private static final int MAX_ORDER = 5;
    private static final int N = 10;

    @Before
    public void setUp() {
    	selection = new UniformRandomSelection();
    	gen = new UniformGenerator();
        population = new GAPopulation();

        r = new Random();
        selection.setPRNG(r);
        gen.setPRNG(r);

    	analysis = new AnalysisResults(
       			new String[] {"a", "b", "c", "d", "e", "f", "g", "h"},
       			new BigInteger[] {BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.valueOf(7), BigInteger.ONE, BigInteger.ONE, BigInteger.ONE},
       			new BigInteger[] {BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.valueOf(8), BigInteger.ONE, BigInteger.ONE, BigInteger.ONE});
		state = new GAState();
		state.setAnalysisResults(analysis);
		
        // Builds a population with five individuals
        for(int i = 0; i < N; i++) {
        	gen.generate(state, MAX_ORDER, null, population);
        }
    }

	@Test
	public void testSelect() throws Exception {
		selection.validate();
		gen.validate();
		
		for(int i = 0; i < 1; i++) {
			// Assign any fitness to the individuals
	        for (GAIndividual ind : population.getIndividuals()) {
				ind.setFitness(r.nextInt(25));
			}
	        // Check that selection returns an individual
			assertNotNull(selection.select(population));
		}
	}

}
