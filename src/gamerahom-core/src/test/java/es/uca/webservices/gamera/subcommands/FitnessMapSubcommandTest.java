package es.uca.webservices.gamera.subcommands;

import org.junit.Test;

import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertFalse;

/**
 * Tests for the {@link FitnessMapSubcommandTest} class.
 *
 * @author Antonio García-Domínguez
 */
public class FitnessMapSubcommandTest {

	@Test
	public void generateReport() throws Exception {
        final ByteArrayOutputStream bOS = new ByteArrayOutputStream();
		final FitnessMapSubcommand cmd = new FitnessMapSubcommand();
		cmd.parseArgs("src/test/resources/loanRPC.simrecord.order1.yaml");
        cmd.setOutputStream(bOS);
		cmd.run();

        final String texOutput = bOS.toString();
        // TODO Remake the report because the fitness of an individual will change depends on other
        // individuals in the execution matrix.
        assertFalse("Output from the Velocity template should include fitnesses", texOutput.contains("$fit"));
	}
}
