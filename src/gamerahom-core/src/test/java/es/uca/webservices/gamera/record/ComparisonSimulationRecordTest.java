package es.uca.webservices.gamera.record;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests for the {@link ComparisonSimulationRecord} class.
 *
 * @author Antonio García-Domínguez
 */
public class ComparisonSimulationRecordTest {

	private static final BigInteger FIRST_CASE_NANOS = BigInteger.valueOf(20);
	private static final BigInteger MIDDLE_CASE_NANOS = BigInteger.valueOf(30);
	private static final BigInteger LAST_CASE_NANOS = BigInteger.valueOf(50);
	private static final BigInteger TOTAL_NANOS = FIRST_CASE_NANOS.add(MIDDLE_CASE_NANOS).add(LAST_CASE_NANOS);
	private ComparisonSimulationRecord cR;

	@Before
	public void setUp() {
		cR = record(TOTAL_NANOS, 0, 0, 0);
		cR.setTestWallNanos(
			new BigInteger[] {
				FIRST_CASE_NANOS,
				MIDDLE_CASE_NANOS,
				LAST_CASE_NANOS
			});
	}

	@Test
	public void surviving_withPartialTimes() {
		assertEquals(cR.getTestCaseCount(), cR.getFirstTestWithDifferentOutput());
		assertEquals(TOTAL_NANOS, cR.getNanosUpToFirstDeath());
	}

	@Test
	public void surviving_noPartialTimes() {
		cR.setTestWallNanos(null);
		assertEquals(cR.getTestCaseCount(), cR.getFirstTestWithDifferentOutput());
		assertEquals(TOTAL_NANOS, cR.getNanosUpToFirstDeath());
	}

	@Test
	public void dead_100() {
		cR.getRow()[0] = 1;
		assertEquals(0, cR.getFirstTestWithDifferentOutput());
		assertEquals(FIRST_CASE_NANOS, cR.getNanosUpToFirstDeath());
	}

	@Test
	public void dead_110() {
		cR.getRow()[0] = 1;
		cR.getRow()[1] = 1;
		assertEquals(0, cR.getFirstTestWithDifferentOutput());
		assertEquals(FIRST_CASE_NANOS, cR.getNanosUpToFirstDeath());
	}

	@Test
	public void dead_111() {
		cR.getRow()[0] = 1;
		cR.getRow()[1] = 1;
		cR.getRow()[2] = 1;
		assertEquals(0, cR.getFirstTestWithDifferentOutput());
		assertEquals(FIRST_CASE_NANOS, cR.getNanosUpToFirstDeath());
	}

	@Test
	public void dead_010() {
		cR.getRow()[1] = 1;
		assertEquals(1, cR.getFirstTestWithDifferentOutput());
		assertEquals(FIRST_CASE_NANOS.add(MIDDLE_CASE_NANOS), cR.getNanosUpToFirstDeath());
	}

	@Test
	public void dead_011() {
		cR.getRow()[1] = 1;
		cR.getRow()[2] = 1;
		assertEquals(1, cR.getFirstTestWithDifferentOutput());
		assertEquals(FIRST_CASE_NANOS.add(MIDDLE_CASE_NANOS), cR.getNanosUpToFirstDeath());
	}

	@Test
	public void dead_001() {
		cR.getRow()[2] = 1;
		assertEquals(2, cR.getFirstTestWithDifferentOutput());
		assertEquals(TOTAL_NANOS, cR.getNanosUpToFirstDeath());
	}

	@Test
	public void aliveDoesNotSubsumeItself() {
		assertFalse(cR.subsumes(cR));
	}

	@Test
	public void deadSubsumesItself() {
		cR.getRow()[0] = 1;
		assertTrue(cR.subsumes(cR));
	}

	@Test
	public void deadSubsumes() {
		cR.getRow()[0] = 1;
		cR.getRow()[1] = 1;

		assertTrue("(1, 0, 0) subsumes (1, 1, 0)", record(1, 0, 0).subsumes(cR));
		assertTrue("(0, 1, 0) subsumes (1, 1, 0)", record(0, 1, 0).subsumes(cR));
		assertFalse("(1, 1, 0) does not subsume (0, 1, 0)", cR.subsumes(record(0, 1, 0)));
		assertFalse("(1, 1, 0) does not subsume (0, 0, 1)", cR.subsumes(record(0, 0, 1)));
	}

	@Test
	public void coverageSubsumes() {
		assertTrue("(0, 1, 0) subsumes (3, 1, 0)", record(0, 1, 0).subsumes(record(3, 1, 0)));
		assertFalse("(3, 1, 0) does not subsume (0, 1, 0)", record(3, 1, 0).subsumes(record(0, 1, 0)));
	}

	protected ComparisonSimulationRecord record(Integer... cells) {
		return record(BigInteger.ZERO, cells);
	}

	protected ComparisonSimulationRecord record(BigInteger time, Integer... cells) {
		return new ComparisonSimulationRecord(cells, time);
	}
}
