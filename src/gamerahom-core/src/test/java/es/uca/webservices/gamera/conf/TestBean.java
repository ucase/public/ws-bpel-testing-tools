package es.uca.webservices.gamera.conf;

/**
 * Test bean for the package prefix constructor tests.
 *
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class TestBean {
    private String   name;
    private String   address;
    private TestBean nested;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public TestBean getNested() {
        return nested;
    }

    public void setNested(TestBean nested) {
        this.nested = nested;
    }
}
