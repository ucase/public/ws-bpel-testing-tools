package es.uca.webservices.gamera.fitness;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.ComparisonResult;

/**
 * Tests for the {@link MutantStrengthFitnessFunction} class.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 */
public class MutantStrengthFitnessFunctionTest {

	private MutantStrengthFitnessFunction fit;

	final ComparisonResult[] sameRow = new ComparisonResult[]{ComparisonResult.SAME_OUTPUT, 
			ComparisonResult.SAME_OUTPUT, ComparisonResult.SAME_OUTPUT};
	final ComparisonResult[] invalidRow = new ComparisonResult[]{ComparisonResult.INVALID, 
			ComparisonResult.INVALID, ComparisonResult.INVALID};
	final ComparisonResult[] deadRow = new ComparisonResult[]{ComparisonResult.SAME_OUTPUT, 
			ComparisonResult.DIFFERENT_OUTPUT, ComparisonResult.SAME_OUTPUT};
	final ComparisonResult[] deadRow2 = new ComparisonResult[]{ComparisonResult.DIFFERENT_OUTPUT, 
			ComparisonResult.DIFFERENT_OUTPUT, ComparisonResult.SAME_OUTPUT};
	private final int nTestCases = sameRow.length;

	@Before
	public void setUp() {
		fit = new MutantStrengthFitnessFunction();
	}

	@Test
	public void invalidIndividualsHaveZeroFitness() {
		assertEquals(0, fit.computeFitness(invalidRow, nTestCases, 1, null), 1e-3);
	}

	@Test
	public void aliveIndividualsHaveMaximumFitness_1x3() {
		assertEquals(3, fit.computeFitness(sameRow, nTestCases, 1, new double[] {0, 0, 0}), 1e-3);
	}

	@Test
	public void deadIndividualFitness() {
		double[] sumExecutionMatrix = new double[]{1, 2, 0};

		assertEquals(4, fit.computeFitness(deadRow, nTestCases, 2, sumExecutionMatrix), 1e-3);
		assertEquals(3, fit.computeFitness(deadRow2, nTestCases, 2, sumExecutionMatrix), 1e-3);
	}

	@Test
	public void deadIndividualFitnessInvalidAreIgnored() {
		double[] sumExecutionMatrix = new double[]{1, 2, 0};

		assertEquals(7, fit.computeFitness(deadRow, nTestCases, 3, sumExecutionMatrix), 1e-3);
		assertEquals(6, fit.computeFitness(deadRow2, nTestCases, 3, sumExecutionMatrix), 1e-3);
		assertEquals(0, fit.computeFitness(invalidRow, nTestCases, 3, sumExecutionMatrix), 1e-3);
	}

	@Test
	public void fitnessIsClippedToZero() {
		fit.setCoverageModifierFactor(-5);
		double[] sumExecutionMatrix = new double[]{0.5, 1, 1};
		assertEquals(0, fit.computeFitness(new ComparisonResult[]{
			ComparisonResult.EXERCISED, ComparisonResult.DIFFERENT_OUTPUT, ComparisonResult.DIFFERENT_OUTPUT
		}, nTestCases, 1, sumExecutionMatrix), 1e-3);
	}

}
