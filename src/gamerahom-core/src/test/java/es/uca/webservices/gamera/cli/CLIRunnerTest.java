package es.uca.webservices.gamera.cli;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import es.uca.webservices.gamera.IntegrationTest;
import es.uca.webservices.gamera.RandomSelectionAlgorithm;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.subcommands.RandomRunSubcommand;
import es.uca.webservices.gamera.subcommands.RandomSimulationRunSubcommand;

/**
 * Tests for the {@link CLIRunner} class.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public class CLIRunnerTest {

    private ByteArrayOutputStream errByteArray;
    private ByteArrayOutputStream outByteArray;
    private PrintStream           oldOut;
    private PrintStream           oldErr;
    
    public CLIRunnerTest(){
    	System.setProperty("install.dir", new File("target/dependency").getAbsolutePath());
    }

    @Before
    public void setUp() {
        // Redirect stdout and stderr so we can test for them
        errByteArray = new ByteArrayOutputStream();
        outByteArray = new ByteArrayOutputStream();
        oldErr = System.err;
        oldOut = System.out;
        System.setErr(new PrintStream(errByteArray));
        System.setOut(new PrintStream(outByteArray));
    }

    @After
    public void cleanUp() {
        // Restore original stdout and stderr
        System.setErr(oldErr);
        System.setOut(oldOut);
    }

    @Test
    public void usagePrintedWhenNoArgs() throws GenerationException, ComparisonException {
        assertEquals(CLIRunner.BAD_USAGE, CLIRunner.mainMethod(new String[] {}));
    }

    @Test
    public void simulationRunFOM() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void simulationRunFOMWithHOFReport() throws Exception {
    	final File tmpHOF = File.createTempFile("hof", ".txt");
    	try {
    		assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", "--hof", tmpHOF.getCanonicalPath(), getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    	} finally {
    		tmpHOF.delete();
    	}
    }

    @Test
    public void simulationRunFOM_popSizeAsPercentOfTotalMutants() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", getResource("popsize-percent-total-mutants.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void simulationRunFOM_30percentStrong() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", "--strong=0.3", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void simulationRunIncludeOperators() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", "--include-ops=eaa-emf", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void simulationRunExcludeOperators() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", "--exclude-ops=eaa-emf", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void simulationRunIncludeTests() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", "--include-tests=2-4", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void simulationRunIncludeTestsFromFile() throws Exception {
    	File tmpIncludePatterns = createTempFile("1\n2-4");
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", "--include-tests-from=" + tmpIncludePatterns.getCanonicalPath(), getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

	@Test
    public void simulationRunIncludeTestsByName() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", "--include-tests=SmallAmountLowRisk-SmallAmountHighRiskRejected", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void simulationRunIncludeTestsByFallbackName() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", "--include-tests=test1-test4", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order2.yaml"}));
    }

    @Test
    public void simulationRunExcludeTests() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"randsimrun", "--exclude-tests=1,5-6", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void simulationRunExcludeTestsByName() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"randsimrun", "--exclude-tests=LargeAmountOK,SmallAmountHighRiskOK", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void simulationRunExcludeTestsByNameFromFile() throws Exception {
    	final File tmpExcludePatterns = createTempFile("LargeAmountOK");
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"randsimrun", "--exclude-tests-from=" + tmpExcludePatterns.getCanonicalPath(), getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void simulationRunExcludeTestsFromEmptyFile() throws Exception {
    	final File tmpExcludePatterns = createTempFile("");
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"randsimrun", "--exclude-tests-from=" + tmpExcludePatterns.getCanonicalPath(), getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void simulationRunFOMWithHOM2Record() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order2.yaml"}));
    }

    @Test
    public void simulationRunHOM2() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", getResource("second-order.yaml"), "src/test/resources/loanRPC.simrecord.order2.yaml"}));
    }

    @Test
    public void simulationRunHOM2WithFOMRecord() throws Exception {
    	assertEquals(CLIRunner.INVALID_CONFIGURATION, CLIRunner.mainMethod(new String[] {"simrun", getResource("second-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    @Category(IntegrationTest.class)
    public void randRun() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"randrun", "--" + RandomRunSubcommand.ALL_OPTION + "=0.3",  getResource("second-order.yaml")}));
    }

    @Test
    @Category(IntegrationTest.class)
    public void randRunStopAtFirstDiff() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"randrun", "--" + RandomSelectionAlgorithm.STOP_AT_FIRST_DIFF_OPTION, "--" + RandomRunSubcommand.ALL_OPTION + "=0.3",  getResource("second-order.yaml")}));
    }

    @Test
    public void randSimRun() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"randsimrun", "--" + RandomSimulationRunSubcommand.STRONG_OPTION + "=0.3", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    public void randSimRunStopAtFirstDiff() throws Exception {
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"randsimrun", "--" + RandomSelectionAlgorithm.STOP_AT_FIRST_DIFF_OPTION, "--" + RandomSimulationRunSubcommand.STRONG_OPTION + "=0.3", getResource("first-order.yaml"), "src/test/resources/loanRPC.simrecord.order1.yaml"}));
    }

    @Test
    @Category(IntegrationTest.class)
    public void regularExecution() throws Exception {
		assertEquals(0, CLIRunner.mainMethod(new String[] {getResource("all-ok.yaml")}));
    }

	@Test
	@Category(IntegrationTest.class)
    public void simulationRecordsFOM() throws Exception {
		// A first-order simulation record can only be used to simulate first-order runs
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrecord", getResource("first-order.yaml"), "target/loanRPC.simrecord.order1.yaml"}));
    	assertEquals(CLIRunner.INVALID_CONFIGURATION, CLIRunner.mainMethod(new String[] {"simrun", getResource("second-order.yaml"), "target/loanRPC.simrecord.order1.yaml"}));
       assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", getResource("first-order.yaml"), "target/loanRPC.simrecord.order1.yaml"}));
    }

	@Test
	@Category(IntegrationTest.class)
    public void simulationRecordsFOMRaw() throws Exception {
		// A first-order simulation record can only be used to simulate first-order runs
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrecord", getResource("first-order.yaml"), "target/loanRPC.simrecord.order1.raw"}));
    	assertEquals(CLIRunner.INVALID_CONFIGURATION, CLIRunner.mainMethod(new String[] {"simrun", getResource("second-order.yaml"), "target/loanRPC.simrecord.order1.raw"}));
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", getResource("first-order.yaml"), "target/loanRPC.simrecord.order1.raw"}));
    }

	@Test
	@Category(IntegrationTest.class)
    public void simulationRecordsHOM2() throws Exception {
		// A second-order simulation record can be used to simulate first-order and second-order runs
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrecord", getResource("second-order.yaml"), "target/loanRPC.simrecord.order2.yaml"}));
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", getResource("second-order.yaml"), "target/loanRPC.simrecord.order2.yaml"}));
    	assertEquals(0, CLIRunner.mainMethod(new String[] {"simrun", getResource("first-order.yaml"), "target/loanRPC.simrecord.order2.yaml"}));
	}

	private File createTempFile(final String contents) throws IOException, FileNotFoundException {
		File tmpIncludePatterns = File.createTempFile("patterns", ".txt");
		tmpIncludePatterns.deleteOnExit();
	
		PrintStream ps = null;
		try {
			ps =  new PrintStream(new FileOutputStream(tmpIncludePatterns));
			ps.print(contents);
			return tmpIncludePatterns;
		} finally {
			if (ps != null) {
				ps.close();
			}
		}
	}

	private String getResource(final String fileBasename) {
		return CLIRunner.class.getResource("/" + fileBasename).getPath();
	}
}
