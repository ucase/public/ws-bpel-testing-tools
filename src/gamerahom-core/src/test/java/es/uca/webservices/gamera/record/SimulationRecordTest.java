package es.uca.webservices.gamera.record;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.util.ranges.IntegerRangeListFilter;
import es.uca.webservices.gamera.api.util.ranges.StringArrayRangeListFilter;
import es.uca.webservices.gamera.exec.bpel.BPELExecutor;
import es.uca.webservices.gamera.record.MutantSubsumptionRecord.SubsumingMutant;

public class SimulationRecordTest {

	private static final BigInteger DEFAULT_TEST_NANOS = BigInteger.valueOf(100);

	@Test
	public void saveLoadIndividual() throws Exception {
		final Yaml yaml = new Yaml(new SimulationRecordConstructor(GAIndividual.class), new SimulationRecordRepresenter());
		final GAIndividual ind = new GAIndividual(1, 2, 3, false);
		ind.setFitness(5);
		final String dumped = yaml.dump(ind);
		final GAIndividual loaded = (GAIndividual)yaml.load(dumped);
		assertEquals(ind, loaded);
		assertEquals(ind.getFitness(), loaded.getFitness(), 1e-3);
	}

	@Test
	public void saveLoadIndividualWithOperatorName() throws Exception {
		final Yaml yaml = new Yaml(
			new SimulationRecordRepresenter(new String[] { "OP" })
		);
		final GAIndividual ind = new GAIndividual(1, 2, 3, false);
		ind.setFitness(5);
		final String dumped = yaml.dump(ind);
		assertTrue(dumped.contains("OP"));
	}

	@Test
	public void saveLoadTest() throws Exception {
		final SimulationRecord record = createBasicRecord();
		assertModelIsSavedAndLoadedCorrectly(record, new File("saved.yaml"));
	}

	@Test
	public void saveLoadTestRawFormat() throws Exception {
		final SimulationRecord record = createBasicRecord();
		assertModelIsSavedAndLoadedCorrectly(record, new File("saved.raw"));
	}

	@Test
	public void strongMutantsAreCorrectlyComputed_empty() {
		final SimulationRecord r = new SimulationRecord();
		assertEquals(0, r.getStrongMutants().size());
	}

	@Test
	public void strongMutantsAreCorrectlyComputed_oneAlive() {
		final SimulationRecord r = new SimulationRecord();
		r.addComparisonRecord(new ComparisonResults(1, 1, 1, 0, 0, 0), DEFAULT_TEST_NANOS);
		assertEquals(1, r.getStrongMutants().size());
	}

	@Test
	public void strongMutantsAreCorrectlyComputed_oneDeadByOneCase() {
		final SimulationRecord r = new SimulationRecord();
		r.addComparisonRecord(new ComparisonResults(1, 1, 1, 0, 1, 0), DEFAULT_TEST_NANOS);
		assertEquals(1, r.getStrongMutants().size());
	}

	@Test
	public void strongMutantsAreCorrectlyComputed_oneDeadByTwoCases() {
		final SimulationRecord r = new SimulationRecord();
		r.addComparisonRecord(new ComparisonResults(1, 1, 1, 0, 1, 1), DEFAULT_TEST_NANOS);
		assertEquals(0, r.getStrongMutants().size());
	}

	@Test
	public void strongMutantsAreCorrectlyComputed_twoDeadByOneDifferentCase() {
		final SimulationRecord r = new SimulationRecord();
		r.addComparisonRecord(new ComparisonResults(1, 1, 1, 0, 1, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(1, 1, 2, 0, 0, 1), DEFAULT_TEST_NANOS);
		assertEquals(2, r.getStrongMutants().size());
	}

	@Test
	public void strongMutantsAreCorrectlyComputed_twoDeadByOneSharedCase() {
		final SimulationRecord r = new SimulationRecord();
		r.addComparisonRecord(new ComparisonResults(1, 1, 1, 0, 1, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(1, 1, 2, 0, 1, 0), DEFAULT_TEST_NANOS);
		assertEquals(0, r.getStrongMutants().size());
	}

	@Test
	public void strongMutantsAreCorrectlyComputed_twoDeadByOneDifferentCase_oneInvalid() {
		final SimulationRecord r = new SimulationRecord();
		r.addComparisonRecord(new ComparisonResults(1, 1, 1, 0, 1, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(1, 1, 2, 0, 0, 1), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(1, 1, 3, 2, 2, 2), DEFAULT_TEST_NANOS);
		assertEquals(2, r.getStrongMutants().size());
	}

	@Test
	public void strongMutantsAreCorrectlyComputed_twoDeadByOneSharedCase_oneInvalid() {
		final SimulationRecord r = new SimulationRecord();
		r.addComparisonRecord(new ComparisonResults(1, 1, 1, 0, 1, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(1, 1, 2, 0, 1, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(1, 1, 3, 2, 2, 2), DEFAULT_TEST_NANOS);
		assertEquals(0, r.getStrongMutants().size());
	}

	@Test
	public void strongMutantsOneCol() {
		final SimulationRecord r = new SimulationRecord();
		r.addComparisonRecord(new ComparisonResults(1, 1, 1, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(1, 1, 2, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(1, 1, 3, 1), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(1, 1, 4, 1), DEFAULT_TEST_NANOS);
		assertEquals(2, r.getStrongMutants().size());
	}

	@Test
	public void strongMutantsAreCorrectlyComputed_mix() {
		final SimulationRecord r = createFilterStrongRecord();
		assertEquals(3, r.getStrongMutants().size());
	}

	@Test
	public void strongMutantsAreCorrectlycomputed_mix_excludeFirstOperator() {
		final SimulationRecord r = createFilterStrongRecord();
		r.filterOperators(new StringArrayRangeListFilter(Arrays.asList(r.getAnalysisRecord().getOperatorNames()), null, "a"));
		assertArrayEquals(bi(0, 2, 1), r.getAnalysisRecord().getLocationCounts());
		assertEquals(3, r.getComparisonRecords().size());
		assertEquals(3, r.getStrongMutants().size());
	}

	@Test
	public void strongMutantsAreCorrectlycomputed_mix_includeFirstLast() {
		final SimulationRecord r = createFilterStrongRecord();
		r.filterOperators(new StringArrayRangeListFilter(Arrays.asList(r.getAnalysisRecord().getOperatorNames()), "1,3"));
		assertArrayEquals(bi(1, 0, 1), r.getAnalysisRecord().getLocationCounts());
		assertEquals(4, r.getComparisonRecords().size());
		assertEquals(1, r.getStrongMutants().size());
	}

	@Test
	public void excludeFirstTest() {
		final SimulationRecord r = createFilterRecord();
		r.filterTests(new IntegerRangeListFilter(1,	r.getTestCaseCount(), null, "1"));
		assertTwoLastTestsInFilterRecord(r);
	}

	@Test
	public void includeTwoLastTests() {
		final SimulationRecord r = createFilterRecord();
		r.filterTests(new IntegerRangeListFilter(1,	r.getTestCaseCount(), "2-3"));
		assertTwoLastTestsInFilterRecord(r);
	}

	@Test
	public void subsumptionNonSubsuming() {
		final SimulationRecord r = new SimulationRecord();

		final int operator = 1;
		final int attribute = 1;
		r.addComparisonRecord(new ComparisonResults(operator, 1, attribute, 1, 0, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(operator, 2, attribute, 0, 1, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(operator, 3, attribute, 0, 0, 1), DEFAULT_TEST_NANOS);

		assertEquals(3, r.computeSubsumingMutants().getSubsumingMutants().size());
	}

	@Test
	public void subsumptionRemoveOneSubsuming() {
		final SimulationRecord r = new SimulationRecord();

		final int operator = 1;
		final int attribute = 1;
		r.addComparisonRecord(new ComparisonResults(operator, 1, attribute, 1, 0, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(operator, 2, attribute, 1, 1, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(operator, 3, attribute, 0, 0, 1), DEFAULT_TEST_NANOS);

		MutantSubsumptionRecord subsumingMutants = r.computeSubsumingMutants();
		assertEquals(2, subsumingMutants.getSubsumingMutants().size());

		SubsumingMutant subsumingMutant = subsumingMutants.getSubsumingMutants().get(Arrays.asList(1, 0, 0));
		assertEquals(1, subsumingMutant.getSubsuming().getLocation().get(0).intValue());
		assertEquals(1, subsumingMutant.getSubsumed().size());
	}

	@Test
	public void subsumptionRemoveLive() {
		final SimulationRecord r = new SimulationRecord();

		final int operator = 1;
		final int attribute = 1;
		r.addComparisonRecord(new ComparisonResults(operator, 1, attribute, 1, 0, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(operator, 2, attribute, 0, 1, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(operator, 3, attribute, 0, 0, 0), DEFAULT_TEST_NANOS);

		MutantSubsumptionRecord subsumingMutants = r.computeSubsumingMutants();
		assertEquals(2, subsumingMutants.getSubsumingMutants().size());
		assertEquals(1, subsumingMutants.getLiveMutants().size());
	}

	@Test
	public void subsumptionRemoveDuplicate() {
		final SimulationRecord r = new SimulationRecord();

		final int operator = 1;
		final int attribute = 1;
		r.addComparisonRecord(new ComparisonResults(operator, 1, attribute, 1, 0, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(operator, 2, attribute, 1, 0, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(operator, 3, attribute, 0, 1, 0), DEFAULT_TEST_NANOS);

		MutantSubsumptionRecord subsumingMutants = r.computeSubsumingMutants();
		assertEquals(1, subsumingMutants.getSubsumingMutants().size());
		assertEquals(2, subsumingMutants.getDuplicateMutants().get(Arrays.asList(1, 0, 0)).size());
	}

	@Test
	public void dumpSubsumptionAsYaml() {
		MutantSubsumptionRecord subRecord = new MutantSubsumptionRecord();

		subRecord.getLiveMutants().add(new GAIndividual(1, 1, 1));
		subRecord.getDuplicateMutants().put(Arrays.asList(0, 1, 0), new HashSet<>(Arrays.asList(
			new GAIndividual(1, 2, 1),
			new GAIndividual(1, 2, 2)
		)));
		SubsumingMutant value = new SubsumingMutant();
		value.setSubsuming(new GAIndividual(3, 1, 1));
		subRecord.getSubsumingMutants().put(Arrays.asList(1, 0, 0), value);

		Yaml yaml = new Yaml(new SimulationRecordRepresenter());
		String sYaml = yaml.dump(subRecord);
		assertTrue(sYaml.contains("liveMutants"));
		assertTrue(sYaml.contains("duplicateMutants"));
		assertTrue(sYaml.contains("subsumingMutants"));
	}

	private void assertModelIsSavedAndLoadedCorrectly(	final SimulationRecord record, final File tmpFile) throws Exception {
		tmpFile.deleteOnExit();
		record.saveTo(tmpFile);
		assertEquals(record, SimulationRecord.loadFrom(tmpFile));
	}

	private void assertTwoLastTestsInFilterRecord(final SimulationRecord r) {
		final List<ComparisonSimulationRecord> records
			= new ArrayList<ComparisonSimulationRecord>(r.getComparisonRecords().values());
		assertArrayEquals(new Integer[]{1, 0}, records.get(0).getRow());
		assertArrayEquals(bi(2, 3), records.get(0).getTestWallNanos());
		assertEquals(BigInteger.valueOf(5), records.get(0).getTotalWallNanos());
		assertEquals(BigInteger.valueOf(2), records.get(0).getNanosUpToFirstDeath());

		assertArrayEquals(new Integer[]{1, 0}, records.get(1).getRow());
		assertArrayEquals(bi(5, 6), records.get(1).getTestWallNanos());
		assertEquals(BigInteger.valueOf(11), records.get(1).getTotalWallNanos());
		assertEquals(BigInteger.valueOf(5), records.get(1).getNanosUpToFirstDeath());

		assertArrayEquals(new Integer[]{0, 1}, records.get(2).getRow());
		assertArrayEquals(bi(8, 9), records.get(2).getTestWallNanos());
		assertEquals(BigInteger.valueOf(17), records.get(2).getTotalWallNanos());
		assertEquals(BigInteger.valueOf(17), records.get(2).getNanosUpToFirstDeath());
		assertEquals(1, records.get(2).getFirstTestWithDifferentOutput());
	}

	private BigInteger[] bi(int... values) {
		final BigInteger[] results = new BigInteger[values.length];
		for (int i = 0; i < values.length; ++i) {
			results[i] = BigInteger.valueOf(values[i]);
		}
		return results;
	}

	private SimulationRecord createBasicRecord() {
		final SimulationRecord record = new SimulationRecord();

		final BPELExecutor executor = new BPELExecutor();
		executor.setPathToOdeWar("path/to/ode.war");
		record.setExecutor(executor);
		record.setRecorder("Fake recorder");
		record.setMaxOrder(1);
		record.setCleanupWallNanos(BigInteger.valueOf(200));
		record.setPrepareWallNanos(BigInteger.valueOf(300));
		record.setTestCaseNames(Arrays.asList("first", "second", "third"));
		record.setAnalysisRecord(new AnalysisSimulationRecord(
			new AnalysisResults(
				new String[] {"a", "b", "c"},
				new BigInteger[] { BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(4)},
				new BigInteger[] { BigInteger.ONE, BigInteger.valueOf(4), BigInteger.valueOf(5) }),
			BigInteger.valueOf(500)));
	
		final ComparisonResults firstCR = new ComparisonResults(
			new GAIndividual(1, 1, 1, true),
			new ComparisonResult[] { ComparisonResult.SAME_OUTPUT });
		firstCR.setTestWallNanos(new BigInteger[] { BigInteger.valueOf(42) });
		record.addComparisonRecord(firstCR, DEFAULT_TEST_NANOS);

		record.addComparisonRecord(
			new ComparisonResults(
				new GAIndividual(1, 1, 2),
				new ComparisonResult[] { ComparisonResult.DIFFERENT_OUTPUT }),
			DEFAULT_TEST_NANOS);
		return record;
	}

	private SimulationRecord createFilterRecord() {
		final SimulationRecord r = new SimulationRecord();

		final ComparisonResults cr1 = new ComparisonResults(1, 1, 1, 0, 1, 0);
		cr1.setTestWallNanos(bi(1, 2, 3));
		r.addComparisonRecord(cr1, BigInteger.valueOf(6));

		final ComparisonResults cr2 = new ComparisonResults(1, 1, 2, 1, 1, 0);
		cr2.setTestWallNanos(bi(4, 5, 6));
		r.addComparisonRecord(cr2, BigInteger.valueOf(15));

		final ComparisonResults cr3 = new ComparisonResults(1, 1, 3, 0, 0, 1);
		cr3.setTestWallNanos(bi(7, 8, 9));
		r.addComparisonRecord(cr3, BigInteger.valueOf(24));
		return r;
	}

	private SimulationRecord createFilterStrongRecord() {
		final SimulationRecord r = new SimulationRecord();
		r.setAnalysisRecord(new AnalysisSimulationRecord(
			new AnalysisResults(
				new String[] { "a", "b", "c" },
				new BigInteger[] { BigInteger.ONE, BigInteger.valueOf(2), BigInteger.ONE },
				new BigInteger[] { BigInteger.valueOf(3), BigInteger.ONE, BigInteger.ONE }
			), BigInteger.ZERO));
		r.addComparisonRecord(new ComparisonResults(1, 1, 1, 0, 1, 0, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(1, 1, 2, 0, 1, 1, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(1, 1, 3, 2, 2, 2, 2), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(2, 1, 1, 1, 0, 0, 0), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(2, 2, 1, 0, 0, 0, 1), DEFAULT_TEST_NANOS);
		r.addComparisonRecord(new ComparisonResults(3, 1, 1, 0, 0, 0, 0), DEFAULT_TEST_NANOS);
		return r;
	}

}
