package es.uca.webservices.gamera.genetic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.util.Pair;
import es.uca.webservices.gamera.log.AncestryLogger;

/**
 * Tests for the {@link OrderCrossoverOperator} class.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.1
 */
public class OrderCrossoverOperatorTest {
    private GAIndividual mom, dad;
    private BigInteger[] momOldOp, momOldLoc, momOldAtt, momNewOp, momNewLoc, momNewAtt;
    private BigInteger[] dadOldOp, dadOldLoc, dadOldAtt, dadNewOp, dadNewLoc, dadNewAtt;
    private int order, crossoverPoint;
    
    private boolean mutateAttribute = true;
    private boolean createTree = false;
    
	private static final int CROSS_OPERATOR = 1;
	private static final int CROSS_LOCATION = CROSS_OPERATOR + 1;
	private static final int CROSS_ATTRIBUTE = CROSS_LOCATION + 1;

	private static final int MAX_LOCATION = 9;
	private static final int MAX_ATTRIBUTE = 9;

	private AnalysisResults analysis = new AnalysisResults(
		new String[] { "a", "b", "c", "d", "e" },
		new BigInteger[] { BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION)},
		new BigInteger[] { BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE)}
	);

    private AncestryLogger logger = new AncestryLogger();   
    private GAState state = new GAState();
    private OrderCrossoverOperator operator;

    private BigInteger[] a = {BigInteger.valueOf(1),BigInteger.valueOf(2),
        BigInteger.valueOf(3), BigInteger.valueOf(4),BigInteger.valueOf(5)};
    private BigInteger[] b = {BigInteger.valueOf(6),BigInteger.valueOf(7),
        BigInteger.valueOf(8), BigInteger.valueOf(9),BigInteger.valueOf(1)};
    private BigInteger[] c = {BigInteger.valueOf(9),BigInteger.valueOf(8),
        BigInteger.valueOf(7), BigInteger.valueOf(6),BigInteger.valueOf(5)};

    @Before
    public void setUp() {
    	operator = new OrderCrossoverOperator();
    	operator.setPRNG(new Random(0));
    }
 
    @Test
    public void applyingValidCopyOfOrderCrossover() {
        
    	for(int i = 0; i < 1000; i++) {
	    	generateValidValues();
	    	
			dadOldOp  = (BigInteger[])dad.getOperator().toArray();
			dadOldLoc = (BigInteger[])dad.getLocation().toArray();
			dadOldAtt = (BigInteger[])dad.getAttribute().toArray();
			momOldOp  = (BigInteger[])mom.getOperator().toArray();
			momOldLoc = (BigInteger[])mom.getLocation().toArray();
			momOldAtt = (BigInteger[])mom.getAttribute().toArray();

			final Pair<GAIndividual, GAIndividual> children = operator.doCrossover(dad, mom, crossoverPoint);

			dadNewOp  = (BigInteger[])children.getLeft().getOperator().toArray();
			dadNewLoc = (BigInteger[])children.getLeft().getLocation().toArray();
			dadNewAtt = (BigInteger[])children.getLeft().getAttribute().toArray();
			momNewOp  = (BigInteger[])children.getRight().getOperator().toArray();
			momNewLoc = (BigInteger[])children.getRight().getLocation().toArray();
			momNewAtt = (BigInteger[])children.getRight().getAttribute().toArray();
	
			// Check the changes
    		// Zero means that they are equal
			switch (crossoverPoint) {
				case CROSS_OPERATOR:
					for(int k = 0; k < dad.getMaxOrder(); k++) {
						assertEquals(dadOldOp[k], dadNewOp[k]);
						assertEquals(dadOldLoc[k], momNewLoc[k]);
						assertEquals(dadOldAtt[k], momNewAtt[k]);
						assertEquals(momOldOp[k], momNewOp[k]);
						assertEquals(momOldLoc[k], dadNewLoc[k]);
						assertEquals(momOldAtt[k], dadNewAtt[k]);
					}
					break;
				case CROSS_LOCATION:
					for(int k = 0; k < dad.getMaxOrder(); k++) {
						assertEquals(dadOldOp[k], dadNewOp[k]);
						assertEquals(dadOldLoc[k], dadNewLoc[k]);
						assertEquals(dadOldAtt[k], momNewAtt[k]);
						assertEquals(momOldOp[k], momNewOp[k]);
						assertEquals(momOldLoc[k], momNewLoc[k]);
						assertEquals(momOldAtt[k], dadNewAtt[k]);
					}
					break;
				default:
					break;
			}
    	}
    }

	@Test
	public void testCrossover() {
		generateValidValues();

		GAPopulation population = new GAPopulation();
		population.addIndividual(dad);
		population.addIndividual(mom);
		
		operator.apply(state, population, population, logger, mutateAttribute, createTree);
		assertSame(dad, population.getIndividuals().get(0));
		assertSame(mom, population.getIndividuals().get(1));

		final GAIndividual firstChild = population.getIndividuals().get(2);
		assertNotNull(firstChild);
		assertNotSame(dad, firstChild);
		assertNotSame(mom, firstChild);

		final GAIndividual secondChild = population.getIndividuals().get(3);
		assertNotNull(secondChild);
		assertNotSame(dad, secondChild);
		assertNotSame(mom, secondChild);

		assertTrue(population.getPopulationSize() == 4);
		
	}

    @Test (expected = IllegalArgumentException.class)
    public void applyingWrongCopyOfOrderCrossover_CPisLowerThanMinAllowed() {
    	generateValidValues();
    	crossoverPoint = CROSS_OPERATOR - 1;
    	
    	operator.doCrossover(mom, dad, crossoverPoint);
    	// It's not necessary checking the values because the operator won't be executed.	
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void applyingWrongCopyOfOrderCrossover_CPisGreaterThanMaxAllowed() {
    	generateValidValues();
    	crossoverPoint = CROSS_ATTRIBUTE;
    	
    	operator.doCrossover(mom, dad, crossoverPoint);
    	// It's not necessary checking the values because the operator won't be executed.	
    }
    
	private void generateValidValues() {
		final Random prng = new Random(0);
		
		state.setAnalysisResults(analysis);
	    logger.setConsole(true);
	    
		Collections.shuffle(Arrays.asList(a));
		Collections.shuffle(Arrays.asList(b));
		Collections.shuffle(Arrays.asList(c));
		order = prng.nextInt(a.length) + 1;

        mom = new GAIndividual(a, b, c, order);
        mom.setFitness(8);
        
		Collections.shuffle(Arrays.asList(a));
		Collections.shuffle(Arrays.asList(b));
		Collections.shuffle(Arrays.asList(c));
		order = prng.nextInt(a.length) + 1;
        
        dad = new GAIndividual(a, b, c, order);
        dad.setFitness(15);

		crossoverPoint = CROSS_OPERATOR + prng.nextInt(CROSS_LOCATION - CROSS_OPERATOR + 1);
	}

}
