package es.uca.webservices.gamera.select;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Tests for the {@link TournamentSelection} class.
 *
 * @author Antonio García-Domínguez
 */
public class TournamentSelectionTest {

	private TournamentSelection ts;

	@Before
	public void setUp() {
		ts = new TournamentSelection();
		ts.setPRNG(new Random(0));
	}

	@Test(expected=InvalidConfigurationException.class)
	public void nullPRNGIsInvalid() throws Exception {
		ts.setPRNG(null);
		ts.validate();
	}

	@Test(expected=InvalidConfigurationException.class)
	public void negativeSizeIsInvalid() throws Exception {
		ts.setTournamentSize(-1);
		ts.validate();
	}

	@Test(expected=InvalidConfigurationException.class)
	public void zeroSizeIsInvalid() throws Exception {
		ts.setTournamentSize(0);
		ts.validate();
	}

	@Test
	public void nullWithEmptyPopulation() throws Exception {
		ts.validate();

		final GAPopulation pop = new GAPopulation();
		assertNull(ts.select(pop));
	}

	@Test
	public void populationWith1Individual() throws Exception {
		ts.validate();

		final GAPopulation pop = new GAPopulation();
		final GAIndividual i1 = new GAIndividual(1, 1, 1);
		pop.addIndividual(i1);
		i1.setFitness(42);
		assertSame(i1, ts.select(pop));
	}

	@Test
	public void populationWith2Individuals() throws Exception {
		ts.validate();

		final GAPopulation pop = new GAPopulation();
		final GAIndividual i1 = new GAIndividual(1, 1, 1);
		final GAIndividual i2 = new GAIndividual(1, 1, 2);
		i1.setFitness(1);
		i2.setFitness(5);
		pop.addIndividual(i1);
		pop.addIndividual(i2);

		assertSame(i2, ts.select(pop));
	}

	@Test
	public void populationWith3Individuals() throws Exception {
		ts.validate();

		final GAPopulation pop = new GAPopulation();
		final GAIndividual i1 = new GAIndividual(1, 1, 1);
		final GAIndividual i2 = new GAIndividual(1, 1, 2);
		final GAIndividual i3 = new GAIndividual(1, 1, 3);
		i1.setFitness(1);
		i2.setFitness(5);
		i3.setFitness(10);
		pop.addIndividual(i1);
		pop.addIndividual(i2);
		pop.addIndividual(i3);

		for (int i = 0; i < 10; ++i) {
			final GAIndividual selected = ts.select(pop);
			assertTrue(i1 == selected || i2 == selected || i3 == selected);
		}
	}

}
