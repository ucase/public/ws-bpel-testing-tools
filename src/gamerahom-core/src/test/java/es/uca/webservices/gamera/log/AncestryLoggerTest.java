package es.uca.webservices.gamera.log;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.math.BigInteger;
import java.util.Arrays;

import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Tests for the {@link AncestryLogger} class.
 *
 * @author Emma Blanco-Muñoz
 * @version 1.0
 */
public class AncestryLoggerTest {

	private static final String OUTPUT_TXT = "output_AncestryLoggerTest.txt";
	private GAIndividual parent, child;
	private int order = 2;

	private static final BigInteger[] OPERATOR_FIELDS = {
		BigInteger.valueOf(1),BigInteger.valueOf(2),
		BigInteger.valueOf(3), BigInteger.valueOf(4),
		BigInteger.valueOf(5)
	};
	private static final BigInteger[] LOCATION_FIELDS = {
		BigInteger.valueOf(6),BigInteger.valueOf(7),
		BigInteger.valueOf(8), BigInteger.valueOf(9),
		BigInteger.valueOf(1)
	};
	private static final BigInteger[] ATTRIBUTE_FIELDS = {
		BigInteger.valueOf(9),BigInteger.valueOf(8),
		BigInteger.valueOf(7), BigInteger.valueOf(6),
		BigInteger.valueOf(5)
	};

	private AnalysisResults analysis = new AnalysisResults(
			new String[] { "a", "b", "c", "d", "e" },
			LOCATION_FIELDS, ATTRIBUTE_FIELDS
	);

	private GAState state = new GAState();

	@Test
	public void onlyConsoleIsValid() throws InvalidConfigurationException {
		new AncestryLogger().validate();
	}

	@Test
	public void showByConsole() throws InvalidConfigurationException {
		initialize();
		AncestryLogger logger = new AncestryLogger();
		logger.validate();
		logger.appliedGeneticOperator(state, "Operador Genético", 
				Arrays.asList(parent), Arrays.asList(child));
	}

	@Test
	public void onlyFileIsValid() throws InvalidConfigurationException {
		AncestryLogger logger = new AncestryLogger();
		logger.setConsole(false);
		logger.setFile(OUTPUT_TXT);
		logger.validate();
	}

	@Test(expected=InvalidConfigurationException.class)
	public void noConsoleAndNoFileIsInvalid() throws InvalidConfigurationException {
		AncestryLogger logger = new AncestryLogger();
		logger.setConsole(false);
		logger.validate();
	}

	@Test(expected=InvalidConfigurationException.class)
	public void ConsoleAndFileIsInvalid() throws InvalidConfigurationException {
		AncestryLogger logger = new AncestryLogger();
		logger.setFile(OUTPUT_TXT);
		logger.validate();
	}

	@Test
	public void FileIsGenerated() throws InvalidConfigurationException, InterruptedException{
		initialize();
		AncestryLogger logger = new AncestryLogger();
		logger.setConsole(false);
		logger.setFile(OUTPUT_TXT);

		logger.appliedGeneticOperator(state, "Operador Genético", 
				Arrays.asList(parent), Arrays.asList(child));

		// Sleep for a second to check the timestamp
		Thread.sleep(1000);
		logger.finishedAnalysis(new GAState());
		assertTrue(new File(OUTPUT_TXT).exists() == true);
	}

	private void initialize() {
		state.setAnalysisResults(analysis);
		parent = new GAIndividual(OPERATOR_FIELDS, LOCATION_FIELDS, ATTRIBUTE_FIELDS, order);
		child = new GAIndividual(OPERATOR_FIELDS, LOCATION_FIELDS, ATTRIBUTE_FIELDS, order+1);
	}
}
