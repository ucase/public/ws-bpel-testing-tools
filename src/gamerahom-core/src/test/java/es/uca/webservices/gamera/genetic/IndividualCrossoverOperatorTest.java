package es.uca.webservices.gamera.genetic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.util.Pair;
import es.uca.webservices.gamera.log.AncestryLogger;

/**
 * Tests for the {@link IndividualCrossoverOperator} class.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public class IndividualCrossoverOperatorTest {

    private GAIndividual mom, dad;
    private AncestryLogger logger = new AncestryLogger();   
    private GAState state = new GAState();
    
    private int order, crossoverPoint, mutatedField;

    private boolean mutateAttribute = true;
    private boolean createTree = false;
    
	private final int XOVER_OPERATOR = 1;
    private final int XOVER_LOCATION = 2;
	private final int XOVER_ATTRIBUTE = 3;	
	
	private static final int MAX_LOCATION = 9;
	private static final int MAX_ATTRIBUTE = 9;

	private AnalysisResults analysis = new AnalysisResults(
		new String[] { "a", "b", "c", "d", "e" },
		new BigInteger[] { BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION)},
		new BigInteger[] { BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE)}
	);
    IndividualCrossoverOperator operator = new IndividualCrossoverOperator();
	
    BigInteger[] a = {BigInteger.valueOf(1),BigInteger.valueOf(2),
        BigInteger.valueOf(3), BigInteger.valueOf(4),BigInteger.valueOf(5)};
    BigInteger[] b = {BigInteger.valueOf(6),BigInteger.valueOf(7),
        BigInteger.valueOf(8), BigInteger.valueOf(9),BigInteger.valueOf(1)};
    BigInteger[] c = {BigInteger.valueOf(9),BigInteger.valueOf(8),
        BigInteger.valueOf(7), BigInteger.valueOf(6),BigInteger.valueOf(5)};

    @Test
    public void applyingValidIndividualCrossover() {
    	for(int i = 0; i < 1000; i++) {
    		generateValidValues();
    		   	    
    		final BigInteger dadOldValue = getXOverValue(dad);
    		final BigInteger momOldValue = getXOverValue(mom);
    		final Pair<GAIndividual, GAIndividual> children = operator.doCrossover(dad, mom, crossoverPoint, mutatedField);
    		final BigInteger dadNewValue = getXOverValue(children.getLeft());
    		final BigInteger momNewValue = getXOverValue(children.getRight());

    		// Zero means that they are equal
            assertEquals(momOldValue, dadNewValue); 
            assertEquals(dadOldValue, momNewValue);
    	}
    }  

	@Test
	public void testCrossover() {
		generateValidValues();
		
		GAPopulation population = new GAPopulation();
		population.addIndividual(dad);
		population.addIndividual(mom);

		operator.setPRNG(new Random());
		operator.apply(state, population, population, logger, mutateAttribute, createTree);

		assertTrue(population.getPopulationSize() == 4);
		assertTrue(population.getIndividuals().get(0) == dad);
		assertTrue(population.getIndividuals().get(1) == mom);

		final GAIndividual firstChild = population.getIndividuals().get(2);
		assertNotNull(firstChild);
		assertNotSame(firstChild, mom);
		assertNotSame(firstChild, dad);

		final GAIndividual secondChild = population.getIndividuals().get(3);
		assertNotNull(secondChild);
		assertNotSame(secondChild, mom);
		assertNotSame(secondChild, dad);
	}
	
    @Test (expected = IllegalArgumentException.class)
    public void applyingWrongMutatedField() {
    	generateValidValues();
    	mutatedField = 5;
    	
    	operator.doCrossover(mom, dad, crossoverPoint, mutatedField);
    	// It's not necessary checking the values because the operator won't be executed.	
    }
	
    @Test (expected = IllegalArgumentException.class)
    public void applyingWrongIndividualCrossover_CPisZero() {
    	generateValidValues();
    	crossoverPoint = 0;
    	
    	operator.doCrossover(mom, dad, crossoverPoint, mutatedField);
    	// It's not necessary checking the values because the operator won't be executed.	
    }

    @Test (expected = IllegalArgumentException.class)
    public void applyingWrongIndividualCrossover_CPisGreaterThanMaxOrder() {
    	generateValidValues();
    	crossoverPoint = mom.getOrder() + 1;
    	
    	operator.doCrossover(mom, dad, crossoverPoint, mutatedField);
    	// It's not necessary checking the values because the operator won't be executed.	
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void applyingWrongIndividualCrossover_CPisGreaterThanParentsMin() {
    	generateValidValues();
    	if(mom.getOrder() < dad.getOrder()) {
    		crossoverPoint = mom.getOrder() + 1;
    	} else if (dad.getOrder() < mom.getOrder()) {
    		crossoverPoint = dad.getOrder() + 1;
    	} else { 
    		crossoverPoint = mom.getOrder() + 1;
    	}
    	
    	operator.doCrossover(mom, dad, crossoverPoint, mutatedField);
    	// It's not necessary checking the values because the operator won't be executed.	
    }

	private BigInteger getXOverValue(GAIndividual i) {
		switch(mutatedField) {
		case XOVER_OPERATOR:
			return i.getThOperator(crossoverPoint);
		case XOVER_LOCATION:
			return i.getThLocation(crossoverPoint);
		default:
		case XOVER_ATTRIBUTE:
			return i.getThAttribute(crossoverPoint);
		}
	}

	private void generateValidValues() {
		final Random prng = new Random();		
		state.setAnalysisResults(analysis);
	    logger.setConsole(true);

		Collections.shuffle(Arrays.asList(a));
		Collections.shuffle(Arrays.asList(b));
		Collections.shuffle(Arrays.asList(c));
		order = prng.nextInt(a.length) + 1;

        mom = new GAIndividual(a, b, c, order);
        mom.setFitness(8);
        
		Collections.shuffle(Arrays.asList(a));
		Collections.shuffle(Arrays.asList(b));
		Collections.shuffle(Arrays.asList(c));
		order = prng.nextInt(a.length) + 1;

        dad = new GAIndividual(a, b, c, order);
        dad.setFitness(15);
        
	    crossoverPoint = 1 + prng.nextInt(Math.min(dad.getOrder(), mom.getOrder()));
	    
        // Now, the value to be modified has to be chosen randomly too. [1, 3]
	    mutatedField = XOVER_OPERATOR + prng.nextInt(XOVER_ATTRIBUTE - XOVER_OPERATOR + 1);
	}

	public boolean isMutateAttribute() {
		return mutateAttribute;
	}

	public void setMutateAttribute(boolean mutateAttribute) {
		this.mutateAttribute = mutateAttribute;
	}

}
