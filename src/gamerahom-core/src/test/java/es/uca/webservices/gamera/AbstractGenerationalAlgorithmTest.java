package es.uca.webservices.gamera;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.cli.CLIRunner;
import es.uca.webservices.gamera.conf.Configuration;
import es.uca.webservices.gamera.conf.ConfigurationLoader;
import es.uca.webservices.gamera.exec.SimulatedExecutor;
import es.uca.webservices.gamera.log.AggregateLogger;
import es.uca.webservices.gamera.record.AnalysisSimulationRecord;
import es.uca.webservices.gamera.record.SimulationRecord;

/**
 * Tests for the {@link AbstractGenerationalAlgorithm} class.
 *
 * @author Antonio García-Domínguez
 */
public class AbstractGenerationalAlgorithmTest {

	private static final BigInteger[] DEFAULT_ATTRS = new BigInteger[] {BigInteger.valueOf(5), BigInteger.valueOf(6), BigInteger.valueOf(7), BigInteger.valueOf(8)};
	private static final BigInteger[] DEFAULT_LOCS = new BigInteger[] {BigInteger.ONE, BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(4)};
	private static final String[] DEFAULT_OPERATORS = new String[] {"a", "b", "c", "d"};
	private static final int DEFAULT_TOTAL_MUTANTS = 5 * 1 + 6 * 2 + 7 * 3 + 8 * 4;
	//private static final String[] EQUIV_MUT = new String[]{"m02_1_1_a", "m02_2_1_a", "m03_4_1_a"};

	private AbstractGenerationalAlgorithm algo;
	private GAState state;
	private GALogger logger;
	
	public AbstractGenerationalAlgorithmTest(){
		System.setProperty("install.dir", new File("target/dependency").getAbsolutePath());
	}

	@Before
	public void setup() {
		algo = new GeneticAlgorithm();
		state = new GAState();
		logger = new AggregateLogger(Collections.<GALogger>emptyList());
	}

	@Test
	public void noFilteringByDefault() throws Exception {
		final GAExecutor executor = createAnalysisExecutor();
		final AnalysisResults analysis = algo.analyze(state, logger, executor);

		assertArrayEquals(DEFAULT_OPERATORS, analysis.getOperatorNames());
		assertArrayEquals(DEFAULT_LOCS, analysis.getLocationCounts());
		assertArrayEquals(DEFAULT_ATTRS, analysis.getFieldRanges());
		assertEquals(DEFAULT_TOTAL_MUTANTS, state.getTotalNumberOfMutants());
		assertSame(analysis, state.getAnalysisResults());
		assertSame(analysis, state.getHof().getAnalysis());
	}

	@Test
	public void onlyIncludeOps() throws Exception {
		final GAExecutor executor = createAnalysisExecutor();
		algo.getConfiguration().setIncludedOperators("a,c-d");
		final AnalysisResults analysis = algo.analyze(state, logger, executor);

		assertEquals(DEFAULT_TOTAL_MUTANTS - 6 * 2, state.getTotalNumberOfMutants());
		assertSame(analysis, state.getAnalysisResults());
		assertSame(analysis, state.getHof().getAnalysis());
	}

	@Test
	public void onlyExcludeOps() throws Exception {
		final GAExecutor executor = createAnalysisExecutor();
		algo.getConfiguration().setExcludedOperators("1, c");
		algo.analyze(state, logger, executor);
		assertEquals(DEFAULT_TOTAL_MUTANTS - 1 * 5 - 3 * 7, state.getTotalNumberOfMutants());
	}

	@Test
	public void bothIncludeExcludeOps() throws Exception {
		final GAExecutor executor = createAnalysisExecutor();
		algo.getConfiguration().setIncludedOperators("a - c");
		algo.getConfiguration().setExcludedOperators("b - d");
		algo.analyze(state, logger, executor);
		assertEquals(1 * 5, state.getTotalNumberOfMutants());
	}

	@Test(expected=AnalysisException.class)
	public void noRangesIncluded() throws Exception {
		final GAExecutor executor = createAnalysisExecutor();
		algo.getConfiguration().setIncludedOperators("");
		algo.analyze(state, logger, executor);
	}

	@Test(expected=AnalysisException.class)
	public void emptyRangeIncluded() throws Exception {
		final GAExecutor executor = createAnalysisExecutor();
		algo.getConfiguration().setIncludedOperators("3 - 1");
		algo.analyze(state, logger, executor);
	}

	@Test(expected=AnalysisException.class)
	public void emptyRangeExcluded() throws Exception {
		final GAExecutor executor = createAnalysisExecutor();
		algo.getConfiguration().setExcludedOperators("3 - 1");
		algo.analyze(state, logger, executor);
	}
	
	@Test
	public void loadEquivMutants() throws Exception {
			
	/*	final GAExecutor executor = createAnalysisExecutor();
		algo.getConfiguration().setTceEquivalent(true);
		algo.getConfiguration().setTcefEquiv("/home/pedro/equivalentes.txt");
		algo.prepare(state, logger, executor);
	
		List<String> equivM = state.getEquivalentMutants();
		String[] equivMArray = new String[]{"", "", ""};
		
		int cont = 0;
		for(String m: equivM){
			equivMArray[cont] = m;
			cont++;
		}
		
		assertArrayEquals(equivMArray, EQUIV_MUT);*/
	}
	
	@Test
	public void loadDupMutants() throws Exception {
			
		/*final GAExecutor executor = createAnalysisExecutor();
		algo.getConfiguration().setTceDuplicate(true);
		algo.getConfiguration().setTcefDup("/home/pedro/duplicados.txt");
		algo.prepare(state, logger, executor);
	
		//DUP_MUT > {{"m02_1_1_a", "m03_4_1_a", "m04_1_1_a"},{"m02_2_1_a", "m04_2_1_a"}};
	
		List<List<String>> dupM = state.getDuplicateMutants();
		
		String[] dupMArray = new String[]{"", "", ""};
		int cont = 0;
		for(String m: dupM.get(0)){
			dupMArray[cont] = m;
			cont++;
		}
		
		String[] DUP_MUT = new String[]{"m02_1_1_a", "m03_4_1_a", "m04_1_1_a"};
		assertArrayEquals(dupMArray, DUP_MUT);
		
		dupMArray = new String[]{"", ""};
		cont = 0;
		for(String m: dupM.get(1)){
			dupMArray[cont] = m;
			cont++;
		}
		
		DUP_MUT = new String[]{"m02_2_1_a", "m04_2_1_a"};
		assertArrayEquals(dupMArray, DUP_MUT);*/
	}

    @Test
    @Category(IntegrationTest.class)
    public void runTwiceProducesSameResults() throws Exception {
		final File yaml = new File(getResource("all-ok.yaml"));
        final Configuration config = new ConfigurationLoader().parse(new FileInputStream(yaml));
        final AbstractGenerationalAlgorithm generator = new GeneticAlgorithm();
		generator.setConfiguration(config);

        config.setSeed("internationalist");
		final GAHof hof1 = generator.run().getHof();
		
		config.setSeed("internationalist");
		final GAHof hof2 = generator.run().getHof();

		assertEquals(hof1, hof2);
    }

	private String getResource(final String fileBasename) {
		return CLIRunner.class.getResource("/" + fileBasename).getPath();
	}

	private GAExecutor createAnalysisExecutor() {
		final SimulationRecord record = new SimulationRecord();
		record.setAnalysisRecord(new AnalysisSimulationRecord(
			new AnalysisResults(DEFAULT_OPERATORS, DEFAULT_LOCS, DEFAULT_ATTRS),
			BigInteger.ZERO));

		final GAExecutor executor = new SimulatedExecutor(record);
		return executor;
	}
	
}
