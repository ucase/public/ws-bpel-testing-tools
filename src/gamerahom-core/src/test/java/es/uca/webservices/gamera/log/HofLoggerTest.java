package es.uca.webservices.gamera.log;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Tests for the {@link HofLogger} class.
 *
 * @author Emma Blanco-Muñoz
 * @version 1.0
 */
public class HofLoggerTest {

	private static final String OUTPUT_TXT = "output_HofLoggerTest.txt";

	@Test
	public void FileIsGenerated() {
		HofLogger logger = new HofLogger();
        logger.setConsole(false);
        logger.setFile(OUTPUT_TXT);
        
        GAState state = new GAState();
        prepareState(state);
        logger.finished(state);
        assertTrue(new File(OUTPUT_TXT).exists() == true);
	}

	private void prepareState(GAState state) {
		AnalysisResults analysis = new AnalysisResults(
			new String[] {"OP1", "OP2"},
			new BigInteger[] { BigInteger.ZERO, BigInteger.ZERO },
			new BigInteger[] { BigInteger.ZERO, BigInteger.ZERO });
        
        GAHof hof = new GAHof(analysis);
        
        Map<GAIndividual, ComparisonResults> generatedIndividuals = 
        	new HashMap<GAIndividual, ComparisonResults>();
        
        BigInteger[] values = {BigInteger.ONE, BigInteger.ONE.add(BigInteger.ONE), BigInteger.ONE, 
        		BigInteger.ONE, BigInteger.ONE, BigInteger.ONE};
		GAIndividual i = new GAIndividual(values, values, values, 3, true);
		
        ComparisonResult[] cr = {ComparisonResult.DIFFERENT_OUTPUT,
        		ComparisonResult.SAME_OUTPUT, 
        		ComparisonResult.SAME_OUTPUT};
        ComparisonResults compResults = new ComparisonResults(i, cr);
        generatedIndividuals.put(i, compResults);
        
		hof.clear();
		hof.putAll(generatedIndividuals);
        state.setHof(hof);
        state.setAnalysisResults(analysis);
	}
	
	@Test
    public void onlyConsoleIsValid() throws InvalidConfigurationException {
        new HofLogger().validate();
    }

    @Test
    public void onlyFileIsValid() throws InvalidConfigurationException {
        HofLogger logger = new HofLogger();
        logger.setConsole(false);
        logger.setFile(OUTPUT_TXT);
        logger.validate();
    }

    @Test(expected=InvalidConfigurationException.class)
    public void noConsoleAndNoFileIsInvalid() throws InvalidConfigurationException {
        HofLogger logger = new HofLogger();
        logger.setConsole(false);
        logger.validate();
    }
    
    @Test(expected=InvalidConfigurationException.class)
    public void ConsoleAndFileIsInvalid() throws InvalidConfigurationException {
        HofLogger logger = new HofLogger();
        logger.setFile(OUTPUT_TXT);
        logger.validate();
    }
    
}
