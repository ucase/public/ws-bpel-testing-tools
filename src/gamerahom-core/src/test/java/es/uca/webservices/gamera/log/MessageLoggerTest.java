package es.uca.webservices.gamera.log;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Tests for the {@link MessageLogger} class.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public class MessageLoggerTest {

	private static final String OUTPUT_TXT = "output_MessageLoggerTest.txt";

	@Test
    public void onlyConsoleIsValid() throws InvalidConfigurationException {
        new MessageLogger().validate();
    }

    @Test
    public void showByConsole() throws InvalidConfigurationException {
        MessageLogger logger = new MessageLogger();
        logger.validate();
        logger.startedAnalysis(new GAState());
    }
    
    @Test
    public void onlyFileIsValid() throws InvalidConfigurationException {
        MessageLogger logger = new MessageLogger();
        logger.setConsole(false);
        logger.setFile(OUTPUT_TXT);
        logger.validate();
    }

    @Test(expected=InvalidConfigurationException.class)
    public void noConsoleAndNoFileIsInvalid() throws InvalidConfigurationException {
        MessageLogger logger = new MessageLogger();
        logger.setConsole(false);
        logger.validate();
    }
    
    @Test(expected=InvalidConfigurationException.class)
    public void ConsoleAndFileIsInvalid() throws InvalidConfigurationException {
        MessageLogger logger = new MessageLogger();
        logger.setFile(OUTPUT_TXT);
        logger.validate();
    }
    
    @Test
    public void FileIsGenerated() throws InvalidConfigurationException, InterruptedException{
        MessageLogger logger = new MessageLogger();
        logger.setConsole(false);
        logger.setFile(OUTPUT_TXT);
        
        final AnalysisResults analysis = new AnalysisResults();
        GAState state = new GAState();
        state.setCurrentGeneration(1);
        state.setTotalNumberOfMutants(10);
        state.setAnalysisResults(analysis);
		state.setHof(new GAHof(analysis));

        final GAPopulation population = new GAPopulation();

        logger.started(state);
        logger.startedAnalysis(state);
        logger.finishedAnalysis(state);
        logger.newGeneration(state, population);
		logger.startedComparison(state, population);
        logger.finishedComparison(state, population);
        logger.finishedEvaluation(state, population);
        logger.startedEvaluation(state, population);
        logger.finished(state);
        
        // Sleep for a second to check the timestamp
        Thread.sleep(1000);
        logger.finishedAnalysis(new GAState());
        assertTrue(new File(OUTPUT_TXT).exists() == true);
    }
}
