package es.uca.webservices.gamera.term;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.math.BigInteger;

import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.record.SimulationRecord;

/**
 * Tests for the {@link PercentStrongMutantsCondition} class.
 *
 * @author Antonio García-Domínguez
 */
public class PercentStrongMutantsConditionTest {

	/**
	 * Strong mutants in the simulation record created by
	 * {@link #createConditionWith3StrongMutants()}.
	 */
	private static final GAIndividual[] STRONG_MUTANTS = {
		new GAIndividual(2, 1, 1),
		new GAIndividual(2, 2, 1),
		new GAIndividual(3, 1, 1)
	};

	@Test(expected=InvalidConfigurationException.class)
	public void needsToReceiveSimRecord() throws InvalidConfigurationException {
		new PercentStrongMutantsCondition().validate();
	}

	@Test
	public void withSimRecordIsOK() throws Exception {
		createFOMCondition().validate();
	}

	@Test(expected=InvalidConfigurationException.class)
	public void negativePercentIsNotValid() throws Exception {
		final PercentStrongMutantsCondition c = createFOMCondition();
		c.setPercent(-1);
		c.validate();
	}

	@Test(expected=InvalidConfigurationException.class)
	public void greaterThan1PercentIsNotValid() throws Exception {
		final PercentStrongMutantsCondition c = createFOMCondition();
		c.setPercent(2);
		c.validate();
	}

	@Test
	public void evaluate_100target_0strong() {
		testCondition(1d, 0.00d);
	}

	@Test
	public void evaluate_100target_35strong() {
		testCondition(1d, 0.35d);
	}

	@Test
	public void evaluate_100target_70strong() {
		testCondition(1d, 0.70d);
	}

	@Test
	public void evaluate_100target_100strong() {
		testCondition(1d, 1d);
	}

	@Test
	public void evaluate_70target_0strong() {
		testCondition(0.7d, 0.00d);
	}

	@Test
	public void evaluate_70target_35strong() {
		testCondition(0.7d, 0.35d);
	}

	@Test
	public void evaluate_70target_70strong() {
		testCondition(0.7d, 0.70d);
	}

	@Test
	public void evaluate_70target_100strong() {
		testCondition(0.7d, 1d);
	}

	@Test
	public void evaluate_35target_0strong() {
		testCondition(0.35d, 0.00d);
	}

	@Test
	public void evaluate_35target_35strong() {
		testCondition(0.35d, 0.35d);
	}

	@Test
	public void evaluate_35target_70strong() {
		testCondition(0.35d, 0.70d);
	}

	@Test
	public void evaluate_35target_100strong() {
		testCondition(0.35d, 1d);
	}

	private void testCondition(final double targetPercent, final double strongPercent) {
		final PercentStrongMutantsCondition c = createConditionWith3StrongMutants();
		final GAState state = createEmptyStateFor3StrongMutants();
		c.setPercent(targetPercent);
		for (int i = 0; i < (int)(STRONG_MUTANTS.length * strongPercent); ++i) {
			state.getHof().put(STRONG_MUTANTS[i], null);
		}
		final boolean expected = strongPercent >= targetPercent;
		assertEquals(
			"With " + (strongPercent * 100) + " strong mutants generated and target percent " + (targetPercent * 100) + ", it should be " + expected,
			expected, c.evaluate(state));
	}

	private GAState createEmptyStateFor3StrongMutants() {
		final GAState state = new GAState();
		state.setHof(new GAHof(new AnalysisResults(
			new String[] { "a", "b", "c" },
			new BigInteger[] { BigInteger.ONE, BigInteger.valueOf(2), BigInteger.ONE},
			new BigInteger[] { BigInteger.valueOf(3), BigInteger.ONE, BigInteger.ONE}
		)));
		return state;
	}

	private PercentStrongMutantsCondition createConditionWith3StrongMutants() {
		final SimulationRecord r = new SimulationRecord();
		final BigInteger DEFAULT_TIME = BigInteger.valueOf(100);
		r.addComparisonRecord(new ComparisonResults(1, 1, 1, 0, 1, 0, 0), DEFAULT_TIME);
		r.addComparisonRecord(new ComparisonResults(1, 1, 2, 0, 1, 1, 0), DEFAULT_TIME);
		r.addComparisonRecord(new ComparisonResults(1, 1, 3, 2, 2, 2, 2), DEFAULT_TIME);
		r.addComparisonRecord(new ComparisonResults(2, 1, 1, 1, 0, 0, 0), DEFAULT_TIME);
		r.addComparisonRecord(new ComparisonResults(2, 2, 1, 0, 0, 0, 1), DEFAULT_TIME);
		r.addComparisonRecord(new ComparisonResults(3, 1, 1, 0, 0, 0, 0), DEFAULT_TIME);

		final PercentStrongMutantsCondition c = new PercentStrongMutantsCondition();
		c.setSimulationRecord(r);
		return c;
	}

	private PercentStrongMutantsCondition createFOMCondition() throws Exception {
		final PercentStrongMutantsCondition c = new PercentStrongMutantsCondition();
		c.setFile(new File("src/test/resources/loanRPC.simrecord.order1.yaml"));
		return c;
	}
}
