package es.uca.webservices.gamera.genetic;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.log.AncestryLogger;

/**
 * Tests for the {@link IndividualMutationOperator} class.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.1
 */
public class IndividualMutationOperatorTest {   
    
    private static final int MAX_OPERATOR = 5;
	private static final int MAX_LOCATION = 9;
	private static final int MAX_ATTRIBUTE = 9;
	
	private static final int MUT_OPERATOR = 1;
	private static final int MUT_LOCATION = MUT_OPERATOR + 1;
	private static final int MUT_ATTRIBUTE = MUT_LOCATION + 1;

    private static final BigInteger[] OPERATOR_FIELDS = {
    	BigInteger.valueOf(1),BigInteger.valueOf(2),
        BigInteger.valueOf(3), BigInteger.valueOf(4),
        BigInteger.valueOf(5)
    };
    private static final BigInteger[] LOCATION_FIELDS = {
    	BigInteger.valueOf(6),BigInteger.valueOf(7),
        BigInteger.valueOf(8), BigInteger.valueOf(9),
        BigInteger.valueOf(1)
    };
    private static final BigInteger[] ATTRIBUTE_FIELDS = {
    	BigInteger.valueOf(9),BigInteger.valueOf(8),
        BigInteger.valueOf(7), BigInteger.valueOf(6),
        BigInteger.valueOf(5)
    };

    private AncestryLogger logger = new AncestryLogger();   
    private GAState state = new GAState();
    
    private IndividualMutationOperator operator = new IndividualMutationOperator();
	private AnalysisResults analysis = new AnalysisResults(
		new String[] { "a", "b", "c", "d", "e" },
		new BigInteger[] { BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION)},
		new BigInteger[] { BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE)}
	);
    private GAPopulation population;

    private int order, mutant, mutatedField;
    
    private boolean mutateAttribute = true;
    private boolean createTree = false;
    
    private GAIndividual ind;
    private Random rnd;

    @Test
    public void applyingValidIndividualMutationWithModuloArithmetic() throws InvalidConfigurationException {
		initialize();
		operator.setClip(false);
		operator.setUseOperatorSteps(false);
    	testApplyingValidIndividualMutation();
    }

    @Test
    public void applyingValidIndividualMutationWithModuloArithmeticAndOperatorSteps() throws InvalidConfigurationException {
		initialize();
		operator.setClip(false);
		operator.setUseOperatorSteps(true);
    	testApplyingValidIndividualMutation();
    }

    @Test
    public void applyingValidIndividualMutationWithClipping() throws InvalidConfigurationException {
		initialize();
		operator.setClip(true);
		operator.setUseOperatorSteps(false);
    	testApplyingValidIndividualMutation();
    }

    @Test
    public void applyingValidIndividualMutationWithClippingAndOperatorSteps() throws InvalidConfigurationException {
		initialize();
		operator.setClip(true);
		operator.setUseOperatorSteps(true);
    	testApplyingValidIndividualMutation();
    }

    @Test
    public void applyingValidIndividualMutationWithRandomRangePercent() throws InvalidConfigurationException {
		initialize();
		operator.setRandomRange(null);
		operator.setRandomRangePercent(1d);
		operator.setScaleFactor(0.1);
    	testApplyingValidIndividualMutation();
    }

	private void testApplyingValidIndividualMutation()	throws InvalidConfigurationException {
		operator.validate();

		int countDifferent = 0;
    	for(int i = 0; i < 1000; i++) {
    		generateValidValues();
			final GAIndividual newInd = operator.doMutation(analysis, ind, mutant, mutatedField);

			if (!ind.equals(newInd)) {
				++countDifferent;
			}
			for (int iOrder = 1; iOrder <= newInd.getOrder(); ++iOrder) {
				assertTrue(newInd.getThOperator(iOrder).compareTo(BigInteger.ONE) >= 0);
				assertTrue(newInd.getThLocation(iOrder).compareTo(BigInteger.ONE) >= 0);
				assertTrue(newInd.getThAttribute(iOrder).compareTo(BigInteger.ONE) >= 0);
				assertTrue(newInd.getThOperator(iOrder).longValue() <= MAX_OPERATOR);
				assertTrue(newInd.getThLocation(iOrder).longValue() <= MAX_LOCATION);
				assertTrue(newInd.getThAttribute(iOrder).longValue() <= MAX_ATTRIBUTE);
			}
    	}
    	assertTrue(countDifferent > 780);
	}

	@Test
	public void testMutation() {
		initialize();
		generateValidValues();
		operator.apply(state, population, population,logger, mutateAttribute, createTree);
		
		assertTrue(population.getPopulationSize() == 2);
		final List<GAIndividual> inds = population.getIndividuals();
		assertNotNull(inds.get(0));
		assertNotNull(inds.get(1));
		assertNotSame(inds.get(0), inds.get(1));
	}
	
    @Test (expected = IllegalArgumentException.class)
    public void applyingWrongIndividualMutation_mutantIsZero() throws InvalidConfigurationException {
    	initialize();
    	
    	generateValidValues();
		operator.validate();
    	mutant = 0;			
		operator.doMutation(analysis, ind, mutant, mutatedField);
    }

    @Test (expected = IllegalArgumentException.class)
    public void applyingWrongIndividualMutation_mutantIsGreaterThanMax() throws InvalidConfigurationException {
    	initialize();
    	
    	generateValidValues();
		operator.validate();
    	mutant = ind.getMaxOrder() + 1;			
		operator.doMutation(analysis, ind, mutant, mutatedField);
    }

    @Test (expected = IllegalArgumentException.class)
    public void applyingWrongIndividualMutation_IncorrectMutatedField() throws InvalidConfigurationException {
    	initialize();
    	
    	generateValidValues();
		operator.validate();
    	mutatedField = MUT_ATTRIBUTE + 1;	
		operator.doMutation(analysis, ind, mutant, mutatedField);
    }

    @Test (expected = InvalidConfigurationException.class)
    public void applyingWrongRandomRangeInOrderMutationOperator() throws InvalidConfigurationException {
    	operator.setRandomRange(-1);
    	operator.setScaleFactor(0.5);
    	operator.validate();
    }
    
    @Test (expected = InvalidConfigurationException.class)
    public void applyingWrongScaleFactorInOrderMutationOperator() throws InvalidConfigurationException {
    	operator.setRandomRange(1);
    	operator.setScaleFactor(2d);
    	operator.validate();
    }

	private void generateValidValues() {
		state.setAnalysisResults(analysis);
	    logger.setConsole(true);

		Collections.shuffle(Arrays.asList(OPERATOR_FIELDS), rnd);
		Collections.shuffle(Arrays.asList(LOCATION_FIELDS), rnd);
		Collections.shuffle(Arrays.asList(ATTRIBUTE_FIELDS), rnd);
		order = rnd.nextInt(OPERATOR_FIELDS.length) + 1;

		population = new GAPopulation();
        ind = new GAIndividual(OPERATOR_FIELDS, LOCATION_FIELDS, ATTRIBUTE_FIELDS, order);
        ind.setFitness(3);
        population.addIndividual(ind);
        
        // To calculate a random number from M to N, we use this rule:
        //    Math.random() * (N - M) + M)
	    mutant = rnd.nextInt(order - MUT_OPERATOR + 1) + MUT_OPERATOR;
         // Now, the value to be modified has to be chosen randomly too. [1, 3]
		mutatedField = rnd.nextInt(MUT_ATTRIBUTE - MUT_OPERATOR + 1) + MUT_OPERATOR;

	}

	private void initialize() {
		rnd = new Random(0);
		operator.setPRNG(rnd);
		operator.setRandomRange(100);
		operator.setScaleFactor(0.5);
	}

}
