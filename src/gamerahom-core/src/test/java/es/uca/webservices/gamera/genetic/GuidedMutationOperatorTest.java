package es.uca.webservices.gamera.genetic;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class GuidedMutationOperatorTest {

	private GuidedBitMutator bc;
	private Random mockRandom;

	@Before
	public void setUp() {
		bc = new GuidedBitMutator();
		mockRandom = mock(Random.class);
	}

	@Test
	public void bcOnlyOne() {
		bc.update(1);
		assertEquals(1, bc.getProbability(0), 1e-4);
	}

	@Test
	public void bcOneTwo() {
		bc.update(1, 2);
		assertEquals(0.5, bc.getProbability(0), 1e-4);
		assertEquals(0.5, bc.getProbability(1), 1e-4);
	}

	@Test
	public void bcOneThree() {
		bc.update(1, 3);
		assertEquals(1, bc.getProbability(0), 1e-4);
		assertEquals(0.5, bc.getProbability(1), 1e-4);
	}

	@Test
	public void bcLearningFactor() {
		bc.setLearningFactor(0.6);
		bc.update(1, 3);
		bc.update(2, 4);

		// 0.4 * 0 + 0.6 * 0.5 = 0.3
		assertEquals(0.3, bc.getProbability(2), 1e-4);

		// 0.4 * 0.5 + 0.6 * 0.5 = 0.5
		assertEquals(0.5, bc.getProbability(1), 1e-4);

		// 0.4 * 1 + 0.6 * 0 = 0.4 
		assertEquals(0.4, bc.getProbability(0), 1e-4);
	}

	@Test
	public void bcEmpty() {
		assertEquals(0, bc.getProbability(0), 1e-4);
	}

	@Test
	public void mutateNoSampling() {
		bc.setSamplingRate(0);
		bc.update(1, 2);
		when(mockRandom.nextDouble())
			.thenReturn(1.0)
			.thenReturn(1.0)
			.thenReturn(1.0);
		
		assertEquals(5, bc.mutate(5, mockRandom));
	}

	@Test
	public void mutateSamplingFirst() {
		bc.setSamplingRate(0.4);
		// probabilities: 1 - 0.33, 2 - 0.33, 4 - 0.66
		bc.update(2, 4, 5);
		when(mockRandom.nextDouble())
			.thenReturn(0.6) // keep 1 bit
			.thenReturn(0.3) // change 2 bit...
			.thenReturn(0.4) // ... to a 0
			.thenReturn(0.5) // keep 4 bit
			.thenReturn(0.3) // change 8 bit...
			.thenReturn(0.01) // ... to a 0 (probability seen so far is 0)
			;

		assertEquals(5, bc.mutate(15, mockRandom));
	}
}
