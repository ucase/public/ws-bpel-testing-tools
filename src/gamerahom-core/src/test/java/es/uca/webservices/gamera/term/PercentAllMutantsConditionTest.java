package es.uca.webservices.gamera.term;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.GAHof;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Tests for the {@link PercentAllMutantsCondition} class.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.1
 */
public class PercentAllMutantsConditionTest {

    private PercentAllMutantsCondition condition;
    private GAState                    state;

    BigInteger[] a = {BigInteger.valueOf(1),BigInteger.valueOf(2),
        BigInteger.valueOf(3), BigInteger.valueOf(4),BigInteger.valueOf(5)};
    BigInteger[] b = {BigInteger.valueOf(6),BigInteger.valueOf(7),
        BigInteger.valueOf(8), BigInteger.valueOf(9),BigInteger.valueOf(0)};
    BigInteger[] c = {BigInteger.valueOf(9),BigInteger.valueOf(8),
        BigInteger.valueOf(7), BigInteger.valueOf(6),BigInteger.valueOf(5)};

    @Before
    public void setUp() throws InvalidConfigurationException {
        condition = new PercentAllMutantsCondition();
        condition.setPercent(0.5);
        condition.validate();
        state = new GAState();
        state.setTotalNumberOfMutants(100);
        // Initializes the GAHof which contains the generated individuals
        state.setHof(new GAHof(null));
    }

    @Test
    public void falseWhenEmpty() {
        assertFalse(
                "Condition should be false when no mutants have been generated",
                condition.evaluate(state));
    }
   
    @Test
    public void falseWhenLess() {
        condition.setPercent(0.1);
    	// 1% of 100 mutants = 10 mutants
    	// Only there will be five mutants
        addPercentIndividuals(condition.getPercent());
        assertFalse("Condition should be false when less than the required "
                + "percent have been generated", condition.evaluate(state));
    }
 
    @Test
    public void trueWhenEqual() {
        condition.setPercent(0.05);
        // 5% of 100 mutants = 5 individuals
    	// Only there will be five mutants
        addPercentIndividuals(condition.getPercent());
        assertTrue("Condition should be true when equal to the required percent",
                condition.evaluate(state));
    }

    @Test
    public void trueWhenGreater() {
        condition.setPercent(0.04);
        // 4% of 100 mutants = 4 individuals
        addPercentIndividuals(condition.getPercent());        
        // Adds one more, different than the others 
        GAIndividual fakeInd = new GAIndividual(b, b, b, 2, true);
        state.getHof().put(fakeInd, null);
        assertTrue(
                "Condition should be true when greater than the required percent",
                condition.evaluate(state));
    }

    @Test(expected=InvalidConfigurationException.class)
    public void zeroPercentIsInvalid() throws InvalidConfigurationException {
        PercentAllMutantsCondition cond = new PercentAllMutantsCondition();
        cond.validate();
    }

    @Test(expected=InvalidConfigurationException.class)
    public void negativePercentIsInvalid() throws InvalidConfigurationException {
        PercentAllMutantsCondition cond = new PercentAllMutantsCondition();
        cond.setPercent(-0.01);
        cond.validate();
    }

    @Test
    public void hundredPercentIsValid() throws InvalidConfigurationException {
        PercentAllMutantsCondition cond = new PercentAllMutantsCondition();
        cond.setPercent(1.00);
        cond.validate();
    }

    @Test(expected=InvalidConfigurationException.class)
    public void moreThanHundredPercentIsInvalid() throws InvalidConfigurationException {
        PercentAllMutantsCondition cond = new PercentAllMutantsCondition();
        cond.setPercent(1.01);
        cond.validate();
    }

    private void addPercentIndividuals(final double percent) {
        final int icount = (int) Math.floor(percent * state.getTotalNumberOfMutants());
        int o = 1; // Changeable parameter for the order
        
        //With this combination only five individuals different will be generated
        for (int i = 0; i < icount; i++) {
        	if(o > a.length) {
        		o = 1;
        	}
            GAIndividual fakeInd = new GAIndividual(a, b, c, o, true);
            o++;
            // Forces the individual insertion without normalize it 
            state.getHof().put(fakeInd, null);
        }
    }
}
