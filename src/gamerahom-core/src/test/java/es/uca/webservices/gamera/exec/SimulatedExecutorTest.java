package es.uca.webservices.gamera.exec;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.record.AnalysisSimulationRecord;
import es.uca.webservices.gamera.record.SimulationRecord;

public class SimulatedExecutorTest {

	private static final BigInteger[] DEFAULT_TEST_NANOS = new BigInteger[] {
			BigInteger.valueOf(20),
			BigInteger.valueOf(30),
			BigInteger.valueOf(50)
	};
	private static final BigInteger DEFAULT_NANOS = BigInteger.valueOf(100);
	private static final Integer[] RESULTS_M1_1_1 = new Integer[]{1, 1, 0};
	private static final Integer[] RESULTS_M1_1_1_STOP_FIRST_DIFF = new Integer[]{1, 0, 0};
	private SimulationRecord simRecord;
	private SimulatedExecutor simExecutor;

	@Before
	public void setUp() throws InvalidConfigurationException {
		simRecord = new SimulationRecord();
		simRecord.setAnalysisRecord(new AnalysisSimulationRecord(
			new AnalysisResults(
				new String[] {"a", "b"},
				new BigInteger[] { BigInteger.valueOf(2), BigInteger.valueOf(2) },
				new BigInteger[] { BigInteger.ONE, BigInteger.ONE }
			), DEFAULT_NANOS));

		final ComparisonResults firstComparisonResults = new ComparisonResults(1, 1, 1, true, RESULTS_M1_1_1);
		firstComparisonResults.setTestWallNanos(DEFAULT_TEST_NANOS);
		simRecord.addComparisonRecord(firstComparisonResults, DEFAULT_NANOS);
		simRecord.addComparisonRecord(new ComparisonResults(1, 2, 1, true, 0, 1, 0), DEFAULT_NANOS);
		simRecord.addComparisonRecord(new ComparisonResults(2, 1, 1, true, 0, 0, 1), DEFAULT_NANOS);
		// Individual (2, 1, 2) is missing

		simExecutor = new SimulatedExecutor();
		simExecutor.setRecord(simRecord);
		simExecutor.validate();
	}

	@Test
	public void existingIndividualIsOK() throws ComparisonException {
		final GAIndividual ind = new GAIndividual(1, 1, 1, true);

		final ComparisonResults results = simExecutor.compare(null, ind)[0];
		assertEqualRows(results.getRow(), RESULTS_M1_1_1);
		assertTrue(results.getTotalNanos().compareTo(BigInteger.ZERO) > 0);
		for (BigInteger nano : results.getTestWallNanos()) {
			assertTrue(nano.compareTo(BigInteger.ZERO) >= 0);
		}
	}

	@Test
	public void stopAtFirstDiffIsHonored() throws ComparisonException {
		simExecutor.setStopAtFirstDiff(true);
		final GAIndividual ind = new GAIndividual(1, 1, 1, true);

		final ComparisonResults results = simExecutor.compare(null, ind)[0];
		assertEqualRows(results.getRow(), RESULTS_M1_1_1_STOP_FIRST_DIFF);
		assertEquals(DEFAULT_TEST_NANOS[0], results.getTotalNanos());
		assertEquals(DEFAULT_TEST_NANOS[0], results.getTestWallNanos()[0]);

		assertTrue(results.getTestWallNanos()[0].compareTo(BigInteger.ZERO) > 0);
		assertEquals(BigInteger.ZERO, results.getTestWallNanos()[1]);
		assertEquals(BigInteger.ZERO, results.getTestWallNanos()[2]);
	}

	@Test(expected=ComparisonException.class)
	public void missingIndividual_shouldThrowExceptionByDefault() throws ComparisonException {
		final GAIndividual ind = new GAIndividual(2, 1, 2, true);
		simExecutor.compare(null, ind);
	}

	@Test(expected=ComparisonException.class)
	public void missingIndividual_shouldThrowExceptionWhenInstructed() throws ComparisonException {
		simRecord.setMissingIndividualsAreInvalid(false);
		missingIndividual_shouldThrowExceptionByDefault();
	}

	@Test
	public void missingIndividual_shouldBeInvalidWhenInstructed() throws ComparisonException {
		simRecord.setMissingIndividualsAreInvalid(true);

		final GAIndividual ind = new GAIndividual(2, 1, 2, true);
		final ComparisonResults results = simExecutor.compare(null, ind)[0];
		assertEqualRows(results.getRow(), 2, 2, 2);
		assertEquals(BigInteger.ZERO, results.getTotalNanos());
		assertNull(results.getTestWallNanos());
	}

	@Test
	public void testTimesAreReported() throws ComparisonException {
		final GAIndividual ind = new GAIndividual(1, 1, 1, true);
		final ComparisonResults r = simExecutor.compare(null, ind)[0];
		assertArrayEquals(DEFAULT_TEST_NANOS, r.getTestWallNanos());
	}

	private void assertEqualRows(final ComparisonResult[] row, final Integer... rawRow) {
		for (int i = 0; i < row.length; ++i) {
			assertEquals(rawRow[i].intValue(), row[i].getValue());
		}
	}

}
