package es.uca.webservices.gamera.term;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Tests for the {@link GenerationCountCondition} class.
 *
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class GenerationCountConditionTest {

    private GenerationCountCondition condition;

    @Before
    public void setUp() throws InvalidConfigurationException {
        condition = new GenerationCountCondition();
        condition.setCount(100);
        condition.validate();
    }

    @Test
    public void falseWhenLess() {
        GAState state = new GAState();
        state.setCurrentGeneration(10);
        assertFalse("Condition should be false when count < threshold",
                condition.evaluate(state));
    }

    @Test
    public void falseWhenLessByOne() {
        GAState state = new GAState();
        state.setCurrentGeneration(condition.getCount() - 1);
        assertFalse("Condition should be false when count = threshold - 1",
                condition.evaluate(state));
    }

    @Test
    public void trueWhenEqual() {
        GAState state = new GAState();
        state.setCurrentGeneration(condition.getCount());
        assertTrue("Condition should be true when count = threshold", condition
                .evaluate(state));
    }

    @Test
    public void trueWhenGreater() {
        GAState state = new GAState();
        state.setCurrentGeneration(condition.getCount() + 1);
        assertTrue("Condition should be true when count > threshold", condition
                .evaluate(state));
    }

    @Test(expected=InvalidConfigurationException.class)
    public void zeroCountIsInvalid() throws InvalidConfigurationException {
        GenerationCountCondition cond = new GenerationCountCondition();
        cond.validate();
    }

    @Test(expected=InvalidConfigurationException.class)
    public void negativeCountIsInvalid() throws InvalidConfigurationException {
        GenerationCountCondition cond = new GenerationCountCondition();
        cond.setCount(-1);
        cond.validate();
    }
}
