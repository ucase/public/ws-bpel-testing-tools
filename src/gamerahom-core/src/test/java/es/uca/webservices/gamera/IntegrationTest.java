package es.uca.webservices.gamera;

/**
 * JUnit marker interface for the category of integration tests which should
 * only be run in nightly jobs.
 * 
 * @author Antonio García-Domínguez
 */
public interface IntegrationTest {}
