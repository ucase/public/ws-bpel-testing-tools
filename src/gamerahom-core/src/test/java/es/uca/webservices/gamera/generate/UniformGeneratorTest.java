package es.uca.webservices.gamera.generate;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;

/**
 * Tests for the {@link UniformGenerator} class.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public class UniformGeneratorTest {

    private UniformGenerator gen;
    private AnalysisResults analysis;
	private GAState state;
    private static final int MAX_ORDER = 5;

    @Before
    public void setUp() {
    	gen = new UniformGenerator();
    	gen.setPRNG(new Random(0));

    	analysis = new AnalysisResults(
       			new String[] {"a", "b", "c", "d", "e", "f", "g", "h"},
       			new BigInteger[] {BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.valueOf(7), BigInteger.ONE, BigInteger.ONE, BigInteger.ONE},
       			new BigInteger[] {BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.valueOf(8), BigInteger.ONE, BigInteger.ONE, BigInteger.ONE});

    	state = new GAState();
    	state.setAnalysisResults(analysis);
    }

    @Test (expected = IllegalArgumentException.class)
    public void wrongGeneration_OrderLessThanOne() throws InvalidConfigurationException{
        testProbability(0.5);
        gen.generate(null, 0, null, null);
    }
    
    @Test
    public void validIndividualCreationOneByOne() throws InvalidConfigurationException{
        testProbability(0.5);
        
        for(int i = 0; i < 1000; i ++) {
        	GAIndividual ind = gen.createIndividual(state, MAX_ORDER);

        	// The order is in the valid range
        	assertTrue(ind.getOrder() >= 1 && ind.getOrder() <= MAX_ORDER);

	        for(BigInteger op : ind.getOperator()) {
	        	// The values start with one
	         	 assertEquals(1, op.compareTo(BigInteger.ZERO));
	         	 // The values are in the valid range
	          	 assertTrue(op.compareTo(analysis.getMaxOp()) <= 0);
	        }
	        for(BigInteger loc : ind.getLocation()) {
	        	 assertEquals(1, loc.compareTo(BigInteger.ZERO));
	         	 assertTrue(loc.compareTo(analysis.getMaxLoc()) <= 0);
	        }
	        for(BigInteger at : ind.getAttribute()) {
	        	 assertEquals(1, at.compareTo(BigInteger.ZERO));
	          	 assertTrue(at.compareTo(analysis.getMaxAtt()) <= 0);
	        }
	   }
    }

    @Test
    public void validIndividualGeneration() throws InvalidConfigurationException{
        GAPopulation destination = new GAPopulation();
        testProbability(0.5);
        int prevPopSize = destination.getPopulationSize();
        assertThat(prevPopSize, is(0));
        for(int i = 0; i < 1000; i++) {
        	gen.generate(state, MAX_ORDER, null, destination);
	        assertThat(destination.getPopulationSize(), is(prevPopSize+1));  
	        
	        prevPopSize = destination.getPopulationSize();
        }
    }
    
    @Test
    public void zeroProbabilityIsValid() throws InvalidConfigurationException {
        testProbability(0.0);
    }

    @Test
    public void oneProbabilityIsValid() throws InvalidConfigurationException {
        testProbability(1.0);
    }

    @Test
    public void halfProbabilityIsValid() throws InvalidConfigurationException {
        testProbability(0.5);
    }

    @Test(expected = InvalidConfigurationException.class)
    public void negativeProbabilityIsInvalid()
            throws InvalidConfigurationException {
        testProbability(-0.01);
    }

    @Test(expected = InvalidConfigurationException.class)
    public void largerThanOneProbabilityIsInvalid()
            throws InvalidConfigurationException {
        testProbability(1.01);
    }

    private void testProbability(final double percent)
            throws InvalidConfigurationException {
        gen.setPercent(percent);
        gen.validate();
    }
}
