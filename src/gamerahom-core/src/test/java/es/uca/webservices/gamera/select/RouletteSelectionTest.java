package es.uca.webservices.gamera.select;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.generate.UniformGenerator;

/**
 * Tests for the {@link RouletteSelection} class.
 * 
 * @author Emma Blanco-Muñoz
 * @version 1.0
 */
public class RouletteSelectionTest {

    private final static int MAX_ORDER = 5;
    private final static int N = 10;

	private RouletteSelection roulette;
	private UniformGenerator gen;
	private AnalysisResults analysis;
	private GAPopulation population;
	private Random r;
	private GAState state;
    
    @Before
    public void setUp() {
    	roulette = new RouletteSelection();
    	gen = new UniformGenerator();

    	analysis = new AnalysisResults(
   			new String[] {"a", "b", "c", "d", "e", "f", "g", "h"},
   			new BigInteger[] {BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.valueOf(7), BigInteger.ONE, BigInteger.ONE, BigInteger.ONE},
   			new BigInteger[] {BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.ONE, BigInteger.valueOf(8), BigInteger.ONE, BigInteger.ONE, BigInteger.ONE});

    	state = new GAState();
    	state.setAnalysisResults(analysis);
    	
    	population = new GAPopulation();
    	r = new Random(0);
    	roulette.setPRNG(r);
    	gen.setPRNG(r);
    }

	@Test
	public void testSelect() {
		initialize();
		
		for(int i = 0; i < 1000; i++) {
			// Assign any fitness to the individuals
	        for (GAIndividual ind : population.getIndividuals()) {
				ind.setFitness(r.nextInt(25));
			}
	        // Check that selection returns an individual
			assertNotNull(roulette.select(population));
		}
	}

	@Test
	public void testRoulette_StartingPositionisZero() {
        testRoulette(0);
	}

	@Test
	public void testRoulette_StartingPositionisInTheMiddle() {
		testRoulette(N/2);
	}

	@Test
	public void testRoulette_StartingPositionIsTheLast() {
		testRoulette(N-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRoulette_InvalidStartingPosition() {
		testRoulette(N);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void testRoulette_StartingPositionIsOutofBounds() {
		testRoulette(N+1);
	}
	
	private void testRoulette(final int startingPosition) {
		int totalFitness = 0;
	
	    initialize();
	    final List<GAIndividual> individuals = population.getIndividuals();
	    
	    for(int i = 0; i < 1000; i++) {
	        // Assign any fitness and calculate the sum of every individual fitness
			for (GAIndividual ind : individuals) {
				ind.setFitness(r.nextInt(25));
				totalFitness += ind.getFitness();
			}
			
	        int rouletteResult = r.nextInt(totalFitness + 1);
	        assertTrue(rouletteResult >= 0 && rouletteResult <= totalFitness);
	        assertNotNull(roulette.doRoulette(individuals, rouletteResult, startingPosition));
	    }
	}

	private void initialize() {
        // Builds a population with five individuals
        for(int i = 0; i < N; i++) {
        	gen.generate(state, MAX_ORDER, null, population);
        }
	}

}
