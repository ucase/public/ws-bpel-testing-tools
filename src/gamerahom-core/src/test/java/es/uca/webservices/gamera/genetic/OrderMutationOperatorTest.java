package es.uca.webservices.gamera.genetic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.GAPopulation;
import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.log.AncestryLogger;

/**
 * Tests for the {@link OrderMutationOperator} class.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public class OrderMutationOperatorTest {
	
	private static final int MAX_LOCATION = 9;
	private static final int MAX_ATTRIBUTE = 9;

	private AnalysisResults analysis = new AnalysisResults(
		new String[] { "a", "b", "c", "d", "e" },
		new BigInteger[] { BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION),
				BigInteger.valueOf(MAX_LOCATION)},
		new BigInteger[] { BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE),
				BigInteger.valueOf(MAX_ATTRIBUTE)}
	);
	
    private OrderMutationOperator operator = new OrderMutationOperator();
    private GAPopulation population; 
    
    private int order, newOrder;
    
    private boolean mutateAttribute = true;
    private boolean createTree = false;
    
    private GAIndividual ind;

    private AncestryLogger logger = new AncestryLogger();   
    private GAState state = new GAState();
	
	private final int N = 5;
	
    BigInteger[] a = {BigInteger.valueOf(1),BigInteger.valueOf(2),
        BigInteger.valueOf(3), BigInteger.valueOf(4),BigInteger.valueOf(5)};
    BigInteger[] b = {BigInteger.valueOf(6),BigInteger.valueOf(7),
        BigInteger.valueOf(8), BigInteger.valueOf(9),BigInteger.valueOf(1)};
    BigInteger[] c = {BigInteger.valueOf(9),BigInteger.valueOf(8),
        BigInteger.valueOf(7), BigInteger.valueOf(6),BigInteger.valueOf(5)};

    private Random rnd;

    @Test
    public void applyingOrderMutation() throws InvalidConfigurationException {
		initialize();
    	operator.validate();
		for(int i = 0; i < 100; i++) {
			generateValidValues();
		
			final GAIndividual child = operator.doMutation(ind, newOrder);
			assertEquals(ind.getOperator(), child.getOperator());
			assertEquals(ind.getLocation(), child.getLocation());
			assertEquals(ind.getAttribute(), child.getAttribute());
			assertEquals(newOrder, child.getOrder());
		}		
    }

	@Test
	public void testMutationWithRandomRange() {
		initialize();
		doTestMutation();
	}

	@Test
	public void testMutationWithRandomRangePercent() {
		initialize();
		operator.setRandomRange(null);
		operator.setRandomRangePercent(0.1);
		doTestMutation();
	}

	private void doTestMutation() {
		generateValidValues();
		operator.apply(state, population, population, logger, mutateAttribute, createTree);

		final List<GAIndividual> inds = population.getIndividuals();
		assertNotNull(inds.get(0));
		assertNotNull(inds.get(1));
		assertNotSame(inds.get(0), inds.get(1));
		
		assertTrue(population.getPopulationSize() == 2);
	}
	
    @Test (expected = InvalidConfigurationException.class)
    public void applyingWrongRandomRangeInOrderMutationOperator() throws InvalidConfigurationException {
    	operator.setRandomRange(-1);
    	operator.setScaleFactor(0.5);
    	operator.validate();
    }
    
    @Test (expected = InvalidConfigurationException.class)
    public void applyingTooLargeScaleFactorInOrderMutationOperator() throws InvalidConfigurationException {
    	operator.setRandomRange(1);
    	operator.setScaleFactor(2d);
    	operator.validate();
    }

    @Test (expected = InvalidConfigurationException.class)
    public void applyingTooSmallScaleFactorInOrderMutationOperator() throws InvalidConfigurationException {
    	operator.setRandomRange(1);
    	operator.setScaleFactor(-2d);
    	operator.validate();
    }

    @Test (expected = InvalidConfigurationException.class)
    public void applyingNoRandomRangeInOrderMutationOperator() throws InvalidConfigurationException {
    	operator.setRandomRange(null);
    	operator.setScaleFactor(0.5);
    	operator.validate();
    }

    @Test (expected = InvalidConfigurationException.class)
    public void applyingBothRandomRangeOptionsInOrderMutationOperator() throws InvalidConfigurationException {
    	operator.setRandomRange(5);
    	operator.setRandomRangePercent(0.2);
    	operator.setScaleFactor(0.5);
    	operator.validate();
    }

    @Test (expected = InvalidConfigurationException.class)
    public void applyingTooSmallRandomRangePercentInOrderMutationOperator() throws InvalidConfigurationException {
    	operator.setRandomRangePercent(-1.0);
    	operator.setScaleFactor(0.5);
    	operator.validate();
    }

    @Test (expected = InvalidConfigurationException.class)
    public void applyingTooLargeRandomRangePercentInOrderMutationOperator() throws InvalidConfigurationException {
    	operator.setRandomRangePercent(1.1);
    	operator.setScaleFactor(0.5);
    	operator.validate();
    }

	private void generateValidValues() {

		state.setAnalysisResults(analysis);
	    logger.setConsole(true);
	    
		Collections.shuffle(Arrays.asList(a));
		Collections.shuffle(Arrays.asList(b));
		Collections.shuffle(Arrays.asList(c));
		order = rnd.nextInt(a.length) + 1;

		population = new GAPopulation();
        ind = new GAIndividual(a, b, c, order);
        ind.setFitness(3);
        population.addIndividual(ind);

		newOrder = rnd.nextInt(N) + 1;
	}

	private void initialize() {
		rnd = new Random(0);
        operator.setRandomRange(N);
        operator.setScaleFactor(0.5);
        operator.setPRNG(rnd);
	}

}
