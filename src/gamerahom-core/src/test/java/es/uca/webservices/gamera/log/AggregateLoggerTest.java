package es.uca.webservices.gamera.log;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.gamera.api.GAState;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.log.GALogger;

/**
 * Tests for the {@link AggregateLogger} class.
 * 
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public class AggregateLoggerTest {
	
	private static final String OUTPUT_TXT = "output_AggregateLoggerTest.txt";

    private GALogger sublogger1;
    private GALogger sublogger2;
    private AggregateLogger aggLogger;

    @Before
    public void setUp() {
        List<GALogger> loggers = new ArrayList<GALogger>();
        sublogger1 = mock(GALogger.class);
        sublogger2 = mock(GALogger.class);
        loggers.add(sublogger1);
        loggers.add(sublogger2);

        aggLogger = new AggregateLogger(loggers);
    }

    @Test
    public void testSeveralLogger() {
        MessageLogger msgLogger = new MessageLogger();
        
        HofLogger hofLogger = new HofLogger();
        hofLogger.setConsole(false);
        hofLogger.setFile(OUTPUT_TXT);
        
        List<GALogger> loggers = new ArrayList<GALogger>();
        loggers.add(msgLogger);
        loggers.add(hofLogger);
        
        aggLogger = new AggregateLogger(loggers);
        
        // This instruction should print by console a message from
        // the MessageLogger and the message produced by the method started
        // of the HofLogger. In this case, HofLogger doesn't override this
        // method, so the file will be empty.
        
        aggLogger.started(new GAState());

        assertTrue(new File(OUTPUT_TXT).exists() == true);
    }
    
    @Test
    public void startIsAggregated() {
        GAState state = new GAState();
        aggLogger.started(state);
        verify(sublogger1).started(state);
        verify(sublogger2).started(state);
    }

    @Test
    public void finishIsAggregated() {
        GAState state = new GAState();
        state.setCurrentGeneration(0);
        state.setTotalNumberOfMutants(10);
        aggLogger.finished(state);
        verify(sublogger1).finished(state);
        verify(sublogger2).finished(state);
    }

    @Test
    public void startAnalysisIsAggregated() {
       aggLogger.startedAnalysis(null);
        verify(sublogger1).startedAnalysis(null);
        verify(sublogger2).startedAnalysis(null);
    }

    @Test
    public void finishAnalysisIsAggregated() {
       aggLogger.finishedAnalysis(null);
        verify(sublogger1).finishedAnalysis(null);
        verify(sublogger2).finishedAnalysis(null);
    }

    @Test
    public void finishComparisonIsAggregated() {
        aggLogger.finishedComparison(null,null);
        verify(sublogger1).finishedComparison(null, null);
        verify(sublogger2).finishedComparison(null, null);
    }
    
    @Test
    public void startedComparisonIsAggregated() {
        aggLogger.startedComparison(null,null);
        verify(sublogger1).startedComparison(null, null);
        verify(sublogger2).startedComparison(null, null);
    }

    @Test
    public void finishExtendedComparisonIsAggregated() {
        aggLogger.finishedComparison(null, null, null, null);
        verify(sublogger1).finishedComparison(null, null, null, null);
        verify(sublogger2).finishedComparison(null, null, null, null);
    }

    @Test
    public void startExtendedComparisonIsAggregated() {
        aggLogger.startedComparison(null, null, null);
        verify(sublogger1).startedComparison(null, null, null);
        verify(sublogger2).startedComparison(null, null, null);
    }

    @Test
    public void finishEvaluationIsAggregated() {
    	aggLogger.finishedEvaluation(null, null);
        verify(sublogger1).finishedEvaluation(null, null);
        verify(sublogger2).finishedEvaluation(null, null);
    }
    
    @Test
    public void startEvaluationIsAggregated() {
    	aggLogger.startedEvaluation(null, null);
        verify(sublogger1).startedEvaluation(null, null);
        verify(sublogger2).startedEvaluation(null, null);
    }
    
    @Test
    public void newGenerationIsAggregated() {
        GAState state = new GAState();
        state.setCurrentGeneration(0);
        aggLogger.newGeneration(state, null);
        verify(sublogger1).newGeneration(state, null);
        verify(sublogger2).newGeneration(state, null);
    }
    
    @Test(expected = InvalidConfigurationException.class)
    public void AggregatedLoggerIsClear() throws Exception {
    	aggLogger = new AggregateLogger(null);
    	aggLogger.validate();
    }
}
