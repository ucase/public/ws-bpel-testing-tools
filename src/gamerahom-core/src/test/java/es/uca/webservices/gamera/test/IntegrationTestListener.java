package es.uca.webservices.gamera.test;

import org.junit.runner.Description;
import org.junit.runner.notification.RunListener;

/**
 * JUnit test listener which produces additional test progress messages for long-running integration tests.
 *
 * @author Antonio García-Domínguez
 */
public class IntegrationTestListener extends RunListener {

	@Override
	public void testStarted(Description description) throws Exception {
		super.testStarted(description);
		System.out.println(String.format(
				"Test %s#%s started",
				description.getClassName(),
				description.getMethodName()));
	}

	@Override
	public void testFinished(Description description) throws Exception {
		super.testFinished(description);

		System.out.println(String.format(
				"Test %s#%s finished",
				description.getClassName(),
				description.getMethodName()));
	}
	
}
