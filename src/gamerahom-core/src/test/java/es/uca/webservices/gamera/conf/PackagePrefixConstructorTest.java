package es.uca.webservices.gamera.conf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.ConstructorException;

/**
 * Tests for the package prefix SnakeYAML constructor.
 *
 * @author Antonio García-Domínguez
 * @version 1.0
 */
public class PackagePrefixConstructorTest {

    @Test
    public void missingRootIsReported() throws Exception {
        try {
            Yaml parser = getParser();
            parser.load("!!do.not.exist {}");
            fail("A ConstructorException was expected");
        } catch (ConstructorException ex) {
            assertClassWasNotFound(ex);
        }
    }

    @Test
    public void wrongPrefixDoesNotWork() throws Exception {
        try {
            Yaml parser = getParser("es.uca.webservices.wrong");
            parser.load("!!conf.TestBean");
            fail("A ConstructorException was expected");
        } catch (ConstructorException ex) {
            assertClassWasNotFound(ex);
        }
    }

    @Test
    public void fullNameWorks() throws Exception {
        assertFileHasExpectedData(getParser(), "full-name");
    }

    @Test
    public void shortenedNameWorks() throws Exception {
        assertFileHasExpectedData(getParser(), "shortened-name");
    }

    @Test
    public void shortenedNameWithoutRootTagWorks() throws Exception {
        assertFileHasExpectedData(getParserWithRoot(TestBean.class.getName()),
                "shortened-name-no-root");
    }

    private void assertFileHasExpectedData(final Yaml parser,
            final String classifier) throws ClassNotFoundException {
        TestBean bean = (TestBean) parser.load(getClass().getResourceAsStream(
                "/test-bean-" + classifier + ".yaml"));
        assertEquals("Hi", bean.getName());
        assertEquals("Blabla, Inc. 24 Street, California\n", bean.getAddress());
        assertNotNull(bean.getNested());

        TestBean nested = bean.getNested();
        assertEquals("Again", nested.getName());
        assertEquals("Humbug", nested.getAddress());
        assertNull(nested.getNested());
    }

    private void assertClassWasNotFound(ConstructorException ex) {
        assertContains(
                "The constructor exception should be due to a ClassNotFoundException",
                "Class not found", ex.getCause().getMessage());
    }

    private void assertContains(String msg, String needle, String haystack) {
        if (!haystack.contains(needle)) {
            fail(msg);
        }
    }

    private Yaml getParser() throws ClassNotFoundException {
        return getParser(ConfigurationLoader.CLASS_TAG_PREFIX);
    }

    private Yaml getParser(final String prefix) {
        return new Yaml(new PackagePrefixConstructor(prefix));
    }

    private Yaml getParserWithRoot(final String root)
            throws ClassNotFoundException {
        return new Yaml(new PackagePrefixConstructor(root,
                ConfigurationLoader.CLASS_TAG_PREFIX));
    }
}
