package es.uca.webservices.gamera.conf;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Test;

import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.generate.GAIndividualGenerator;
import es.uca.webservices.gamera.api.generate.GAIndividualGeneratorOptions;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperator;
import es.uca.webservices.gamera.api.genetic.GAGeneticOperatorOptions;
import es.uca.webservices.gamera.api.log.GALogger;
import es.uca.webservices.gamera.api.select.GASelectionOperator;
import es.uca.webservices.gamera.api.select.GASelectionOperatorOptions;
import es.uca.webservices.gamera.api.term.GATerminationCondition;

/**
 * Tests for {@link ConfigurationLoader} and {@link Configuration}.
 *
 * @author Antonio García-Domínguez, Emma Blanco-Muñoz
 * @version 1.0
 */
public class ConfigurationTest {

    private static final String POPSIZE_PERCENT_TOTAL_YAML = "popsize-percent-total-mutants.yaml";
	private static final String ALL_OK_YAML = "all-ok.yaml";
	private static final String CUSTOM_FITNESS = "custom-fitness-function.yaml";
	private static final String KNOWN_RATIOS = "known-ratios.yaml";
	private static final String MISSING_ORIGINAL_YAML = "missing-original.yaml";

	@Test(expected = FileNotFoundException.class)
    public void missingConfigurationIsReported() throws Exception {
        ConfigurationLoader loader = new ConfigurationLoader();
        loader.parse(new FileInputStream(new File("do-not-exist.yaml")));
    }

    @Test
    public void goodConfigurationLoadsAndValidatesCorrectly() throws Exception {
        loadConfiguration(ALL_OK_YAML).validate(ConfigurationValidationMode.FULL);
    }
    
    @Test(expected = InvalidConfigurationException.class)
    public void configurationWithInvalidOrder() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.setMaxOrder(0);
        conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test
    public void validAggregateLogger() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        assertNotNull(conf.getAggregateLogger());
    }
    
    @Test(expected = InvalidConfigurationException.class)
    public void zeroPopulationSizeIsInvalid() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.setPopulationSize(0);
        conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected = InvalidConfigurationException.class)
    public void negativePopulationSizeIsInvalid() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.setPopulationSize(-1);
        conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected = InvalidConfigurationException.class)
    public void missingExecutorIsInvalid() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.setExecutor(null);
        conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected = InvalidConfigurationException.class)
    public void missingGeneticOpIsInvalid() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.getGeneticOperators().clear();
        conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test
    public void missingGeneticOpIsValidInRandomSelectionMode() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.getGeneticOperators().clear();
        conf.validate(ConfigurationValidationMode.RANDOM_SELECTION);
    }

    @Test(expected = InvalidConfigurationException.class)
    public void nullIndividualGeneratorsIsInvalid() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.setIndividualGenerators(null);
        conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test
    public void nullIndividualGeneratorsIsValidInRandomSelectionMode() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.setIndividualGenerators(null);
        conf.validate(ConfigurationValidationMode.RANDOM_SELECTION);
    }

    @Test
    public void noSelectionOpIsValid() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.getSelectionOperators().clear();
        conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected = InvalidConfigurationException.class)
    public void nullSelectionOpIsInvalid() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.setSelectionOperators(null);
        conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected = InvalidConfigurationException.class)
    public void nullTerminationConditionsIsInvalid() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.setTerminationConditions(null);
        conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected = InvalidConfigurationException.class)
    public void missingLoggersIsInvalid() throws Exception {
        Configuration conf = loadConfiguration(ALL_OK_YAML);
        conf.getLoggers().clear();
        conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected = InvalidConfigurationException.class)
    public void confWithInvalidExecutorIsInvalid() throws Exception {
        Configuration conf = loadConfiguration(MISSING_ORIGINAL_YAML);
        conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test
    public void configurationComponentsAreValidated() throws Exception {
        Configuration conf = new Configuration();

        final GAExecutor mockExecutor = mock(GAExecutor.class);
        final GAGeneticOperator mockGeneticOperator = mock(GAGeneticOperator.class);
        final GAIndividualGenerator mockIndividualGenerator = mock(GAIndividualGenerator.class);
        final GASelectionOperator mockSelectionOperator = mock(GASelectionOperator.class);
        final GATerminationCondition mockTerminationCondition = mock(GATerminationCondition.class);
        final GALogger mockLogger = mock(GALogger.class);

        final GAGeneticOperatorOptions geneticOpOptions = new GAGeneticOperatorOptions();
        final GAIndividualGeneratorOptions individualGeneratorOptions = new GAIndividualGeneratorOptions();
        final GASelectionOperatorOptions selectionOpOptions = new GASelectionOperatorOptions();
        geneticOpOptions.setProbability(1);
        individualGeneratorOptions.setPercent(0.4);
        selectionOpOptions.setPercent(0.2);

        conf.setPopulationSize(10);
        conf.setExecutor(mockExecutor);
		conf.getGeneticOperators().put(mockGeneticOperator, geneticOpOptions);
		conf.getIndividualGenerators().put(mockIndividualGenerator, individualGeneratorOptions);
		conf.getSelectionOperators().put(mockSelectionOperator, selectionOpOptions);
        conf.getTerminationConditions().add(mockTerminationCondition);
        conf.getLoggers().add(mockLogger);
        conf.validate(ConfigurationValidationMode.FULL);

        verify(mockExecutor).validate();
        verify(mockGeneticOperator).validate();
        verify(mockIndividualGenerator).validate();
        verify(mockSelectionOperator).validate();
        verify(mockTerminationCondition).validate();
        verify(mockLogger).validate();
    }

    @Test
    public void popSizeAsPercentOfTotalMutantsWorks() throws Exception {
    	final Configuration conf = loadConfiguration(POPSIZE_PERCENT_TOTAL_YAML);
    	conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected=InvalidConfigurationException.class)
    public void zeroPopSizeAsPercentOfTotalMutantsIsInvalid() throws Exception {
    	final Configuration conf = loadConfiguration(POPSIZE_PERCENT_TOTAL_YAML);
    	conf.setPopulationSizeAsPercentOfTotalMutants(0d);
    	conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected=InvalidConfigurationException.class)
    public void negativePopSizeAsPercentOfTotalMutantsIsInvalid() throws Exception {
    	final Configuration conf = loadConfiguration(POPSIZE_PERCENT_TOTAL_YAML);
    	conf.setPopulationSizeAsPercentOfTotalMutants(-.1d);
    	conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected=InvalidConfigurationException.class)
    public void greaterThanOnePopSizeAsPercentOfTotalMutantsIsInvalid() throws Exception {
    	final Configuration conf = loadConfiguration(POPSIZE_PERCENT_TOTAL_YAML);
    	conf.setPopulationSizeAsPercentOfTotalMutants(1.1d);
    	conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected=InvalidConfigurationException.class)
    public void bothPopulationSizeOptionsIsInvalid() throws Exception {
    	final Configuration conf = loadConfiguration(POPSIZE_PERCENT_TOTAL_YAML);
    	conf.setPopulationSize(10);
    	conf.validate(ConfigurationValidationMode.FULL);
    }

    @Test(expected=InvalidConfigurationException.class)
    public void noPopulationSizeOptionsIsInvalid() throws Exception {
    	final Configuration conf = loadConfiguration(POPSIZE_PERCENT_TOTAL_YAML);
    	conf.setPopulationSize(null);
    	conf.setPopulationSizeAsPercentOfTotalMutants(null);
    	conf.validate(ConfigurationValidationMode.FULL);
    }

	@Test(expected=InvalidConfigurationException.class)
	public void emptyIncludesIsInvalid() throws Exception {
		final Configuration conf = loadConfiguration(ALL_OK_YAML);
		conf.setIncludedOperators("");
		conf.validate(ConfigurationValidationMode.FULL);
	}

	@Test
	public void emptyExcludesIsValid() throws Exception {
		final Configuration conf = loadConfiguration(ALL_OK_YAML);
		conf.setExcludedOperators("");
		conf.validate(ConfigurationValidationMode.FULL);
	}

	@Test(expected=InvalidConfigurationException.class)
	public void noFitnessFunctionIsInvalid() throws Exception {
		final Configuration conf = loadConfiguration(ALL_OK_YAML);
		conf.setFitnessFunction(null);
		conf.validate(ConfigurationValidationMode.FULL);
	}

	@Test
	public void customFitnessFunctionIsValid() throws Exception {
		final Configuration conf = loadConfiguration(CUSTOM_FITNESS);
		conf.validate(ConfigurationValidationMode.FULL);
	}

	@Test
	public void knownRatiosIsValid() throws Exception {
		final Configuration conf = loadConfiguration(KNOWN_RATIOS);
		conf.validate(ConfigurationValidationMode.FULL);
	}

    private Configuration loadConfiguration(final String basename)
			throws FileNotFoundException, ClassNotFoundException,
			InvalidConfigurationException {
		ConfigurationLoader loader = new ConfigurationLoader();
		return loader.parse(getClass().getResourceAsStream("/" + basename));
	}
}
