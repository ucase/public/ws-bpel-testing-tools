package es.uca.webservices.bpel;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.wsdl.Binding;
import javax.wsdl.WSDLException;
import javax.xml.namespace.QName;

import org.junit.Test;

import es.uca.webservices.wsdl.analyzer.SchemaReadingException;
import es.uca.webservices.wsdl.util.WSDL2XSDTreeException;

/**
 * Tests for the sample Auction composition. This composition uses endpoint
 * assignments, which confused the instrumenter.
 * 
 * @author Antonio García-Domínguez
 */
public class AuctionTest extends CompositionTestBase {

	private static final String AUCTION_WSDL_TARGET_NS = "http://example.com/auction/wsdl/auctionService/";

	public AuctionTest() throws Exception {
		super("auction");
	}

	@Test
	public void dependencies() throws Exception {
		assertCorrectDependenciesAreListed(new String[]{
			"auctionServiceInterface.wsdl"
		});
	}

	@Test
	public void soapStyleDefault() throws Exception {
		final Binding binding = bpelProcess.getWSDLCatalog().getBinding(
				new QName(AUCTION_WSDL_TARGET_NS, "BuyerAnswerSOAPBinding"));
		assertSOAPStyleIs(BPELConstants.SOAP_DOCUMENT_STYLE, bpelProcess, binding);
	}

	@Test
	public void soapStyleDoc() throws Exception {
		final Binding binding = bpelProcess.getWSDLCatalog().getBinding(
				new QName(AUCTION_WSDL_TARGET_NS, "SellerAnswerSOAPBinding"));
		assertSOAPStyleIs(BPELConstants.SOAP_DOCUMENT_STYLE, bpelProcess, binding);
	}

	@Test
	public void soapStyleRPC() throws Exception {
		final Binding binding = bpelProcess.getWSDLCatalog().getBinding(
				new QName(AUCTION_WSDL_TARGET_NS, "AuctionRegistratonAnswerPT_RPC"));
		assertSOAPStyleIs(BPELConstants.SOAP_RPC_STYLE, bpelProcess, binding);
	}

	private void assertSOAPStyleIs(final String expectedStyle,
			final BPELProcessDefinition def, final Binding binding)
			throws InvalidProcessException, IOException, WSDL2XSDTreeException,
			SchemaReadingException, WSDLException {
		assertEquals(expectedStyle, def.getWSDLCatalog().getBindingSOAPStyle(binding));
	}
}
