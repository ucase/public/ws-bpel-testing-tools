package es.uca.webservices.bpel;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.junit.Test;

import es.uca.webservices.bpel.activebpel.DeploymentArchivePackager;

public class DoXSLTransformTest extends CompositionTestBase {

	private static final String XSL_FILENAME = "a_to_b.xsl";

	public DoXSLTransformTest() throws Exception {
		super("doXslTransform");
	}

	@Test
	public void dependencies() throws Exception {
		assertCorrectDependenciesAreListed(new String[]{
			"ReplaceAWithBProcessArtifacts.wsdl",
			XSL_FILENAME
		});
	}

	@Override
	public void generateBPR() throws Exception {
		final File fBPR = new DeploymentArchivePackager(bpelProcess).generateBPR(bprPath);
		final ZipInputStream zfBPR = new ZipInputStream(new FileInputStream(fBPR));

		try {
			ZipEntry entry;
			while ((entry = zfBPR.getNextEntry()) != null) {
				if (("xslt/" + XSL_FILENAME).equals(entry.getName())) {
					return;
				}
			}
			fail("Could not find " + XSL_FILENAME + " inside the generated .bpr");
		} finally {
			zfBPR.close();
		}
	}

}
