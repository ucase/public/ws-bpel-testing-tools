package es.uca.webservices.bpel;

import org.junit.Test;

/**
 * Tests if the MetaSearch composition is correctly instrumented. After
 * starting work on making the TravelReservationService work, this one
 * stopped working.
 *
 * @author Antonio García Domínguez
 */
public class MetaSearchTest extends CompositionTestBase {

	public MetaSearchTest() throws Exception {
		super("metaSearch");
	}

	@Test
	public void dependencies() throws Exception {
		assertCorrectDependenciesAreListed(new String[]{
			"GoogleBridge.wsdl", "MetaSearch.wsdl",
			"msnsearch.wsdl", "msnsearchRef.wsdl"
		});
	}
}
