package es.uca.webservices.bpel;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.activebpel.DeploymentArchivePackager;

/**
 * Base class for all tests which ensure certain compositions can be
 * instrumented. Defines a few predefined tests of its own: tries to create a
 * BPR with an unmodified process definition, generates a Daikon .decls file,
 * and tries to produce an instrumented process definition.
 * 
 * This class assumes that the main BPEL file has the same basename as its
 * containing folder. The folder should be placed along with the rest of the
 * test resource folders in this project.
 * 
 * @author Antonio García Domínguez
 */
public abstract class CompositionTestBase {
	protected final BPELProcessDefinition bpelProcess;
	protected String bpelPath;
	protected String bprPath;
	protected String declsPath;

	private final File resourceFolder;
	private String[] tmpFiles;

	public CompositionTestBase(String subfolderName) throws Exception {
		resourceFolder = new File(subfolderName);
		bpelProcess = new BPELProcessDefinition(getBPELFile());
	}

	public File getBPELFile() throws IOException {
		return new File(getClass().getResource(
				"/" + resourceFolder.getName() +
				"/" + resourceFolder.getName() + ".bpel"
		).getFile());
	}

	public String getResourcePath(String resName) throws IOException {
		return getResourceFile(resName).getCanonicalPath();
	}

	public File getResourceFile(String resName) throws IOException {
		return new File(getBPELFile().getParentFile(), resName);
	}

	@Before
	public void setUp() throws Exception {
		this.bpelPath = getBPELFile().getCanonicalPath();
		this.bprPath  = getResourcePath(resourceFolder.getName() + ".bpr");
		this.declsPath = getResourcePath(resourceFolder.getName() + ".decls");

		this.tmpFiles = new String[] {
				DeploymentArchivePackager.CATALOGXML_FILENAME,
				DeploymentArchivePackager.ORIGINALPDD_FILENAME,
				declsPath
		};
	}

	/** Cleans up all temporary files produced during instrumentation. */
	@After
	public void cleanUp() {
		// Delete all known temporary files
		for (String tmpPath : tmpFiles) {
			File tmpFile = new File(tmpPath);
			tmpFile.delete();
		}
	}

	@Test
	public void generateBPR() throws Exception {
		new DeploymentArchivePackager(bpelProcess).generateBPR(bprPath);
	}

	protected void assertCorrectDependenciesAreListed(final String[] deps)
			throws ParserConfigurationException, SAXException, IOException,
			XPathExpressionException, InvalidProcessException {
				final Set<File> expected = new HashSet<File>();
				for (String path : deps)
				{
					expected.add(getResourceFile(path));
				}

				assertEquals(expected, bpelProcess.getDependencies());
			}
}
