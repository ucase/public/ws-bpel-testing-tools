package es.uca.webservices.bpel;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.bpel.deploy.ZippedDeploymentArchive;

/**
 * Tests for the {@link ZippedDeploymentArchive} class.
 */
public class BPRDeploymentArchiveTest {

    private ZippedDeploymentArchive echoBPR;

    @Before
    public void setUp() throws IOException {
        echoBPR = new ZippedDeploymentArchive(new File("src/test/resources/simplebpel.bpr"));
    }

    @Test
    public void listBPELFiles() throws Exception {
        final String[] paths = echoBPR.getBPELPaths();
        assertArrayEquals(new String[] {"bpel/com.innoq.simplebpel/bpel/simplebpel.bpel"}, paths);
    }

    @Test
    public void getBPELFile() throws Exception {
        final BPELProcessDefinition bpel = echoBPR.getBPEL(echoBPR.getBPELEntries().get(0));
        assertEquals("simplebpel", bpel.getName().getLocalPart());
    }

    @Test
    public void getMainBPELFile() throws Exception {
        final BPELProcessDefinition bpel = echoBPR.getMainBPEL();
        assertEquals("simplebpel", bpel.getName().getLocalPart());
    }

    @Test
    public void createCopyWithReplacedEntries() throws Exception {
        final Map<String, File> replacements = new HashMap<String, File>();
        replacements.put(echoBPR.getMainBPELEntry().getName(), new File("src/test/resources/auction/auction.bpel"));

        final File tmpBPR = File.createTempFile("tmp", ".bpr");
        tmpBPR.deleteOnExit();
        echoBPR.copyWhileReplacing(replacements, tmpBPR);

        final ZippedDeploymentArchive replaced = new ZippedDeploymentArchive(tmpBPR);
        assertEquals("auctionService", replaced.getMainBPEL().getName().getLocalPart());
    }
}
