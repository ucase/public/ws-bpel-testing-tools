package es.uca.webservices.bpel.bpelunit;

import static org.junit.Assert.assertArrayEquals;

import javax.xml.namespace.QName;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests for the {@link FastBPELUnitXMLResultComparator} class.
 *
 * @author Antonio García Domínguez
 */
public class FastBPELUnitXMLResultComparatorTest {

	private FastBPELUnitXMLResultComparator cmp;

	@Before
	public void setUp() {
		cmp = new FastBPELUnitXMLResultComparator();
	}

	@Test
	public void compareNoTestCases() throws Exception {
		assertComparisonWorks(results(), results());		
	}
	
	@Test(expected=IncompatibleResultsException.class)
	public void compareLessTestCasesInOriginal() throws Exception {
		assertComparisonWorks(results(), results(testCase()));
	}

	@Test(expected=IncompatibleResultsException.class)
	public void compareLessTestCasesInMutated() throws Exception {
		assertComparisonWorks(results(testCase()), results());
	}

	@Test
	public void compareOneEmptyTestCase() throws Exception {
		assertComparisonWorks(results(testCase()), results(testCase()), true);
	}

	@Test
	public void compareOneEqualTestCase() throws Exception {
		assertComparisonWorks(
			results(testCase(dataPackage(xmlData("A")))),
			results(testCase(dataPackage(xmlData("A")))),
			true
		);
	}

	@Test
	public void compareOneEqualTestCaseWSHT() throws Exception {
		assertComparisonWorks(
			results(testCase(dataPackageWSHT(xmlDataWSHT("A")))),
			results(testCase(dataPackageWSHT(xmlDataWSHT("A")))),
			true
		);
	}

	@Test
	public void compareTwoFirstEqual() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("A"))),
				testCase(dataPackage(xmlData("B")))
			),
			results(
				testCase(dataPackage(xmlData("A"))),
				testCase(dataPackage(xmlData("C")))
			),
			true, false
		);
	}

	@Test
	public void compareTwoFirstEqualWSHT() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackageWSHT(xmlDataWSHT("A"))),
				testCase(dataPackage(xmlData("B")))
			),
			results(
				testCase(dataPackageWSHT(xmlDataWSHT("A"))),
				testCase(dataPackage(xmlData("C")))
			),
			true, false
		);
	}

	@Test
	public void compareTwoSecondEqual() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("x"))),
				testCase(dataPackage(xmlData("A")))
			),
			results(
				testCase(dataPackage(xmlData("y"))),
				testCase(dataPackage(xmlData("A")))
			),
			false, true
		);
	}

	@Test
	public void compareTwoSecondEqualWSHT() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackageWSHT(xmlDataWSHT("x"))),
				testCase(dataPackage(xmlData("A")))
			),
			results(
				testCase(dataPackageWSHT(xmlDataWSHT("y"))),
				testCase(dataPackage(xmlData("A")))
			),
			false, true
		);
	}

	@Test
	public void compareTwoSecondEqualFirstLessDataPkgInMutated() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("x")), dataPackage(xmlData("x"))),
				testCase(dataPackage(xmlData("A")))
			),
			results(
				testCase(dataPackage(xmlData("x"))),
				testCase(dataPackage(xmlData("A")))
			),
			false, true
		);
	}

	@Test
	public void compareTwoSecondEqualFirstLessDataPkgInMutatedWSHT() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("x")), dataPackageWSHT(xmlDataWSHT("x"))),
				testCase(dataPackage(xmlData("A")))
			),
			results(
				testCase(dataPackage(xmlData("x"))),
				testCase(dataPackage(xmlData("A")))
			),
			false, true
		);
	}

	@Test
	public void compareTwoSecondEqualFirstLessDataPkgInOriginal() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("x"))),
				testCase(dataPackage(xmlData("A")))
			),
			results(
				testCase(dataPackage(xmlData("x")), dataPackage(xmlData("x"))),
				testCase(dataPackage(xmlData("A")))
			),
			false, true
		);
	}

	@Test
	public void compareTwoFirstEqualSecondEmptyInMutated() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("x"))),
				testCase(dataPackage(xmlData("A")))
			),
			results(
				testCase(dataPackage(xmlData("x"))),
				testCase()
			),
			true, false
		);
	}

	@Test(expected=IllegalArgumentException.class)
	public void compareOneEmptyDataPackageInOriginal() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage())
			),
			results(
				testCase(dataPackage(xmlData("x")))
			)
		);
	}

	@Test(expected=IllegalArgumentException.class)
	public void compareOneEmptyDataPackageInMutated() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("x")))
			),
			results(
				testCase(dataPackage())
			)
		);
	}

	@Test(expected=IllegalArgumentException.class)
	public void compareOneIllegalDataPackageInOriginal() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(dataPackage()))
			),
			results(
				testCase(dataPackage(xmlData("x")))
			)
		);
	}

	@Test(expected=IllegalArgumentException.class)
	public void compareOneIllegalDataPackageInMutated() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("x")))
			),
			results(
				testCase(dataPackage(dataPackage()))
			)
		);
	}

	@Test
	public void differentAttributeOrderShouldBeEqual() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("<x a=\"1\" b=\"2\"/>")))
			),
			results(
				testCase(dataPackage(xmlData("<x b=\"2\" a=\"1\"/>")))
			),
			true
		);
	}

	@Test
	public void differentAttributeValueShouldBeDifferent() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("<x a=\"1\" b=\"2\"/>")))
			),
			results(
				testCase(dataPackage(xmlData("<x b=\"3\" a=\"1\"/>")))
			),
			false
		);
	}

	@Test
	public void differentNumberOfElementsShouldBeDifferent() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("<x/><x/>")))
			),
			results(
				testCase(dataPackage(xmlData("<x/>")))
			),
			false
		);
	}

	@Test
	public void differentElementsShouldBeDifferent() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackage(xmlData("<x/>")))
			),
			results(
				testCase(dataPackage(xmlData("<y/>")))
			),
			false
		);
	}

	@Test
	public void differentElementsShouldBeDifferentWSHT() throws Exception {
		assertComparisonWorks(
			results(
				testCase(dataPackageWSHT(xmlDataWSHT("<x/>")))
			),
			results(
				testCase(dataPackageWSHT(xmlDataWSHT("<y/>")))
			),
			false
		);
	}

	@Test
	public void ignoreTagsShouldWorkOnMutated() throws Exception {
		final FastBPELUnitXMLResultComparator cmp = new FastBPELUnitXMLResultComparator();
		cmp.ignoreTags(new QName("y"));
		
		String original = results(
			testCase(dataPackage(xmlData("<x/>")))
		);
		String mutated = results(
			testCase(dataPackage(xmlData("<y/><x/>")))
		);
		assertArrayEquals(new Boolean[]{ true }, cmp.compare(original, mutated));
	}

	@Test
	public void ignoreTagsShouldWorkOnOriginal() throws Exception {
		final FastBPELUnitXMLResultComparator cmp = new FastBPELUnitXMLResultComparator();
		cmp.ignoreTags(new QName("y"));
		
		String original = results(
			testCase(dataPackage(xmlData("<x/><y/>")))
		);
		String mutated = results(
			testCase(dataPackage(xmlData("<x/>")))
		);
		assertArrayEquals(new Boolean[]{ true }, cmp.compare(original, mutated));
	}

	@Test
	public void ignoreTagsShouldWorkWithContent() throws Exception {
		final FastBPELUnitXMLResultComparator cmp = new FastBPELUnitXMLResultComparator();
		cmp.ignoreTags(new QName("y"));

		String original = results(
			testCase(dataPackage(xmlData("<x/><y>something</y>")))
		);
		String mutated = results(
			testCase(dataPackage(xmlData("<x/>")))
		);
		assertArrayEquals(new Boolean[]{ true }, cmp.compare(original, mutated));
	}

	@Test
	public void ignoreTagsShouldWorkWithContentNestedWS() throws Exception {
		final FastBPELUnitXMLResultComparator cmp = new FastBPELUnitXMLResultComparator();
		cmp.ignoreTags(new QName("y"));

		String original = results(
			testCase(dataPackage(xmlData("<x/>")))
		);
		String mutated = results(
			testCase(dataPackage(xmlData(" <y><y>foo</y>bar</y> <x/>")))
		);
		assertArrayEquals(new Boolean[]{ true }, cmp.compare(original, mutated));
	}

	private void assertComparisonWorks(String sOriginal, String sMutated, Boolean... expected) throws Exception {
		assertArrayEquals(expected, cmp.compare(sOriginal, sMutated));
	}

	private String element(String tag, String attr, String... nestedSubstrings) {
		final StringBuffer sbuf = new StringBuffer(String.format("<%s %s>", tag, attr));
		for (String s : nestedSubstrings) {
			sbuf.append(s);
		}
		sbuf.append("</" + tag + ">");
		return sbuf.toString();
	}

	private String results(String... nestedSubstrings) {
		return element(
			"r:testResult",
			String.format("xmlns:r=\"%s\"", BPELUnitXMLResult.BPELUNIT_RESULT_NAMESPACE),
			nestedSubstrings
		);
	}

	private String testCase(String... nestedSubstrings) {
		return element("r:testCase", "", nestedSubstrings);
	}

	private String dataPackage(String... nestedSubstrings) {
		return element(
			"r:dataPackage",
			"name=\"" + FastBPELUnitXMLResultComparator.DATAPKG_NAMEVAL  + "\"", nestedSubstrings);
	}

	private String xmlData(String... nestedSubstrings) {
		return element(
			"r:xmlData",
			"name=\"" + FastBPELUnitXMLResultComparator.XMLDATA_NAMEVAL  + "\"", nestedSubstrings);
	}

	private String dataPackageWSHT(String... nestedSubstrings) {
		return element(
			"r:dataPackage",
			"name=\"" + FastBPELUnitXMLResultComparator.DATAPKG_WSHT_NAMEVAL  + "\"", nestedSubstrings);
	}

	private String xmlDataWSHT(String... nestedSubstrings) {
		return element(
			"r:xmlData",
			"name=\"" + FastBPELUnitXMLResultComparator.XMLDATA_WSHT_NAMEVAL  + "\"", nestedSubstrings);
	}
}
