package es.uca.webservices.bpel;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import es.uca.webservices.bpel.activebpel.DeploymentArchivePackager;
import es.uca.webservices.bpel.xml.XMLDocument;

public class TradeIngressTest extends CompositionTestBase {

	public TradeIngressTest() throws Exception {
		super("tradeIngress");
	}

	@Test
	public void pddUsesBothEstablishmentServices() throws Exception {
		final XMLDocument pdd = new DeploymentArchivePackager(bpelProcess).generatePDD();
		assertEquals("1", pdd.evaluateExpression(
			"count(//*[local-name() = 'myRole' and @service = 'EstablishmentService'])"));
		assertEquals("1", pdd.evaluateExpression(
			"count(//*[local-name() = 'myRole' and @service = 'Establishment2Service'])"));
	}
}
