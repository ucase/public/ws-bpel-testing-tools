package es.uca.webservices.bpel;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.bpel.activebpel.DeploymentArchivePackager;
import es.uca.webservices.bpel.io.util.FileUtils;

/**
 * Tests that BPR packaging works with paths that have spaces in them.
 *
 * @author Antonio García-Domínguez
 */
public class FolderWithSpacesTest {

	private final File fOriginalResourceDir = new File("src/test/resources/metaSearch");
	private final String BPEL_BASENAME = "metaSearch.bpel"; 
	private File fResourceDir;
	private File fBPRLocation;

	@Before
	public void createResourceDirectory() throws Exception {
		fResourceDir = FileUtils.createTemporaryDirectory("has spaces", "tmp");
		org.apache.commons.io.FileUtils.copyDirectory(fOriginalResourceDir, fResourceDir);
	}

	@Before
	public void createBPRDestination() throws Exception {
		fBPRLocation = File.createTempFile("folderspaces", ".bpr");
	}

	@After
	public void removeResourceDirectory() throws Exception {
		if (fResourceDir != null) {
			org.apache.commons.io.FileUtils.deleteDirectory(fResourceDir);
		}
	}

	@After
	public void removeBPRDestination() throws Exception {
		if (fBPRLocation != null) {
			fBPRLocation.delete();
		}
	}

	@Test
	public void packageWithSpaces() throws Exception {
		final BPELProcessDefinition bpel = new BPELProcessDefinition(new File(fResourceDir, BPEL_BASENAME));
		new DeploymentArchivePackager(bpel).generateBPR(fBPRLocation);
		assertTrue(fBPRLocation.canRead());
	}
}
