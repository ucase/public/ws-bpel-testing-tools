package es.uca.webservices.bpel.bpelunit;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;

/**
 * Unit tests for BPELUnitSpecification
 * @author Antonio García Domínguez
 */
public class LoanStructuredSpecTest {
    private BPELUnitSpecification spec;
    private static final String BPR_NAME = "ServicioPrestamos.bpr";
    private static final String BPTS_RESOURCE = "/bpts/loanStructured.bpts";

    private File getBPTSFile() {
        return new File(getClass().getResource(BPTS_RESOURCE).getFile());
    }

    @Test
    public void bprPath() throws XPathExpressionException, IOException {
        assertEquals(BPR_NAME, spec.getBPRFileOption());
    }

    @Before
    public void setUp() throws ParserConfigurationException, SAXException, IOException {
        spec = new BPELUnitSpecification(getBPTSFile());
    }
}
