package es.uca.webservices.bpel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.w3c.dom.Element;

import es.uca.webservices.bpel.wsdl.BPELPropertyAlias;
import es.uca.webservices.bpel.wsdl.WSDLCatalog;


public class ShippingTest extends CompositionTestBase {

	public ShippingTest() throws Exception {
		super("shipping");
	}

	@Test
	public void dependencies() throws Exception {
		assertCorrectDependenciesAreListed(new String[] {
				"ship.xsd", "shippingLT.wsdl",
				"shippingProperties.wsdl",
				"shippingPT.wsdl"
		});
	}

	@Test
	public void propertyAliases() throws Exception {
		final Element firstPP = bpelProcess.getInstrumentableSequences().get(0);

		final List<BPELVariable> vars = bpelProcess.getProgramPointVariables(firstPP);
		final BPELVariable varShipRequest = vars.get(0);
		final BPELVariable varShipNotice = vars.get(1);

		final WSDLCatalog catalog = bpelProcess.getWSDLCatalog();

		final Set<BPELPropertyAlias> aliasesRequest = catalog.getVariablePropertyAliases(varShipRequest);
		assertEquals(4, aliasesRequest.size());
		assertCanFindAllReferredProperties(catalog, aliasesRequest);

		final Set<BPELPropertyAlias> aliasesNotice = catalog.getVariablePropertyAliases(varShipNotice);
		assertEquals(2, aliasesNotice.size());
		assertCanFindAllReferredProperties(catalog, aliasesNotice);
		
		final Set<BPELPropertyAlias> aliases = catalog.getVariablePropertyAliases();
		assertEquals(6, aliases.size());
	}

	@Test
	public void doNotInstrumentProgramPointsByDefaultIsHonored() throws Exception {
		bpelProcess.setInstrumentSequencesByDefault(false);
		assertEquals(0, bpelProcess.getInstrumentableSequences().size());
	}

	private void assertCanFindAllReferredProperties(
			final WSDLCatalog catalog,
			final Set<BPELPropertyAlias> aliasesRequest) {
		for (BPELPropertyAlias alias : aliasesRequest) {
			final BPELProperty prop = catalog.getProperty(alias.getPropertyName());
			assertNotNull(prop);
		}
	}
}
