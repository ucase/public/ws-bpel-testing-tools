package es.uca.webservices.bpel.bpelunit;

import java.io.File;
import java.util.List;

/**
 * Java bean which encapsulates the results of running a BPELUnit test suite.
 *
 * @author Antonio García-Domínguez
 */
public class ExecutionResults {

	private final int testCaseCount;
	private final File testReportFile, logsDirectory;
	private final List<Long> testWallNanos;

	public ExecutionResults(int testCaseCount, File testReportFile, List<Long> testWallNanos, File logsDirectory) {
		assert testWallNanos.size() <= testCaseCount : "# times (" + testWallNanos.size() + ") <= testCaseCount (" + testCaseCount + ")";

		this.testCaseCount = testCaseCount;
		this.testReportFile = testReportFile;
		this.testWallNanos = testWallNanos;
		this.logsDirectory = logsDirectory;
	}

	public int getTestCasesRun() {
		return testWallNanos.size();
	}

	public int getTestCaseCount() {
		return testCaseCount;
	}

	public File getTestReportFile() {
		return testReportFile;
	}

	public List<Long> getTestWallNanos() {
		return testWallNanos;
	}

	public File getLogsDirectory() { return logsDirectory; }
}
