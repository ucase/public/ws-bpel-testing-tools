package es.uca.webservices.bpel.bpelunit;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.bpelunit.framework.BPELUnitRunner;
import net.bpelunit.framework.base.BPELUnitBaseRunner;
import net.bpelunit.framework.control.deploy.IBPELDeployer;
import net.bpelunit.framework.control.deploy.activebpel.ActiveBPELDeployer;
import net.bpelunit.framework.control.deploy.ode.ODEDeployer;
import net.bpelunit.framework.exception.ConfigurationException;
import net.bpelunit.framework.exception.DeploymentException;
import net.bpelunit.framework.exception.SpecificationException;
import net.bpelunit.framework.model.ProcessUnderTest;
import net.bpelunit.framework.model.test.PartnerTrack;
import net.bpelunit.framework.model.test.TestCase;
import net.bpelunit.framework.model.test.TestSuite;
import net.bpelunit.framework.model.test.data.SendDataSpecification;
import net.bpelunit.framework.model.test.report.ITestArtefact;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.log4j.varia.NullAppender;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.util.Pair;

/**
 * Runner which implements several methods to allow for a higher level of
 * customization over the ActiveBPEL deployer and the ports used by ActiveBPEL
 * and BPELUnit. The runner also adds a useful test listener which aborts the
 * test if it hasn't completed its execution 1 second after the client track
 * is finished.
 *
 * @author Antonio García-Domínguez
 */
public abstract class CustomizedRunner extends BPELUnitBaseRunner {

	/**
	 * Default number of milliseconds that BPELUnit should wait for before
	 * aborting a test case in a test suite and going to the next one.
	 */
	public static final int DEFAULT_TIMEOUT = 20000;

	/**
	 * Default port on which the BPELUnit mockups should listen.
	 */
	public static final int DEFAULT_BPELUNIT_PORT = 7777;

	/**
	 * Default port on which the embedded BPEL engine should listen.
	 */
	public static final int DEFAULT_ENGINE_PORT = 8080;

	/** Name of the log filter which produces one log per process execution. */
	public static final String FULL_LOGLEVEL = "full";

	/** Name of the log filter which does not produce any process execution logs. */
	public static final String NONE_LOGLEVEL = "none";

	private static final org.slf4j.Logger LOGGER = LoggerFactory
			.getLogger(CustomizedRunner.class);

	protected TestSuite testSuite = null;
	protected TestTimesListener timesListener = null;
	protected LogEachTestCase logEachTestCase = null;

	private String fLoggingFilterName = NONE_LOGLEVEL;

	private File fBPRDirectory;
	private String fArchiveLocation;
	private int fEnginePort = DEFAULT_ENGINE_PORT;
	private int fBPELUnitPort = DEFAULT_BPELUNIT_PORT;
	private boolean fAbortAfterClientTrack = false;

	private int fTestTimeout = DEFAULT_TIMEOUT;

	// For HTTP Basic auth
	private String fBPELUsername, fBPELPassword;

	private String fIgnoreXPath;

	public CustomizedRunner(File testSpecification)
			throws ConfigurationException, SpecificationException {
		initialize(getOptions());
		testSuite = loadTestSuite(testSpecification);
	}

	private Map<String, String> getOptions() {
		/**
		 * Tell BPELUnit that extensions are OK so long as they're implemented
		 * or mustUnderstand is set to false. Also, change the timeout to 20
		 * seconds: BPELUnit's new default is 50 seconds!
		 */
		Map<String, String> options = new HashMap<String, String>();
		options.put(BPELUnitRunner.SKIP_UNKNOWN_EXTENSIONS, "true");
		options.put(BPELUnitRunner.GLOBAL_TIMEOUT, Integer.toString(getTestTimeout()));

		return options;
	}

	/**
	 * Called by one of the ancestors to configure the logging system.
	 * 
	 * @throws ConfigurationException
	 *             There are errors in the configuration of BPELUnit
	 */
	@Override
	public void configureLogging() throws ConfigurationException {
		Logger.getRootLogger().addAppender(new NullAppender());
	}

	/**
	 * Returns the number of test cases in the test suite.
	 * 
	 * @return Number of test cases in the test suite.
	 */
	public Integer getTestCaseCount() {
		return testSuite.getTestCaseCount();
	}

	/**
	 * Returns the names of the test cases in the test suite.
	 */
	public List<String> getTestCaseNames() {
		return testSuite.getTestCases();
	}

	/* PROTECTED METHODS FOR CUSTOMIZING SUBCLASSES */

	/**
	 * Subclasses should call this method after {@link TestSuite#setUp()} and
	 * before {@link TestSuite#run()}, so the deployer is correctly customized
	 * (as long as we're using ActiveBPEL).
	 * 
	 * @throws DeploymentException
	 *             The deployer could not be properly customized.
	 */
	protected void customizeDeployer() throws DeploymentException {
		ProcessUnderTest put = testSuite.getProcessUnderTest();
		IBPELDeployer deployer = put.getDeployer();
		if (deployer instanceof ActiveBPELDeployer) {
			ActiveBPELDeployer aDeployer = (ActiveBPELDeployer) deployer;
			try {
				if (getDeploymentDirectory() != null) {
					aDeployer.setDeploymentDirectory(getDeploymentDirectory()
							.getCanonicalPath());
				}
				if (getArchiveLocation() != null) {
					aDeployer.setBPRFile(getArchiveLocation());
				}

				URL originalAdminURL = new URL(aDeployer
						.getAdministrationServiceURL());
				URL originalDeployURL = new URL(aDeployer
						.getDeploymentAdminServiceURL());
				URL customAdminURL = new URL(originalAdminURL.getProtocol(),
						originalAdminURL.getHost(), getEnginePort(),
						originalAdminURL.getPath());
				URL customDeployURL = new URL(originalDeployURL.getProtocol(),
						originalDeployURL.getHost(), getEnginePort(),
						originalDeployURL.getPath());
				aDeployer
						.setAdministrationServiceURL(customAdminURL.toString());
				aDeployer.setDeploymentAdminServiceURL(customDeployURL
						.toString());
				
				aDeployer.setUsername(fBPELUsername);
				aDeployer.setPassword(fBPELPassword);
			} catch (IOException e) {
				throw new DeploymentException("Could not customize deployer", e);
			}
		}
		else if (deployer instanceof ODEDeployer) {
			final ODEDeployer oDeployer = (ODEDeployer)deployer;
			if (getArchiveLocation() != null) {
				oDeployer.setDeploymentArchive(getArchiveLocation());
			}
			String newURL = oDeployer.getODEDeploymentServiceURL();
			if(!newURL.contains(Integer.toString(getEnginePort()))){
				String [] port = newURL.split("[0-9]+");
				oDeployer.setODEDeploymentServiceURL(port[0]+getEnginePort()+port[1]);	
			}
		}
	}

	/**
	 * Method which updates the target URLs that point to ActiveBPEL with the
	 * port returned by {@link #getEnginePort()}. This method should be called
	 * right before calling {@link TestSuite#setUp()}.
	 * 
	 * @throws SpecificationException
	 *             Some target URL could not be customized.
	 */
	protected void customizeTargetURLs() throws SpecificationException {
		traverseCustomizedTargetURL(testSuite);
	}

	/**
	 * Method which updates BPELUnit's base URL so its mockups will listen on
	 * the port returned by {@link #getBPELUnitPort()}.
	 *
	 * @throws SpecificationException
	 *             Could not create the URL with the new port.
	 */
	protected void customizeBaseURL() throws SpecificationException {
		URL baseURL = testSuite.getBaseURL();
		URL revisedBaseURL;
		try {
			revisedBaseURL = new URL(baseURL.getProtocol(), baseURL.getHost(),
					getBPELUnitPort(), baseURL.getPath());
			testSuite.setBaseURL(revisedBaseURL);
		} catch (MalformedURLException e) {
			throw new SpecificationException(
					"Could not customize the base URL", e);
		}
	}

	private void traverseCustomizedTargetURL(ITestArtefact artefact)
			throws SpecificationException {
		if (artefact instanceof SendDataSpecification) {
			SendDataSpecification spec = (SendDataSpecification) artefact;
			String sTargetURL = spec.getTargetURL();
			URL url;
			try {
				url = new URL(sTargetURL);
				if (url.getPath().contains("active-bpel")  || url.getPath().contains("/ode/")) {
					// URL is an ActiveBPEL URL, customize it
					try {
						URL revisedURL = new URL(url.getProtocol(), url
								.getHost(), getEnginePort(), url.getPath());
						spec.setTargetURL(revisedURL.toString());
					} catch (MalformedURLException e) {
						throw new SpecificationException(
								"Could not customize target URL '" + sTargetURL + "'", e);
					}
				}
			} catch (MalformedURLException e1) {
				// The target URL is not a well-formed URL. Leave it as-is so
				// BPELUnit deals with it
				LOGGER.debug("Not a well-formed URL, skipping customization: " + sTargetURL);
			}
		} else {
			for (ITestArtefact child : artefact.getChildren()) {
				traverseCustomizedTargetURL(child);
			}
		}
	}

	/**
	 * Modifies the sending activities of the client track to use HTTP Basic
	 * authentication on the BPEL engine, if it has been set through
	 * {@link #setBPELCredentials(String, String)}.
	 */
	protected void addAuthentication() {
		if (fBPELUsername != null && fBPELPassword != null) {
			traverseAuthentication(testSuite);
		}
	}

	/* GETTERS/SETTERS FOR DEPLOYER/ENGINE/BPELUNIT OPTIONS */
	
	private void traverseAuthentication(ITestArtefact artefact) {
		if (artefact instanceof SendDataSpecification) {
			final SendDataSpecification spec = (SendDataSpecification)artefact;
			final String cookie = fBPELUsername  + ":" + fBPELPassword;
			final String cookieBase64 = new String(Base64.encodeBase64(cookie.getBytes()));
			spec.putProtocolOption("Authorization", "Basic " + cookieBase64);
		}
		else if (artefact instanceof TestCase) {
			for (PartnerTrack pt : ((TestCase)artefact).getPartnerTracks()) {
				if ("client".equals(pt.getPartner().getName())) {
					traverseAuthentication(pt);
					break;
				}
			}
		}
		else {
			for (ITestArtefact child : artefact.getChildren()) {
				traverseAuthentication(child);
			}
		}
	}

	/**
	 * Returns the directory where the .bpr are supposed to be kept. If
	 * <code>null</code>, the default directory according to BPELUnit's
	 * configuration will be used.
	 */
	public File getDeploymentDirectory() {
		return fBPRDirectory;
	}

	/**
	 * Changes the directory where the .bpr are supposed to be kept. If
	 * <code>null</code>, the default directory according to BPELUnit's
	 * configuration will be used.
	 */
	public void setDeploymentDirectory(File bprDirectory) {
		fBPRDirectory = bprDirectory;
	}

	/**
	 * Returns the port on which ActiveBPEL should be listening. By default, it
	 * is set to {@link #DEFAULT_ENGINE_PORT}.
	 */
	public int getEnginePort() {
		return fEnginePort;
	}

	/**
	 * Changes the port on which ActiveBPEL should be listening. By default, it
	 * is set to {@link #DEFAULT_ENGINE_PORT}.
	 */
	public void setEnginePort(int enginePort) {
		fEnginePort = enginePort;
	}

	/**
	 * Returns the port on which BPELUnit should be listening. By default, it is
	 * set to {@link #DEFAULT_BPELUNIT_PORT}.
	 */
	public int getBPELUnitPort() {
		return fBPELUnitPort;
	}

	/**
	 * Changes the port on which BPELUnit should be listening. By default, it is
	 * set to {@link #DEFAULT_BPELUNIT_PORT}.
	 */
	public void setBPELUnitPort(int port) {
		this.fBPELUnitPort = port;
	}

	/**
	 * Returns the path to the .bpr file to be deployed to ActiveBPEL, or the
	 * .zip file to be deployed to ODE. If <code>null</code>, the BPRFile option
	 * set in the BPTS will be used.
	 */
	public String getArchiveLocation() {
		return fArchiveLocation;
	}

	/**
	 * Changes the path to the .bpr file to be deployed to ActiveBPEL, or the
	 * .zip file to be deployed to ODE. If <code>null</code>, the BPRFile option
	 * set in the BPTS will be used.
	 */
	public void setArchiveLocation(String pathToBPR) {
		fArchiveLocation = pathToBPR;
	}

	/**
	 * Returns <code>true</code> if each test should be aborted after the client
	 * track finishes its execution, or <code>false</code> otherwise. By
	 * default, tests are <em>not</em> aborted.
	 */
	public boolean getAbortAfterClientTrack() {
		return fAbortAfterClientTrack;
	}

	/**
	 * Changes whether tests should be aborted after the client track finishes
	 * its execution. By default, tests are <em>not</em> aborted.
	 * 
	 * @param shouldAbort
	 *            If <code>true</code>, tests will abort after the client track
	 *            finishes its execution. Otherwise, they will not.
	 */
	public void setAbortAfterClientTrack(boolean shouldAbort) {
		this.fAbortAfterClientTrack = shouldAbort;
	}

	/**
	 * Returns the number of milliseconds that BPELUnit will wait for each test case before aborting it.
	 */
	public int getTestTimeout() {
		return fTestTimeout;
	}

	/**
	 * Changes the number of milliseconds that BPELUnit will wait for each test case before aborting it.
	 * @throws ConfigurationException BPELUnit reported a configuration error while setting the new timeout.
	 */
	public void setTestTimeout(int timeout) throws ConfigurationException {
		this.fTestTimeout = timeout;
		initialize(getOptions());
	}

	/**
	 * Returns the username/password pair that will be used to authenticate
	 * against the BPEL engine (HTTP Basic), or <code>null</code> if
	 * authentication will not be used (the default).
	 */
	public Pair<String, String> getBPELCredentials() {
		if (fBPELUsername != null && fBPELPassword != null) {
			return new Pair<String, String>(fBPELUsername, fBPELPassword);
		} else {
			return null;
		}
	}

	/**
	 * Adds a default set of test listeners to the runner, and initializes the
	 * <code>timesListener</code> field in order to record test execution times.
	 */
	protected void addListeners() {
		this.timesListener = new TestTimesListener();
		testSuite.addResultListener(timesListener);

		IBPELDeployer deployer = testSuite.getProcessUnderTest().getDeployer();
		if (deployer != null && FULL_LOGLEVEL.equals(fLoggingFilterName)) {
			this.logEachTestCase = new LogEachTestCase();
			logEachTestCase.setBasePath(getLogsDirectory().getAbsolutePath());
			testSuite.addResultListener(logEachTestCase);
		}
		if (getAbortAfterClientTrack()) {
			testSuite.addResultListener(new AbortAfterClientTrackListener());
		}
	}

	/**
	 * Changes the username/password pair that will be used to authenticate
	 * against the BPEL engine (HTTP Basic). If either <code>username</code> or
	 * <code>password</code> are <code>null</code>, authentication will not be
	 * used (the default).
	 */
	public void setBPELCredentials(final String username, final String password) {
		if (username == null || password == null) {
			fBPELUsername = fBPELPassword = null;
		}
		else {
			fBPELUsername = username;
			fBPELPassword = password;
		}
	}

	/**
	 * Returns the XPath expression that will be used to remove the nodes
	 * that should be ignored during the comparisons between the outputs
	 * of the mutant and the original composition. Returns <code>null</code>
	 * if no nodes will be ignored.
	 */
	public String getIgnoreXPath() {
		return fIgnoreXPath;
	}

	/**
	 * Changes the XPath expression that will be used to remove the nodes
	 * that should be ignored during the comparisons between the outputs
	 * of the mutant and the original composition. If <code>null</code>
	 * is passed, this behavior will be disabled.
	 */
	public void setIgnoreXPath(String ignoreXPath) {
		this.fIgnoreXPath = ignoreXPath;
	}

	/**
	 * Returns an unmodifiable list with the nanoseconds required by each test
	 * case in the last run. This method should only be called after running a
	 * test suite.
	 */
	protected List<Long> getTestWallNanos() {
		return timesListener.getTestWallNanos();
	}
	
	/**
	 * Return the engine's log level 
	 */
	public String getLoggingFilterName() {
		return fLoggingFilterName;
	}

	/**
	 * Changes the engine's log level 
	 */
	public void setLoggingFilterName(String fLoggingFilterName) {
		this.fLoggingFilterName = fLoggingFilterName;
	}

	/**
	 * Returns the directory that will store the per-process logs, if they have
	 * been turned on.
	 */
	public File getLogsDirectory() {
		return new File(new File(getArchiveLocation()).getParentFile(), "process-logs");
	}
}
