package es.uca.webservices.bpel.deploy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.InvalidProcessException;

/**
 * <p>
 * Convenience view of the contents in a <code>zip</code>-based BPEL deployment
 * archive (an ActiveBPEL <code>.bpr</code> file or an ODE <code>.zip</code>
 * file). Zip entries are cached for performance: this class assumes that the
 * archive will not change during its objects' lifetime.
 * </p>
 */
public class ZippedDeploymentArchive {

    private final ZipFile bprFile;
    private static final Logger LOGGER = LoggerFactory.getLogger(ZippedDeploymentArchive.class);

    public ZippedDeploymentArchive(File fBPR) throws IOException {
        this.bprFile = new ZipFile(fBPR);
    }

    public List<ZipEntry> getBPELEntries() throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
    	return listEntriesByExtension(".bpel");
    }

    public BPELProcessDefinition getBPEL(ZipEntry bpelEntry) throws IOException, SAXException,
            XPathExpressionException, InvalidProcessException, ParserConfigurationException {
        return new BPELProcessDefinition(bprFile.getInputStream(bpelEntry));
    }

    public ZipEntry getMainBPELEntry() throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        final List<ZipEntry> bpelEntries = getBPELEntries();
        final String mainBPELName = FilenameUtils.getBaseName(bprFile.getName()) + ".bpel";
        ZipEntry mainBPELEntry = bpelEntries.isEmpty() ? null : bpelEntries.get(0);
        for (ZipEntry entry : bpelEntries) {
        	LOGGER.debug("Checking if entry {} is the main BPEL entry", entry.getName());
            final File fEntry = new File(entry.getName());
            if (mainBPELName.equals(fEntry.getName())) {
                mainBPELEntry = entry;
                break;
            }
        }
        if (LOGGER.isDebugEnabled()) {
            if (mainBPELEntry != null) {
                LOGGER.debug("Main BPEL entry for BPR " + bprFile.getName() + " is " + mainBPELEntry);
            } else {
                LOGGER.debug("No main BPEL entry was found in BPR {}", bprFile.getName());
            }
        }
        return mainBPELEntry;
    }

    public String[] getBPELPaths() throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        return listPaths(getBPELEntries());
    }

    /**
     * Returns the main process definition in the deployment archive. If more than
     * one process definition is included in the archive, the one that is named like
     * the <code>.bpr</code> file will take preference. Otherwise, the first <code>.bpel</code>
     * in the <code>.bpr</code> will be picked.
     *
     * If no process definition is available, returns <code>null</code>.
     *
     * @see #getBPEL(ZipEntry)
     */
    public BPELProcessDefinition getMainBPEL()
            throws IOException, SAXException, XPathExpressionException,
            ParserConfigurationException, InvalidProcessException
    {
        final ZipEntry mainBPELEntry = getMainBPELEntry();
        if (mainBPELEntry != null) {
            return getBPEL(mainBPELEntry);
        }
        return null;
    }

    /**
     * Creates a new copy of the <code>.bpr</code> while replacing some files.
     * @param replacements Mapping from paths in the <code>.bpr</code> to the local files
     *                     (<em>not</em> directories) with their new contents.
     * @param fOutput Destination file for the new <code>.bpr</code>.
     */
    public void copyWhileReplacing(final Map<String, File> replacements, final File fOutput) throws IOException {
        ZipOutputStream zOS = new ZipOutputStream(new FileOutputStream(fOutput));
        try {
            final Enumeration<? extends ZipEntry> entries = bprFile.entries();
            while (entries.hasMoreElements()) {
                final ZipEntry entry = entries.nextElement();

                final File replacement = replacements.get(entry.getName());
                if (replacement != null) {
                    final ZipEntry replacedEntry = new ZipEntry(entry.getName());
                    zOS.putNextEntry(replacedEntry);

                    final FileInputStream fIS = new FileInputStream(replacement);
                    try {
                        IOUtils.copy(fIS, zOS);
                    } finally {
                        fIS.close();
                    }
                } else {
                    zOS.putNextEntry(entry);
                    IOUtils.copy(bprFile.getInputStream(entry), zOS);
                }
                zOS.closeEntry();
            }
        } finally {
            zOS.close();
        }
    }

    public void replaceMainBPEL(BPELProcessDefinition def, File fOutput)
            throws IOException, TransformerException, SAXException,
            ParserConfigurationException, XPathExpressionException
    {
        final File fMutatedBPEL = File.createTempFile("mutated", ".bpel");
        fMutatedBPEL.deleteOnExit();
        def.dumpToFile(fMutatedBPEL);

        final Map<String, File> map = new HashMap<String, File>();
        map.put(getMainBPELEntry().getName(), fMutatedBPEL);
        copyWhileReplacing(map, fOutput);
    }

	private String[] listPaths(List<ZipEntry> zipEntries) {
        final List<String> paths = new ArrayList<String>();
        for (ZipEntry entry : zipEntries) {
            paths.add(entry.getName());
        }
        return toArray(paths);
    }

    private String[] toArray(List<String> paths) {
        return paths.toArray(new String[paths.size()]);
    }

    private List<ZipEntry> listEntriesByExtension(String fileSuffix) {
        final List<ZipEntry> filtered = new ArrayList<ZipEntry>();
        final Enumeration<? extends ZipEntry> entries = bprFile.entries();
        while (entries.hasMoreElements()) {
            final ZipEntry entry = entries.nextElement();
            if (entry.getName().endsWith(fileSuffix)) {
                filtered.add(entry);
            }
        }
        return filtered;
    }

}
