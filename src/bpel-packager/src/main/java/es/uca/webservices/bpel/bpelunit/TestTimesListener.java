package es.uca.webservices.bpel.bpelunit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.bpelunit.framework.model.test.ITestResultListener;
import net.bpelunit.framework.model.test.TestCase;
import net.bpelunit.framework.model.test.report.ITestArtefact;

public class TestTimesListener implements ITestResultListener {

	private final List<Long> testTimes = new ArrayList<Long>();
	private long currentTestStart; 

	@Override
	public void testCaseStarted(TestCase testCase) {
		currentTestStart = System.nanoTime();
	}

	@Override
	public void testCaseEnded(TestCase testCase) {
		testTimes.add(System.nanoTime() - currentTestStart);
	}

	@Override
	public void progress(ITestArtefact testArtefact) {
		// nothing to do
	}

	/**
	 * Returns an unmodifiable list with the wall times required by each test, measured in nanoseconds.
	 */
	public List<Long> getTestWallNanos() {
		return Collections.unmodifiableList(testTimes);
	}

}
