package es.uca.webservices.bpel.ode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.wsdl.Definition;
import javax.wsdl.Port;
import javax.wsdl.Service;
import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import es.uca.webservices.bpel.BPELPartnerLink;
import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.activebpel.BPELPackagingException;
import es.uca.webservices.bpel.bpelunit.CustomizedRunner;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.bpel.wsdl.WSDLCatalog;
import es.uca.webservices.bpel.xml.XMLDocument;

/**
 * Class with the necessary logic to generate a deployment <code>.zip</code> for Apache ODE.
 *
 * @author Olga Mena-Gutiérrez, Antonio García-Domínguez
 */
public class ODEDeploymentArchivePackager {

	public static final String DEPLOY_NAMESPACE = "http://www.apache.org/ode/schemas/dd/2007/03";
	private static final int ZIP_BUFSIZE = 8096;

	private final BPELProcessDefinition bpel;
	private int odePort = CustomizedRunner.DEFAULT_ENGINE_PORT;
	private int bpelunitPort = CustomizedRunner.DEFAULT_BPELUNIT_PORT;
	private boolean preserveServiceURLs = false;

	public ODEDeploymentArchivePackager(BPELProcessDefinition def) {
		this.bpel = def;
	}

	/**
	 * Returns the port under which ODE is expected to be listening. By default,
	 * it is {@link CustomizedRunner#DEFAULT_ENGINE_PORT}.
	 */
	public int getODEPort() {
		return odePort;
	}

	/**
	 * Changes the port under which ODE is expected to be listening.
	 *
	 * @see #getODEPort()
	 */
	public void setODEPort(int odePort) {
		this.odePort = odePort;
	}

	/**
	 * Returns the port under which BPELUnit is expected to be listening. By default,
	 * it is {@link CustomizedRunner#DEFAULT_BPELUNIT_PORT}.
	 */
	public int getBPELUnitPort() {
		return bpelunitPort;
	}

	/**
	 * Changes the port under which BPELUnit is expected to be listening.
	 *
	 * @see #getBPELUnitPort()
	 */
	public void setBPELUnitPort(int bpelunitPort) {
		this.bpelunitPort = bpelunitPort;
	}

	/**
	 * Returns <code>true</code> if the URLs in the original .wsdl files should
	 * be preserved, or <code>false</code>. By default, they are not preserved:
	 * the port is changed to {@link #getODEPort()} for the URLs with '/ode/',
	 * and to {@link #getBPELUnitPort()} for the URLs with '/ws/'.
	 */
	public boolean isPreserveServiceURLs() {
		return preserveServiceURLs;
	}

	/**
	 * If <code>preserveServiceURLs</code> is <code>true</code>, the URLs in the
	 * original .wsdl files will be preserved. Otherwise, they may undergo
	 * changes. For the actual changes that are performed, see
	 * {@link #isPreserveServiceURLs()}.
	 * 
	 * @see #isPreserveServiceURLs()
	 */
	public void setPreserveServiceURLs(boolean preserveServiceURLs) {
		this.preserveServiceURLs = preserveServiceURLs;
	}

	/**
	 * Generates the contents of the deploy.xml file.
	 */
	public XMLDocument generateDeploymentDescriptor() throws BPELPackagingException {
		try {
			final WSDLCatalog cat = bpel.getWSDLCatalog();
			final Map<String, String> nsURItoPrefix = createPrefixMap(cat);

			final DocumentBuilderFactory bldF = DocumentBuilderFactory.newInstance();
			bldF.setNamespaceAware(true);
			final Document doc = bldF.newDocumentBuilder().newDocument();

			final Element eDeploy = doc.createElementNS(DEPLOY_NAMESPACE, "deploy");
			for (Map.Entry<String, String> entry : nsURItoPrefix.entrySet()) {
				eDeploy.setAttribute(
						XMLConstants.XMLNS_ATTRIBUTE + ":" + entry.getValue(),
						entry.getKey());
			}
			doc.appendChild(eDeploy);

			Element eProcess = doc.createElementNS(DEPLOY_NAMESPACE, "process");
			eProcess.setAttribute("name", "bpel:" + bpel.getName().getLocalPart());
			eDeploy.appendChild(eProcess);

			Element eActive = doc.createElementNS(DEPLOY_NAMESPACE, "active");
			eActive.setTextContent("true");
			eProcess.appendChild(eActive);

			Element eRetired = doc.createElementNS(DEPLOY_NAMESPACE, "retired");
			eRetired.setTextContent("false");
			eProcess.appendChild(eRetired);

			Element eEvents = doc.createElementNS(DEPLOY_NAMESPACE, "process-events");
			eEvents.setAttribute("generate", "all");
			eProcess.appendChild(eEvents);

			/* Create invoke and provide elements */
			final Set<Pair<QName, String>> usedPorts = new HashSet<Pair<QName, String>>();
			for (BPELPartnerLink pl : bpel.getPartnerLinks()) {
				/* invoke */
				if (pl.getPartnerRole() != null
						&& pl.getPartnerRole().length() > 0) {
					Pair<Service, Port> searchResult = bpel.findService(
							pl.getType(), pl.getPartnerRole(), usedPorts);
					if (searchResult == null) {
						throw new BPELPackagingException(
								String.format(
										"Could not find a matching service for role '%s' in partner link '%s'",
										pl.getPartnerRole(), pl.getName()));
					}

					createServiceElementForPartnerLink(nsURItoPrefix, doc,
							eProcess, pl, "invoke", searchResult);
				}

				/* provide */
				if (pl.getMyRole() != null && pl.getMyRole().length() > 0) {
					Pair<Service, Port> searchResult = bpel.findService(
							pl.getType(), pl.getMyRole(), usedPorts);
					if (searchResult == null) {
						throw new BPELPackagingException(
								String.format(
										"Could not find a matching service for role '%s' in partner link '%s'",
										pl.getMyRole(), pl.getName()));
					}

					createServiceElementForPartnerLink(nsURItoPrefix, doc,
							eProcess, pl, "provide", searchResult);
				}
			}

			return new XMLDocument(doc);
		} catch (Exception ex) {
			throw new BPELPackagingException(ex);
		}
	}

	public File packODE(final File targetZip) throws BPELPackagingException
	{
		try {
			final ZipOutputStream zipOS = new ZipOutputStream(new FileOutputStream(targetZip));
			try {
				// Generate and add the deploy.xml file
				final File fDeploy = File.createTempFile("deploy", ".xml");
				generateDeploymentDescriptor().dumpToFile(fDeploy);
				addFileToZip(zipOS, fDeploy, "deploy.xml");
				fDeploy.delete();

				// Add the dependencies, changing the URLs in the .wsdl files if necessary
				for (final File fDependency : bpel.getDependencies()) {
					if (!preserveServiceURLs && fDependency.getName().endsWith("wsdl")) {
						final File fChangedWSDL = File.createTempFile("changeports", ".wsdl");
						changeWSDLServicePort(fDependency).dumpToFile(fChangedWSDL);
						addFileToZip(zipOS, fChangedWSDL,  fDependency.getName());
						fChangedWSDL.delete();
					} else {
						addFileToZip(zipOS, fDependency, fDependency.getName());
					}
				}

				// Add the BPEL file itself
				addFileToZip(zipOS, bpel.getFile(), bpel.getFile().getName());

				return targetZip;
			} finally {
				zipOS.flush();
				zipOS.close();
			}
		} catch (Exception ex) {
			throw new BPELPackagingException(ex);
		}
	}
	
	private XMLDocument changeWSDLServicePort(File fichWSDL) throws Exception {
		final Document wsdlDoc = new XMLDocument(fichWSDL).getDocument();
		final NodeList addressesList = wsdlDoc.getElementsByTagNameNS("http://schemas.xmlsoap.org/wsdl/soap/", "address");

		for (int i = 0; i < addressesList.getLength(); i++) {
			final NamedNodeMap addressNode = addressesList.item(i).getAttributes();
			for (int j = 0; j < addressNode.getLength(); j++) {
				final String address = addressNode.item(j).getNodeValue();
				final URI uri = new URI(address);
				final String path = uri.getPath();

				if (path.contains("/ode/")) {
					if (uri.getPort() != getODEPort()) {
						addressNode.item(j).setNodeValue(changeURIPort(uri, getODEPort()).toString());
					}
				}
				else if (path.contains("/ws/")) {
					if (uri.getPort() != getBPELUnitPort()) {
						addressNode.item(j).setNodeValue(changeURIPort(uri, getBPELUnitPort()).toString());
					}
				}
			}
		}
		
		return new XMLDocument(wsdlDoc);
	}

	/**
	 * Returns the same URI, but with a different port.
	 */
	private URI changeURIPort(final URI uri, int newPort) throws URISyntaxException {
		return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), newPort, uri.getPath(), uri.getQuery(), uri.getFragment());
	}
	
	private void addFileToZip(ZipOutputStream zipOS, File file, String entryName) throws IOException
	{
		zipOS.putNextEntry(new ZipEntry(entryName));

		FileInputStream is = null;
		try {
			is = new FileInputStream(file);

			byte[] buf = new byte[ZIP_BUFSIZE];
			int readBytes = -1;
			while ((readBytes = is.read(buf)) != -1) {
				zipOS.write(buf, 0, readBytes);
			}

			zipOS.closeEntry();
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}
	
	private void createServiceElementForPartnerLink(
			final Map<String, String> nsURItoPrefix, Document doc,
			Element eProcess, BPELPartnerLink pl, final String tagName,
			Pair<Service, Port> searchResult) {
		final Service service = searchResult.getLeft();
		final Port port = searchResult.getRight();

		Element eInvoke = doc.createElementNS(DEPLOY_NAMESPACE, tagName);
		eInvoke.setAttribute("partnerLink", pl.getName());
		eProcess.appendChild(eInvoke);

		/* Create service element */
		final String prefix = nsURItoPrefix.get(service.getQName()
				.getNamespaceURI());
		Element eServiceI = doc.createElementNS(DEPLOY_NAMESPACE, "service");
		eServiceI.setAttribute("name", prefix + ":"
				+ service.getQName().getLocalPart());
		eServiceI.setAttribute("port", port.getName());
		eInvoke.appendChild(eServiceI);
	}

	private Map<String, String> createPrefixMap(final WSDLCatalog cat) {
		int nsCounter = 0;
		final Map<String, String> nsURItoPrefix = new HashMap<String, String>();
		nsURItoPrefix.put(bpel.getTargetNamespaceURI(), "bpel");
		for (Definition def : cat.getDefinitions()) {
			final String targetNS = def.getTargetNamespace();
			if (!nsURItoPrefix.containsKey(targetNS)) {
				nsURItoPrefix.put(targetNS, "ns" + nsCounter++);
			}
		}
		return nsURItoPrefix;
	}
}
