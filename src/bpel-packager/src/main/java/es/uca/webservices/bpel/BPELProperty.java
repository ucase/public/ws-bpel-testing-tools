package es.uca.webservices.bpel;

import javax.wsdl.extensions.ExtensibilityElement;
import javax.xml.namespace.QName;


/**
 * WSDL4J extensibility element for a WS-BPEL 2.0 variable property. These are
 * defined inside the imported WSDL files.
 *
 * @author Antonio García-Domínguez
 */
public class BPELProperty implements ExtensibilityElement {

	public static final QName TAG = new QName(
			BPELConstants.VARIABLE_PROPERTIES_NAMESPACE, "property");

	private QName name, type, element;

	public BPELProperty(QName name, QName type, QName element) {
		this.name = name;
		this.type = type;
		this.element = element;
	}

	public QName getName() {
		return name;
	}

	public boolean hasType() {
		return type != null;
	}

	public QName getType() {
		return type;
	}

	public boolean hasElement() {
		return element != null;
	}

	public QName getElement() {
		return element;
	}

	@Override
	public QName getElementType() {
		return TAG;
	}

	@Override
	public Boolean getRequired() {
		return false;
	}

	@Override
	public void setElementType(QName type) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setRequired(Boolean required) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((element == null) ? 0 : element.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof BPELProperty)) {
			return false;
		}
		BPELProperty other = (BPELProperty) obj;
		if (element == null) {
			if (other.element != null) {
				return false;
			}
		} else if (!element.equals(other.element)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return String.format("BPELProperty [name=%s, element=%s, type=%s]", name, element, type);
	}
}
