package es.uca.webservices.bpel;

import javax.xml.namespace.QName;

/**
 * Exception to be thrown when a WS-BPEL 2.0 variable property cannot be found.
 * @author Antonio García-Domínguez
 */
public class MissingPropertyException extends Exception {

	private static final long serialVersionUID = 1L;

	private QName propertyName;

	public MissingPropertyException(QName propertyName) {
		super("Could not find the type of the property " + propertyName);
		this.propertyName = propertyName;
	}

	public QName getPropertyName() {
		return propertyName;
	}

}
