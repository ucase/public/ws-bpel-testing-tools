package es.uca.webservices.bpel.bpelunit;

import net.bpelunit.framework.control.util.BPELUnitConstants;
import net.bpelunit.framework.model.test.ITestResultListener;
import net.bpelunit.framework.model.test.PartnerTrack;
import net.bpelunit.framework.model.test.TestCase;
import net.bpelunit.framework.model.test.report.ArtefactStatus;
import net.bpelunit.framework.model.test.report.ITestArtefact;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test case listener that aborts the test 1 second after the client track
 * has finished its execution. This can help speed up some tests in case the
 * composition did not call all the mockups we expected.
 * 
 * NOTE: we assume that the last message sent by the composition to the client
 * is the very last step done by the composition. This is true for most of the
 * compositions, but it might not be always the case. For this reason, this
 * listener should only be used when explicitly requested by the user.
 *
 * @author Antonio García-Domínguez
 */
public class AbortAfterClientTrackListener implements ITestResultListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbortAfterClientTrackListener.class);
	private static final int RETRY_MILLISECONDS = 250;
	private static final int RETRY_COUNT = 4;

	@Override
	public void testCaseStarted(TestCase testCase) {
		// do nothing
	}

	@Override
	public void testCaseEnded(TestCase testCase) {
		// do nothing
	}

	@Override
	public void progress(ITestArtefact testArtefact) {
		if (!(testArtefact instanceof PartnerTrack)) {
			return;
		}

		final PartnerTrack track = (PartnerTrack)testArtefact;
		if (!BPELUnitConstants.CLIENT_NAME.equals(track.getPartnerName())) {
			// This is not the client track
			return;
		}
		if (track.getStatus().isInProgress() || track.getStatus().isInitial()) {
			// The client track is not done yet
			return;
		}

		final TestCase testCase = (TestCase)track.getParent();
		try {
			int i;

			retries: for (i = 0; i < RETRY_COUNT; ++i) {
				synchronized (this) {
					wait(RETRY_MILLISECONDS);
				}

				for (PartnerTrack pt : testCase.getPartnerTracks()) {
					if (pt == track) continue;

					final ArtefactStatus status = pt.getStatus();
					if (status.isInitial() || status.isInProgress()) {
						// This partner track is not done yet - wait a bit more
						continue retries;
					}
				}

				// All partner tracks are done - exit the loop
				break;
			}

			if (i == RETRY_COUNT) {
				// Still not done after all these retries - abort the test
				LOGGER.warn("Aborting test " + testCase.getName() + " as it's taking too long after the client track has finished.");
				testCase.abortTest();
			}
		} catch (InterruptedException ex) {}
	}

}
