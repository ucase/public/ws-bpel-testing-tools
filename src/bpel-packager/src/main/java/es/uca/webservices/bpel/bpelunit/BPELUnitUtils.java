package es.uca.webservices.bpel.bpelunit;

import java.io.IOException;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import net.bpelunit.framework.model.test.data.SendDataSpecification;
import net.bpelunit.framework.model.test.data.XMLData;
import net.bpelunit.framework.model.test.report.ITestArtefact;

import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.BPELProcessNamespaceContext;
import es.uca.webservices.bpel.util.ArrayNodeList;
import es.uca.webservices.bpel.util.XMLUtils;

/**
 * Assorted utility methods for handling BPELUnit test case results.
 *
 * @author Antonio García Domínguez
 */
public final class BPELUnitUtils {

    private static final String WSA_HEADERS_XPATH = "//soapenv:Header/wsa:* | //soapenv:Header/wsa2:* | //soapenv:Header/wsa3:* | //soapenv:Header/wsa4:*";

	// Namespace context for XPath queries on WS-BPEL 2.0 compositions
    private static final NamespaceContext BPEL_NS_CONTEXT = new BPELProcessNamespaceContext();

    private static final Logger LOGGER = LoggerFactory.getLogger(BPELUnitUtils.class);

    private BPELUnitUtils() {}

    /**
     * Collects all outgoing SOAP messages into the provided ArrayNodeList. It
     * is pretty much a Java reimplementation of the XPath query in
     * {@link BPELUnitXMLResult#getCompositionOutboundMessages()}, in order to
     * avoid having to refactor BPELUnit's XMLResultProducer class.
     *
     * @param nodeList
     *            Node list where DOM element nodes should be placed with the
     *            root of each SOAP message.
     * @param artefact
     *            Root artefact from which messages should be collected.
     * @throws XmlException
     *             There was a problem while parsing the wrapped XML data.
     * @throws ParserConfigurationException
     *             There was a problem while confuguring the XML parser for the
     *             pretty-printed XML data.
     * @throws IOException
     *             There was a problem while reading the pretty-printed XML
     *             data.
     * @throws SAXException
     *             There was a problem while parsing the pretty-printed XML
     *             data.
     */
	public static void collectOutgoingSOAPMessageDataNodes(ArrayNodeList nodeList, ITestArtefact artefact)
			throws XmlException, SAXException, IOException, ParserConfigurationException
	{
        if (artefact instanceof XMLData) {
            if (!"SOAP Message data".equals(artefact.getName()) && !"Input XML Data".equals(artefact.getName())) {
                return;
            }
            XMLData data = (XMLData) artefact;

            // Strip declarations and processing instructions
            String contents = XMLUtils.stripDeclarations(data.getXmlData());
            if ("(no message)".equals(contents)) {
               // "(no message)" is a special string added by BPELUnit
               // when no message text is available.
               contents = "";
            }

            // Wrap with <xmlData> element
            final String xmlSource = String
                .format(
                        "<res:xmlData name=\"%s\" xmlns:res=\"%s\">%s</res:xmlData>",
                        data.getName(),
                        BPELUnitXMLResult.BPELUNIT_RESULT_NAMESPACE,
                        contents);
            try {
                final String prettyXmlSource = XMLUtils.prettyPrint(xmlSource);
                Document doc = XMLUtils.parse(prettyXmlSource);
                nodeList.add(doc.getDocumentElement());
            } catch (XmlException ex) {
                LOGGER.error("Error while pretty printing:"
                             + "\n--- Original source ---\n"
                             + contents
                             + "\n--- Cleaned source ---\n"
                             + xmlSource
                             + "\n---\n" + ex.getLocalizedMessage());
                throw ex;
            }
        } else if (!(artefact instanceof SendDataSpecification)) {
            for (ITestArtefact child : artefact.getChildren()) {
                collectOutgoingSOAPMessageDataNodes(nodeList, child);
            }
        }
    }

    /**
     * Compares the results produced by the current test case in two
     * compositions.
     * 
     * @param iTestCase
     *            Position of the current test case in the test suite.
     * @param ownMessages
     *            Node list with the outgoing messages from the first
     *            composition, as produced by
     *            {@link BPELUnitXMLResult#getCompositionOutboundMessages()} or
     *            {@link #collectOutgoingSOAPMessageDataNodes(ArrayNodeList, ITestArtefact)}
     *            .
     * @param otherMessages
     *            Node list with the outgoing messages from the second
     *            composition. Expectations are the same as the previous method.
     *
     * @param ignoreXPath
     *            If not <code>null</code>, an XPath expression that will be used
     *            to remove nodes on both sides before comparison, ignoring them.
     *
     * @return <code>true</code> if all results are equal, <code>false</code>
     *         otherwise.
     * @throws IncompatibleResultsException
     *             The two node lists cannot be compared: either of them
     *             contains no messages or a different number of messages.
     */
    public static boolean compareTestCases(int iTestCase, NodeList ownMessages, NodeList otherMessages, String ignoreXPath) throws IncompatibleResultsException {
        if (ownMessages.getLength() != otherMessages.getLength()) {
            throw new IncompatibleResultsException(
                    String
                            .format(
                                    "Results report a different number of messages in "
                                            + "test case %d and are thus incomparable: %d versus %d",
                                    iTestCase, ownMessages.getLength(),
                                    otherMessages.getLength()));
        }

        boolean allMessagesEqual = true;
        for (int iMessage = 0; allMessagesEqual
                && iMessage < ownMessages.getLength(); ++iMessage) {
            try {
                Node ownMessage   = removeWSAHeaders(ownMessages.item(iMessage));
                Node otherMessage = removeWSAHeaders(otherMessages.item(iMessage));
                if (ignoreXPath != null) {
                	ownMessage = removeByXPath(ignoreXPath, ownMessage);
                	otherMessage = removeByXPath(ignoreXPath, otherMessage);
                }

                allMessagesEqual  = XMLUtils.compare(BPEL_NS_CONTEXT, ownMessage, otherMessage);
            } catch (XPathExpressionException e) {
                throw new IncompatibleResultsException(
                        "Could not compare the messages: there was a "
                                + "problem while evaluating the XPath expression", e);
            }
        }

        return allMessagesEqual;
    }

	public static ArrayNodeList prettyPrintNodes(ArrayNodeList nodes)
			throws XmlException, SAXException, IOException, ParserConfigurationException
	{
	    ArrayNodeList prettyNodes= new ArrayNodeList();
	    for (Node n : nodes) {
	        String prettySource = XMLUtils.prettyPrint(n);
	        Document prettyDoc = XMLUtils.parse(prettySource);
	        prettyNodes.add(prettyDoc.getDocumentElement());
	    }
	    return prettyNodes;
	}

	/**
	 * Remove WS-Addressing headers from the message. This ensures we do not
	 * take irrelevant details into account, such as the unique message ID or
	 * the exact engine it came from (when running several engines in parallel).
	 * Returns the original node, after removing the WS-Addressing headers. Also
	 * removes whitespace before and after the headers.
	 */
	private static Node removeWSAHeaders(final Node msg) throws XPathExpressionException {
		return removeByXPath(WSA_HEADERS_XPATH, msg);
	}

	private static Node removeByXPath(String xpathNodesToBeRemoved,
			final Node msg) throws XPathExpressionException {
		final NodeList nodesToBeRemoved = 
			(NodeList)XMLUtils.evaluate(
					BPEL_NS_CONTEXT,
					xpathNodesToBeRemoved, msg, null,
				XPathConstants.NODESET);
		for (int i = 0; i < nodesToBeRemoved.getLength(); ++i) {
			final Node wsaHeader = nodesToBeRemoved.item(i);
			removeIfWhitespace(wsaHeader.getPreviousSibling());
			removeIfWhitespace(wsaHeader.getNextSibling());
			wsaHeader.getParentNode().removeChild(wsaHeader);
		}
		return msg;
	}

	private static void removeIfWhitespace(final Node prev) {
		if (prev != null && prev.getTextContent().trim().isEmpty()) {
			prev.getParentNode().removeChild(prev);
		}
	}
}
