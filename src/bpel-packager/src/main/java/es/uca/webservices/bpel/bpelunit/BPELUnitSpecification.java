package es.uca.webservices.bpel.bpelunit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import net.bpelunit.framework.exception.ConfigurationException;
import net.bpelunit.framework.exception.DeploymentException;
import net.bpelunit.framework.exception.SpecificationException;
import net.bpelunit.framework.model.test.ITestResultListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.BPELProcessNamespaceContext;
import es.uca.webservices.bpel.activebpel.BPELPackagingException;
import es.uca.webservices.bpel.activebpel.DeploymentArchivePackager;
import es.uca.webservices.bpel.bpelunit.AbortOnFirstDifferenceListener.AbortOnDiff;
import es.uca.webservices.bpel.io.util.FileUtils;
import es.uca.webservices.bpel.ode.ODEDeploymentArchivePackager;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.bpel.xml.XMLDocument;

/**
 * Class that handles BPELUnit test specification files (BPTS). It can also run
 * them and customize some of the deployer options, when using ActiveBPEL.
 * 
 * @author Antonio García Domínguez
 */
public class BPELUnitSpecification extends XMLDocument {

	private static final Logger LOGGER = LoggerFactory.getLogger(BPELUnitSpecification.class);

    public static final String BPELUNIT_SPEC_NAMESPACE = "http://www.bpelunit.org/schema/testSuite";
    public static final String BPELUNIT_SPEC_NS_PREFIX = "tes";

    private File fBPRDirectory = new File("/tmp/activebpel");
	private int fEnginePort = CustomizedRunner.DEFAULT_ENGINE_PORT;
	private int fBPELUnitPort = CustomizedRunner.DEFAULT_BPELUNIT_PORT;
	private int fTestTimeout = CustomizedRunner.DEFAULT_TIMEOUT;
	private boolean fPreserveURLs = false, fAbortAfterClientTrack = false;
	private String fIgnoreXPath;
	
	private List<ITestResultListener> fListeners = new ArrayList<ITestResultListener>();
	private String fBPELUsername, fBPELPassword;
	private String loggingLevelName;

    public String getLoggingLevelName() {
		return loggingLevelName;
	}

	public void setLoggingLevelName(String loggingLevelName) {
		this.loggingLevelName = loggingLevelName;
	}

	/**
     * Constructor from a Document object
     * 
     * @param d
     *            The document object
     */
    public BPELUnitSpecification(Document d) {
        super(d);
        setNamespaceContext(new BPELProcessNamespaceContext());
    }

    /**
     * Constructor from a File
     * 
     * @param f
     *            The source file
     */
    public BPELUnitSpecification(File f) throws ParserConfigurationException,
            SAXException, IOException {
        super(f);
        setNamespaceContext(new BPELProcessNamespaceContext());
    }

    /**
     * Returns the type of deployer to be used ('fixed' for fixed deployment,
     * 'ode' for Apache ODE and 'activebpel' for ActiveBPEL).
     */
    public String getDeployerType() throws XPathExpressionException {
    	return evaluateExpression("tes:testSuite/tes:deployment/tes:put/@type");
    }
    
    /**
     * Returns the path to the BPR
     * 
	 * @return A string with the path of the BPR file set in the BPTS or
	 *         <code>null</code> if none has been set.
     */
	public String getBPRFileOption() throws XPathExpressionException, IOException {
		return evaluateExpression("tes:testSuite/tes:deployment/tes:put/tes:property[@name='BPRFile']/text()");
    }

	/**
	 * Parse the .bpts file and return the number of test cases in this test suite.
	 */
	public int getTestCaseCount() throws ConfigurationException, SpecificationException {
		return new XMLDumperRunner(getFile()).getTestCaseCount();
	}

	/**
	 * Parse the .bpts file and return the names of the test cases in this test suite.
	 */
	public List<String> getTestCaseNames() throws ConfigurationException, SpecificationException {
		return new XMLDumperRunner(getFile()).getTestCaseNames();
	}

    /**
	 * Convenience version of {@link #run(String)} that generates the BPR from the BPEL document automatically.
     * @throws XPathExpressionException 
     * @see #run(String)
	 */
    public ExecutionResults run(BPELProcessDefinition def) throws ConfigurationException,
            SpecificationException, DeploymentException, BPELPackagingException,
            IOException, XPathExpressionException
    {
    	LOGGER.info("Running all tests for " + def);
        final String pathToBPR = generateDeploymentArchive(def);
        final ExecutionResults results = run(pathToBPR);
        return results;
    }

    /**
     * Run the full test suite.
     *
     * @param archiveLocation
     *            Path to the ActiveBPEL deplyoment archive to be run.
     * @return An {@link ExecutionResults} object with the results obtained and
     *         times measured from running all test cases.
     * @throws ConfigurationException
     *             There are errors in the BPELUnit configuration
     * @throws SpecificationException
     *             There are errors in the test suite specification
     * @throws DeploymentException
     *             An error occurred during the deployment
     * @throws BPELPackagingException
     *             An error occurred during the generation of the BPR file
     * @throws IOException
     *             An Input/Output error occurred
     * @throws XPathExpressionException 
     */
    public ExecutionResults run(String archiveLocation) throws ConfigurationException, SpecificationException, DeploymentException, IOException, XPathExpressionException {
    	LOGGER.info("Running all tests in {} for the BPEL archive at {}", getFile(), archiveLocation);

    	XMLDumperRunner runnerOriginal;
    	runnerOriginal = new XMLDumperRunner(getFile());
        if ("ode".equals(getDeployerType())){   
        	runnerOriginal.setBPELUnitPort(getBPELUnitPort());
        	configureRunner(new File(archiveLocation).getAbsolutePath(), runnerOriginal);
        }else{
        	configureRunner(archiveLocation, runnerOriginal);
        }

        final File results = runnerOriginal.run();
		return new ExecutionResults(
        		runnerOriginal.getTestCaseCount(),
        		results,
                       runnerOriginal.getTestWallNanos(),
                       runnerOriginal.getLogsDirectory()
               );
    }

    /**
	 * Convenience version of {@link #run(AbortOnDiff, String, BPELUnitXMLResult)}
	 * that automatically generates the BPR file.
	 */
    public Pair<ExecutionResults, Set<Integer>> run(final AbortOnDiff abortType, final BPELProcessDefinition def, final BPELUnitXMLResult originalResults) throws Exception {
    	if (LOGGER.isInfoEnabled()) {
    		LOGGER.info("Running the tests in " + getFile() + " for " + def + " using the abort type " + abortType + " while comparing with the original results from " + originalResults);
    	}
        final String pathToBPR = generateDeploymentArchive(def);
        final Pair<ExecutionResults, Set<Integer>> results = run(abortType, pathToBPR, originalResults);

        // Delete the automatically generated .bpr file from disk (it's always generated within a temporary directory)
        if (pathToBPR != null) {
        	org.apache.commons.io.FileUtils.deleteDirectory(new File(pathToBPR).getParentFile());
        }
        return results;
	}

    /**
	 * Run the test suite, storing the zero-based positions of the first or all
	 * the test cases which produce a different output.
	 * 
	 * @param abortType
	 *            If {@link AbortOnDiff#SUITE}, only the position of the first
	 *            test case will be stored, and the suite will be aborted on the
	 *            first difference. If {@link AbortOnDiff#TEST}, only the test
	 *            will be aborted, so the positions of all the test cases with
	 *            differing outputs will be reported.
	 * @param archiveLoc
	 *            Path to the ActiveBPEL deployment archive to be run.
	 * @param originalResults
	 *            BPELUnit XML report from running the original composition.
	 * @return Pair with the {@link ExecutionResults} from running up to the
	 *         first test case with a different test result on the left, and the
	 *         value returned by {@link MutantComparisonRunner#run(AbortOnDiff)}
	 *         on the right.
	 * @throws Exception
	 *             There was a problem while deploying the WS-BPEL composition,
	 *             running the WS-BPEL composition or performing the comparison.
	 */
    public Pair<ExecutionResults, Set<Integer>> run(final AbortOnDiff abortType, final String archiveLoc, final BPELUnitXMLResult originalResults) throws Exception {
    	LOGGER.info("Running the tests for the BPEL archive at " + archiveLoc + " for the abort type " + abortType + " with the original results on " + originalResults);

    	MutantComparisonRunner runner = new MutantComparisonRunner(getFile(), originalResults);
        configureRunner(archiveLoc, runner);
        final Set<Integer> firstDifference = runner.run(abortType);
        return new Pair<ExecutionResults, Set<Integer>>(
                new ExecutionResults(
                        runner.getTestCaseCount(),
                        null,
                        runner.getTestWallNanos(),
                        runner.getLogsDirectory()
                ),
                firstDifference);
    }

    private void configureRunner(final String archiveLoc, CustomizedRunner runner) throws ConfigurationException
	{
		runner.setDeploymentDirectory(getDeploymentDirectory());
		runner.setEnginePort(getEnginePort());
		runner.setBPELUnitPort(getBPELUnitPort());
		runner.setArchiveLocation(archiveLoc);
		runner.setAbortAfterClientTrack(isAbortAfterClientTrack());
		runner.setTestTimeout(getTestTimeout());
		runner.setBPELCredentials(getBPELUsername(), getBPELPassword());
		runner.setIgnoreXPath(getIgnoreXPath());
		runner.setLoggingFilterName(getLoggingLevelName());
		for (ITestResultListener l : getListeners()) {
			runner.testSuite.addResultListener(l);
		}
	}

	/**
	 * Returns the File object for the .bpr deployment directory.
	 */
	public File getDeploymentDirectory() {
		return fBPRDirectory;
	}

	/**
	 * Sets the deployment directory with the .bpr files.
	 */
	public void setDeploymentDirectory(File bprDirectory) {
		fBPRDirectory = bprDirectory;
	}

	/**
	 * Returns the port on which ActiveBPEL is listening.
	 */
	public int getEnginePort() {
		return fEnginePort;
	}

	/**
	 * Returns the port on which BPELUnit is listening.
	 */
	public int getBPELUnitPort() {
		return fBPELUnitPort;
	}

	/**
	 * Sets the port on which ActiveBPEL is supposed to be listening.
	 */
	public void setEnginePort(int port) {
		fEnginePort = port;
	}

	/**
	 * Sets the port on which BPELUnit is supposed to be listening.
	 */
	public void setBPELUnitPort(int port) {
		fBPELUnitPort = port;
	}

	/**
	 * Returns <code>true</code> if we should call the services using the same
	 * URLs as in the original WSDL documents, or <code>false</code> if we
	 * should always invoke the BPELUnit mockups (the default behaviour).
	 */
	public boolean isPreserveURLs() {
		return fPreserveURLs;
	}

	/**
	 * If set to <code>true</code>, we should call the services using the same
	 * URLs as in the original WSDL documents. Otherwise, we should always invoke
	 * the BPELUnit mockups (the default behaviour).
	 */
	public void setPreserveURLs(boolean fPreserveURLs) {
		this.fPreserveURLs = fPreserveURLs;
	}

	/**
	 * Returns <code>true</code> if tests should abort pending activities once
	 * all activities in the client track have completed their execution.
	 * Otherwise, returns <code>false</code>. By default, tests do not abort
	 * pending activities.
	 */
	public boolean isAbortAfterClientTrack() {
		return fAbortAfterClientTrack;
	}

	/**
	 * If set to <code>true</code>, tests will abort pending activities once all
	 * activities in the client track have completed their execution. Otherwise,
	 * they will not. By default, tests do not abort pending activities.
	 */
	public void setAbortAfterClientTrack(boolean shouldAbort) {
		this.fAbortAfterClientTrack = shouldAbort;
	}

	/**
	 * Returns the number of milliseconds that BPELUnit will wait for each test case before aborting it.
	 */
	public int getTestTimeout() {
		return fTestTimeout;
	}

	/**
	 * Changes the number of milliseconds that BPELUnit will wait for each test case before aborting it.
	 */
	public void setTestTimeout(int timeout) {
		this.fTestTimeout = timeout;
	}

	/**
	 * Returns the list of the custom test listeners that will be invoked when
	 * running the tests, in addition to the default ones.
	 */
	public List<ITestResultListener> getListeners() {
		return fListeners;
	}

	/**
	 * Changes the list of the custom test listeners that will be invoked when
	 * running the tests, in addition to the default ones.
	 */
	public void setListeners(List<ITestResultListener> listeners) {
		this.fListeners = listeners;
	}

	/**
	 * Returns the username that will be used to authenticate against the BPEL
	 * engine, or <code>null</code> if authentication will not be used (the
	 * default).
	 * 
	 * @see CustomizedRunner#getBPELCredentials()
	 */
	public String getBPELUsername() {
		return fBPELUsername;
	}

	/**
	 * Changes the username that will be used to authenticate against the BPEL
	 * engine. If <code>null</code>, authentication will not be used (the
	 * default).
	 */
	public void setBPELUsername(String fBPELUsername) {
		this.fBPELUsername = fBPELUsername;
	}

	/**
	 * Returns the password that will be used to authenticate against the BPEL
	 * engine, or <code>null</code> if authentication will not be used (the
	 * default).
	 * 
	 * @see CustomizedRunner#getBPELCredentials()
	 */
	public String getBPELPassword() {
		return fBPELPassword;
	}

	/**
	 * Changes the password that will be used to authenticate against the BPEL
	 * engine. If <code>null</code>, authentication will not be used (the
	 * default).
	 * 
	 * @see CustomizedRunner#setBPELCredentials(String, String)
	 */
	public void setBPELPassword(String fBPELPassword) {
		this.fBPELPassword = fBPELPassword;
	}

	/**
	 * Returns the XPath expression that will be used to remove the nodes
	 * that should be ignored during the comparisons between the outputs
	 * of the mutant and the original composition. Returns <code>null</code>
	 * if no nodes will be ignored.
	 */
	public String getIgnoreXPath() {
		return fIgnoreXPath;
	}

	private String generateDeploymentArchive(final BPELProcessDefinition def) throws BPELPackagingException, IOException {
		String deployerType;
		try {
			deployerType = getDeployerType();
		} catch (XPathExpressionException e) {
			throw new BPELPackagingException("Could not obtain the deployer type", e);
		}

		if ("activebpel".equals(deployerType)) {
			return generateActiveBPELArchive(def);
		} else if ("ode".equals(deployerType)) {
			return generateODEArchive(def);
		} else {
			LOGGER.warn("PUT type '" + deployerType + "' is unknown: cannot generate archive");
			return null;
		}
	}

	/**
	 * Changes the XPath expression that will be used to remove the nodes
	 * that should be ignored during the comparisons between the outputs
	 * of the mutant and the original composition. If <code>null</code>
	 * is passed, this behavior will be disabled.
	 */
	public void setIgnoreXPath(String ignoreXPath) {
		this.fIgnoreXPath = ignoreXPath;
	}

	private String generateActiveBPELArchive(final BPELProcessDefinition def) throws BPELPackagingException {
		try {
			if (!"activebpel".equals(getDeployerType())) {
				LOGGER.warn("PUT type '" + getDeployerType() + "' is not for ActiveBPEL: will not generate a BPR file");
				return null;
			}
			
			String pathToBPR = getBPRFileOption();
			if (pathToBPR != null && !"".equals(pathToBPR)) {

				// We're only interested in the basename, not on its full path:
				// we'll generate the actual .bpr in a temporary directory, so
				// we can run several instrumenters at the same time
				File fBPROption = new File(pathToBPR);
				File tempDir = FileUtils.createTemporaryDirectory("instrumenter", ".dir");
				pathToBPR = new File(tempDir, fBPROption.getName()).getCanonicalPath();
			}

			DeploymentArchivePackager packager = new DeploymentArchivePackager(def);
			packager.setBPELUnitPort(getBPELUnitPort());
			packager.setPreserveServiceURLs(isPreserveURLs());
			packager.generateBPR(pathToBPR);
			return pathToBPR;
		} catch (XPathExpressionException e) {
			throw new BPELPackagingException("Could not read path to the BPR file:" + e.getLocalizedMessage(), e);
		} catch (Exception e) {
			throw new BPELPackagingException(e);
		}
	}

	private String generateODEArchive(final BPELProcessDefinition def) throws BPELPackagingException {
		final ODEDeploymentArchivePackager packager = new ODEDeploymentArchivePackager(def);
		packager.setBPELUnitPort(getBPELUnitPort());
		packager.setODEPort(getEnginePort());
		packager.setPreserveServiceURLs(isPreserveURLs());

		try {
			final File tempDir = FileUtils.createTemporaryDirectory("instrumenter", ".dir");
			final File pathToZIP = new File(tempDir, def.getFile().getName() + ".zip");
			packager.packODE(pathToZIP);
			return pathToZIP.getCanonicalPath();
		} catch (Exception e) {
			throw new BPELPackagingException(e);
		}
	}

}
