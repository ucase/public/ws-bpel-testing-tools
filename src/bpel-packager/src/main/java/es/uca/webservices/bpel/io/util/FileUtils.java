package es.uca.webservices.bpel.io.util;

import java.io.File;
import java.io.IOException;

/**
 * Utility methods for handling files and directories.
 * 
 * @author Antonio García-Domínguez
 */
public final class FileUtils {

	private FileUtils() {}

	/**
	 * Creates a new temporary directory inside the directory pointed to by the
	 * java.io.tmpdir property. This directory has an unique filename, which
	 * begins and ends by a prefix and suffix set by the caller, respectively.
	 * 
	 * @param prefix
	 *            Prefix to be added at the beginning of the name of the
	 *            directory.
	 * @param suffix
	 *            Suffix to be added at the end of the name of the directory.
	 * @return File pointing to the newly created directory.
	 */
	public static File createTemporaryDirectory(String prefix, String suffix)
			throws IOException {
		File tmp = File.createTempFile(prefix, suffix);
		if (!tmp.delete()) {
			throw new IOException("Could not delete temporary file: "
					+ tmp.getCanonicalPath());
		}
		if (!tmp.mkdir()) {
			throw new IOException("Could not create temporary dir: "
					+ tmp.getCanonicalPath());
		}
		return tmp;
	}

}
