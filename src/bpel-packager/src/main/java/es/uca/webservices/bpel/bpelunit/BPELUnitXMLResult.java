package es.uca.webservices.bpel.bpelunit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import net.bpelunit.framework.model.test.PartnerTrack;
import net.bpelunit.framework.model.test.TestCase;
import net.bpelunit.framework.model.test.TestSuite;
import net.bpelunit.framework.model.test.activity.Activity;
import net.bpelunit.framework.model.test.report.ITestArtefact;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.BPELProcessNamespaceContext;
import es.uca.webservices.bpel.util.ArrayNodeList;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.bpel.xml.XMLDocument;

/**
 * This class handles the XML results produced by BPELUnit
 * 
 * @author Antonio García Domínguez
 * @version 1.1
 */
public class BPELUnitXMLResult extends XMLDocument {

    /**
	 * <p>Returns an {@link Iterable} object which traverses the entries in
	 * <code>a</code> and <code>b</code> in parallel. The iterator will stop
	 * when all entries on <code>a</code> or <code>b</code> have been traversed.
	 * This is especially useful for loops:</p>
	 * <code>
	 * for (Pair<A, B> pairs : zip(listOfA, listOfB)) {
	 *   final A a = pairs.getLeft();
	 *   final B b = pairs.getRight();
	 *
	 *   // do something...
	 * }
	 * </code>
	 */
	private static <A, B> Iterable<Pair<A, B>> zip(final Iterable<A> a, final Iterable<B> b) {
		return new Iterable<Pair<A, B>>() {
			@Override
			public Iterator<Pair<A, B>> iterator() {
				final Iterator<A> itA = a.iterator();
				final Iterator<B> itB = b.iterator();
	
				return new Iterator<Pair<A, B>>() {
					@Override
					public boolean hasNext() {
						return itA.hasNext() && itB.hasNext();
					}
	
					@Override
					public Pair<A, B> next() {
						return new Pair<A, B>(itA.next(), itB.next());
					}
	
					@Override
					public void remove() {
						throw new UnsupportedOperationException();
					}
				};
			}
		};
	}

	public static final String BPELUNIT_RESULT_NAMESPACE = "http://www.bpelunit.org/schema/testResult";
    public static final String BPELUNIT_RESULT_NS_PREFIX = "res";

    /**
     * Constructor from a Document object
     * 
     * @param d
     *            The Document object
     */
    public BPELUnitXMLResult(Document d) {
        super(d);
        setNamespaceContext(new BPELProcessNamespaceContext());
    }

    /**
     * Constructor from a File object
     * 
     * @param f
     *            The File object
     * @throws ParserConfigurationException
     *             The parser raised an exception
     * @throws SAXException
     *             An error occurred while parsing
     * @throws IOException
     *             An error occurred while reading the file
     */
    public BPELUnitXMLResult(File f) throws ParserConfigurationException,
            SAXException, IOException {
        super(f);
        setNamespaceContext(new BPELProcessNamespaceContext());
    }

    /**
     * Obtains the global status code for all test cases.
     * 
     * @return Global status code for all of the test cases, as listed in the
     *         BPELUnit StatusCode enumeration.
     * @throws XPathExpressionException
     * @see net.bpelunit.framework.model.test.report.ArtefactStatus.StatusCode
     */
    public String getGlobalStatusCode() throws XPathExpressionException {
        return evaluateExpression("/res:testResult/res:state[@name='Status Code']");
    }

    /**
     * Returns the outbound messages from the WS-BPEL composition.
     * 
     * @return List of node lists with outbound messages from the WS-BPEL
     *         composition in each test case. Each node list contains the SOAP
     *         message received from the WS-BPEL composition and all requests
     *         done by the WS-BPEL composition to the mockups of the external
     *         services, in document order. The soap:Envelope element is a
     *         direct child of each of these nodes.
     * 
     * @throws XPathExpressionException
     *             There was an error while evaluating the XPath expression
     *             included in this function. This should not happen.
     */
    public List<NodeList> getCompositionOutboundMessages()
            throws XPathExpressionException {

        final ArrayList<NodeList> results = new ArrayList<NodeList>();
        final NodeList testCaseNodes = (NodeList) evaluateExpression(
                "res:testResult/res:testCase", XPathConstants.NODESET);

        for (int i = 0; i < testCaseNodes.getLength(); ++i) {
            final Node testCaseNode = testCaseNodes.item(i);
            final NodeList receivedMessages = (NodeList) evaluateExpression(
                    testCaseNode,
                    "res:partnerTrack//res:dataPackage[@name='Receive Data Package' or @name='Complete Task Specification']/"
                            + "res:xmlData[@name='SOAP Message data' or @name='Input XML Data']",
                    XPathConstants.NODESET);

            results.add(receivedMessages);
        }

        return results;
    }

    /**
     * Compares two BPELUnit XML result documents and checks whether all SOAP
     * messages produced by the WS-BPEL composition in each test case are the
     * same. Equivalence is defined as an exact node-per-node content-based
     * match between both trees, including whitespace. No semantic information
     * is considered here, for the sake of simplicity.
     * 
     * @param otherResult
     *            BPELUnit XML result document to be compared with.
     * @return True if the results are all the same, false otherwise.
     * @throws XPathExpressionException
     *             This would be a bug: please report it if you experience it.
     * @throws IncompatibleResultsException
     *             Each XML result file has a different number of test cases,
     *             and therefore probably come from different test cases. As
     *             they are not comparable, neither true nor false can be
     *             properly returned.
     * @throws MissingResultsException
     *             No results have been reported on either side: mathematically
     *             speaking it wouldn't be a problem for comparison, but it
     *             doesn't make sense in this case, so we report it as an error.
     */
    public boolean hasSameResultsAs(BPELUnitXMLResult otherResult, String ignoreXPath)
            throws XPathExpressionException, IncompatibleResultsException,
            MissingResultsException {

        Boolean[] results = compareResultsTo(otherResult, ignoreXPath);
        for (boolean b : results) {
            if (!b) {
                return false;
            }
        }
        return true;
    }

    /**
     * Compares this XML result with another and reports what test cases show
     * different outbound SOAP messages from the WS-BPEL composition in each.
     * These outbound messages include the reply from the WS-BPEL composition to
     * the request which created the instance, and all requests sent from the
     * composition to the external services.
     * 
     * @param otherResult
     *            BPELUnit XML result document to be compared with.
     * @param ignoreXPath
     *            If not <code>null</code>, XPath expression of the nodes
     *            that should be removed before comparison on each side.
     *            These nodes will be ignored for the sake of comparison.
     * @return Array of Booleans indicating for each test case whether they
     *         produced the same result (true) or not (false).
     * @throws XPathExpressionException
     * @throws IncompatibleResultsException
     * @throws MissingResultsException
     */
    public Boolean[] compareResultsTo(BPELUnitXMLResult otherResult, String ignoreXPath)
            throws XPathExpressionException, IncompatibleResultsException,
            MissingResultsException {

        List<NodeList> ownResults = getCompositionOutboundMessages();
        List<NodeList> otherResults = otherResult.getCompositionOutboundMessages();

        if (ownResults.size() != otherResults.size()) {
            throw new IncompatibleResultsException(String.format(
                    "Results report a different number of test cases "
                            + " and are thus incomparable: %d versus %d",
                    ownResults.size(), otherResults.size()));
        } else if (ownResults.size() == 0) {
            throw new MissingResultsException(
                    "No test cases have been reported: cannot compare");
        }

        Boolean[] results = new Boolean[ownResults.size()];

        for (int iTestCase = 0; iTestCase < ownResults.size(); ++iTestCase) {
            NodeList ownMessages = ownResults.get(iTestCase);
            NodeList otherMessages = otherResults.get(iTestCase);

            results[iTestCase] = BPELUnitUtils.compareTestCases(iTestCase, ownMessages, otherMessages, ignoreXPath);
        }

        return results;
    }

	public Map<ITestArtefact, ArrayNodeList> matchMessagesWithTestSuite(final TestSuite testSuite) throws IncompatibleResultsException {
		try {
			final ArrayNodeList xmlTestCases = evaluateExpressionAsNodeList("//res:testCase");
			final List<ITestArtefact> testCases = testSuite.getChildren();
			if (xmlTestCases.size() != testCases.size()) {
				throw new IncompatibleResultsException(
						String.format(
								"The number of test cases in the loaded suite (%d) and in the original results (%d) do not match",
								testCases.size(), xmlTestCases.size()));
			}

			final Map<ITestArtefact, ArrayNodeList> originalMessages = new IdentityHashMap<ITestArtefact, ArrayNodeList>();
			int iTestCase = 0;
			for (Pair<ITestArtefact, Node> entry : zip(testCases, xmlTestCases)) {
				final TestCase testCase = (TestCase) entry.getLeft();
				final Element xmlTestCase = (Element) entry.getRight();

				final String xmlTestCaseName = xmlTestCase.getAttribute("name");
				if (!testCase.getName().equals(xmlTestCaseName)) {
					throw new IncompatibleResultsException(
							String.format(
									"The name of test case %d in the loaded suite ('%s') and in the original results ('%s') do not match",
									iTestCase, testCase.getName(),
									xmlTestCaseName));
				}

				matchMessagesFromTestCase(originalMessages, testCase, xmlTestCase);
				++iTestCase;
			}
			return originalMessages;
		} catch (IncompatibleResultsException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new IncompatibleResultsException(ex);
		}
	}

	@Override
	public String toString() {
		return "BPELUnitXMLResult [getFile()=" + getFile() + "]";
	}

	private void matchMessagesFromTestCase(final Map<ITestArtefact, ArrayNodeList> originalMessages, final TestCase testCase, final Element xmlTestCase)
		throws Exception
	{
		final ArrayNodeList xmlPartnerTracks = evaluateExpressionAsNodeList(xmlTestCase, "res:partnerTrack");
		final List<PartnerTrack> partnerTracks = testCase.getPartnerTracks();
		if (xmlPartnerTracks.size() != partnerTracks.size()) {
			throw new IncompatibleResultsException(
					String.format(
							"The number of partner tracks in the loaded suite '%s' (%d) and in the original results (%d) do not match",
							testCase.getName(), partnerTracks.size(), xmlPartnerTracks.size()));
		}

		int iPartnerTrack = 0;
		for (Pair<PartnerTrack, Node> ptEntry : zip(partnerTracks, xmlPartnerTracks)) {
			final PartnerTrack partnerTrack = ptEntry.getLeft();
			final Element xmlPartnerTrack = (Element)ptEntry.getRight();

			final String xmlPartnerTrackName = xmlPartnerTrack.getAttribute("name");
			if (!partnerTrack.getName().equals(xmlPartnerTrackName)) {
				throw new IncompatibleResultsException(
						String.format(
								"The name of the partner track %d in the loaded suite '%s' ('%s') and in the original results ('%s') do not match",
								iPartnerTrack, testCase.getName(), partnerTrack.getName(), xmlPartnerTrackName));
			}

			matchMessagesFromPartnerTrack(originalMessages, testCase, partnerTrack, xmlPartnerTrack);
			++iPartnerTrack;
		}
	}

	private void matchMessagesFromPartnerTrack(
			final Map<ITestArtefact, ArrayNodeList> originalMessages,
			final TestCase testCase, final PartnerTrack partnerTrack,
			final Element xmlPartnerTrack)
		throws Exception
	{
		final ArrayNodeList childElements = evaluateExpressionAsNodeList(xmlPartnerTrack, "res:activity");
		final List<ITestArtefact> activities = partnerTrack.getChildren();
		if (activities.size() != childElements.size()) {
			throw new IncompatibleResultsException(
					String.format(
							"The number of activities in partner track '%s' of test case '%s' in the loaded suite (%d) and in the original results (%d) do not match",
							partnerTrack.getName(), testCase.getName(),
							activities.size(), childElements.size()));
		}

		for (Pair<ITestArtefact, Node> acEntry : zip(activities, childElements)) {
			final ITestArtefact activity = acEntry.getLeft();
			final Node xmlActivity = acEntry.getRight();
			assert activity instanceof Activity : "The children of a partner track should all be Activities";

			final ArrayNodeList rawMessages = evaluateExpressionAsNodeList(
				xmlActivity,
				".//res:dataPackage[@name='Receive Data Package' or @name='Complete Task Specification']/res:xmlData[@name='SOAP Message data' or @name='Input XML Data']");

			originalMessages.put(activity, BPELUnitUtils.prettyPrintNodes(rawMessages));
		}
	}

}
