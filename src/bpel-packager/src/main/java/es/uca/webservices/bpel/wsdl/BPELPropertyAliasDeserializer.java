package es.uca.webservices.bpel.wsdl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.wsdl.Definition;
import javax.wsdl.WSDLException;
import javax.wsdl.extensions.ExtensibilityElement;
import javax.wsdl.extensions.ExtensionDeserializer;
import javax.wsdl.extensions.ExtensionRegistry;
import javax.xml.namespace.QName;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import es.uca.webservices.bpel.BPELConstants;
import es.uca.webservices.bpel.util.XMLUtils;

/**
 * WSDL4J deserializer for the WSDL extensibility elements for WS-BPEL 2.0 property aliases.
 *
 * @author Antonio García-Domínguez
 */
class BPELPropertyAliasDeserializer implements ExtensionDeserializer {

	@SuppressWarnings("rawtypes")
	@Override
	public ExtensibilityElement unmarshall(Class parentClass, QName qname,
			Element elem, Definition def, ExtensionRegistry reg)
			throws WSDLException {
		
		final String name = elem.getAttribute("propertyName");
		final String messageType = elem.getAttribute("messageType");
		final String part = elem.getAttribute("part");
		final String type = elem.getAttribute("type");
		final String element = elem.getAttribute("element");
		final NodeList queries = elem.getElementsByTagNameNS(BPELConstants.VARIABLE_PROPERTIES_NAMESPACE, "query");
		final Element query = (Element)queries.item(0);
		String queryText = null;
		if(query != null){
			queryText = query.getTextContent().replaceAll("[ \n\r]", "");;
		}

		final QName nameQName = XMLUtils.stringToQName(name, elem, def.getTargetNamespace());
		final QName messageTypeQName = XMLUtils.stringToQName(messageType, elem, def.getTargetNamespace());
		final QName typeQName = XMLUtils.stringToQName(type, elem, def.getTargetNamespace());
		final QName elementQName = XMLUtils.stringToQName(element, elem, def.getTargetNamespace());

		return new BPELPropertyAlias(nameQName, messageTypeQName, part,
				typeQName, elementQName, queryText, getAllNamespaces(query));
	}
	
	private Map<String, String> getAllNamespaces(Node node){
		HashMap<String, String> result = new HashMap<String, String>();
		if(node == null){
			return result;
		}
		if (node.getParentNode() != null){
			Map<String, String> resultParent = getAllNamespaces(node.getParentNode());
			for(Entry<String, String> entry : resultParent.entrySet()){
				if(!result.containsKey(entry.getKey())){
					result.put(entry.getKey(), entry.getValue());
				}
			}
		}
		NamedNodeMap attributes = node.getAttributes();
		if(attributes != null){
			for (int i=0; i<attributes.getLength(); i++){
				if(attributes.item(i).getNodeName().contains("xmlns:")){
					String nodeName = attributes.item(i).getNodeName();
					String prefix = nodeName.split(":")[1];
					if(!result.containsKey(prefix)){
						result.put(prefix, attributes.item(i).getTextContent());
					}
				}
			}
		}
		return result;
	}

}
