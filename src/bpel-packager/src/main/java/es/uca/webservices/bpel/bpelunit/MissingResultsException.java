package es.uca.webservices.bpel.bpelunit;

/**
 * This exception indicates that they are missing results
 * @author Antonio García Domínguez
 */
public class MissingResultsException extends Exception {

	private static final long serialVersionUID = 6948284726680732624L;

        /**
         * Constructor from a String
         * @param arg0      Exception message
         */
	public MissingResultsException(String arg0) {
		super(arg0);
	}

        /**
         * Constructor from a Throwable object
         * @param arg0  The throwable object that will be nested
         */
	public MissingResultsException(Throwable arg0) {
		super(arg0);
	}

        /**
         * Constructor from a String and a Throwable object
         * @param arg0  The exception message
         * @param arg1  The throwable object that will be nested
         */
	public MissingResultsException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
