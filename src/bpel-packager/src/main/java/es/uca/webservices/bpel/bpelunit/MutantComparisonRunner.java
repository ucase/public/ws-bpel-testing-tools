package es.uca.webservices.bpel.bpelunit;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import net.bpelunit.framework.exception.ConfigurationException;
import net.bpelunit.framework.exception.SpecificationException;

import org.xml.sax.SAXException;

import es.uca.webservices.bpel.bpelunit.AbortOnFirstDifferenceListener.AbortOnDiff;

/**
 * Test runner which runs every test in the test suite until a different output
 * from the original composition's is returned, or no more test cases remain.
 * 
 * @author Antonio García Domínguez
 * @version 1.0
 */
public class MutantComparisonRunner extends CustomizedRunner {
    private BPELUnitXMLResult fOriginalResults;

    /**
     * Creates a new runner.
     * 
     * @param bpts
     *            File with the full BPELUnit test specification.
     * @param originalResults
     *            XML results produced by BPELUnit for the original composition.
     * @throws ParserConfigurationException
     *             There was a problem while configuring the parser for the XML
     *             results of the original composition.
     * @throws SAXException
     *             There was a problem while parsing the XML results of the
     *             original composition using SAX.
     * @throws IOException
     *             The results of the original composition could not be read.
     * @throws ConfigurationException
     *             There was a problem while configuring BPELUnit.
     * @throws SpecificationException
     *             There was a problem while reading the BPTS file.
     */
    public MutantComparisonRunner(File bpts, BPELUnitXMLResult originalResults)
            throws ParserConfigurationException, SAXException, IOException,
            ConfigurationException, SpecificationException {
		super(bpts);
        fOriginalResults = originalResults;
    }

    /**
	 * Runs the test suite and returns the positions of either the first test
	 * case (if <code>abortType</code> is {@link AbortOnDiff#SUITE}) or all
	 * test cases (if <code>abortType</code> is {@link AbortOnDiff#TEST})
	 * which produced a different output.
	 */
    public Set<Integer> run(AbortOnDiff abortType) throws Exception {
        AbortOnFirstDifferenceListener diffListener = new AbortOnFirstDifferenceListener(testSuite, fOriginalResults, getIgnoreXPath());
        diffListener.setAbortType(abortType);

        try {
            customizeDeployer();
            customizeTargetURLs();
            customizeBaseURL();
            testSuite.setUp();
            addListeners();
            addAuthentication();
            testSuite.addResultListener(diffListener);
            testSuite.run();
        } finally {
            testSuite.shutDown();
        }

        if (diffListener.getComparisonException() != null) {
            throw diffListener.getComparisonException();
        }
        return diffListener.getPositionsOfDifferentTestCases();
    }
}
