package es.uca.webservices.bpel.bpelunit;

/**
 * This exception notifies that they are some incompatible results
 * @author Antonio García Domínguez
 */
public class IncompatibleResultsException extends Exception {

	private static final long serialVersionUID = 4172562651130715895L;

        /**
         * Constructor from a String
         * @param arg0  The message string
         */
	public IncompatibleResultsException(String arg0) {
		super(arg0);
	}

        /**
         * Constructor from a Throwable object
         * @param arg0  The object to be nested
         */
	public IncompatibleResultsException(Throwable arg0) {
		super(arg0);
	}

        /**
         * Constructor from a String and a Throwable object
         * @param arg0  The message string
         * @param arg1  The object to be nested
         */
	public IncompatibleResultsException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
