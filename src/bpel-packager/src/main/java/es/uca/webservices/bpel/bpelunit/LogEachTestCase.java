package es.uca.webservices.bpel.bpelunit;

import java.io.File;
import java.io.IOException;

import net.bpelunit.framework.model.test.ITestResultListener;
import net.bpelunit.framework.model.test.TestCase;
import net.bpelunit.framework.model.test.report.ITestArtefact;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogEachTestCase implements ITestResultListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(LogEachTestCase.class);
	private static int countLogs = 0;
	private FileAppender fAppender;
	private String basePath; 
	private org.apache.log4j.Logger odeLogger;
	
	public LogEachTestCase() {
		
	}

	@Override
	public void testCaseStarted(TestCase testCase) {
		LOGGER.info("Initialize test case " + testCase.getName());
		odeLogger = org.apache.log4j.Logger.getLogger("org.apache.ode");
		odeLogger.setLevel(Level.toLevel("all"));
	
		try {
			countLogs = countLogs +1;
			fAppender = new FileAppender(new PatternLayout(
				"%d{yyyy-MM-dd HH:mm:ss} %-40c{1} [%5p] %m%n"), basePath +
				File.separator + countLogs + ".log", false, false, 8192);
		} catch (IOException e) {
			e.printStackTrace();
		}
		odeLogger.addAppender(fAppender);
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	@Override
	public void testCaseEnded(TestCase testCase) {
		odeLogger.removeAllAppenders();
		LOGGER.info("Finalize test case " + testCase.getName());
	}

	@Override
	public void progress(ITestArtefact testArtefact) {
		// do nothing
	}

}