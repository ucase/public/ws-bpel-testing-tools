package es.uca.webservices.bpel;

import javax.xml.namespace.QName;

import org.w3c.dom.Element;

import es.uca.webservices.bpel.util.XMLUtils;

/**
 * A variable in a WS-BPEL 2.0 process definition.
 *
 * @author Antonio García-Domínguez
 */
public class BPELVariable {

	private String name;
	private QName type, element, messageType;

	/**
	 * Initializes this object from a regular bpel:variable DOM element. In
	 * addition to computing the name, it will use the attributes of the
	 * bpel:variable element to set the rest of the fields of this variable.
	 */
	public BPELVariable(Element variableElement) {
		name = variableElement.getAttribute("name");

		if (variableElement.hasAttribute("type")) {
			type = XMLUtils.stringToQName(variableElement.getAttribute("type"), variableElement, null);
		}
		if (variableElement.hasAttribute("element")) {
			element = XMLUtils.stringToQName(variableElement.getAttribute("element"), variableElement, null);
		}
		if (variableElement.hasAttribute("messageType")) {
			messageType = XMLUtils.stringToQName(variableElement.getAttribute("messageType"), variableElement, null);
		}
	}

	/**
	 * Initializes this object from a WS-BPEL 2.0 variable property. In addition
	 * to computing the name, it will use the type and element attributes from
	 * the property, if set.
	 */
	public BPELVariable(String variableName, BPELProperty property) {
		name = variableName + "~" + property.getName().getLocalPart();
		type = property.getType();
		element = property.getElement();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public QName getType() {
		return type;
	}

	public void setType(QName type) {
		this.type = type;
	}

	public QName getElement() {
		return element;
	}

	public void setElement(QName element) {
		this.element = element;
	}

	public QName getMessageType() {
		return messageType;
	}

	public void setMessageType(QName messageType) {
		this.messageType = messageType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((element == null) ? 0 : element.hashCode());
		result = prime * result
				+ ((messageType == null) ? 0 : messageType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BPELVariable other = (BPELVariable) obj;
		if (element == null) {
			if (other.element != null)
				return false;
		} else if (!element.equals(other.element))
			return false;
		if (messageType == null) {
			if (other.messageType != null)
				return false;
		} else if (!messageType.equals(other.messageType))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BPELVariable [name=" + name
				+ ", type=" + type
				+ ", element=" + element
				+ ", messageType=" + messageType
				+ "]";
	}

}
