package es.uca.webservices.bpel;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;

import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;
import es.uca.webservices.bpel.bpelunit.BPELUnitXMLResult;
import es.uca.webservices.bpel.xml.XMLSchemaFile;

/**
 * Namespace context, used in the evaluation of expressions related to BPEL, XML Schema and WSDL files
 * 
 * @author Antonio García Domínguez
 * @version 1.0
 * */
public class BPELProcessNamespaceContext implements NamespaceContext {
	private static final Map<String, String> MAP_PREFIX_NS;
	private static final Map<String, String> MAP_NS_PREFIX;

	static {
		MAP_PREFIX_NS = new HashMap<String,String>();
		MAP_NS_PREFIX = new HashMap<String,String>();
		
		String[][] mapEntries = new String[][] {
				new String[] {BPELConstants.BPEL2_NAMESPACE, BPELConstants.BPEL2_NS_PREFIX},
				new String[] {BPELConstants.WSDL_NAMESPACE, BPELConstants.WSDL_NS_PREFIX},
				new String[] {BPELConstants.SOAP_NAMESPACE, BPELConstants.SOAP_NS_PREFIX},
				new String[] {BPELConstants.SOAPENV_NAMESPACE, BPELConstants.SOAPENV_NS_PREFIX},
				new String[] {BPELConstants.PLINK_NAMESPACE_WSBPEL2, BPELConstants.PLINK_NS_PREFIX_WSBPEL2},
				new String[] {BPELConstants.PLINK_NAMESPACE_BPEL4WS, BPELConstants.PLINK_NS_PREFIX_BPEL4WS},
				new String[] {XMLSchemaFile.XSD_NAMESPACE, XMLSchemaFile.XSD_NS_PREFIX},
				new String[] {BPELConstants.UCA_NAMESPACE, BPELConstants.UCA_NS_PREFIX},
				new String[] {BPELUnitSpecification.BPELUNIT_SPEC_NAMESPACE, BPELUnitSpecification.BPELUNIT_SPEC_NS_PREFIX},
				new String[] {BPELUnitXMLResult.BPELUNIT_RESULT_NAMESPACE, BPELUnitXMLResult.BPELUNIT_RESULT_NS_PREFIX},
				new String[] {BPELConstants.WSA200303_NAMESPACE, BPELConstants.WSA200303_PREFIX},
				new String[] {BPELConstants.WSA200403_NAMESPACE, BPELConstants.WSA200403_PREFIX},
				new String[] {BPELConstants.WSA200408_NAMESPACE, BPELConstants.WSA200408_PREFIX},
				new String[] {BPELConstants.WSA200508_NAMESPACE, BPELConstants.WSA200508_PREFIX},
		};
		
		for (String[] e : mapEntries) {
			MAP_NS_PREFIX.put(e[0], e[1]);
			MAP_PREFIX_NS.put(e[1], e[0]);
		}		
	}

        /**
         * Gets the URI associated with a given prefix
         * @param prefix    The prefix
         * @return  A String with the URI
         */
	public String getNamespaceURI(String prefix) {
		return MAP_PREFIX_NS.get(prefix);
	}

        /**
         * Gets the prefix associated with a given URI
         * @param namespaceURI  The URI of the namespace
         * @return  The associated prefix
         */
	public String getPrefix(String namespaceURI) {
		return MAP_NS_PREFIX.get(namespaceURI);
	}

        /**
         * Gets an interator over the prefixes associated with a given namespace
         * @param namespaceURI  The namespace URI
         * @return  An iterator
         */
	public Iterator<String> getPrefixes(String namespaceURI) {
		return MAP_PREFIX_NS.keySet().iterator();
	}

}
