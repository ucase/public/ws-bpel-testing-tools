package es.uca.webservices.bpel.bpelunit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import net.bpelunit.framework.control.result.XMLResultProducer;
import net.bpelunit.framework.exception.ConfigurationException;
import net.bpelunit.framework.exception.DeploymentException;
import net.bpelunit.framework.exception.SpecificationException;

/**
 * Test runner which produces a BPELUnit XML result file.
 * 
 * @author Antonio García Domínguez
 */
public class XMLDumperRunner extends CustomizedRunner {

	/**
	 * Constructor
	 * 
	 * @param testSpecification
	 *            The file where the test cases are specified
	 * @throws ConfigurationException
	 *             There are errors in the configuration of BPELUnit
	 * @throws SpecificationException
	 *             There are errors in the test specification
	 */
	public XMLDumperRunner(File testSpecification)
			throws ConfigurationException, SpecificationException {
		super(testSpecification);
	}

	/**
	 * Executes the test suite and stores the log in the file passed as a
	 * parameter
	 * 
	 * @param f
	 *            The destination file
	 * @return The file passed as a parameter
	 * @throws DeploymentException
	 *             An error occurred during the deployment
	 * @throws IOException
	 *             The file could not be opened
	 * @throws SpecificationException
	 *             The target URLs could not be customized
	 */
	public File run(File f) throws DeploymentException, IOException,
			SpecificationException {
		try {
			customizeDeployer();
			customizeTargetURLs();
			customizeBaseURL();
			testSuite.setUp();
            addListeners();
            addAuthentication();
			testSuite.run();
		} finally {
			testSuite.shutDown();
		}

		XMLResultProducer.writeXML(new FileOutputStream(f), testSuite);

		return f;
	}

	/**
	 * Executes the test suite and stores the log in a temporary file
	 * 
	 * @return The created temporary file
	 * @throws DeploymentException
	 *             An error occurred during the deployment
	 * @throws IOException
	 *             The temporary file could not be created
	 * @throws SpecificationException
	 *             The target URLs could not be customized
	 */
	public File run() throws DeploymentException, IOException, SpecificationException {
		File f = File.createTempFile("bpelunit", ".out");
		return run(f);
	}
}
