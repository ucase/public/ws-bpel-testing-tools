package es.uca.webservices.bpel;

import javax.xml.namespace.QName;

/**
 * This class is used to store useful information about partnerLinks in BPEL
 */
public class BPELPartnerLink {
	private String name, myRole, partnerRole;
	private QName type;

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            The name of the partnerLink
	 * @param type
	 *            The type of the partnerLink
	 * @param myRole
	 *            The value of the "myRole" attribute
	 * @param partnerRole
	 *            The value of the "partnerRole" attribute
	 */
	public BPELPartnerLink(String name, QName type, String myRole, String partnerRole) {
		this.name = name;
		this.type = type;
		this.myRole = myRole;
		this.partnerRole = partnerRole;
	}

	/**
	 * Gets the name of the partnerLink
	 * 
	 * @return A String with the name of the partnetLink
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the type of the partnerLink
	 * 
	 * @return A QName with the type of the partnerLink
	 */
	public QName getType() {
		return type;
	}

	/**
	 * Gets the value of the myRole attribute
	 * 
	 * @return A String with the value of the myRole attribute
	 */
	public String getMyRole() {
		return myRole;
	}

	/**
	 * Gets the value of the partnerRole attribute
	 * 
	 * @return A String with the value of the parnerRole attribute
	 */
	public String getPartnerRole() {
		return partnerRole;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result	+ ((myRole == null) ? 0 : myRole.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result	+ ((partnerRole == null) ? 0 : partnerRole.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BPELPartnerLink other = (BPELPartnerLink) obj;
		if (myRole == null) {
			if (other.myRole != null)
				return false;
		} else if (!myRole.equals(other.myRole))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (partnerRole == null) {
			if (other.partnerRole != null)
				return false;
		} else if (!partnerRole.equals(other.partnerRole))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PartnerLink [name=" + name + ", myRole=" + myRole
				+ ", partnerRole=" + partnerRole + ", type=" + type + "]";
	}

}