package es.uca.webservices.bpel;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.*;

import javax.wsdl.Binding;
import javax.wsdl.Definition;
import javax.wsdl.Port;
import javax.wsdl.PortType;
import javax.wsdl.Service;
import javax.wsdl.WSDLException;
import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.bpel.util.Predicate;
import es.uca.webservices.bpel.wsdl.BPELPartnerLinkType;
import es.uca.webservices.bpel.wsdl.WSDLCatalog;
import es.uca.webservices.bpel.xml.XMLDocument;
import es.uca.webservices.bpel.xsd.XMLSchemaCatalog;
import es.uca.webservices.bpel.xslt.XSLTDependencyVisitor;
import es.uca.webservices.wsdl.analyzer.SchemaReadingException;
import es.uca.webservices.wsdl.util.WSDL2XSDTreeException;
import es.uca.webservices.xpath.ConversorXMLXPath;
import es.uca.webservices.xpath.parser.ASTExpression;
import es.uca.webservices.xpath.parser.ParseException;
import es.uca.webservices.xpath.parser.XPathParser;

/**
 * Utility class for static analysis of WS-BPEL 2.0 process compositions.
 *
 * @author Antonio García-Domínguez
 */
public class BPELProcessDefinition extends XMLDocument {

	private static final String INSTRUMENT_ATTR_NAME = "instrument";
	private static final Set<String> INSTRUMENTABLE_TAGS = new HashSet<String>();
	private static final Logger LOGGER = LoggerFactory.getLogger(BPELProcessDefinition.class);

	static {
		INSTRUMENTABLE_TAGS.addAll(Arrays.asList("catch", "catchAll",
				"compensationHandler", "terminationHandler", "flow", "if",
				"elseIf", "else", "onMessage", "onAlarm", "scope", "while",
				"repeatUntil", "sequence"));
	}

	private Set<BPELPartnerLink> partnerLinks;
	private Set<File> dependencies;
	private WSDLCatalog wsdlCatalog;
	private XMLSchemaCatalog xmlSchemaCatalog;

	private boolean instrumentSequencesByDefault, instrumentVariablesByDefault;
	private int instrumentSequencesDepthThreshold;

	// Original version of the process definition, before it was mutated and/or
	// instrumented.
	private BPELProcessDefinition original = this;

	/**
	 * Constructor. Parses a file.
	 * 
	 * @param f
	 *            The File to be parsed
	 * @throws ParserConfigurationException
	 *             Indicates a serious configuration error
	 * @throws SAXException
	 *             An error occurred while parsing
	 * @throws IOException
	 *             The file could not be read
	 * @throws XPathExpressionException
	 *             Could not query the XML document to find out if program
	 *             points or variables should be instrumented by default.
	 * @throws InvalidProcessException
	 *             This is not a valid WS-BPEL 2.0 process definition.
	 */
	public BPELProcessDefinition(File f) throws ParserConfigurationException,
			SAXException, IOException, XPathExpressionException,
			InvalidProcessException {
		super(f);
		setNamespaceContext(new BPELProcessNamespaceContext());
		checkProcessValidity();
		initOptionsFromXML();
	}

	/**
	 * Creates a new BPEL process definition parsing a file. This constructor
	 * will use the type catalog and dependencies from the <code>original</code>
	 * process definition.
	 * 
	 * @throws WSDLException
	 *             There was a problem reading one of the WSDL files.
	 * @throws SchemaReadingException
	 *             There was a problem reading one of the XML Schema files.
	 * @throws WSDL2XSDTreeException
	 *             There was a problem extracting the XML Schema from one of the
	 *             WSDL files.
	 */
	public BPELProcessDefinition(File f, BPELProcessDefinition original)
			throws XPathExpressionException, ParserConfigurationException,
			SAXException, IOException, InvalidProcessException,
			WSDL2XSDTreeException, SchemaReadingException, WSDLException {
		this(f);
		this.original = original;
		initOptionsFromBPEL(original);
	}

	/**
	 * Constructor from a Document.
	 * 
	 * @param d
	 *            The document to be parsed
	 * @throws InvalidProcessException
	 *             This is not a valid WS-BPEL 2.0 process definition.
	 * @throws XPathExpressionException
	 *             Could not read the instrumentation options from the XML file.
	 */
	public BPELProcessDefinition(Document d) throws InvalidProcessException, XPathExpressionException {
		super(d);
		setNamespaceContext(new BPELProcessNamespaceContext());
		checkProcessValidity();
		initOptionsFromXML();
	}

	/**
	 * Constructor from a Document. Copies the type catalog and dependencies
	 * from the original process definition.
	 * 
	 * @see #BPELProcessDefinition(File, BPELProcessDefinition)
	 */
	public BPELProcessDefinition(Document d, BPELProcessDefinition original)
			throws InvalidProcessException, XPathExpressionException,
			IOException, WSDL2XSDTreeException, SchemaReadingException,
			WSDLException, ParserConfigurationException, SAXException {
		this(d);
		this.original = original;
		initOptionsFromBPEL(original);
	}

    /**
     * Constructor from an input stream.
     * @param is The input stream from which the BPEL document should be parsed.
     * @throws SAXException
     *             There was a problem parsing the BPEL document.
     * @throws IOException
     *             There was a problem reading the BPEL document.
     * @throws ParserConfigurationException
     *             The XML parser could not be set up.
     * @throws InvalidProcessException
     *             This is not a valid WS-BPEL 2.0 process definition.
     * @throws XPathExpressionException
     *             Could not read the instrumentation options from the XML file.
     */
    public BPELProcessDefinition(InputStream is)
            throws SAXException, IOException, ParserConfigurationException,
            InvalidProcessException, XPathExpressionException {
        super(is);
        setNamespaceContext(new BPELProcessNamespaceContext());
        checkProcessValidity();
        initOptionsFromXML();
    }

    /**
	 * Returns the {@link WSDLCatalog} consisting of the WSDL files imported
	 * by this composition. This is useful to look up WSDL message types,
	 * partner links and WS-BPEL 2.0 variable properties.
	 * 
	 * @return the generated type catalog. The catalog is cached in memory for
	 *         future calls.
	 * 
	 * @throws InvalidProcessException
	 *             The WS-BPEL 2.0 process definition used an unknown import
	 *             type.
	 * @throws IOException
	 *             Could not canonicalize the path to the WS-BPEL file.
	 * @throws WSDLException
	 *             There was a problem reading a WSDL file.
	 * @throws SchemaReadingException
	 *             There was a problem reading an XML Schema file.
	 * @throws WSDL2XSDTreeException
	 *             There was a problem extracting the tree of XML Schema files
	 *             from the WSDL files.
	 */
	public synchronized WSDLCatalog getWSDLCatalog() throws InvalidProcessException, IOException, WSDL2XSDTreeException, SchemaReadingException, WSDLException {
		if (original != this) {
			return original.getWSDLCatalog();
		}
		if (wsdlCatalog != null) {
			return wsdlCatalog;
		}

		LOGGER.debug("Creating the WSDL catalog for the WS-BPEL process definition {}", getFile());
		wsdlCatalog = new WSDLCatalog();

		final Set<File> wsdlImports = getImportsByType().get(BPELConstants.WSDL_NAMESPACE);
		if (wsdlImports != null) {
			for (File importFile : wsdlImports) {
				LOGGER.debug("Adding WSDL document {} to the WSDL catalog", importFile);
				wsdlCatalog.addWSDL(getBaseDirectory(), importFile);
			}
		}

		LOGGER.debug("Created the WSDL catalog for the WS-BPEL process definition {}", getFile());
		return wsdlCatalog;
	}

	/**
	 * Returns the {@link XMLSchemaCatalog} consisting of the XML Schema files
	 * imported from the WS-BPEL composition, whether directly or through a WSDL
	 * file. This is useful to look up global element and type definitions.
	 * 
	 * @throws SchemaReadingException
	 *             Could not read one of the imported XML Schema files or the
	 *             built-in service reference schema.
	 * @throws InvalidProcessException
	 *             The WS-BPEL 2.0 process did not define its imports correctly.
	 * @throws IOException
	 *             There was an I/O error while reading one of the imported WSDL
	 *             or XML Schema files.
	 * @throws WSDLException
	 *             The imported WSDL files are invalid.
	 * @throws WSDL2XSDTreeException
	 *             There was a problem while extracting the XML Schema documents
	 *             embedded in the imported WSDL files.
	 */
	public synchronized XMLSchemaCatalog getXMLSchemaCatalog()
			throws SchemaReadingException, InvalidProcessException, WSDL2XSDTreeException, WSDLException, IOException
	{
		if (original != this) {
			return original.getXMLSchemaCatalog();
		}
		if (xmlSchemaCatalog != null) {
			return xmlSchemaCatalog;
		}

		xmlSchemaCatalog = new XMLSchemaCatalog();

		final Map<String, Set<File>> importsByType = getImportsByType();
		final Set<File> wsdlImports = importsByType.get(BPELConstants.WSDL_NAMESPACE);
		final Set<File> xsdImports = importsByType.get(BPELConstants.XSD_NAMESPACE);
		final File baseDir = getBaseDirectory();

		if (wsdlImports != null) {
			for (File wsdlImport : wsdlImports) {
				xmlSchemaCatalog.addWSDL(baseDir, wsdlImport);
			}
		}
		if (xsdImports != null) {
			for (File xsdImport : xsdImports) {
				xmlSchemaCatalog.addXMLSchema(baseDir, xsdImport);
			}
		}

		return xmlSchemaCatalog;
	}

	/**
	 * Gets the name of the BPEL Process
	 * 
	 * @return A QName with the name of the BPEL Process
	 * @throws XPathExpressionException
	 *             The name could not be retrieved
	 */
	public QName getName() throws XPathExpressionException {
		return stringToQName(evaluateExpression("bpel:process/@name"),
				(Node) evaluateExpression("bpel:process", XPathConstants.NODE));
	}

	/**
	 * Returns all the partner links declared in the WS-BPEL 2.0 process
	 * definition.
	 */
	public synchronized Set<BPELPartnerLink> getPartnerLinks() {
		if (partnerLinks != null) {
			return partnerLinks;
		}

		partnerLinks = new LinkedHashSet<>();
		final NodeList nPartnerLinks = getDocument().getElementsByTagNameNS(
				BPELConstants.BPEL2_NAMESPACE, "partnerLink");
		for (int iPartnerLink = 0; iPartnerLink < nPartnerLinks.getLength(); ++iPartnerLink) {
			final Element ePartnerLink = (Element) nPartnerLinks
					.item(iPartnerLink);
			final QName type = stringToQName(
					ePartnerLink.getAttribute("partnerLinkType"), ePartnerLink);

			final String name = ePartnerLink.getAttribute("name");
			final String myRole = ePartnerLink.getAttribute("myRole");
			final String partnerRole = ePartnerLink.getAttribute("partnerRole");

			BPELPartnerLink partnerLink = new BPELPartnerLink(name, type, myRole, partnerRole);
			partnerLinks.add(partnerLink);
		}

		return partnerLinks;
	}

	/**
	 * Returns an unmodifiable set with all the files required to run this
	 * composition, excluding the <code>.bpel</code> file itself. The files are
	 * returned in canonical form. Results of the first call are cached for
	 * subsequent calls.
	 * 
	 * This function requires parsing the imported dependencies as XML, but it
	 * does not parse the resulting DOM trees as proper WSDL or XML Schema
	 * documents. Instead, it simply runs a few XPath queries on them.
	 *
	 * Dependencies on external XSLT stylesheets through 'bpel:doXslTransform'
	 * in a &lt;from&gt; XPath expression are also detected, so long as the
	 * first argument of the 'bpel:doXslTransform' call is a string literal
	 * of the form "project:/file.xsl". Any other form is not supported.
	 */
	public synchronized Set<File> getDependencies()
			throws XPathExpressionException, ParserConfigurationException,
			SAXException, IOException {
		if (original != this) {
			return original.getDependencies();
		}
		if (dependencies == null) {
			dependencies = new HashSet<File>();
			addDependencies(getFile(), getDocument(), dependencies);
			addXSLTStylesheets(dependencies);
		}
		return Collections.unmodifiableSet(dependencies);
	}

	/**
	 * Returns all the dependencies which belong to the namespace URI
	 * <code>nsURI</code>.
	 * 
	 * @param nsURI
	 *            Namespace URI to which the dependencies should belong.
	 * @return Set of files which belong to the namespace URI <code>nsURI</code>.
	 */
	public Set<File> getDependenciesWithURI(final String nsURI)
			throws XPathExpressionException, ParserConfigurationException,
			SAXException, IOException {
		final Set<File> wsdlDependencies = new HashSet<File>();
		for (File f : getDependencies()) {
			final XMLDocument doc = new XMLDocument(f);
			if (nsURI.equals(doc.getNamespaceURI())) {
				wsdlDependencies.add(f);
			}
		}
		return wsdlDependencies;
	}

	/**
	 * Returns the WSDL files among all the dependencies of this BPEL composition.
	 * @see #getDependenciesWithURI(String)
	 */
	public Set<File> getWSDLDependencies() throws XPathExpressionException,
			ParserConfigurationException, SAXException, IOException {
		return getDependenciesWithURI(BPELConstants.WSDL_NAMESPACE);
	}

	/**
	 * Returns the XML Schema files among all the dependencies of this BPEL composition.
	 * @see #getDependenciesWithURI(String)
	 */
	public Set<File> getXSDDependencies() throws XPathExpressionException,
			ParserConfigurationException, SAXException, IOException {
		return getDependenciesWithURI(BPELConstants.XSD_NAMESPACE);
	}

	/**
	 * Returns the XSLT files among all the dependencies of this BPEL composition.
	 * @see #getDependenciesWithURI(String)
	 */
	public Set<File> getXSLTDependencies() throws XPathExpressionException,
			ParserConfigurationException, SAXException, IOException {
		return getDependenciesWithURI(BPELConstants.XSLT_NAMESPACE);
	}

	/**
	 * If this process definition was produced by instrumenting or mutating
	 * another process definition, returns that process definition. Otherwise,
	 * returns <code>this</code>.
	 */
	public BPELProcessDefinition getOriginal() {
		return original;
	}

	public boolean getInstrumentSequencesByDefault() {
		return instrumentSequencesByDefault;
	}

	public void setInstrumentSequencesByDefault(boolean instrumentSequencesByDefault) {
		this.instrumentSequencesByDefault = instrumentSequencesByDefault;
	}

	public int getInstrumentSequencesDepthThreshold() {
		return instrumentSequencesDepthThreshold;
	}

	public void setInstrumentSequencesDepthThreshold(int instrumentSequencesDepthThreshold) {
		this.instrumentSequencesDepthThreshold = instrumentSequencesDepthThreshold;
	}

	/**
	 * Returns <code>true</code> if the element should be instrumented.
	 */
	public boolean shouldInstrumentElement(Element context)
	{
		final String sInstrument = context.getAttributeNS(BPELConstants.UCA_NAMESPACE, INSTRUMENT_ATTR_NAME);

		if (!INSTRUMENTABLE_TAGS.contains(context.getLocalName())) {
			// This XML element cannot be instrumented: ignore it
			return false;
		}
		else if (!"sequence".equals(context.getLocalName()) && (hasChildWithLocalName(context, "sequence") || hasChildWithLocalName(context, "empty"))) {
			// Non-sequence elements do not have to be specially instrumented if they contain a
			// <sequence> child, and non-sequence elements with <empty> contents can be skipped
			return false;
		}
		else if ("yes".equals(sInstrument)) {
			// Manually marked: use that
			return true;
		}
		else if ("no".equals(sInstrument)) {
			// Manually marked: use that
			return false;
		}
		else if (instrumentSequencesDepthThreshold <= 0) {
			// If no threshold has been set, use the default value
			return instrumentSequencesByDefault;
		}
		else {
			return countLevels(context) <= instrumentSequencesDepthThreshold;
		}
	}

	/**
	 * Returns a list of the sequences in the process definition which can be
	 * instrumented.
	 * 
	 * @throws XPathExpressionException
	 *             Could not query the BPEL process definition to find the
	 *             instrumented program points.
	 */
	public List<Element> getInstrumentableSequences() throws XPathExpressionException {
		return getInstrumentableElements("sequence");
	}

	/**
	 * Returns a list of the elements in the process definition under the
	 * WS-BPEL namespace and a certain local part in their QNames which can be
	 * instrumented (possibly after wrapping it inside a &lt;sequence&gt;). If
	 * <code>tag</code> is <code>*</code>, all instrumentable elements will be
	 * returned.
	 * 
	 * @throws XPathExpressionException
	 *             Could not query the BPEL process definition.
	 */
	public List<Element> getInstrumentableElements(String tag) throws XPathExpressionException {
		final List<Element> elems = new ArrayList<Element>();
		addInstrumentableElements(getDocument().getDocumentElement(), tag, elems);
		return elems;
	}

	public boolean getInstrumentVariablesByDefault() {
		return instrumentVariablesByDefault;
	}

	public void setInstrumentVariablesByDefault(boolean instrumentVariablesByDefault) {
		this.instrumentVariablesByDefault = instrumentVariablesByDefault;
	}

	/**
	 * Returns <code>true</code> if the variable should be instrumented.
	 */
	public boolean shouldInstrumentVariable(Element context) throws XPathExpressionException {
		final String sInstrument = context.getAttributeNS(BPELConstants.UCA_NAMESPACE, INSTRUMENT_ATTR_NAME);
		if ("yes".equals(sInstrument)) {
			return true;
		}
		else if ("no".equals(sInstrument)) {
			return false;
		}
		else {
			return instrumentVariablesByDefault;
		}
	}

	/**
	 * Returns a list of the variables available at the specified node, which
	 * should be one of those returned by {@link #getInstrumentableSequences()}.
	 * 
	 * @param pp
	 *            DOM Element node that represents the program point.
	 * @throws ParserConfigurationException
	 *             There was a problem configuring the XML parser for the DOM
	 *             AST for the XPath expression.
	 * @throws ParseException
	 *             There was a problem parsing the DOM AST for the XPath
	 *             expression.
	 * @throws MissingPropertyException
	 *             Could not find one of the properties used in the WS-BPEL 2.0
	 *             process definition.
	 * @throws WSDLException
	 *             There was a problem while reading one of the WSDL files.
	 * @throws SchemaReadingException
	 *             There was a problem while reading one of the XML Schema
	 *             files.
	 * @throws WSDL2XSDTreeException
	 *             There was a problem while extracting the XML Schema content
	 *             from the WSDL files.
	 * @throws IOException
	 *             There was an I/O error while reading some of the files.
	 * @throws InvalidProcessException
	 *             The process is not a valid WS-BPEL 2.0 process definition.
	 */
	public List<BPELVariable> getProgramPointVariables(Element pp)
			throws XPathExpressionException, ParseException,
			ParserConfigurationException, MissingPropertyException,
			InvalidProcessException, IOException, WSDL2XSDTreeException,
			SchemaReadingException, WSDLException
	{
		final List<BPELVariable> result = new ArrayList<BPELVariable>();
		addProgramPointVariableDeclarations(pp, result);
		addProgramPointPropertyVariables(pp, getWSDLCatalog(), result);
		return result;
	}

	/**
	 * Searches for a service that satisfies a specific role from a partner link
	 * type in some of the WSDL
	 * 
	 * @param pltQName
	 *            Partner link name
	 * @param role
	 *            Role name
	 * @param usedPorts
	 *            Services which have been already used before, and should not
	 *            be used again.
	 * @return A triplet with the service, port and binding that satisfies the
	 *         requirement, or <code>null</code> if there were no coincidences.
	 */
	public Pair<Service, Port> findService(final QName pltQName, final String role, Set<Pair<QName, String>> usedPorts)
			throws InvalidProcessException, IOException, WSDL2XSDTreeException, SchemaReadingException, WSDLException
	{
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Looking for a service for role '%s' in partner link type '%s'", pltQName, role));
		}
	
		final BPELPartnerLinkType selectedPLT = findPartnerLinkType(pltQName);
		if (selectedPLT == null) {
			LOGGER.warn("Could not find the partner link type {}", pltQName);
			// Could not find the PLT
			return null;
		}
	
		final PortType selectedPortType = findRolePortType(selectedPLT, role);
		if (selectedPortType == null) {
			LOGGER.warn("Could not find the role {} within partner link type {}", role, selectedPLT.getName());
			// Could not find the specified role in the PLT
			return null;
		}
		LOGGER.debug("Found port type {}", selectedPortType.getQName());
	
		// Filter bindings to search for those with the right PLTs
		final Set<QName> mapSelectedBindings = findBindingQNamesByPLT(selectedPortType.getQName());
		if (mapSelectedBindings.isEmpty()) {
			LOGGER.warn("Could not find a binding for the port type {}", selectedPortType.getQName());
			// No matching bindings
			return null;
		}
	
		// Pick the first service which has a port with one of the bindings selected above
		return findServiceWithBindingIn(mapSelectedBindings, usedPorts);
	}

	@Override
	public String toString() {
		try {
			return "BPELProcessDefinition [getName()=" + getName() + ", getFile()=" + getFile() + "]";
		} catch (XPathExpressionException e) {
			LOGGER.error("Error getting the name of the process definition", e);
			return "BPELProcessDefinition [getName()=<error>, getFile()=" + getFile() + "]";
		}
	}

	private void addDependencies(File baseFile, Document document,
			Set<File> deps) throws XPathExpressionException,
			ParserConfigurationException, SAXException, IOException {
		final NodeList importedPaths = (NodeList) evaluateExpression(
				document,
				"//bpel:import/@location | //xsd:import/@schemaLocation | //wsdl:import/@location",
				XPathConstants.NODESET);

		for (int iPath = 0; iPath < importedPaths.getLength(); ++iPath) {
			final Attr importAttr = (Attr) importedPaths.item(iPath);
			final String importedPath = importAttr.getValue();

			File importedFile = new File(importedPath);
			if (!importedFile.isAbsolute()) {
				importedFile = new File(baseFile.getParentFile(), importedPath);
			}
			importedFile = importedFile.getCanonicalFile();
			if (!deps.add(importedFile)) {
				// Skip files that have been visited before
				continue;
			}
	
			final Document importedDoc = new XMLDocument(importedFile)
					.getDocument();
			addDependencies(importedFile, importedDoc, deps);
		}
	}

	private void addInstrumentableElements(final Element e, final String tag, final List<Element> elems) {
		if (instrumentSequencesDepthThreshold > 0 && countLevels(e) > instrumentSequencesDepthThreshold) {
			return;
		}

		// NOTE: keep in sync with shouldInstrumentElement(e)
		// We increase the level as we go, to avoid having to call countLevels(...) all the time
		final String elementTag = e.getLocalName();
		if (shouldInstrumentElement(e) && ("*".equals(tag) || elementTag.equals(tag))) {
			elems.add(e);
		}

		// Continue with the children
		final NodeList children = e.getChildNodes();
		for (int i = 0; i < children.getLength(); ++i) {
			final Node n = children.item(i);
			if (!(n instanceof Element)) {
				continue;
			}
			final Element child = (Element) n;

			addInstrumentableElements(child, tag, elems);
		}
	}

	private void addProgramPointPropertyVariables(Element pp,
			final WSDLCatalog catalog, final List<BPELVariable> result)
			throws XPathExpressionException, ParseException,
			ParserConfigurationException, MissingPropertyException {
		final NodeList props = (NodeList)evaluateExpression(pp,
				"bpel:assign[@uca:punto = 'EXIT']/bpel:copy/bpel:from",
				XPathConstants.NODESET);
		for (int iProp = 0; iProp < props.getLength(); ++iProp) {
			final Element element = (Element)props.item(iProp);
			if (shouldInstrumentVariable(element)) {
				addVirtualVariablesForProperties(catalog, element, result);
			}			
		}
	}

	private void addProgramPointVariableDeclarations(Element pp,
			final List<BPELVariable> result) throws XPathExpressionException {
		final NodeList decls = (NodeList)evaluateExpression(pp,
			"ancestor::bpel:process/bpel:variables/bpel:variable " +
			"| ancestor::bpel:scope/bpel:variables/bpel:variable",
			XPathConstants.NODESET);
		for (int iDecl = 0; iDecl < decls.getLength(); ++iDecl) {
			final Element element = (Element)decls.item(iDecl);
			if (shouldInstrumentVariable(element)) {
				result.add(new BPELVariable(element));
			}
		}
	}

	private void addVirtualVariablesForProperties(WSDLCatalog catalog,
			Element bpelElement, List<BPELVariable> result)
			throws ParseException, ParserConfigurationException,
			XPathExpressionException, MissingPropertyException {
		final String xpathExpression = bpelElement.getTextContent().trim();
		if (xpathExpression.isEmpty()) {
			return;
		}

		final Document xpathAST = ConversorXMLXPath.xpath2DOM(xpathExpression);
		final NodeList propertyScopes = (NodeList) evaluateExpression(
				xpathAST.getDocumentElement(),
				"/uca:expression/uca:functionCall[uca:qname/@prefix = 'uca' and uca:qname/@localPart = 'inspeccionarPropiedad']",
				XPathConstants.NODESET);

		for (int iScope = 0; iScope < propertyScopes.getLength(); ++iScope) {
			final Element xpathASTElement = (Element) propertyScopes.item(iScope);
			final NodeList args = xpathASTElement.getElementsByTagNameNS(
					BPELConstants.UCA_NAMESPACE, "stringConstant");

			final String bpelVariableName = args.item(0).getTextContent().trim().replace("'", "");
			final String propertyNS = args.item(1).getTextContent().trim().replace("'", "");
			final String propertyLocalName = args.item(2).getTextContent().trim().replace("'", "");
			final QName propertyQName = new QName(propertyNS, propertyLocalName);

			final BPELProperty property = catalog.getProperty(propertyQName);
			if (property == null) {
				throw new MissingPropertyException(propertyQName);
			}

			final BPELVariable virtualVariable = new BPELVariable(
					bpelVariableName, property);
			result.add(virtualVariable);
		}
	}

	private void addXSLTStylesheets(Set<File> dependencies) {
		final NodeList nl = getDocument().getElementsByTagNameNS(BPELConstants.BPEL2_NAMESPACE, "from");
		for (int i = 0; i < nl.getLength(); ++i) {
			final Element eFrom = (Element)nl.item(i);
			if (!eFrom.hasChildNodes()) continue;
			final String expr = eFrom.getTextContent().trim();
			if (expr.length() == 0 || !expr.contains("doXslTransform")) continue;

			final XPathParser parser = new XPathParser(new StringReader(expr));
			try {
				final ASTExpression exprAST = parser.Start();
				new XSLTDependencyVisitor(getFile().getParentFile(), dependencies).visit(exprAST, null);
			} catch (ParseException e) {
				LOGGER.warn("Could not parse XPath expression '" + expr + "' to find XSLT dependencies", e);
			}
		}
	}

	/**
	 * Checks that the parsed file is a valid BPEL 2.0 document
	 * 
	 * @throws InvalidProcessException
	 *             This is not a valid WS-BPEL 2.0 process definition.
	 */
	private void checkProcessValidity() throws InvalidProcessException {
		if (!BPELConstants.BPEL2_NAMESPACE.equals(getNamespaceURI())) {
			throw new InvalidProcessException(
					"This document is not a WS-BPEL 2.0 process definition: the document element belong to the namespace "
							+ getNamespaceURI());
		}
	}

	private int countLevels(Element context) {
		int nLevels = 0;
		for (Node n = context; n instanceof Element; n = n.getParentNode()) {
			if (INSTRUMENTABLE_TAGS.contains(n.getLocalName())) {
				if ("sequence".equals(n.getLocalName()) || !hasChildWithLocalName(n, "sequence")) {
					++nLevels;
				}
				if ("else".equals(n.getLocalName()) || "elseIf".equals(n.getLocalName())) {
					// Skip over the <if> node, to ensure <if>/<elseIf>/<else> are on the same level.
					n = n.getParentNode();
					if (n == null) {
						// Just in case - avoids a NPE
						break;
					}
				}
			}
		}
		return nLevels;
	}

	private Set<QName> findBindingQNamesByPLT(final QName tipoPuertoBuscado) throws
		InvalidProcessException, IOException, WSDL2XSDTreeException, SchemaReadingException, WSDLException
	{
		Set<Binding> bindings = getWSDLCatalog().findAllBindings(
			new Predicate<Binding>() {
				public boolean evaluate(Binding b) {
					return tipoPuertoBuscado.equals(b.getPortType().getQName());
				}
		});
	
		Set<QName> results = new HashSet<QName>();
		for (Binding b : bindings) {
			results.add(b.getQName());
		}
		return results;
	}

	private BPELPartnerLinkType findPartnerLinkType(final QName tipoPL)	throws
		InvalidProcessException, IOException, WSDL2XSDTreeException,
		SchemaReadingException, WSDLException
	{
		return getWSDLCatalog().findExtensibilityElement(
			BPELPartnerLinkType.class,
			new Predicate<BPELPartnerLinkType>() {
				public boolean evaluate(BPELPartnerLinkType plt) {
					return plt.getName().equals(tipoPL);
				}
			});
	}

	private PortType findRolePortType(BPELPartnerLinkType chosenPLT,
			final String rol) throws InvalidProcessException, IOException,
			WSDL2XSDTreeException, SchemaReadingException, WSDLException
	{
		final QName portTypeName = chosenPLT.getPortTypesByRole().get(rol);
		return getWSDLCatalog().getPortType(portTypeName);
	}

	@SuppressWarnings("unchecked")
	private Pair<Service, Port> findServiceWithBindingIn(Set<QName> mapSelectedBindings, Set<Pair<QName, String>> usedPorts)
			throws InvalidProcessException, IOException, WSDL2XSDTreeException, SchemaReadingException, WSDLException
	{
		for (Definition def : getWSDLCatalog().getDefinitions()) {
			for (Service s : (Collection<Service>)def.getAllServices().values()) {
				final QName serviceQName = s.getQName();
				for (Port p : (Collection<Port>)s.getPorts().values()) {
					final QName bindingQName = p.getBinding().getQName();
					final Pair<QName, String> servicePortKey =
						new Pair<QName, String>(serviceQName, p.getName());
					if (mapSelectedBindings.contains(bindingQName) && usedPorts.add(servicePortKey)) {
						return new Pair<Service, Port>(s, p);
					}
				}
			}
		}
		return null;
	}

	private File getBaseDirectory() {
		final File baseDir = getFile() != null ? getFile().getParentFile() : null;
		return baseDir;
	}

	private Map<String, Set<File>> getImportsByType() throws InvalidProcessException {
		final NodeList imports = getDocument().getElementsByTagNameNS(BPELConstants.BPEL2_NAMESPACE, "import");
		final Map<String, Set<File>> importedFiles = new HashMap<String, Set<File>>();

		for (int iImport = 0; iImport < imports.getLength(); ++iImport) {
			final Element importElem = (Element)imports.item(iImport);
	
			final String importType = importElem.getAttribute("importType");
			final String importLoc  = importElem.getAttribute("location");
			if (importType == null) {
				throw new InvalidProcessException("One of the imports did not have the type attribute set");
			}
			if (importLoc == null) {
				throw new InvalidProcessException("One of the imports did not have the location attribute set");
			}
	
			// NOTE: only local locations are supported - http:// or https:// URLs are not
			final File importFile = new File(importLoc);
			if (!importedFiles.containsKey(importType)) {
				importedFiles.put(importType, new HashSet<File>());
			}
			importedFiles.get(importType).add(importFile);
		}
		return importedFiles;
	}

	private boolean hasChildWithLocalName(final Node n, String localName) {
		final NodeList nl = n.getChildNodes();
		for (int i = 0; i < nl.getLength(); ++i) {
			if (nl.item(i) instanceof Element) {
				final Element child = (Element)nl.item(i);
				final String childTag = child.getLocalName();
				if (localName.equals(childTag)) {
					return true;
				}
			}
		}
		return false;
	}

	private void initOptionsFromXML() throws XPathExpressionException {
		final Boolean xmlInsSequences = (Boolean) evaluateExpression(
				"not(/bpel:process/@uca:instrumentSequencesByDefault = 'no')",
				XPathConstants.BOOLEAN);
		final Boolean xmlInsVariables = (Boolean) evaluateExpression(
				"not(/bpel:process/@uca:instrumentVariablesByDefault = 'no')",
				XPathConstants.BOOLEAN);
		final Attr maxDepthAttr = (Attr) evaluateExpression(
				"/bpel:process/@uca:maxInstrumentationDepth",
				XPathConstants.NODE);

		// By default, instrument everything
		this.instrumentSequencesByDefault = xmlInsSequences != null ? xmlInsSequences : true;
		this.instrumentVariablesByDefault = xmlInsVariables != null ? xmlInsVariables : true;
		this.instrumentSequencesDepthThreshold = maxDepthAttr != null ? Integer.parseInt(maxDepthAttr.getValue()) : 0;
	}

	private void initOptionsFromBPEL(BPELProcessDefinition original) {
		this.instrumentSequencesByDefault = original.instrumentSequencesByDefault;
		this.instrumentSequencesDepthThreshold = original.instrumentSequencesDepthThreshold;
		this.instrumentVariablesByDefault = original.instrumentVariablesByDefault;
	}
}
