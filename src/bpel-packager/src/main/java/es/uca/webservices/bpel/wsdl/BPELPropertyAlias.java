package es.uca.webservices.bpel.wsdl;

import java.util.HashMap;
import java.util.Map;

import javax.wsdl.extensions.ExtensibilityElement;
import javax.xml.namespace.QName;

import es.uca.webservices.bpel.BPELConstants;
import es.uca.webservices.bpel.BPELVariable;

/**
 * <p>
 * WSDL4J extensibility element for a WS-BPEL 2.0 variable property alias. These
 * are defined inside the imported WSDL files.
 * </p>
 * 
 * <p>
 * When accessing the fields of this class, please keep in mind that:
 * </p>
 * 
 * <ul>
 * <li>Every property alias lists the name of the aliased property.
 * (varprop:propertyAlias XML Schema, WS-BPEL 2.0 spec)</li>
 * <li>There may only be one property alias for each combination of
 * type/element/message+part and property. (SA00022, WS-BPEL 2.0 spec)</li>
 * <li>The only valid combinations of attributes are message+part, element and
 * type. After selecting one of these alternatives, the other attributes should
 * be kept <code>null</code>. (SA00020, WS-BPEL 2.0 spec)</li>
 * </ul>
 * 
 * @author Antonio García-Domínguez
 */
public class BPELPropertyAlias implements ExtensibilityElement {

	public static final QName TAG = new QName(
			BPELConstants.VARIABLE_PROPERTIES_NAMESPACE, "propertyAlias");

	private QName propertyName;
	private QName messageType;
	private String part;
	private QName type;
	private QName element;
	private String queryText;
	Map<String, String> namespaces = new HashMap<String, String>();

	/**
	 * Creates a new alias.
	 * 
	 * @throws IllegalArgumentException
	 *             An invalid combination of messageType/part/type/element
	 *             values was used. According to SA00020, only one among
	 *             message+part, type or element should be non-null, and the
	 *             rest should be null.
	 */
	public BPELPropertyAlias(QName name, QName messageType,
			String part, QName type, QName element, String queryText, Map<String, String> namespaces) {
		this.propertyName = name;
		this.messageType = messageType;
		this.part = part;
		this.type = type;
		this.element = element;
		this.queryText = queryText;
		this.namespaces.putAll(namespaces);
		
		boolean valid =
			   messageType != null && part != null && type == null && element == null
			|| messageType == null && part == null && type != null && element == null
			|| messageType == null && part == null && type == null && element != null;
		if (!valid) {
			throw new IllegalArgumentException("Invalid combination of attributes used: should be one of messageType+part, type or element");
		}
	}

	public QName getPropertyName() {
		return propertyName;
	}

	public QName getMessageType() {
		return messageType;
	}

	public String getPart() {
		return part;
	}

	public QName getType() {
		return type;
	}

	public QName getElement() {
		return element;
	}
	
	public String getQueryText(){
		return queryText;
	}
	
	public Map<String, String> getNamespaces(){
		return namespaces;
	}

	@Override
	public QName getElementType() {
		return TAG;
	}

	@Override
	public Boolean getRequired() {
		return false;
	}

	@Override
	public void setElementType(QName arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setRequired(Boolean arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((element == null) ? 0 : element.hashCode());
		result = prime * result + ((messageType == null) ? 0 : messageType.hashCode());
		result = prime * result + ((propertyName == null) ? 0 : propertyName.hashCode());
		result = prime * result + ((part == null) ? 0 : part.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof BPELPropertyAlias)) {
			return false;
		}
		BPELPropertyAlias other = (BPELPropertyAlias) obj;
		if (element == null) {
			if (other.element != null) {
				return false;
			}
		} else if (!element.equals(other.element)) {
			return false;
		}
		if (messageType == null) {
			if (other.messageType != null) {
				return false;
			}
		} else if (!messageType.equals(other.messageType)) {
			return false;
		}
		if (propertyName == null) {
			if (other.propertyName != null) {
				return false;
			}
		} else if (!propertyName.equals(other.propertyName)) {
			return false;
		}
		if (part == null) {
			if (other.part != null) {
				return false;
			}
		} else if (!part.equals(other.part)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		if(queryText == null){
			if(other.type != null){
				return false;
			}
		} else if (!queryText.equals(other.queryText)){
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("BPELPropertyAlias [propertyName=");
		sb.append(propertyName);
		if (messageType != null) {
			sb.append(", messageType=");
			sb.append(messageType);
			sb.append(", part=");
			sb.append(part);
		}
		else if (element != null) {
			sb.append(", element=");
			sb.append(element);
		}
		else if (queryText != null){
			sb.append(", queryText=");
			sb.append(queryText);
		}
		else {
			sb.append(", type=");
			sb.append(type);
		}
		sb.append(']');
		return sb.toString();
	}

	/**
	 * Returns <code>true</code> if this property alias is applicable to the
	 * specified variable, and <code>false</code> if it is not.
	 */
	public boolean isApplicableTo(BPELVariable variable) {
		if (messageType != null) {
			return messageType.equals(variable.getMessageType());
		}
		else if (type != null) {
			return type.equals(variable.getType());
		}
		else {
			return element.equals(variable.getElement());
		}
	}
}
