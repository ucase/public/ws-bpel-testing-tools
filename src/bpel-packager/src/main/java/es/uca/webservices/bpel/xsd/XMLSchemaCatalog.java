package es.uca.webservices.bpel.xsd;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.wsdl.WSDLException;
import javax.xml.namespace.QName;

import org.apache.xmlbeans.SchemaGlobalElement;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.SchemaTypeLoader;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlObject;

import es.uca.webservices.wsdl.analyzer.SchemaReadingException;
import es.uca.webservices.wsdl.util.WSDL2XSDTreeException;
import es.uca.webservices.wsdl.util.WSDLGraphTransformer;

/**
 * Class which encapsulates all the WSDL message types and BPEL types used
 * inside a WS-BPEL document. This class will eventually entirely replace the
 * <code>types.xml</code> file that is now produced using XML Schema.
 * 
 * @author Antonio García-Domínguez
 */
public class XMLSchemaCatalog {

	private SchemaTypeLoader mergedLoader;
	private List<SchemaTypeLoader> xsdAnalyzers = new ArrayList<SchemaTypeLoader>();

	public XMLSchemaCatalog() throws SchemaReadingException {
		// We need the ServiceRef XML Schema for the built-in service reference variable types
		addXMLSchema(XMLSchemaCatalog.class.getResourceAsStream("/serviceref.xsd"));
	}

	/**
	 * Adds an XML Schema file to the type catalog. Dependencies will be
	 * traversed recursively.
	 * 
	 * @param baseDir
	 *            Directory from which relative paths should be resolved. If
	 *            <code>null</code>, relative paths will be resolved from the
	 *            current working directory.
	 * @param xsdFile
	 *            XML Schema file whose types are to be added to the BPEL type
	 *            catalog.
	 */
	public void addXMLSchema(File baseDir, File xsdFile) throws SchemaReadingException {
		try {
			if (!xsdFile.isAbsolute() && baseDir != null) {
				xsdFile = new File(baseDir, xsdFile.getPath());
			}
			SchemaTypeLoader sts = XmlBeans.loadXsd(new XmlObject[] { XmlObject.Factory.parse(xsdFile) });
			addXMLSchema(sts);
		} catch (Exception e) {
			throw new SchemaReadingException(e);
		}
	}

	/**
	 * Adds an XML Schema to the type catalog. Dependencies will be traversed recursively.
	 */
	public void addXMLSchema(InputStream is) throws SchemaReadingException {
		try {
			SchemaTypeLoader sts = XmlBeans.loadXsd( new XmlObject[] { XmlObject.Factory.parse(is) });
			addXMLSchema(sts);
		} catch (Exception e) {
			throw new SchemaReadingException(e);
		}
	}

	/**
	 * Adds a parsed XML Schema file to the type catalog. Dependencies will be
	 * traversed recursively.
	 * 
	 * @param analyzer
	 *            ServiceAnalyzer XSD analyzer produced by parsing an XML Schema
	 *            file.
	 */
	public void addXMLSchema(SchemaTypeLoader analyzer) {
		xsdAnalyzers.add(analyzer);
		mergedLoader = null;
	}

	/**
	 * Adds a WSDL file to the type catalog. This will make both its XML Schema
	 * types and WSDL message types available in the type catalog. Dependencies
	 * will be traversed recursively.
	 *
	 * @param baseDir
	 *            Directory from which relative paths should be resolved. If
	 *            <code>null</code>, relative paths will be resolved from the
	 *            current directory.
	 * @param wsdlFile
	 *            WSDL file to be loaded.
	 */
	public void addWSDL(File baseDir, File wsdlFile)
			throws WSDL2XSDTreeException, SchemaReadingException, WSDLException, IOException
	{
		if (baseDir != null && !wsdlFile.isAbsolute()) {
			wsdlFile = new File(baseDir, wsdlFile.getPath());
		}
		addXMLSchema(createSchemaTypeLoader(wsdlFile));
	}

	/**
	 * Finds a XML Schema type definition by QName.
	 * 
	 * @param propertyQName
	 *            QName of the type to be searched.
	 * @return Returns the matching {@link SchemaType} object if found, or
	 *         <code>null</code> if not found.
	 */
	public SchemaType getType(QName typeQName) {
		return getMergedLoader().findType(typeQName);
	}

	/**
	 * Finds a XML Schema element definition by QName.
	 * 
	 * @param elementQName
	 *            QName of the element to be searched.
	 * @return Returns the matching {@link SchemaGlobalElement} object if found, or
	 *         <code>null</code> if not found.
	 */
	public SchemaGlobalElement getElement(QName elementQName) {
		return getMergedLoader().findElement(elementQName);
	}

	private synchronized SchemaTypeLoader getMergedLoader() {
		if (mergedLoader != null) {
			return mergedLoader;
		}

		// ASSEMBLE!
		mergedLoader = XmlBeans.typeLoaderUnion(
			xsdAnalyzers.toArray(new SchemaTypeLoader[xsdAnalyzers.size()]));
		return mergedLoader;
	}

	private SchemaTypeLoader createSchemaTypeLoader(File file)
			throws WSDL2XSDTreeException, SchemaReadingException, WSDLException
	{
		return loadTypesFromWSDL(new WSDLGraphTransformer(file));
	}

	private SchemaTypeLoader loadTypesFromWSDL(WSDLGraphTransformer t)
			throws WSDL2XSDTreeException, SchemaReadingException
	{
	    final File root = t.generateXSDTree();

		try {
			return XmlBeans.loadXsd(new XmlObject[] { XmlObject.Factory.parse(root) });
		} catch (Exception e) {
			throw new SchemaReadingException(e);
		}
	}
}
