/**
 * 
 */
package es.uca.webservices.bpel.bpelunit;

import java.util.Collections;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import net.bpelunit.framework.model.test.ITestResultListener;
import net.bpelunit.framework.model.test.TestCase;
import net.bpelunit.framework.model.test.TestSuite;
import net.bpelunit.framework.model.test.report.ArtefactStatus;
import net.bpelunit.framework.model.test.report.ArtefactStatus.StatusCode;
import net.bpelunit.framework.model.test.report.ITestArtefact;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.util.ArrayNodeList;

/**
 * Test result listener which aborts the test suite at the first difference
 * between the results of the original composition and the mutant. Optionally,
 * it can be made to abort only the current test case and not the entire test
 * suite.
 * 
 * @author Antonio García Domínguez
 */
public class AbortOnFirstDifferenceListener implements ITestResultListener {

	/**
	 * Operation mode for the current listener.
	 */
	public enum AbortOnDiff {
		/**
		 * The entire test suite should be aborted after finding the first different output.
		 * Only the position of the first test case which produced a different output will be
		 * available.
		 */
		SUITE,

		/**
		 * The current test should be aborted after finding the first different output, but we should continue anyway.
		 * The position of all the test cases that produced a different output will be available.
		 */
		TEST
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(AbortOnFirstDifferenceListener.class); 

	private AbortOnDiff abortType;
    private int fCurrentTestCase = 0;
    private Exception fExceptionDuringComparison;

    // Set of zero-based positions of the test cases which produce different results
    private Set<Integer> fDifferentTestCases = new HashSet<Integer>();

    private Map<ITestArtefact, ArrayNodeList> fOriginalMessages = new IdentityHashMap<ITestArtefact, ArrayNodeList>();

    // XPath expression with the nodes to be ignored
    private String fIgnoreXPath;


    /**
     * Creates a new instance.
     * 
     * @param testSuite BPTS specification, as parsed by BPELUnit.
     * @param xmlResults XML results of the original process definti
     */
    public AbortOnFirstDifferenceListener(final TestSuite testSuite, final BPELUnitXMLResult xmlResults, final String ignoreXPath) throws IncompatibleResultsException
    {
    	this.fOriginalMessages = xmlResults.matchMessagesWithTestSuite(testSuite);
    	this.fIgnoreXPath = ignoreXPath;
    }

    @Override
    public void progress(final ITestArtefact testArtefact) {
    	// If we already detected a difference or ran into an exception, don't do anything else
    	if (fExceptionDuringComparison != null || fDifferentTestCases.contains(fCurrentTestCase)) {
    		return;
    	}

		// Do not do the comparison until the activity completes
    	final ArtefactStatus status = testArtefact.getStatus();
		if (StatusCode.NOTYETSPECIFIED.equals(status) || StatusCode.INPROGRESS.equals(status)) {
			return;
		}

		// Ignore anything that is not an actvity
		final ArrayNodeList originalNodes = fOriginalMessages.get(testArtefact);
    	if (originalNodes == null) {
    		return;
    	}

		final TestCase testCase = locateAncestor(testArtefact, TestCase.class);
		final TestSuite suite = locateAncestor(testCase, TestSuite.class);
    	try {
    		final ArrayNodeList currentNodes = new ArrayNodeList();
			BPELUnitUtils.collectOutgoingSOAPMessageDataNodes(currentNodes, testArtefact);

			// compare the two node lists
			if (!BPELUnitUtils.compareTestCases(fCurrentTestCase, originalNodes, currentNodes, getIgnoreXPath())) {
				LOGGER.info("Detected changed messages in test artefact {} of test case {}", testArtefact, testCase.getName());
				fDifferentTestCases.add(fCurrentTestCase);

				switch (abortType) {
				case TEST:
					testCase.abortTest();
					break;
				default:
					suite.abortTest();
					break;
				}
			}
		} catch (Exception ex) {
			LOGGER.error("There was a problem while comparing the outputs", ex);
			fExceptionDuringComparison = ex;
			suite.abortTest();
		}
    }

    @Override
    public void testCaseEnded(TestCase testCase) {
        ++fCurrentTestCase;
    }

    @Override
    public void testCaseStarted(TestCase testCase) {
    	// nothing to do
    }

    /**
     * 
     */
    public void setAbortType(AbortOnDiff type) {
    	this.abortType = type;
    }

    /**
     * Returns {@link AbortOnDiff#SUITE} if the entire test suite will be aborted after finding the first difference,
     * or {@link AbortOnDiff#TEST} if only the test case should be aborted. 
     * @param type
     * @return
     */
    public AbortOnDiff getAbortType() {
    	return abortType;
    }

    public String getIgnoreXPath() {
		return fIgnoreXPath;
	}

	public void setIgnoreXPath(String fIgnoreXPath) {
		this.fIgnoreXPath = fIgnoreXPath;
	}

	/**
	 * Returns an unmodifiable view of the set of the first test case which
	 * produced a different output (if {@link #getAbortType()} would
	 * return {@link AbortOnDiff#SUITE}) or all the test cases which
	 * produced a different output (if it would return
	 * {@link AbortOnDiff#TEST}). If all tests have produced the same
	 * output, it will return an empty set.
	 */
    public Set<Integer> getPositionsOfDifferentTestCases() {
        return Collections.unmodifiableSet(fDifferentTestCases);
    }

    /**
     * Returns the last exception thrown during the comparison of the outputs
     * of the original and the new composition.
     */
    public Exception getComparisonException() {
        return fExceptionDuringComparison;
    }

	@SuppressWarnings("unchecked")
	private <T extends ITestArtefact> T locateAncestor(ITestArtefact testArtefact, Class<T> klazz) {
		while (testArtefact != null && !(klazz.isInstance(testArtefact))) {
			testArtefact = testArtefact.getParent();
		}
		return (T)testArtefact;
	}

}