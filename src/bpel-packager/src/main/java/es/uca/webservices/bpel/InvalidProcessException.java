package es.uca.webservices.bpel;

/**
 * The process definition is not a valid WS-BPEL 2.0 process definition.
 */
public class InvalidProcessException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidProcessException(String message) {
		super(message);
	}

	public InvalidProcessException(String message, Throwable cause) {
		super(message, cause);
	}

}
