package es.uca.webservices.bpel.xslt;

import java.io.File;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.BPELConstants;
import es.uca.webservices.xpath.parser.ASTAdd;
import es.uca.webservices.xpath.parser.ASTAllDescendants;
import es.uca.webservices.xpath.parser.ASTAnd;
import es.uca.webservices.xpath.parser.ASTAnyNode;
import es.uca.webservices.xpath.parser.ASTAxis;
import es.uca.webservices.xpath.parser.ASTAxisSelector;
import es.uca.webservices.xpath.parser.ASTChildren;
import es.uca.webservices.xpath.parser.ASTCurrentNode;
import es.uca.webservices.xpath.parser.ASTDiv;
import es.uca.webservices.xpath.parser.ASTEqual;
import es.uca.webservices.xpath.parser.ASTExpression;
import es.uca.webservices.xpath.parser.ASTFunctionCall;
import es.uca.webservices.xpath.parser.ASTGreaterEqual;
import es.uca.webservices.xpath.parser.ASTGreaterThan;
import es.uca.webservices.xpath.parser.ASTLessEqual;
import es.uca.webservices.xpath.parser.ASTLessThan;
import es.uca.webservices.xpath.parser.ASTLiteralProcessingInstruction;
import es.uca.webservices.xpath.parser.ASTMod;
import es.uca.webservices.xpath.parser.ASTMult;
import es.uca.webservices.xpath.parser.ASTNeg;
import es.uca.webservices.xpath.parser.ASTNodeType;
import es.uca.webservices.xpath.parser.ASTNotEqual;
import es.uca.webservices.xpath.parser.ASTNumberConstant;
import es.uca.webservices.xpath.parser.ASTOnlyTestNamespaceNode;
import es.uca.webservices.xpath.parser.ASTOr;
import es.uca.webservices.xpath.parser.ASTParentNode;
import es.uca.webservices.xpath.parser.ASTPredicate;
import es.uca.webservices.xpath.parser.ASTQName;
import es.uca.webservices.xpath.parser.ASTRoot;
import es.uca.webservices.xpath.parser.ASTStringConstant;
import es.uca.webservices.xpath.parser.ASTSubtract;
import es.uca.webservices.xpath.parser.ASTUnion;
import es.uca.webservices.xpath.parser.ASTVariableReference;
import es.uca.webservices.xpath.parser.SimpleNode;
import es.uca.webservices.xpath.parser.XPathParserVisitor;

/**
 * XPath AST visitor that extracts the XSLT stylesheets referenced by calls to
 * the <code>bpel:doXslTransform</code> XPath extension function described by
 * the WS-BPEL 2.0 standard.
 *
 * @author Antonio García-Domínguez
 */
public class XSLTDependencyVisitor implements XPathParserVisitor {

	private static final Logger LOGGER = LoggerFactory.getLogger(XSLTDependencyVisitor.class);
	private static final String DOXSLT_FNAME = "doXslTransform";
	private static final Pattern DOXSLT_REGEX = Pattern.compile("[\"']project:/xslt/(.*)[\"']");
	private final File baseDir;
	private final Set<File> dependencies;

	public XSLTDependencyVisitor(File baseDir, Set<File> dependencies) {
		this.baseDir = baseDir;
		this.dependencies = dependencies;
	}

	@Override
	public Object visit(SimpleNode node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTExpression node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTOr node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTAnd node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTEqual node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTNotEqual node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTLessThan node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTGreaterThan node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTLessEqual node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTGreaterEqual node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTAdd node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTSubtract node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTMult node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTDiv node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTMod node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTNeg node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTUnion node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTChildren node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTAllDescendants node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTRoot node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTPredicate node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTAxisSelector node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTAxis node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTCurrentNode node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTParentNode node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTLiteralProcessingInstruction node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTNodeType node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTOnlyTestNamespaceNode node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTAnyNode node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTVariableReference node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTStringConstant node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTNumberConstant node, Object data) {
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTFunctionCall node, Object data) {
		final ASTQName fName = (ASTQName)node.jjtGetChild(0);
		if (BPELConstants.BPEL2_NS_PREFIX.equals(fName.getPrefix()) && DOXSLT_FNAME.equals(fName.getLocalPart())) {
			if (node.jjtGetNumChildren() > 1) {
				 if (node.jjtGetChild(1) instanceof ASTStringConstant) {
					 final String firstArg = ((ASTStringConstant)node.jjtGetChild(1)).getValue();
					 final Matcher matcher = DOXSLT_REGEX.matcher(firstArg);
					 final String xslPath;
					 if (matcher.matches()) {
						 xslPath = matcher.group(1);
					 } else {
//						 LOGGER.warn("Cannot extract path to XSLT dependency from '{}', as it does not follow the expected regex '" + DOXSLT_REGEX.pattern() + "'", firstArg);
						 xslPath = firstArg.replaceAll("\"", "");
					 }
					 dependencies.add(new File(baseDir, xslPath));
				 } else {
					 LOGGER.warn("Cannot extract path to XSLT dependency from '{}', as it is not constant", node.jjtGetChild(1));
				 }
			}
		}
		
		return acceptIntoChildren(node, data);
	}

	@Override
	public Object visit(ASTQName node, Object data) {
		return acceptIntoChildren(node, data);
	}

	private Object acceptIntoChildren(SimpleNode node, Object data) {
		for (int i = 0; i < node.jjtGetNumChildren(); ++i) {
			node.jjtGetChild(i).jjtAccept(this, data);
		}
		return null;
	}

}
