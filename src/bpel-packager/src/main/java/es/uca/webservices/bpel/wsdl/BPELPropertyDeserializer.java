package es.uca.webservices.bpel.wsdl;

import javax.wsdl.Definition;
import javax.wsdl.WSDLException;
import javax.wsdl.extensions.ExtensibilityElement;
import javax.wsdl.extensions.ExtensionDeserializer;
import javax.wsdl.extensions.ExtensionRegistry;
import javax.xml.namespace.QName;

import org.w3c.dom.Element;

import es.uca.webservices.bpel.BPELProperty;
import es.uca.webservices.bpel.util.XMLUtils;

/**
 * WSDL4J deserializer for WS-BPEL 2.0 variable property extensibility elements.
 * 
 * NOTE: this deserializer does not extract the actual query expressions. The XML Schema for that
 * part is too lax, and it wouldn't be very useful for our use cases anyway.
 *
 * @author Antonio García-Domínguez
 */
class BPELPropertyDeserializer implements ExtensionDeserializer {
	@SuppressWarnings("rawtypes")
	@Override
	public ExtensibilityElement unmarshall(Class parentClass, QName qname,
			Element elem, Definition def, ExtensionRegistry reg)
			throws WSDLException {
		final String propName = elem.getAttribute("name");
		final String propType = elem.getAttribute("type");
		final String propElem = elem.getAttribute("element");

		final QName nameQName = XMLUtils.stringToQName(propName, elem, def.getTargetNamespace());
		final QName typeQName = XMLUtils.stringToQName(propType, elem, def.getTargetNamespace());
		final QName elemQName = XMLUtils.stringToQName(propElem, elem, def.getTargetNamespace());

		return new BPELProperty(nameQName, typeQName, elemQName);
	}
}