package es.uca.webservices.bpel.bpelunit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;
import javax.xml.stream.EventFilter;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Compares BPELUnit XML result files using StAX.
 *
 * @author Antonio García-Domínguez
 */
public class FastBPELUnitXMLResultComparator {

	static final String XMLDATA_NAMEVAL = "SOAP Message data";
	static final String XMLDATA_WSHT_NAMEVAL = "Input XML Data";
	static final String DATAPKG_NAMEVAL = "Receive Data Package";
	static final String DATAPKG_WSHT_NAMEVAL = "Complete Task Specification";
	private static final Pattern RE_WHITESPACE = Pattern.compile("\\s*");

	private final Set<QName> ignoredTags = new HashSet<QName>();

	private static class AnyOf implements EventFilter {
		private EventFilter[] filters;

		public AnyOf(EventFilter... filters) {
			this.filters = filters;
		}

		public boolean accept(XMLEvent ev) {
			for (EventFilter f : filters) {
				if (f.accept(ev)) return true;
			}
			return false;
		}
	}

	private static class OnlyElementStart implements EventFilter {
		private static final QName QNAME_NAMEATTR = new QName("name");

		private QName elementName;
		private String nameAttrValue;

		public OnlyElementStart(QName name) {
			this(name, null);
		}

		public OnlyElementStart(QName name, String nameAttrValue) {
			this.elementName = name;
			this.nameAttrValue = nameAttrValue;
		}

		@Override
		public boolean accept(XMLEvent arg0) {
			if (arg0.isStartElement()) {
				final StartElement se = arg0.asStartElement();
				return elementName.equals(se.getName()) && optionalNameAttrMatches(se);
			}
			return false;
		}

		private boolean optionalNameAttrMatches(StartElement se) {
			return nameAttrValue == null || nameAttrValue.equals(se.getAttributeByName(QNAME_NAMEATTR).getValue());
		}
	}

	private static final QName QNAME_TESTCASE = new QName(
			BPELUnitXMLResult.BPELUNIT_RESULT_NAMESPACE, "testCase");
	private static final QName QNAME_XMLDATA = new QName(
			BPELUnitXMLResult.BPELUNIT_RESULT_NAMESPACE, "xmlData");
	private static final QName QNAME_DATAPKG = new QName(
			BPELUnitXMLResult.BPELUNIT_RESULT_NAMESPACE, "dataPackage");

	public void ignoreTags(Collection<QName> tags) {
		ignoredTags.addAll(tags);
	}

	public void ignoreTags(QName... tags) {
		ignoreTags(Arrays.asList(tags));
	}

	/**
	 * Convenience version of {@link #compare(XMLEventReader, XMLEventReader)} for two files. 
	 */
	public Boolean[] compare(File fOriginal, File fMutated) throws FileNotFoundException, XMLStreamException, IncompatibleResultsException {
		final XMLInputFactory factory = createFactory();
		XMLEventReader reader1 = factory.createXMLEventReader(new FileInputStream(fOriginal));
		XMLEventReader reader2 = factory.createXMLEventReader(new FileInputStream(fMutated));
		return compare(reader1, reader2);
	}

	/**
	 * Convenience version of {@link #compare(XMLEventReader, XMLEventReader)} for two strings. 
	 */
	public Boolean[] compare(String sOriginal, String sMutated) throws XMLStreamException, IncompatibleResultsException {
		final XMLInputFactory factory = createFactory();
		XMLEventReader reader1 = factory.createXMLEventReader(new StringReader(sOriginal));
		XMLEventReader reader2 = factory.createXMLEventReader(new StringReader(sMutated));
		return compare(reader1, reader2);		
	}

	/**
	 * Compares this XML result with another and reports what test cases show
	 * different outbound SOAP messages from the WS-BPEL composition in each.
	 * These outbound messages include the reply from the WS-BPEL composition to
	 * the request which created the instance, and all requests sent from the
	 * composition to the external services.
	 * 
	 * @param readerOriginal
	 *            StAX event reader for the results of the original program.
	 * @param ignoreXPath
	 *            StAX event reader for the results of the mutated program.
	 * @return Array of Booleans indicating for each test case whether they
	 *         produced the same result (true) or not (false).
	 * @throws XMLStreamException
	 *             An error occurred while parsing one of the two results.
	 * @throws IncompatibleResultsException
	 *             The original and mutated program produce incomparable results
	 *             (e.g. different number of test cases).
	 */
	public Boolean[] compare(XMLEventReader readerOriginal, XMLEventReader readerMutated) throws XMLStreamException, IncompatibleResultsException {
		final boolean SAME_RESULT = true;
		final boolean DIFF_RESULT = false;
		final List<Boolean> results = new ArrayList<Boolean>();

		final EventFilter testCaseFilter = new OnlyElementStart(QNAME_TESTCASE);
		final EventFilter dataPkgFilter = new OnlyElementStart(QNAME_DATAPKG, DATAPKG_NAMEVAL);
		final EventFilter dataPkgWSHTFilter = new OnlyElementStart(QNAME_DATAPKG, DATAPKG_WSHT_NAMEVAL);
		final EventFilter xmlDataFilter = new OnlyElementStart(QNAME_XMLDATA, XMLDATA_NAMEVAL);
		final EventFilter xmlDataWSHTFilter = new OnlyElementStart(QNAME_XMLDATA, XMLDATA_WSHT_NAMEVAL);

		final EventFilter testCaseOrDataPkgFilter = new AnyOf(testCaseFilter, dataPkgFilter, dataPkgWSHTFilter);
		final EventFilter testCaseOrDataPkgOrDataFilter = new AnyOf(testCaseFilter,	dataPkgFilter, dataPkgWSHTFilter, xmlDataFilter, xmlDataWSHTFilter);

		// Start looking at the first test case
		StartElement evOriginal = nextStart(readerOriginal, testCaseFilter);
		StartElement evMutated = nextStart(readerMutated, testCaseFilter);

		while (evOriginal != null) {
			if (evMutated == null) {
				throw new IncompatibleResultsException("Incompatible number of test cases");
			}

			// Go to the next data package or test case element, whichever happens first
			evOriginal = nextStart(readerOriginal, testCaseOrDataPkgFilter);
			evMutated = nextStart(readerMutated, testCaseOrDataPkgFilter);

			// Compare where we landed in the original and mutated test case
			if (evOriginal == null || QNAME_TESTCASE.equals(evOriginal.getName())) {
				if (evMutated == null || QNAME_TESTCASE.equals(evMutated.getName())) {
					// We reached the end of the data packages in the original test
					// case at the same time than the mutated test case: mutant survived.
					results.add(SAME_RESULT);
				}
				else {
					// We reached the end of the data packages in the original test
					// case before the mutated test case: mutant was killed.
					results.add(DIFF_RESULT);
					evMutated = nextStart(readerMutated, testCaseFilter);
				}
			}
			else {
				if (evMutated != null && QNAME_DATAPKG.equals(evMutated.getName())) {
					// Compare the XML data elements in both sides: if they're not
					// the same, kill this mutant and skip to the next test case.
					//
					// We assume that every data package has a xmlData element with
					// the name 'SOAP message data'.
					evOriginal = nextStart(readerOriginal, testCaseOrDataPkgOrDataFilter);
					evMutated = nextStart(readerMutated, testCaseOrDataPkgOrDataFilter);

					if (evOriginal == null || !QNAME_XMLDATA.equals(evOriginal.getName())) {
						throw new IllegalArgumentException("Original data package without the expected xmlData child");
					}
					else if (evMutated == null || !QNAME_XMLDATA.equals(evMutated.getName())) {
						throw new IllegalArgumentException("Mutated data package without the xmlData child");
					}

					if (!sameUntilElementEnds(readerOriginal, readerMutated, QNAME_XMLDATA)) {
						results.add(DIFF_RESULT);
						evOriginal = nextStart(readerOriginal, testCaseFilter);
						evMutated = nextStart(readerMutated, testCaseFilter);
					}
				}
				else {
					// We reached the end of the data packages in the mutated test
					// case before the original test case: mutant was killed.
					results.add(DIFF_RESULT);
					evOriginal = nextStart(readerOriginal, testCaseFilter);
				}
			}
		}
		if (evMutated != null) {
			throw new IncompatibleResultsException("Incompatible number of test cases");
		}

		return results.toArray(new Boolean[results.size()]);
	}

	private XMLInputFactory createFactory() throws FactoryConfigurationError {
		final XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
		factory.setProperty(XMLInputFactory.IS_COALESCING, true);
		return factory;
	}

	/**
	 * Comparison of StAX events, as XMLEvent does not implement its own version
	 * of {@link #equals(Object)}.
	 */
	private boolean equalEvents(XMLEvent evOriginal, XMLEvent evMutated) {
		if (evOriginal.getEventType() != evMutated.getEventType()) {
			return false;
		}
		else if (evOriginal.isStartElement()) {
			StartElement evOriginalSE = evOriginal.asStartElement();
			StartElement evMutatedSE = evMutated.asStartElement();
			if (!evOriginalSE.getName().equals(evMutatedSE.getName())) {
				return false;
			}

			final Map<QName, String> mapAttrOriginal = extractAttributeMap(evOriginalSE);
			final Map<QName, String> mapAttrMutated = extractAttributeMap(evMutatedSE);
			if (!mapAttrOriginal.equals(mapAttrMutated)) {
				return false;
			}
		}
		else if (evOriginal.isCharacters()) {
			final String dataOriginal = evOriginal.asCharacters().getData();
			final String dataMutated = evMutated.asCharacters().getData();
			if (!dataOriginal.equals(dataMutated)) {
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private Map<QName, String> extractAttributeMap(StartElement evOriginalSE) {
		final Map<QName, String> mapAttrOriginal = new HashMap<QName, String>();
		final Iterator<Attribute> attrOriginal = evOriginalSE.getAttributes();
		while (attrOriginal.hasNext()) {
			final Attribute attr = attrOriginal.next();
			mapAttrOriginal.put(attr.getName(), attr.getValue());
		}
		return mapAttrOriginal;
	}

	private StartElement nextStart(XMLEventReader reader, EventFilter filter) throws XMLStreamException {
		while (reader.hasNext()) {
			XMLEvent ev = reader.nextEvent();
			if (filter.accept(ev)) {
				return ev.asStartElement();
			}
		}
		return null;
	}

	private boolean sameUntilElementEnds(XMLEventReader readerOriginal,
			XMLEventReader readerMutated, QName endTagName)
			throws XMLStreamException {
		while (readerOriginal.hasNext()) {
			if (!readerMutated.hasNext()) {
				return false;
			}
			XMLEvent evOriginal = nextEventNotIgnored(readerOriginal);
			XMLEvent evMutated = nextEventNotIgnored(readerMutated);

			if (!equalEvents(evOriginal, evMutated)) {
				return false;
			}
			if (evOriginal.isEndElement()) {
				final EndElement evOriginalEndElement = evOriginal.asEndElement();
				if (endTagName.equals(evOriginalEndElement.getName())) {
					return true;
				}
			}
		}
		throw new IllegalArgumentException("Did not find the end tag");
	}

	private XMLEvent nextEventNotIgnored(XMLEventReader reader) throws XMLStreamException {
		XMLEvent ev = reader.nextEvent();
		while (ev != null) {
			if (ev.isStartElement() && ignoredTags.contains(ev.asStartElement().getName())) {
				// Start of an ignored element: ignore all its contents
				int nestingLevel = 1;
				while (ev != null && nestingLevel > 0) {
					ev = reader.nextEvent();
					if (ev.isStartElement()) {
						++nestingLevel;
					} else if (ev.isEndElement()) {
						--nestingLevel;
					}
				}
				if (ev != null) {
					// We want the event right after the ignored element ended
					ev = reader.nextEvent();
				}
			}
			else if (ev.isCharacters() && RE_WHITESPACE.matcher(ev.asCharacters().getData()).matches()) {
				// Ignore pure whitespace
				ev = reader.nextEvent();
			}
			else {
				return ev;
			}
		}
		return ev;
	}

}
