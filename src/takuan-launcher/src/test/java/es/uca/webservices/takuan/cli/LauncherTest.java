package es.uca.webservices.takuan.cli;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests for the main launcher. Note: these tests only work on Linux installations
 * with 'bash' installed, due to the takuan-perl-wrapper.sh Bash script used to
 * monitor Perl's memory usage.
 * 
 * <p>
 * Copyright (C) 2012 Antonio García-Domínguez.
 * </p>
 * 
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * </p>
 * 
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * 
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a href="http://www.gnu.org/licenses/">here</a>.
 * </p>
 */
public class LauncherTest {

	private static final class LogFilenameFilter implements FilenameFilter {
		@Override
		public boolean accept(File file, String filename) {
			return filename.endsWith(".dtrace");
		}
	}

	private ByteArrayOutputStream bosOut;
	private ByteArrayOutputStream bosErr;
	private PrintStream posOut;
	private PrintStream posErr;

	@Before
	public void setup() {
		bosOut = new ByteArrayOutputStream();
		bosErr = new ByteArrayOutputStream();
		posOut = new PrintStream(bosOut);
		posErr = new PrintStream(bosErr);
	}

	@Test
	public void illegalNoArgs() throws Exception {
		illegalArgs();
	}

	@Test
	public void integrationTest() throws Exception {
		System.setProperty(Launcher.DAIKON_DEFAULT_OPTIONS_PROPERTY, "src/main/assembly/daikon.options");
		System.setProperty(Launcher.DAIKON_JVM_FLAGS_PROPERTY, "-Dsimplify.path=bin/simplify");
		System.setProperty(Launcher.TAKUAN_PERL_CMD_PROPERTY, "bin/takuan-perl-wrapper.sh");
		new Launcher().run(new String[] {"src/test/resources/integration/sample.yaml"}, System.out, System.err);

		final File fBaseDir = new File("target");
		final File fDefaultDir = new File(fBaseDir, "integration-results-default");
		final File fComparabilityDir = new File(fBaseDir, "integration-results-without-comparability");
		final File f1LevelDir = new File(fBaseDir, "integration-results-1level");
		final File fDaikonXMLDefault = new File(fDefaultDir, Launcher.DAIKON_XML_FILENAME);
		final File fDaikonXMLComparability = new File(fComparabilityDir, Launcher.DAIKON_XML_FILENAME);
		final File fDaikonStatsDefault = new File(fDefaultDir, Launcher.DAIKON_STATS_FILENAME);
		final File fDaikonStatsComparability = new File(fComparabilityDir, Launcher.DAIKON_STATS_FILENAME);
		final File fDaikonOutDefault = new File(fDefaultDir, Launcher.DAIKON_STDOUT_FILENAME);
		final File fDaikonOutComparability = new File(fComparabilityDir, Launcher.DAIKON_STDOUT_FILENAME);
		final File fCoverageDefault = new File(fDefaultDir, Launcher.COVERAGE_EXECUTED_FILENAME);
		final File fCoverageComparability = new File(fComparabilityDir, Launcher.COVERAGE_EXECUTED_FILENAME);
		final File fCoverage1Level = new File(f1LevelDir, Launcher.COVERAGE_EXECUTED_FILENAME);
		final File fActiveBPELLogsDefault = new File(new File(fDefaultDir, Launcher.ACTIVEBPEL_RESULT_SUBDIR), Launcher.ACTIVEBPEL_LOGS_SUBDIR);
		final File fActiveBPELLogsComparability = new File(new File(fComparabilityDir, Launcher.ACTIVEBPEL_RESULT_SUBDIR), Launcher.ACTIVEBPEL_LOGS_SUBDIR);
		final File fActiveBPELLogs1Level = new File(new File(f1LevelDir, Launcher.ACTIVEBPEL_RESULT_SUBDIR), Launcher.ACTIVEBPEL_LOGS_SUBDIR);
		final File fTakuanDirDefault = new File(fDefaultDir, Launcher.TAKUAN_DTRACE_DIRECTORY_NAME);
		final File fTakuanDirComparability = new File(fComparabilityDir, Launcher.TAKUAN_DTRACE_DIRECTORY_NAME);
		final File fTakuanDir1Level = new File(f1LevelDir, Launcher.TAKUAN_DTRACE_DIRECTORY_NAME);
		final File fSavedCGraphs1Level = new File(fTakuanDir1Level, Launcher.TAKUAN_CGRAPHS_FILENAME);

		assertTrue("inv2XML should be honored", fDaikonXMLDefault.canRead());
		assertTrue("Comparability graphs should be saved", fSavedCGraphs1Level.canRead());
		assertFalse("stats is false by default", fDaikonStatsDefault.canRead());
		assertTrue("coverage should be honored", fCoverageDefault.canRead());
		assertFalse("ActiveBPEL exec logs should be deleted by default", fActiveBPELLogsDefault.exists());
		assertEquals("Takuan dtraces should be deleted by default", 0, fTakuanDirDefault.listFiles(new LogFilenameFilter()).length);

		assertFalse("inv2XML is false by default", fDaikonXMLComparability.canRead());
		assertTrue("stats should be honored", fDaikonStatsComparability.canRead());
		assertFalse("coverage is false by default", fCoverageComparability.canRead());
		assertFalse("ActiveBPEL exec logs should be cached", fActiveBPELLogsComparability.exists());
		assertEquals("Takuan dtraces should be deleted by default", 0, fTakuanDirComparability.listFiles(new LogFilenameFilter()).length);

		assertFalse("coverage is false by default", fCoverage1Level.canRead());
		assertTrue("keepExecutionLogs should be honored", fActiveBPELLogs1Level.isDirectory());
		assertTrue("keepDTraces should be honored", fTakuanDir1Level.listFiles(new LogFilenameFilter()).length > 0);

		assertTrue("Some invariants should have been generated for the default configuration", fDaikonOutDefault.length() > 0);
		assertTrue("Some invariants should have been generated for the configuration with comparability indices", fDaikonOutComparability.length() > 0);
	}

	private void illegalArgs(String... args) throws Exception {
		try {
			new Launcher().run(args, posOut, posErr);
			fail("An exception was expected.");
		} catch (Exception ex) {}
		final String sOut = bosOut.toString();
		final String sErr = bosErr.toString();

		assertEquals("", sOut);
		assertTrue("Usage string should contain the value of the version property",
				sErr.contains("version"));
		assertTrue("Usage string should contain the usage of the launcher",
				sErr.contains("Usage:"));
	}
}
