package es.uca.webservices.takuan.conf;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeThat;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import es.uca.webservices.bpel.bpelunit.CustomizedRunner;

/**
 * <p>
 * Test cases for the YAML configuration loader.
 * </p>
 * 
 * <p>
 * Copyright (C) 2011 Antonio García-Domínguez.
 * </p>
 * 
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * </p>
 * 
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * 
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a href="http://www.gnu.org/licenses/">here</a>.
 * </p>
 */
public class ConfigurationLoaderTest {

	private ConfigurationLoader loader;

	@Before
	public void setup() throws Exception {
		loader = new ConfigurationLoader();
	}

	@Test
	public void empty() throws Exception {
		final List<ConfigurationNode> configs = load("empty.yaml").getConfigurations();
		assertThat(configs.size(), is(0));
	}

	@Test
	public void one() throws Exception {
		final Configurations results = load("one-test.yaml");
		final File fYAML = results.getFile();
		final List<ConfigurationNode> configs = results.getConfigurations();
		assertThat(configs.size(), is(1));
		assertThat(configs.get(0).getActiveBPELPort(), is(CustomizedRunner.DEFAULT_ENGINE_PORT));

		assertConfigurationEquals(fYAML.getParentFile(),
			"f.bpel", "f.bpts", "results",
			new String[]{"--simplify", "--disable-comparabilities"},
			configs.get(0));
	}

	@Test(expected=ConfigurationException.class)
	public void oneMissingBPEL() throws Exception {
		load("one-test-missing-bpel.yaml");
	}

	@Test(expected=ConfigurationException.class)
	public void oneMissingBPTS() throws Exception {
		load("one-test-missing-bpts.yaml");
	}

	@Test(expected=ConfigurationException.class)
	public void oneMissingDir() throws Exception {
		load("one-test-missing-dir.yaml");
	}

	@Test(expected=ConfigurationException.class)
	public void twoSameDir() throws Exception {
		load("two-tests-same-dir.yaml");
		fail("An exception should have been thrown");
	}

	@Test
	public void twoDiffDir() throws Exception {
		final Configurations results = load("two-tests-diff-dir.yaml");
		final File fYAML = results.getFile();
		final List<ConfigurationNode> configs = results.getConfigurations();

		assertThat(configs.size(), is(2));
		assertConfigurationEquals(fYAML.getParentFile(),
				"f.bpel", "f.bpts", "results-f",
				new String[]{"--simplify", "--disable-comparabilities"},
				configs.get(0)
		);
		assertConfigurationEquals(fYAML.getParentFile(),
				"g.bpel", "h.bpts", "results-h",
				new String[]{"--index-flattening"},
				configs.get(1)
		);
	}

	@Test
	public void treeNoChildren() throws Exception {
		// This test is skipped on non-Unix systems which do not use / as the root directory
		assumeThat(systemUnderstandsUnixPaths(), is(true));

		final Configurations results = load("test-tree-0children.yaml");
		final List<ConfigurationNode> configs = results.getConfigurations();
		assertThat(configs.size(), is(1));

		assertConfigurationEquals(null,
				"/tmp/f.bpel", "/tmp/f.bpts", "/tmp/results",
				new String[] { "--simplify", "--disable-comparabilities" },
				configs.get(0)
		);
	}

	@Test
	public void treeOneChild() throws Exception {
		final Configurations results = load("test-tree-1child.yaml");
		final File fYAML = results.getFile();
		final List<ConfigurationNode> configs = results.getConfigurations();
		assertThat(configs.size(), is(1));

		assertConfigurationEquals(fYAML.getParentFile(),
				"f.bpel", "f.bpts", "results",
				new String[] { "--simplify", "--disable-comparabilities" },
				configs.get(0)
		);
	}

	@Test
	public void treeTwoChildren() throws Exception {
		final Configurations results = load("test-tree-2children-ok.yaml");
		final File fYAML = results.getFile();
		final List<ConfigurationNode> configs = results.getConfigurations();
		assertThat(configs.size(), is(2));

		assertThat(configs.get(0).getActiveBPELPort(), is(8081));
		assertThat(configs.get(1).getActiveBPELPort(), is(8081));

		assertConfigurationEquals(fYAML.getParentFile(),
				"f.bpel", "f.bpts", "results-simplify",
				new String[] { "--simplify", "--disable-comparabilities" },
				configs.get(0)
		);
		assertConfigurationEquals(fYAML.getParentFile(),
				"f.bpel", "f.bpts", "results-nosimplify",
				new String[] { "--disable-comparabilities" },
				configs.get(1)
		);

		assertThat(configs.get(0).getMaxInstrumentationDepth(), is(equalTo(3)));
		assertThat(configs.get(1).getMaxInstrumentationDepth(), is(equalTo(3)));
		assertThat(configs.get(0).getInstrumentVariablesByDefault(), is(false));
		assertThat(configs.get(1).getInstrumentVariablesByDefault(), is(false));
		assertThat(configs.get(0).getInstrumentSequencesByDefault(), is(false));
		assertThat(configs.get(1).getInstrumentSequencesByDefault(), is(nullValue()));
	}

	@Test
	public void treeFlagsAreGathered() throws Exception {
		final Configurations results =  load("test-tree-gather-flags.yaml");
		final File fYAML = results.getFile();
		final List<ConfigurationNode> configs = results.getConfigurations();
		assertThat(configs.size(), is(2));

		assertConfigurationEquals(fYAML.getParentFile(),
				"f.bpel", "f.bpts", "results-simplify",
				new String[] { "--disable-comparabilities", "--simplify" },
				configs.get(0)
		);
		assertConfigurationEquals(fYAML.getParentFile(),
				"f.bpel", "f.bpts", "results-nosimplify",
				new String[] { "--disable-comparabilities" },
				configs.get(1)
		);
		assertThat(configs.get(0).getDaikonFlags(), is(equalTo(Arrays.asList("-a", "-b"))));
		assertThat(configs.get(1).getDaikonFlags(), is(equalTo(Arrays.asList("-a"))));
	}

	@Test(expected=ConfigurationException.class)
	public void treeTwoChildrenDirectoryConflict() throws Exception {
		load("test-tree-2children-dirconflict.yaml");
	}

	@Test
	public void treeNested() throws Exception {
		final Configurations configs = load("test-tree-nested.yaml");
		performNestedAssertions(configs.getFile().getParentFile(), configs.getConfigurations());
	}

	@Test
	public void treeSuffixes() throws Exception {
		final Configurations  configs = load("test-tree-suffixes.yaml");
		performNestedAssertions(configs.getFile().getParentFile(), configs.getConfigurations());
	}

	@Test(expected=ConfigurationException.class)
	public void innerNodesCannotHaveIDs() throws Exception {
		load("ids-inner-node-with-id.yaml");
	}

	@Test(expected=ConfigurationException.class)
	public void cannotRepeatIDsInsideDifferentDocsInSameFile() throws Exception {
		load("ids-repeated-id-flat.yaml");
	}

	@Test(expected=ConfigurationException.class)
	public void cannotRepeatIDsInsideSameDocInSameFile() throws Exception {
		load("ids-repeated-id-tree.yaml");
	}

	@Test
	public void differentIDsShouldWork_flat() throws Exception {
		final Configurations results = load("ids-different-id-flat.yaml");

		// IDs are properly saved inside the objects
		final ConfigurationNode configF = results.getConfigurations().get(0);
		final ConfigurationNode configG = results.getConfigurations().get(1);
		assertThat(configF.getId(), is(equalTo("f")));
		assertThat(configG.getId(), is(equalTo("g")));
	}

	@Test
	public void differentIDsShouldWork_tree() throws Exception {
		load("ids-different-id-tree.yaml");
	}

	@Test
	public void emptyCompareWithShouldWork() throws Exception {
		final Configurations configs = load("comparewith-empty.yaml");
		assertThat(configs.getConfigurations().get(0).getCompareWith().size(), is(equalTo(0)));
		assertThat(configs.getConfigurations().get(1).getCompareWith().size(), is(equalTo(0)));
	}

	@Test
	public void compareWithUsingExistingIDShouldWork() throws Exception {
		final Configurations configs = load("comparewith-good.yaml");
		assertThat(configs.getConfigurations().get(0).getCompareWith().size(), is(equalTo(0)));

		final List<String> cwithSecondNode = configs.getConfigurations().get(1).getCompareWith();
		assertThat(cwithSecondNode.size(), is(equalTo(1)));
		assertThat(cwithSecondNode.get(0), is(equalTo("f")));
	}

	@Test(expected=ConfigurationException.class)
	public void compareWithUsingMissingIDShouldFail() throws Exception {
		load("comparewith-missingID.yaml");
	}

	@Test(expected=ConfigurationException.class)
	public void addRawDTracesIncompatibleWithLogsForSameIDInvalid() throws Exception {
		load("addlogs-addtraces-invalid.yaml");
	}

	@Test
	public void addRawDTracesIncompatibleWithLogsForSameIDValid() throws Exception {
		load("addlogs-addtraces-valid.yaml");
	}

	private boolean systemUnderstandsUnixPaths() {
		try {
			new File("/").getCanonicalFile();
			return true;
		} catch (IOException ex) {
			return false;
		}
	}

	private void performNestedAssertions(final File baseDir, final List<ConfigurationNode> configs) {
		assertConfigurationEquals(baseDir,
				"f.bpel", "f.bpts", "results-f-simp-nocomp",
				new String[] { "--simplify", "--disable-comparabilities" },
				configs.get(0)
		);
		assertConfigurationEquals(baseDir,
				"f.bpel", "f.bpts", "results-f-nosimp-nocomp",
				new String[] { "--disable-comparabilities" },
				configs.get(1)
		);
		assertConfigurationEquals(baseDir,
				"f.bpel", "g.bpts", "results-g-simp-nocomp",
				new String[] { "--simplify", "--disable-comparabilities" },
				configs.get(2)
		);
		assertConfigurationEquals(baseDir,
				"f.bpel", "g.bpts", "results-g-nosimp-nocomp",
				new String[] { "--disable-comparabilities" },
				configs.get(3)
		);
	}

	private Configurations load(final String yaml) throws ConfigurationException, IOException {
		final File fYAML = new File("src/test/resources/" + yaml).getCanonicalFile();
		loader.setBaseDir(fYAML.getCanonicalFile().getParentFile());
		return loader.load(fYAML);
	}

	private void assertConfigurationEquals(
			final File baseDir,
			final String expectedBPEL,
			final String expectedBPTS,
			final String expectedOutputDir,
			final String[] expectedFlags,
			final ConfigurationNode obtained)
	{
		final File fExpectedBPEL = (baseDir != null
				? new File(baseDir, expectedBPEL) : new File(expectedBPEL));
		final File fExpectedBPTS = (baseDir != null
				? new File(baseDir, expectedBPTS) : new File(expectedBPTS));
		final File fExpectedOutputDir = (baseDir != null
				? new File(baseDir, expectedOutputDir) : new File(expectedOutputDir));

		assertThat(obtained.getBpel(), is(fExpectedBPEL));
		assertThat(obtained.getBpts(), is(fExpectedBPTS));
		assertThat(obtained.getDir(), is(fExpectedOutputDir));
		assertThat(obtained.getFlags(), is(Arrays.asList(expectedFlags)));
	}
}
