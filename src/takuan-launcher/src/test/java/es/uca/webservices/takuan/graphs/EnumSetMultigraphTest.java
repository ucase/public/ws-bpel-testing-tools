package es.uca.webservices.takuan.graphs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Map;

import org.junit.Test;

/**
 * Tests for the {@link EnumSetMultigraph} class.
 *
 * @author Antonio García-Domínguez
 */
public class EnumSetMultigraphTest {

	private enum Color { RED, GREEN, BLUE };
	private static class StringMG extends EnumSetMultigraph<String, Color> {}

	@Test
	public void emptyGraph() {
		final StringMG graph = new StringMG();
		assertTrue(graph.getVertices().isEmpty());
	}

	@Test
	public void removeFromEmptyGraph() {
		final StringMG g = new StringMG();
		g.removeVertex("a");
		assertHasVertices(g);
	}

	@Test
	public void addOneNode() {
		final StringMG g = new StringMG();
		g.addVertex("a");
		assertHasVertices(g, "a");
	}

	@Test
	public void addRemoveOneNode() {
		final StringMG g = new StringMG();
		g.addVertex("a");
		g.removeVertex("a");
		assertHasVertices(g);
	}

	@Test
	public void addSameNodesTwice() {
		final StringMG g = new StringMG();
		g.addEdge("a", "b", EnumSet.of(Color.RED));
		g.addVertex("a");
		g.addVertex("b");
		assertHasEdges(g, "a", "b", EnumSet.of(Color.RED));
	}

	@Test
	public void addOneEdgeThenAnother() {
		final StringMG g = new StringMG();
		g.addEdge("a", "b", EnumSet.of(Color.RED));
		g.addEdge("a", "b", EnumSet.of(Color.BLUE));
		assertHasEdges(g, "a", "b", EnumSet.of(Color.RED, Color.BLUE));
	}

	@Test
	public void addTwoEdgesRemoveOne() {
		final StringMG g = new StringMG();
		g.addEdge("a", "b", EnumSet.of(Color.RED, Color.GREEN));
		g.removeEdge("a", "b", EnumSet.of(Color.RED));
		assertHasEdges(g, "a", "b", EnumSet.of(Color.GREEN));
	}

	@Test
	public void addTwoEdgesRemoveTwo() {
		final StringMG g = new StringMG();
		assertNull(g.getEdgesBetween("a", "b"));
		g.addEdge("a", "b", EnumSet.of(Color.RED, Color.BLUE));
		g.removeEdge("a", "b", EnumSet.of(Color.RED, Color.BLUE));
		assertNull(g.getEdgesBetween("a", "b"));
	}

	@Test
	public void selectOutgoingEdgesBySet() {
		final StringMG g = new StringMG();
		g.addEdge("a", "b", EnumSet.of(Color.RED));
		g.addEdge("a", "c", EnumSet.of(Color.GREEN));
		g.addEdge("a", "d", EnumSet.of(Color.BLUE));

		final Map<String, EnumSet<Color>> aOut = g.getOutgoing("a");
		assertEquals(3, aOut.size());

		final Map<String, EnumSet<Color>> aOutRG = g.getOutgoing("a", EnumSet.of(Color.RED, Color.GREEN));
		assertEquals(2, aOutRG.size());
		assertTrue(aOutRG.containsKey("b"));
		assertTrue(aOutRG.containsKey("c"));

		final Map<String, EnumSet<Color>> aOutB = g.getOutgoing("a", EnumSet.of(Color.BLUE));
		assertEquals(1, aOutB.size());
		assertTrue(aOutB.containsKey("d"));
	}

	@Test
	public void selectIncomingEdgesBySet() {
		final StringMG g = new StringMG();
		g.addEdge("r", "a", EnumSet.of(Color.RED));
		g.addEdge("g", "a", EnumSet.of(Color.GREEN));
		g.addEdge("b", "a", EnumSet.of(Color.BLUE));

		assertEquals(3, g.getIncoming("a").size());

		final Map<String, EnumSet<Color>> aInRB = g.getIncoming("a", EnumSet.of(Color.RED, Color.BLUE));
		assertEquals(2, aInRB.size());
		assertTrue(aInRB.containsKey("r"));
		assertTrue(aInRB.containsKey("b"));

		final Map<String, EnumSet<Color>> aInG = g.getIncoming("a", EnumSet.of(Color.GREEN));
		assertEquals(1, aInG.size());
		assertTrue(aInG.containsKey("g"));
	}

	@Test
	public void emptyGraphHasNoCycles() {
		final StringMG g = new StringMG();
		assertFalse("An empty have cannot have any cycles", g.isCyclic(null));
	}

	@Test
	public void oneNodeZeroEdgeGraphWithNoCycles() {
		final StringMG g = new StringMG();
		g.addVertex("a");
		assertFalse("A graph with one node and no edges cannot have any cycles", g.isCyclic(null));
	}

	@Test
	public void oneNodeSelfLoopGraphHasCycles() {
		final StringMG g = new StringMG();
		g.addEdge("a", "a", EnumSet.of(Color.RED));
		assertTrue("Cycle when using all", g.isCyclic(null));
		assertTrue("Cycle when using red", g.isCyclic(EnumSet.of(Color.RED)));
		assertFalse("No cycle when using blue", g.isCyclic(EnumSet.of(Color.BLUE)));
	}

	@Test
	public void twoNodeSingleEdgeGraphHasNoCycles() {
		final StringMG g = new StringMG();
		g.addEdge("a", "b", EnumSet.of(Color.RED));
		assertFalse("A graph with two nodes and an edge between them cannot have any cycles", g.isCyclic(null));
	}

	@Test
	public void twoNodeTwoEdgeGraphHasCycles() {
		final StringMG g = new StringMG();
		g.addEdge("a", "b", EnumSet.of(Color.RED));
		g.addEdge("b", "a", EnumSet.of(Color.BLUE));
		assertTrue("Cycle when using all", g.isCyclic(null));
		assertTrue("Cycle when using red or blue", g.isCyclic(EnumSet.of(Color.RED, Color.BLUE)));
		assertFalse("No cycle when using red or green", g.isCyclic(EnumSet.of(Color.RED, Color.GREEN)));
	}

	@Test
	public void threeNodeTwoEdgeGraphHasNoCycles() {
		final StringMG g = new StringMG();
		g.addEdge("a", "b", EnumSet.of(Color.GREEN));
		g.addEdge("b", "c", EnumSet.of(Color.BLUE));
		assertFalse("A graph with three nodes and two edges cannot have any cycles", g.isCyclic(null));
	}

	@Test
	public void threeNodeThreeEdgeGraphWithoutCycles() {
		final StringMG g = new StringMG();
		g.addEdge("a", "b", EnumSet.of(Color.GREEN));
		g.addEdge("b", "c", EnumSet.of(Color.BLUE));
		g.addEdge("a", "c", EnumSet.of(Color.RED));
		assertFalse(g.isCyclic(null));
	}

	@Test
	public void threeNodeThreeEdgeGraphWithCycles() {
		final StringMG g = new StringMG();
		g.addEdge("a", "b", EnumSet.of(Color.GREEN));
		g.addEdge("b", "c", EnumSet.of(Color.BLUE));
		g.addEdge("c", "a", EnumSet.of(Color.RED));
		assertTrue(g.isCyclic(null));
		assertFalse("No cycle when using only 2 colors", g.isCyclic(EnumSet.of(Color.BLUE, Color.GREEN)));
	}

	@Test
	public void descendantsExternalNode() {
		final StringMG g = new StringMG();
		assertNull(g.getDescendants("a", EnumSet.allOf(Color.class)));
	}

	@Test
	public void descendantsSingleNode() {
		final StringMG g = new StringMG();
		g.addVertex("a");
		assertEquals(0, g.getDescendants("a", EnumSet.allOf(Color.class)).size());
	}

	@Test
	public void descendantsOneLevel() {
		final StringMG g = new StringMG();
		g.addEdge("a", "r", EnumSet.of(Color.RED));
		g.addEdge("a", "g", EnumSet.of(Color.GREEN));
		g.addEdge("a", "b", EnumSet.of(Color.BLUE));

		assertEquals(Arrays.asList("r", "g", "b"), g.getDescendants("a", EnumSet.allOf(Color.class)));
		assertEquals(Arrays.asList("r", "g"), g.getDescendants("a", EnumSet.of(Color.RED, Color.GREEN)));
		assertEquals(Arrays.asList("r", "b"), g.getDescendants("a", EnumSet.of(Color.RED, Color.BLUE)));
		assertEquals(Arrays.asList("g", "b"), g.getDescendants("a", EnumSet.of(Color.GREEN, Color.BLUE)));
		assertEquals(Arrays.asList("r"), g.getDescendants("a", EnumSet.of(Color.RED)));
		assertEquals(Arrays.asList("g"), g.getDescendants("a", EnumSet.of(Color.GREEN)));
		assertEquals(Arrays.asList("b"), g.getDescendants("a", EnumSet.of(Color.BLUE)));
	}

	@Test
	public void descendantsTwoLevels() {
		final StringMG g = new StringMG();
		g.addEdge("a", "r", EnumSet.of(Color.RED));
		g.addEdge("a", "g", EnumSet.of(Color.GREEN));
		g.addEdge("a", "b", EnumSet.of(Color.BLUE));
		g.addEdge("r", "rr", EnumSet.of(Color.RED));
		g.addEdge("r", "rg", EnumSet.of(Color.GREEN));
		g.addEdge("r", "rb", EnumSet.of(Color.BLUE));
		g.addEdge("g", "gr", EnumSet.of(Color.RED));
		g.addEdge("g", "gg", EnumSet.of(Color.GREEN));
		g.addEdge("g", "gb", EnumSet.of(Color.BLUE));
		g.addEdge("b", "br", EnumSet.of(Color.RED));
		g.addEdge("b", "bg", EnumSet.of(Color.GREEN));
		g.addEdge("b", "bb", EnumSet.of(Color.BLUE));

		assertEquals(Arrays.asList(
				"rr", "rg", "rb", "r",
				"gr", "gg", "gb", "g",
				"br", "bg", "bb", "b"),
				g.getDescendants("a", EnumSet.allOf(Color.class)));
		assertEquals(Arrays.asList("rr", "rg", "r",	"gr", "gg", "g"),
				g.getDescendants("a", EnumSet.of(Color.RED, Color.GREEN)));
		assertEquals(Arrays.asList("rr", "rb", "r",	"br", "bb", "b"),
				g.getDescendants("a", EnumSet.of(Color.RED, Color.BLUE)));
		assertEquals(Arrays.asList("gg", "gb", "g",	"bg", "bb", "b"),
				g.getDescendants("a", EnumSet.of(Color.GREEN, Color.BLUE)));
		assertEquals(Arrays.asList("rr", "r"), g.getDescendants("a", EnumSet.of(Color.RED)));
		assertEquals(Arrays.asList("gg", "g"), g.getDescendants("a", EnumSet.of(Color.GREEN)));
		assertEquals(Arrays.asList("bb", "b"), g.getDescendants("a", EnumSet.of(Color.BLUE)));
	}

	@Test
	public void toposortEmpty() {
		final StringMG g = new StringMG();
		assertEquals(0, g.reverseTopologicalSort().size());
	}

	@Test
	public void toposortNoEdges() {
		final StringMG g = new StringMG();
		g.addVertex("a");
		g.addVertex("b");
		g.addVertex("c");
		assertEquals(Arrays.asList("a", "b", "c"), g.reverseTopologicalSort());
	}

	@Test
	public void toposortLine() {
		final StringMG g = new StringMG();
		g.addEdge("a", "b", EnumSet.of(Color.RED));
		g.addEdge("b", "c", EnumSet.of(Color.GREEN));
		assertEquals(Arrays.asList("c", "b", "a"), g.reverseTopologicalSort());
		assertEquals(Arrays.asList("c", "b", "a"), g.reverseTopologicalSort(EnumSet.of(Color.RED, Color.GREEN)));
		assertEquals(Arrays.asList("b", "a", "c"), g.reverseTopologicalSort(EnumSet.of(Color.RED)));
		assertEquals(Arrays.asList("a", "c", "b"), g.reverseTopologicalSort(EnumSet.of(Color.GREEN)));
		assertEquals(Arrays.asList("a", "b", "c"), g.reverseTopologicalSort(EnumSet.of(Color.BLUE)));
	}

	private void assertHasVertices(StringMG g, String... vertices) {
		assertEquals(new HashSet<String>(Arrays.asList(vertices)), g.getVertices());
	}

	private void assertHasEdges(StringMG g, String src, String dst, EnumSet<Color> edges) {
		assertEquals(edges, g.getEdgesBetween(src, dst));
		assertTrue(g.getOutgoing(src, edges).containsKey(dst));
		assertTrue(g.getIncoming(dst, edges).containsKey(src));
	}
}
