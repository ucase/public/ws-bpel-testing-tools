package es.uca.webservices.takuan.io;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Test;

/**
 * Tests for the Java <code>tail(1)</code> ports.
 *
 * @author Antonio García Domínguez
 */
public class TailTest {

	@Test
	public void emptyFile() throws Exception {
		assertEquals("", computeLastLine(""));
	}

	@Test
	public void noLineBreaks() throws Exception {
		assertEquals("x", computeLastLine("x"));
	}

	@Test
	public void exactlyAtLF() throws Exception {
		assertEquals("", computeLastLine("x\n"));
	}

	@Test
	public void exactlyAtCR() throws Exception {
		assertEquals("", computeLastLine("x\r"));
	}

	@Test
	public void exactlyAtCRLF() throws Exception {
		assertEquals("", computeLastLine("x\r\n"));
	}

	@Test
	public void twoLines() throws Exception {
		assertEquals("y", computeLastLine("x\ny"));
	}

	@Test
	public void twoLinesAtLF() throws Exception {
		assertEquals("", computeLastLine("x\ny\n"));
	}

	@Test
	public void threeLines() throws Exception {
		assertEquals("z", computeLastLine("x\ry\rz"));
	}

	@Test
	public void largeFile() throws Exception {
		final String line = "I am the very, very, very long line #";
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 10000; ++i) {
			if (i > 0) sb.append("\n");
			sb.append(line + i);
		}
		assertEquals("I am the very, very, very long line #9999", computeLastLine(sb.toString()));
	}

	@Test
	public void largeFileOneLine() throws Exception {
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 10000; ++i) {
			sb.append(" banana");
		}
		final String bananas = sb.toString();
		assertEquals(bananas, computeLastLine(bananas));
	}

	private String computeLastLine(String text) throws IOException {
		final File tmp = File.createTempFile("tail", ".txt");
		final FileWriter fW = new FileWriter(tmp);
		fW.write(text);
		fW.close();

		final String lastLine = Tail.lastLine(tmp);
		tmp.delete();

		return lastLine;
	}
}
