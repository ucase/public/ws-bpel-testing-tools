Takuan
======

This is a short manual on the usage of Takuan, a Daikon [1] front-end
for WS-BPEL compositions. Takuan extracts potential invariants from
WS-BPEL 2.0 compositions by running them on the ActiveBPEL 4.1 engine
against a BPELUnit test suite.

Takuan is licensed under the terms of the GNU General Public License
version 3 or later. For more information about Takuan, please contact
Manuel Palomo-Duarte at <manuel dot palomo duarte at uca dot es> or
Antonio Garcia-Dominguez at <antonio dot garciadominguez at uca dot
es>.

[1]: http://groups.csail.mit.edu/pag/daikon/

Contents
--------

1. Installation and usage
2. Configuration file format
   2.1 Available options
   2.2 Minimal example
   2.3 Multiple experiments
       2.3.1 Multiple YAML documents
       2.3.2 Experiment trees
   2.4 Anchors and references
3. Results
4. Fine-tuning Takuan
   4.1 Process instrumentation
   4.2 Log preprocessing
       4.2.1 Outputs
       4.2.2 Performance
       4.2.3 Mapping schemes
   4.3 Logging
5. Troubleshooting

1. Installation and usage
-------------------------

Installing Takuan is very simple:

1. Unpack the .zip or .tar.gz with the binary distribution for your
   architecture (*-Linux-i386-* for 32-bit systems and *-Linux-amd64-*
   for 64-bit systems) to a directory of your choice.

2. Create a symlink to the bundled takuan script to a directory listed
   in your PATH variable.

For instance, if ~/bin is listed in PATH and we're using a 64-bit system:

    cd ~/bin
    unzip ~/Downloads/takuan-launcher-(version)-dist-amd64-linux.zip
    ln -s ~/bin/takuan-launcher-(version)/takuan

After these commands, we should be able to run Takuan by running:

    takuan (1+ paths to .yaml files...)

For instance, you can try it with the bundled sample experiments:

    takuan sample/sample.yaml

For more help on the valid arguments which you can pass to Takuan, you
can run:

    takuan --help

2. Configuration file format
----------------------------

Takuan uses YAML files to specify the parameters of each
experiment. YAML is a textual format that is easy to read and write by
humans. For an in-depth description of YAML, please consult the
official specification [1], which includes many examples.

2.1 Available options
---------------------

A single .yaml file can contain many YAML documents, separated by
lines with "----". Each YAML document should contain a map, which will
be the root of a "experiment tree". Relative file paths are usually
resolved from the directory of the .yaml file, unless the base
directory has been explicitly set through  the '--baseDir'
command-line flag.

These are the available options:

- bpel (mandatory): path to the WS-BPEL process definition. A default
  value can be set through the command-line '--bpel' flag.

- bpts (mandatory): path to the BPELUnit test suite. A default value
  can be set through the command-line '--bpts' flag.

- dir (mandatory): path to the results directory. Its contents are
  described in section 3.

- dirSuffix (optional): suffix to be added to the name of the results
  directory. This is mostly useful when using an experiment tree: see
  section 2.3 for examples.

- daikonOptions (optional): path to the Daikon options file. For
  details about the syntax of the file and the available options, see
  [2]. By default, the daikon.options file included in the binary
  distribution will be used.

- daikonFlags (optional): list of strings with additional command line
  flags to be passed to Daikon. For details about the available flags,
  see [3]. Please note that if a flag receives an argument, the flag
  itself and its argument should be in separate elements of the
  list. We use a list instead of a simple string so we can safely have
  arguments with spaces in them.

  NOTE: if you use this option in an inner node and one of its
  children, the value set in the children will *extend* the value set
  in the inner node.

- flags (optional): list of strings with additional command line flags
  to be passed to the ActiveBPEL log preprocessor. For more details,
  see section 4. As in daikonFlags, if one of the flags receives an
  argument, you should put the flag and its argument as separate
  elements of the list.

  NOTE: if you use this option in an inner node and one of its
  children, the value set in the children will *extend* the value set
  in the inner node.

- activeBPELPort (optional): port on which the instance of ActiveBPEL
  launched by Takuan should listen. By default, it is 8080.

- bpelUnitPort (optional): port on which BPELUnit should make its
  mockups listen. By default, it is 7777.

- useSimplify (optional): indicates whether Daikon should use Simplify
  to remove redundant and/or contradictory invariants. This is the
  same as adding '--suppress_redundant' to daikonFlags: this is just a
  convenient shortcut.

- children (optional): list of maps with the children experiments of
  this node of the experiment tree. Inner nodes of the experiment tree
  are not run: instead, they provide default values for the parameters
  of their children. Most options are inheritable, except for
  'dirSuffix'.

- id (optional, only available for leaf nodes in the experiment tree):
  string with an unique ID for the node among all the nodes in the
  .yaml file. Currently only used with 'compareWith'.

- compareWith (optional): list of strings with the IDs of the nodes
  whose invariants should be compared against the invariants from this
  composition, using the Daikon diff tool. These IDs should have been
  defined in previous nodes, and these nodes should have the same
  instrumentation options and refer to the same BPEL process definition.

- addLogsFrom (optional): list of strings with the IDs of the nodes
  whose raw logs should be added to those produced by the tests for
  this node. These IDs should have been defined in previous nodes, and
  these nodes should have the same instrumentation options and refer
  to the same BPEL process definition. This option is mostly useful
  when trying Daikon with increasingly large test suites that extend
  each other.

  NOTE: if the referenced node also uses addLogsFrom, the
  corresponding ActiveBPEL output directories will be added as
  well. This will be repeated as many times as required. Duplicate
  mentions of the same ActiveBPEL output directory will be implicitly
  removed.

  NOTE: if you need accurate execution times or memory usage, please
  avoid this option, as it will count ActiveBPEL startup/teardown
  several times and may not have the same memory usage as running all
  the tests in sequence.

- addRawDTracesFrom (optional): list of strings with the IDs of the
  nodes whose raw dtraces (before flattening) should be added to those
  produced by the tests for this node. In addition, Takuan will merge
  the comparability graphs saved during their execution with the one
  produced from the tests in this configuration.

  Same restrictions and notes as those of 'addLogsFrom' apply. In
  addition, if a configuration is reached through both 'addLogsFrom'
  and 'addRawDTracesFrom', 'addLogsFrom' will take preference. To
  avoid confusions, it is forbidden to refer to the same ID using
  'addLogsFrom' and 'addRawDTracesFrom' in the same configuration.

  NOTE 1: if you need accurate execution times or memory usage, please
  avoid this option. See note in 'addLogsFrom' above.

  NOTE 2: if you need accurate execution coverage reports, please avoid
  this option. These can only be generated from the actual ActiveBPEL
  execution logs. Use 'addLogsFrom' instead (see the previous note).

- useAsDataSrc (optional): path to a file which will be copied to
  'data.src' in the directory of the '.bpts' file. This is mostly useful
  when reusing the same template-based BPTS with different datasets.

- maxInstrumentationDepth (optional): number of levels of nested
  activities which should be instrumented by default if
  'instrumentSequencesByDefault' is true. If unset or set to 0, all
  levels will be instrumented. <if>/<elseif>/<else> are considered to
  be at the same nesting level.

- instrumentSequencesByDefault (optional): if unset or set to 'true',
  all sequences will be instrumented, unless instructed otherwise
  through the uca:instrument attribute or the
  'maxInstrumentationDepth' option above. If set to 'false', only
  those sequences or activity containers with 'uca:instrument' set to
  'yes' will be instrumented.

- instrumentVariablesByDefault (optional): if unset or set to 'true',
  all variables at every instrumented sequence will be instrumented,
  unless 'uca:instrument' has been set to 'no' for them. Otherwise,
  only those variables with 'uca:instrument' set to 'yes' will be
  instrumented.

- inv2XML (optional): if set to 'true', a 'daikon.xml' file with the
  invariants produced by Daikon in XML format will be generated at the
  output directory. By default, this file is not produced, as it can
  take quite a while to generate for large numbers of invariants.

- stats (optional): if set to true, a 'stats.txt' file with statistics
  about the invariants produced by Daikon will be generated at the
  output directory. By default, this file is not produced, as it file
  may be expensive to produce if there are many (30k+) invariants
  being generated.

- coverage (optional): if set to true, a set of test coverage reports
  will be generated at the output directory. By default it will not be
  generated, to speed up the process and save disk space.

- keepExecutionLogs (optional): if set to true, the 'activebpel'
  subdirectory of the output directory will be preserved. This
  directory contains all the raw execution logs and the server log for
  the BPEL engine. Otherwise, this directory will be deleted as soon
  as it is no longer necessary, to save disk space.

- keepDTraces (optional): if set to true, the 'takuan' subdirectory of
  the output directory will be preserved. This directory contains the
  raw and flattened .dtrace files sent to Daikon. Otherwise, this
  directory will be deleted as soon as it is no longer necessary, to
  save disk space.

- runDaikon (optional): if set to false, Daikon will not be run for
  that configuration. This is useful in combination with 'addLogsFrom'
  or 'addRawDTracesFrom' for processing large test suites in batches,
  in order to avoid running out of disk space. We can process all logs
  and generate partial comparability graphs before the last one
  without running Daikon, and then run Daikon on the last one, which
  will aggregate all logs and use the full comparability graph.

- daikonTimeoutInMinutes (optional): integer value with the maximum
  number of minutes to wait for Daikon to finish inferring all
  invariants before killing the process. Useful when we want to avoid
  wasting a lot of time in a particular configuration which uses up
  too much memory.

Please note that all options which use paths can use either absolute
or relative paths. Relative paths are resolved from the directory
containing the .yaml file to be loaded.

2.2 Minimal example
-------------------

The simplest configuration that can be written looks like this:

  bpel: my.bpel
  bpts: my.bpts
  dir: results

This configuration will run the WS-BPEL process definition at (.yaml
directory)/my.bpel against the BPELUnit test suite located (.yaml
directory) at my.bpts. Results and logs will be placed in (.yaml
directory)/results.

2.3 Multiple experiments
------------------------

A single invocation of 'takuan' can run many experiments by combining
these approaches:

1. Passing more than one .yaml file from the command line.

2. Including multiple YAML documents in a .yaml file.

3. Using 'children' to construct an experiment tree.

We recommend running all experiments at once from a single invocation
of Takuan, as this will allow Takuan to do some useful
optimizations. For instance, if multiple experiments share the same
.bpel file, Takuan will reuse the instrumented .bpel and the raw
.decls for Daikon. In addition, if multiple experiments share the same
.bpel and .bpts files, they will share the ActiveBPEL logs, saving
considerable time.

The following subsections will explain how to use approaches 2 and 3
above.

[1]: http://yaml.org/
[2]: http://groups.csail.mit.edu/pag/daikon/download/doc/daikon.html#List-of-configuration-options
[3]: http://groups.csail.mit.edu/pag/daikon/download/doc/daikon.html#Running-Daikon

2.3.1 Multiple YAML documents
-----------------------------

YAML allows for writing several documents in a single .yaml file. Each
document must be separated from the others by a single line with
"----" on it. For instance, this would run two experiments on the same
.bpel file, but with different .bpts files:

    bpel: f.bpel
    bpts: g.bpts
    dir: results-g
    -----
    bpel: f.bpel
    bpts: h.bpts
    dir: results-h

Note: it is forbidden to use the same result directory (taking into
account 'dirSuffix') in several experiments in the same .yaml
file. This ensures that an experiment in a .yaml file will not clobber
the results of a previous experiment of the same .yaml file.

Note 2: it is also forbidden to use the same ID in several experiments
in the same .yaml file. This ensures that the entries in the
'compareWith' attributes are unambiguous inside a single .yaml file.

2.3.2 Experiment trees
----------------------

Instead of a single experiment, a YAML document can contain a tree of
experiments. Inner nodes in the tree are used to propagate default
option values to its children. This is useful when running multiple
experiments on the same .bpel, but with different .bpts or different
flags for Daikon and/or the preprocessor.

A YAML document using experiment trees that is equivalent to that in
section 2.3.1 would be:

    bpel: f.bpel
    dir: results
    children:
      - bpts: g.bpts
        dirSuffix: -g
      - bpts: h.bpts
        dirSuffix: -h

2.4 Anchors and references
--------------------------

YAML has a powerful feature called "anchors". An anchor allows for
referencing a separate part of the document, so we don't have to
repeat it again and again. For instance, let us assume that we need to
run the same subexperiments for two .bpts files being run against the
same .bpel file. Originally, we would write it like this:

    bpel: f.bpel
    children:
      - bpts: g.bpts
        dirSuffix: -g
        children:
          - dirSuffix: -without-simplify
	  - useSimplify: true
	    dirSuffix: -with-simplify
      - bpts: h.bpts
        dirSuffix: -h
	children:
	  - dirSuffix: -without-simplify
	  - useSimplify: true
	    dirSuffix: -with-simplify

We've repeated part of the document in two places. This is cumbersome
to read and to write. Let's try again, this time with anchors and
references:

    bpel: f.bpel
    children:
      - bpts: g.bpts
        dirSuffix: -g
        children: &variations
          - dirSuffix: -without-simplify
	  - useSimplify: true
	    dirSuffix: -with-simplify
      - bpts: h.bpts
        dirSuffix: -h
	children: *variations

This is shorter, easier to read and easier to modify. We avoid
repeating the variations for each experiment by creating the anchor
&variations and then referencing it whenever we need it with
*variations.

3. Results
----------

Successful completion of an experiment will result in the creation and
population of the specified result directory with a set of files. We
will briefly describe their contents below.

IMPORTANT: if the directory already existed, it will be *deleted*
along with all its files before being recreated and repopulated. This
ensures that subsequent runs of the same experiments are not polluted
by previous results.

- bpelunit.xml: BPELUnit test report in XML format. We should ensure
  that all tests pass before trying to infer invariants from the logs.

- daikon.out: contents of the standard output and standard error
  streams while Daikon was being run. If Daikon finished its execution
  successfully, it will contain the produced invariants in a textual
  format.

- daikon.inv.gz: invariants produced by Daikon, serialized and
  compressed with gzip.

- daikon.xml (when 'inv2XML' is set to 'true'): invariants produced by
  Daikon, in an XML-based format.

- inspected.decls: raw .decls generated by Takuan for Daikon. This
  file is not yet usable by Daikon: it needs to be preprocessed using
  the execution logs and a specific mapping scheme.

- instrumented.bpel: instrumented version of the WS-BPEL composition
  which logs the values of every interesting variable and variable
  property in every interesting program point. To know more about how
  to specify "interesting" variables and program points, please see
  section 4.1.

- pathCoverage*.log (when 'coverage' is set to 'true'): XML documents
  with coverage reports about all available paths, executed paths or
  not executed paths.

- takuan-perl.log: a log with the output produced while runing the
  Perl-based ActiveBPEL log preprocessor. For more information on how
  to fine-tune the log preprocessor, please refer to section 4.2.

- stats.txt: if 'stats' is set to 'true', this file will contain
  several statistics produced by running Daikon's diff tool with the
  -s flag on daikon.inv.gz. For details, please check Daikon's
  documentation:

  http://groups.csail.mit.edu/pag/daikon/download/doc/daikon.html#Invariant-Diff

- (ID)_diff.txt, (ID)_diffU.txt and (ID)_stats.txt: outputs produced
  by comparing with Daikon's diff tool the inv.gz of the current node
  with the inv.gz of the experimented node using ID as an identifier
  (see attributes 'id' and 'compareWith' above).  For details about
  the format of these files, please check Daikon's documentation:

  http://groups.csail.mit.edu/pag/daikon/download/doc/daikon.html#Invariant-Diff

- text-comparisons.csv: created if 'compareWith' has been set. It is a
  text file in Comma-Separated Values (CSV) format with additional
  comparison results. Among other information, it includes comparisons
  between the number of invariants, the results produced by Daikon's
  diff with and without the -u option, whether the raw and/or flat
  traces were the same and the number of total program points, program
  points with "interesting" invariants or program points with any
  invariants at all.

  The first line includes a header describing each of the fields, and
  each of the following lines is a comparison between the results of
  the experiment node and those referred to by the 'compareWith'
  attribute. Fields are separated by commas (",").

- activebpel (if 'keepExecutionLogs' is set to 'true'): this is the
  main directory for the ActiveBPEL instance started by Takuan. Its
  contents are as follows:

    - bpr/aeEngineConfig.xml: configuration file used by
      ActiveBPEL. This file is automatically generated by Takuan.

    - bpr/work: temporary work directory for unpacking .bpr deployment
      archives for ActiveBPEL.

    - deployment-logs: deployment logs for the WS-BPEL process under
      test. If your tests fail, please make sure that your .bpel
      process has been deployed correctly here.

    - jetty.log: log produced while running the .bpts file against the
      .bpel process definition in ActiveBPEL.

    - process-logs/*.log: raw execution logs produced by each test
      case in the .bpts. These are the execution logs that are
      preprocessed in order to generate input files for Daikon.

- takuan (if 'keepDTraces' is set to 'true'): this is the output
  directory for the Perl preprocessor.

    - N.dtrace, where N is a positive integer: raw Daikon .dtrace
      produced from activebpel/process-logs/N.log. Still not usable by
      Daikon: needs to be mapped using a certain scheme.

    - N.flat.dtrace, where N is a positive integer: mapped Daikon
      .dtrace which can be used by Daikon.

    - coverage.log: XML coverage report on the sentence coverage of
      the BPELUnit test suite.

    - inspected.cmp.decls: if comparability index generation is
      enabled, it is a decorated version of inspected.decls with the
      computed comparability indices. It is still not usable by Daikon.

    - inspected.flat.decls: mapped version of inspected.cmp.decls (or
      inspected.decls if comprability index generation is
      disabled). This is usable by Daikon.

Takuan will also generate several files in the directory from which it
was launched:

- takuan-launcher.log: textual log of the entire Takuan invocation.

- performance.csv: Comma-Separated Values file with performance
  metrics for each configuration, including wall time, CPU time and
  memory required by each step in Takuan's workflow and some useful
  subtotals. These values do not include the memory used by Simplify
  or the Perl trace preprocessor. In Linux, the wrapper Bash script
  used to launch the Perl trace preprocessor prints its maximum
  resident set size (RSS, see ps(1)) as the last line of
  takuan-perl.log.

4. Fine-tuning Takuan
---------------------

Takuan can run with the minimal configuration described in section
2.2. However, users may want to customize the way some of the steps
performed by Takuan are performed. The following subsections describe
how to customize some of these steps.

4.1 Process instrumentation
---------------------------

Normally, Takuan considers every program point and variable to be
interesting for the purposes of instrumentation. This may result in a
very high usage of memory or CPU time for large compositions. In that
case, we can limit the program points and variables to be
instrumented.

To do so, we can annotate the WS-BPEL with a few extension
attributes. These attributes belong to the namespace
"http://www.uca.es/xpath/2007/11": you can easily declare it by adding
'xmlns:uca="http://www.uca.es/xpath/2007/11"' to your <bpel:process>
element. You will have to add an appropriate <bpel:extension> element
to your <bpel:extensions> section as well (which should be the first
child of the root <bpel:process> element), like this:

   <bpel:extensions>
      <bpel:extension mustUnderstand="no" namespace="http://www.uca.es/xpath/2007/11"/>
   </bpel:extensions>

These are the available options for the applicable BPEL element types:

- <process>:

    - uca:instrumentSequencesByDefault (default: "yes"): if "yes", all
      sequences will be instrumented as program points. If "no", only
      those sequences which are explicitly set to be instrumented are
      considered as program points.

      You can also use the 'instrumentSequencesByDefault' option in
      the .yaml file, if you prefer to use the original WS-BPEL file.

    - uca:instrumentVariablesByDefault (default: "yes"): if "yes", all
      variables and their properties will be instrumented for
      inspection before and after each program point. If "no", only
      those variables which are explicitly set to be instrumented are
      considered as program points.

      You can also use the 'instrumentVariablesByDefault' option in
      the .yaml file, if you prefer to use the original WS-BPEL file.

    - uca:maxInstrumentationDepth (default: 0): if N > 0, it will only
      instrument the program points in the N first levels of the DOM
      tree of the WS-BPEL process composition.

      You can also use the 'maxInstrumentationDepth' option in the
      .yaml file, if you prefer to use the original WS-BPEL file.

- <sequence>, <variable>:

    - uca:instrument (default: not set): if "yes", the program point
      or variable will be instrumented. If "no", it will not be
      instrumented. If not set, it will use the default value from
      uca:instrumentSequencesByDefault or
      uca:instrumentVariablesByDefault, respectively.

Please note that the instrumentation process will wrap conditional
branches, loops and handlers which contain only one simple activity
with new <sequence> elements. If you'd like to set instrumentation
options on these activities, you will need to wrap them yourself with
<sequence> elements and annotate the new <sequence> element.

4.2 Log preprocessing
---------------------

Before Takuan can launch Daikon, it needs to preprocess the raw .decls
file and the raw ActiveBPEL execution logs. There are many variables
involved in this process. Fine-tuning the Perl-based preprocessor
(bundled as bin/takuan-perl in the binary distribution) requires
passing the appropriate flags through the 'flags' key in the YAML
document.

We will now list some of the available options. For more information,
please run 'bin/takuan-perl' and read the usage message. Please note
that some flags should only be used when invoking "takuan-perl"
directly, and that they should not be used in the YAML documents
passed to "takuan".

4.2.1 Outputs
-------------

These flags control the format and location of the outputs produced by
"takuan-perl":

- --index-flattening: uses matrix slicing (also known as index
    flattening) as the mapping scheme, instead of the default matrix
    flattening scheme. Each scheme has its own advantages and
    disadvantages: for more information, please refer to section
    4.2.3.

- --simplify: calls Simplify from Daikon. This flag is only to be used
    when invoking "takuan-perl" directly, and not from the main
    "takuan" launcher.

- --output-dir DIR: sets the output directory. This flag is only to be
    used when calling "takuan-perl" directly and not through
    "takuan". Please use the 'dir' YAML configuration key instead of
    this flag.

- --metrics: enables the generation of metrics about the number of
    program points and variables generated. This flag is always passed
    by "takuan" to "takuan-perl", so you should never need to use it.

- --coverage-plain-text: switches coverage.log to a textual format.

4.2.2 Performance
-----------------

These flags can disable or enable certain optimizations in
"takuan-perl" which can affect Daikon's performance. Some of the
optimizations are safer than others.

- --disable-comparability: disables the generation of Daikon
    comparability indices. This implies --disable-filter-unused as
    well. Normally it is not a good idea to use this option unless for
    debugging or performance comparison purposes, as the memory and
    CPU time required by Daikon will sharply increase.

- --disable-filter-unused: variables which are not used at a certain
    program point will be preserved, instead of being removed as
    usual. Again, this option should normally not be used unless we
    suspect there is a bug or we want to compare Takuan's performance
    with and without this option.

- --disable-xml-schema: will not annotate variables with additional
    type information extracted from their XML Schema
    declarations. This additional information is normally used by our
    patched version of Daikon to remove redundant invariants which are
    already described in the XML Schema declarations. You should
    normally never need to use this option.

- --discard-empty-lists: if a certain list variable is always empty,
    discard it. This option can save considerable amounts of memory
    and time, at the cost of limiting the results produced by Daikon.

- --max-log-size BYTES: changes the maximum file size for acceptable
    execution logs. Normally it is set to 20MiB. This ensures that we
    skip overly large logs, which are usually produced by infinite
    loops and other error conditions in the program.

- --ncpu N: uses several CPUs to speed up some of the processing
    steps. When using this flag, setting it to (number of processes) +
    1 is a good start.

4.2.3 Mapping schemes
---------------------

The Perl preprocessor needs to map the contents of the XML-based
tree-like variables into the scalar and linear sequence variables
supported by Daikon. However, there are many ways to do so. Each of
these ways is what we call a "mapping scheme".

Takuan supports two schemes: "matrix slicing" (also called "index
flattening" in some of our papers) and "matrix flattening" (the
default). Suppose we had this XML document:

    <a>
      <b>
        <c>1</c>
        <c>2</c>
      </b>
      <b>
        <c>3</c>
        <c>4</c>
      </b>
    </a>

The raw variable declaration would be a[].b[].c[]: we have one or more
<a> elements containing one or more <b> elements containing one or
more <c> elements. However, we can only use unidimensional sequences
in Daikon, so we will have to flatten it.

Matrix slicing simply generates one variable for every combination of
indices, except the last one. In this case, it would generate
a[1].b[1].c[] (containing the list [1 2]) and a[1].b[2].c[] (with the
list [3 4]).

Matrix flattening collects all values for every combination of indices
into a single sequence, a.b.c[], containing [1 2 3 4].

Matrix slicing can generate invariants for specific subsets of <c>
elements, but it can generate many more variables than matrix
flattening, and for that reason it will usually be more expensive to
use.

Matrix flattening can generate invariants over all values of <c>, and
will only generate one variable, regardless of the number of <a> and
<b> elements, and it will use less memory and CPU time.

Users will need to pick one of these mapping schemes depending on the
desired invariants and performance: there is no clear winner in this
case.

4.3 Logging
-----------

In order to limit or expand the logging messages generated by Takuan,
you can edit the log4j.properties file bundled in
lib/takuan-launcher-(version).jar to customize what to log and where
to log it.

For more information on Log4J and the log4j.properties format, please
check the short introduction at [1].

[1]: http://logging.apache.org/log4j/1.2/manual.html

5. Troubleshooting
------------------

- takuan-perl exits with code 137: takuan-perl has been killed by the
  operating system, as it was using too much memory. Try setting
  max-log-size to a lower value or limiting the variables and program
  points to be instrumented.

- Daikon says "No return from procedure observed N times. Unmatched
  entries are ignored!": normally, these entries do not have a
  matching :::EXIT1 trace as the execution of the program point was
  finished prematurely, due to a fault or some other sort of external
  event.

  Takuan uses this behaviour by design: it is assumed that :::ENTER
  invariants should behave like preconditions, and thus they should
  only describe the successful cases in which :::EXIT1 is reached. In
  short: this is not a bug, but a feature :-).

- Daikon crashes with "The specified size exceeds the maximum
  representable size": the value set in '-XmxYm' in 'DAIKON_JVM_FLAGS'
  is set too high for your architecture. 32-bit JVMs can only address
  about 2048MB of RAM.

- Takuan runs out of disk space: ActiveBPEL logs and the temporary
  files used by Daikon can take up large amounts of disk space. This
  is usually a problem when trying to run thousands of test cases.

  To work around this problem, you should split your tests over
  several executions, and aggregate them using the 'addDTracesFrom' or
  'addLogsFrom' options. If you're not concerned about having accurate
  coverage metrics, you should use 'addDTracesFrom'. If you do not
  want to run Daikon again and again, but rather only for the full
  test suite, combine it with the 'runDaikon' option.

- Takuan crashes with an "Address already in use" error: there is some
  program already listening at the ports used for Takuan or
  ActiveBPEL. Please disable those services before trying again.

- Takuan crashes with a java.lang.OutOfMemoryError: the JVM ran out of
  memory. You need to set higher memory usage limits for the heap and
  the stack using -XmxYm (where Y is the maximum amount of memory to
  be used for the heap, in MiB) and -XssYm (where Y is the maximum
  amount of memory to be used for the stack, in MiB). Good default
  values are -Xmx1024m and -Xss1m for small and medium cases. Large
  cases might require -Xmx4096m and -Xss2m or even -Xmx8192 and
  -Xss4m. You may also need to increase the value of
  '-XX:MaxPermSize'.

  In addition, if you're using a recent 64-bit JVM prior to Java 6
  Update 23, you might want to use the -XX:+UseCompressedOops option,
  which compresses 64-bit pointers into 32-bit shifts whenever
  possible. It is enabled by default on later versions of the OpenJDK
  and Oracle JREs.

  Yet another option is to enable the
  '-XX:+HeapDumpOnOutOfMemoryError' flag so a heap dump is produced
  when this error happens. This heap dump can be then examined with
  tools such as VisualVM (http://visualvm.java.net/) or the Eclipse
  Memory Analyzer (http://www.eclipse.org/mat/).

  To set these options, you'll need to edit the 'JVM_FLAGS' variable
  inside the main 'takuan' shell script. If it's Daikon the one
  running out of memory, you should change 'DAIKON_JVM_FLAGS' instead.
