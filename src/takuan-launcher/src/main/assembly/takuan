#!/bin/bash

# Bash wrapper around the Takuan launcher
# Copyright (C) 2012 Antonio Garcia-Dominguez
# Licensed under GPLv3 or later

set -o errexit
set -o allexport

if [ -z "$JAVA_HOME" ]; then
    echo "The JAVA_HOME environment variable is not set to the path to your Java installation."
    echo "Please set it in your ~/.bashrc or ~/.profile files and try again."
    exit 1
fi

BASEDIR=$(dirname "$(readlink -f "$0")")
MAINCLASS=${mainclass}
SIMPLIFY="$BASEDIR/bin/simplify"
TAKUAN_PERL="$BASEDIR/bin/takuan-perl-wrapper.sh"
DAIKON_OPTIONS="$BASEDIR/daikon.options"

# Set up the classpath (main .jar should be first)
CLASSPATH=$JAVA_HOME/lib/tools.jar:$BASEDIR/lib/${project.artifactId}-${project.version}.jar
for lib in "$BASEDIR/lib"/*.jar; do
    CLASSPATH=$CLASSPATH:$lib
done

JVM_FLAGS="-Xmx512m -Xss8m"
DAIKON_JVM_FLAGS="-Xmx2048m -Xss20m -Dsimplify.path=$SIMPLIFY"

# Start the application
java $JVM_FLAGS \
    -Ddefault.daikon.options="$DAIKON_OPTIONS" \
    -Ddaikon.jvm.flags="$DAIKON_JVM_FLAGS" \
    -Dexecutable.perl="$TAKUAN_PERL" \
    "$MAINCLASS" $@
