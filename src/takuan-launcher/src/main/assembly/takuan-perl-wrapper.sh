#!/bin/bash

# Memory usage watcher wrapper for takuan-perl
# Copyright (C) 2012 Antonio García-Domínguez
# Licensed under the GPLv3

# Run Perl and watch its RSS in the background
cat | "$(dirname "$(readlink -f "$0")")"/takuan-perl "$@" &
PERL_PID=$!

MAX_RSS=0
while true
do
    RSS=$(ps --no-heading -o rss "$PERL_PID") || break
    if [ "$RSS" -gt "$MAX_RSS" ]; then
	MAX_RSS=$RSS
    fi
    sleep 0.1s
done

echo "Maximum RSS: $MAX_RSS KB"
wait "$PERL_PID"
echo "takuan-perl exited with status code $?"
