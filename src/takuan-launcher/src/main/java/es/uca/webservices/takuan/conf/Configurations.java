package es.uca.webservices.takuan.conf;

import java.io.File;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Graph of experiment configurations, loaded from a series of YAML documents.
 * These sequences should all have different output directories and IDs. IDs are
 * optional for each node.
 */
public class Configurations {

	private Map<String, ConfigurationNode> id2ConfigNode;
	private ConfigurationDependencyGraph dependencyGraph;
	private File file;

	public Configurations(List<ConfigurationNode> configs) throws ConfigurationException {
		validateIDs(configs);
		computeDependencyGraph(configs);
	}

	/**
	 * Returns the dependency graph connecting all configurations. Vertices
	 * in this graph should be traversed in reverse topological order. This
	 * will ensure that every dependency of a configuration node is run
	 * before it.
	 */
	public ConfigurationDependencyGraph getDependencyGraph() {
		return dependencyGraph;
	}

	/**
	 * Returns the loaded configurations in reverse topological order according
	 * to the configuration dependency graph. 
	 */
	public List<ConfigurationNode> getConfigurations() {
		return dependencyGraph.reverseTopologicalSort();
	}

	/**
	 * If the YAML documents were loaded from a file, returns the file.
	 * Otherwise, returns <code>null</code>.
	 */
	public File getFile() {
		return file;
	}

	/**
	 * Changes the file from which this sequence of YAML documents was loaded. 
	 */
	void setFile(File f) {
		this.file = f;
	}

	private void validateIDs(List<ConfigurationNode> configs)
			throws ConfigurationException {
		id2ConfigNode = new HashMap<String, ConfigurationNode>();

		final Set<File> outputDirectories = new HashSet<File>();

		for (ConfigurationNode c : configs) {
			if (!outputDirectories.add(c.getDir())) {
				throw new ConfigurationException(String.format(
					"There is another configuration in %s using %s as" +
					" an output directory. To avoid data loss, please change it.",
					file, c.getDir()));
			}
			if (c.getId() != null && id2ConfigNode.put(c.getId(), c) != null) {
				throw new ConfigurationException(
					String.format("There are two or more configurations in %s with the ID %s",
						file, c.getId()));
			}
		}

		for (ConfigurationNode c : configs) {
			// Ensure that all referenced IDs are defined and that they have compatible options
			final InstrumentationOptions myOptions = new InstrumentationOptions(c);
			checkIDReferences(c, myOptions, c.getCompareWith());
			checkIDReferences(c, myOptions, c.getAddLogsFrom());
			checkIDReferences(c, myOptions, c.getAddRawDTracesFrom());
		}
	}

	private void computeDependencyGraph(List<ConfigurationNode> configs) throws ConfigurationException {
		dependencyGraph = new ConfigurationDependencyGraph();

		// Add most vertices and edges here
		for (ConfigurationNode c : configs) {
			dependencyGraph.addVertex(c);
			for (String compareWithID : c.getCompareWith()) {
				dependencyGraph.addEdge(c, id2ConfigNode.get(compareWithID), EnumSet.of(ConfigurationDependencyType.COMPARE_WITH));
			}
			for (String addLogsFrom : c.getAddLogsFrom()) {
				dependencyGraph.addEdge(c, id2ConfigNode.get(addLogsFrom), EnumSet.of(ConfigurationDependencyType.ADD_LOGS));
			}
			for (String addRawDTraces : c.getAddRawDTracesFrom()) {
				dependencyGraph.addEdge(c,  id2ConfigNode.get(addRawDTraces), EnumSet.of(ConfigurationDependencyType.ADD_RAW_DTRACES));
			}
		}
		if (dependencyGraph.isCyclic()) {
			throw new ConfigurationException("There is a cycle in the configuration node dependency graph");
		}


		// Add edges for the "same logs" constraints - these shouldn't change the
		// topological order, but they will be useful to keep track of when we should
		// remove the directory
		List<ConfigurationNode> topoOrder = dependencyGraph.reverseTopologicalSort();
		Map<ExecutionLogOptions, ConfigurationNode> execLogOptionsMap = new HashMap<ExecutionLogOptions, ConfigurationNode>();
		for (ConfigurationNode c : topoOrder) {
			final ExecutionLogOptions execLogOptions = new ExecutionLogOptions(c);
			if (execLogOptionsMap.containsKey(execLogOptions)) {
				dependencyGraph.addEdge(c, execLogOptionsMap.get(execLogOptions), EnumSet.of(ConfigurationDependencyType.SAME_LOGS));
			}
			else {
				execLogOptionsMap.put(execLogOptions, c);
			}
		}

	}

	private void checkIDReferences(ConfigurationNode c, final InstrumentationOptions myOptions, final List<String> references)
		throws ConfigurationException
	{
		for (String compareWithID : references) {
			if (!id2ConfigNode.containsKey(compareWithID)) {
				throw new ConfigurationException("Configuration '" + c.getId() + "' referenced an ID which does not exist: " + compareWithID);
			}
			final ConfigurationNode other = id2ConfigNode.get(compareWithID);
			if (!myOptions.equals(new InstrumentationOptions(other))) {
				throw new ConfigurationException("Configuration '"
						+ c.getId() + "' cannot be compared with configuration '"
						+ compareWithID + "', as it has different instrumentation options");
			}
		}
	}

}