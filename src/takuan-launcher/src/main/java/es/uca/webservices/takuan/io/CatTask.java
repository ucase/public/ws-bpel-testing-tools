package es.uca.webservices.takuan.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Task which dumps the contents of a sequence of text files through
 * an OutputStream, adding blank lines between one file and the next.
 * After all files have been dumped, it closes the stream.
 *
 * @author Antonio García-Domínguez
 */
public class CatTask extends OutputStreamRunnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(CatTask.class);
	private final File[] files;

	public CatTask(String[] paths) {
		files = new File[paths.length];
		for (int i = 0; i < files.length; ++i) {
			files[i] = new File(paths[i]);
		}
	}

	public CatTask(File[] files) {
		this.files = new File[files.length];
		for (int i = 0; i < this.files.length; ++i) {
			this.files[i] = files[i];
		}
	}

	@Override
	public void run() {
		final PrintStream os = new PrintStream(getOutputStream());
		try {
			for (File f : files) {
				if (Thread.interrupted()) {
					throw new InterruptedException();
				}

				BufferedReader is = null;
				try {
					is = new BufferedReader(new FileReader(f));
					String line;
					while ((line = is.readLine()) != null) {
						os.println(line);
						if (Thread.interrupted()) {
							throw new InterruptedException();
						}
					}
				} catch (IOException e) {
					LOGGER.error("I/O exception while concatenating a file to a stream", e);
				} finally {
					if (is != null) {
						try {
							is.close();
						} catch (IOException e) {
							LOGGER.error("I/O exception while closing a file", e);
						}

						os.println();
					}
				}
			}
		} catch (InterruptedException ex) {
			LOGGER.warn("Interrupted while concatenating files", ex);
		} finally {
			os.close();
		}
	}

}
