package es.uca.webservices.takuan.conf;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.uca.webservices.bpel.bpelunit.CustomizedRunner;

/**
 * <p>
 * Launch configuration to be used in Takuan. A launch configuration can contain
 * other configurations in a tree-like structure. Children configurations
 * inherit most of their unset attributes from their parents, except for
 * <code>dirSuffix</code> (by design).
 * </p>
 * 
 * <p>
 * Copyright (C) 2011-2012 Antonio García-Domínguez.
 * </p>
 * 
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * </p>
 * 
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * 
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a href="http://www.gnu.org/licenses/">here</a>.
 * </p>
 */
public final class ConfigurationNode implements Cloneable {

	private File bpel, bpts, outputDirectory, daikonOptions, useAsDataSrc;
	private List<String> analyzerFlags, daikonFlags;
	private List<ConfigurationNode> subtests;
	private ConfigurationNode parent;
	private String dirSuffix, id;
	private Integer activeBPELPort, bpelUnitPort, maxInstrumentationDepth, daikonTimeoutInMinutes;
	private Boolean useSimplify, instrumentVariablesByDefault, instrumentSequencesByDefault, inv2XML, generateStats, generateCoverage, keepExecLogs, keepDTraces, runDaikon;
	private List<String> compareWith, addLogsFrom, addRawDTracesFrom;

	public File getBpel() {
		return bpel != null ? bpel : (parent != null ? parent.getBpel() : null);
	}

	public void setBpel(File bpel) {
		this.bpel = bpel;
	}

	public File getBpts() {
		return bpts != null ? bpts : (parent != null ? parent.getBpts() : null);
	}

	public void setBpts(File bpts) {
		this.bpts = bpts;
	}

	public File getDir() {
		File baseDir = outputDirectory != null ? outputDirectory : (parent != null ? parent.getDir() : null);
		if (baseDir != null) {
			return new File(baseDir.getPath() + (dirSuffix != null ? dirSuffix : ""));
		} else {
			return null;
		}
	}

	public void setDir(File outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	public File getDaikonOptions() throws IOException {
		if (daikonOptions != null) {
			return daikonOptions;
		}
		else if (getParent() != null) {
			File parentDaikonOptions = getParent().getDaikonOptions();
			if (parentDaikonOptions != null) {
				return parentDaikonOptions;
			}
		}
		return null;
	}

	public void setDaikonOptions(File daikonOptions) {
		this.daikonOptions = daikonOptions;
	}


	public File getUseAsDataSrc() {
		return useAsDataSrc != null ? useAsDataSrc : (parent != null ? parent.getUseAsDataSrc() : null);
	}

	public void setUseAsDataSrc(File useAsDataSrc) {
		this.useAsDataSrc = useAsDataSrc;
	}

	public List<String> getFlags() {
		List<String> flags = new ArrayList<String>();
		if (parent != null) {
			flags.addAll(parent.getFlags());
		}
		if (analyzerFlags != null) {
			flags.addAll(analyzerFlags);
		}
		return flags;
	}

	public void setFlags(List<String> flags) {
		this.analyzerFlags = flags;
	}

	public List<String> getDaikonFlags() {
		List<String> flags = new ArrayList<String>();
		if (parent != null) {
			flags.addAll(parent.getDaikonFlags());
		}
		if (daikonFlags != null) {
			flags.addAll(daikonFlags);
		}
		return flags;
	}

	public void setDaikonFlags(List<String> flags) {
		this.daikonFlags = flags;
	}

	public List<ConfigurationNode> getChildren() {
		return subtests;
	}

	public void setChildren(List<ConfigurationNode> subtests) {
		this.subtests = subtests;
	}

	public String getDirSuffix() {
		return dirSuffix;
	}

	public void setDirSuffix(String dirSuffix) {
		this.dirSuffix = dirSuffix;
	}

	public Integer getActiveBPELPort() {
		if (activeBPELPort != null) {
			return activeBPELPort;
		}
		else if (getParent() != null) {
			Integer parentPort = getParent().getActiveBPELPort();
			if (parentPort != null) {
				return parentPort;
			}
		}
		return CustomizedRunner.DEFAULT_ENGINE_PORT;
	}

	public void setActiveBPELPort(Integer activeBPELPort) {
		this.activeBPELPort = activeBPELPort;
	}

	public Integer getBpelUnitPort() {
		if (bpelUnitPort != null) {
			return bpelUnitPort;
		}
		else if (getParent() != null) {
			Integer parentPort = getParent().getBpelUnitPort();
			if (parentPort != null) {
				return parentPort;
			}
		}
		return CustomizedRunner.DEFAULT_BPELUNIT_PORT;
	}

	public void setBpelUnitPort(Integer port) {
		this.bpelUnitPort = port;
	}

	public Boolean getUseSimplify() {
		if (useSimplify != null) {
			return useSimplify;
		}
		else if (getParent() != null) {
			Boolean parentUseSimplify = getParent().getUseSimplify();
			if (parentUseSimplify != null) {
				return parentUseSimplify;
			}
		}
		return false;
	}

	public void setUseSimplify(Boolean useSimplify) {
		this.useSimplify = useSimplify;
	}

	public Boolean getInv2XML() {
		return inv2XML != null ? inv2XML : (parent != null ? parent.getInv2XML() : false);
	}

	public void setInv2XML(Boolean inv2xml) {
		this.inv2XML = inv2xml;
	}

	public Boolean getStats() {
		return generateStats != null ? generateStats : (parent != null ? parent.getStats() : false);
	}

	public void setStats(Boolean generateStats) {
		this.generateStats = generateStats;
	}

	public Boolean getCoverage() {
		return generateCoverage != null ? generateCoverage : (parent != null ? parent.getStats() : false);
	}

	public void setCoverage(Boolean generateCoverage) {
		this.generateCoverage = generateCoverage;
	}

	public Boolean getKeepExecutionLogs() {
		return keepExecLogs != null ? keepExecLogs : (parent != null ? parent.getKeepExecutionLogs() : false);
	}

	public void setKeepExecutionLogs(Boolean keepTempResults) {
		this.keepExecLogs = keepTempResults;
	}

	public Boolean getKeepDTraces() {
		return keepDTraces != null ? keepDTraces : (parent != null ? parent.getKeepDTraces() : false);
	}

	public void setKeepDTraces(Boolean keepDTraces) {
		this.keepDTraces = keepDTraces;
	}

	public Integer getMaxInstrumentationDepth() {
		return maxInstrumentationDepth != null ? maxInstrumentationDepth : (parent != null ? parent.getMaxInstrumentationDepth() : null);
	}

	public void setMaxInstrumentationDepth(Integer maxInstrumentationDepth) {
		this.maxInstrumentationDepth = maxInstrumentationDepth;
	}

	public Boolean getInstrumentVariablesByDefault() {
		return instrumentVariablesByDefault != null ? instrumentVariablesByDefault : (parent != null ? parent.getInstrumentVariablesByDefault() : null);
	}

	public void setInstrumentVariablesByDefault(Boolean instrumentVariablesByDefault) {
		this.instrumentVariablesByDefault = instrumentVariablesByDefault;
	}

	public Boolean getInstrumentSequencesByDefault() {
		return instrumentSequencesByDefault != null ? instrumentSequencesByDefault : (parent != null ? parent.getInstrumentSequencesByDefault() : null);
	}

	public void setInstrumentSequencesByDefault(Boolean instrumentSequencesByDefault) {
		this.instrumentSequencesByDefault = instrumentSequencesByDefault;
	}

	public Boolean getRunDaikon() {
		return runDaikon != null ? runDaikon : (parent != null ? parent.getRunDaikon() : true);
	}

	public void setRunDaikon(Boolean runDaikon) {
		this.runDaikon = runDaikon;
	}

	public Integer getDaikonTimeoutInMinutes() {
		return daikonTimeoutInMinutes != null ? daikonTimeoutInMinutes : (parent != null ? parent.getDaikonTimeoutInMinutes() : null);
	}

	public void setDaikonTimeoutInMinutes(Integer daikonTimeoutInMinutes) {
		this.daikonTimeoutInMinutes = daikonTimeoutInMinutes;
	}

	public ConfigurationNode getParent() {
		return parent;
	}

	public void setParent(ConfigurationNode parent) {
		this.parent = parent;
	}

	/**
	 * Returns the document-wide unique ID for this experiment tree node.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Changes the document-wide unique ID for this experiment tree node.
	 */
	public void setId(String id) {
		this.id = id;
	}

	public List<String> getCompareWith() {
		return compareWith != null ? compareWith : (parent != null ? parent.getCompareWith() : new ArrayList<String>());
	}

	public void setCompareWith(List<String> ids) {
		this.compareWith = ids;
	}

	public List<String> getAddLogsFrom() {
		return addLogsFrom != null ? addLogsFrom : (parent != null ? parent.getAddLogsFrom() : new ArrayList<String>());
	}

	public void setAddLogsFrom(List<String> ids) {
		this.addLogsFrom = ids;
	}

	public List<String> getAddRawDTracesFrom() {
		return addRawDTracesFrom != null ? addRawDTracesFrom : (parent != null ? parent.getAddRawDTracesFrom() : new ArrayList<String>());
	}

	public void setAddRawDTracesFrom(List<String> ids) {
		this.addRawDTracesFrom = ids;
	}

	public void validate() throws ConfigurationException {
		if (getBpel() == null) {
			throw new ConfigurationException("Required attribute 'bpel' has not been set");
		}
		if (getBpts() == null) {
			throw new ConfigurationException("Required attribute 'bpts' has not been set");
		}
		if (getDir() == null) {
			throw new ConfigurationException("Required attribute 'dir' has not been set");
		}
		if (getActiveBPELPort() < 1) {
			throw new ConfigurationException("Required attribute 'activeBPELPort' must be an integer greater than 0");
		}
		if (getBpelUnitPort() < 1) {
			throw new ConfigurationException("Required attribute 'bpelUnitPort' must be an integer greater than 0");
		}
		if (getActiveBPELPort() == getBpelUnitPort()) {
			throw new ConfigurationException("'activeBPELPort' and 'bpelUnitPort' must be different from each other");
		}
		if (getChildren() != null && !getChildren().isEmpty() && getId() != null) {
			throw new ConfigurationException("Inner nodes in the experiment tree cannot use the 'id' attribute");
		}
		if (getAddLogsFrom() != null && getAddRawDTracesFrom() != null) {
			final Set<String> intersection = new HashSet<String>(getAddLogsFrom());
			intersection.retainAll(getAddRawDTracesFrom());
			if (!intersection.isEmpty()) {
				throw new ConfigurationException(
					"These IDs are both in addLogsFrom and in addRawDTracesFrom, but they are incompatible: " + intersection);
			}
		}
		if (!getRunDaikon()) {
			if (getInv2XML()) {
				throw new ConfigurationException("inv2XML cannot be used when runDaikon is set to false");
			}
			if (getStats()) {
				throw new ConfigurationException("stats cannot be used when runDaikon is set to false");
			}
			if (!getCompareWith().isEmpty()) {
				throw new ConfigurationException("compareWith cannot be used when runDaikon is set to false");
			}
		}
	}

	/**
	 * Resolves any pending relative paths using the provided base directory.
	 * After this method is run, all paths in the configuration are updated
	 * to absolute canonical paths. Path resolution is propagated to all
	 * ancestors as well.
	 *
	 * @param baseDir
	 *            Base directory to be used to resolve any pending relative
	 *            paths.
	 * @throws ConfigurationException
	 *             Could not canonicalize one of the resolved directories.
	 */
	public void resolveRelativePaths(File baseDir) throws ConfigurationException {
		try {
			if (getParent() != null) {
				getParent().resolveRelativePaths(baseDir);
			}
			this.bpel = resolveRelativePath(baseDir, this.bpel);
			this.bpts = resolveRelativePath(baseDir, this.bpts);
			this.useAsDataSrc = resolveRelativePath(baseDir, this.useAsDataSrc);
			this.outputDirectory = resolveRelativePath(baseDir, this.outputDirectory);
			this.daikonOptions = resolveRelativePath(baseDir, this.daikonOptions);
		} catch (IOException ex) {
			throw new ConfigurationException("Could not canonicalize a directory", ex);
		}
	}

	@Override
	public ConfigurationNode clone() {
		final ConfigurationNode config = new ConfigurationNode();
		if (bpel != null) {
			config.setBpel(new File(bpel.getPath()));
		}
		if (bpts != null) {
			config.setBpts(new File(bpts.getPath()));
		}
		if (outputDirectory != null) {
			config.setDir(new File(outputDirectory.getPath()));
		}
		if (daikonOptions != null) {
			config.setDaikonOptions(new File(daikonOptions.getPath()));
		}
		if (analyzerFlags != null) {
			config.setFlags(new ArrayList<String>(analyzerFlags));
		}
		if (daikonFlags != null) {
			config.setDaikonFlags(new ArrayList<String>(daikonFlags));
		}
		if (compareWith != null) {
			config.setCompareWith(new ArrayList<String>(compareWith));
		}
		if (addLogsFrom != null) {
			config.setAddLogsFrom(new ArrayList<String>(addLogsFrom));
		}
		if (addRawDTracesFrom != null) {
			config.setAddRawDTracesFrom(new ArrayList<String>(addRawDTracesFrom));
		}
		if (useAsDataSrc != null) {
			config.setUseAsDataSrc(useAsDataSrc);
		}
		if (getChildren() != null) {
			final List<ConfigurationNode> childrenCopy = new ArrayList<ConfigurationNode>();
			for (ConfigurationNode c : getChildren()) {
				childrenCopy.add(c.clone());
			}
			config.setChildren(childrenCopy);
		}
		config.setParent(getParent());
		config.setDirSuffix(getDirSuffix());
		config.setActiveBPELPort(activeBPELPort);
		config.setBpelUnitPort(bpelUnitPort);
		config.setUseSimplify(useSimplify);
		config.setInstrumentSequencesByDefault(instrumentSequencesByDefault);
		config.setInstrumentVariablesByDefault(instrumentVariablesByDefault);
		config.setMaxInstrumentationDepth(maxInstrumentationDepth);
		config.setInv2XML(inv2XML);
		config.setStats(generateStats);
		config.setCoverage(generateCoverage);
		config.setKeepExecutionLogs(keepExecLogs);
		config.setKeepDTraces(keepDTraces);
		config.setRunDaikon(runDaikon);
		config.setDaikonTimeoutInMinutes(daikonTimeoutInMinutes);
		config.setId(getId());

		return config;
	}

	@Override
	public String toString() {
		return toString("", this);
	}

	private static String toString(final String indent, final ConfigurationNode c) {
		final StringBuilder sb = new StringBuilder("ConfigurationNode [");
		if (c.activeBPELPort != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("activeBPELPort=");
			sb.append(c.activeBPELPort);
		}
		if (c.addLogsFrom != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("addLogsFrom=");
			sb.append(c.addLogsFrom);
		}
		if (c.addRawDTracesFrom != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("addRawDTracesFrom=");
			sb.append(c.addRawDTracesFrom);
		}
		if (c.analyzerFlags != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("analyzerFlags=");
			sb.append(c.analyzerFlags);
		}
		if (c.bpel != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("bpel=");
			sb.append(c.bpel);
		}
		if (c.bpelUnitPort != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("bpelUnitPort=");
			sb.append(c.bpelUnitPort);
		}
		if (c.bpts != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("bpts=");
			sb.append(c.bpts);
		}
		if (c.compareWith != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("compareWith=");
			sb.append(c.compareWith);
		}
		if (c.daikonFlags != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("daikonFlags=");
			sb.append(c.daikonFlags);
		}
		if (c.daikonOptions != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("daikonOptions=");
			sb.append(c.daikonOptions);
		}
		if (c.daikonTimeoutInMinutes != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("daikonTimeoutInMinutes=");
			sb.append(c.daikonTimeoutInMinutes);
		}
		if (c.dirSuffix != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("dirSuffix=");
			sb.append(c.dirSuffix);
		}
		if (c.generateStats != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("generateStats=");
			sb.append(c.generateStats);
		}
		if (c.generateCoverage != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("generateCoverage=");
			sb.append(c.generateCoverage);
		}
		if (c.id != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("id=");
			sb.append(c.id);
		}
		if (c.instrumentSequencesByDefault != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("instrumentSequencesByDefault=");
			sb.append(c.instrumentSequencesByDefault);
		}
		if (c.instrumentVariablesByDefault != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("instrumentVariablesByDefault=");
			sb.append(c.instrumentVariablesByDefault);
		}
		if (c.inv2XML != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("inv2XML=");
			sb.append(c.inv2XML);
		}
		if (c.keepDTraces != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("keepDTraces=");
			sb.append(c.keepDTraces);
		}
		if (c.keepExecLogs != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("keepExecutionLogs=");
			sb.append(c.keepExecLogs);
		}
		if (c.runDaikon != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("runDaikon=");
			sb.append(c.runDaikon);
		}
		if (c.maxInstrumentationDepth != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("maxInstrumentationDepth=");
			sb.append(c.maxInstrumentationDepth);
		}
		if (c.outputDirectory != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("outputDirectory=");
			sb.append(c.outputDirectory);
		}
		if (c.parent != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("parent=");
			sb.append(toString(indent + "  ", c.parent));
		}
		if (c.useAsDataSrc != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("useAsDataSrc=");
			sb.append(c.useAsDataSrc);
		}
		if (c.useSimplify != null) {
			sb.append("\n");
			sb.append(indent);
			sb.append("useSimplify=");
			sb.append(c.useSimplify);
		}
		sb.append("\n");
		sb.append(indent);
		sb.append("]");
		return sb.toString();
	}

	private File resolveRelativePath(final File baseDir, final File file)
			throws IOException {
		if (file != null && !file.isAbsolute()) {
			return new File(baseDir, file.getPath()).getCanonicalFile();
		}
		return file;
	}
}
