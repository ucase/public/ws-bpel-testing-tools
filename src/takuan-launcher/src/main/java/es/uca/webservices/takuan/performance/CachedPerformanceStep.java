package es.uca.webservices.takuan.performance;

import java.lang.management.ThreadMXBean;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.takuan.cache.CachedValue;
import es.uca.webservices.takuan.cache.ICache;

public class CachedPerformanceStep<CacheKeyType, ResultType> extends PerformanceStep<ResultType> {

	private static final Logger LOGGER = LoggerFactory.getLogger(CachedPerformanceStep.class);

	private ICache<CacheKeyType, ResultType> cache;
	private CacheKeyType cacheKey;

	public CachedPerformanceStep(String name, ThreadMXBean threadBean, MemoryWatcher memoryWatcher, ICache<CacheKeyType, ResultType> cache, CacheKeyType cacheKey, Callable<ResultType> task) {
		super(name, threadBean, memoryWatcher, task);
		this.cache = cache;
		this.cacheKey = cacheKey;
	}

	@Override
	public ResultType run(PerformanceMetrics performanceData) throws Exception {
		if (cache.get(cacheKey) != null) {
			LOGGER.info("Reusing cached value for " + getTaskName() + " using key " + cacheKey);
			final CachedValue<ResultType> cacheHit = cache.get(cacheKey);

			setWalltimeMillis(cacheHit.getWalltimeMillis());
			setCputimeMillis(cacheHit.getCputimeMillis());
			setUsedBytes(cacheHit.getUsedBytes());
			setCommittedBytes(cacheHit.getCommittedBytes());

			performanceData.putTimeUsage(getTaskName(), getWalltimeMillis(), getCputimeMillis());
			performanceData.putMemoryUsage(getTaskName(), getUsedBytes(), getCommittedBytes());
			performanceData.putCached(getTaskName(), true);

			return cacheHit.getValue();
		}
		else {
			final ResultType result = super.run(performanceData);
			cache.put(cacheKey, new CachedValue<ResultType>(result,
					getWalltimeMillis(), getCputimeMillis(),
					getUsedBytes(), getCommittedBytes()));
			performanceData.putCached(getTaskName(), false);
			return result;
		}
	}

}