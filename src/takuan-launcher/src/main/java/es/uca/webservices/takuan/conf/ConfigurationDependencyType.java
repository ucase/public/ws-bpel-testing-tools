package es.uca.webservices.takuan.conf;

/**
 * Types of edges in the configuration dependency multigraph.
 * 
 * @author Antonio García-Domínguez
 */
public enum ConfigurationDependencyType {
	/**
	 * Both configurations use the same logs, as they have the same
	 * ExecutionLogOptions.
	 */
	SAME_LOGS,
	/**
	 * The source configuration adds the execution logs of the target
	 * configuration to its own.
	 */
	ADD_LOGS,
	/**
	 * The source configuration adds the raw dtraces of the target configuration
	 * to its own.
	 */
	ADD_RAW_DTRACES,
	/**
	 * The source configuration compares its raw and flat dtraces and the
	 * obtained invariants to those of the target configuration.
	 */
	COMPARE_WITH
}
