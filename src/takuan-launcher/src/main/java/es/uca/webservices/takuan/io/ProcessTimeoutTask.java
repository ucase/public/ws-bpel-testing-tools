package es.uca.webservices.takuan.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Background task which will kill a subprocess in a certain amount of time. It
 * will also interrupt the task that is waiting for the subprocess to complete.
 * 
 * @author Antonio García-Domínguez
 */
public class ProcessTimeoutTask implements Runnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessTimeoutTask.class);

	private final long delayMillis;
	private final Process process;
	private final Thread waitThread;

	public ProcessTimeoutTask(long delayMillis, Thread waitThread, Process process) {
		this.delayMillis = delayMillis;
		this.process = process;
		this.waitThread = waitThread;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(delayMillis);
			waitThread.interrupt();
			process.destroy();
		} catch (InterruptedException e) {
			LOGGER.debug("Interrupted wait for timeout of " + delayMillis + "ms", e);
		}
	}

}
