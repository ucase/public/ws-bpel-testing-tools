package es.uca.webservices.takuan.performance;

import java.lang.management.ThreadMXBean;
import java.util.concurrent.Callable;

import es.uca.webservices.bpel.util.Pair;

public class PerformanceStep<ResultType> {
	private final ThreadMXBean threadBean;
	private final MemoryWatcher memoryWatcher;
	private final String taskName;
	private final Callable<ResultType> task;

	private long walltimeMillis;
	private long cputimeMillis;
	private long usedBytes;
	private long committedBytes;

	public PerformanceStep(String name, ThreadMXBean threadBean, MemoryWatcher memoryWatcher, Callable<ResultType> task) {
		this.threadBean = threadBean;
		this.memoryWatcher = memoryWatcher;
		this.taskName = name;
		this.task = task;
	}

	public ResultType run(PerformanceMetrics performanceData) throws Exception {
		// Save the starting CPU and wall times
		final long walltimeBeforeInstrumentation = System.currentTimeMillis();
		final long cputimeBeforeInstrumentation = threadBean.getCurrentThreadCpuTime();

		// Run the task
		try {
			final ResultType result = task.call();
			return result;
		} finally {
			// Collect timing and memory usage information
			collectPerformanceMetrics(walltimeBeforeInstrumentation, cputimeBeforeInstrumentation);
			performanceData.putTimeUsage(taskName, walltimeMillis, cputimeMillis);
			performanceData.putMemoryUsage(taskName, usedBytes, committedBytes);
		}
	}

	protected void collectPerformanceMetrics(final long walltimeBeforeInstrumentation, final long cputimeBeforeInstrumentation)
	{
		this.walltimeMillis = getElapsedWallMillis(walltimeBeforeInstrumentation);
		this.cputimeMillis = getElapsedCPUMillis(threadBean, cputimeBeforeInstrumentation);

		final Pair<Long, Long> usage = memoryWatcher.getMaxValues();
		this.usedBytes = usage.getLeft();
		this.committedBytes = usage.getRight();
		memoryWatcher.reset();
	}

	public String getTaskName() {
		return taskName;
	}

	public long getWalltimeMillis() {
		return walltimeMillis;
	}

	public long getCputimeMillis() {
		return cputimeMillis;
	}

	public long getUsedBytes() {
		return usedBytes;
	}

	public long getCommittedBytes() {
		return committedBytes;
	}

	protected void setWalltimeMillis(long walltimeMillis) {
		this.walltimeMillis = walltimeMillis;
	}

	protected void setCputimeMillis(long cputimeMillis) {
		this.cputimeMillis = cputimeMillis;
	}

	protected void setUsedBytes(long usedBytes) {
		this.usedBytes = usedBytes;
	}

	protected void setCommittedBytes(long committedBytes) {
		this.committedBytes = committedBytes;
	}

	private long getElapsedCPUMillis(final ThreadMXBean threadBean,	final long cputimeBefore) {
		return (threadBean.getCurrentThreadCpuTime() - cputimeBefore)/1000000;
	}

	private long getElapsedWallMillis(final long walltimeBefore) {
		return System.currentTimeMillis() - walltimeBefore;
	}
}