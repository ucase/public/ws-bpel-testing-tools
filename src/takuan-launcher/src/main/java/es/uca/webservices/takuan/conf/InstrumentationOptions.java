package es.uca.webservices.takuan.conf;

import java.io.File;


/**
 * Immutable Java bean which stores the subset of options of a
 * {@link ConfigurationNode} related to the instrumentation of the WS-BPEL
 * process and the generation of the .decls file.
 *
 * @author Antonio Garcia-Dominguez
 */
public class InstrumentationOptions {

	private final File bpel;
	private final Boolean instrumentSeqByDefault, instrumentVarByDefault;
	private final Integer instrumentationDepth;

	public InstrumentationOptions(File bpel, Boolean instrumentSeqByDefault, Boolean instrumentVarByDefault, Integer instrumentationDepth) {
		this.bpel = bpel;
		this.instrumentSeqByDefault = instrumentSeqByDefault;
		this.instrumentVarByDefault = instrumentVarByDefault;
		this.instrumentationDepth = instrumentationDepth;
	}

	public InstrumentationOptions(ConfigurationNode config) {
		this(config.getBpel(),
			config.getInstrumentSequencesByDefault(),
			config.getInstrumentVariablesByDefault(),
			config.getMaxInstrumentationDepth());
	}

	public File getBpel() {
		return bpel;
	}

	public Boolean getInstrumentSeqByDefault() {
		return instrumentSeqByDefault;
	}

	public Boolean getInstrumentVarByDefault() {
		return instrumentVarByDefault;
	}

	public Integer getInstrumentationDepth() {
		return instrumentationDepth;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bpel == null) ? 0 : bpel.hashCode());
		result = prime
				* result
				+ ((instrumentSeqByDefault == null) ? 0
						: instrumentSeqByDefault.hashCode());
		result = prime
				* result
				+ ((instrumentVarByDefault == null) ? 0
						: instrumentVarByDefault.hashCode());
		result = prime
				* result
				+ ((instrumentationDepth == null) ? 0 : instrumentationDepth
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InstrumentationOptions other = (InstrumentationOptions) obj;
		if (bpel == null) {
			if (other.bpel != null)
				return false;
		} else if (!bpel.equals(other.bpel))
			return false;
		if (instrumentSeqByDefault == null) {
			if (other.instrumentSeqByDefault != null)
				return false;
		} else if (!instrumentSeqByDefault.equals(other.instrumentSeqByDefault))
			return false;
		if (instrumentVarByDefault == null) {
			if (other.instrumentVarByDefault != null)
				return false;
		} else if (!instrumentVarByDefault.equals(other.instrumentVarByDefault))
			return false;
		if (instrumentationDepth == null) {
			if (other.instrumentationDepth != null)
				return false;
		} else if (!instrumentationDepth.equals(other.instrumentationDepth))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InstrumentationCacheKey [bpel=" + bpel
				+ ", instrumentSeqByDefault=" + instrumentSeqByDefault
				+ ", instrumentVarByDefault=" + instrumentVarByDefault
				+ ", instrumentationDepth=" + instrumentationDepth + "]";
	}
}
