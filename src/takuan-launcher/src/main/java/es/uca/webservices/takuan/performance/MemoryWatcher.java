package es.uca.webservices.takuan.performance;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.MalformedURLException;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;

import es.uca.webservices.bpel.util.Pair;

/**
 * Task that monitors the total memory usage (heap and non-heap) of this JVM or
 * another JVM in the same machine. Reports maximum values for committed and
 * used memory since it was last reset. Stops as soon as the thread is
 * interrupted. Tries to reconnect at every update, if not connected yet. 
 * 
 * Note: it would be a good idea to run this task inside a daemon thread, so we
 * don't have to explicitly stop it.
 * 
 * @author Antonio García-Domínguez
 */
public class MemoryWatcher implements Runnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemoryWatcher.class);

	private JMXServiceURL url;

	private final long updateMillis;
	private long maxUsed = 0;
	private long maxCommitted = 0;

	private MemoryMXBean memoryBean;

	public static MemoryWatcher fromPid(int pid, long updateMillis) throws AttachNotSupportedException, IOException, AgentLoadException, AgentInitializationException {
		return new MemoryWatcher(getURLForPid(pid), updateMillis);
	}

	public static MemoryWatcher fromPort(int jmxPort, int memoryWatchInterval)
			throws MalformedURLException
	{
		return new MemoryWatcher(
			new JMXServiceURL("service:jmx:rmi:///jndi/rmi://127.0.0.1:" + jmxPort + "/jmxrmi"),
			memoryWatchInterval);
	}

	/**
	 * Creates a new watcher for this JVM.
	 * @param updateMillis Delay in milliseconds between updates.
	 */
	public MemoryWatcher(long updateMillis) {
		this.updateMillis = updateMillis;
	}

	/**
	 * Creates a new watcher for the JVM running on process ID <code>pid</code>.
	 * @param pid Process ID of the JVM to be monitored.
	 * @param updateMillis Delay in milliseconds between updates.
	 */
	public MemoryWatcher(JMXServiceURL url, long updateMillis) {
		this.updateMillis = updateMillis;
		this.url = url;
	}

	@Override
	public void run() {
		JMXConnector jmxConnector = null;
		try {
			memoryBean = null;
			while (!Thread.interrupted()) {
				if (memoryBean == null) {
					if (url != null) {
						try {
							// Try to connect to the JMX URL if not connected yet
							jmxConnector = JMXConnectorFactory.connect(url);
							final MBeanServerConnection mbean = jmxConnector.getMBeanServerConnection();
							memoryBean = ManagementFactory.newPlatformMXBeanProxy(mbean, ManagementFactory.MEMORY_MXBEAN_NAME, MemoryMXBean.class);
						} catch (IOException ex) {
							LOGGER.debug("Could not connect to the JMX instance: waiting until next retry", ex);
						}
					} else {
						memoryBean = ManagementFactory.getMemoryMXBean();
					}
				}
				update();
				Thread.sleep(updateMillis);
			}
		} catch (UndeclaredThrowableException ex) {
			LOGGER.debug("connection was closed abruptly", ex);
		} catch (InterruptedException e) {
			LOGGER.debug("run() was interrupted");
		} finally {
			if (jmxConnector != null) {
				try {
					jmxConnector.close();
				} catch (IOException e) {
					LOGGER.debug("Could not close the JMX connector: it might be already closed", e);
				}
			}
		}
	}

	/**
	 * Returns a (maxUsed, maxCommitted) pair with the maximum memory used and
	 * commited by the JVM since the last {@link #reset()} or since the thread
	 * was started.
	 */
	public synchronized Pair<Long, Long> getMaxValues() {
		return new Pair<Long, Long>(maxUsed, maxCommitted);
	}

	/**
	 * Resets the saved memory values to 0, performs a garbage collection and
	 * performs an immediate update.
	 */
	public synchronized void reset() {
		maxUsed = maxCommitted = 0;
		System.gc();
		update();
	}

	private synchronized void update() throws UndeclaredThrowableException {
		if (memoryBean != null) {
			final MemoryUsage heapUsage = memoryBean.getHeapMemoryUsage();
			final MemoryUsage nonheapUsage = memoryBean.getNonHeapMemoryUsage();
			maxUsed = Math.max(maxUsed, heapUsage.getUsed() + nonheapUsage.getUsed());
			maxCommitted = Math.max(maxCommitted, heapUsage.getCommitted() + nonheapUsage.getCommitted());
		}
	}

	/* taken from https://blogs.oracle.com/jmxetc/entry/how_to_retrieve_remote_jvm */

	private static final String CONNECTOR_ADDRESS =
	          "com.sun.management.jmxremote.localConnectorAddress";

	private static JMXServiceURL getURLForPid(int pid)
			throws AttachNotSupportedException, IOException, AgentLoadException, AgentInitializationException
	{
      // attach to the target application
      final VirtualMachine vm = VirtualMachine.attach("" + pid);

      // get the connector address
      String connectorAddress =
              vm.getAgentProperties().getProperty(CONNECTOR_ADDRESS);

      // no connector address, so we start the JMX agent
      if (connectorAddress == null) {
          String agent = vm.getSystemProperties().getProperty("java.home") +
                  File.separator + "lib" + File.separator + "management-agent.jar";
          vm.loadAgent(agent);

          // agent is started, get the connector address
          connectorAddress =
                  vm.getAgentProperties().getProperty(CONNECTOR_ADDRESS);
          assert connectorAddress != null;
      }
      return new JMXServiceURL(connectorAddress);
  }

}
