package es.uca.webservices.takuan.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple cache based on a hash table. Does not free resources automatically.
 * 
 * @author Antonio García-Domínguez
 */
public class HashCache<K, V> implements ICache<K, V> {

	private final Map<K, CachedValue<V>> hashTable = new HashMap<K, CachedValue<V>>();

	@Override
	public CachedValue<V> get(K key) {
		return hashTable.get(key);
	}

	@Override
	public void put(K key, CachedValue<V> value) {
		hashTable.put(key, value);
	}

	@Override
	public void configurationDoneWith(K key) {
		// nothing to do
	}

	@Override
	public String toString() {
		return "HashCache[" + hashTable + "]";
	}
}
