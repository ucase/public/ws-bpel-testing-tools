package es.uca.webservices.takuan.io;

/**
 * Line processor which extracts several importants results from the output
 * of the Takuan Perl scripts.
 *
 * @author Antonio Garcia-Dominguez
 */
public class TakuanPerlLineProcessor implements LineProcessor {

	static final String TAKUAN_PERL_DTRACES_LINEPREFIX = "dtraces: ";
	static final String TAKUAN_PERL_DECLS_LINEPREFIX = "decls: ";
	static final String TAKUAN_PERL_ACCEPTED_LOGS_LINEPREFIX = "acceptedLogs: ";

	private String decls;
	private String[] dtraces;
	private String[] acceptedLogs;

	public String getDecls() {
		return decls;
	}

	public String[] getDTraces() {
		return dtraces;
	}

	public String[] getAcceptedLogs() {
		return acceptedLogs;
	}

	@Override
	public void process(String line) {
		if (line.startsWith(TAKUAN_PERL_DECLS_LINEPREFIX)) {
			decls = line.substring(TAKUAN_PERL_DECLS_LINEPREFIX.length());
		}
		else if (line.startsWith(TAKUAN_PERL_DTRACES_LINEPREFIX)) {
			dtraces = line.substring(TAKUAN_PERL_DTRACES_LINEPREFIX.length()).split(", ");
		}
		else if (line.startsWith(TAKUAN_PERL_ACCEPTED_LOGS_LINEPREFIX)) {
			acceptedLogs = line.substring(TAKUAN_PERL_ACCEPTED_LOGS_LINEPREFIX.length()).split(", ");
		}
	}
}