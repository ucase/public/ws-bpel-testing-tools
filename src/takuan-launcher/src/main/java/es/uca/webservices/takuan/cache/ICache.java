package es.uca.webservices.takuan.cache;

/**
 * Cache which stores intermediate results. Some implementations may
 * also free these results when they are no longer needed.
 *
 * @author Antonio García-Domínguez
 */
public interface ICache<K, V> {

	/**
	 * Retrieves the value with key <code>key</code> from the cache.
	 * 
	 * @return The corresponding value, or <code>null</code> if it is not in the
	 *         cache.
	 */
	CachedValue<V> get(K key);

	/**
	 * Changes the value assigned to key <code>key</code>.
	 */
	void put(K key, CachedValue<V> value);

	/**
	 * Notifies the cache that a configuration has finished using the cached
	 * value corresponding to <code>key</code>. Some implementations might free
	 * the related resources when no configurations need the value anymore. 
	 */
	void configurationDoneWith(K key);
}
