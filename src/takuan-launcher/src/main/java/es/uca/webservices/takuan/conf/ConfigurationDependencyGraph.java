package es.uca.webservices.takuan.conf;

import es.uca.webservices.takuan.graphs.EnumSetMultigraph;

/**
 * EnumSet-based multigraph which stores the dependencies between configuration
 * nodes.
 * 
 * @author Antonio García-Domínguez
 */
public class ConfigurationDependencyGraph extends
		EnumSetMultigraph<ConfigurationNode, ConfigurationDependencyType> {

}
