package es.uca.webservices.takuan.cli;

import static es.uca.webservices.takuan.conf.ConfigurationDependencyType.ADD_LOGS;
import static es.uca.webservices.takuan.conf.ConfigurationDependencyType.ADD_RAW_DTRACES;
import static es.uca.webservices.takuan.conf.ConfigurationDependencyType.COMPARE_WITH;
import static es.uca.webservices.takuan.conf.ConfigurationDependencyType.SAME_LOGS;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import net.bpelunit.framework.exception.ConfigurationException;
import net.bpelunit.framework.exception.DeploymentException;
import net.bpelunit.framework.exception.SpecificationException;

import org.activebpel.rt.bpel.server.logging.AeLoggingFilter;
import org.activebpel.rt.jetty.AeJettyRunner;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.mutable.MutableLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import es.uca.takuan.coverage.PathCoverage;
import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.bpel.InvalidProcessException;
import es.uca.webservices.bpel.activebpel.BPELPackagingException;
import es.uca.webservices.bpel.bpelunit.BPELUnitSpecification;
import es.uca.webservices.bpel.bpelunit.ExecutionResults;
import es.uca.webservices.bpel.daikon.decls.DeclarationGenerationException;
import es.uca.webservices.bpel.daikon.decls.DeclarationsFormatter;
import es.uca.webservices.bpel.daikon.decls.DeclarationsGenerator;
import es.uca.webservices.bpel.daikon.instrument.BPELFileInstrumenter;
import es.uca.webservices.bpel.daikon.instrument.BPELInstrumentationException;
import es.uca.webservices.bpel.inv.XMLFromInv;
import es.uca.webservices.bpel.util.Pair;
import es.uca.webservices.takuan.cache.CachedValue;
import es.uca.webservices.takuan.cache.HashCache;
import es.uca.webservices.takuan.conf.ConfigurationDependencyGraph;
import es.uca.webservices.takuan.conf.ConfigurationDependencyType;
import es.uca.webservices.takuan.conf.ConfigurationLoader;
import es.uca.webservices.takuan.conf.ConfigurationNode;
import es.uca.webservices.takuan.conf.Configurations;
import es.uca.webservices.takuan.conf.ExecutionLogOptions;
import es.uca.webservices.takuan.conf.InstrumentationOptions;
import es.uca.webservices.takuan.exceptions.TakuanException;
import es.uca.webservices.takuan.io.ActiveBPELLogDirectoryWatcher;
import es.uca.webservices.takuan.io.CatTask;
import es.uca.webservices.takuan.io.OutputStreamRunnable;
import es.uca.webservices.takuan.io.ProcessTimeoutTask;
import es.uca.webservices.takuan.io.TakuanPerlLineProcessor;
import es.uca.webservices.takuan.performance.CachedPerformanceStep;
import es.uca.webservices.takuan.performance.MemoryWatcher;
import es.uca.webservices.takuan.performance.PerformanceMetrics;
import es.uca.webservices.takuan.performance.PerformanceStep;
import es.uca.webservices.wsdl.analyzer.Application;

/**
 * <p>
 * Main command line launcher for Takuan.
 * </p>
 * 
 * <p>
 * Copyright (C) 2011-2012 Antonio García-Domínguez.
 * </p>
 * 
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * </p>
 * 
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * 
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a href="http://www.gnu.org/licenses/">here</a>.
 * </p>
 */
public class Launcher {

	private static final int ACTIVEBPEL_RETRY_DELAY = 500;
	private static final int ACTIVEBPEL_WAIT_RETRIES = 10;
	private static final String HELP_OPTION = "help";
	private static final String BASEDIR_OPTION = "baseDir";
	private static final String BPTS_OPTION = "bpts";
	private static final String BPEL_OPTION = "bpel";
	private static final int JMX_RMI_PORT = 19782;
	private static final int MEMORY_WATCH_INTERVAL = 100;
	private static final Logger LOGGER = LoggerFactory.getLogger(Launcher.class);

	private static final Pattern PATTERN_DAIKON_STDOUT_PROGRAM_POINT = Pattern.compile(":::");
	private static final Pattern PATTERN_DIFF_INVARIANT = Pattern.compile("^(?!.*(?::::|SUU|SJJ|SJU|SUJ)).*$");
	private static final Pattern PATTERN_LOG_FILENAME = Pattern.compile("[0-9]+.log");
	private static final Pattern PATTERN_RAW_DTRACE_FILENAME = Pattern.compile("[0-9]+.dtrace");
	private static final Pattern PATTERN_FLAT_DTRACE_FILENAME = Pattern.compile("[0-9]+.flat.dtrace");

	private static final String COVERAGE_ALL_FILENAME = "pathCoverageAll.log";
	private static final String COVERAGE_NOTEXECUTED_FILENAME = "pathCoverageNotExecuted.log";
	private static final String DAIKON_DECLS_FILENAME = "inspected.decls";
	private static final String DAIKON_INVGZ_FILENAME = "daikon.inv.gz";
	private static final String VERSION_PROPERTY = "takuan.version";

	static final String ACTIVEBPEL_RESULT_SUBDIR = "activebpel";
	static final String ACTIVEBPEL_LOGS_SUBDIR = "process-logs";
	static final String COVERAGE_EXECUTED_FILENAME = "pathCoverageExecuted.log";
	static final String DAIKON_DEFAULT_OPTIONS_PROPERTY = "default.daikon.options";
	static final String DAIKON_JVM_FLAGS_PROPERTY = "daikon.jvm.flags";
	static final String DAIKON_STATS_FILENAME = "stats.txt";
	static final String DAIKON_STDOUT_FILENAME = "daikon.out";
	static final String DAIKON_XML_FILENAME = "daikon.xml";
	static final String TAKUAN_DTRACE_DIRECTORY_NAME = "takuan";
	static final String TAKUAN_PERL_CMD_PROPERTY = "executable.perl";
	static final String TAKUAN_CGRAPHS_FILENAME = "saved.cgraphs";

	private static Properties properties;

	private HashCache<InstrumentationOptions, BPELProcessDefinition> cachedInstrumentedProcesses;
	private HashCache<InstrumentationOptions, File> cachedDaikonDeclarations;
	private HashCache<ExecutionLogOptions, File> cachedActiveBPELDirs;

	private MemoryWatcher memoryWatcher;

	public Launcher() {
		memoryWatcher = new MemoryWatcher(MEMORY_WATCH_INTERVAL);
		Thread memoryThread = new Thread(memoryWatcher, "MemoryWatcher");
		memoryThread.setDaemon(true);
		memoryThread.start();
	}

	public static void main(String[] args) {
		try {
			new Launcher().run(args, System.out, System.err);
		} catch (IllegalArgumentException e) {
			// Bad arguments given through the interface: an usage message should have been printed already
			System.exit(1);
		} catch (Exception e) {
			LOGGER.error("Unhandled exception", e);
			e.printStackTrace();
			System.exit(2);
		}
	}

	public void run(String[] args, PrintStream out, PrintStream err) throws TakuanException
	{
		try {
			OptionParser parser = new OptionParser();
			parser.accepts(HELP_OPTION, "Provides help for this program");
			parser.accepts(BPEL_OPTION, "Sets the default .bpel file if none is specified in the .yaml file").withRequiredArg().ofType(File.class).describedAs(".bpel");
			parser.accepts(BPTS_OPTION, "Sets the default .bpts file if none is specified in the .yaml file").withRequiredArg().ofType(File.class).describedAs(".bpts");
			parser.accepts(BASEDIR_OPTION, "Changes the base directory where the result subdirectories should be placed").withRequiredArg().ofType(File.class).describedAs("dir");
			final OptionSet options = parser.parse(args);
			final List<String> yamlPaths = options.nonOptionArguments();
			if (options.has(HELP_OPTION)) {
				printUsage(err);
				parser.printHelpOn(err);
				return;
			}
			else if (yamlPaths.isEmpty()) {
				printUsage(err);
				parser.printHelpOn(err);
				throw new IllegalArgumentException("Bad arguments: expected the path to the .yaml file");
			}

			final File fCSV = new File("performance.csv");
			ConfigurationLoader loader = new ConfigurationLoader();
			if (options.has(BPEL_OPTION)) {
				loader.setDefaultBPEL(((File)options.valueOf(BPEL_OPTION)).getCanonicalFile());
			}
			if (options.has(BPTS_OPTION)) {
				loader.setDefaultBPTS(((File)options.valueOf(BPTS_OPTION)).getCanonicalFile());
			}
			if (options.has(BASEDIR_OPTION)) {
				loader.setBaseDir(((File)options.valueOf(BASEDIR_OPTION)).getCanonicalFile());
			}

			for (String yamlPath : yamlPaths) {
				final File fYAML = new File(yamlPath);
				if (!options.has(BASEDIR_OPTION)) {
					loader.setBaseDir(fYAML.getCanonicalFile().getParentFile());
				}
				final Configurations contents = loader.load(fYAML);

				// Initialize the caches
				cachedInstrumentedProcesses = new HashCache<InstrumentationOptions, BPELProcessDefinition>();
				cachedDaikonDeclarations = new HashCache<InstrumentationOptions, File>();
				cachedActiveBPELDirs = new HashCache<ExecutionLogOptions, File>();

				for (ConfigurationNode config : contents.getConfigurations()) {
					try {
						PerformanceMetrics pm = run(contents, config, out, err);
						pm.put(BASEDIR_OPTION, config.getDir().getPath());
						appendToCSV(fCSV, pm);
					} catch (Exception ex) {
						LOGGER.error("There was a problem while running the configuration {}", config, ex);
						throw ex;
					}
				}
			}
		} catch (IllegalArgumentException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new TakuanException(ex);
		}
	}

	private PerformanceMetrics run(Configurations contents, final ConfigurationNode config, PrintStream out, PrintStream err) throws TakuanException
	{
		try {
			final File baseDir = config.getDir();
			if (baseDir.isDirectory()) {
				// Clean base directory if it exists
				FileUtils.cleanDirectory(baseDir);
			}
			ensureDirectoryExists(baseDir);
			LOGGER.info("Starting to run configuration with output directory {}", baseDir);

			/*
			 * Use this hash to store instrumentation, execution, postprocess, Daikon and total wall and CPU
			 * times and JVM memory usage
			 */
			final PerformanceMetrics performanceData = new PerformanceMetrics();
			final ThreadMXBean threadBean = ManagementFactory.getThreadMXBean();

			/* INSTRUMENTATION */

			// Generate instrumented .bpel
			final InstrumentationOptions instrumentationCacheKey = new InstrumentationOptions(config);
			final CachedPerformanceStep<InstrumentationOptions, BPELProcessDefinition> instrumentStep
				= new CachedPerformanceStep<InstrumentationOptions, BPELProcessDefinition>(
					"Instrumentation", threadBean, memoryWatcher, cachedInstrumentedProcesses, instrumentationCacheKey,
					new Callable<BPELProcessDefinition>() {
						public BPELProcessDefinition call() throws Exception {
							return instrumentBPELProcess(config, baseDir);
						}
					}
			);
			final BPELProcessDefinition instrumentedDef = instrumentStep.run(performanceData);

			/* EXECUTION */

			// Run the .bpts against the .bpel
			final ExecutionLogOptions execLogsCacheKey = new ExecutionLogOptions(config);
			final CachedPerformanceStep<ExecutionLogOptions, File> executionStep
				= new CachedPerformanceStep<ExecutionLogOptions, File>(
					"Execution (own tests)", threadBean, memoryWatcher, cachedActiveBPELDirs, execLogsCacheKey,
					new Callable<File>() {
						public File call() throws Exception {
							return runBPTS(config, baseDir, instrumentedDef);
						}
					}
			);
			final Set<File> activeBPELLogs = listActiveBPELLogs(executionStep.run(performanceData));

			// Collect logs from our execution and logs/raw dtraces/cgraphs from referenced configurations
			final ConfigurationDependencyGraph depGraph = contents.getDependencyGraph();
			final Set<File> rawDTraces = new LinkedHashSet<File>();
			final MutableLong totalWallMillisFromReferences = new MutableLong(0);
			final MutableLong totalCPUMillisFromReferences = new MutableLong(0);
			collectReferencesToPreviousExecutions(depGraph, config,
					activeBPELLogs, rawDTraces, totalWallMillisFromReferences,
					totalCPUMillisFromReferences);
			final Set<File> cGraphs = collectComparabilityGraphs(depGraph, config);
			performanceData.putTimeUsage("Execution (references)", totalWallMillisFromReferences.longValue(), totalCPUMillisFromReferences.longValue());

			performanceData.putTimeUsage("Execution (total)",
					executionStep.getWalltimeMillis() + totalWallMillisFromReferences.longValue(),
					executionStep.getCputimeMillis() + totalCPUMillisFromReferences.longValue());

			/* ANALYSIS */

			// Generate the .decls file
			final CachedPerformanceStep<InstrumentationOptions, File> declsStep
				= new CachedPerformanceStep<InstrumentationOptions, File>(
					"Declarations", threadBean, memoryWatcher, cachedDaikonDeclarations, instrumentationCacheKey,
					new Callable<File>() {
						public File call() throws Exception {
							return generateDaikonDeclarations(config, instrumentedDef);
						}
					}
			);
			final File fDecls = declsStep.run(performanceData);

			// Preprocess the execution traces generated by ActiveBPEL for Daikon
			final File takuanPerlDir = new File(baseDir, TAKUAN_DTRACE_DIRECTORY_NAME);
			final PerformanceStep<TakuanPerlLineProcessor> preprocessStep = new PerformanceStep<TakuanPerlLineProcessor>(
				"Trace preprocessing", threadBean, memoryWatcher, new Callable<TakuanPerlLineProcessor>() {
					public TakuanPerlLineProcessor call() throws Exception {
						ensureDirectoryExists(takuanPerlDir);
						return runTakuanPerl(config, baseDir, activeBPELLogs, rawDTraces, cGraphs, takuanPerlDir, fDecls, instrumentedDef);
					}
				}
			);
			final TakuanPerlLineProcessor takuanPerlInfo = preprocessStep.run(performanceData);

			// Run Daikon and print the invariants in XML format as well
			final PerformanceStep<File> daikonStep = new PerformanceStep<File>(
				"Invariant generation", threadBean, memoryWatcher, new Callable<File>() {
					public File call() throws Exception {
						if (config.getRunDaikon()) {
							return runDaikon(config, baseDir, takuanPerlInfo, performanceData);
						} else {
							return null;
						}
					}
				}
			);
			final File fSerializedInvariants = daikonStep.run(performanceData); 

			final long analysisWallMillis = declsStep.getWalltimeMillis() + preprocessStep.getWalltimeMillis() + daikonStep.getWalltimeMillis();
			final long analysisCpuMillis = declsStep.getCputimeMillis() + preprocessStep.getCputimeMillis() + daikonStep.getCputimeMillis();
			performanceData.putTimeUsage("Analysis (decls + preprocess + Daikon)", analysisWallMillis, analysisCpuMillis);

			/* STATISTICS AND OTHER RANDOM REPORTS */

			performanceData.putTimeUsage("IEA",
					instrumentStep.getWalltimeMillis() + executionStep.getWalltimeMillis() + analysisWallMillis,
					instrumentStep.getCputimeMillis() + executionStep.getCputimeMillis() + analysisCpuMillis);
			performanceData.put("Generated invariants", Integer.toString(countInvariants(new File(baseDir, DAIKON_STDOUT_FILENAME))));

			final PerformanceStep<Object> coverageStep = new PerformanceStep<Object>(
				"Coverage reports", threadBean, memoryWatcher, new Callable<Object>() {
					public Object call() throws Exception {
						if (config.getCoverage()) {
							generatePathCoverageReports(baseDir, instrumentedDef, takuanPerlInfo);
						}
						return null;
					}
				}
			);
			coverageStep.run(performanceData);

			final PerformanceStep<Object> inv2XMLStep = new PerformanceStep<Object>(
				"XML invariant formatting", threadBean, memoryWatcher, new Callable<Object>() {
					public Object call() throws Exception {
						if (fSerializedInvariants != null && config.getInv2XML()) {
							LOGGER.info("Saving invariants as XML");
							new XMLFromInv(fSerializedInvariants).dumpXML(new FileOutputStream(new File(baseDir, DAIKON_XML_FILENAME)));
						}
						return null;
					}
				}
			);
			final PerformanceStep<Object> statsStep = new PerformanceStep<Object>(
				"Statistics", threadBean, memoryWatcher, new Callable<Object>() {
					public Void call() throws Exception {
						if (fSerializedInvariants != null && config.getStats()) {
							runDaikonDiff(new File(baseDir, DAIKON_STATS_FILENAME), "-s", new File(baseDir, DAIKON_INVGZ_FILENAME).getPath());
						}
						return null;
					}
				}
			);
			inv2XMLStep.run(performanceData);
			statsStep.run(performanceData);

			/* COMPARISONS */
			for (ConfigurationNode compareWith : depGraph.getOutgoing(config, EnumSet.of(COMPARE_WITH)).keySet()) {
				performComparison(config, compareWith);
			}

			/* CLEANUP */
			doneWithConfiguration(depGraph, config);

			return performanceData;
		} catch (TakuanException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new TakuanException(ex);
		}
	}

	private static String getVersion() throws IOException {
	    return getProperties().getProperty(VERSION_PROPERTY);
	}

	private static synchronized Properties getProperties() throws IOException {
	    InputStream in = null;
	    try {
	        if (properties == null) {
	            properties = new Properties();
	            in = Application.class.getResourceAsStream("/takuan.properties");
	            properties.load(in);
	        }
	        return properties;
	    } finally {
	        if (in != null) {
	            in.close();
	        }
	    }
	}

	private void printUsage(PrintStream err) throws IOException {
		err.println("Takuan launcher version " + getVersion());
		err.println("Usage: takuan (paths to 1+ .yaml files ...)");
		err.println("\nA valid example .yaml file has been included in the default distribution.\nPlease check the sample subdirectory in your install directory.\n");
	}

	private File generateDaikonDeclarations(final ConfigurationNode config,	final BPELProcessDefinition instrumentedDef)
			throws DeclarationGenerationException, IOException
	{
		final File baseDir = config.getDir();
		final File fBPEL = instrumentedDef.getFile();
		final File fDecls = new File(baseDir, DAIKON_DECLS_FILENAME);
		final DeclarationsGenerator gen = new DeclarationsGenerator();
		final FileWriter writer = new FileWriter(fDecls);
		try {
			LOGGER.info("Generating Daikon declarations for " + fBPEL + " at " + fDecls);
			gen.generateAndPrint(instrumentedDef, new DeclarationsFormatter(), writer);
		} finally {
			writer.flush();
			writer.close();
		}
		return fDecls;
	}

	private void generatePathCoverageReports(final File baseDir,
			final BPELProcessDefinition instrumentedDef,
			final TakuanPerlLineProcessor takuanPerlInfo) throws SAXException,
			IOException, InterruptedException, ParserConfigurationException,
			XPathExpressionException, TakuanException {
		PathCoverage coverage = new PathCoverage();
		coverage.setOutputFile(new File(baseDir, COVERAGE_NOTEXECUTED_FILENAME));
		coverage.setPossiblePathsFile(new File(baseDir, COVERAGE_ALL_FILENAME));
		coverage.setExecutedPathsFile(new File(baseDir, COVERAGE_EXECUTED_FILENAME));
		coverage.setBPELFile(instrumentedDef.getFile());
		if (takuanPerlInfo.getAcceptedLogs() != null) {
			List<File> logs = new ArrayList<File>(takuanPerlInfo.getAcceptedLogs().length);
			for (String log : takuanPerlInfo.getAcceptedLogs()) {
				logs.add(new File(log));
			}
			coverage.generateCoverage(logs);
		}
		else {
			throw new TakuanException("The Perl script for Takuan did not produce the required list of accepted logs");
		}
	}

	private BPELProcessDefinition instrumentBPELProcess(ConfigurationNode config,
			final File baseDir) throws ParserConfigurationException,
			SAXException, IOException, XPathExpressionException,
			InvalidProcessException, BPELInstrumentationException
	{
		final File fOriginalBPEL = config.getBpel();

		final BPELProcessDefinition originalDef = new BPELProcessDefinition(fOriginalBPEL);
		if (config.getInstrumentSequencesByDefault() != null) {
			originalDef.setInstrumentSequencesByDefault(config.getInstrumentSequencesByDefault());
		}
		if (config.getInstrumentVariablesByDefault() != null) {
			originalDef.setInstrumentVariablesByDefault(config.getInstrumentVariablesByDefault());
		}
		if (config.getMaxInstrumentationDepth() != null) {
			originalDef.setInstrumentSequencesDepthThreshold(config.getMaxInstrumentationDepth());
		}

		final BPELFileInstrumenter instrumenter = new BPELFileInstrumenter(originalDef);
		LOGGER.info("Generating instrumented BPEL process definition for {}", originalDef.getFile());
		final BPELProcessDefinition instrumentedDef = instrumenter.instrument(new File(baseDir, "instrumented.bpel"));
		LOGGER.info("Generated instrumented BPEL process definition at {}", instrumentedDef.getFile());

		return instrumentedDef;
	}

	private File runBPTS(ConfigurationNode config, final File baseDir,
			final BPELProcessDefinition instrumentedDef) throws Exception
	{
		final File activeBPELDir = new File(baseDir, ACTIVEBPEL_RESULT_SUBDIR);

		// Start ActiveBPEL under the specified base directory and with the specified port
		ensureDirectoryExists(activeBPELDir);

		final Integer activeBPELPort = config.getActiveBPELPort();
		final AeJettyRunner activeBPELRunner = new AeJettyRunner(
			activeBPELDir, activeBPELPort, AeLoggingFilter.FULL);
		LOGGER.info("Starting ActiveBPEL on directory "
			+ activeBPELDir + " and port " + activeBPELPort + " with full logging");
		try {
			activeBPELRunner.start();
		} catch (Throwable e) {
			throw new Exception(e);
		}
		LOGGER.info("Finished starting ActiveBPEL");

		try {
			// Run the BPTS test suite against the instrumented BPEL process definition
			runBPELUnit(config, instrumentedDef, activeBPELDir);
		} finally {
			activeBPELRunner.stop();
			activeBPELRunner.join();
			activeBPELRunner.uninstallLogging();
		}

		// Give it a bit more time until all the logs have been *really* written to a file
		try {
			final File logsSubdir = new File(activeBPELDir, ACTIVEBPEL_LOGS_SUBDIR);
			new ActiveBPELLogDirectoryWatcher(
					logsSubdir, ACTIVEBPEL_WAIT_RETRIES, ACTIVEBPEL_RETRY_DELAY
			).waitForCompletion();
		} catch (Exception ex) {
			LOGGER.error("Exception while waiting for the logs to be written", ex);
		}

		return activeBPELDir;
	}

	private File runDaikon(ConfigurationNode config, final File baseDir,
			final TakuanPerlLineProcessor takuanPerlInfo, PerformanceMetrics performanceData) throws IOException, TakuanException
	{
		if (takuanPerlInfo.getDecls() == null) {
			throw new TakuanException("Takuan's Perl script did not list the generated .decls file");
		}
		else if (takuanPerlInfo.getDTraces() == null) {
			throw new TakuanException("Takuan's Perl script did not list the generated .dtrace files");
		}

		final File fSerializedInvariants = new File(baseDir, DAIKON_INVGZ_FILENAME);
		final List<String> daikonArgs = new ArrayList<String>();
		daikonArgs.add("--config");
		if (config.getDaikonOptions() != null) {
		    daikonArgs.add(config.getDaikonOptions().getPath());
		}
		else {
		    daikonArgs.add(System.getProperty(DAIKON_DEFAULT_OPTIONS_PROPERTY));
		}
		daikonArgs.add("-o");
		daikonArgs.add(fSerializedInvariants.getPath());
		if (config.getUseSimplify()) {
			daikonArgs.add("--suppress_redundant");
		}
		daikonArgs.addAll(config.getDaikonFlags());
		daikonArgs.add(takuanPerlInfo.getDecls());

		// We will send all the dtraces through stdin: if we pass them as arguments,
		// we might hit the limit on the maximum length of a command (ARG_MAX) that
		// the underlying POSIX execve(3p) system call has
		daikonArgs.add("-");

		LOGGER.info("Launching Daikon");
		LOGGER.debug("Arguments: {}", daikonArgs);

		final File fDaikonOut = new File(baseDir, DAIKON_STDOUT_FILENAME);
		final FileOutputStream daikonOutput = new FileOutputStream(fDaikonOut);
		try {
			final String[] args = daikonArgs.toArray(new String[daikonArgs.size()]);
			Pair<Long, Long> memUsage = invokeClassInSubprocess("daikon.Daikon", daikonOutput, new CatTask(takuanPerlInfo.getDTraces()), config.getDaikonTimeoutInMinutes(), JMX_RMI_PORT, args);
			if (memUsage != null) {
				performanceData.putMemoryUsage("Invariant generation (Daikon subprocess)", memUsage.getLeft(), memUsage.getRight());
			}
		} catch (Exception e) {
			throw new TakuanException(e);
		} finally {
			daikonOutput.flush();
			daikonOutput.close();
		}
		LOGGER.info("Finished running Daikon");

		return fSerializedInvariants;
	}

	private void runDaikonDiff(final File fDiff, String... args) throws Exception {
		LOGGER.info("Running Daikon diff, writing to {}", fDiff);

		final OutputStream fDiffOS = new FileOutputStream(fDiff);
		try {
			invokeClassInSubprocess("daikon.diff.Diff", fDiffOS, null, null, 19782, args);
		} finally {
			fDiffOS.close();
		}
	}

	private void runDaikonTextDiff(ConfigurationNode left, ConfigurationNode right, File fDiff, File fDiffU) throws IOException {
		final File fRightOutput = new File(right.getDir(), DAIKON_STDOUT_FILENAME);

		// Compare invariants
		final int totalInvariants = countInvariants(fRightOutput);
		final int differentInterestingInvariants = countLinesWithRegexp(fDiff, PATTERN_DIFF_INVARIANT);
		final int differentInvariants = countLinesWithRegexp(fDiffU, PATTERN_DIFF_INVARIANT);

		// Compare program points
		final int totalProgramPoints = countLinesWithRegexp(fRightOutput, PATTERN_DAIKON_STDOUT_PROGRAM_POINT);
		final int differentInterestingProgramPoints = countProgramPointsInDiffOutput(fDiff);
		final int differentUninterestingProgramPoints = countProgramPointsInDiffOutput(fDiffU);

		// Compare traces (TODO: take into account addRawDTraces here)
		final File fLeftDTraceDir = new File(left.getDir(), TAKUAN_DTRACE_DIRECTORY_NAME);
		final Map<String, File> leftRawTraces = buildNameToFileMap(fLeftDTraceDir, PATTERN_RAW_DTRACE_FILENAME);
		final File fRightDTraceDir = new File(right.getDir(), TAKUAN_DTRACE_DIRECTORY_NAME);
		final Map<String, File> rightRawTraces = buildNameToFileMap(fRightDTraceDir, PATTERN_RAW_DTRACE_FILENAME);
		final Map<String, File> leftFlatTraces = buildNameToFileMap(fLeftDTraceDir, PATTERN_FLAT_DTRACE_FILENAME);
		final Map<String, File> rightFlatTraces = buildNameToFileMap(fRightDTraceDir, PATTERN_FLAT_DTRACE_FILENAME);
		final boolean sameRawTraces = pairwiseEqual(leftRawTraces, rightRawTraces);
		final boolean sameFlatTraces = pairwiseEqual(leftFlatTraces, rightFlatTraces);

		final Map<String, String> values = new LinkedHashMap<String, String>();
		values.put("Path to .bpel", right.getBpel().getPath());
		values.put("Raw Traces", Integer.toString(rightRawTraces.size()));
		values.put("Flat Traces", Integer.toString(rightFlatTraces.size()));
		values.put("Same Raw Traces?", sameRawTraces ? "1" : "0");
		values.put("Same Flat Traces?", sameFlatTraces ? "1" : "0");
		values.put("Invariants", Integer.toString(totalInvariants));
		values.put("Different Invariants (interesting)", Integer.toString(differentInterestingInvariants));
		values.put("Different Invariants (all)", Integer.toString(differentInvariants));
		values.put("Program Points", Integer.toString(totalProgramPoints));
		values.put("Different Program Points (interesting)", Integer.toString(differentInterestingProgramPoints));
		values.put("Different Program Points (all)", Integer.toString(differentUninterestingProgramPoints));
		values.put("ID", right.getId());
		values.put("Path to .dtrace dir", fRightDTraceDir.getPath());

		appendToCSV(new File(left.getDir(), "text-comparisons.csv"), values);
	}

	private TakuanPerlLineProcessor runTakuanPerl(ConfigurationNode config,
			final File baseDir, final Collection<File> logFiles, final Collection<File> rawDTraces,
			Set<File> cGraphs, final File takuanPerlDir, final File fDecls,
			final BPELProcessDefinition instrumentedDef) throws IOException,
			InterruptedException, TakuanException {
		List<String> takuanPerlArgs = new ArrayList<String>();
		takuanPerlArgs.add(System.getProperty(TAKUAN_PERL_CMD_PROPERTY));
		takuanPerlArgs.addAll(config.getFlags());
		takuanPerlArgs.add("--metrics"); // Generate metrics about the generated declarations
		takuanPerlArgs.add("--no-java"); // We will call Daikon on our own
		takuanPerlArgs.add("--output-dir");
		takuanPerlArgs.add(takuanPerlDir.getPath());
		takuanPerlArgs.add(fDecls.getPath());
		takuanPerlArgs.add(instrumentedDef.getFile().getPath());

		// We will pass the rest of the arguments through stdin, to avoid problems
		// when we have 7000+ files (and we hit the inherent limitation of ARG_MAX in
		// POSIX-based systems - see execve(3p)
		takuanPerlArgs.add("-");

		final TakuanPerlLineProcessor storeDeclsDTracesProcessor = new TakuanPerlLineProcessor();
		LOGGER.info("Launching takuan-perl");
		LOGGER.debug("Arguments: {}", takuanPerlArgs);
		ProcessBuilder pb = new ProcessBuilder();
		pb.command(takuanPerlArgs);
		pb.redirectErrorStream(true);
		final Process proc = pb.start();

		final BufferedReader stdout = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		final PrintStream stdin = new PrintStream(proc.getOutputStream());
		final Writer logWriter = new FileWriter(new File(baseDir, "takuan-perl" + ".log"));
		try {
			for (File logFile : logFiles) {
				stdin.println(logFile.getPath());
			}
			for (File rawDTrace : rawDTraces) {
				stdin.println(rawDTrace.getPath());
			}
			for (File cGraph : cGraphs) {
				stdin.println(cGraph.getPath());
			}
			stdin.close();

			String line;
			while ((line = stdout.readLine()) != null) {
				LOGGER.debug("takuan-perl: {}", line);
				logWriter.write(line);
				logWriter.write('\n');
				storeDeclsDTracesProcessor.process(line);
			}
			if (proc.waitFor() != 0) {
				throw new TakuanException("takuan-perl finished with non-zero status code " + proc.exitValue());
			}
		} finally {
			stdout.close();
			logWriter.close();
			stdin.close();
		}
		return storeDeclsDTracesProcessor;
	}

	private void runBPELUnit(ConfigurationNode config,
			final BPELProcessDefinition instrumentedDef,
			final File activeBPELDir) throws ParserConfigurationException,
			SAXException, IOException, ConfigurationException,
			SpecificationException, DeploymentException, BPELPackagingException, XPathExpressionException {

		File copiedDataSrc = null;
		if (config.getUseAsDataSrc() != null) {
			copiedDataSrc = new File(config.getBpts().getParentFile(), "data.src");
			FileUtils.copyFile(config.getUseAsDataSrc(), copiedDataSrc);
		}

		final BPELUnitSpecification bpts = new BPELUnitSpecification(config.getBpts());
		LOGGER.info("Starting to run the BPELUnit test suite at {} on port " + config.getBpelUnitPort(), bpts.getFile());
		bpts.setDeploymentDirectory(new File(activeBPELDir, "bpr"));
		bpts.setEnginePort(config.getActiveBPELPort());
		bpts.setBPELUnitPort(config.getBpelUnitPort());
		final ExecutionResults results = bpts.run(instrumentedDef);

		// Move the BPELUnit XML report to the base directory
		FileUtils.copyFile(results.getTestReportFile(), new File(config.getDir(), "bpelunit.xml"));
		results.getTestReportFile().delete();
		if (copiedDataSrc != null) { copiedDataSrc.delete(); }

		LOGGER.info("Finished running the BPELUnit test suite", bpts.getFile());
	}

	/* --- UTILITY METHODS --- */

	private void addReferencedActiveBPELLogs(
			ConfigurationDependencyGraph depGraph,
			final ConfigurationNode config,
			final Set<File> activeBPELLogs,
			final MutableLong totalWallMillis,
			final MutableLong totalCPUMillis) throws TakuanException
	{
		for (ConfigurationNode other : depGraph.getDescendants(config, EnumSet.of(ADD_LOGS))) {
			final ExecutionLogOptions cacheKey = new ExecutionLogOptions(other);
			final CachedValue<File> otherCached = cachedActiveBPELDirs.get(cacheKey);
			final File otherDir = otherCached.getValue();
			if (otherDir == null) {
				throw new TakuanException("Could not find a cached directory among " + cachedActiveBPELDirs + " with the options " + cacheKey);
			}

			final Set<File> otherLogs = listActiveBPELLogs(otherDir);
			activeBPELLogs.addAll(otherLogs);
			totalWallMillis.add(otherCached.getWalltimeMillis());
			totalCPUMillis.add(otherCached.getCputimeMillis());
		}
	}

	private void addReferencedRawDTraces(
			ConfigurationDependencyGraph depGraph,
			final ConfigurationNode config,
			final Set<File> rawDTraces,
			final MutableLong totalWallMillis,
			final MutableLong totalCPUMillis)
	{
		for (ConfigurationNode other : depGraph.getDescendants(config, EnumSet.of(ADD_RAW_DTRACES))) {
			final CachedValue<File> otherExecStep = cachedActiveBPELDirs.get(new ExecutionLogOptions(other));
			final File otherTakuanDir = new File(other.getDir(), TAKUAN_DTRACE_DIRECTORY_NAME);
			final Set<File> otherRawDTraces = listFiles(otherTakuanDir, PATTERN_RAW_DTRACE_FILENAME);
			rawDTraces.addAll(otherRawDTraces);
			totalWallMillis.add(otherExecStep.getWalltimeMillis());
			totalWallMillis.add(otherExecStep.getCputimeMillis());
		}
	}

	private void appendToCSV(final File fCSV, final Map<String, String> values)	throws IOException {
		final PrintWriter pw = new PrintWriter(new FileWriter(fCSV, true));

		try {
			// We'll always print the header, in case it's different in some cases
			// or if we have runs from previous versions
			printCSVRow(pw, values.keySet());
			printCSVRow(pw, values.values());
		} finally {
			pw.close();
		}
	}

	/**
	 * Builds a map from the basename of each file in the directory
	 * <code>dir</code> matching <code>pattern</code> to its actual File object.
	 */
	private Map<String, File> buildNameToFileMap(File dir, Pattern pattern) {
		final Map<String, File> results = new HashMap<String, File>();

		for (File f : dir.listFiles()) {
			if (pattern.matcher(f.getName()).matches()) {
				results.put(f.getName(), f);
			}
		}

		return results;
	}

	private Set<File> collectComparabilityGraphs(ConfigurationDependencyGraph depGraph,
			final ConfigurationNode config) {
		final Set<File> cGraphs = new HashSet<File>();
		for (ConfigurationNode other : depGraph.getOutgoing(config, EnumSet.of(ConfigurationDependencyType.ADD_RAW_DTRACES)).keySet()) {
			cGraphs.add(
				new File(new File(other.getDir(), TAKUAN_DTRACE_DIRECTORY_NAME),
				TAKUAN_CGRAPHS_FILENAME));
		}
		return cGraphs;
	}

	private void collectReferencesToPreviousExecutions(ConfigurationDependencyGraph depGraph,
			final ConfigurationNode config, final Set<File> activeBPELLogs,
			final Set<File> rawDTraces,
			final MutableLong totalWallMillisFromReferences,
			final MutableLong totalCPUMillisFromReferences) throws TakuanException
	{
		addReferencedActiveBPELLogs(depGraph, config, activeBPELLogs,
				totalWallMillisFromReferences, totalCPUMillisFromReferences);
		addReferencedRawDTraces(depGraph, config, rawDTraces,
				totalWallMillisFromReferences, totalCPUMillisFromReferences);
	}

	private int countProgramPointsInDiffOutput(File fDiff) throws IOException {
		final BufferedReader reader = new BufferedReader(new FileReader(fDiff));

		try {
			final Pattern ppMatcher = Pattern.compile("^<(.*):::(?:ENTER|EXIT)");
			final Set<String> programPoints = new HashSet<String>();
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				Matcher matcher = ppMatcher.matcher(line);
				if (matcher.matches()) {
					programPoints.add(matcher.group(1));
				}
			}

			return programPoints.size();
		} finally {
			reader.close();
		}
	}

	private int countLinesWithRegexp(File file, Pattern pattern) throws IOException {
		final BufferedReader reader = new BufferedReader(new FileReader(file));

		try {
			int count = 0;
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				if (pattern.matcher(line).matches()) {
					++count;
				}
			}

			return count;
		} finally {
			reader.close();
		}
	}

	private int countInvariants(File fLeftOutput) throws IOException {
		if (!fLeftOutput.exists()) return 0;

		final BufferedReader reader = new BufferedReader(new FileReader(fLeftOutput));

		try {
			// Look for the first program point
			String line;
			while ((line = reader.readLine()) != null && !line.contains(":::"));
			if (line == null) {
				// We reached EOF before finding the first program point - just return 0 
				return 0;
			}

			// Count everything that doesn't have ::: or the separator as an invariant now
			int count = 0;
			while ((line = reader.readLine()) != null) {
				if (!line.contains(":::") && !line.contains("===")) {
					++count;
				}
			}

			// Do not count the very last line (it's "Exiting Daikon.")
			return count - 1;
		} finally {
			reader.close();
		}
	}

	private void doneWithConfiguration(ConfigurationDependencyGraph depGraph, ConfigurationNode config) {
		LOGGER.info("Finished running configuration with output directory {}", config.getDir());
	
		removeUnneededExecutionLogs(depGraph, config);
		removeUnneededDTraces(depGraph, config);
	}

	private void ensureDirectoryExists(final File mainDir)
			throws TakuanException {
		if (mainDir.exists()) {
			if (!mainDir.isDirectory()) {
				throw new TakuanException("Main ActiveBPEL directory " + mainDir + " exists and is not a file");
			}
		} else if (!mainDir.mkdirs()) {
			throw new TakuanException("Failed to create the main directory for ActiveBPEL at " + mainDir);
		}
	}

	private Pair<Long, Long> invokeClassInSubprocess(final String className, OutputStream os, OutputStreamRunnable stdinTask, Integer timeLimitInMinutes, int jmxPort, final String... args)
			throws TakuanException
	{
		List<String> lArgs = new ArrayList<String>();
		lArgs.add("java");
		final String daikonJVMFlags = System.getProperty(DAIKON_JVM_FLAGS_PROPERTY);
		if (daikonJVMFlags != null) {
			lArgs.addAll(Arrays.asList(daikonJVMFlags.split("\\s+")));
		}

		// See: http://stackoverflow.com/questions/856881
		// This is inherently insecure, but Takuan shouldn't be run in a production system anyway
		lArgs.add("-Dcom.sun.management.jmxremote.port=" + jmxPort);
		lArgs.add("-Dcom.sun.management.jmxremote.local.only=false");
		lArgs.add("-Dcom.sun.management.jmxremote.authenticate=false");
		lArgs.add("-Dcom.sun.management.jmxremote.ssl=false");

		lArgs.add("-cp");
		lArgs.add(System.getProperty("java.class.path"));
		lArgs.add(className);
		lArgs.addAll(Arrays.asList(args));

		ProcessBuilder pb = new ProcessBuilder(lArgs);
		pb.redirectErrorStream(true);
		Process process;
		try {
			LOGGER.debug("Launching process with args: {}", lArgs);
			process = pb.start();
		} catch (IOException e) {
			throw new TakuanException("Could not start process", e);
		}

		// Try to obtain a JMX connection to watch the process' memory usage
		MemoryWatcher mw = getMemoryWatcherFor(jmxPort);
		Thread tMWSubprocess = null; 
		if (mw != null) {
			tMWSubprocess = new Thread(mw, "MemoryWatcher-Subprocess");
			tMWSubprocess.start();
		}

		// If a time limit has been set, start the watcher thread now
		Thread tTimeoutThread = null;
		if (timeLimitInMinutes != null) {
			final int timeLimitInMillis = timeLimitInMinutes * 60 * 1000;
			tTimeoutThread = new Thread(new ProcessTimeoutTask(timeLimitInMillis, Thread.currentThread(), process));
			tTimeoutThread.start();
		}

		// Dump the process' stderr and stdout into a file and wait for completion
		try {
			Thread stdinThread = null;
			if (stdinTask != null) {
				stdinTask.setOutputStream(process.getOutputStream());
				stdinThread = new Thread(stdinTask, "Stdin-Subprocess");
				stdinThread.start();
			}
			final BufferedReader pReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			final PrintWriter osWriter = new PrintWriter(new OutputStreamWriter(os));
			try {
				String line;
				while ((line = pReader.readLine()) != null) {
					osWriter.println(line);
				}
			} catch (IOException ex) {
				if (Thread.interrupted()) {
					throw new InterruptedException("Reading the output of the process was interrupted");
				}
				else {
					throw ex;
				}
			} finally {
				pReader.close();
				osWriter.flush();
				if (stdinThread != null) {
					stdinThread.interrupt();
				}
			}

			int statusCode = process.waitFor();
			if (statusCode == 0) {
				LOGGER.info("Process finished successfully", lArgs);
			}
			if (tMWSubprocess != null) {
				return mw.getMaxValues();
			}
		} catch (InterruptedException ex) {
			LOGGER.warn("Wait was interrupted");
			process.destroy();
		} catch (IOException ex) {
			LOGGER.warn("I/O exception while running process", ex);
		} finally {
			if (tMWSubprocess != null) {
				// Stop the subprocess' memory watcher if started, no matter what
				tMWSubprocess.interrupt();
			}
		}

		return null;
	}

	private MemoryWatcher getMemoryWatcherFor(int jmxPort) {
		MemoryWatcher mw = null;
		try {
			mw = MemoryWatcher.fromPort(jmxPort, MEMORY_WATCH_INTERVAL);
		} catch (Exception ex) {
			LOGGER.error("Could not create the memory watcher: will not be able to report memory usage", ex);
		}
		return mw;
	}

	private Set<File> listActiveBPELLogs(File activeBPELDir) throws TakuanException {
		final File activeBPELLogsDir = new File(activeBPELDir, ACTIVEBPEL_LOGS_SUBDIR);
		return listFiles(activeBPELLogsDir, PATTERN_LOG_FILENAME);
	}

	private Set<File> listFiles(final File dir, final Pattern regex) {
		final File[] rawDTraces = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File f, String fname) {
				return regex.matcher(fname).matches();
			}});

		return new LinkedHashSet<File>(Arrays.asList(rawDTraces));
	}

	private boolean pairwiseEqual(Map<String, File> leftRawTraces, Map<String, File> rightRawTraces) throws IOException {
		if (!leftRawTraces.keySet().equals(rightRawTraces.keySet())) {
			return false;
		}
	
		for (Map.Entry<String, File> entry : leftRawTraces.entrySet()) {
			final File leftFile = entry.getValue();
			final File rightFile = rightRawTraces.get(entry.getKey());
			if (!FileUtils.contentEquals(leftFile, rightFile)) {
				return false;
			}
		}
	
		return true;
	}

	private void performComparison(ConfigurationNode left, ConfigurationNode right) throws Exception {
		final File fLeftInvGZ = new File(left.getDir(), DAIKON_INVGZ_FILENAME);
		final File fRightInvGZ = new File(right.getDir(), DAIKON_INVGZ_FILENAME);
	
		try {
			final String namePrefix = right.getId() + "_";
	
			// Regular differences
			final File fDiff = new File(left.getDir(), namePrefix + "diff.txt");
			runDaikonDiff(fDiff, fLeftInvGZ.getPath(), fRightInvGZ.getPath());
	
			// Including uninteresting invariants
			final File fDiffU = new File(left.getDir(), namePrefix + "diffU.txt");
			runDaikonDiff(fDiffU, "-u", fLeftInvGZ.getPath(), fRightInvGZ.getPath());
	
			// Statistics
			final File fStats = new File(left.getDir(), namePrefix + DAIKON_STATS_FILENAME);
			runDaikonDiff(fStats, "-s", fLeftInvGZ.getPath(), fRightInvGZ.getPath());
	
			// Text-based difference report (.csv file)
			runDaikonTextDiff(left, right, fDiff, fDiffU);
		} catch (Exception e) {
			LOGGER.error("There was an exception while comparing "
					+ fLeftInvGZ + " with " + fRightInvGZ, e);
			throw e;
		}
	}

	private void printCSVRow(final PrintWriter pw, final Collection<String> values) {
		boolean bFirst = true;
		for (String key : values) {
			if (bFirst) {
				bFirst = false;
			} else {
				pw.print(",");
			}
			pw.print(key);
		}
		pw.println();
	}

	private void removeUnneededExecutionLogs(ConfigurationDependencyGraph depGraph, ConfigurationNode config) {
		if (config.getKeepExecutionLogs()) {
			return;
		}
	
		final EnumSet<ConfigurationDependencyType> logEdgeTypes = EnumSet.of(SAME_LOGS, ADD_LOGS);
		if (!depGraph.getIncoming(config, logEdgeTypes).isEmpty()) {
			return;
		}
	
		final File logDir = new File(config.getDir(), ACTIVEBPEL_RESULT_SUBDIR);
		if (logDir.isDirectory()) {
			try {
				FileUtils.deleteDirectory(logDir);
			} catch (IOException e) {
				LOGGER.error("Could not delete unneeded ActiveBPEL log directory", e);
			}
		}
	
		for (ConfigurationNode dependency : depGraph.getOutgoing(config, logEdgeTypes).keySet()) {
			depGraph.removeEdge(config, dependency, logEdgeTypes);
			removeUnneededExecutionLogs(depGraph, dependency);
		}
	}

	private void removeUnneededDTraces(ConfigurationDependencyGraph depGraph, ConfigurationNode config) {
		if (config.getKeepDTraces()) {
			return;
		}

		final File takuanDir = new File(config.getDir(), TAKUAN_DTRACE_DIRECTORY_NAME);
		final boolean notNeededForComparisons = depGraph.getIncoming(config, EnumSet.of(COMPARE_WITH)).isEmpty();
		final boolean notNeededForAddDTrace   = depGraph.getIncoming(config, EnumSet.of(ADD_RAW_DTRACES)).isEmpty();

		if (notNeededForComparisons) {
			if (takuanDir.isDirectory()) {
				for (File flatDTrace : listFiles(takuanDir, PATTERN_FLAT_DTRACE_FILENAME)) {
					flatDTrace.delete();
				}
			}
		}
		if (notNeededForComparisons && notNeededForAddDTrace) {
			if (takuanDir.isDirectory()) {
				for (File rawDTrace : listFiles(takuanDir, PATTERN_RAW_DTRACE_FILENAME)) {
					rawDTrace.delete();
				}
			}
		}

		final EnumSet<ConfigurationDependencyType> edgesToRemove = EnumSet.noneOf(ConfigurationDependencyType.class);
		if (notNeededForComparisons) {
			edgesToRemove.add(COMPARE_WITH);
		}
		if (notNeededForAddDTrace) {
			edgesToRemove.add(ADD_RAW_DTRACES);
		}
		if (!edgesToRemove.isEmpty()) {
			for (ConfigurationNode dependency : depGraph.getOutgoing(config, edgesToRemove).keySet()) {
				depGraph.removeEdge(config, dependency, edgesToRemove);
				removeUnneededDTraces(depGraph, dependency);
			}
		}
	}
}
