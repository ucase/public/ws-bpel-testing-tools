package es.uca.webservices.takuan.cache;

/**
 * Cached value which also stores its own performance statistics.
 *
 * @author Antonio Garcia-Dominguez
 */
public class CachedValue<T> {

	private final T value;

	protected final long elapsedWallMillis;
	protected final long elapsedCPUMillis;
	protected final long maxUsedBytes;
	protected final long maxCommittedBytes;

	public CachedValue(T value, long elapsedWallMillis, long elapsedCPUMillis, long maxUsedBytes, long maxCommittedBytes) {
		this.value = value;
		this.elapsedWallMillis = elapsedWallMillis;
		this.elapsedCPUMillis = elapsedCPUMillis;
		this.maxUsedBytes = maxUsedBytes;
		this.maxCommittedBytes = maxCommittedBytes;
	}

	public T getValue() {
		return value;
	}

	public long getWalltimeMillis() {
		return elapsedWallMillis;
	}

	public long getCputimeMillis() {
		return elapsedCPUMillis;
	}

	public long getUsedBytes() {
		return maxUsedBytes;
	}

	public long getCommittedBytes() {
		return maxCommittedBytes;
	}

}