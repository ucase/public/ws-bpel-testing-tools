package es.uca.webservices.takuan.graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Generic multigraph implementation using EnumSets. There can be multiple edges
 * between vertices, each "marked" with a different value of a Java enumeration.
 * 
 * @author Antonio García-Domínguez
 */
public class EnumSetMultigraph<Vertex, EnumType extends Enum<EnumType>> {

	private final Map<Vertex, Map<Vertex, EnumSet<EnumType>>> outgoingEdges = new LinkedHashMap<Vertex, Map<Vertex, EnumSet<EnumType>>>();
	private final Map<Vertex, Map<Vertex, EnumSet<EnumType>>> incomingEdges = new LinkedHashMap<Vertex, Map<Vertex, EnumSet<EnumType>>>();

	/**
	 * Adds a vertex to the graph, without any incoming or outgoing edges, if it
	 * wasn't already in the graph.
	 */
	public void addVertex(Vertex v) {
		if (!outgoingEdges.containsKey(v)) {
			outgoingEdges.put(v, new LinkedHashMap<Vertex, EnumSet<EnumType>>());
			incomingEdges.put(v, new LinkedHashMap<Vertex, EnumSet<EnumType>>());
		}
	}

	/**
	 * Removes a vertex to the graph and all its outgoing and incoming edges, if
	 * it was already in the graph.
	 */
	public void removeVertex(Vertex v) {
		outgoingEdges.remove(v);
		incomingEdges.remove(v);
	}

	/**
	 * Returns the set of all vertices in the graph.
	 */
	public Set<Vertex> getVertices() {
		return outgoingEdges.keySet();
	}

	/**
	 * Adds the edges corresponding to the values in <code>edges</code> from
	 * <code>src</code> to <code>dst</code>. If they already existed, they will
	 * not be added again. If the vertices are not in the graph, they will be
	 * added to the graph.
	 * 
	 * @param src
	 *            Source vertex.
	 * @param dst
	 *            Destination vertex.
	 * @param edges
	 *            Enum values representing the edges between
	 *            <code>src>/code> and <code>dst</code> to be added.
	 */
	public void addEdge(Vertex src, Vertex dst, EnumSet<EnumType> newEdges) {
		addVertex(src);
		addVertex(dst);

		final Map<Vertex, EnumSet<EnumType>> srcOut = outgoingEdges.get(src);
		final Map<Vertex, EnumSet<EnumType>> dstIn = incomingEdges.get(dst);
		if (srcOut.containsKey(dst)) {
			// There is at least one edge from src to dst
			srcOut.get(dst).addAll(newEdges);
			dstIn.get(src).addAll(newEdges);
		} else {
			srcOut.put(dst, newEdges);
			dstIn.put(src, newEdges);
		}
	}

	/**
	 * Removes the edges corresponding to the values in <code>edges</code> from
	 * <code>src</code> to <code>dst</code>, if they exist. Vertices will not be
	 * added or removed automatically to or from the graph.
	 * 
	 * @param src
	 *            Source vertex.
	 * @param dst
	 *            Destination vertex.
	 * @param edges
	 *            Enum values representing the edges between
	 *            <code>src>/code> and <code>dst</code> to be removed.
	 */
	public void removeEdge(Vertex src, Vertex dst, EnumSet<EnumType> edges) {
		final Map<Vertex, EnumSet<EnumType>> srcOut = outgoingEdges.get(src);
		if (srcOut != null && srcOut.containsKey(dst)) {
			final Map<Vertex, EnumSet<EnumType>> dstIn = incomingEdges.get(dst);
			final EnumSet<EnumType> outEdges = srcOut.get(dst);
			final EnumSet<EnumType> inEdges = dstIn.get(src);

			outEdges.removeAll(edges);
			inEdges.removeAll(edges);
			if (outEdges.isEmpty()) {
				srcOut.remove(dst);
				dstIn.remove(src);
			}
		}
	}

	/**
	 * Returns the edges from <code>src</code> to <code>dst</code>. If no such
	 * edges exist or if any of the vertices do not belong to the graph, returns
	 * <code>null</code>.
	 */
	public EnumSet<EnumType> getEdgesBetween(Vertex src, Vertex dst) {
		if (outgoingEdges.containsKey(src)) {
			final Map<Vertex, EnumSet<EnumType>> srcOut = outgoingEdges
					.get(src);
			return srcOut.containsKey(dst) ? srcOut.get(dst) : null;
		} else {
			return null;
		}
	}

	/**
	 * Returns all the incoming edges of the form <code>(x, dst)</code> for a
	 * vertex <code>dst</code> that belong to any of the enum values in
	 * <code>anyOf</code>.
	 * 
	 * @param dst
	 *            Vertex whose incoming edges are to be listed.
	 * @param anyOf
	 *            Edge types to be considered, or <code>null</code> if all types
	 *            are to be considered.
	 * @return If the vertex <code>dst</code> is part of the graph, returns an
	 *         unmodifiable map from the source vertices of the incoming edges
	 *         to the intersection of their edges with <code>anyOf</code>.
	 *         Otherwise, returns <code>null</code>.
	 */
	public Map<Vertex, EnumSet<EnumType>> getIncoming(Vertex dst,
			EnumSet<EnumType> anyOf) {
		if (incomingEdges.containsKey(dst)) {
			return filterEdgesWithAnyOf(incomingEdges.get(dst), anyOf);
		}
		else {
			return null;
		}
	}

	/**
	 * Convenience version of {@link #getIncoming(Vertex, EnumSet)} considering
	 * all edge types.
	 */
	public Map<Vertex, EnumSet<EnumType>> getIncoming(Vertex dst) {
		return getIncoming(dst, null);
	}

	/**
	 * Returns all the outgoing edges of the form <code>(src, x)</code> for a
	 * vertex <code>src</code> that belong to any of the enum values in
	 * <code>anyOf</code>.
	 * 
	 * @param src
	 *            Vertex whose outgoing edges are to be listed.
	 * @param anyOf
	 *            Edge types to be considered, or <code>null</code> if all types
	 *            are to be considered.
	 * @return If the vertex <code>dst</code> is part of the graph, returns an
	 *         unmodifiable map from the destination vertices of the outgoing
	 *         edges to the intersection of their edges with <code>anyOf</code>.
	 *         Otherwise, returns <code>null</code>.
	 */
	public Map<Vertex, EnumSet<EnumType>> getOutgoing(Vertex src,
			EnumSet<EnumType> anyOf) {
		if (outgoingEdges.containsKey(src)) {
			return filterEdgesWithAnyOf(outgoingEdges.get(src), anyOf);
		} else {
			return null;
		}
	}

	/**
	 * Convenience version of {@link #getOutgoing(Vertex, EnumSet)} considering
	 * all edge types.
	 */
	public Map<Vertex, EnumSet<EnumType>> getOutgoing(Vertex src) {
		return getOutgoing(src, null);
	}

	/**
	 * Returns <code>true</code> if the graph has a cycle when using the edges
	 * of the types in <code>anyOf</code> (all types if <code>anyOf</code> is
	 * <code>null</code>). Otherwise, returns <code>false</code>.
	 */
	public boolean isCyclic(EnumSet<EnumType> anyOf) {
		final Set<Vertex> visited = new HashSet<Vertex>();
		final Set<Vertex> visiting = new HashSet<Vertex>();

		for (Vertex v : outgoingEdges.keySet()) {
			if (!visited.contains(v) && hasCycles(v, anyOf, visited, visiting)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Convenience version of {@link #isCyclic(EnumSet)} considering all edge types.
	 */
	public boolean isCyclic() {
		return isCyclic(null);
	}

	/**
	 * Returns a list with the vertices of the graph in reverse topological
	 * order. This means that for every edge from i to j that is of one of the
	 * types in <code>anyOf</code>, j will come before i. If <code>anyOf</code>
	 * is <code>null</code>, all edge types will be considered.
	 */
	public List<Vertex> reverseTopologicalSort(EnumSet<EnumType> anyOf) {
		final Set<Vertex> result = new LinkedHashSet<Vertex>();
		for (Vertex v : outgoingEdges.keySet()) {
			if (!result.contains(v)) {
				addDescendants(v, anyOf, result);
			}
			result.add(v);
		}
		return new ArrayList<Vertex>(result);
	}

	public List<Vertex> reverseTopologicalSort() {
		return reverseTopologicalSort(null);
	}

	/**
	 * <p>
	 * Returns a sequence with all the descendants of the <code>root</code>
	 * vertex that are connected through edges with any of the types in
	 * <code>anyOf</code>. If <code>anyOf</code> is <code>null</code>, all edge
	 * types will be considered.
	 * </p>
	 * 
	 * <p>
	 * <code>root</code> itself is not included. The nodes are in reverse
	 * topological order (see {@link #reverseTopologicalSort()}). If the vertex
	 * is not contained in the graph, returns <code>null</code>.
	 * </p>
	 */
	public List<Vertex> getDescendants(Vertex root, EnumSet<EnumType> anyOf) {
		if (outgoingEdges.containsKey(root)) {
			final Set<Vertex> descendants = new LinkedHashSet<Vertex>();
			for (Vertex child : filterEdgesWithAnyOf(outgoingEdges.get(root), anyOf).keySet()) {
				if (!descendants.contains(child)) {
					addDescendants(child, anyOf, descendants);
				}
			}
			return new ArrayList<Vertex>(descendants);
		} else {
			return null;
		}
	}

	/**
	 * Convenience version of {@link #getDescendants(Object, EnumSet)}
	 * considering all edge types.
	 */
	public List<Vertex> getDescendants(Vertex root) {
		return getDescendants(root, null);
	}

	private void addDescendants(Vertex root, EnumSet<EnumType> anyOf, Set<Vertex> descendants) {
		for (Vertex dst : filterEdgesWithAnyOf(outgoingEdges.get(root), anyOf).keySet()) {
			if (!descendants.contains(dst)) {
				addDescendants(dst, anyOf, descendants);
			}
		}
		descendants.add(root);
	}

	private Map<Vertex, EnumSet<EnumType>> filterEdgesWithAnyOf(
			final Map<Vertex, EnumSet<EnumType>> edges, EnumSet<EnumType> anyOf) {
		if (anyOf != null) {
			Map<Vertex, EnumSet<EnumType>> filteredIn = new LinkedHashMap<Vertex, EnumSet<EnumType>>();
			for (Map.Entry<Vertex, EnumSet<EnumType>> entry : edges.entrySet()) {
				final EnumSet<EnumType> intersection = EnumSet.copyOf(entry.getValue());
				intersection.retainAll(anyOf);
				if (!intersection.isEmpty()) {
					filteredIn.put(entry.getKey(), intersection);
				}
			}
			return Collections.unmodifiableMap(filteredIn);
		} else {
			return Collections.unmodifiableMap(edges);
		}
	}

	private boolean hasCycles(Vertex src, EnumSet<EnumType> anyOf, Set<Vertex> visited, Set<Vertex> visiting) {
		visiting.add(src);
	
		for (Vertex dst : filterEdgesWithAnyOf(outgoingEdges.get(src), anyOf).keySet()) {
			if (visiting.contains(dst) || !visited.contains(dst) && hasCycles(dst, anyOf, visited, visiting)) {
				return true;
			}
		}

		visiting.remove(src);
		visited.add(src);
		return false;
	}
}
