package es.uca.webservices.takuan.io;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Watches an ActiveBPEL log directory. Useful for waiting until all logs have
 * been fully written. Implements a timeout based on a number of retries since
 * the last change).
 */
public class ActiveBPELLogDirectoryWatcher {

	private static final Logger LOGGER = LoggerFactory.getLogger(ActiveBPELLogDirectoryWatcher.class);

	private final File directory;
	private final int maxRetries;
	private final int retryWaitInMillis;

	public ActiveBPELLogDirectoryWatcher(File directory, int maxRetries, int retryWaitInMillis) {
		this.directory = directory;
		this.maxRetries = maxRetries;
		this.retryWaitInMillis = retryWaitInMillis;
	}

	public File getDirectory() {
		return directory;
	}

	public int getMaxRetries() {
		return maxRetries;
	}

	public int getRetryWaitInMillis() {
		return retryWaitInMillis;
	}

	public void waitForCompletion() throws InterruptedException {
		int nRetries = 0;

		final Set<File> allFiles = new HashSet<File>();
		final Set<File> pendingFiles = new HashSet<File>();
		final Map<File, Long> fileSizes = new HashMap<File, Long>();

		do {
			if (Thread.interrupted()) {
				throw new InterruptedException();
			}
			if (nRetries > 0) {
				Thread.sleep(retryWaitInMillis);
			}

			// Check for new logs
			final Set<File> newLogs = listLogs();
			newLogs.removeAll(allFiles);
			if (newLogs.size() > 0) {
				nRetries = 0;
				allFiles.addAll(newLogs);
				pendingFiles.addAll(newLogs);
				for (File fNew : newLogs) {
					fileSizes.put(fNew, fNew.length());
				}
			}

			// Check for size changes
			for (File fPend : pendingFiles) {
				final long currentSize = fPend.length();
				if (currentSize != fileSizes.get(fPend).longValue()) {
					nRetries = 0;
					fileSizes.put(fPend, currentSize);
				}
			}

			// Check for completed executions
			for (Iterator<File> i = pendingFiles.iterator(); i.hasNext();) {
				try {
					final File fPend = i.next();
					if (Tail.lastLine(fPend).contains("[/process]")) {
						nRetries = 0;
						i.remove();
					}
				} catch (IOException e) {
					LOGGER.warn("I/O exception: ignoring pending file", e);
					i.remove();
				}
			}

			++nRetries;
		} while (!pendingFiles.isEmpty() && nRetries < maxRetries);
	}

	private Set<File> listLogs() {
		final File[] logs = directory.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File f, String fname) {
				return fname.endsWith(".log");
			}
		});

		return new HashSet<File>(Arrays.asList(logs));
	}
}
