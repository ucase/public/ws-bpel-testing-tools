package es.uca.webservices.takuan.conf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

/**
 * <p>
 * Loads a .yaml file, converting it into a list of configurations and expanding
 * the original graph into a rooted tree. This is required in order to implement
 * inheritance of configuration values in YAML.
 * </p>
 * 
 * <p>
 * Copyright (C) 2011-2012 Antonio García-Domínguez.
 * </p>
 * 
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * </p>
 * 
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * 
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <a href="http://www.gnu.org/licenses/">here</a>.
 * </p>
 */
public class ConfigurationLoader {

	private final Yaml yaml;
	private File baseDir, defaultBPEL, defaultBPTS;

	/**
	 * Creates a new loader. Relative paths will be resolved from the current
	 * directory: to change it, please use {@link #setBaseDir(File)}.
	 *
	 * @throws IOException Could not locate the current directory.
	 */
	public ConfigurationLoader() throws IOException {
		yaml = new Yaml(new Constructor(ConfigurationNode.class));
		baseDir = new File(".").getCanonicalFile();
	}

	/**
	 * Returns the base directory for resolving relative paths. By default,
	 * the current directory is used.
	 */
	public File getBaseDir() {
		return baseDir;
	}

	/**
	 * Changes the base directory for resolving relative paths.
	 *
	 * @param baseDir
	 *            New base directory to be set. This file should be the
	 *            canonical representative of that directory.
	 * @see #getBaseDir()
	 */
	public void setBaseDir(File baseDir) {
		this.baseDir = baseDir;
	}

	/**
	 * Returns the default .bpel to be used if none is specified in the .yaml file.
	 */
	public File getDefaultBPEL() {
		return defaultBPEL;
	}

	/**
	 * Changes the default .bpel to be used if none is specified in the .yaml file.
	 */
	public void setDefaultBPEL(File defaultBPEL) {
		this.defaultBPEL = defaultBPEL;
	}

	/**
	 * Returns the default .bpts to be used if none is specified in the .yaml file.
	 */
	public File getDefaultBPTS() {
		return defaultBPTS;
	}

	/**
	 * Changes the default .bpts to be used if none is specified in the .yaml file.
	 */
	public void setDefaultBPTS(File defaultBPTS) {
		this.defaultBPTS = defaultBPTS;
	}

	/**
	 * Loads every document in the provided file, using {@link #load(File)}.
	 * Relative paths will be resolved from the directory containing the
	 * provided YAML file.
	 *
	 * @throws IOException
	 *             Could not read the provided YAML file.
	 * @throws ConfigurationException
	 *             The configuration provided by the YAML file is not valid.
	 */
	public Configurations load(File fYAML) throws IOException, ConfigurationException {
		Configurations configs = load(new FileInputStream(fYAML));
		configs.setFile(fYAML);
		return configs;
	}

	/**
	 * Loads every document in the provided input stream. Flattens the
	 * configuration trees into a linear sequence of fully initialized and
	 * validated configurations.
	 */
	public Configurations load(InputStream is) throws ConfigurationException
	{
		final List<ConfigurationNode> configs = new ArrayList<ConfigurationNode>();
		final Set<File> outputDirectories = new HashSet<File>();
		for (Object doc : yaml.loadAll(is)) {
			processConfiguration((ConfigurationNode)doc, outputDirectories, configs);
		}
		return new Configurations(configs);
	}

	private void processConfiguration(final ConfigurationNode config,
			final Set<File> outputDirectories, final List<ConfigurationNode> results)
			throws ConfigurationException {
		if (config.getChildren() == null || config.getChildren().isEmpty()) {
			// Leaf node: add it to the configurations
			if (config.getBpel() == null) {
				config.setBpel(defaultBPEL);
			}
			if (config.getBpts() == null) {
				config.setBpts(defaultBPTS);
			}
			config.validate();
			config.resolveRelativePaths(baseDir);
			results.add(config);
		} else {
			if (config.getId() != null) {
				// This check is required since we do not fully validate inner nodes with this loader
				// See Configuration#validate
				throw new ConfigurationException("Inner nodes in the experiment tree cannot use the 'id' attribute");
			}

			// Inner node: clone children, setting their parent field and traversing them
			// (a child may be shared among several parents in the original YAML graph)
			for (ConfigurationNode child : config.getChildren()) {
				final ConfigurationNode clonedChild = child.clone();
				clonedChild.setParent(config);
				processConfiguration(clonedChild, outputDirectories, results);
			}
		}
	}

}
