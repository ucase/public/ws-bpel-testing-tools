package es.uca.webservices.takuan.performance;

import java.util.LinkedHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Specialized hashmap for logging various performance metrics.
 *
 * @author Antonio Garcia-Dominguez
 */
public class PerformanceMetrics extends LinkedHashMap<String, String> {

	private static final int BYTES_TO_MB_FACTOR = 1024*1024;
	private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceMetrics.class);
	
	private static final long serialVersionUID = 1L;

	public void putMemoryUsage(String taskName, long usedBytes, long committedBytes) {
		LOGGER.info(taskName + ": max used " + usedBytes/BYTES_TO_MB_FACTOR + "MiB, max committed " + committedBytes/BYTES_TO_MB_FACTOR + "MiB");
		put(taskName + " (bytes used)", Long.toString(usedBytes));
		put(taskName + " (bytes committed)", Long.toString(committedBytes));
	}

	public void putTimeUsage(String taskName, long walltimeMillis, long cputimeMillis) {
		LOGGER.info(taskName + ": wall time " + walltimeMillis + "ms, CPU time " + cputimeMillis + "ms");
		put(taskName + " (wall time in ms)", Long.toString(walltimeMillis));
		put(taskName + " (CPU time in ms)", Long.toString(cputimeMillis));
	}

	public void putCached(String taskName, boolean isCached) {
		put(taskName + " (cached)", "" + isCached);
	}

}
