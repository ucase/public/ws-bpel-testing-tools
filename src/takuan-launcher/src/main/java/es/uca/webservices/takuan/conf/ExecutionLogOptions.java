package es.uca.webservices.takuan.conf;

import java.io.File;


/**
 * Immutable Java bean which stores the subset of options which affect the
 * execution logs produced by the WS-BPEL composition.
 *
 * @author Antonio Garcia-Dominguez
 */
public class ExecutionLogOptions extends InstrumentationOptions {

	private final File bpts, useAsDataSrc;

	public ExecutionLogOptions(File bpel, File bpts, File useAsDataSrc,
			Boolean instrumentSeqByDefault, Boolean instrumentVarByDefault,
			Integer instrumentationDepth) {
		super(bpel, instrumentSeqByDefault, instrumentVarByDefault, instrumentationDepth);
		this.bpts = bpts;
		this.useAsDataSrc = useAsDataSrc;
	}

	public ExecutionLogOptions(ConfigurationNode config) {
		super(config);
		this.bpts = config.getBpts();
		this.useAsDataSrc = config.getUseAsDataSrc();
	}

	public File getBpts() {
		return bpts;
	}

	public File getUseAsDataSrc() {
		return useAsDataSrc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bpts == null) ? 0 : bpts.hashCode());
		result = prime * result
				+ ((useAsDataSrc == null) ? 0 : useAsDataSrc.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExecutionLogOptions other = (ExecutionLogOptions) obj;
		if (bpts == null) {
			if (other.bpts != null)
				return false;
		} else if (!bpts.equals(other.bpts))
			return false;
		if (useAsDataSrc == null) {
			if (other.useAsDataSrc != null)
				return false;
		} else if (!useAsDataSrc.equals(other.useAsDataSrc))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExecutionLogsCacheKey ["
				+ "bpel=" + getBpel()
				+ ", bpts=" + bpts
				+ ", instrumentSeqByDefault=" + getInstrumentSeqByDefault()
				+ ", instrumentVarByDefault=" + getInstrumentVarByDefault()
				+ ", instrumentationDepth="	+ getInstrumentationDepth()
				+ ", useAsDataSrc="	+ useAsDataSrc
				+ "]";
	}

}
