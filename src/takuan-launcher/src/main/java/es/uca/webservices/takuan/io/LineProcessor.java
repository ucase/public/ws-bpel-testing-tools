package es.uca.webservices.takuan.io;

/**
 * Interface for a class which filters the output of an external process.
 *
 * @author Antonio Garcia-Dominguez
 */
public interface LineProcessor {
	void process(String line);
}