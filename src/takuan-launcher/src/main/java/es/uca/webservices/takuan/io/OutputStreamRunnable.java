package es.uca.webservices.takuan.io;

import java.io.OutputStream;

/**
 * Runnable which wraps an output stream inside it. {@link #run()} should
 * only be called <emph>after</code> setting its output stream.
 */
public abstract class OutputStreamRunnable implements Runnable {

	private OutputStream os;

	public OutputStream getOutputStream() {
		return os;
	}

	public void setOutputStream(OutputStream os) {
		this.os = os;
	}

}
