package es.uca.webservices.takuan.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

/**
 * Simple partial implementations of the <code>tail(1)</code> UNIX command.
 *
 * @author Antonio García-Domínguez
 */
public final class Tail {

	/** Size of the buffer used by the functions in this class. */
	private final static int BUFSIZE = 1024;

	private Tail() {
		// do nothing
	}

	/**
	 * Reads the last line of a file, decoding it using the default platform encoding.
	 * Line terminator characters are stripped.
	 */
	public static String lastLine(File f) throws IOException {
		final byte[] buffer = new byte[BUFSIZE];
		final RandomAccessFile rf = new RandomAccessFile(f, "r");
		final long fSize = rf.length();

		try {
			long start = Math.max(0, fSize - BUFSIZE);
			long end = fSize;
			String line = "";
			int firstNewline;

			// We need to loop, in case the last line doesn't fit entirely in the buffer
			do {
				rf.seek(start);
				final int readBytes = rf.read(buffer, 0, (int)(end - start));
				if (readBytes <= 0) return line;

				// Go backwards, looking for the first CR or LF
				for (
					firstNewline = readBytes - 1;
					firstNewline >= 0 && buffer[firstNewline] != '\n' && buffer[firstNewline] != '\r';
					--firstNewline
				);

				// Is there anything to read past the first CR/LF?
				if (firstNewline < readBytes - 1) {
					final BufferedReader sR = new BufferedReader(
						new InputStreamReader(
							new ByteArrayInputStream(buffer, firstNewline + 1, readBytes - firstNewline - 1)));
					line = sR.readLine() + line;
				}

				if (firstNewline < 0) {
					if (start > 0) {
						// We couldn't find a CR/LF in the buffer: keep going back
						end = start;
						start = Math.max(0, start - BUFSIZE);
					}
					else {
						// There wasn't a single CR/LF in the file
						return line;
					}
				}
			} while (firstNewline < 0);

			return line;
		} finally {
			rf.close();
		}
	}
}
