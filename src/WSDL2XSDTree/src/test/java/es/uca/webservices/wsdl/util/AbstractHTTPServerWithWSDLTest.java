package es.uca.webservices.wsdl.util;

import org.apache.commons.io.FileUtils;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.junit.After;
import org.junit.Before;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Base class for all tests that need to simulate running a local HTTP
 * server serving WSDL documents through the network.
 */
public abstract class AbstractHTTPServerWithWSDLTest {
    protected static final String URL_PREFIX = "http://127.0.0.1:";
    protected static final int DEFAULT_SERVER_PORT = 50123;

    protected static final String RESOURCE_DIR = "src/test/resources/importFromURL/";
    protected static final String ORDERS_MAIN_QUERY = "/orders?wsdl";
    protected static final String ORDERS_IMP_QUERY = "/orders?wsdl=Orders.wsdl";
    protected static final String ORDERS_MAIN_FILE = "orders-main.wsdl";
    protected static final String ORDERS_IMP_FILE  = "orders-definitions.wsdl";

    private Server server;

    @Before
    public void setUp() throws Exception {
        final MapQueryToFileHandler urlMapHandler = new MapQueryToFileHandler();
        final Map<String, File> urlMap = urlMapHandler.getFileMap();
        urlMap.put(ORDERS_MAIN_QUERY, new File(RESOURCE_DIR, ORDERS_MAIN_FILE));
        urlMap.put(ORDERS_IMP_QUERY, new File(RESOURCE_DIR, ORDERS_IMP_FILE));

        final HandlerList handlers = new HandlerList();
        handlers.addHandler(urlMapHandler);
        handlers.addHandler(new DefaultHandler());

        // Random-looking port that can't be allocated by the IANA
        server = new Server(DEFAULT_SERVER_PORT);
        server.setHandler(handlers);
        server.start();
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
        server.join();
    }

    private static class MapQueryToFileHandler extends AbstractHandler {
        private Map<String, File> fileMap = new HashMap<String, File>();

        @Override
        public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            final String query = target + (request.getQueryString() != null ? "?" + request.getQueryString() : "");
            final File file = fileMap.get(query);
            if (file != null) {
                response.setContentType("application/wsdl+xml");
                response.setStatus(HttpServletResponse.SC_OK);
                baseRequest.setHandled(true);
                FileUtils.copyFile(file, response.getOutputStream());
            }
        }

        public Map<String, File> getFileMap() {
            return fileMap;
        }

        public void setFileMap(Map<String, File> fileMap) {
            this.fileMap = fileMap;
        }
    }
}
