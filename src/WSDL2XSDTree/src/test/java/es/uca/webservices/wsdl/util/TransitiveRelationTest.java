package es.uca.webservices.wsdl.util;


/* Test a tree that results of a wsdl file that imports 
other .wsdl that imports a.xsd */
public class TransitiveRelationTest extends TreeTestBase {

    public TransitiveRelationTest() {
        super("shippingProperties");
    }
}
