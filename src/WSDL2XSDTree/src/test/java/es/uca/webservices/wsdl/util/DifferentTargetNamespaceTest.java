package es.uca.webservices.wsdl.util;

import es.uca.webservices.bpel.xml.XMLDocument;

import java.io.File;
import javax.xml.xpath.XPathConstants;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

/* Test a tree that results of a wsdl file having a embed XML Schema with a
different targetNamespace 
 */
public class DifferentTargetNamespaceTest extends TreeTestBase {

    public DifferentTargetNamespaceTest() {
        super("DiffTargetNamespace");
    }

    @Test
    public void generatesOtherXSDFile() throws Exception {
        final WSDLGraphTransformer t = new WSDLGraphTransformer(getWSDLFile());
        final File xsdFile = t.generateXSDTree();
		final XMLDocument schemaDoc = new XMLDocument(xsdFile);

        assertTrue("The embed XML Schema should have been transformed into a XSD:import",
                (Boolean) schemaDoc.evaluateExpression(
                "count(//*[local-name(.) = 'import' and "
                + "@namespace='http://example.org/foo/'"
                + "]) = 1", XPathConstants.BOOLEAN));

        String schema = (String) schemaDoc.evaluateExpression(
                "//*[local-name(.) = 'import' and "
                + "@namespace='http://example.org/foo/'"
                + "]/@schemaLocation", XPathConstants.STRING);

        XMLDocument schemaDoc2 = new XMLDocument(new File(schema));
        assertEquals("The XML Schema should have been conserved his targetNamespace",
                (String) schemaDoc2.evaluateExpression(
                "/*[local-name(.) = 'schema'"
                + "]/@targetNamespace", XPathConstants.STRING), "http://example.org/foo/");
    }
}
