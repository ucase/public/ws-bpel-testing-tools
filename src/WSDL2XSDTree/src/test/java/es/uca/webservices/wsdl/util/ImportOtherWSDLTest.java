package es.uca.webservices.wsdl.util;

import es.uca.webservices.bpel.xml.XMLDocument;

import java.io.File;
import javax.xml.xpath.XPathConstants;
import org.junit.Test;
import static org.junit.Assert.assertTrue;


/* Test a tree that results of a wsdl file that imports only other .wsdl */
public class ImportOtherWSDLTest extends TreeTestBase {

    public ImportOtherWSDLTest() {
        super("LoanService"); // This case has no types at both wsdl files.
    }

    @Test
    public void wsdlImportIsTransformedCorrectly() throws Exception {
        final WSDLGraphTransformer t = new WSDLGraphTransformer(getWSDLFile());
        final File xsd = t.generateXSDTree();
        final XMLDocument schemaDoc = new XMLDocument(xsd);

        assertTrue("A WSDL:import should have been transformed into a XSD:import",
                (Boolean) schemaDoc.evaluateExpression(
                "count(//*[local-name(.) = 'import' and "
                + "@namespace='http://j2ee.netbeans.org/wsdl/loanServicePT'"
                + "]) = 1", XPathConstants.BOOLEAN));

        String schema = (String) schemaDoc.evaluateExpression(
                "//*[local-name(.) = 'import' and "
                + "@namespace='http://j2ee.netbeans.org/wsdl/loanServicePT'"
                + "]/@schemaLocation", XPathConstants.STRING);

        XMLDocument schemaDoc2 = new XMLDocument(new File(schema));
        assertTrue("The XML Schema should have been empty",
                (Boolean) schemaDoc2.evaluateExpression(
                "count(/*[local-name(.) = 'schema'"
                + "]/*) = 0", XPathConstants.BOOLEAN));
    }
}
