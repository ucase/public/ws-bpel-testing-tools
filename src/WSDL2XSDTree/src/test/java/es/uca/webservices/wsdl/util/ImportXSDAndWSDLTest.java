package es.uca.webservices.wsdl.util;

import es.uca.webservices.bpel.xml.XMLDocument;

import java.io.File;
import javax.xml.xpath.XPathConstants;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/* Test a tree that results of a wsdl file that imports both (a .xsd and a .wsdl) */
public class ImportXSDAndWSDLTest extends TreeTestBase {

    public ImportXSDAndWSDLTest() {
        super("shippingPropertiesRedundant");
    }

    @Test
    public void bothImportsAreCollected() throws Exception {
        final WSDLGraphTransformer t = new WSDLGraphTransformer(getWSDLFile());
        final File xsd = t.generateXSDTree();
        final XMLDocument schemaDoc = new XMLDocument(xsd);

        assertTrue("A WSDL:import should have been transformed into a XSD:import",
                (Boolean) schemaDoc.evaluateExpression(
                "count(//*[local-name(.) = 'import'"
                + "]) = 2", XPathConstants.BOOLEAN));
    }
}
