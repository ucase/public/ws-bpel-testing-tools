package es.uca.webservices.wsdl.util;

import es.uca.webservices.bpel.xml.XMLDocument;

import java.io.File;
import javax.xml.xpath.XPathConstants;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/* Test a tree that results of a wsdl file that imports neither a .wsdl nor a .xsd */
public class StandaloneWSDLTest extends TreeTestBase {

    public StandaloneWSDLTest() {
        super("GoogleBridge");
    }

    @Test
    public void xsdIsCollectedIntoXSDRoot() throws Exception {
        final WSDLGraphTransformer t = new WSDLGraphTransformer(getWSDLFile());
        final File xsd = t.generateXSDTree();
        final XMLDocument schemaDoc = new XMLDocument(xsd);

        assertTrue("The namespace with prefix typens should have been collected",
                (Boolean) schemaDoc.evaluateExpression(
                "count(//*[local-name(.) = 'schema']/namespace::typens) = 1",
                XPathConstants.BOOLEAN));

        assertTrue("Three complex types should have been collected",
                (Boolean) schemaDoc.evaluateExpression(
                "count(//*[local-name(.) = 'complexType']) = 3",
                XPathConstants.BOOLEAN));

        assertTrue("Three global elements should have been collected",
                (Boolean) schemaDoc.evaluateExpression(
                "count(/*[local-name(.) = 'schema']/*[local-name(.) = 'element']) = 3",
                XPathConstants.BOOLEAN));
    }
}
