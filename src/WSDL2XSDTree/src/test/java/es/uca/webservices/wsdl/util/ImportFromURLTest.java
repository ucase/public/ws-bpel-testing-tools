package es.uca.webservices.wsdl.util;

import org.apache.xmlbeans.*;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Tests that WSDL2XSDTree can load documents from URLs, and not just from  regular file paths.
 *
 * @author Antonio Garcia-Dominguez
 */
public class ImportFromURLTest extends AbstractHTTPServerWithWSDLTest {

    @Test
    public void canLoadFromURL() throws Exception {
        final WSDL2XSDTreeCommand cmd = new WSDL2XSDTreeCommand();
        cmd.parseArgs(URL_PREFIX + DEFAULT_SERVER_PORT + ORDERS_IMP_QUERY);
        final File rootXSD = cmd.run();
        compileXSD(rootXSD);
    }

    @Test
    public void canProcessImportsFromURL() throws Exception {
        final WSDL2XSDTreeCommand cmd = new WSDL2XSDTreeCommand();
        cmd.parseArgs(URL_PREFIX + DEFAULT_SERVER_PORT + ORDERS_MAIN_QUERY);
        final File rootXSD = cmd.run();
        compileXSD(rootXSD);
    }

    private void compileXSD(File rootXSD) throws XmlException, IOException {
        final XmlObject schemas = XmlObject.Factory.parse(rootXSD);
        final List<XmlError> errors = new ArrayList<XmlError>();
        XmlOptions compileOptions = new XmlOptions().setErrorListener(errors).setCompileDownloadUrls();
        SchemaTypeSystem sts = XmlBeans.compileXsd(new XmlObject[]{schemas},
                XmlBeans.getBuiltinTypeSystem(), compileOptions);
    }

}