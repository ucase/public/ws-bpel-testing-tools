package es.uca.webservices.wsdl.util;

import es.uca.webservices.bpel.xml.XMLDocument;

import java.io.File;
import javax.xml.xpath.XPathConstants;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/* Test a tree that results of a wsdl file that imports others wsdl files in
differents folders
 */
public class DepsInDifferentFolderTest extends TreeTestBase {

    public DepsInDifferentFolderTest() {
        super("MetaSearch");
    }

    @Test
    public void importIsGeneratedCorrectly() throws Exception {
        final WSDLGraphTransformer t = new WSDLGraphTransformer(getWSDLFile());
        final File xsd = t.generateXSDTree();
        final XMLDocument schemaDoc = new XMLDocument(xsd);

        final String xsdImportPath = (String) schemaDoc.evaluateExpression(
                "//*[local-name(.) = 'import']/@schemaLocation", XPathConstants.STRING
        );
        assertTrue(
                "The embedded XML Schema should have been transformed into a XSD:import",
                xsdImportPath.endsWith(".xsd"));

        assertEquals("The attributeFormDefault attribute should have been preserved",
        		(String) schemaDoc.evaluateExpression(
        		"//*[local-name(.) = 'schema']/@attributeFormDefault", XPathConstants.STRING),
        		"qualified");

        assertEquals("The elementFormDefault attribute should have been preserved",
        		(String) schemaDoc.evaluateExpression(
        		"//*[local-name(.) = 'schema']/@elementFormDefault", XPathConstants.STRING),
        		"qualified");
    }
}
