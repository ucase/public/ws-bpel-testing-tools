package es.uca.webservices.wsdl.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import junit.framework.Assert;
import org.apache.xmlbeans.SchemaTypeSystem;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;

import org.junit.Before;
import org.junit.Test;

/*
 * Base class for all tests which ensure certain WSDL files can be
 * converterted into a XSD tree. Defines a few predefined tests of its own: 
 * generates the XSD tree and then, tries to compile the same.
 * 
 * This class assumes that the main WSDL file has the same basename as its
 * containing folder. The folder should be placed along with the rest of the
 * test resource folders in this project.
 */
public abstract class TreeTestBase {

    private final File resourceFolder;

    public TreeTestBase(String subfolderName) {
        resourceFolder = new File(subfolderName);
    }

    public File getWSDLFile() throws IOException {
        return new File(getClass().getResource(
                "/" + resourceFolder.getName()
                + "/" + resourceFolder.getName()
                + ".wsdl").getFile());
    }

    public String getResourcePath(File parent, String resName) throws IOException {
        return parent.getCanonicalPath() + File.separator + resName;
    }

    public String getPath(String name) throws Exception {
        File wsdlFile = getWSDLFile(), parent = wsdlFile.getParentFile();
        return getResourcePath(parent, name);
    }

    @Test
    public void XSDIsGeneratedCorrectly() throws Exception {
        WSDLGraphTransformer t = new WSDLGraphTransformer(getWSDLFile());
        final File generatedXSD = t.generateXSDTree();

        XmlObject schema = XmlObject.Factory.parse(generatedXSD,
                (new XmlOptions()).setLoadLineNumbers().setLoadMessageDigest());
        XmlObject[] schemas = {schema};

        Assert.assertEquals("Can not load schema file", schemas.length, 1);
        Collection<?> errors = new ArrayList<Object>();
        XmlOptions compileOptions = new XmlOptions();
        compileOptions.setCompileDownloadUrls();
        SchemaTypeSystem sts = XmlBeans.compileXsd(schemas,
                XmlBeans.getBuiltinTypeSystem(),
                compileOptions);
        Assert.assertTrue("Schema compilation errors", errors.isEmpty());
        Assert.assertNotNull("No Schema to process", sts);
    }
}
