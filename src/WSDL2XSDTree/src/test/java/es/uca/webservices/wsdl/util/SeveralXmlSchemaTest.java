package es.uca.webservices.wsdl.util;

import es.uca.webservices.bpel.xml.XMLDocument;

import java.io.File;
import javax.xml.xpath.XPathConstants;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/* Test a tree that results of a wsdl file with several XML Schema definitions in <types>  */
public class SeveralXmlSchemaTest extends TreeTestBase {

    public SeveralXmlSchemaTest() {
        super("SeveralXmlSchema");
    }

    @Test
    public void xsdIsCollectedIntoRootFile() throws Exception {
        final WSDLGraphTransformer t = new WSDLGraphTransformer(getWSDLFile());
        final File xsd = t.generateXSDTree();
        final XMLDocument schemaDoc = new XMLDocument(xsd);

        assertTrue("The element Data1 should have been collected (XML Schema #1)",
                (Boolean) schemaDoc.evaluateExpression(
                "count(//*[local-name(.) = 'element' and @name='Data1']) = 1",
                XPathConstants.BOOLEAN));

        assertTrue("There should be an additional import for the schema with Data2",
                (Boolean) schemaDoc.evaluateExpression(
                "count(//*[local-name(.) = 'import']) = 1",
                XPathConstants.BOOLEAN));
    }
}
