import sys
import re
from xml.etree import ElementTree
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement


""" FindClassInQuery function 

The queries under study are strong GQL ones, so to find the key class it is needed a variable witch contains the name of the classes involved in the program (the variable testsuiteclass). The function returns the key class in the query. """

def FindClassInQuery(query, testsuiteclasses):
	pos_key = query.find('KEY')
	subquery = query[pos_key:];
	for testsuiteclass in testsuiteclasses:
		if (testsuiteclass in subquery):
			return str(testsuiteclass);


""" FindClass function 

The queries under study are strong GQL ones, so to find the class involved in the FROM clause it is needed a variable witch contains the name of the classes involved in the program (the variable testsuiteclass). The function returns the class involved in the FROM clause in the query. """

def FindClass(query, testsuiteclasses):
	pos_key = query.find('KEY')
	subquery = query[:pos_key];
	for testsuiteclass in testsuiteclasses:
		if (testsuiteclass in subquery):
			return str(testsuiteclass);	


def readFileLine(mutantOp):
	fileAnalisys = open("Analisys.txt", "r");
	line = "";
	while (not mutantOp in line):
		line = fileAnalisys.readline();
	fileAnalisys.close();
	digits = re.findall('[0-9]+', line)
	return digits;


def MatchParentClasses(QueryClass, xmlfile):
	"""This function find the classes witch its parent is QueryClass"""
	tree = ElementTree.parse(xmlfile)
	root = tree.getroot()
	matchparent = []
	for child in root:
		for parent in child.iter('parent'):
			parentType = parent.get('type')
			if (parentType == QueryClass):
				matchparent.append(child.tag)
	return matchparent


def GetFieldsIntTypes(typeClass, xmlfile):
	"""This function find the fields of the class typeClass[0][1] witch their type are int"""
	tree = ElementTree.parse(xmlfile)
	root = tree.getroot()
	intfields = []
	i = 0
	for child in root:
		if child.tag == typeClass[0][1]:
			for kid in child:
				typefields = kid.get('type')
				if (typefields == 'int'):
					while i < len(typeClass):
						if (typeClass[i][0] == kid.tag):
							intfields.append(kid.tag)
						i += 1
					i = 0
	return intfields


def GetType(fromClass, typeClass, xmlfile):
	"""This function returns the type of the typeClass"""
	tree = ElementTree.parse(xmlfile)
	root = tree.getroot()
	intfields = []
	i = 0
	for child in root:
		if child.tag == fromClass:
			for kid in child:
				if kid.tag == typeClass:
					typefield = kid.get('type')
	return typefield


def ObtainData(query, QueryClass):
	"""This function obtains the data to create the first entity"""
	queryname = re.findall('IS KEY\((\'[A-Z][a-z]+\'), (\'[A-Z][a-z]+\')\)', query)
	entity = [['class', QueryClass], ['name', queryname[0][1]]]
	return entity


def GenerateName(entity, numberofentities):
	"""This function generate the variable name of the new entity"""
	i = 0;
	while entity[0][1] != numberofentities[i][0]:
		i += 1
	name = entity[0][1] + str(numberofentities[i][1])
	numberofentities[i][1] += 1
	return name


def VariableDeclaration(variablename, entity, xmlfile, parentvar):
	"""This function creates the declaration of the entity"""
	line = variablename + " = " + entity[0][1] + "("
	tree = ElementTree.parse(xmlfile)
	root = tree.getroot()
	for child in root:
		if child.tag == entity[0][1]:
			for kid in child:
				if kid.tag == 'key_name':
					line += kid.tag + " = " + variablename + ", "
				if (kid.tag == 'parent' and parentvar != "false"):
					line += kid.tag + " = " + parentvar + ").put"
				if (kid.tag == 'parent' and parentvar == "false"):
					endindex = len(line)
					line = line[:endindex - 2] + ").put" 
				if (kid.tag != 'key_name' and kid.tag != 'parent'):
					if (len(entity) > 1):
						i = 1;
						typed = "false"
						for i in range(len(entity)):
							if (kid.tag == entity[i][0]):
								line += kid.tag + " = " + str(entity[i][1]) + ", "
								typed = "true"
						if typed == "false":
							if kid.get('type') == 'int':
								line += kid.tag + " = 1, "
							if kid.get('type') == 'datetime':
								line += kid.tag + " = datetime.date(year = 2014, month = 01, day = 01), "
							if kid.get('type') == 'float':
								line += kid.tag + " = 1.0, "
							if (kid.get('type') != 'int' and kid.get('type') != 'datetime' and kid.get('type') != 'float'):
								line += kid.tag + " = 'foo', "
					if (len(entity) <= 1):
						if kid.get('type') == 'int':
							line += kid.tag + " = 1, "
						if kid.get('type') == 'datetime':
							line += kid.tag + " = datetime.date(year = 2014, month = 01, day = 01), "
						if kid.get('type') == 'float':
							line += kid.tag + " = 1.0, "
						if (kid.get('type') != 'int' and kid.get('type') != 'datetime' and kid.get('type') != 'float'):
							line += kid.tag + " = 'foo', "
	return line


def SaveEntity(TCfile, entity, xmlfile, numberofentities, parentvar):
	"""This function saves in a file an entity"""
	variablename = GenerateName(entity, numberofentities)
	line = VariableDeclaration(variablename, entity, xmlfile, parentvar)
	file = open(TCfile, "a");
	file.write(line + "\n");
	file.close();
	return variablename


def FindSelectAtt(query):
	"""This function returns the field after the SELECT"""
	patronDST = re.compile('SELECT ([A-z][a-z]+)')
	auxatt = patronDST.findall(query)
	stratt = str(auxatt)
	index = len(stratt)
	value = stratt[2:index - 2]
	return value;


def GenerateEntity(fromclass, fields, xmlfile, order):
	"""This function creates a new entity based in the fields"""
	tree = ElementTree.parse(xmlfile)
	root = tree.getroot()
	entity = [['class', fromclass]]
	pair = []
	i = 0;
	while i < len(fields):
		for child in root:
			if child.tag == fromclass:
				for kid in child:
					if kid.tag == fields[i][0] and fields[i][1] == "":
						fieldType = kid.get('type')
						if fieldType == 'int':
							entity += [[fields[i][0], 1 + order]]
						if fieldType == 'datetime':
							entity += [[fields[i][0], " = datetime.date(year = 2014, month = 01, day = 01), "]]
						if fieldType != 'int' and fieldType != 'datetime':
							entity += [[fields[i][0], fields[i][0] + str(i + order)]]
					if kid.tag == fields[i][0] and fields[i][1] != "":
						entity += [[fields[i][0], fields[i][1]]]

		i += 1
	return entity


def GenerateFieldsandValues(subquery, occurences):
	"""This function returns the fields before the 'string' witch determines the order"""
	i = 0;
	fields = []
	while i < occurences:
		if ("." in subquery[i][3]):
			if (subquery[i][2] == '>'):
				value = str(float(subquery[i][3]) + 1.0)
			if (subquery[i][2] == '<'):
				value = str(float(subquery[i][3]) - 1.0)
			if (subquery[i][2] == '!='):
				value = str(float(subquery[i][3]) - 1.0)
			if (subquery[i][2] != '<' and subquery[i][2] != '>' and subquery [i][2] != '!='):
				value = subquery[i][3]
		else:
			if (subquery[i][2] == '>'):
				value = str(int(subquery[i][3]) + 1)
			if (subquery[i][2] == '<'):
				value = str(int(subquery[i][3]) - 1)
			if (subquery[i][2] == '!='):
				value = str(int(subquery[i][3]) - 1) 
			if (subquery[i][2] != '<' and subquery[i][2] != '>' and subquery[i][2] != '!='):
				value = subquery[i][3]
		fields.append([subquery[i][1], value])
		i += 1
	return fields


def FindConditionalFields(query):
	"""This function returns the fields for new the entities witch have to meet the query constrains"""
	patron = re.compile('(AND) ([A-Za-z]+) (<|>|<=|>=|=|!=) ([0-9]+(\.[0-9]+)?|\'[A-Za-z ]+\')')
	subquery = patron.findall(query)
	fields = FilterFields(subquery)
	condfields = GenerateFieldsandValues(fields, len(fields))
	return condfields


def FilterFields(fields):
	"""This function returns the fields for the new entities removing the repeated one"""
	number = len(fields)
	i = 0
	j = 1
	filters = []
	occurrences = 0
	while i < number:
		while j < number:
			if fields[i][1] == fields[j][1]:	
				occurrences += 1
				filters.append(fields[i])
			j += 1
		i += 1
		j = i + 1
	if (occurrences > 0):
		i = 0
		j = 0
		while i < occurrences:
			while j < len(fields):
				if filters[i][1] == fields[j][1]:
					if "." in fields[j][3]:
						if fields[j][2] != '>' and fields[j][2] != '>=':
							fields.remove(fields[j])
							j = len(fields)
					else:
						if fields[j][2] != '<' and fields[j][2] != '<=':
							fields.remove(fields[j])
							j = len(fields)	
				j += 1
			j = 0
			i += 1
	return fields

