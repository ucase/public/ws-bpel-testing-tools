import sys
import re
import utils

def fdoOpAnalisys(query):
	"""This function analyses if in the query can be applied the FDO mutation operator"""
	patronFDO = re.compile(".*[0-9]{4}-[0-9]{2}-[0-9]{2}.*")
	if (patronFDO.match(query)):
		digits = re.findall('[0-9]{4}-[0-9]{2}-[0-9]{2}', query)
		fileAnalisys = open("Analisys.txt", "a");
		mutants = ""
		for d in digits:
			numbers = d.split('-')
			if int(numbers[2]) < 13:
				mutants += " 7";
			else:
				mutants += " 6";
		line = "FDO " + str(len(digits)) + mutants + "\n";
		fileAnalisys.write(line);
		fileAnalisys.close();
	else:
		fileAnalisys = open("Analisys.txt", "a");
		line = "FDO 0 7\n";
		fileAnalisys.write(line);
		fileAnalisys.close();


def fdoOpGeneration(query, file):
	"""This function generates a mutated query applying the FDO mutation operator"""
	mutations = 0;
	i = 0
	mutations = utils.readFileLine("FDO");
	if (int(mutations[0]) > 0):
		patronFDO = re.compile('([0-9]{4})-([0-9]{2})-([0-9]{2})')
		groupquery = patronFDO.findall(query)
		while (i < len(groupquery)):
			"""First mutation"""
			subquery = str(groupquery[i][0]) + "-" + str(groupquery[i][1]) + "-" + str(groupquery[i][2])
			index = query.index(subquery)
			day = int(groupquery[i][2])
			day += 1
			month = int(groupquery[i][1])
			year = int(groupquery[i][0])
			if day > 28:
				day = 1
				month += 1
				if month == 12:
					month = 1
					year += 1
			mutsubquery = str(year) + "-" + str(month) + "-" + str(day)
			mutquery = query[:index] + mutsubquery + query[index + len(subquery):]
			filename = file + "FDO" + str(i) + "1";
			mutantfile = open(filename, "w");
			mutantfile.write(mutquery + "\n");
			mutantfile.close();

			"""Second mutation"""
			subquery = str(groupquery[i][0]) + "-" + str(groupquery[i][1]) + "-" + str(groupquery[i][2])
			index = query.index(subquery)
			day = int(groupquery[i][2])
			day -= 1
			month = int(groupquery[i][1])
			year = int(groupquery[i][0])
			if day == 0:
				day = 28
				month -= 1
				if month == 0:
					month = 12
					year -= 1
			mutsubquery = str(year) + "-" + str(month) + "-" + str(day)
			mutquery = query[:index] + mutsubquery + query[index + len(subquery):]
			filename = file + "FDO" + str(i) + "2";
			mutantfile = open(filename, "w");
			mutantfile.write(mutquery + "\n");
			mutantfile.close();

			"""Third mutation"""
			subquery = str(groupquery[i][0]) + "-" + str(groupquery[i][1]) + "-" + str(groupquery[i][2])
			index = query.index(subquery)
			day = int(groupquery[i][2])
			month = int(groupquery[i][1])
			year = int(groupquery[i][0])
			month += 1
			if month > 12:
				month = 1
				year += 1
			mutsubquery = str(year) + "-" + str(month) + "-" + str(day)
			mutquery = query[:index] + mutsubquery + query[index + len(subquery):]
			filename = file + "FDO" + str(i) + "3";
			mutantfile = open(filename, "w");
			mutantfile.write(mutquery + "\n");
			mutantfile.close();

			"""Fourth mutation"""
			subquery = str(groupquery[i][0]) + "-" + str(groupquery[i][1]) + "-" + str(groupquery[i][2])
			index = query.index(subquery)
			day = int(groupquery[i][2])
			month = int(groupquery[i][1])
			year = int(groupquery[i][0])
			month -= 1
			if month == 0:
				month = 12
				year -= 1
			mutsubquery = str(year) + "-" + str(month) + "-" + str(day)
			mutquery = query[:index] + mutsubquery + query[index + len(subquery):]
			filename = file + "FDO" + str(i) + "4";
			mutantfile = open(filename, "w");
			mutantfile.write(mutquery + "\n");
			mutantfile.close();

			"""Fifth mutation"""
			subquery = str(groupquery[i][0]) + "-" + str(groupquery[i][1]) + "-" + str(groupquery[i][2]) 
			index = query.index(subquery)
			day = int(groupquery[i][2])
			month = int(groupquery[i][1])
			year = int(groupquery[i][0])
			year += 1
			mutsubquery = str(year) + "-" + str(month) + "-" + str(day)
			mutquery = query[:index] + mutsubquery + query[index + len(subquery):]
		    	filename = file + "FDO" + str(i) + "5";
			mutantfile = open(filename, "w");
			mutantfile.write(mutquery + "\n");
			mutantfile.close();
		
			"""Sixth mutation"""
			subquery = str(groupquery[i][0]) + "-" + str(groupquery[i][1]) + "-" + str(groupquery[i][2]) 
			index = query.index(subquery)
			day = int(groupquery[i][2])
			month = int(groupquery[i][1])
			year = int(groupquery[i][0])
			year -= 1
			mutsubquery = str(year) + "-" + str(month) + "-" + str(day)
			mutquery = query[:index] + mutsubquery + query[index + len(subquery):]
			filename = file + "FDO" + str(i) + "6";
			mutantfile = open(filename, "w");
			mutantfile.write(mutquery + "\n");
			mutantfile.close();
	
			"""Seventh mutation"""
			if int(groupquery[i][2]) < 13:
				print "Entra"
				subquery = str(groupquery[i][0]) + "-" + str(groupquery[i][1]) + "-" + str(groupquery[i][2]) 
				index = query.index(subquery)
				mutsubquery = str(groupquery[i][0]) + "-" + str(groupquery[i][2]) + "-" + str(groupquery[i][1])
				mutquery = query[:index] + mutsubquery + query[index + len(subquery):]
				filename = file + "FDO" + str(i) + "7";
				mutantfile = open(filename, "w");
				mutantfile.write(mutquery + "\n");
				mutantfile.close();
			i += 1
		print "FDO mutant queries are saved"
	else:
		print "FDO mutant operator is not applied"


def GeneratefdoFieldsandValues(subquery, occurences):
	"""This function returns pairs witch contain the fields after the 'AND' their values"""
	i = 0
	j = 0
	patronFDO = re.compile('([0-9]{4})-([0-9]{2})-([0-9]{2})')
	fields = []
	while i < occurences:
		if (subquery[i][2] == '>'):
			groupquery = patronFDO.findall(subquery[i][3])
			year = int(groupquery[0][0])
			year += 1
			value = "datetime.date(year = " + str(year) + ", month = " + groupquery[0][1] + ", day = " + groupquery[0][2] + ")"
			fields.append([subquery[i][1], value])
			month = int(groupquery[0][1])
			month += 1
			year = int(groupquery[0][0])
			if month > 12:
				month = 1
				year = int(groupquery[0][0]) + 1
			value = "datetime.date(year = " + str(year) + ", month = " + str(month) + ", day = " + groupquery[0][2] + ")"
			fields.append([subquery[i][1], value])
			day = int(groupquery[0][2])
			day += 1
			month = int(groupquery[0][1])
			year = int(groupquery[0][0])
			if day > 28:
				day = 1
				month = int(groupquery[0][1] + 1)
				if month > 12:
					month = 1
					year = int(groupquery[0][0]) + 1
			value = "datetime.date(year = " + str(year) + ", month = " + str(month) + ", day = " + str(day) + ")"
			fields.append([subquery[i][1], value])
		if (subquery[i][2] == '<'):
			groupquery = patronFDO.findall(subquery[i][3])
			year = int(groupquery[0][0])
			year -= 1
			value = "datetime.date(year = " + str(year) + ", month = " + groupquery[0][1] + ", day = " + groupquery[0][2] + ")"
			fields.append([subquery[i][1], value])
			year = int(groupquery[0][0])
			month = int(groupquery[0][1])
			month -= 1
			if month == 0:
				month = 12
				year = int(groupquery[0][0]) - 1
			value = "datetime.date(year = " + str(year) + ", month = " + str(month) + ", day = " + groupquery[0][2] + ")"
			fields.append([subquery[i][1], value])
			year = int(groupquery[0][0])
			month = int(groupquery[0][1])
			day = int(groupquery[0][2])
			day -= 1
			if day == 0:
				day = 28
				month -= 1
				if month == 0:
					month = 12
					year -= 1
			value = "datetime.date(year = " + str(year) + ", month = " + str(month) + ", day = " + str(day) + ")"
			fields.append([subquery[i][1], value])
		if (subquery[i][2] != '<' and subquery[i][2] != '>'):
			value = "datetime.date(year = " + groupquery[0][0] + ", month = " + groupquery[0][1] + ", day = " + groupquery[0][2] + ")"
			fields.append([subquery[i][1], value])
		groupquery = patronFDO.findall(subquery[i][3])
		day = int(groupquery[0][2])
		if day < 13:
			value = "datetime.date(year = " + groupquery[0][0] + ", month = " + groupquery[0][2] + ", day = " + groupquery[0][1] + ")"
			fields.append([subquery[i][1], value])
		i += 1
	return fields


def FindfdoConditionalFields(query):
	"""This function returns the fields for the new entities witch meet the constrains"""
	patronFDO = re.compile(".*[0-9]{4}-[0-9]{2}-[0-9]{2}.*")
	i = 0;
	fields = []
	values = []
	numline = 0
	filterquery = query	
	subquery = re.findall('(AND) ([A-Za-z]+) (<|>|<=|>=|!=|=) DATETIME\(:([1-9]+)\)', query)
	while i < len(subquery):
		while numline < int(subquery[i][3]):
			index = filterquery.index('\n')
			valuequery = filterquery[index + 1:]
			indexval = valuequery.index('\n')
			value = valuequery[:indexval]
			filterquery = filterquery[index + 1:]
			numline += 1
			if (patronFDO.match(value)):
				datepatron = str(patronFDO.findall(value))
				date = datepatron[3:len(datepatron) - 3]
				values.append((subquery[0][0], subquery[0][1], subquery[0][2], date))
		i += 1
	condfields = GeneratefdoFieldsandValues(values, len(values))
	return condfields


def NewEntitiesfdoGeneration(query, QueryClass, fromclass, xmlfile, file):
	"""This function saves in a file the necesaries entities to differenciate the original and mutant outputs"""
	frstentity = utils.ObtainData(query, QueryClass)
       	fdoTCfile = "fdoTCgenerated" + file
	numberofentities = []
	numberofentities.append([QueryClass, 0])
	numberofentities.append([fromclass, 0])
	parentvar = "false" #The first entity doesn't need a parent
	parentvar = utils.SaveEntity(fdoTCfile, frstentity, xmlfile, numberofentities, parentvar)
	fdofields = FindfdoConditionalFields(query)
	condfields = utils.FindConditionalFields(query)
	#Entities witch meet the query constrains
	i = 0
	while i < len(fdofields):
		condfields.append(fdofields[i])
		newentity = utils.GenerateEntity(fromclass, condfields, xmlfile, 0)
		utils.SaveEntity(fdoTCfile, newentity, xmlfile, numberofentities, parentvar)
		condfields.remove(fdofields[i])
		i += 1


def fdoOpTCGeneration(query, file, xmlfile, testsuiteclasses):
	"""This function finds the data to generate the necesaries entities to differenciate the original and mutant outputs"""
	mutations = 0;
	mutations = utils.readFileLine("FDO");
	if (int(mutations[0]) > 0):
		QueryClass = utils.FindClassInQuery(query, testsuiteclasses)
		fromclass = utils.FindClass(query, testsuiteclasses)
		NewEntitiesfdoGeneration(query, QueryClass, fromclass, xmlfile, file)
		print "The necesaries entities to differenciate the original and FDO mutant outputs are created"
	else:
		print "FDO mutant operator is not applied"
