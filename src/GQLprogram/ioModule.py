import sys
import re
import utils


def ioOpAnalisys(query):
	"""This function analyses if in the query can be applied the IO mutation operator"""
	patronIO = re.compile('(AND) ([A-Za-z]+) (<|>|<=|>=|=|!=) ([0-9]+)')
	digits = patronIO.findall(query)
	occurrences = len(digits)
	if (occurrences > 0):
		fileAnalisys = open("Analisys.txt", "a");
		line = "IO " + str(occurrences) + " 1\n";
		fileAnalisys.write(line);
		fileAnalisys.close();
	else:
		fileAnalisys = open("Analisys.txt", "a");
		fileAnalisys.write("IO 0 1\n");
		fileAnalisys.close();


def ioOpGeneration(query, file):
	"""This function generates a mutated query applying the IO mutation operator"""
	mutations = 0;
	mutations = utils.readFileLine("IO");
	if (int(mutations[0]) > 0):
		patronIO = re.compile('(AND) ([A-Za-z]+) (<|>|<=|>=|=|!=) ([0-9]+(\.[0-9]+)?)')
		groupquery = patronIO.findall(query)
		"""while of mutations"""
		iter = 0;
		while iter < int(mutations[0]):
			subquery = str(groupquery[iter][0]) + " " + str(groupquery[iter][1]) + " " + str(groupquery[iter][2]) + " " + str(groupquery[iter][3])
			index = query.index(subquery)
			if ("." in groupquery[iter][3]):
				subquery = subquery[:-2]
				mutquery = query[:index] + subquery + query[index + len(subquery) + 2:]
			else:
				mutquery = query[:index] + subquery + ".0" + query[index + len(subquery):]
			iter += 1
			filename = file + "IO" + str(iter) + "0";
			mutantfile = open(filename, "w");
			mutantfile.write(mutquery + "\n");
			mutantfile.close();
		print "IO mutant queries are saved"
	else:
		print "IO mutant operator is not applied"


def GetOccurrences(file, query):
	"""This function get the occurrences of the IO mutant operator"""
	patron = re.compile('(AND) ([A-Z]*[a-z]+) (<|<=|>|>=|=|!=) ([0-9]+(\.[0-9]+)?))')
	subquery = patron.findall(query)
	return len(subquery)


def GenerateioFieldsandValues(subquery, occurences):
	"""This function returns pairs witch contain the fields after the 'AND' their values"""
	i = 0;
	fields = []
	while i < occurences:
		if ("." in subquery[i][3]):
			if (subquery[i][2] == '>'):
				value = str(float(subquery[i][3]) - 1.0)
			if (subquery[i][2] == '<'):
				value = str(float(subquery[i][3]) + 1.0)
			if (subquery[i][2] == '>='):
				value = str(float(subquery[i][3]) - 1.0)
			if (subquery[i][2] == '='):
				value = str(float(subquery[i][3]) + 1.0)
			if (subquery[i][2] == '<=' or subquery[i][2] == '!='):
				value = subquery[i][3]
		else:
			if (subquery[i][2] == '>'):
				value = str(int(subquery[i][3]) + 1)
			if (subquery[i][2] == '<'):
				value = str(int(subquery[i][3]) + 1)
			if (subquery[i][2] == '<='):
				value = str(int(subquery[i][3]) + 1)
			if (subquery[i][2] == '='):
				value = str(int(subquery[i][3]) + 1)
			if (subquery[i][2] == '>=' or subquery [i][2] == '!='):
				value = subquery[i][3]
		fields.append([subquery[i][1], value])
		i += 1
	return fields


def FindioConditionalFields(query):
	"""This function returns the fields for witch the entities have to be ordered"""
	patron = re.compile('(AND) ([A-Z]*[a-z]+) (<|>|<=|>=|=|!=) ([0-9]+(\.[0-9]+)?)')
	subquery = patron.findall(query)
	fields = utils.FilterFields(subquery)
	condfields = GenerateioFieldsandValues(fields, len(fields))
	return condfields


def NewEntitiesioGeneration(query, QueryClass, fromclass, xmlfile, file):
	"""This function saves in a file the necesaries entities to differenciate the original and mutant outputs"""
	frstentity = utils.ObtainData(query, QueryClass)
       	ioTCfile = "ioTCgenerated" + file
	numberofentities = []
	numberofentities.append([QueryClass, 0])
	numberofentities.append([fromclass, 0])
	parentvar = "false" #The first entity doesn't need a parent
	parentvar = utils.SaveEntity(ioTCfile, frstentity, xmlfile, numberofentities, parentvar)
	condfields = utils.FindConditionalFields(query)
        #Entity witch does not meet the query constrains
	newentity = utils.GenerateEntity(fromclass, condfields, xmlfile, 0)
	utils.SaveEntity(ioTCfile, newentity, xmlfile, numberofentities, parentvar)
	condfields = FindioConditionalFields(query)
        #Entity witch does not meet the query constrains
	newentity = utils.GenerateEntity(fromclass, condfields, xmlfile, 0)
	utils.SaveEntity(ioTCfile, newentity, xmlfile, numberofentities, parentvar)


def ioOpTCGeneration(query, file, xmlfile, testsuiteclasses):
	"""This function finds the data to generate the necesaries entities to differenciate the original and mutant outputs"""
	mutations = 0;
	mutations = utils.readFileLine("IO");
	if (int(mutations[0]) > 0):
		QueryClass = utils.FindClassInQuery(query, testsuiteclasses)
		fromclass = utils.FindClass(query, testsuiteclasses)
		NewEntitiesioGeneration(query, QueryClass, fromclass, xmlfile, file)
		print "The necesaries entities to differenciate the original and IO mutant outputs are created"
	else:
		print "IO mutant operator is not applied"

