import sys
import re
import utils


def dstOpAnalisys(query):
	"""This function analyses if in the query can be applied the DST mutation operator"""
	patrondst = re.compile('SELECT [A-z][a-z]+')
	if (patrondst.match(query)):
			fileAnalisys = open("Analisys.txt", "a");
			fileAnalisys.write("DST 1 1\n");
			fileAnalisys.close();
	else:
		fileAnalisys = open("Analisys.txt", "a");
		fileAnalisys.write("DST 0 1\n");
		fileAnalisys.close();


def dstOpGeneration(query, file):
	"""This function generates a mutated query applying the DST mutation operator"""
	mutations = 0;
	mutations = utils.readFileLine("DST");
	if (int(mutations[0]) > 0):
		if ("DISTINCT" in query):
			mutquery = query.replace("SELECT DISTINCT", "SELECT");
			filename = file + "DST" + mutations[0] + mutations[1];
			mutantfile = open(filename, "w");
			mutantfile.write(mutquery + "\n");
			mutantfile.close();
		else:
			mutquery = query.replace("SELECT", "SELECT DISTINCT");
			filename = file + "DST" + mutations[0] + mutations[1];
			mutantfile = open(filename, "w");
			mutantfile.write(mutquery + "\n");
			mutantfile.close();
		print "DST mutant query is saved"
	else:
		print "DST mutant operator is not applied"


def NewEntitiesdstGeneration(query, QueryClass, fromclass, xmlfile, file):
	"""This function saves in a file the necesaries entities to differenciate the original and mutant outputs"""
	frstentity = utils.ObtainData(query, QueryClass)
       	dstTCfile = "dstTCgenerated" + file
	numberofentities = []
	numberofentities.append([QueryClass, 0])
	numberofentities.append([fromclass, 0])
	parentvar = "false" #The first entity doesn't need a parent
	parentvar = utils.SaveEntity(dstTCfile, frstentity, xmlfile, numberofentities, parentvar)
	selectatt = utils.FindSelectAtt(query)
	i = 0;
	typeentity = utils.GetType(fromclass, selectatt, xmlfile)
	while i < 2: # Two entities will have the same select attribute
		if typeentity == 'string':
			newentity = [['class', fromclass], [selectatt, fromclass + selectatt]]
		if typeentity == 'int':
			newentity = [['class', fromclass], [selectatt, 1]]
		if typeentity == 'date':
			newentity = [['class', fromclass], [selectatt, "datetime.date(year = 2014, month = 01, day = 01), "]]

		utils.SaveEntity(dstTCfile, newentity, xmlfile, numberofentities, parentvar)
		i += 1
	# Just one new entity will have a disfferent select attribute
	typeentity = utils.GetType(fromclass, selectatt, xmlfile)
	if typeentity == 'string':
		newentity = [['class', fromclass], [selectatt, fromclass + selectatt + "Diff"]]
	if typeentity == 'int':
		newentity = [['class', fromclass], [selectatt, 2]]
	if typeentity == 'date':
		newentity = [['class', fromclass], [selectatt, "datetime.date(year = 2014, month = 01, day = 02), "]]
	utils.SaveEntity(dstTCfile, newentity, xmlfile, numberofentities, parentvar)


def dstOpTCGeneration(query, file, xmlfile, testsuiteclasses):
	"""This function finds the data to generate the necesaries entities to differenciate the original and mutant outputs"""
	mutations = 0;
	mutations = utils.readFileLine("DST");
	if (int(mutations[0]) > 0):
		QueryClass = utils.FindClassInQuery(query, testsuiteclasses)
		fromclass = utils.FindClass(query, testsuiteclasses)
		NewEntitiesdstGeneration(query, QueryClass, fromclass, xmlfile, file)
		print "The necesaries entities to differenciate the original and DST mutant outputs are created"
	else:
		print "DST mutant operator is not applied"

