import sys
import re
import utils

def limOpAnalisys(query):
	"""This function analyses if in the query can be applied the LIM mutation operator"""
	patronLIM = re.compile('.*LIMIT [0-9]+, [0-9]+.*')
	if (patronLIM.match(query)):		
		fileAnalisys = open("Analisys.txt", "a");
		fileAnalisys.write("LIM 1 4\n");
		fileAnalisys.close();
	else:
		fileAnalisys = open("Analisys.txt", "a");
		fileAnalisys.write("LIM 0 4\n");
		fileAnalisys.close();


def limOpGeneration(query, file):
	"""This function generates a mutated query applying the LIM mutation operator"""
	mutations = 0;
	mutations = utils.readFileLine("LIM");
	if (int(mutations[0]) > 0):
		patronLIM = re.compile('(LIMIT) ([0-9]+), ([0-9]+)')
		patronLIM.findall(query)

		"""First mutation"""
		mutquery = patronLIM.sub("\g<1> \g<2>", query)
		filename = file + "LIM" + mutations[0] + str(1)
		mutantfile = open(filename, "w");
		mutantfile.write(mutquery + "\n");
		mutantfile.close();

		"""Second mutation"""
		mutquery = patronLIM.sub("\g<1> \g<3>", query)
		filename = file + "LIM" + mutations[0] + str(2)
		mutantfile = open(filename, "w");
		mutantfile.write(mutquery + "\n");
		mutantfile.close();

		"""Third mutation"""
		mutquery = patronLIM.sub("", query)
		filename = file + "LIM" + mutations[0] + str(3)
		mutantfile = open(filename, "w");
		mutantfile.write(mutquery + "\n");
		mutantfile.close();

		"""Fourth mutation"""
		mutquery = patronLIM.sub("\g<1> \g<3>, \g<2>", query)
		filename = file + "LIM" + mutations[0] + str(4)
		mutantfile = open(filename, "w");
		mutantfile.write(mutquery + "\n");
		mutantfile.close();
		print "LIM mutant queries are saved"
	else:
		print "LIM mutant operator is not applied"


def GetLimitMaxNumber(query):
	"""This function return the max number in the LIMIT clause"""
	patronLIM = re.compile('(LIMIT) ([0-9]+), ([0-9]+)')
	numbers = patronLIM.findall(query)
	return int(numbers[0][1]) + int(numbers[0][2])


def NewEntitieslimGeneration(query, QueryClass, fromclass, xmlfile, file):
	"""This function saves in a file the necesaries entities to differenciate the original and mutant outputs"""
	frstentity = utils.ObtainData(query, QueryClass)
       	limTCfile = "limTCgenerated" + file
	numberofentities = []
	numberofentities.append([QueryClass, 0])
	numberofentities.append([fromclass, 0])
	parentvar = "false" #The first entity doesn't need a parent
	parentvar = utils.SaveEntity(limTCfile, frstentity, xmlfile, numberofentities, parentvar)
	condfields = utils.FindConditionalFields(query)
	limnumber = GetLimitMaxNumber(query)
	i = 0;
	while i < int(limnumber):
		newentity = utils.GenerateEntity(fromclass, condfields, xmlfile, i)
		utils.SaveEntity(limTCfile, newentity, xmlfile, numberofentities, parentvar)
		i += 1


def limOpTCGeneration(query, file, xmlfile, testsuiteclasses):
	"""This function finds the data to generate the necesaries entities to differenciate the original and mutant outputs"""
	mutations = 0;
	mutations = utils.readFileLine("LIM");
	if (int(mutations[0]) > 0):
		QueryClass = utils.FindClassInQuery(query, testsuiteclasses)
		fromclass = utils.FindClass(query, testsuiteclasses)
		NewEntitieslimGeneration(query, QueryClass, fromclass, xmlfile, file)
		print "The necesaries entities to differenciate the original and LIM mutant outputs are created"
	else:
		print "LIM mutant operator is not applied"
