import sys
import re
import utils
import itertools
from xml.etree import ElementTree
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement


def bpoOpAnalisys(query):
	#This function analyses if in the query can be applied the BPO mutation operator
	patronBPO = re.compile(".*( :[0-9]+| :[a-z]+).*")
	if (patronBPO.match(query)):
		digits = re.findall('( :[0-9]+| :[a-z]+)', query)
		fileAnalisys = open("Analisys.txt", "a");
		if ((len(digits) - 1) == 0):
			line = "BPO 0 0 \n";
			fileAnalisys.write(line);
			fileAnalisys.close();
		else:
			l = list(range(len(digits)))
			permut = itertools.permutations(l)
			line = "BPO " + str(1) + " " + str(len(list(permut)) - 1) + "\n";
			fileAnalisys.write(line);
			fileAnalisys.close();
	else:
		fileAnalisys = open("Analisys.txt", "a");
		fileAnalisys.write("BPO 0 0\n");
		fileAnalisys.close();


def bpoOpGeneration(query, file):
	#This function generates a mutated query applying the BPO mutation operator
	mutations = 0;
	i = 1;
	mutations = utils.readFileLine("BPO");
	listbounded = []
	if (int(mutations[0]) > 0):
		patronBPO = re.compile(' :[0-9]+')
		digits = re.findall('( :[0-9]+)', query)

		while i <= len(digits):
			listbounded.append(" :" + str(i))
			i += 1
	
		element_permut = itertools.permutations(listbounded)
		listpermut = list(element_permut)
		dividedquery = patronBPO.split(query)

		i = 1
		j = 0
		mutquery = ""
		while i < len(listpermut):
			boundedelement = list(listpermut[i])
			while j < len(boundedelement):
				mutquery += dividedquery[j] + boundedelement[j]
				j += 1
			mutquery += dividedquery[j]
			filename = file + "BPO 1 " + str(i);
			mutantfile = open(filename, "w");
			mutantfile.write(mutquery + "\n");
			mutantfile.close();
			mutquery = ""
			i += 1
			j = 0
		print "BPO mutant queries are saved"
	else:
		print "BPO mutant operator is not applied"


def GetOccurrences(file, query):
	"""This function get the occurrences of the IO mutant operator"""
	patron = re.compile('(AND) ([A-Z]*[a-z]+) (<|>|<=|>=|=|!=) :([0-9]+)')
	subquery = patron.findall(query)
	return len(subquery)


def GeneratebpoFieldsandValues(subquery, occurrences, query):
	"""This function returns pairs witch contain the fields after the 'AND' their values"""
	i = 0;
	fields = []
	numline = 0
	filterquery = query
	patronHour = re.compile(".* [0-9]{2}:[0-9]{2}:[0-9]{2}\'.*")
	patronFDO = re.compile('([0-9]{4})-([0-9]{2})-([0-9]{2})')
	while i < occurrences:
		while numline < int(subquery[i][3]):
			index = filterquery.index('\n')
			valuequery = filterquery[index + 1:]
			indexval = valuequery.index('\n')
			value = valuequery[:indexval]
			filterquery = filterquery[index + 1:]
			numline += 1
		if subquery[i][2] == '<':
			if (patronHour.match(value)):
				groupquery = patronFDO.findall(value)
				year = int(groupquery[0][0])
				year -= 1
				value = "datetime.date(year = " + str(year) + ", month = " + groupquery[0][1] + ", day = " + groupquery[0][2] + ")"
			else:
				value = str(int(value.rstrip()) - 1)
		if subquery[i][2] == '>':
			if (patronHour.match(value)):
				groupquery = patronFDO.findall(value)
				year = int(groupquery[0][0])
				year += 1
				value = "datetime.date(year = " + str(year) + ", month = " + groupquery[0][1] + ", day = " + groupquery[0][2] + ")"
			else:
				value = str(int(value.rstrip()) + 1)
		fields.append([subquery[i][1], value.rstrip()])
		i += 1
		numline = 0
		filterquery = query
	return fields


def FindbpoConditionalFields(query):
	"""This function returns the fields for witch the entities have to be ordered"""
	patron = re.compile('(AND) ([A-Za-z]+) (<|>|<=|>=|=|!=) :([0-9]+)')
	subquery = patron.findall(query)
	if subquery != []:
		subquery = utils.FilterFields(subquery)
		condfields = GeneratebpoFieldsandValues(subquery, len(subquery), query)
	else:
		patron = re.compile('(AND) ([A-Za-z]+) (<|>|<=|>=|=|!=) DATETIME\(:([0-9]+)\)')
		subquery = patron.findall(query)
		subquery = utils.FilterFields(subquery)
		condfields = GeneratebpoFieldsandValues(subquery, len(subquery), query)
		
	return condfields


def ChangeValues(entities, xmlfile):
	"""This function change the bpo values"""
	i = 0;
	intfields = utils.GetFieldsIntTypes(entities, xmlfile)
	if (len(intfields) > 1):
		fieldstochange = []
		times = len(entities)
		while i < len(intfields):
			j = 1
			while j < len(intfields):
				if intfields[i] == entities[j][0]:
					fieldstochange.append(entities[j])
					entities.remove(entities[j])
				j += 1
			i += 1
		i = 0
		value = fieldstochange[0][1]
		while i < len(fieldstochange) - 1:
			fieldstochange[i][1] = fieldstochange[i + 1][1] 
			entities.append(fieldstochange[i])
			i += 1
		fieldstochange[i][1] = value
		entities.append(fieldstochange[i])
		return entities


def NewEntitiesbpoGeneration(query, QueryClass, fromclass, xmlfile, file):
	"""This function saves in a file the necesaries entities to differenciate the original and mutant outputs"""
	frstentity = utils.ObtainData(query, QueryClass)
       	bpoTCfile = "bpoTCgenerated" + file
	numberofentities = []
	numberofentities.append([QueryClass, 0])
	numberofentities.append([fromclass, 0])
	parentvar = "false" #The first entity doesn't need a parent
	parentvar = utils.SaveEntity(bpoTCfile, frstentity, xmlfile, numberofentities, parentvar)
	condfields = FindbpoConditionalFields(query)
        #Entity witch meet the query constrains
	newentity = utils.GenerateEntity(fromclass, condfields, xmlfile, 0)
	utils.SaveEntity(bpoTCfile, newentity, xmlfile, numberofentities, parentvar)
	#Entity witch does not meet the query constrains
	newentity = ChangeValues(newentity, xmlfile)
	if (newentity is not None):
		utils.SaveEntity(bpoTCfile, newentity, xmlfile, numberofentities, parentvar)


def bpoOpTCGeneration(query, file, xmlfile, testsuiteclasses):
	"""This function finds the data to generate the necesaries entities to differenciate the original and mutant outputs"""
	mutations = 0;
	mutations = utils.readFileLine("BPO");
	if (int(mutations[0]) > 0):
		QueryClass = utils.FindClassInQuery(query, testsuiteclasses)
		fromclass = utils.FindClass(query, testsuiteclasses)
		NewEntitiesbpoGeneration(query, QueryClass, fromclass, xmlfile, file)
		print "The necesaries entities to differenciate the original and BPO mutant outputs are created"
	else:
		print "BPO mutant operator is not applied"


