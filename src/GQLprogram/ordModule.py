import sys
import re
import utils

def ordOpAnalisys(query):
	"""This function analyses if in the query can be applied the ORD mutation operator"""
	patronORD = re.compile('.*ORDER BY .*')
	if (patronORD.match(query)):
		numasc = query.count("ASC")
		num = numasc + query.count("DESC")
		fileAnalisys = open("Analisys.txt", "a");
		fileAnalisys.write("ORD 1 " + str(num) + "\n");
		fileAnalisys.close();
	else:
		fileAnalisys = open("Analisys.txt", "a");
		fileAnalisys.write("ORD 0 0\n");
		fileAnalisys.close();


def replaceOneOccurrence(query, order, filenumber, file):
	parts = query.split(order)
	iter = 0;
	mutquery = "";
	while (iter < len(parts) - 1):
		cont = 0;
		while (cont < len(parts)):
			mutquery += parts[cont]
			if (cont != len(parts) - 1):
				if (iter == cont):
					if (order == ' ASC'):
						mutquery += ' DESC'
					else:
						mutquery += ' ASC'
				else:
					mutquery += order
			cont += 1;
		filename = str(file) + "ORD" + "1" + str(filenumber);
		mutantfile = open(filename, "w");
		mutantfile.write(mutquery + "\n");
		mutantfile.close();
		mutquery = "";
		iter += 1;
		filenumber += 1;
	return filenumber;


def ordOpGeneration(query, file):
	"""This function generates a mutated query applying the ORD mutation operator"""
	mutations = 0;
	filenumber = 1;
	mutations = utils.readFileLine("ORD");
	if (int(mutations[1]) > 0):
		filenumber = replaceOneOccurrence(query, ' ASC', filenumber, file)
		replaceOneOccurrence(query, ' DESC', filenumber, file)
		print "ORD mutant queries are saved"
	else:
		print "ORD mutant operator is not applied"


def GenerateFields(string, subquery, occurences):
	"""This function returns the fields before the 'string' witch determines the order"""
	i = 0;
	fields = []
	while i < occurences:
		index = str(subquery).index(string)
		auxquery = str(subquery)[:index - 1]
		letter = ""
		startindex = len(auxquery)
		while letter != ' ':
			letter = auxquery[startindex - 1: startindex]
			startindex -= 1
		fields.append([auxquery[startindex + 1:], ""])
		subquery = str(subquery)[index + len(string):]
		i += 1
	return fields


def FindOrdFields(query):
	"""This function returns the fields for witch the entities have to be ordered"""
	patronORD = re.compile('ORDER BY .* [ ASC| DESC]+')
	subquery = patronORD.findall(query)
	numasc = query.count("ASC")
	if (numasc > 0):
		ascfields = GenerateFields("ASC", subquery, numasc)
	numdesc = query.count("DESC")
	if (numdesc > 0):
		descfields = GenerateFields("DESC", subquery, numdesc)

	totalfields = ascfields + descfields
	return totalfields


def NewEntitiesordGeneration(query, QueryClass, fromclass, xmlfile, file):
	"""This function saves in a file the necesaries entities to differenciate the original and mutant outputs"""
	frstentity = utils.ObtainData(query, QueryClass)
       	ordTCfile = "ordTCgenerated" + file
	numberofentities = []
	numberofentities.append([QueryClass, 0])
	numberofentities.append([fromclass, 0])
	parentvar = "false" #The first entity doesn't need a parent
	parentvar = utils.SaveEntity(ordTCfile, frstentity, xmlfile, numberofentities, parentvar)
	ordfields = FindOrdFields(query)
	condfields = utils.FindConditionalFields(query)
	i = 0
	while i < len(ordfields):
		condfields.append(ordfields[i])
		i += 1
	i = 0
	while i < 2: # Two entities are enough to check the order 
		newentity = utils.GenerateEntity(fromclass, condfields, xmlfile, i)
		utils.SaveEntity(ordTCfile, newentity, xmlfile, numberofentities, parentvar)
		i += 1


def ordOpTCGeneration(query, file, xmlfile, testsuiteclasses):
	"""This function finds the data to generate the necesaries entities to differenciate the original and mutant outputs"""
	mutations = 0;
	mutations = utils.readFileLine("ORD");
	if (int(mutations[0]) > 0):
		QueryClass = utils.FindClassInQuery(query, testsuiteclasses)
		fromclass = utils.FindClass(query, testsuiteclasses)
		NewEntitiesordGeneration(query, QueryClass, fromclass, xmlfile, file)
		print "The necesaries entities to differenciate the original and ORD mutant outputs are created"
	else:
		print "ORD mutant operator is not applied"
