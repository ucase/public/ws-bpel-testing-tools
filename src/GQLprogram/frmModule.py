import sys
import re
import utils


def frmOpAnalisys(query):
	"""This function analyses if in the query can be applied the FRM mutation operator"""
	patronFRM = re.compile('^((?!AND).)*$')
	if (patronFRM.match(query)):
			fileAnalisys = open("Analisys.txt", "w");
			fileAnalisys.write("FRM 1 1\n");
			fileAnalisys.close();
	else:
		fileAnalisys = open("Analisys.txt", "w");
		fileAnalisys.write("FRM 0 1\n");
		fileAnalisys.close();


def frmOpGeneration(query, file):
	"""This function generates a mutated query applying the FRM mutation operator"""
	mutations = 0;
	mutations = utils.readFileLine("FRM");
	if (int(mutations[0]) > 0):
		patronFRM = re.compile('FROM [A-Z][a-z]+')
		mutquery = patronFRM.sub("", query);
		filename = file + "FRM" + mutations[0] + mutations[1];
		mutantfile = open(filename, "w");
		mutantfile.write(mutquery + "\n");
		mutantfile.close();
		print "FRM mutant query is saved"
	else:
		print "FRM mutant operator is not applied"


def NewEntitiesfrmGeneration(query, QueryClass, matchparent, xmlfile, file):
	"""This function saves in a file the necesaries entities to differenciate the original and mutant outputs"""
	frstentity = utils.ObtainData(query, QueryClass)
	frmTCfile = "frmTCgenerated" + file
	i = 0;
	numberofentities = []
	if len(matchparent) != 1:
		while i < len(matchparent):
			numberofentities.append([matchparent[i], 0])
			i += 1
		parentvar = "false" #The first entity doesn't need a parent
		parentvar = utils.SaveEntity(frmTCfile, frstentity, xmlfile, numberofentities, parentvar)
		i = 0;
		while i < len(matchparent):
			newentity = [['class', matchparent[i]]]
			utils.SaveEntity(frmTCfile, newentity, xmlfile, numberofentities, parentvar)
			i += 1
	else:
		parentvar = "false" #The first entity doesn't need a parent
		numberofentities.append([QueryClass, 0])
		parentvar = utils.SaveEntity(frmTCfile, frstentity, xmlfile, numberofentities, parentvar)
		newentity = [['class', matchparent[0]]]
		numberofentities = [[matchparent[0], 0]]
		utils.SaveEntity(frmTCfile, newentity, xmlfile, numberofentities, parentvar)
		print "WARNING: Due to just the class " + matchparent[0] + " has as parent " + QueryClass + " the FRM generated mutants will be equivalents"


def frmOpTCGeneration(query, file, xmlfile, testsuiteclasses):
	"""This function finds the data to generate the necesaries entities to differenciate the original and mutant outputs"""
	mutations = 0;
	mutations = utils.readFileLine("FRM");
	if (int(mutations[0]) > 0):
		QueryClass = utils.FindClassInQuery(query, testsuiteclasses)
		matchparent = utils.MatchParentClasses(QueryClass, xmlfile)
		NewEntitiesfrmGeneration(query, QueryClass, matchparent, xmlfile, file)
		print "The necesaries entities to differenciate the original and FRM mutant outputs are created"
	else:
		print "FRM mutant operator is not applied"
