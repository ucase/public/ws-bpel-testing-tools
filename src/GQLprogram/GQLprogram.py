import sys
import re
import utils
import frmModule
import dstModule
import ordModule
import limModule
import ioModule
import inModule
import bpoModule
import fdoModule

from xml.etree import ElementTree
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement


def analyze(query):
	"""This function analyses the query in order to know which mutation operators can be applied"""
	frmModule.frmOpAnalisys(query)
	dstModule.dstOpAnalisys(query)
	ordModule.ordOpAnalisys(query)
	limModule.limOpAnalisys(query)
	ioModule.ioOpAnalisys(query)
	bpoModule.bpoOpAnalisys(query)
	fdoModule.fdoOpAnalisys(query)
	inModule.inOpAnalisys(query)
	print "\nThe mutation operators that can be applied are:\n"
	fileAnalisys = open("Analisys.txt", "r");
	content = fileAnalisys.read();
	print content
	fileAnalisys.close();


def generation(query, file):
	"""This function generates the mutated query in order to the mutation operators that can be applied"""
	frmModule.frmOpGeneration(query, file)
	dstModule.dstOpGeneration(query, file)
	ordModule.ordOpGeneration(query, file)
	limModule.limOpGeneration(query, file)
	ioModule.ioOpGeneration(query, file)
	bpoModule.bpoOpGeneration(query, file)
	fdoModule.fdoOpGeneration(query, file)
	inModule.inOpGeneration(query, file)
	print "\nAll the mutated queries are generated\n"


def testcasesgeneration(query, file, xmlfile, testsuiteclasses):
	"""This function generates the necesary test cases to differenciate the original and mutated outputs"""
	frmModule.frmOpTCGeneration(query, file, xmlfile, testsuiteclasses)
	dstModule.dstOpTCGeneration(query, file, xmlfile, testsuiteclasses)
	ordModule.ordOpTCGeneration(query, file, xmlfile, testsuiteclasses)
	limModule.limOpTCGeneration(query, file, xmlfile, testsuiteclasses)
	ioModule.ioOpTCGeneration(query, file, xmlfile, testsuiteclasses)
	bpoModule.bpoOpTCGeneration(query, file, xmlfile, testsuiteclasses)
	fdoModule.fdoOpTCGeneration(query, file, xmlfile, testsuiteclasses)
	inModule.inOpTCGeneration(query, file, xmlfile, testsuiteclasses)
	print "\nAll the entities are created"


def procesingSchema(xmlfile):
	"""This function analyzes the schema in the XML file"""
	tree = ElementTree.parse(xmlfile)
	root = tree.getroot()
	testsuiteclasses = []
	for child in root:
		testsuiteclasses.append(child.tag)
	return testsuiteclasses
	

if (len(sys.argv) == 3):
	print "Reading the file: " + sys.argv[1]
	file = sys.argv[1]
	infile = open(file, 'r');
	queryan = infile.readline();
	querygen = queryan
	query = queryan.rstrip()

	while True:
		line = infile.readline();
		if (line):
			query = query + " " + line.rstrip()
			querygen = querygen + line
		else:
			break
	print "The query to analyse is:\n", query
	infile.close();
	analyze(query)
	print "According to the last results the mutated queries are going to be generated"
	generation(querygen, file)
	print "Reading the file: " + sys.argv[2]
	xmlfile = sys.argv[2]
	testsuiteclasses = procesingSchema(xmlfile)
	testcasesgeneration(querygen, file, xmlfile, testsuiteclasses)
else:
	print "You must type the name of the files: GQLprogram Queryfile Schemafile"
