import sys
import re
import utils


def inOpAnalisys(query):
	"""This function analyses if in the query can be applied the BPO mutation operator"""
	patronIN = re.compile(".* IN \(.*\)")
	if (patronIN.match(query)):
		indexIN = query.index(" IN ")
		subquery = query[indexIN:]
		indexFIN = subquery.index(")")
		inlist = subquery[:indexFIN]
		num = inlist.count(",")
		fileAnalisys = open("Analisys.txt", "a");
		fileAnalisys.write("IN 1 " + str(num + 1) + "\n");
		fileAnalisys.close();
	else:
		fileAnalisys = open("Analisys.txt", "a");
		fileAnalisys.write("IN 0 0\n");
		fileAnalisys.close();


def inOpGeneration(query, file):
	"""This function generates a mutated query applying the IO mutation operator"""
	mutations = 0;
	mutations = utils.readFileLine("IN");
	if (int(mutations[1]) > 0):
		patronIN = re.compile('\'[A-Z][a-z]+\'')
		indexIN = query.index(" IN ")
		subquery = query[indexIN:]
		indexFIN = subquery.index(")")
		inlist = subquery[:indexFIN]
		groupquery = patronIN.findall(inlist)
		"""while of mutations"""
		iter = 0;
		while (iter < int(mutations[1])):
			element = groupquery[iter]
			if (iter != int(mutations[1]) - 1):
				mutquery = query.replace(element + ",", "")
			else:
				mutquery = query.replace(", " + element, "")
			iter += 1;
			filename = file + "IN 1 " + str(iter);
			mutantfile = open(filename, "w");
			mutantfile.write(mutquery + "\n");
			mutantfile.close();
		print "IN mutant queries are saved"
	else:
		print "IN mutant operator is not applied"


def GenerateinFieldsandValues(subquery):
	"""This function returns pairs witch contains the field before the IN and each of the values in the IN list"""
	i = 0;
	fields = []
	inlist = str(subquery[0][3])
	parts = inlist.split(",")
	occurrences =  len(parts)
	while i < occurrences:
		if i == 0:
			value = str(parts[i])[1:]
		if i == len(parts) - 1:
			index = len(parts[i])
			value = str(parts[i])[:index - 1]
		if i > 0 and i < len(parts) - 1:
			value = parts[i]
		fields.append([subquery[0][1], value])
		i += 1
	return fields


def FindListFields(query):
	"""This function returns the fields for witch the entities have to be ordered"""
	patron = re.compile('(AND) ([A-Z]*[a-z]+) (IN) (\(.*\))')
	subquery = patron.findall(query)
	fields = GenerateinFieldsandValues(subquery) 
	return fields


def NewEntitiesinGeneration(query, QueryClass, fromclass, xmlfile, file):
	"""This function saves in a file the necesaries entities to differenciate the original and mutant outputs"""
	frstentity = utils.ObtainData(query, QueryClass)
       	inTCfile = "inTCgenerated" + file
	numberofentities = []
	numberofentities.append([QueryClass, 0])
	numberofentities.append([fromclass, 0])
	parentvar = "false" #The first entity doesn't need a parent
	parentvar = utils.SaveEntity(inTCfile, frstentity, xmlfile, numberofentities, parentvar)
	condfields = utils.FindConditionalFields(query)
	listfields = FindListFields(query)
	i = 0
	while i < len(listfields):
		condfields.append(listfields[i])
		newentity = utils.GenerateEntity(fromclass, condfields, xmlfile, i)
		utils.SaveEntity(inTCfile, newentity, xmlfile, numberofentities, parentvar)
		condfields.remove(listfields[i])
		i += 1


def inOpTCGeneration(query, file, xmlfile, testsuiteclasses):
	"""This function finds the data to generate the necesaries entities to differenciate the original and mutant outputs"""
	mutations = 0;
	mutations = utils.readFileLine("IN");
	if (int(mutations[0]) > 0):
		QueryClass = utils.FindClassInQuery(query, testsuiteclasses)
		fromclass = utils.FindClass(query, testsuiteclasses)
		NewEntitiesinGeneration(query, QueryClass, fromclass, xmlfile, file)
		print "The necesaries entities to differenciate the original and IN mutant outputs are created"
	else:
		print "IN mutant operator is not applied"
