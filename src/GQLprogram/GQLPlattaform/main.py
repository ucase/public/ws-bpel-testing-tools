import cgi
import datetime
import re
import sys

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db

""" Class Tutor """

class Tutor(db.Model):
  name = db.StringProperty()
  surname = db.StringProperty()
  date = db.DateProperty() #Birthday
  department = db.StringProperty()
  title = db.StringProperty()

""" Entities of class Tutor """

helen = Tutor(key_name = 'Helen', name = 'Helen', surname = 'Smith', date = datetime.date(year=1953, month=12, day=27), department = 'Computer Science and Engineering', title = 'Science')
helen.put()
walter = Tutor(key_name = 'Walter', name = 'Walter', surname = 'White', date = datetime.date(year=1963, month=10, day=12), department = 'Business Studies', title = 'Economy')
walter.put()
litz = Tutor(key_name = 'Litz', name = 'Litz', surname = 'Brown', date = datetime.date(year=1983, month=10, day=25), department = 'Computer Science', title = 'Computer Science', parent=helen)
litz.put()
teresa = Tutor(key_name = 'Teresa', name = 'Teresa', surname = 'Lisbon', date = datetime.date(year=1965, month=05, day=30), department = 'Marketing', title = 'Publicity')
teresa.put()
john = Tutor(key_name = 'John', name = 'John', surname = 'Red', date = datetime.date(year=1949, month=02, day=15), department = 'Marketing', title = 'Marketing and Business')
john.put()
jessie = Tutor(key_name = 'Jessie', name = 'Jessie', surname = 'Pickman', date = datetime.date(year=1970, month=03, day=04), department = 'Business Studies', title = 'Marketing and Business', parent=walter)
jessie.put()
patrick = Tutor(key_name = 'Patrick', name = 'Patrick', surname = 'Jane', date = datetime.date(year=1960, month=11, day=04), department = 'Marketing', title = 'Publicity', parent=teresa)
patrick.put()
carol = Tutor(key_name = 'Carol', name = 'Carol', surname = 'Taylor', date = datetime.date(1973, month=05, day=06), department = 'Computer Science', title = 'Computer Science')
carol.put()
albert = Tutor(key_name = 'Albert', name = 'Albert', surname = 'Lamb', date = datetime.date(year=1973, month=03, day=04), department = 'Computer Science', title = 'Computer Science', parent=helen)
albert.put()

TutorList = [helen, walter, litz, teresa, john, jessie, patrick, carol, albert]

""" Class Student """

class Student(db.Model):
  name = db.StringProperty()
  surname = db.StringProperty()
  date = db.DateProperty() #Birthday
  title = db.StringProperty()
  grade = db.IntegerProperty()
  mingrade = db.IntegerProperty()
  credits = db.IntegerProperty()
  average = db.FloatProperty()
  tutor = db.StringProperty()

""" Entities of class Student """

sheldom = Student(key_name = 'Sheldom', name = 'Sheldom', surname = 'Cooper', date = datetime.date(year=1988, month=12, day=04), title = 'Computer Science', grade = 4, tutor = 'Carol', parent=carol)
sheldom.put();
ian = Student(key_name = 'Ian', name = 'Ian', surname = 'Ritchie', date = datetime.date(year=1990, month=06, day=12), title = 'Publicity', grade = 3, tutor = 'Teresa', parent=teresa)
ian.put();
#adele = Student(key_name = 'Adele', name = 'Adele', surname = 'Ritchie', date = datetime.date(year=1991, month=09, day=30), title = 'Computer Science', grade = 3, tutor = 'Teresa', parent=teresa)
#adele.put()
robert = Student(key_name = 'Robert', name = 'Robert', surname = 'Langdon', date = datetime.date(year=1992, month=12, day=05), title = 'Business and Economy', grade = 2, mingrade = 1, credits = 35, average = 6.7, tutor = 'Walter', parent=walter)
robert.put();
debra = Student(key_name = 'Debra', name = 'Debra', surname = 'Fieguth', date = datetime.date(year=1990, month=07, day=07), title = 'Computer Science', grade = 4, tutor = 'Helen', parent=helen)
debra.put();
jaime = Student(key_name = 'Jaime', name = 'Jaime', surname = 'Smith', date = datetime.date(year=1993, month=03, day=01), title = 'Business and Economy', grade = 2)
jaime.put();
alicia = Student(key_name = 'Alicia', name = 'Alicia', surname = 'Mant', date = datetime.date(year=1989, month=04, day=14), title = 'Publicity', grade= 4, tutor = 'Carol', parent=carol)
alicia.put();
aviva = Student(key_name = 'Aviva', name = 'Aviva', surname = 'Webb', date = datetime.date(year=1990, month=02, day=25), title = 'Business and Economy', grade = 4, tutor = 'Carol', parent=carol)
aviva.put();
alex = Student(key_name = 'Alex', name = 'Alex', surname = 'Kay', date = datetime.date(year=1986, month=02, day=28), title = 'Publicity', grade = 4, mingrade = 3, credits = 72, average = 7.3, tutor = 'Walter', parent=walter)
alex.put();
david = Student(key_name = 'David', name = 'David', surname = 'Moon', date = datetime.date(year=1989, month=01, day=31), title = 'Marketing', grade = 3, tutor = 'John', parent=john)
david.put();
#erin = Student(key_name = 'Erin', name = 'Erin', surname = 'Wu', date = datetime.date(year=1992, month=08, day=19), title = 'Computer Science', grade = 4, tutor = 'John', parent=john)
#erin.put();
amy = Student(key_name = 'Amy', name = 'Amy', surname = 'House', date = datetime.date(year=1991, month=05, day=13), title = 'Marketing', grade = 1)
amy.put();
alanis = Student(key_name = 'Alanis', name = 'Alanis', surname = 'Parket', date = datetime.date(year=1988, month=07, day=22), title = 'Publicity', grade = 4, mingrade = 4, credits = 87, average = 8.3, tutor = 'Walter', parent=walter)
alanis.put();
denis = Student(key_name = 'Denis', name = 'Denis', surname = 'Woodman', date = datetime.date(year=1993, month=05, day=15), title = 'Publicity', grade = 1, mingrade = 1, credits = 25, average = 5.3, tutor = 'Walter', parent=walter)
denis.put();

StudentList = [sheldom, ian, robert, debra, jaime, alicia, aviva, alex, david, amy, alanis, denis]

""" Class FinalProject """

class FinalProject(db.Model):
  title = db.StringProperty()
  name = db.StringProperty()    #Author name
  surname = db.StringProperty() #Author surname
  read = db.BooleanProperty(default=False)
  date = db.DateProperty()      #When was read
  mark = db.FloatProperty()
  pages = db.IntegerProperty()

""" Entities of class FinalProject """

sTASheldom = FinalProject(key_name = 'STASheldom', title = 'String theory aplication', name = 'Sheldom', surname = 'Cooper', read = True, date = datetime.date(year=2012, month=04, day=23), mark = 10.0, pages = 250, parent=sheldom)
sTASheldom.put()
aMPVPIan = FinalProject(key_name = 'AMPVPIan', title = 'A marketing point of view for a product', name = 'Ian', surname = 'Ritchie', pages = 100, parent=ian)
aMPVPIan.put()
aGCDebra = FinalProject(key_name = 'AGCDebra', title = 'Algorithms for graph coloring', name = 'Debra', surname = 'Fieguth', read = True, date = datetime.date(year=2013, month=10, day=25), pages = 150, parent=debra)
aGCDebra.put()
sPPLAlicia = FinalProject(key_name = 'SPPLAlicia', title = 'Study, packaging and product launch', name = 'Alicia', surname = 'Mant', read = True, date = datetime.date(year=2012, month=11, day=04), mark = 9.0, pages = 100, parent=alicia)
sPPLAlicia.put()
cSNLPBAlex = FinalProject(key_name = 'CSNLPBAlex', title = 'Comercial study for a new line of publicity business', name = 'Alex', surname = 'Kay', pages = 90, parent=alex)
cSNLPBAlex.put()
mSMWAviva = FinalProject(key_name = 'MSMWAviva', title = 'A Micro-economy study on mobile world', name = 'Aviva', surname = 'Webb', read = True, date = datetime.date(year=2012, month=06, day=13), mark = 8.5, pages = 320, parent=aviva)
mSMWAviva.put()
#dMErin = FinalProject(key_name = 'DMErin', title = '3D in Medicine', name = 'Erin', surname = 'Wu', read = True, date = datetime.date(year=2012, month=03, day=10), pages = 320, parent=erin)
#dMErin.put()

FinalProjectList = [sTASheldom, aMPVPIan, aGCDebra, sPPLAlicia, cSNLPBAlex, mSMWAviva]

testsuiteclasses = ['Tutor', 'Student', 'FinalProject']
classlists = [TutorList, StudentList, FinalProjectList]

""" Main page html structure 

This page contains the structure of the main page of the program. 

The first form is to execute a GQL query. The files witch contains the query has an specific structure: the first line is the query, and the following lines are its possibles arguments. The action in the form /inputfile is focus in the specific GQL queries developed by TER and UCASE research groups. The functions behind the inputfile action have been developed to prove the GQL mutation operators defined by us.

The second form is to execute a GQL query in witch can be applied the FRM mutation operator. The action in the form /FRMalgorithmfile and the functions behind have been developed to analyse a GQL query and check the entities witch meet the constrains of the FRM mutation operator and give another new entities witch meet the constrains. 

The second form is to execute a GQL query in witch can be applied the IO mutation operator. The action in the form /IOalgorithmfile and the functions behind have been developed to analyse a GQL query and check the entities witch meet the constrains of the IO mutation operator and give another new entities witch meet the constrains. 

The second form is to execute a GQL query in witch can be applied the FDO mutation operator. The action in the form /FDOalgorithmfile and the functions behind have been developed to analyse a GQL query and check the entities witch meet the constrains of the FDO mutation operator and give another new entities witch meet the constrains. 

"""

MAIN_PAGE_HTML = """\
<html>
	<body>
		<form action="/inputfile" method="post">
			<div><p>Introduce the name of the file to execute:</p></div>
			<div><textarea name="content" rows="1" cols="30"></textarea></div>
			<div><input type="submit" value="Submit"></div>
		</form>
		<form action="/FRMalgorithmfile" method="post">
			<div><p>Introduce the name of the file to execute with the FRM algorithm:</p></div>
			<div><textarea name="contentFRMalgorithm" rows="1" cols="30"></textarea></div>
			<div><input type="submit" value="Submit"></div>
		</form>
		<form action="/IOalgorithmfile" method="post">
			<div><p>Introduce the name of the file to execute with the IO algorithm:</p></div>
			<div><textarea name="contentIOalgorithm" rows="1" cols="30"></textarea></div>
			<div><input type="submit" value="Submit"></div>
		</form>
		<form action="/FDOalgorithmfile" method="post">
			<div><p>Introduce the name of the file to execute with the FDO algorithm:</p></div>
			<div><textarea name="contentFDOalgorithm" rows="1" cols="30"></textarea></div>
			<div><input type="submit" value="Submit"></div>
		</form>
		<form action="/LIMalgorithmfile" method="post">
			<div><p>Introduce the name of the file to execute with the LIM algorithm:</p></div>
			<div><textarea name="contentLIMalgorithm" rows="1" cols="30"></textarea></div>
			<div><input type="submit" value="Submit"></div>
		</form>
		<form action="/DSTalgorithmfile" method="post">
			<div><p>Introduce the name of the file to execute with the DST algorithm:</p></div>
			<div><textarea name="contentDSTalgorithm" rows="1" cols="30"></textarea></div>
			<div><input type="submit" value="Submit"></div>
		</form>
	</body>
</html>
"""

""" Class MainPage """

class MainPage(webapp.RequestHandler):
  def get(self):
    self.response.out.write(MAIN_PAGE_HTML)

""" Function readFileLine

This function read files witch contains one GQL query. The first line is the query, and the following ones are the possibles arguments of the query. The function returns the GQL query that can be processed by the Google App Engine. """
	
def readFileLine(file):
	fileToExecute = open(file, "r");
	query = fileToExecute.readline();
	query = query[0:-1]
	item = 0;
	patron = re.compile('(:[0-9]+)')
	replacelist = patron.findall(query)
	while True:
		line = fileToExecute.readline();
		line = line[0:-1]
		if (line):		
			query = query.replace(replacelist[item], line)
			item += 1;
		else:
			break
	fileToExecute.close();
	return query;

""" ResultsPage class

This class contains the structure of the results html page. This part of the program is based on the classes, entities and queries developed by TER and UCASE research groups. This class is just used in the action "inputfile" of the class MainPage form. This class help us to check if the outputs queries are different. The structure of the html page is different depends on the number of the file witch contains the GQL language. """

class ResultsPage(webapp.RequestHandler):
  def post(self):
	self.response.out.write('<html><body><h1>The query to execute:</h1><pre>')
	file = cgi.escape(self.request.get('content'))
	query = readFileLine(file)
	self.response.out.write('<p><b>%s</b>' % query)
	digit = file[5:6]
	
	qresults = db.GqlQuery(query)
	
	if (int(digit) == 1 or int(digit) == 2 or int(digit) == 4 or int(digit) == 6 or int(digit) == 8):
		self.response.out.write('<h1>Results for the query:</h1>')
		for qresult in qresults:
			self.response.out.write('<p><b>Name:</b> %s  ' % qresult.name)
			self.response.out.write('<b>Surname:</b> %s </p>' % qresult.surname)
			self.response.out.write('<p><b>Date:</b> %s </p>' % qresult.date)
			self.response.out.write('<p><b>Title:</b> %s </p>' % qresult.title)
			self.response.out.write('<p><b>grade:</b> %d </p>' % qresult.grade)
			self.response.out.write('<p><b>mingrade:</b> %d </p>' % qresult.mingrade)
		
	if (int(digit) == 7 or int(digit) == 3):
		self.response.out.write('<h1>Results for the query:</h1>')
		for qresult in qresults:
			self.response.out.write('<p><b>Title:</b> %s </p>' % qresult.title)
			self.response.out.write('<p><b>Name:</b> %s  ' % qresult.name)
			self.response.out.write('<b>Surname:</b> %s </p>' % qresult.surname)
			self.response.out.write('<p><b>Date:</b> %s </p>' % qresult.date)
			self.response.out.write('<p><b>Credits:</b> %d </p>' % qresult.credits)
	
	if (int(digit) == 5 or int(digit) == 11):
		self.response.out.write('<h1>Results for the query:</h1>')
		for qresult in qresults:
			self.response.out.write('<p><b>Name:</b> %s  ' % qresult.name)
			self.response.out.write('<b>Surname:</b> %s </p>' % qresult.surname)
			self.response.out.write('<p><b>Date:</b> %s </p>' % qresult.date)
			self.response.out.write('<p><b>Department:</b> %s </p>' % qresult.department)
			
	if (int(digit) == 9):
		self.response.out.write('<h1>Results for the query:</h1>')
		for qresult in qresults:
			self.response.out.write('<p><b>Title:</b> %s  ' % qresult.title)
			self.response.out.write('<p><b>Author:</b> %s  ' % qresult.name)
			self.response.out.write('<p><b>Date:</b> %s  ' % qresult.date)
			self.response.out.write('<p><b>Mark:</b> %f  ' % qresult.mark)

	self.response.out.write('</pre></body></html>')


"""def StudentDefinition(parent, object):
	entitydefinition = "Student(key_name = \'Generated\', name = \'" + object.name + "\', surname = \'" + object.surname + "\', date = \'" + str(object.date) + "\', title = \'" + object.title + " tutor = \'" + parent + "\', parent = " + parent 

def newEntityToFile(filename, parent, QueryClass, object):
	if (QueryClass == 'Student'):
		entitydefinition = StudentDefinition(parent, object)
#	if (QueryClass == 'Tutor'):
#		entitydefinition = TutorDefinition(parent, object)
#	if (QueryClass == 'FinalProject'):
#		entitydefinition = FinalProjectDefinition(parent, object)
	file.open(filename, "a")
	file.write(entitydefinition + "\n");
	file.close();"""


# FRM Mutation operator functions #

""" Class FRMAlgorithmPage 

This class contains the structure of the FRMalgorithm page. This part of the program can be used with strong GQL queries. This class analyses a GQL query and check the entities witch meet the constrains of the FRM mutation operator and give another new entities witch meet the constrains. """


class FRMAlgorithmPage(webapp.RequestHandler):
  def post(self):
	self.response.out.write('<html><body><h1>The query to execute with the algorithm:</h1><pre>')
	file = cgi.escape(self.request.get('contentFRMalgorithm'))
	query = readFileLine(file)
	self.response.out.write('<p><b>%s</b>' % query)
	QueryClass = FindClassInQuery(query)
	QueryList = FindAssociateList(QueryClass)
	self.response.out.write('<p><b>Involved classes:</b> %s  ' % QueryClass)
	for classlist in classlists:
		if (classlist != QueryList):
			self.response.out.write('<p><b>Entities under study:</b>')
			for elem in classlist:
				self.response.out.write(' %s  ' % elem.name)
			constraintList = FRMConstraintAlgorithm(QueryList, classlist)
			self.response.out.write('<p><b>Entities which meet the constraints:</b> %s  ' % constraintList)
			newdatasetList = FRMDataSetGenerationAlgorithm(QueryList, classlist, QueryClass)
			self.response.out.write('<p><b>New entities which meet the constraints:</b> %s  ' % newdatasetList)
	
	self.response.out.write('</pre></body></html>')

""" FindClassInQuery function 

The queries under study are strong GQL ones, so to find the key class it is needed a variable witch contains the name of the classes involved in the program (the variable testsuiteclass). The function returns the key class in the query. """

def FindClassInQuery(query):
	pos_key = query.find('KEY')
	subquery = query[pos_key:];
	for testsuiteclass in testsuiteclasses:
		if (testsuiteclass in subquery):
			return str(testsuiteclass);

""" FindAssciateList function 

This is one function that has to be developed or changed in order to apply the FRM, IO and FDO algorithms. This function receives a variable with the name of a class and returns a variable witch contains a list of entities of involved in each class. These variables have to be also defined, at the end of the definitions of the entities of each class these variables are defined. """

def FindAssociateList(QueryClass):
	if (QueryClass == "Tutor"):
		return TutorList;
	if (QueryClass == "Student"):
		return StudentList;
	if (QueryClass == "FinalProject"):
		return FinalProjectList;

""" FRMConstraintAlgorithm function

This function contains the algorithm witch find the entities of two classes witch meet the constraints for the FRM mutation operator. The GQL query under study is a strong one, so in case of a no strong GQL query this algorithm will be different. """

def FRMConstraintAlgorithm(ClassAList, ClassBList):	
 parentList = []
 for objectA in ClassAList:
   if objectA.parent():
    objectAparentname = objectA.parent()
    for objectB in ClassBList:
      if objectB.parent():
         objectBparentname = objectB.parent()
         if (objectAparentname.name == objectBparentname.name):
            if not (objectAparentname.name in parentList):
			   parentList.append(objectAparentname.name)
			   
 return parentList
 
""" FRMDataSetGenerationAlgorithm function

This function contains the algorithm witch base on the defined entities gives new ones witch meet the constraints for the FRM mutation operator. The GQL query under study is a strong one, so in case of a no strong GQL query this algorithm will be different. """ 
 
def FRMDataSetGenerationAlgorithm(ClassAList, ClassBList, QueryClass):
	newparentList = []
	"""filename = QueryClass + "FRMGeneratedEntities";
	file = open(filename, "w");
	file.write("Generated Entities for FRM mutation operator \n")
	file.close"""
	
	for objectA in ClassAList:
		if objectA.parent():
			objectAparentname = objectA.parent()
			for objectB in ClassBList:
				if not(objectB.parent()):
					newpair = (objectAparentname.name, objectB.name);
					if not (objectAparentname.name == objectB.name):
						if not (newpair in newparentList):
							newparentList.append(newpair)
							#newEntityToFile(filename, str(objectAparentname), QueryClass, objectB)

	return newparentList

# IO Mutation operators functions #

""" Class IOAlgorithmPage 

This class contains the structure of the IOalgorithm page. This part of the program can be used with strong GQL queries. This class analyses a GQL query and check the entities witch meet the constrains of the IO mutation operator and give another new entities witch meet the constrains. """

class IOAlgorithmPage(webapp.RequestHandler):
	"""The numbers which are applied the IO mutation operator are in comparisons"""
	def post(self):
		self.response.out.write('<html><body><h1>The query to execute with the algorithm:</h1><pre>')
		file = cgi.escape(self.request.get('contentIOalgorithm'))
		query = readFileLine(file)
		self.response.out.write('<p><b>%s</b>' % query)
		number = FindNumber(query)
		self.response.out.write('<p><b>Number in the comparison:</b> %s  ' % number)
		involvedVariable = FindInvolvedVariableIO(query)
		self.response.out.write('<p><b>Involved variable:</b> %s  ' % involvedVariable)
		involvedEntity = FindInvolvedEntity(query)
		self.response.out.write('<p><b>Involved entity:</b> %s  ' % str(involvedEntity))
		QueryClass = FindClass(query)
		QueryList = FindAssociateList(QueryClass)
		QueryAncestorList = FindAncestorClass(QueryList)
		self.response.out.write('<p><b>Involved class:</b> %s  ' % QueryClass)
		constraintList = IOConstraintAlgorithm(QueryList, number, involvedVariable, involvedEntity, QueryAncestorList)
		self.response.out.write('<p><b>Entities which meet the constraints:</b> %s  ' % constraintList)
		newdatasetList = IODataSetGenerationAlgorithm(QueryList, number, involvedVariable, involvedEntity, QueryAncestorList)
		self.response.out.write('<p><b>New entities which meet the constraints:</b> %s  ' % newdatasetList)
		self.response.out.write('</pre></body></html>')

""" FindNumber function

The queries under study for the IO mutation operator are the ones with < or <= relational operators. The output of the mutated queries with the rest of relational operators are always different from the original query. The outputs of the < and <= relational operators are equivalents from the original query. This function returns the number involved in the relation. """

def FindNumber(query):
	relationalOps = ['<', '<='] #The constraint is only valid with that operators
	for relatOp in relationalOps:
		if (relatOp in query):
			numbers = re.findall('\d+', query)
			pos_op = query.find(relatOp)
			subquery = query[pos_op:]
			for number in numbers:
				if number in subquery:
					return number;

""" FindClass function 

The queries under study are strong GQL ones, so to find the class involved in the FROM clause it is needed a variable witch contains the name of the classes involved in the program (the variable testsuiteclass). The function returns the class involved in the FROM clause in the query. """

def FindClass(query):
	pos_key = query.find('KEY')
	subquery = query[:pos_key];
	for testsuiteclass in testsuiteclasses:
		if (testsuiteclass in subquery):
			return str(testsuiteclass);

""" FindInvolvedVariableIO function

The queries under study for the IO mutation operator are the ones with < or <= relational operators. The output of the mutated queries with the rest of relational operators are always different from the original query. The outputs of the < and <= relational operators are equivalents from the original query. This function returns the variable involved in the relation. """

def FindInvolvedVariableIO(query):
	if ('<=' in query):
		patron = re.compile('.* ([aA-zZ]+) <= ([0-9]+).*')
		variable = patron.sub("\g<1>", query)
	if ('<' in query):
		patron = re.compile('.* ([aA-zZ]+) < ([0-9]+).*')
		variable = patron.sub("\g<1>", query)
	return variable;

""" FindInvolvedEntity function 

The queries under study are strong GQL ones, so to find the selected entity of the key class it is needed a variable witch contains the name of the classes involved in the program (the variable testsuiteclass). The function returns the entity of the key class in the query. """

def FindInvolvedEntity(query):
	patron = re.compile('.* ANCESTOR IS KEY\(\'([aA-zZ]+)\', \'([aA-zZ]+)\'\) .*')
	entity = (patron.sub("\g<1>", query), patron.sub("\g<2>", query))
	
	return entity;

""" FindAssciateList function 

This is another function that has to be developed or changed in order to apply the FRM, IO and FDO algorithms. This function receives a variable with a list of entities of a class, and returns the name of the list witch contains this list. These variables have to be also defined, in our example, at the end of the definitions of the entities of each class these variables are defined. """

def FindAncestorClass(QueryList):
	object = QueryList[0];
	for entity in TutorList:
		if (object.name == entity.name):
			return TutorList;
			break;
	for entity in StudentList:
		if (object.name == entity.name):
			return StudentList;
			break;
	for entity in FinalProjectList:
		if (object.name == entity.name):
			return FinalProjectList;
			break;

""" IOConstraintAlgorithm function

This function contains the algorithm witch find the entities of two classes witch meet the constraints for the IO mutation operator. The GQL query under study is a strong one, so in case of a no strong GQL query this algorithm will be different. """

def IOConstraintAlgorithm(QueryList, number, involvedVariable, involvedEntity, AncestorList):
	newparentList = []
	for objectAncestor in AncestorList:
		if objectAncestor.parent():
			parentobject = objectAncestor.parent()
			if (parentobject.name == involvedEntity[1]):
				if (hasattr(objectAncestor, involvedVariable)):
					if (getattr(objectAncestor, involvedVariable) > int(number)):
						newparentList.append(objectAncestor.name)
				else:
					for objectQuery in QueryList:
						parentobjectquery = objectQuery.parent()
						if (objectAncestor.name == parentobjectquery.name):
							if (getattr(objectQuery, involvedVariable) > int(number)):
								newparentList.append(objectAncestor.name)
	return newparentList

""" IODataSetGenerationAlgorithm function

This function contains the algorithm witch base on the defined entities gives new ones witch meet the constraints for the IO mutation operator. The GQL query under study is a strong one, so in case of a no strong GQL query this algorithm will be different. """ 

def IODataSetGenerationAlgorithm(QueryList, number, involvedVariable, involvedEntity, AncestorList):
	newparentList = []
	for objectAncestor in AncestorList:
		if objectAncestor.parent():
			parentobject = objectAncestor.parent()
			if (parentobject.name == involvedEntity[1]):
				if (hasattr(objectAncestor, involvedVariable)):
					if (getattr(objectAncestor, involvedVariable) <= int(number)):
							element = (objectAncestor.name, getattr(objectAncestor, involvedVariable) + int(number))
							newparentList.append(element)
				else:
					for objectQuery in QueryList:
						parentobjectquery = objectQuery.parent()
						if (objectAncestor.name == parentobjectquery.name):
							if (getattr(objectQuery, involvedVariable) <= int(number)):
								element = (objectAncestor.name, getattr(objectQuery, involvedVariable) + int(number))
								newparentList.append(element)
	return newparentList
	
# FDO Mutation operators functions #

""" Class FDOAlgorithmPage 

This class contains the structure of the FDOalgorithm page. This part of the program can be used with strong GQL queries. This class analyses a GQL query and check the entities witch meet the constrains of the FDO mutation operator and give another new entities witch meet the constrains. """

class FDOAlgorithmPage(webapp.RequestHandler):
	"""The numbers which are applied the FDO mutation operator are in comparisons"""
	def post(self):
		self.response.out.write('<html><body><h1>The query to execute with the algorithm:</h1><pre>')
		file = cgi.escape(self.request.get('contentFDOalgorithm'))
		query = readFileLine(file)
		self.response.out.write('<p><b>%s</b>' % query)
		date = FindDate(query)
		self.response.out.write('<p><b>Date in the comparison:</b> %s  ' % date)
		involvedVariable = FindInvolvedVariableFDO(query)
		self.response.out.write('<p><b>Involved variable:</b> %s  ' % involvedVariable)
		involvedOperator = FindInvolvedOperator(query)
		constraintOperator = ChangeOperator(involvedOperator)
		involvedEntity = FindInvolvedEntity(query)
		self.response.out.write('<p><b>Involved entity:</b> %s  ' % str(involvedEntity))
		QueryClass = FindClass(query)
		QueryList = FindAssociateList(QueryClass)
		QueryAncestorList = FindAncestorClass(QueryList)
		self.response.out.write('<p><b>Involved class:</b> %s  ' % QueryClass)
		constraintList = FDOConstraintAlgorithm(QueryList, date, involvedVariable, involvedEntity, QueryAncestorList, constraintOperator)
		self.response.out.write('<p><b>Entities which meet the constraints:</b> %s  ' % constraintList)
		newdatasetList = FDODataSetGenerationAlgorithm(QueryList, date, involvedVariable, involvedEntity, QueryAncestorList, involvedOperator)
		self.response.out.write('<p><b>New entities which meet the constraints:</b> %s  ' % newdatasetList)
		self.response.out.write('</pre></body></html>')

""" FindDate function

The queries under study for the FDO mutation operator are the ones with dates in relational relations. This function returns the date involved in the query. """

def FindDate(query):
	patron = re.compile('.*([0-9]{4}-[0-9]{2}-[0-9]{2}).*')
	date = patron.sub("\g<1>", query)
	return date;

""" FindInvolvedVariableFDO function

The queries under study for the FDO mutation operator are the ones with dates in relational relations. This function returns the variable involved in that relational relation. """

def FindInvolvedVariableFDO(query):
	patron = re.compile('.* ([aA-zZ]+) [<|>|=|!=|<=|>=] DATETIME.*')
	variable = patron.sub("\g<1>", query)
	return variable;

""" FindInvolvedOperator function

The queries under study for the FDO mutation operator are the ones with dates in relational relations. This function returns the relational operator involved in that relational relation. """

def FindInvolvedOperator(query):
	patron = re.compile('.* ([aA-zZ]+) ([<|>|=|!=|<=|>=]) DATETIME.*')
	variable = patron.sub("\g<2>", query)
	return variable;

""" ChangeOperator function

The queries under study for the FDO mutation operator are the ones with dates in relational relations. This function receives the involved relational operator in that relational relation, and returns the one witch helps to meet the constraints for the FDO mutation operator. """

def ChangeOperator(involvedOperator):
	relationalOperators = {'>':'<', '<':'>', '>=':'<=', '<=':'>='}
	return relationalOperators[involvedOperator]

""" FDOConstraintAlgorithm function

This function contains the algorithm witch find the entities of two classes witch meet the constraints for the FDO mutation operator. The GQL query under study is a strong one, so in case of a no strong GQL query this algorithm will be different. """

def FDOConstraintAlgorithm(QueryList, date, involvedVariable, involvedEntity, AncestorList, constraintOperator):
	newparentList = []
	for objectAncestor in AncestorList:
		if objectAncestor.parent():
			parentobject = objectAncestor.parent()
			if (parentobject.name == involvedEntity[1]):
				if (hasattr(objectAncestor, involvedVariable)):
					if (eval(str(getattr(objectAncestor, involvedVariable)) + constraintOperator + date)):
						newparentList.append(objectAncestor.name)
				else:
					for objectQuery in QueryList:
						parentobjectquery = objectQuery.parent()
						if (objectAncestor.name == parentobjectquery.name):
							if (eval(str(getattr(objectQuery, involvedVariable)) + constraintOperator + date)):
								newparentList.append(objectAncestor.name)
	return newparentList

""" FDODataSetGenerationAlgorithm function

This function contains the algorithm witch base on the defined entities gives new ones witch meet the constraints for the FDO mutation operator. The GQL query under study is a strong one, so in case of a no strong GQL query this algorithm will be different. """ 

def FDODataSetGenerationAlgorithm(QueryList, date, involvedVariable, involvedEntity, AncestorList, constraintOperator):
	newparentList = []
	for objectAncestor in AncestorList:
		if objectAncestor.parent():
			parentobject = objectAncestor.parent()
			if (parentobject.name == involvedEntity[1]):
				if (hasattr(objectAncestor, involvedVariable)):
					if (eval(str(getattr(objectAncestor, involvedVariable)) + constraintOperator + date)):
							element = (objectAncestor.name, getYear(date, constraintOperator))
							newparentList.append(element)
				else:
					for objectQuery in QueryList:
						parentobjectquery = objectQuery.parent()
						if (objectAncestor.name == parentobjectquery.name):
							if (eval(str(getattr(objectQuery, involvedVariable)) + constraintOperator + date)):
								element = (objectAncestor.name, getattr(objectQuery, involvedVariable).year + 1)
								newparentList.append(element)
	return newparentList

""" getYear function

The queries under study for the FDO mutation operator are the ones with dates in relational relations. This function receives the involved date and the relational operator witch meets the constraints. The function returns the year of the date, and according to the relational operator add or subtract one unit to the year. """

def getYear(date, constraintOperator):
	Operations = {'>':'-', '<':'+', '>=':'-', '<=':'+'}
	patron = re.compile('([0-9]{4})-([0-9]{2})-([0-9]{2})')
	year = patron.sub("\g<1>", date)
	
	return eval (year + Operations[constraintOperator] + '1')

# LIM Mutation operators functions #

""" Class LIMAlgorithmPage 

This class contains the structure of the LIMalgorithm page. This part of the program can be used with strong GQL queries. This class analyses a GQL query and check the entities witch meet the constrains of the LIM mutation operator and give another new entities witch meet the constrains. """

def FindNumbersLIM(query):
	patronLIM = re.compile('LIMIT [0-9]+, [0-9]+')
	limquery = patronLIM.findall(query)
	digits = re.findall('[0-9]+', str(limquery))
	return digits;	

class LIMAlgorithmPage(webapp.RequestHandler):
	def post(self):
		self.response.out.write('<html><body><h1>The query to execute with the algorithm:</h1><pre>')
		file = cgi.escape(self.request.get('contentLIMalgorithm'))
		query = readFileLine(file)
		self.response.out.write('<p><b>%s</b>' % query)
		NumbersLIM = FindNumbersLIM(query)
		self.response.out.write('<p><b>Offset number in the limit:</b> %s  ' % NumbersLIM[0])
		self.response.out.write('<p><b>Limit number in the limit:</b> %s  ' % NumbersLIM[1])
		self.response.out.write('<p><b>Diference between Limit and Offset numbers:</b> %s  ' % str(int(NumbersLIM[1]) - int(NumbersLIM[0])))
		self.response.out.write('<p>Just when the offset is equal to 0 the original and mutant outputs are equivalents.\nIf a new entity is created, the outputs are still the same. In other cases, the\noutputs are always different. </p>')

		if (int(NumbersLIM[1]) - int(NumbersLIM[0]) == int(NumbersLIM[1])):
			self.response.out.write('<p>The original and mutant outputs are always equivalent.</p>')

		self.response.out.write('</pre></body></html>')


# DST Mutation operator functions #

""" Class DSTAlgorithmPage 

This class contains the structure of the DSTalgorithm page. This part of the program can be used with strong GQL queries. This class analyses a GQL query and check the entities witch meet the constrains of the DST mutation operator and give another new entities witch meet the constrains. """


class DSTAlgorithmPage(webapp.RequestHandler):
  def post(self):
	self.response.out.write('<html><body><h1>The query to execute with the algorithm:</h1><pre>')
	file = cgi.escape(self.request.get('contentDSTalgorithm'))
	query = readFileLine(file)
	self.response.out.write('<p><b>%s</b>' % query)
	QueryClass = FindClass(query)
	QueryList = FindAssociateList(QueryClass)
	self.response.out.write('<p><b>Involved classes:</b> %s  ' % QueryClass)
	SelectVariable = FindSelectVariable(query)
	self.response.out.write('<p><b>Select variable:</b> %s  ' % SelectVariable)
	constraintList = DSTConstraintAlgorithm(QueryList, QueryClass, SelectVariable)
	self.response.out.write('<p><b>Entities which meet the constraints:</b> %s  ' % constraintList)
	#for classlist in classlists:
	#	if (classlist != QueryList):
	#		self.response.out.write('<p><b>Entities under study:</b>')
	#		for elem in classlist:
	#			self.response.out.write(' %s  ' % elem.name)
	#		constraintList = DSTConstraintAlgorithm(QueryList, classlist, SelectVariable)
	#		self.response.out.write('<p><b>Entities which meet the constraints:</b> %s  ' % constraintList)
			#newdatasetList = DSTDataSetGenerationAlgorithm(QueryList, classlist, QueryClass)
			#self.response.out.write('<p><b>New entities which meet the constraints:</b> %s  ' % newdatasetList)
	
	self.response.out.write('</pre></body></html>')


def FindSelectVariable(query):
	patronDST = re.compile('SELECT ([A-z][a-z]+)')
	auxquery = patronDST.findall(query)
	variable = patronDST.sub("\g<1>", str(auxquery))
	value = variable[2:2]
	return value;


def ValueObtained(object, QueryClass, SelectVariable):
	if (QueryClass == 'Tutor'):
		if (SelectVariable == 'name'):
			return object.name;
		if (SelectVariable == 'surname'):
			return object.surname;
		if (SelectVariable == 'title'):
			return object.title;
		if (SelectVariable == 'department'):
			return object.department;
		if (SelectVariable == 'date'):
			return object.date;
	if (QueryClass == 'Student'):
		if (SelectVariable == 'name'):
			return object.name;
		if (SelectVariable == 'surname'):
			return object.surname;
		if (SelectVariable == 'title'):
			return object.title;
		if (SelectVariable == 'grade'):
			return object.grade;
		if (SelectVariable == 'date'):
			return object.date;
		if (SelectVariable == 'tutor'):
			return object.tutor;
	if (QueryClass == 'FinalProject'):
		if (SelectVariable == 'name'):
			return object.name;
		if (SelectVariable == 'surname'):
			return object.surname;
		if (SelectVariable == 'title'):
			return object.title;
		if (SelectVariable == 'date'):
			return object.date;
		if (SelectVariable == 'read'):
			return object.read;
		if (SelectVariable == 'pages'):
			return object.pages;


""" DSTConstraintAlgorithm function

This function contains the algorithm witch find the entities of two classes witch meet the constraints for the DST mutation operator. The GQL query under study is a strong one, so in case of a no strong GQL query this algorithm will be different. """

def DSTConstraintAlgorithm(ClassList, QueryClass, SelectVariable):	
	List = []
	for objectA in ClassList:
		#value = ValueObtained(objectA, QueryClass, SelectVariable)
		value = objectA.title
		for objectB in ClassList:
			if (objectA != objectB):
				if (objectB.title == value):
				#if (ValueObtained(objectB, QueryClass, SelectVariable) == value):
					newpair = (objectA.name, value)
					if not (newpair in List):
						List.append(newpair)

	return List
 
""" FRMDataSetGenerationAlgorithm function

This function contains the algorithm witch base on the defined entities gives new ones witch meet the constraints for the FRM mutation operator. The GQL query under study is a strong one, so in case of a no strong GQL query this algorithm will be different. """ 
 
def FRMDataSetGenerationAlgorithm(ClassAList, ClassBList, QueryClass):
	newparentList = []
	"""filename = QueryClass + "FRMGeneratedEntities";
	file = open(filename, "w");
	file.write("Generated Entities for FRM mutation operator \n")
	file.close"""
	
	for objectA in ClassAList:
		if objectA.parent():
			objectAparentname = objectA.parent()
			for objectB in ClassBList:
				if not(objectB.parent()):
					newpair = (objectAparentname.name, objectB.name);
					if not (objectAparentname.name == objectB.name):
						if not (newpair in newparentList):
							newparentList.append(newpair)
							#newEntityToFile(filename, str(objectAparentname), QueryClass, objectB)

	return newparentList


application = webapp.WSGIApplication(
                                     [('/', MainPage),
									 ('/inputfile', ResultsPage),
									 ('/FRMalgorithmfile', FRMAlgorithmPage),
									 ('/IOalgorithmfile', IOAlgorithmPage),
									 ('/FDOalgorithmfile', FDOAlgorithmPage),
									 ('/LIMalgorithmfile', LIMAlgorithmPage),
									 ('/DSTalgorithmfile', DSTAlgorithmPage),],
                                     debug=True)

def main():
  run_wsgi_app(application)

if __name__ == "__main__":
  main()