	--- MUTMIX ---

Autor: Lorena Gutiérrez Madroñal 
lorena.gutierrez@uca.es

MutMix es una aplicación que nos va a permitir obtener las diferentes combinaciones de un determinado estudio de mutación y nos va a dar también como resultado aquellos mutantes que van a poder ser considerados como "Mutantes de alta calidad" (dentro de éstos, los difíciles de matar). Del mismo modo se incluye una opción que nos va a dar un ranking tanto de los mutantes como de los casos de prueba de cada uno de los estudios.



¿CÓMO LA EJECUTO?

Nos vamos a la consola, localizamos el fichero "MutMix.exe" y escribiremos la instrucción correspondiente para que se ejecute un fichero en nuestro sistema:
" ./MutMix.exe "
" MutMix.exe "



OPCIONES DE MUTMIX

En File encontramos tres principales opciones:

- All combinations
- One combination
- Ranking
- Exit

En View encontramos una opción:

- View Results

En About encontramos una opción:

- MutMix



ALL COMBINATIONS

En la primera pestaña, nos crearmos la carpeta donde queremos que se almacenen los resultados que vamos a obtener.

En la segunda pestaña, seleccionamos el estudio a analizar (están adjuntados en la carpeta Estudios):

- TravelTest.txt
- LoanExt.txt
- MetaSearch.txt

Luego tendremos que indicar el número de combinaciones que queremos realizar (mínimo 3, máximo el número de casos de prueba - 1, en el caso de que nos pasemos nos dará un aviso).

Pulsamos Ok.

Si queremos ver los resultados podemos ir ahora a la pestaña VIEW->View Results, o yéndonos al directorio de trabajo.



ONE COMBINATIONS

En la primera pestaña, nos crearmos la carpeta donde queremos que se almacenen los resultados que vamos a obtener.

En la segunda pestaña, seleccionamos el estudio a analizar:

- TravelTest.txt
- LoanExt.txt
- MetaSearch.txt

Luego, tendremos que indicar la combinación exacta que queremos realizar:

Introducimos el número del caso de prueba en el casillero que está encima del botón "Add" y le damos a añadir. A medida que vayamos introduciendo los números irán apareciendo en el casillero que tenemos a mano izquierda. Una vez finalizado, pulsamos Ok.

Si queremos ver los resultados podemos ir ahora a la pestaña VIEW->View Results, o yéndonos al directorio de trabajo.



¿QUÉ FICHEROS OBTENEMOS COMO RESULTADO?

- Combinaciones_NºCombinaciones.txt: Fichero que contiene todas las combinaciones posibles en ese estudio según el número indicado (mínimo 3 y máximo número de casos de prueba - 1).

Ejemplo: Combinaciones_3.txt (Serán las combinaciones de 3 casos de prueba del estudio que se ha analizado).

Contenido: 
_0_1_2
_0_1_3
_0_1_4
_0_1_5
_0_1_6
_0_1_7
_0_1_8
_0_2_3
_0_2_4
_0_2_5
...

Cada columna corresponde a un caso de prueba 

- Combinations_Nºcombinación_Casos_de_prueba_usados.txt: Ficheros de todas las posibles combinaciones con los correspondientes estados después de aplicar el caso de prueba.

Se crearán tantos ficheros como combinaciones existan (número de líneas del fichero Combinaciones_3, por ejemplo)

Ejemplo: Fichero Combinations_3_0_1_5.txt. El 3 indica que hay 3 casos de prueba que se están estudiando. Y los casos de prueba que se están estudiando son: 0, 1 y 5.

Contenido:

0	1	0	
1	0	1	
1	0	0	
0	1	1	
1	0	0	
0	1	0	
1	1	1	
1	1	1
...

- Results_Nºcombinación_Casos_de_prueba_usados.txt: Ficheros de todas las posibles combinaciones con los correspondientes estados después de aplicar el caso de prueba, pero ahora se comprobarán si son mutantes de "Alta calidad". Para cada mutante que solo muera con un caso de prueba se le indicará sustituyendo el 1 por T+Nºcaso_de_prueba_que_lo_mata. Al final del fichero se realizará una suma de los mutantes que mata cada caso de prueba y otra línea que indica el número de veces que dicho caso de prueba mata a un mutante que también es matado por otro caso de prueba. En el caso de que esta última suma fuese 0, es decir no existe otros casos de prueba que maten a los mutantes, aparecerá una C (de calidad).

Se crearán tantos ficheros como combinaciones existan (número de líneas del fichero Combinaciones_3, por ejemplo)

Ejemplo: Fichero Results_3_0_1_5.txt. El 3 indica que hay 3 casos de prueba que se están estudiando. Y los casos de prueba que se están estudiando son: 0, 1 y 5.

Contenido:

0	T1	0	
1	0	1	
T0	0	0	
0	1	1	
T0	0	0	
0	T1	0	
1	1	1
...
35	39	2 -> Suma de los mutantes que solo es matado por el caso de prueba de la columna
31	14	30 -> Suma de 1 todavía existentes (Lo que quiere decir, que otros casos de prueba también los mata)



RANKING

Se nos abre una nueva pantalla bastante parecida a la principal.

En la primera pestaña, nos crearmos la carpeta donde queremos que se almacenen los resultados que vamos a obtener.

En la segunda pestaña, seleccionamos el estudio a analizar (están adjuntados en la carpeta Estudios):

- TravelTest.txt
- LoanExt.txt
- MetaSearch.txt

Una vez hecho esto, pulsamos Ok.

Si queremos ver los resultados podemos ir ahora a la pestaña VIEW->View Results, o yéndonos al directorio de trabajo.


-- ¿QUÉ FICHEROS OBTENEMOS COMO RESULTADO? --

- Ranking_Mutants.txt: En este fichero encontramos diferentes datos sobre los mutantes. En primer lugar, el número de veces que un mutante ha sido matado, donde se identificarán los mutantes por un número y a continuación las veces que éste ha sido matado.

Ejemplo: Vemos el título de la sección que estábamos comentando y seguidamente algunos de los mutantes que pertenecen al caso de prueba y las veces que ha sido matado.

 --- TIMES A MUTANT HAS BEEN KILLED --- 

Mutant 0 died: 20 times
Mutant 1 died: 20 times
Mutant 2 died: 20 times
Mutant 3 died: 20 times
...
Mutant 59 died: 14 times
Mutant 60 died: 0 times
Mutant 61 died: 6 times
Mutant 62 died: 5 times
...

Tras esta sección, se nos muestran los diferentes niveles de mutantes. Los niveles van desde 0 hasta el número de casos de prueba. Pertenecerán al nivel 1 aquellos mutantes que hayan sido matados 1 sola vez, al nivel 2 los que han sido matados 2 veces, etc. Cuanto más alto sea el nivel, más fácil es matar a dicho mutante. Los que pertenecen al nivel 0, es que todavía están vivos.

Ejemplo: Vemos el título de la seción que estábamos comentando y seguidamente los niveles. Indicados por "Level N", siendo N el número del nivel, y a continuación el número identificativo de cada uno de los mutantes que pertenezcan a dicho nivel.

 --- LEVELS --- 

Level 1:  5 10 15 54 59 60 61 63 64 65 67 68 69 70 82 102 110 111 112 121 124 128 131 132 133 134 136 137 138 141 142 143 144 145 146 147 148 149 151 157 158 159 163 164 165 166 167 168 169 170 171 180 181 182 183 184 185 189 190 191 195 197 198 199 203 206 207 208
Level 2:  25 74 76 81 83 85 119 127 130 187 200 201 202
Level 3:  20 45 72 100 101 116 123 135 140 152 154 155 176 177 178 192 193 194
Level 4:  18 103 109 122 186
...

- Ranking_TestCases.txt: Visualizaremos los diferentes datos sobre los casos de prueba. La primera sección trata sobre el número de mutantes que mata cada uno de los casos de prueba, donde se identificarán cada uno de los casos de prueba por un número y posteriormente se indican los mutantes que éste ha matado.

Ejemplo: Vemos el título de la sección que estábamos comentando, y seguidamente cada uno de los casos de prueba con el número de mutantes que ha matado.

 --- MUTANTS KILLED BY EACH TEST CASE --- 

Test case 0 kills: 36 mutants
Test case 1 kills: 39 mutants
Test case 2 kills: 45 mutants
Test case 3 kills: 51 mutants
Test case 4 kills: 43 mutants
...

La siguiente sección trata sobre el número de mutantes que ha matado cada caso de prueba pero de forma única, es decir que esos mutantes solamente han sido matados por un caso de prueba. Estos mutantes, en el fichero anterior, tendrán un 1 en la sección que indicaba número de veces que ha sido matado.

En el caso de que encontremos casos de prueba que tienen un 0 en esta sección, es decir no matan de forma única a ningún mutante, o ningún mutante ha sido matado exclusivamente por dicho caso de prueba, quiere decir que este caso de prueba es redundante. Podríamos quitar este caso de prueba del estudio y seguiríamos obteniendo los mismos resultados.

Ejemplo: Vemos el título de la sección que estábamos comentando, y seguidamente cada uno de los casos de prueba con el número de mutantes que ha matado de forma única. Seguidamente, aparecen los identificadores de los mutantes que cumplen la condición anterior.

 --- MUTANTS KILLED BY EACH TEST CASE UNIQUELY --- 

Test case 0 kills: 5 mutants uniquely
Test case 1 kills: 14 mutants uniquely
Test case 2 kills: 14 mutants uniquely
Test case 3 kills: 15 mutants uniquely
Test case 4 kills: 0 mutants uniquely
Test case 5 kills: 5 mutants uniquely
...

En este ejemplo podríamos quitar el caso de prueba 4, que si observamos el ejemplo anterior (número total de mutantes matados), no tiene relación la cantidad de mutantes matados con el que el caso de prueba mate a mutantes de forma única. Cuanto más alto sea el número de mutantes que mata un caso de prueba de forma única, más difícil es que se convierta en un caso de prueba redundante.



VIEW RESULTS

Se nos abre una nueva pantalla y de ahí seleccionamos el fichero que queramos visualizar de nuestro estudio, cualquiera de los comentados anteriormente (obviamente tendremos que buscarlo en nuestro directorio de trabajo).

Le damos a visualizar y aparecerá en el formulario.



ABOUT

Muestra información sobre la autora, grupo de investigación y el motivo de la creación de MutMix. 
