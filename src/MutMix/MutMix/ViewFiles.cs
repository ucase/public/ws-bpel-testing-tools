
using System;

namespace MutMix
{


	public partial class ViewFiles : Gtk.Window
	{
		protected virtual void OnBtnViewClicked (object sender, System.EventArgs e)
		{
			System.IO.StreamReader file = new System.IO.StreamReader(FileCh.Filename);
		
			TextVF.Buffer.Text = file.ReadToEnd();
			file.Close();
		}

		public ViewFiles () : base(Gtk.WindowType.Toplevel)
		{
			this.Build ();
		}
	}
}
