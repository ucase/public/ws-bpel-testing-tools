// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace MutMix {
    
    
    public partial class AboutMutMix {
        
        private Gtk.VBox vbox1;
        
        private Gtk.Label label1;
        
        private Gtk.ScrolledWindow GtkScrolledWindow;
        
        private Gtk.TextView textview1;
        
        protected virtual void Build() {
            Stetic.Gui.Initialize(this);
            // Widget MutMix.AboutMutMix
            this.Name = "MutMix.AboutMutMix";
            this.Title = Mono.Unix.Catalog.GetString("AboutMutMix");
            this.Icon = Stetic.IconLoader.LoadIcon(this, "gtk-dialog-info", Gtk.IconSize.Menu, 16);
            this.WindowPosition = ((Gtk.WindowPosition)(1));
            this.Modal = true;
            // Container child MutMix.AboutMutMix.Gtk.Container+ContainerChild
            this.vbox1 = new Gtk.VBox();
            this.vbox1.Name = "vbox1";
            this.vbox1.Spacing = 6;
            // Container child vbox1.Gtk.Box+BoxChild
            this.label1 = new Gtk.Label();
            this.label1.Name = "label1";
            this.label1.LabelProp = Mono.Unix.Catalog.GetString("<b>ABOUT MUTMIX</b>");
            this.label1.UseMarkup = true;
            this.label1.UseUnderline = true;
            this.vbox1.Add(this.label1);
            Gtk.Box.BoxChild w1 = ((Gtk.Box.BoxChild)(this.vbox1[this.label1]));
            w1.Position = 0;
            w1.Expand = false;
            w1.Fill = false;
            w1.Padding = ((uint)(11));
            // Container child vbox1.Gtk.Box+BoxChild
            this.GtkScrolledWindow = new Gtk.ScrolledWindow();
            this.GtkScrolledWindow.Name = "GtkScrolledWindow";
            this.GtkScrolledWindow.ShadowType = ((Gtk.ShadowType)(1));
            this.GtkScrolledWindow.BorderWidth = ((uint)(15));
            // Container child GtkScrolledWindow.Gtk.Container+ContainerChild
            this.textview1 = new Gtk.TextView();
            this.textview1.Buffer.Text = "                         --- MutMix ---\n\nMutMix is a program created in order to do\nsome specific tasks in the mutation analysis.\n\nMutMix is a program in which we can know \nif there are quality mutants and a mutants\nranking as test cases ranking.\n\nMutMix has been created to UCASE \ninvestigation group by Lorena Gutiérrez \nMadroñal for her Thesis Proyect.";
            this.textview1.CanFocus = true;
            this.textview1.Name = "textview1";
            this.textview1.Editable = false;
            this.GtkScrolledWindow.Add(this.textview1);
            this.vbox1.Add(this.GtkScrolledWindow);
            Gtk.Box.BoxChild w3 = ((Gtk.Box.BoxChild)(this.vbox1[this.GtkScrolledWindow]));
            w3.Position = 1;
            this.Add(this.vbox1);
            if ((this.Child != null)) {
                this.Child.ShowAll();
            }
            this.DefaultWidth = 331;
            this.DefaultHeight = 325;
            this.Show();
        }
    }
}
