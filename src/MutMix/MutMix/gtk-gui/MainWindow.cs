// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------



public partial class MainWindow {
    
    private Gtk.UIManager UIManager;
    
    private Gtk.Action FileAction;
    
    private Gtk.Action executeAction;
    
    private Gtk.Action propertiesAction;
    
    private Gtk.Action quitAction;
    
    private Gtk.Action ViewAction;
    
    private Gtk.Action findAction;
    
    private Gtk.Action AboutAction;
    
    private Gtk.Action dialogInfoAction;
    
    private Gtk.Action sortDescendingAction;
    
    private Gtk.VBox vbox1;
    
    private Gtk.VBox vbox3;
    
    private Gtk.VBox vbox2;
    
    private Gtk.MenuBar menubar;
    
    private Gtk.Label label1;
    
    private Gtk.FileChooserButton FileChResult;
    
    private Gtk.FileChooserButton FileChEst;
    
    private Gtk.HBox hbox3;
    
    private Gtk.VBox vbox5;
    
    private Gtk.Label LabAllCase;
    
    private Gtk.Label LabOneCase;
    
    private Gtk.Entry EntryAllTest;
    
    private Gtk.VBox vbox6;
    
    private Gtk.SpinButton SpComb;
    
    private Gtk.Entry EntryCase;
    
    private Gtk.Button BtnAdd;
    
    private Gtk.HBox hbox2;
    
    private Gtk.Image image1;
    
    private Gtk.Button BtnOk;
    
    protected virtual void Build() {
        Stetic.Gui.Initialize(this);
        // Widget MainWindow
        this.UIManager = new Gtk.UIManager();
        Gtk.ActionGroup w1 = new Gtk.ActionGroup("Default");
        this.FileAction = new Gtk.Action("FileAction", Mono.Unix.Catalog.GetString("File"), null, null);
        this.FileAction.ShortLabel = Mono.Unix.Catalog.GetString("File");
        w1.Add(this.FileAction, null);
        this.executeAction = new Gtk.Action("executeAction", Mono.Unix.Catalog.GetString("All combinations"), null, "gtk-execute");
        this.executeAction.ShortLabel = Mono.Unix.Catalog.GetString("All combinations");
        w1.Add(this.executeAction, null);
        this.propertiesAction = new Gtk.Action("propertiesAction", Mono.Unix.Catalog.GetString("One combination"), null, "gtk-properties");
        this.propertiesAction.ShortLabel = Mono.Unix.Catalog.GetString("One combination");
        w1.Add(this.propertiesAction, null);
        this.quitAction = new Gtk.Action("quitAction", Mono.Unix.Catalog.GetString("Exit"), null, "gtk-quit");
        this.quitAction.ShortLabel = Mono.Unix.Catalog.GetString("Exit");
        w1.Add(this.quitAction, null);
        this.ViewAction = new Gtk.Action("ViewAction", Mono.Unix.Catalog.GetString("View"), null, null);
        this.ViewAction.ShortLabel = Mono.Unix.Catalog.GetString("View");
        w1.Add(this.ViewAction, null);
        this.findAction = new Gtk.Action("findAction", Mono.Unix.Catalog.GetString("Results files"), null, "gtk-find");
        this.findAction.ShortLabel = Mono.Unix.Catalog.GetString("Results files");
        w1.Add(this.findAction, null);
        this.AboutAction = new Gtk.Action("AboutAction", Mono.Unix.Catalog.GetString("About"), null, null);
        this.AboutAction.ShortLabel = Mono.Unix.Catalog.GetString("About");
        w1.Add(this.AboutAction, null);
        this.dialogInfoAction = new Gtk.Action("dialogInfoAction", Mono.Unix.Catalog.GetString("MutMix"), null, "gtk-dialog-info");
        this.dialogInfoAction.ShortLabel = Mono.Unix.Catalog.GetString("MutMix");
        w1.Add(this.dialogInfoAction, null);
        this.sortDescendingAction = new Gtk.Action("sortDescendingAction", Mono.Unix.Catalog.GetString("Ranking"), null, "gtk-sort-descending");
        this.sortDescendingAction.ShortLabel = Mono.Unix.Catalog.GetString("Ranking");
        w1.Add(this.sortDescendingAction, null);
        this.UIManager.InsertActionGroup(w1, 0);
        this.AddAccelGroup(this.UIManager.AccelGroup);
        this.Name = "MainWindow";
        this.Title = Mono.Unix.Catalog.GetString("MutMix");
        this.Icon = Stetic.IconLoader.LoadIcon(this, "stock_chart-edit-type", Gtk.IconSize.Menu, 16);
        this.WindowPosition = ((Gtk.WindowPosition)(1));
        this.Resizable = false;
        this.AllowGrow = false;
        // Container child MainWindow.Gtk.Container+ContainerChild
        this.vbox1 = new Gtk.VBox();
        this.vbox1.Name = "vbox1";
        this.vbox1.Spacing = 6;
        // Container child vbox1.Gtk.Box+BoxChild
        this.vbox3 = new Gtk.VBox();
        this.vbox3.Name = "vbox3";
        this.vbox3.Spacing = 6;
        this.vbox3.BorderWidth = ((uint)(6));
        // Container child vbox3.Gtk.Box+BoxChild
        this.vbox2 = new Gtk.VBox();
        this.vbox2.Name = "vbox2";
        this.vbox2.Spacing = 6;
        this.vbox2.BorderWidth = ((uint)(3));
        // Container child vbox2.Gtk.Box+BoxChild
        this.UIManager.AddUiFromString("<ui><menubar name='menubar'><menu name='FileAction' action='FileAction'><menuitem name='executeAction' action='executeAction'/><menuitem name='propertiesAction' action='propertiesAction'/><menuitem name='sortDescendingAction' action='sortDescendingAction'/><menuitem name='quitAction' action='quitAction'/></menu><menu name='ViewAction' action='ViewAction'><menuitem name='findAction' action='findAction'/></menu><menu name='AboutAction' action='AboutAction'><menuitem name='dialogInfoAction' action='dialogInfoAction'/></menu></menubar></ui>");
        this.menubar = ((Gtk.MenuBar)(this.UIManager.GetWidget("/menubar")));
        this.menubar.Name = "menubar";
        this.vbox2.Add(this.menubar);
        Gtk.Box.BoxChild w2 = ((Gtk.Box.BoxChild)(this.vbox2[this.menubar]));
        w2.Position = 0;
        w2.Expand = false;
        w2.Fill = false;
        // Container child vbox2.Gtk.Box+BoxChild
        this.label1 = new Gtk.Label();
        this.label1.Name = "label1";
        this.label1.LabelProp = Mono.Unix.Catalog.GetString("Choose the folder which will save the results \nand the study file, then select the \ncombination number");
        this.vbox2.Add(this.label1);
        Gtk.Box.BoxChild w3 = ((Gtk.Box.BoxChild)(this.vbox2[this.label1]));
        w3.Position = 1;
        w3.Expand = false;
        w3.Fill = false;
        this.vbox3.Add(this.vbox2);
        Gtk.Box.BoxChild w4 = ((Gtk.Box.BoxChild)(this.vbox3[this.vbox2]));
        w4.Position = 0;
        w4.Expand = false;
        w4.Fill = false;
        // Container child vbox3.Gtk.Box+BoxChild
        this.FileChResult = new Gtk.FileChooserButton(Mono.Unix.Catalog.GetString("Select a result folder"), ((Gtk.FileChooserAction)(2)));
        this.FileChResult.Name = "FileChResult";
        this.FileChResult.ShowHidden = true;
        this.vbox3.Add(this.FileChResult);
        Gtk.Box.BoxChild w5 = ((Gtk.Box.BoxChild)(this.vbox3[this.FileChResult]));
        w5.Position = 1;
        w5.Expand = false;
        w5.Fill = false;
        w5.Padding = ((uint)(5));
        // Container child vbox3.Gtk.Box+BoxChild
        this.FileChEst = new Gtk.FileChooserButton(Mono.Unix.Catalog.GetString("Select the study file"), ((Gtk.FileChooserAction)(0)));
        this.FileChEst.Name = "FileChEst";
        this.vbox3.Add(this.FileChEst);
        Gtk.Box.BoxChild w6 = ((Gtk.Box.BoxChild)(this.vbox3[this.FileChEst]));
        w6.Position = 2;
        w6.Expand = false;
        w6.Fill = false;
        w6.Padding = ((uint)(5));
        this.vbox1.Add(this.vbox3);
        Gtk.Box.BoxChild w7 = ((Gtk.Box.BoxChild)(this.vbox1[this.vbox3]));
        w7.Position = 0;
        w7.Expand = false;
        w7.Fill = false;
        // Container child vbox1.Gtk.Box+BoxChild
        this.hbox3 = new Gtk.HBox();
        this.hbox3.Name = "hbox3";
        this.hbox3.Spacing = 6;
        // Container child hbox3.Gtk.Box+BoxChild
        this.vbox5 = new Gtk.VBox();
        this.vbox5.Name = "vbox5";
        this.vbox5.Spacing = 6;
        // Container child vbox5.Gtk.Box+BoxChild
        this.LabAllCase = new Gtk.Label();
        this.LabAllCase.Name = "LabAllCase";
        this.LabAllCase.LabelProp = Mono.Unix.Catalog.GetString("Combinations number:\nMIN: 3 Test cases\nMAX: Last test case - 1");
        this.vbox5.Add(this.LabAllCase);
        Gtk.Box.BoxChild w8 = ((Gtk.Box.BoxChild)(this.vbox5[this.LabAllCase]));
        w8.Position = 0;
        w8.Expand = false;
        w8.Fill = false;
        // Container child vbox5.Gtk.Box+BoxChild
        this.LabOneCase = new Gtk.Label();
        this.LabOneCase.Name = "LabOneCase";
        this.LabOneCase.LabelProp = Mono.Unix.Catalog.GetString("Number Test Case:");
        this.vbox5.Add(this.LabOneCase);
        Gtk.Box.BoxChild w9 = ((Gtk.Box.BoxChild)(this.vbox5[this.LabOneCase]));
        w9.Position = 1;
        w9.Expand = false;
        w9.Fill = false;
        // Container child vbox5.Gtk.Box+BoxChild
        this.EntryAllTest = new Gtk.Entry();
        this.EntryAllTest.CanFocus = true;
        this.EntryAllTest.Name = "EntryAllTest";
        this.EntryAllTest.IsEditable = false;
        this.EntryAllTest.InvisibleChar = '●';
        this.vbox5.Add(this.EntryAllTest);
        Gtk.Box.BoxChild w10 = ((Gtk.Box.BoxChild)(this.vbox5[this.EntryAllTest]));
        w10.Position = 2;
        w10.Expand = false;
        w10.Fill = false;
        this.hbox3.Add(this.vbox5);
        Gtk.Box.BoxChild w11 = ((Gtk.Box.BoxChild)(this.hbox3[this.vbox5]));
        w11.Position = 0;
        w11.Fill = false;
        // Container child hbox3.Gtk.Box+BoxChild
        this.vbox6 = new Gtk.VBox();
        this.vbox6.Name = "vbox6";
        this.vbox6.Spacing = 6;
        // Container child vbox6.Gtk.Box+BoxChild
        this.SpComb = new Gtk.SpinButton(3, 100, 1);
        this.SpComb.CanFocus = true;
        this.SpComb.Name = "SpComb";
        this.SpComb.Adjustment.PageIncrement = 10;
        this.SpComb.ClimbRate = 1;
        this.SpComb.Numeric = true;
        this.SpComb.SnapToTicks = true;
        this.SpComb.Value = 3;
        this.vbox6.Add(this.SpComb);
        Gtk.Box.BoxChild w12 = ((Gtk.Box.BoxChild)(this.vbox6[this.SpComb]));
        w12.Position = 0;
        // Container child vbox6.Gtk.Box+BoxChild
        this.EntryCase = new Gtk.Entry();
        this.EntryCase.WidthRequest = 40;
        this.EntryCase.CanFocus = true;
        this.EntryCase.Name = "EntryCase";
        this.EntryCase.IsEditable = true;
        this.EntryCase.InvisibleChar = '●';
        this.vbox6.Add(this.EntryCase);
        Gtk.Box.BoxChild w13 = ((Gtk.Box.BoxChild)(this.vbox6[this.EntryCase]));
        w13.Position = 1;
        w13.Expand = false;
        w13.Fill = false;
        // Container child vbox6.Gtk.Box+BoxChild
        this.BtnAdd = new Gtk.Button();
        this.BtnAdd.CanFocus = true;
        this.BtnAdd.Name = "BtnAdd";
        this.BtnAdd.UseUnderline = true;
        this.BtnAdd.Label = Mono.Unix.Catalog.GetString("Add");
        this.vbox6.Add(this.BtnAdd);
        Gtk.Box.BoxChild w14 = ((Gtk.Box.BoxChild)(this.vbox6[this.BtnAdd]));
        w14.Position = 2;
        w14.Expand = false;
        w14.Fill = false;
        this.hbox3.Add(this.vbox6);
        Gtk.Box.BoxChild w15 = ((Gtk.Box.BoxChild)(this.hbox3[this.vbox6]));
        w15.Position = 1;
        w15.Fill = false;
        this.vbox1.Add(this.hbox3);
        Gtk.Box.BoxChild w16 = ((Gtk.Box.BoxChild)(this.vbox1[this.hbox3]));
        w16.Position = 1;
        w16.Expand = false;
        w16.Fill = false;
        // Container child vbox1.Gtk.Box+BoxChild
        this.hbox2 = new Gtk.HBox();
        this.hbox2.Name = "hbox2";
        this.hbox2.Spacing = 6;
        this.hbox2.BorderWidth = ((uint)(6));
        // Container child hbox2.Gtk.Box+BoxChild
        this.image1 = new Gtk.Image();
        this.image1.Name = "image1";
        this.image1.Xalign = 0F;
        this.image1.Pixbuf = Stetic.IconLoader.LoadIcon(this, "gtk-execute", Gtk.IconSize.Dialog, 48);
        this.hbox2.Add(this.image1);
        Gtk.Box.BoxChild w17 = ((Gtk.Box.BoxChild)(this.hbox2[this.image1]));
        w17.Position = 0;
        // Container child hbox2.Gtk.Box+BoxChild
        this.BtnOk = new Gtk.Button();
        this.BtnOk.CanFocus = true;
        this.BtnOk.Name = "BtnOk";
        this.BtnOk.UseUnderline = true;
        this.BtnOk.BorderWidth = ((uint)(6));
        this.BtnOk.Label = Mono.Unix.Catalog.GetString("Ok");
        this.hbox2.Add(this.BtnOk);
        Gtk.Box.BoxChild w18 = ((Gtk.Box.BoxChild)(this.hbox2[this.BtnOk]));
        w18.Position = 1;
        this.vbox1.Add(this.hbox2);
        Gtk.Box.BoxChild w19 = ((Gtk.Box.BoxChild)(this.vbox1[this.hbox2]));
        w19.Position = 2;
        w19.Expand = false;
        w19.Fill = false;
        this.Add(this.vbox1);
        if ((this.Child != null)) {
            this.Child.ShowAll();
        }
        this.DefaultWidth = 355;
        this.DefaultHeight = 408;
        this.LabOneCase.Hide();
        this.EntryAllTest.Hide();
        this.EntryCase.Hide();
        this.BtnAdd.Hide();
        this.Show();
        this.DeleteEvent += new Gtk.DeleteEventHandler(this.OnDeleteEvent);
        this.executeAction.Activated += new System.EventHandler(this.OnExecuteActionActivated);
        this.propertiesAction.Activated += new System.EventHandler(this.OnPropertiesActionActivated);
        this.quitAction.Activated += new System.EventHandler(this.OnQuitActionActivated);
        this.findAction.Activated += new System.EventHandler(this.OnFindActionActivated);
        this.dialogInfoAction.Activated += new System.EventHandler(this.OnHelpActionActivated);
        this.sortDescendingAction.Activated += new System.EventHandler(this.OnSortDescendingActionActivated);
        this.BtnAdd.Clicked += new System.EventHandler(this.OnBtnAddClicked);
        this.BtnOk.Clicked += new System.EventHandler(this.OnBtnOkClicked);
    }
}
