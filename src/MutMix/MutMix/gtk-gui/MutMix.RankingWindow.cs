// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace MutMix {
    
    
    public partial class RankingWindow {
        
        private Gtk.UIManager UIManager;
        
        private Gtk.Action FileAction;
        
        private Gtk.Action propertiesAction;
        
        private Gtk.Action sortDescendingAction;
        
        private Gtk.Action quitAction;
        
        private Gtk.Action executeAction;
        
        private Gtk.Action ViewAction;
        
        private Gtk.Action findAction;
        
        private Gtk.Action AboutAction;
        
        private Gtk.Action dialogInfoAction;
        
        private Gtk.VBox vbox1;
        
        private Gtk.VBox vbox3;
        
        private Gtk.VBox vbox2;
        
        private Gtk.Label label1;
        
        private Gtk.FileChooserButton FileChFolderRanking;
        
        private Gtk.FileChooserButton FileChStudyRanking;
        
        private Gtk.HBox hbox3;
        
        private Gtk.Label label2;
        
        private Gtk.HBox hbox1;
        
        private Gtk.Label label4;
        
        private Gtk.VBox vbox4;
        
        private Gtk.Entry EntryNum;
        
        private Gtk.HSeparator hseparator1;
        
        private Gtk.Entry EntryDenom;
        
        private Gtk.Label label3;
        
        private Gtk.HBox hbox2;
        
        private Gtk.Image image1;
        
        private Gtk.Button BtnOkRanking;
        
        protected virtual void Build() {
            Stetic.Gui.Initialize(this);
            // Widget MutMix.RankingWindow
            this.UIManager = new Gtk.UIManager();
            Gtk.ActionGroup w1 = new Gtk.ActionGroup("Default");
            this.FileAction = new Gtk.Action("FileAction", Mono.Unix.Catalog.GetString("File"), null, null);
            this.FileAction.ShortLabel = Mono.Unix.Catalog.GetString("File");
            w1.Add(this.FileAction, null);
            this.propertiesAction = new Gtk.Action("propertiesAction", Mono.Unix.Catalog.GetString("One Combination"), null, "gtk-properties");
            this.propertiesAction.ShortLabel = Mono.Unix.Catalog.GetString("One Combination");
            w1.Add(this.propertiesAction, null);
            this.sortDescendingAction = new Gtk.Action("sortDescendingAction", Mono.Unix.Catalog.GetString("Ranking"), null, "gtk-sort-descending");
            this.sortDescendingAction.ShortLabel = Mono.Unix.Catalog.GetString("Ranking");
            w1.Add(this.sortDescendingAction, null);
            this.quitAction = new Gtk.Action("quitAction", Mono.Unix.Catalog.GetString("Exit"), null, "gtk-quit");
            this.quitAction.ShortLabel = Mono.Unix.Catalog.GetString("Exit");
            w1.Add(this.quitAction, null);
            this.executeAction = new Gtk.Action("executeAction", Mono.Unix.Catalog.GetString("All combinations"), null, "gtk-execute");
            this.executeAction.ShortLabel = Mono.Unix.Catalog.GetString("All combinations");
            w1.Add(this.executeAction, null);
            this.ViewAction = new Gtk.Action("ViewAction", Mono.Unix.Catalog.GetString("View"), null, null);
            this.ViewAction.ShortLabel = Mono.Unix.Catalog.GetString("View");
            w1.Add(this.ViewAction, null);
            this.findAction = new Gtk.Action("findAction", Mono.Unix.Catalog.GetString("Results files"), null, "gtk-find");
            this.findAction.ShortLabel = Mono.Unix.Catalog.GetString("Results files");
            w1.Add(this.findAction, null);
            this.AboutAction = new Gtk.Action("AboutAction", Mono.Unix.Catalog.GetString("About"), null, null);
            this.AboutAction.ShortLabel = Mono.Unix.Catalog.GetString("About");
            w1.Add(this.AboutAction, null);
            this.dialogInfoAction = new Gtk.Action("dialogInfoAction", Mono.Unix.Catalog.GetString("MutMix"), null, "gtk-dialog-info");
            this.dialogInfoAction.ShortLabel = Mono.Unix.Catalog.GetString("MutMix");
            w1.Add(this.dialogInfoAction, null);
            this.UIManager.InsertActionGroup(w1, 0);
            this.AddAccelGroup(this.UIManager.AccelGroup);
            this.WidthRequest = 340;
            this.Name = "MutMix.RankingWindow";
            this.Title = Mono.Unix.Catalog.GetString("RankingWindow");
            this.Icon = Stetic.IconLoader.LoadIcon(this, "stock_chart-edit-type", Gtk.IconSize.Menu, 16);
            this.WindowPosition = ((Gtk.WindowPosition)(1));
            this.Modal = true;
            this.Resizable = false;
            this.AllowGrow = false;
            // Container child MutMix.RankingWindow.Gtk.Container+ContainerChild
            this.vbox1 = new Gtk.VBox();
            this.vbox1.Name = "vbox1";
            this.vbox1.Spacing = 6;
            // Container child vbox1.Gtk.Box+BoxChild
            this.vbox3 = new Gtk.VBox();
            this.vbox3.Name = "vbox3";
            this.vbox3.Spacing = 6;
            this.vbox3.BorderWidth = ((uint)(6));
            // Container child vbox3.Gtk.Box+BoxChild
            this.vbox2 = new Gtk.VBox();
            this.vbox2.Name = "vbox2";
            this.vbox2.Spacing = 6;
            this.vbox2.BorderWidth = ((uint)(3));
            // Container child vbox2.Gtk.Box+BoxChild
            this.label1 = new Gtk.Label();
            this.label1.Name = "label1";
            this.label1.LabelProp = Mono.Unix.Catalog.GetString("Choose the folder which will save the results \nand the study file.");
            this.vbox2.Add(this.label1);
            Gtk.Box.BoxChild w2 = ((Gtk.Box.BoxChild)(this.vbox2[this.label1]));
            w2.Position = 0;
            w2.Expand = false;
            w2.Fill = false;
            this.vbox3.Add(this.vbox2);
            Gtk.Box.BoxChild w3 = ((Gtk.Box.BoxChild)(this.vbox3[this.vbox2]));
            w3.Position = 0;
            w3.Expand = false;
            w3.Fill = false;
            // Container child vbox3.Gtk.Box+BoxChild
            this.FileChFolderRanking = new Gtk.FileChooserButton(Mono.Unix.Catalog.GetString("Select a result folder"), ((Gtk.FileChooserAction)(2)));
            this.FileChFolderRanking.Name = "FileChFolderRanking";
            this.FileChFolderRanking.ShowHidden = true;
            this.vbox3.Add(this.FileChFolderRanking);
            Gtk.Box.BoxChild w4 = ((Gtk.Box.BoxChild)(this.vbox3[this.FileChFolderRanking]));
            w4.Position = 1;
            w4.Expand = false;
            w4.Fill = false;
            w4.Padding = ((uint)(5));
            // Container child vbox3.Gtk.Box+BoxChild
            this.FileChStudyRanking = new Gtk.FileChooserButton(Mono.Unix.Catalog.GetString("Select the study file"), ((Gtk.FileChooserAction)(0)));
            this.FileChStudyRanking.Name = "FileChStudyRanking";
            this.vbox3.Add(this.FileChStudyRanking);
            Gtk.Box.BoxChild w5 = ((Gtk.Box.BoxChild)(this.vbox3[this.FileChStudyRanking]));
            w5.Position = 2;
            w5.Expand = false;
            w5.Fill = false;
            w5.Padding = ((uint)(5));
            this.vbox1.Add(this.vbox3);
            Gtk.Box.BoxChild w6 = ((Gtk.Box.BoxChild)(this.vbox1[this.vbox3]));
            w6.Position = 0;
            w6.Expand = false;
            w6.Fill = false;
            // Container child vbox1.Gtk.Box+BoxChild
            this.hbox3 = new Gtk.HBox();
            this.hbox3.Name = "hbox3";
            this.hbox3.Spacing = 6;
            // Container child hbox3.Gtk.Box+BoxChild
            this.label2 = new Gtk.Label();
            this.label2.Name = "label2";
            this.label2.LabelProp = Mono.Unix.Catalog.GetString("Write the threshold to filter the quality\nmutants of the study. The threshold\ndepends on the test suite of the study (T)");
            this.hbox3.Add(this.label2);
            Gtk.Box.BoxChild w7 = ((Gtk.Box.BoxChild)(this.hbox3[this.label2]));
            w7.Position = 0;
            w7.Fill = false;
            this.vbox1.Add(this.hbox3);
            Gtk.Box.BoxChild w8 = ((Gtk.Box.BoxChild)(this.vbox1[this.hbox3]));
            w8.Position = 1;
            w8.Fill = false;
            // Container child vbox1.Gtk.Box+BoxChild
            this.hbox1 = new Gtk.HBox();
            this.hbox1.Name = "hbox1";
            this.hbox1.Spacing = 6;
            this.hbox1.BorderWidth = ((uint)(6));
            // Container child hbox1.Gtk.Box+BoxChild
            this.label4 = new Gtk.Label();
            this.label4.Name = "label4";
            this.label4.LabelProp = Mono.Unix.Catalog.GetString(" 1 -  ");
            this.hbox1.Add(this.label4);
            Gtk.Box.BoxChild w9 = ((Gtk.Box.BoxChild)(this.hbox1[this.label4]));
            w9.Position = 0;
            // Container child hbox1.Gtk.Box+BoxChild
            this.vbox4 = new Gtk.VBox();
            this.vbox4.Name = "vbox4";
            this.vbox4.Spacing = 6;
            // Container child vbox4.Gtk.Box+BoxChild
            this.EntryNum = new Gtk.Entry();
            this.EntryNum.WidthRequest = 100;
            this.EntryNum.CanFocus = true;
            this.EntryNum.Name = "EntryNum";
            this.EntryNum.Text = Mono.Unix.Catalog.GetString("Numerator");
            this.EntryNum.IsEditable = true;
            this.EntryNum.InvisibleChar = '●';
            this.vbox4.Add(this.EntryNum);
            Gtk.Box.BoxChild w10 = ((Gtk.Box.BoxChild)(this.vbox4[this.EntryNum]));
            w10.Position = 0;
            w10.Expand = false;
            w10.Fill = false;
            // Container child vbox4.Gtk.Box+BoxChild
            this.hseparator1 = new Gtk.HSeparator();
            this.hseparator1.Name = "hseparator1";
            this.vbox4.Add(this.hseparator1);
            Gtk.Box.BoxChild w11 = ((Gtk.Box.BoxChild)(this.vbox4[this.hseparator1]));
            w11.Position = 1;
            w11.Expand = false;
            w11.Fill = false;
            // Container child vbox4.Gtk.Box+BoxChild
            this.EntryDenom = new Gtk.Entry();
            this.EntryDenom.WidthRequest = 100;
            this.EntryDenom.CanFocus = true;
            this.EntryDenom.Name = "EntryDenom";
            this.EntryDenom.Text = Mono.Unix.Catalog.GetString("Denominator * T");
            this.EntryDenom.IsEditable = true;
            this.EntryDenom.InvisibleChar = '●';
            this.vbox4.Add(this.EntryDenom);
            Gtk.Box.BoxChild w12 = ((Gtk.Box.BoxChild)(this.vbox4[this.EntryDenom]));
            w12.Position = 2;
            w12.Expand = false;
            w12.Fill = false;
            this.hbox1.Add(this.vbox4);
            Gtk.Box.BoxChild w13 = ((Gtk.Box.BoxChild)(this.hbox1[this.vbox4]));
            w13.Position = 1;
            // Container child hbox1.Gtk.Box+BoxChild
            this.label3 = new Gtk.Label();
            this.label3.Name = "label3";
            this.label3.LabelProp = Mono.Unix.Catalog.GetString("The threshold will be\ngood or bad depending\non the number of\nmutants of the study");
            this.hbox1.Add(this.label3);
            Gtk.Box.BoxChild w14 = ((Gtk.Box.BoxChild)(this.hbox1[this.label3]));
            w14.Position = 2;
            w14.Expand = false;
            w14.Fill = false;
            this.vbox1.Add(this.hbox1);
            Gtk.Box.BoxChild w15 = ((Gtk.Box.BoxChild)(this.vbox1[this.hbox1]));
            w15.Position = 2;
            w15.Expand = false;
            w15.Fill = false;
            // Container child vbox1.Gtk.Box+BoxChild
            this.hbox2 = new Gtk.HBox();
            this.hbox2.Name = "hbox2";
            this.hbox2.Spacing = 6;
            this.hbox2.BorderWidth = ((uint)(6));
            // Container child hbox2.Gtk.Box+BoxChild
            this.image1 = new Gtk.Image();
            this.image1.Name = "image1";
            this.image1.Xalign = 0F;
            this.image1.Pixbuf = Stetic.IconLoader.LoadIcon(this, "gtk-execute", Gtk.IconSize.Dialog, 48);
            this.hbox2.Add(this.image1);
            Gtk.Box.BoxChild w16 = ((Gtk.Box.BoxChild)(this.hbox2[this.image1]));
            w16.Position = 0;
            // Container child hbox2.Gtk.Box+BoxChild
            this.BtnOkRanking = new Gtk.Button();
            this.BtnOkRanking.CanFocus = true;
            this.BtnOkRanking.Name = "BtnOkRanking";
            this.BtnOkRanking.UseUnderline = true;
            this.BtnOkRanking.BorderWidth = ((uint)(6));
            this.BtnOkRanking.Label = Mono.Unix.Catalog.GetString("Ok");
            this.hbox2.Add(this.BtnOkRanking);
            Gtk.Box.BoxChild w17 = ((Gtk.Box.BoxChild)(this.hbox2[this.BtnOkRanking]));
            w17.Position = 1;
            this.vbox1.Add(this.hbox2);
            Gtk.Box.BoxChild w18 = ((Gtk.Box.BoxChild)(this.vbox1[this.hbox2]));
            w18.Position = 3;
            w18.Expand = false;
            w18.Fill = false;
            this.Add(this.vbox1);
            if ((this.Child != null)) {
                this.Child.ShowAll();
            }
            this.DefaultWidth = 348;
            this.DefaultHeight = 381;
            this.Show();
            this.BtnOkRanking.Clicked += new System.EventHandler(this.OnBtnOkRankingClicked);
        }
    }
}
