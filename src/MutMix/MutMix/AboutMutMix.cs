
using System;

namespace MutMix
{


	public partial class AboutMutMix : Gtk.Window
	{
		protected virtual void OnBtnOkAboutActivated (object sender, System.EventArgs e)
		{
			this.Destroy();
		}
		
		
		
		public AboutMutMix () : base(Gtk.WindowType.Toplevel)
		{
			this.Build ();
		}
	}
}
