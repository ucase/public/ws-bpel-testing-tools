using System;
using Gtk;

namespace MutMix
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();
			MainWindow winAll = new MainWindow ();
			winAll.Show ();
			Application.Run ();
		}
	}
}
