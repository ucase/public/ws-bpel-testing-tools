
using System;

namespace MutMix
{


	public partial class RankingWindow : Gtk.Window
	{
		public RankingWindow () : base(Gtk.WindowType.Toplevel)
		{
			this.Build ();
		}
		
		protected virtual void OnBtnOkRankingClicked (object sender, System.EventArgs e)
		{
			string FileChIn = FileChStudyRanking.Filename;
			string FileOpIn = FileChStudyRanking.Filename;
			string DirChOut = FileChFolderRanking.Filename;		
			string linea = "";
			string aux;
			double NumeradorUmb = 0.0;
			double DenominadorUmb = 0.0;
			
			aux = EntryNum.Text;
			if (!aux.Equals("Numerator")){
				NumeradorUmb = Convert.ToDouble(aux);
				aux = EntryDenom.Text;
				if (!aux.Equals("Denominator * T")){
					DenominadorUmb = Convert.ToDouble(aux);
			
					
					// Crear matriz con todos los estados de los mutantes
				
					System.IO.StreamReader rm = new System.IO.StreamReader(FileChIn);
					string delimitaF = "	";
					char [] delimita = delimitaF.ToCharArray();
					
					linea = rm.ReadLine();
					
					Array elto = linea.Split(delimita);
					string matrixtring = elto.GetValue(0).ToString();
					int filas = Convert.ToInt32(matrixtring);
					matrixtring = elto.GetValue(1).ToString();
					int columnas = Convert.ToInt32(matrixtring);
					rm.Close();
					
					int[,] MatrixOriginal = new int[filas, columnas];
							
					MatrixOriginal = LeerFichero(filas, columnas, FileChIn);
					
					// Quita los casos de prueba redundantes y escribe la matriz de ejecución sin
					// redundantes en un fichero nuevo que se llama NombreComposiciónFiltrado.txt
					
					QuitaRedundantes(filas, columnas, MatrixOriginal);
					
					// Volvemos a leer la matriz de ejecución pero filtrada
					
					string nombre = FileOpIn.Remove(0, FileOpIn.LastIndexOf("/"));
					string FileFiltrado = DirChOut + nombre.Insert(nombre.IndexOf("."),"Filtrado");			
					System.IO.StreamReader r = new System.IO.StreamReader(FileFiltrado);
					
					linea = r.ReadLine();
					
					elto = linea.Split(delimita);
					matrixtring = elto.GetValue(0).ToString();
					filas = Convert.ToInt32(matrixtring);
					matrixtring = elto.GetValue(1).ToString();
					columnas = Convert.ToInt32(matrixtring);
					r.Close();
					
					int[,] MatrixEq = new int[filas, columnas];
					
					MatrixEq = LeerFichero(filas, columnas, FileFiltrado);
					
					// Realizamos otro filtrado para quitar los
					// mutantes equivalentes y erróneos
					
					FileOpIn = FileOpIn.Insert(FileOpIn.IndexOf("."),"Operadores");
					
					int numoperadores;
					
					// Cuenta el número de operadores que intervienen
					
					numoperadores = ContarOperadores(FileOpIn);
					
					int[] vectorop = new int[numoperadores];
					int[] vectoropTot = new int[numoperadores];
					string[] nombop = new string[numoperadores];
					
					// Se introduce el nombre de los operadores
					
					nombop = NombresOperadores(FileOpIn, numoperadores);
					
					// El primer vector contiene los mutantes generados por cada operador, quitando
					// los mutantes erróneos y equivalentes, y el segundo vector contiene el número
					// de mutantes generados por cada operador
					
					vectorop = OperadoresMutantesMuertos(MatrixEq, columnas, numoperadores, FileOpIn);
					vectoropTot = OperadoresTotalMutantes(MatrixEq, columnas, numoperadores, FileOpIn);
					
					// Calculamos el número de mutantes no equivalentes totales para
					// crear la nueva matriz con la que vamos a operar
					
					int mutantes = 0;
					for (int i = 0; i < numoperadores; i++){
						mutantes = mutantes + vectorop[i];
					}
					
					int[,] MatrixMut = new int[mutantes, columnas];
					
					MatrixMut = FiltradoMutantesEq(MatrixEq, filas, columnas, mutantes);
								
					RankingCasosDePrueba(DirChOut, columnas, mutantes, MatrixMut);
					RankingMutantes(DirChOut, columnas, mutantes, MatrixMut, filas, NumeradorUmb, DenominadorUmb);
					RankingOperadores(DirChOut, columnas, mutantes, MatrixMut, vectoropTot, vectorop, nombop);
					this.Destroy();
				}
				else{
					MutMix.ErrorWindow winerr = new MutMix.ErrorWindow ();
					winerr.Show();
				}
			}
			else{
				MutMix.ErrorWindow winerr = new MutMix.ErrorWindow ();
				winerr.Show();
			}
		}
		
		int[,] LeerFichero(int filas, int columnas, string File)
		{
			string linea = "";
			
			// Crear matriz con todos los estados de los mutantes
		
			System.IO.StreamReader rm = new System.IO.StreamReader(File);
			string delimitaF = "	";
			string matrixtring = "";
			char [] delimita = delimitaF.ToCharArray();
			
			linea = rm.ReadLine();
						
			int[,] Matrix = new int[filas, columnas];
			int i = 0;
			int j = 0;

			while((linea = rm.ReadLine()) != null){
				
				Array elto = linea.Split(delimita);
				for (j = 0; j < columnas; j++){
					matrixtring = elto.GetValue(j).ToString();
					Matrix[i,j] = Convert.ToInt32(matrixtring);
				}
				i++;
			}
			
			rm.Close();
			
			return Matrix;
		}
		
		void QuitaRedundantes(int filas, int columnas, int[,] Matrix)
		{
			string FileOpIn = FileChStudyRanking.Filename;
			string DirChOut = FileChFolderRanking.Filename;
			string nombre = FileOpIn.Remove(0, FileOpIn.LastIndexOf("/"));
			string FileFiltrado = DirChOut + nombre.Insert(nombre.IndexOf("."),"Filtrado");			
			string linea = "";
			int columna, fila, suma, i, j;
			int [] sumas = new int[columnas];
			suma = 0;
			
			System.IO.StreamWriter w = new System.IO.StreamWriter(FileFiltrado);
			string delimitaF = "	";
			
			// Ordenamos los casos de prueba por el número de mutantes que han matado
			// Primero calculamos el número de mutantes que mata cada uno
			
			for (columna = 0; columna < columnas; columna++){
				for (fila = 0; fila < filas; fila++){
					if (Matrix[fila, columna].Equals(1)){
						suma = suma + Matrix[fila, columna];
					}
				}
				sumas[columna] = suma;
				suma = 0;
			}
			
			// Ahora ordenamos las sumas y los índices de las columnas de la matriz
			
			int aux = 0;
			int[] indices = new int[columnas];
			
			for (i = 0; i < columnas; i++){
				indices[i] = i;
			}
			
			for (i = 0; i < columnas; i++){
				for (j = i + 1; j < columnas; j++){
					if (sumas[i] < sumas[j]){
						aux = sumas[i];
						sumas[i] = sumas[j];
						sumas[j] = aux;
						aux = indices[i];
						indices[i] = indices[j];
						indices[j] = aux;
					}
				}
			}
			
			
			// Introducimos el caso de prueba que mata a más mutantes y vamos comprobando
			// con el resto de casos de prueba si matan a algún mutante diferente. En cuanto
			// mate a un mutante de forma única se introduce. El recorrido lo haremos de mayor
			// a menor con respecto el número de mutantes que maten.
			
			int[,] MatrixFil = new int[filas, columnas];
			
			// Introducimos el caso de prueba que más mutantes mata
						
			for (i = 0; i < filas; i++){
				MatrixFil[i, 0] = Matrix[i, indices[0]];
			}
			
				
			bool diferente = true;
			int contador = 0;
			int maximo = 0;
			int indiceColum = 0;
			int columfil = 1; // Este controla la columna de la nueva matriz
			
			while (diferente){
				for (i = 0; i < columnas; i++){
					for (j = 0; j < filas; j++){
						if (Matrix[j, i].Equals(1)){
							if (SumaFilaCero(MatrixFil, columfil, j).Equals(0)){
								contador++;
							}
						}
					}
					
					if (maximo < contador){
						maximo = contador;
						indiceColum = i;
					}
					contador = 0;
				}
				
				if (!maximo.Equals(0)){
					// Ahora tenemos el índice de la columna (CP) que contiene mayor número de 
					// unos diferentes, esa columna es la que introducimos en la nueva matriz
									
					for (i = 0; i < filas; i++){
						MatrixFil[i, columfil] = Matrix[i, indiceColum];
					}
					
					columfil++;	
					maximo = 0;
					indiceColum = 0;
				}
				else{
					diferente = false;
				}
			}
			
			// Una vez filtrada se escribe en el fichero nombrecomposicionFiltrado.txt
			
			linea = filas + delimitaF + columfil;
			w.WriteLine(linea);
			linea = "";
			
			for (j = 0; j < filas; j++){
				for (i = 0; i < columfil; i++){
					linea = linea + MatrixFil[j, i] + delimitaF;
				}
				w.WriteLine(linea);
				linea = "";
			}
			
			w.Close();
		}
		
		int SumaFilaCero(int[,] MatrixFil, int columnas, int fila)
		{
			int suma = 0;
			int i;
			
			for (i = 0; i < columnas; i++){
				suma = suma + MatrixFil[fila, i];
			}
			
			return suma;
		}

		int ContarOperadores(string FilechOper)
		{
			System.IO.StreamReader r = new System.IO.StreamReader(FilechOper);
			string linea = "";			
			string delimitaF = " ";
			char [] delimita = delimitaF.ToCharArray();
			int contador = 0;
			
			// Leemos los operadores
			
			while ((linea = r.ReadLine()) != null){
			
				Array elto = linea.Split(delimita);
				string c = elto.GetValue(1).ToString();
				
				if (!c.Equals("0")){
					contador++;
				}
			}
			r.Close();
			return contador;			
		}
		
		string[] NombresOperadores(string FilechOper, int operadores)
		{
			System.IO.StreamReader r = new System.IO.StreamReader(FilechOper);
			string linea = "";			
			string delimitaF = " ";
			char [] delimita = delimitaF.ToCharArray();
			string[] nombres = new string[operadores];
			int i = 0;
			
			// Leemos los operadores
			
			while ((linea = r.ReadLine()) != null){
			
				Array elto = linea.Split(delimita);
				string c = elto.GetValue(1).ToString();
				
				if (!c.Equals("0")){
					nombres[i] = elto.GetValue(0).ToString();
					i++;
				}
			}
			r.Close();
			return nombres;
		}
		
		int[] OperadoresTotalMutantes(int[,] Matrix, int columnas, int operadores, string FichOper)
		{
			System.IO.StreamReader r = new System.IO.StreamReader(FichOper);

			int[] vectorop = new int[operadores];
			int indiceOp, mutantes, mutantesnoerroneos, i;
			string linea = "";			
			string delimitaF = " ";
			char [] delimita = delimitaF.ToCharArray();
			indiceOp = 0;
			i = 0;
			mutantes = 0;
						
			// Leemos los operadores
			
			while ((linea = r.ReadLine()) != null){
			
				Array elto = linea.Split(delimita);
				string c = elto.GetValue(1).ToString();
				
				if (!c.Equals("0")){
					string d = elto.GetValue(2).ToString();
					
					int m = Convert.ToInt32(c);
					int n = Convert.ToInt32(d);
										
					mutantes = m * n + i;
					mutantesnoerroneos = FiltradoMutantesError(columnas, i, mutantes, Matrix);
					vectorop[indiceOp] = mutantesnoerroneos;
					i = mutantes;
					indiceOp++;
				}
			}
			r.Close();
			return vectorop;
		}
		
		int[] OperadoresMutantesMuertos(int[,] Matrix, int columnas, int operadores, string FichOper)
		{
			System.IO.StreamReader r = new System.IO.StreamReader(FichOper);

			int[] vectorop = new int[operadores];
			int mutantes, i, mutantesnoequivalentes, indiceOp;
			string linea = "";			
			string delimitaF = " ";
			char [] delimita = delimitaF.ToCharArray();
			i = 0; // Indice del mutante por el que vamos
			indiceOp = 0;
			mutantes = 0; // Indica el número de mutantes al que tenemos que llegar
			
			// Leemos los operadores
			
			while ((linea = r.ReadLine()) != null){
			
				Array elto = linea.Split(delimita);
				string c = elto.GetValue(1).ToString();
				
				if (!c.Equals("0")){
					string d = elto.GetValue(2).ToString();
					
					int m = Convert.ToInt32(c);
					int n = Convert.ToInt32(d);
					
					// Calculamos el número de mutantes no equivalentes que tiene dicho operador
										
					mutantes = mutantes + m * n;
					mutantesnoequivalentes = FiltradoMutantesSM(columnas, i, mutantes, Matrix);
					vectorop[indiceOp] = mutantesnoequivalentes;
					i = mutantes;
					indiceOp++;
				}
			}
			r.Close();
			return vectorop;
		}

		int[,] FiltradoMutantesEq(int[,] Matrix, int filas, int columnas, int numfilas)
		{
			int[,] MatrixF = new int[numfilas, columnas];
			int columna, fila, indexfila;
			int suma = 0;
			int sumaerroneo = 2 * columnas;
			indexfila = 0;
			
			for (fila = 0; fila < filas; fila++){
				for (columna = 0; columna < columnas; columna++){
					suma = suma + Matrix[fila, columna];
				}
				
				if (!suma.Equals(0)){
					if (!suma.Equals(sumaerroneo)){
						for (columna = 0; columna < columnas; columna++){
							MatrixF[indexfila, columna]= Matrix[fila, columna];
						}
						indexfila++;
					}
				}
				suma = 0;
			}
			return MatrixF;
		}
		void RankingCasosDePrueba(string DirChOut, int columnas, int filas, int[,] MatrixMut)
		{
			string NomResul = DirChOut + "/Ranking_TestCases.txt";
			System.IO.StreamWriter w = new System.IO.StreamWriter(NomResul);

			int columna;			
			int fila;
			int columnasuma;
			int suma = 0;
			int contador = 0;
			string LineaToFile = "";
		
			LineaToFile = "\n --- MUTANTS KILLED BY EACH TEST CASE --- \n";
			w.WriteLine(LineaToFile);
			LineaToFile = "";
			
			// Número de mutantes muertos por cada caso de prueba
			
			for (columna = 0; columna < columnas; columna++){
				for (fila = 0; fila < filas; fila++){
					if (MatrixMut[fila, columna].Equals(1)){
						suma = suma + MatrixMut[fila, columna];
					}
					LineaToFile = "Test case " + columna + " kills: " + suma.ToString() + " mutants";
				}
				w.WriteLine(LineaToFile);
				LineaToFile = "";
				suma = 0;
			}
			
			// Mutantes que mata unicamente cada caso de prueba

			LineaToFile = "\n --- MUTANTS KILLED BY EACH TEST CASE UNIQUELY --- \n";
			w.WriteLine(LineaToFile);
			LineaToFile = "";
			string mutantes = "";
			
			for (columna = 0; columna < columnas; columna++){
				for (fila = 0; fila < filas; fila++){
					if (MatrixMut[fila, columna].Equals(1)){
						
						for (columnasuma = 0; columnasuma < columnas; columnasuma++){
							
							// Para que no cuente los mutantes erróneos
							
							if (MatrixMut[fila, columnasuma].Equals(1)){ 
								suma = suma + MatrixMut[fila, columnasuma];
							}
						}
												
						if (suma.Equals(1)){
							contador = contador + 1;
							mutantes = mutantes + " " + fila;
						}
						suma = 0;
					}
						
					if (contador.Equals(0)){
						LineaToFile = "Test case " + columna + " kills: " + contador.ToString() + " mutants uniquely";
					}
					else{
						LineaToFile = "Test case " + columna + " kills: " + contador.ToString() + " mutants uniquely - " + mutantes;
					}

				}
				w.WriteLine(LineaToFile);
				LineaToFile = "";
				mutantes = "";
				suma = 0;
				contador = 0;
			}
		
			w.Close();
		
		}
		
		
		void RankingMutantes(string DirChOut, int columnas, int filas, int[,] MatrixMut, int totalmutantes, double Num, double Denom)
		{
			string NomResul = DirChOut + "/Ranking_Mutants.txt";
			System.IO.StreamWriter w = new System.IO.StreamWriter(NomResul);

			int columna;			
			int fila;
			int suma = 0;
			string LineaToFile = "";
			int[] RankingMut = new int[filas];
						
			LineaToFile = "\n --- TIMES A MUTANT HAS BEEN KILLED --- \n";
			w.WriteLine(LineaToFile);
			LineaToFile = "";
			
			// Número de veces que un mutante ha sido matado
			
			for (fila = 0; fila < filas; fila++){
				for (columna = 0; columna < columnas; columna++){
					if (MatrixMut[fila, columna].Equals(1)){
						suma = suma + MatrixMut[fila, columna];
					}
					RankingMut[fila] = suma;
					LineaToFile = "Mutant " + fila + " died: " + suma.ToString() + " times";
				}
				w.WriteLine(LineaToFile);
				LineaToFile = "";
				suma = 0;
			}
			
			LineaToFile = "\n --- LEVELS --- \n";
			w.WriteLine(LineaToFile);
			LineaToFile = "";
			
			// Ordena los mutantes en niveles, según el número de veces que éste ha sido matado
			
			LineaToFile = Ordenar(RankingMut, columnas, filas);
			w.WriteLine(LineaToFile);
			
			LineaToFile = "--- QUALITY --- \n";
			w.WriteLine(LineaToFile);
			LineaToFile = "";
			
			LineaToFile = Calidad(RankingMut, columnas, filas, MatrixMut, totalmutantes, Num, Denom);
			w.WriteLine(LineaToFile);

			w.Close();
		
		}
		
		string Calidad(int[] Ranking, int columnas, int filas, int[,] MatrixMut, int totalmutantes, double NumUmb, double DenomUmb)
		{
			int i, j, fila;
			int sumaCP = 0; 
			int contadorcalidad = 0;
			string lineaF = "";
			string linea = "";
			double denominadorcalidad;
			double numeradorcalidad = 0.0;
			double calidad;
			double umbral = 1 - (NumUmb / (DenomUmb * columnas));
			
			denominadorcalidad = columnas * filas;
			
			for (i = 0; i < filas; i++){
							
				// Comprobamos los casos de prueba que lo matan
				for (j = 0; j < columnas; j++){
					if (MatrixMut[i,j].Equals(1)){
					
						// Si lo mata sumamos el conjunto de mutantes que muere por dicho caso de prueba
						for (fila = 0; fila < filas; fila++){
							if (MatrixMut[fila, j].Equals(1)){
								sumaCP = sumaCP + MatrixMut[fila, j];
							}
						}
						numeradorcalidad = numeradorcalidad + sumaCP;
						sumaCP = 0;
					}
				}
			
				calidad = 1 - (numeradorcalidad / denominadorcalidad);
			
				if (umbral > 0){
					if (calidad >= umbral){
						linea = linea + "Quality of M(" + i + ") = (" + numeradorcalidad + "/" + denominadorcalidad + ") = " + calidad + "\n";
						contadorcalidad++;
					}
				}
			
				lineaF = lineaF + "Quality of M(" + i + ") = (" + numeradorcalidad + "/" + denominadorcalidad + ") = " + calidad + "\n";
				numeradorcalidad = 0.0;	
			}
			
			double sobremuertos = 0.0;
			double sobretotal = 0.0;
			
			sobremuertos = ((contadorcalidad * 1.0) / (filas * 1.0)) * 100.0;
			sobretotal = ((contadorcalidad * 1.0) / (totalmutantes * 1.0)) * 100.0;
						
			linea = linea + "\n % KILLS = " + sobremuertos + "\n % TOTAL = " + sobretotal;
			lineaF = lineaF + "\n --- INTERESTING MUTANTS --- \n\n Umbral = 1 - " + NumUmb + "/" + DenomUmb + " * Number of Test Cases = " + umbral + "\n\n" + linea;

			return lineaF;
		}
		
		string Efectividad(int columnas, int filas, int[,] MatrixMut, int[] vectorop, string[] nombop)
		{
			int j;
			double efectividad = 0.0;
			string LineaToFile = "";
			double numeradorefectividad = 0.0;
			double denominadorefectividad = 0.0;
			int contadormutantes = vectorop[0];
			int numoperador = 0;
			int i = 0;
			
			while (numoperador < vectorop.Length){
				while (i < contadormutantes){
					for (j = 0; j < columnas; j++){								
						if (MatrixMut[i, j].Equals(1)){
							numeradorefectividad = numeradorefectividad + MatrixMut[i, j];
						}
					}
					i++;
				}

				denominadorefectividad = columnas * vectorop[numoperador];
					
				if (denominadorefectividad.Equals(0)){
					efectividad = 0;
				}
				else{
					efectividad = (numeradorefectividad / denominadorefectividad);
					LineaToFile = LineaToFile + "Effectiveness of " + nombop[numoperador] + " = " + efectividad + "\n";
				}
				
				numeradorefectividad = 0.0;
				denominadorefectividad = 0.0;
				efectividad = 0.0;
				numoperador++;
				i = contadormutantes;
				
				if (!numoperador.Equals(vectorop.Length))
					contadormutantes = i + vectorop[numoperador];
			}
			
			return LineaToFile;
		}
		
		int FiltradoMutantesSM(int columnas, int fila, int NumMutantes, int[,] MatrixMut)
		{
			int contador = 0;
			int columna;
			int suma = 0;
			int sumaerroneo = columnas * 2;
						
			while (fila < NumMutantes){
				for (columna = 0; columna < columnas; columna++){
					suma = suma + MatrixMut[fila, columna];
				}
				
				if (!suma.Equals(0)){
					if (!suma.Equals(sumaerroneo)){
						contador++;
					}
				}
				suma = 0;
				fila++;
			}
			
			return contador;
		}
		
		int FiltradoMutantesError(int columnas, int fila, int NumMutantes, int[,] MatrixMut)
		{
			int contador = 0;
			int columna;
			int suma = 0;
			int sumaerroneo = columnas * 2;
						
			while (fila < NumMutantes){
				for (columna = 0; columna < columnas; columna++){
					suma = suma + MatrixMut[fila, columna];
				}
				
				if (!suma.Equals(sumaerroneo)){
					contador++;
				}
				
				suma = 0;
				fila++;
			}
			
			return contador;
		}
		
		string Ordenar(int[] Ranking, int columnas, int filas)
		{
			int i;
			int nivel;
			string linea = "";
			string lineaF = "";
			
			for (nivel = 0; nivel <= columnas; nivel++){
				for (i = 0; i < filas; i++){
					if (Ranking[i].Equals(nivel))
						linea = linea + " " + i.ToString();
				}
				if (!linea.Equals("")){
					lineaF = lineaF + "Level " + nivel + ": " + linea + "\n";
				}
				linea = "";
			}
			
			return lineaF;
		}
	
		void RankingOperadores(string DirChOut, int columnas, int filas, int[,] MatrixMut, int[] vectoropTot, int[] vectorop, string[] nombop)		
		{
			
			string NomResul = DirChOut + "/Ranking_Operators.txt";
			System.IO.StreamWriter w = new System.IO.StreamWriter(NomResul);
			
			int fila, i, j;
			int sumaCP = 0;
			double sumaOp = 0.0;
			double calidad = 0.0;
			double calidadOp = 0.0;
			double calidadOpE = 0.0;
			string LineaToFile = "";
			string linea = "\n -- |M| -- \n\n";
			double numeradorcalidad = 0;
			double denominadorcalidad = 0;
			int contadormutantes = 0;
			int numoperador = 0;
			denominadorcalidad = filas * columnas;
			
			LineaToFile = "\n -- |M| - |E| --\n";
			w.WriteLine(LineaToFile);
			LineaToFile = "";
							
			if (numoperador < vectorop.Length && vectorop[numoperador].Equals(0)){
				while (vectorop[numoperador].Equals(0)){
					numoperador++;	
				}
			}
			
			for (i = 0; i < filas; i++){
						
				// Comprobamos los casos de prueba que lo matan
				for (j = 0; j < columnas; j++){
					if (MatrixMut[i,j].Equals(1)){
					
						// Si lo mata sumamos el conjunto de mutantes que muere por dicho caso de prueba
						for (fila = 0; fila < filas; fila++){
							if (MatrixMut[fila, j].Equals(1)){
								sumaCP = sumaCP + MatrixMut[fila, j];
							}
						}
						
						numeradorcalidad = numeradorcalidad + sumaCP;
						sumaCP = 0;
					}
				}
				
				calidad = 1 - (numeradorcalidad / denominadorcalidad);
				sumaOp = sumaOp + calidad;
				calidad = 0.0;
				numeradorcalidad = 0.0;
				
				contadormutantes++;
			
				if (contadormutantes.Equals(vectorop[numoperador])){
					calidadOpE = sumaOp / vectoropTot[numoperador];
					calidadOp = sumaOp / vectorop[numoperador];
					sumaOp = 0.0;
				
					if (vectorop[numoperador].Equals(0)){
						calidadOp = 0;
						calidadOpE = 0;
					}
					
					// En calidad es la suma de todas las calidades de los mutantes divida 
					// entre el número de mutantes generados por dicho operador
						
					LineaToFile = "Qualify of " + nombop[numoperador] + " = " + calidadOp;
					linea = linea + "Qualify of " + nombop[numoperador] + " = " + calidadOpE + "\n";
					w.WriteLine(LineaToFile);
					LineaToFile = "";
					contadormutantes = 0;
					numoperador++;
					
					if (numoperador < vectorop.Length && vectorop[numoperador].Equals(0)){
						while (vectorop[numoperador].Equals(0)){
							numoperador++;	
						}
					}
				}
			}
			w.WriteLine(linea);
			
			LineaToFile = "\n--- EFFECTIVENESS --- \n";
			w.WriteLine(LineaToFile);
			LineaToFile = "";
			
			LineaToFile = Efectividad(columnas, filas, MatrixMut, vectorop, nombop);
			w.WriteLine(LineaToFile);
			
			w.Close();
		}
	}
}
