
using System;

namespace MutMix
{


	public partial class ErrorWindow : Gtk.Window
	{
		protected virtual void OnBtnOkErrorClicked (object sender, System.EventArgs e)
		{
			this.Destroy();
		}
		
		

		public ErrorWindow () : base(Gtk.WindowType.Toplevel)
		{
			this.Build ();
		}
	}
}
