using System;
using Gtk;


public partial class MainWindow : Gtk.Window
{
	
	public MainWindow () : base(Gtk.WindowType.Toplevel)
	{
		Build ();
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}
	
	protected virtual void OnBtnOkClicked (object sender, System.EventArgs e)
	{
		string FileChIn = FileChEst.Filename;
		string DirChOut = FileChResult.Filename;		
		string CombNum = SpComb.Text;
		string linea = "";
		
		if(LabAllCase.Visible){
			
			// CREACIÓN DEL FICHERO CON LOS CASOS DE PRUEBA
			
			// Comprobación de que el número de combinaciones es menor que el número de columnas menos 1
			System.IO.StreamReader sr = new System.IO.StreamReader(FileChIn);
						
			string delimitaF = "	";
			char [] delimita = delimitaF.ToCharArray();
		
			linea = sr.ReadLine();
			sr.Close();
			Array elto = linea.Split(delimita);
			string auxstring = elto.GetValue(1).ToString();
			int columnas = Convert.ToInt32(auxstring);
			
			int comb = Convert.ToInt32(CombNum);
			
			if(comb < columnas){
				CrearFicheroTestCases(FileChIn, DirChOut, comb);				
			}
			else{
				MutMix.ErrorWindow winerr = new MutMix.ErrorWindow ();
				winerr.Show();
			}
		}
		if (LabOneCase.Visible){
			
			if (EntryAllTest.Text == ""){
				MutMix.ErrorWindow winerr = new MutMix.ErrorWindow ();
				winerr.Show();
			}
			else{
				
				char delimitaF = '	';
		
				System.IO.StreamReader sr = new System.IO.StreamReader(FileChIn);
				linea = sr.ReadLine();
				sr.Close();
				
				Array eltoF = linea.Split(delimitaF);
				string auxstring = eltoF.GetValue(1).ToString();
				int columnas = Convert.ToInt32(auxstring);
				
				string aux = EntryAllTest.Text;
				char delimita = '_';
				
				Array a = aux.Split(delimita);				
				int numcasos = Convert.ToInt32(a.Length) - 1;
								
				if (numcasos < columnas){
					CrearFicheroTestCases(FileChIn, DirChOut, numcasos);
				}
				else{
					MutMix.ErrorWindow winerr = new MutMix.ErrorWindow ();
					winerr.Show();
				}
			}
		}
	}
	
	void CrearFicheroTestCases(string FileChIn, string DirChOut, int comb)
	{
		// Crear matriz con todos los estados de los mutantes
		
		System.IO.StreamReader rm = new System.IO.StreamReader(FileChIn);
		string linea = "";
		string delimitaF = "	";
		char [] delimita = delimitaF.ToCharArray();
		
		linea = rm.ReadLine();
		
		Array elto = linea.Split(delimita);
		string matrixtring = elto.GetValue(0).ToString();
		int filas = Convert.ToInt32(matrixtring);
		matrixtring = elto.GetValue(1).ToString();
		int columnas = Convert.ToInt32(matrixtring);
		
		int[,] MatrixMut = new int[filas, columnas];
		int i = 0;
		int j = 0;
			
		while((linea = rm.ReadLine()) != null){
			
			elto = linea.Split(delimita);
			for (j = 0; j < columnas; j++){
				matrixtring = elto.GetValue(j).ToString();
				MatrixMut[i,j] = Convert.ToInt32(matrixtring);
			}
			i++;
		}
		rm.Close();
		
		if (LabAllCase.Visible){
			// Almacenamos en un fichero todas las posibles combinaciones del número introducido y las columnas
			Combinaciones(DirChOut, comb, columnas);
			
			// Creamos cada uno de los ficheros "Combinations" 
			FicherosCombinations(DirChOut, comb, columnas, filas, MatrixMut);
		}
		if (LabOneCase.Visible){
			
			string casosprueba = EntryAllTest.Text;
			EntryAllTest.Text = "";
			FicherosCombinationsOne(DirChOut, comb, columnas, filas, MatrixMut, casosprueba);
		}
		
	}
	
	void Combinaciones(string DirChOut, int comb, int columnas)
	{
		int[] numeros = new int[columnas];
		int[] index = new int[comb];
		
		int aux;
		
		// Inicializamos los vectores
		
		for(aux = 0; aux < columnas; aux++)
			numeros[aux] = aux;
		
		for(aux = 0; aux < comb; aux++)
			index[aux] = numeros[aux];
		
		string NomFich = DirChOut + "/Combinaciones_" + comb.ToString()+ ".txt";
		System.IO.StreamWriter sw = new System.IO.StreamWriter(NomFich);
		string CombToFile = "";
		
		while(PosibleSiguiente(numeros, columnas, index, comb)){
			
			if (index[comb - 1] != columnas - 1){
				
				for (aux = 0; aux < comb; aux++){
					CombToFile = CombToFile + "_" + numeros[index[aux]].ToString();
				}
				
				sw.WriteLine(CombToFile);
				CombToFile = "";
				index[comb - 1] = numeros[index[comb - 1]] + 1;
			}
			if (index[comb - 1] == columnas - 1){
				
				for (aux = 0; aux < comb; aux++){
					CombToFile = CombToFile + "_" + numeros[index[aux]].ToString();
				}
				
				sw.WriteLine(CombToFile);
				CombToFile = "";
			}
		}
		
		sw.Close();
	}
	
	bool PosibleSiguiente(int[] numeros, int columnas, int[] index, int comb)
	{
		bool siguiente = true;
		int aux = comb - 1;
		
		if (index[aux] == columnas - 1){
			
			siguiente = false;
			
			while (aux > 0 && siguiente == false){
				if (numeros[index[aux - 1]] + 1 != numeros[index[aux]]){
					index[aux - 1]++;
					// Se reconstruyen los índices para volver a empezar
					while (aux < comb){
						index[aux] = index[aux - 1] + 1;
						aux++;
					}
					siguiente = true;
				}
				aux--;
			}
		}
		
		return siguiente;
	}
	
	void FicherosCombinations(string DirChOut, int comb, int columnas, int filas, int[,] MatrixMut)
	{
		string AllCombFich = DirChOut + "/Combinaciones_" + comb.ToString() + ".txt";
		System.IO.StreamReader r = new System.IO.StreamReader(AllCombFich);
		string linea = "";
		char delimita = '_';
		int columna = 1;
		int[] ColumSelec = new int[comb];
		
		while ((linea = r.ReadLine()) != null){
			
			string NomFich = DirChOut + "/Combinations_" + comb.ToString() + linea + ".txt";
			System.IO.StreamWriter w = new System.IO.StreamWriter(NomFich);
						
			Array elto = linea.Split(delimita);
			
			while (columna <= comb){
				string aux = elto.GetValue(columna).ToString();
				ColumSelec[columna - 1] = Convert.ToInt32(aux);
				columna++;
			}
			
			int fila = 0;
			string LineaToFile = "";
			
			for (fila = 0; fila < filas; fila++){
				for (columna = 0; columna < comb; columna++){
					LineaToFile = LineaToFile + MatrixMut[fila, ColumSelec[columna]].ToString() + "	";
				}
				w.WriteLine(LineaToFile);
				LineaToFile = "";
			}
		
			w.Close();
		
			// CREACIÓN DEL FICHERO CON LOS RESULTADOS
		
			CrearFicheroResult(DirChOut, linea, NomFich, comb, ColumSelec);
			columna = 1;
		}
		r.Close();
	}
	
	void CrearFicheroResult(string DirChOut, string nom, string NomFich, int CombNum, int[] ColumSelec)
	{
		System.IO.StreamReader r = new System.IO.StreamReader(NomFich);
		string NomResul = DirChOut + "/Results_" + CombNum.ToString() + nom + ".txt";
		System.IO.StreamWriter w = new System.IO.StreamWriter(NomResul);

		string linea = "";
		char delimita = '	';
		int suma = 0;
		
		while ((linea = r.ReadLine()) != null){
			
			Array elto = linea.Split(delimita);
			string aux = "";
			
			for (int i = 0; i < CombNum; i++){
				aux = elto.GetValue(i).ToString();
				suma += Convert.ToInt32(aux);
			}
			
			if (suma == 1){
			
				for (int i = 0; i < CombNum; i++){
					aux = elto.GetValue(i).ToString();
					
					if (aux.Contains("1")){
						string cp = "T" + ColumSelec[i];
						string nwlinea = linea.Replace("1", cp);
						linea = nwlinea;
					}
				}
			}
			w.WriteLine(linea);
			suma = 0;
		}
		
		r.Close();
		w.Close();
		// Contamos los casos de prueba que cumplen los requisitos
		ContarCasosPrueba(NomResul, ColumSelec, CombNum);
		Calidad(NomResul, ColumSelec, CombNum);
	}
	
	void ContarCasosPrueba(string NombResul, int[] ColumSelec, int CombNum)
	{
		System.IO.StreamReader r = new System.IO.StreamReader(NombResul);
		string bloque = "";
		
		bloque = r.ReadToEnd();
		r.Close();
		
		// Leemos la cantidad de los distintos casos de pruebas que cumplen las condiciones
		
		string bloqueBucle = bloque;
		int i;
		int[] sumas = new int[CombNum];
		int total = 0;
		
		for (i = 0; i < CombNum; i++){
			
			while (bloqueBucle.Contains("T" + ColumSelec[i])){
				total ++;
				bloqueBucle = bloqueBucle.Substring(bloqueBucle.IndexOf("T" + ColumSelec[i])+1);
			}
			sumas[i] = total;
			total = 0;
			bloqueBucle = bloque;
		}
		
		System.IO.StreamWriter w = new System.IO.StreamWriter(NombResul, true);
		for (i = 0; i < CombNum; i++){
			w.Write(sumas[i].ToString() + "	");
		}
		
		w.Close();
	}
	
	void Calidad(string NombResul, int[] ColumSelec, int CombNum)
	{
		System.IO.StreamReader r = new System.IO.StreamReader(NombResul);
		
		char delimita = '	';
		string linea = "";
		int[] calidad = new int[CombNum];
			
		while((linea = r.ReadLine()) != null){
			
			Array elto = linea.Split(delimita);
			for (int j = 0; j < CombNum; j++){
				string aux = elto.GetValue(j).ToString();
				
				if (aux.Equals("1"))
					calidad[j] = calidad[j] + 1;
			}
		}
		r.Close();
		
		System.IO.StreamWriter w = new System.IO.StreamWriter(NombResul, true);
		w.WriteLine("");
		
		for (int i = 0; i < CombNum; i++){
			if (calidad[i] == 0){
				w.Write("C	");
			}
			else{
				w.Write(calidad[i].ToString() + "	");
			}
		}
		
		w.Close();
	}
	
	// Para un solo caso de prueba
	
	void FicherosCombinationsOne(string DirChOut, int comb, int columnas, int filas, int[,] MatrixMut, string linea)
	{
		char delimita = '_';
		int columna = 1;
		int[] ColumSelec = new int[comb];
		
		string NomFich = DirChOut + "/Combinations_" + comb.ToString() + linea + ".txt";
		System.IO.StreamWriter w = new System.IO.StreamWriter(NomFich);
		Array elto = linea.Split(delimita);
			
		while (columna <= comb){
			string aux = elto.GetValue(columna).ToString();
			ColumSelec[columna - 1] = Convert.ToInt32(aux);
			columna++;
		}
		
		int fila = 0;
		string LineaToFile = "";
			
		for (fila = 0; fila < filas; fila++){
			for (columna = 0; columna < comb; columna++){
				LineaToFile = LineaToFile + MatrixMut[fila, ColumSelec[columna]].ToString() + "	";
			}
			
			w.WriteLine(LineaToFile);
			LineaToFile = "";
		}
		
		w.Close();
		
		// CREACIÓN DEL FICHERO CON LOS RESULTADOS
		
		CrearFicheroResult(DirChOut, linea, NomFich, comb, ColumSelec);
		columna = 1;
	}
	
	protected virtual void OnExecuteActionActivated (object sender, System.EventArgs e)
	{
		SpComb.Show();
		LabAllCase.Show();
		EntryCase.Hide();
		EntryAllTest.Hide();
		LabOneCase.Hide();
		BtnAdd.Hide();
	}
	
	protected virtual void OnPropertiesActionActivated (object sender, System.EventArgs e)
	{
		SpComb.Hide();
		LabAllCase.Hide();
		EntryCase.Show();
		EntryAllTest.Show();
		LabOneCase.Show();
		BtnAdd.Show();
	}
	
	protected virtual void OnQuitActionActivated (object sender, System.EventArgs e)
	{
		Application.Quit ();
	}
	
	protected virtual void OnFindActionActivated (object sender, System.EventArgs e)
	{
		MutMix.ViewFiles winView = new MutMix.ViewFiles ();
		winView.Show();
	}
	
	protected virtual void OnBtnAddClicked (object sender, System.EventArgs e)
	{
		string casoprueba = EntryAllTest.Text;
		
		if (EntryCase.Text != ""){
			casoprueba += "_" + EntryCase.Text;
		}
		
		EntryCase.Text = "";
		EntryAllTest.Text = casoprueba;
	}
	
	protected virtual void OnHelpActionActivated (object sender, System.EventArgs e)
	{
		MutMix.AboutMutMix winAb = new MutMix.AboutMutMix ();
		winAb.Show();
	}
	
	protected virtual void OnSortDescendingActionActivated (object sender, System.EventArgs e)
	{
		MutMix.RankingWindow winRk = new MutMix.RankingWindow ();
		winRk.Show();
	}
	
	
	
	
}
