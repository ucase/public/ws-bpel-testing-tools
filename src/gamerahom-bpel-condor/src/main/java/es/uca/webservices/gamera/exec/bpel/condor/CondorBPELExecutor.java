package es.uca.webservices.gamera.exec.bpel.condor;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uca.webservices.bpel.BPELProcessDefinition;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.annotations.Option;
import es.uca.webservices.gamera.api.annotations.OptionType;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.GAProgressMonitor;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.exec.bpel.AbstractBPELExecutor;
import es.uca.webservices.mutants.subcommands.CompareSubcommand;

/**
 * <p>
 * MuBPEL-based implementation of {@link GAExecutor} which uses a Condor cluster
 * as its execution environment for running the compositions. Analysis and
 * mutation are still performed locally, as usual. For the purposes of mutant
 * comparison, this class is just a wrapper for the mubpel_condor.py Python 3
 * script located at:
 * </p>
 * 
 * <pre>
 * https://neptuno.uca.es/svn/wsbpel-comp-repo/mubpel_condor.py
 * </pre>
 * 
 * <p>
 * The executor assumes that the script works on the user's machine and that it
 * is located at the path returned by {@link #getLaunchScript()}, which is by
 * default set to <code>$HOME/bin/mubpel_condor.py</code>. The script should be
 * readable and executable by the current user. Users are expected to have
 * Python 3 installed already. The remote machine is expected to be running some
 * flavor of UNIX and an SSH server.
 * </p>
 * 
 * <p>
 * If MuBPEL is not installed into the default <code>~/bin/mubpel.jar</code>
 * location, users will need to specify its location through the
 * {@link #setJarFile} method.
 * </p>
 * 
 * <p>
 * Users should also set up their environment so they can SSH into the Condor
 * cluster without a password. This is best achieved by either using an
 * unencrypted private/public key pair [1] (on a highly trusted computer), or by
 * reusing a previously existing SSH connection [2] (on a less trusted
 * computer).
 * </p>
 * 
 * <p>
 * This executor does not provide any additional reliability features on top of
 * those already provided by the script. The script already sets up the Condor
 * DAGman job to retry failed executions and to time out if the job is not
 * completed within a certain time. Please note that using a Condor cluster for
 * a long time may reduce the Condor user's priority and leave them with less
 * resources for some time.
 * </p>
 * 
 * <p>
 * Users with the default setup will only need to invoke
 * {@link #setRemoteHost(String)}, {@link #setRemoteUser(String)},
 * {@link #setRemoteDirectory(String)} and {@link #setEmail(String)} before
 * using this executor. The other options are just there for the sake of
 * completeness.
 * </p>
 * 
 * <ol>
 * <li>http://www.ece.uci.edu/~chou/ssh-key.html</li>
 * <li>http://www.revsys.com/writings/quicktips/ssh-faster-connections.html</li>
 * </ol>
 * 
 * <p>
 * Note: this class is <emph>not</emph> thread safe. In particular, the
 * {@link #compare(GAProgressMonitor, GAIndividual...)} method should never be
 * called from more than one thread at the same time.
 * </p>
 */
public class CondorBPELExecutor extends AbstractBPELExecutor {
	private static final Logger LOGGER = LoggerFactory.getLogger(CondorBPELExecutor.class);

	// Any integer but 0 is accepted
	private static final String NONZERO_INTEGER_REGEX = "[1-9][0-9]*";

	// From /etc/adduser.conf in Ubuntu
	private static final String USERNAME_REGEX = "^[a-z][-a-z0-9]*$";

	// From http://stackoverflow.com/questions/1418423/the-hostname-regex
	private static final String HOSTNAME_REGEX = "^(?=.{1,255}$)[0-9A-Za-z](?:(?:[0-9A-Za-z]|-){0,61}[0-9A-Za-z])?(?:\\.[0-9A-Za-z](?:(?:[0-9A-Za-z]|-){0,61}[0-9A-Za-z])?)*\\.?$";

	private static final String DEFAULT_SCRIPT_BASENAME = "mubpel_condor.py";

	private File fLaunchScript, fMuBPELJar;
	private String remoteHost, remoteUser, remoteDirectory, email;
	private Integer jobSize, testTimeoutSeconds, jobTimeoutSeconds, retries;

	public CondorBPELExecutor() {
		final String userHome = System.getProperty("user.home");
		if (userHome != null) {
			final File binFolder = new File(userHome, "bin");
			fLaunchScript = new File(binFolder, DEFAULT_SCRIPT_BASENAME);
		}
	}

	// All options need the get/set pair with their natural type (as a Java API)
	// and String types (for the Option annotations).

	public String getLaunchScript() {
		return fLaunchScript != null ? fLaunchScript.getPath() : null;
	}

	public File getLaunchScriptFile() {
		return fLaunchScript;
	}

	public void setLaunchScript(File fLaunchScript) {
		this.fLaunchScript = fLaunchScript;
	}

	@Option(type = OptionType.PATH_LOAD, fileExtensions = {"py"})
	public void setLaunchScript(String pathToLaunchScript) {
		this.fLaunchScript = pathToLaunchScript != null ? new File(pathToLaunchScript) : null;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	@Option(regex = HOSTNAME_REGEX)
	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public String getRemoteUser() {
		return remoteUser;
	}

	@Option(regex = USERNAME_REGEX)
	public void setRemoteUser(String remoteUser) {
		this.remoteUser = remoteUser;
	}

	public String getRemoteDirectory() {
		return remoteDirectory;
	}

	@Option
	public void setRemoteDirectory(String remoteDirectory) {
		this.remoteDirectory = remoteDirectory;
	}

	public String getEmail() {
		return email;
	}

	@Option // no regexp: email addresses are just too complicated
	public void setEmail(String email) {
		this.email = email;
	}

	public String getMuBPELJar() {
		return fMuBPELJar != null ? fMuBPELJar.getPath() : null;
	}

	public File getMuBPELJarFile() {
		return fMuBPELJar;
	}

	@Option(type = OptionType.PATH_LOAD, fileExtensions = { ".jar" })
	public void setMuBPELJar(String jarFile) {
		this.fMuBPELJar = jarFile != null ? new File(jarFile) : null;
	}

	public void setMuBPELJar(File jarFile) {
		this.fMuBPELJar = jarFile;
	}

	public String getJobSize() {
		return jobSize != null ? jobSize + "" : null;
	}

	public Integer getJobSizeInt() {
		return jobSize;
	}

	public void setJobSize(Integer jobSize) {
		this.jobSize = jobSize;
	}

	@Option(regex = NONZERO_INTEGER_REGEX)
	public void setJobSize(String jobSize) {
		this.jobSize = jobSize != null ? Integer.valueOf(jobSize) : null;
	}

	public String getTestTimeoutSeconds() {
		return testTimeoutSeconds != null ? testTimeoutSeconds + "" : null;
	}

	public Integer getTestTimeoutSecondsInt() {
		return testTimeoutSeconds;
	}

	public void setTestTimeoutSeconds(Integer testTimeoutSeconds) {
		this.testTimeoutSeconds = testTimeoutSeconds;
	}

	@Option
	public void setTestTimeoutSeconds(String testTimeoutSeconds) {
		this.testTimeoutSeconds = testTimeoutSeconds != null ? Integer.valueOf(testTimeoutSeconds) : null;
	}

	public String getJobTimeoutSeconds() {
		return jobTimeoutSeconds != null ? jobTimeoutSeconds + "" : null;
	}

	public Integer getJobTimeoutSecondsInt() {
		return jobTimeoutSeconds;
	}

	public void setJobTimeoutSeconds(Integer jobTimeoutSeconds) {
		this.jobTimeoutSeconds = jobTimeoutSeconds;
	}

	@Option(regex = NONZERO_INTEGER_REGEX)
	public void setJobTimeoutSeconds(String jobTimeoutSeconds) {
		this.jobTimeoutSeconds = jobTimeoutSeconds != null ? Integer.valueOf(jobTimeoutSeconds) : null;
	}

	public String getRetries() {
		return retries != null ? retries + "" : null;
	}

	public Integer getRetriesInt() {
		return retries;
	}

	public void setRetries(Integer retries) {
		this.retries = retries;
	}

	@Option(regex = NONZERO_INTEGER_REGEX)
	public void setRetries(String retries) {
		this.retries = retries != null ? Integer.valueOf(retries) : null;
	}
	
	@Override
	public void prepare() throws PreparationException {
		// Nothing to do, as we don't start up an ActiveBPEL instance on the
		// local machine.
	}

	@Override
	public void cleanup() throws PreparationException {
		// Nothing to do, as we don't start up an ActiveBPEL instance on the
		// local machine.
	}

	@Override
	public ComparisonResults[] compare(GAProgressMonitor monitor, GAIndividual... individuals) throws ComparisonException {
		if (individuals == null || individuals.length == 0) {
			return new ComparisonResults[0];
		}

		try {
			final List<String> args = buildCompareCommand(individuals);

			final ProcessBuilder pb = new ProcessBuilder(args);
			pb.redirectErrorStream(true);
			final Process p = pb.start();
			p.getOutputStream().close();

			final Map<GAIndividual, ComparisonResults> resultsByIndividual = readResultsFromCondor(p);

			// Make sure we have results for all individuals
			final ComparisonResults[] cmpResults = new ComparisonResults[individuals.length];
			for (int i = 0; i < individuals.length; i++) {
				final GAIndividual ind = individuals[i];
				final ComparisonResults result = resultsByIndividual.get(ind);
				if (result == null) {
					throw new ComparisonException("Could not find the result for individual " + ind);
				}

				cmpResults[i] = result;
			}

			// Make sure we do not have more results than expected
			if (resultsByIndividual.size() != individuals.length) {
				throw new ComparisonException(
						String.format("%d individuals were requested and only " +
								"%d results were produced. Please enable debugging " +
								"and try again to see the bad lines.",
								individuals.length, resultsByIndividual.size()));
			}

			return cmpResults;
		} catch (ComparisonException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new ComparisonException(ex);
		}
	}

	private Map<GAIndividual, ComparisonResults> readResultsFromCondor(final Process p) throws Exception
	{
		// Read the collected results: we're not sure in which order we will get them, though, so we
		// might as well not rely on it being the same as the order of the individuals. We shouldn't
		// rely on all lines following the MuBPEL format, in case some logging message seeps through
		// the SSH connection.
		final BufferedReader bR = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
		final Map<GAIndividual, ComparisonResults> resultsByIndividual = new IdentityHashMap<GAIndividual, ComparisonResults>();
		try {
			lines:
			for (String line = bR.readLine(); line != null; line = bR.readLine()) {
				LOGGER.debug("Read line '{}'", line);

				final String[] parts = line.split("\\s+");
				if (parts.length < 4) {
					// A valid line should have at least 'filename cmp T time' (4 fields)
					LOGGER.debug("Bad line (less than 4 parts): '{}'", line);
					continue;
				}

				int tSepPos = 0;
				while (tSepPos < parts.length && !"T".equals(parts[tSepPos])) ++tSepPos;
				if (tSepPos == parts.length) {
					LOGGER.debug("Bad line (could not find 'T'): '{}'", line);
					continue;
				}
				final int nTestCases = tSepPos - 1;
				if (parts.length != 1 + nTestCases + 1 + nTestCases) {
					LOGGER.debug("Bad line (not 'bpel tests{n} T times{n}'): '{}'", line);
					continue;
				}
				final String bpel = parts[0];

				final int[] cmps = new int[nTestCases];
				for (int i = 0; i < nTestCases; i++) {
					try {
						cmps[i] = Integer.valueOf(parts[1 + i]);
					} catch (NumberFormatException ex) {
						LOGGER.debug("Bad line (one of the comparisons is not an int): '{}'", line);
						continue lines;
					}
				}

				final long[] times = new long[nTestCases];
				for (int i = 0; i < nTestCases; i++) {
					try {
						times[i] = Long.valueOf(parts[tSepPos + 1 + i]);
					} catch (NumberFormatException ex) {
						LOGGER.debug("Bad line (one of the times is not a long): '{}'", line);
						continue lines;
					}
				}

				final ComparisonResults results = convertLineIntoResults(bpel, cmps, times);
				resultsByIndividual.put(results.getIndividual(), results);
			}
		} finally {
			bR.close();
		}

		p.waitFor();

		return resultsByIndividual;
	}

	private List<String> buildCompareCommand(GAIndividual... individuals) throws Exception {
		final List<String> args = new ArrayList<String>();
		args.add(fLaunchScript.getPath());

		// Make the script wait and collect the results
		args.add("--wait");

		// Mandatory options
		args.add("--ruser"); args.add(remoteUser);
		args.add("--rhost"); args.add(remoteHost);
		args.add("--rdir");  args.add(remoteDirectory);
		args.add("--email"); args.add(email);

		// Options
		if (fMuBPELJar != null) {
			args.add("--jar"); args.add(fMuBPELJar.getPath());
		}
		if (jobSize != null) {
			args.add("--jobsize"); args.add(jobSize.toString());
		}
		if (jobTimeoutSeconds != null) {
			args.add("--job-timeout"); args.add(jobTimeoutSeconds.toString());
		}
		if (retries != null) {
			args.add("--retries"); args.add(retries.toString());
		}
		if (testTimeoutSeconds != null) {
			args.add("--test-timeout"); args.add(testTimeoutSeconds.toString());
		}

		// bpts, bpel, deps, data file, mutants
		args.add(getTestSuiteFile().getPath());
		args.add(getOriginalProgramFile().getPath());
		for (File dep : new BPELProcessDefinition(getOriginalProgramFile()).getDependencies()) {
			args.add(dep.getPath());
		}
		if (getDataFileRaw() != null) {
			args.add(getDataFileRaw().getPath());
		}
		args.add("--mutants");
		for (GAIndividual ind : individuals) {
			args.add(generate(ind).getPath());
		}
		return args;
	}

	@Override
	public List<String> getTestCaseNames() throws AnalysisException {
		final CompareSubcommand cmd = new CompareSubcommand();
		try {
			return cmd.getTestCaseNames(getTestSuite());
		} catch (Exception e) {
			throw new AnalysisException(e);
		}
	}

	@Override
	public void validate() throws InvalidConfigurationException {
		super.validate();

		// Start with the quick and cheap checks first
		if (email == null) {
			throw new InvalidConfigurationException("The notification email must be set"); 
		}
		if (fLaunchScript == null) {
			throw new InvalidConfigurationException("The path to the "
					+ DEFAULT_SCRIPT_BASENAME
					+ " launch script must be specified");
		}
		if (!fLaunchScript.canRead() || !fLaunchScript.canExecute()) {
			throw new InvalidConfigurationException(
					"Cannot read or execute the launch script "
							+ fLaunchScript.getPath() + ": bad permissions");
		}
		if (fMuBPELJar != null && !fMuBPELJar.canRead()) {
			throw new InvalidConfigurationException("The MuBPEL .jar at '"
					+ fMuBPELJar.getPath() + "' cannot be read");
		}
		if (jobSize != null && jobSize <= 0) {
			throw new InvalidConfigurationException("Job size must be > 0");
		}
		if (jobTimeoutSeconds != null && jobTimeoutSeconds <= 0) {
			throw new InvalidConfigurationException("Job timeout must be > 0");
		}
		if (remoteUser == null) {
			throw new InvalidConfigurationException("The remote user must be specified.");
		}
		if (remoteHost == null) { 
			throw new InvalidConfigurationException("The remote host must be specified.");
		}
		if (remoteDirectory == null) {
			throw new InvalidConfigurationException("The remote directory must be specified.");
		}
		if (retries != null && retries <= 0) {
			throw new InvalidConfigurationException("Retries must be > 0");
		}
		if (testTimeoutSeconds != null && testTimeoutSeconds <= 0) {
			throw new InvalidConfigurationException("Test timeout must be > 0");
		}

		// Expensive checks go last
		validateScriptWithNoArgsProducesUsage();
		validateSSHConnectivity();
	}

	private void validateScriptWithNoArgsProducesUsage() throws InvalidConfigurationException {
		final ProcessBuilder pb = new ProcessBuilder(fLaunchScript.getPath());
		pb.redirectErrorStream(true);
		try {
			final Process p = pb.start();
			p.getOutputStream().close();

			boolean bHasUsage = false;
			BufferedReader bR = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
			for (String line = bR.readLine(); line != null; line = bR.readLine()) {
				bHasUsage = bHasUsage || line.contains("MUTANTS");
			}
			p.waitFor();

			if (!bHasUsage) {
				throw new InvalidConfigurationException(
						"Running the launch script "
								+ fLaunchScript.getPath()
								+ " without arguments did not produce the usage"
								+ " message: something must be wrong with your"
								+ " setup. Do you have Python 3 installed?");
			}
		} catch (Exception e) {
			throw new InvalidConfigurationException("There was a problem while doing the dry run of the launch script",	e);
		}
	}

	private void validateSSHConnectivity() throws InvalidConfigurationException {
		// The OpenSSH -oBatchMode=yes option prevents SSH for asking for a password.
		// This prevents the executor from hanging while SSH waits for the user to
		// introduce a password.
		final ProcessBuilder pb = new ProcessBuilder("ssh", "-oBatchMode=yes",
				remoteUser + "@" + remoteHost, "mkdir", "-p", remoteDirectory);
		try {
			final Process p = pb.start();
			final int statusCode = p.waitFor();
			if (statusCode != 0) {
				throw new InvalidConfigurationException(
						"Trying out SSH connectivity with " + pb.command()
								+ " produced the non-zero status code "
								+ statusCode);
			}
		} catch (Exception e) {
			throw new InvalidConfigurationException("There was a problem while testing SSH connectivity to the remote host", e);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((fLaunchScript == null) ? 0 : fLaunchScript.hashCode());
		result = prime * result
				+ ((fMuBPELJar == null) ? 0 : fMuBPELJar.hashCode());
		result = prime * result + ((jobSize == null) ? 0 : jobSize.hashCode());
		result = prime
				* result
				+ ((jobTimeoutSeconds == null) ? 0 : jobTimeoutSeconds
						.hashCode());
		result = prime * result
				+ ((remoteDirectory == null) ? 0 : remoteDirectory.hashCode());
		result = prime * result
				+ ((remoteHost == null) ? 0 : remoteHost.hashCode());
		result = prime * result
				+ ((remoteUser == null) ? 0 : remoteUser.hashCode());
		result = prime * result + ((retries == null) ? 0 : retries.hashCode());
		result = prime
				* result
				+ ((testTimeoutSeconds == null) ? 0 : testTimeoutSeconds
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CondorBPELExecutor other = (CondorBPELExecutor) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (fLaunchScript == null) {
			if (other.fLaunchScript != null)
				return false;
		} else if (!fLaunchScript.equals(other.fLaunchScript))
			return false;
		if (fMuBPELJar == null) {
			if (other.fMuBPELJar != null)
				return false;
		} else if (!fMuBPELJar.equals(other.fMuBPELJar))
			return false;
		if (jobSize == null) {
			if (other.jobSize != null)
				return false;
		} else if (!jobSize.equals(other.jobSize))
			return false;
		if (jobTimeoutSeconds == null) {
			if (other.jobTimeoutSeconds != null)
				return false;
		} else if (!jobTimeoutSeconds.equals(other.jobTimeoutSeconds))
			return false;
		if (remoteDirectory == null) {
			if (other.remoteDirectory != null)
				return false;
		} else if (!remoteDirectory.equals(other.remoteDirectory))
			return false;
		if (remoteHost == null) {
			if (other.remoteHost != null)
				return false;
		} else if (!remoteHost.equals(other.remoteHost))
			return false;
		if (remoteUser == null) {
			if (other.remoteUser != null)
				return false;
		} else if (!remoteUser.equals(other.remoteUser))
			return false;
		if (retries == null) {
			if (other.retries != null)
				return false;
		} else if (!retries.equals(other.retries))
			return false;
		if (testTimeoutSeconds == null) {
			if (other.testTimeoutSeconds != null)
				return false;
		} else if (!testTimeoutSeconds.equals(other.testTimeoutSeconds))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CondorBPELExecutor [fLaunchScript=" + fLaunchScript
				+ ", fMuBPELJar=" + fMuBPELJar + ", remoteHost=" + remoteHost
				+ ", remoteUser=" + remoteUser + ", remoteDirectory="
				+ remoteDirectory + ", email=" + email + ", jobSize=" + jobSize
				+ ", testTimeoutSeconds=" + testTimeoutSeconds
				+ ", jobTimeoutSeconds=" + jobTimeoutSeconds + ", retries="
				+ retries + ", getTestSuite()=" + getTestSuite()
				+ ", getOriginalProgram()=" + getOriginalProgram()
				+ ", getDataFile()=" + getDataFileRaw() + "]";
	}
}
