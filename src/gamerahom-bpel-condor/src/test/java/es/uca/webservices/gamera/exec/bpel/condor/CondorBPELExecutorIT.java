package es.uca.webservices.gamera.exec.bpel.condor;

import java.util.List;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.ValidIndividualsList;

/**
 * Simple integration test for {@link CondorBPELExecutor}. This needs to be run manually,
 * as it requires a complex setup and the appropriate authentication credentials on the
 * Condor cluster. Please see the above class for details.
 *
 * @author Antonio García-Domínguez
 */
public final class CondorBPELExecutorIT {
	private static final String RES_PREFIX = "src/test/resources/loanDoc/";

	private CondorBPELExecutorIT() {}

	public static void main(String[] args) throws Exception {
		CondorBPELExecutor exec = new CondorBPELExecutor();
		exec.setTestSuite(RES_PREFIX + "LoanApprovalProcess.bpts");
		exec.setOriginalProgram(RES_PREFIX + "LoanApprovalProcess.bpel");

		// Please set these properties through your own means!
		exec.setRemoteUser(System.getProperty("condor.user"));
		exec.setRemoteHost(System.getProperty("condor.host"));
		exec.setRemoteDirectory(System.getProperty("condor.dir"));
		exec.setEmail(System.getProperty("condor.email"));

		System.out.println("Validating...");
		exec.validate();

		System.out.println("Analyzing...");
		final AnalysisResults analysis = exec.analyze(null);
		final List<GAIndividual> inds = new ValidIndividualsList(analysis, 1).all();

		System.out.println("Comparing...");
		final ComparisonResults[] results = exec.compare(null, inds.toArray(new GAIndividual[inds.size()]));

		for (ComparisonResults r : results) {
			System.out.println("Obtained: " + r);
		}
	}
}
