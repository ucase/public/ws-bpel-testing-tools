package es.uca.muepl.gamerahom.exec;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.uca.webservices.gamera.api.AnalysisResults;
import es.uca.webservices.gamera.api.ComparisonResult;
import es.uca.webservices.gamera.api.ComparisonResults;
import es.uca.webservices.gamera.api.GAIndividual;
import es.uca.webservices.gamera.api.conf.InvalidConfigurationException;
import es.uca.webservices.gamera.api.exec.AnalysisException;
import es.uca.webservices.gamera.api.exec.ComparisonException;
import es.uca.webservices.gamera.api.exec.GAExecutor;
import es.uca.webservices.gamera.api.exec.GAProgressMonitor;
import es.uca.webservices.gamera.api.exec.GenerationException;
import es.uca.webservices.gamera.api.exec.PreparationException;
import es.uca.webservices.gamera.api.util.Pair;
/**
 * <p>
 * Executor for MuEPL
 *
 * @author Lorena Gutiérrez-Madroñal
 */
public class EPLExecutor implements GAExecutor {

	// From mutant basename (as it may have been run in a remote machine) to GA
	// individual
	private final Map<String, GAIndividual> mapBasenameToIndividual = new HashMap<String, GAIndividual>();
	// From GA individual to locally generated file
	private final Map<GAIndividual, File> mapIndividualToFile = new HashMap<GAIndividual, File>();

	private String originalProgram = null;
	private String originalDirectory = null;
	private String testDirectory = null;
	private String executionOrder = null;

	// Number of mutants per operator and file: rows = operator; columns = file
	private Map<BigInteger, String> mapOpCode = new HashMap<BigInteger, String>();
	private int numberOfQueries;
	private String coverage = "no";

	private String nameEPLDirectory = null;
	//private String outputPath = null;
	private String testSuiteFile = null;

	@Override
	public void prepare() throws PreparationException {

	}

	@Override
	public void cleanup() throws PreparationException {

	}

	@Override
	public void validate() throws InvalidConfigurationException {

		if (getOriginalProgramPath() == null) { // The path to internal results
			throw new InvalidConfigurationException("The path to the original epl queries must be specified");
		}

		if (getNumberOfQueries() == 0) { // Number of EPL queries to be analyzed
			throw new InvalidConfigurationException("The number of the original epl queries must be specified");
		}

		if (getOriginalProgramDirectory() == null) { // The original program
														// path
			throw new InvalidConfigurationException("The path to the original program must be specified");
		}

		if (getDirectoryName() == null) { // The name of the folder where the
											// queries will be read
			throw new InvalidConfigurationException(
					"The name of the folder where epl queries will be read must be specified");
		}

		if (getTestDirectory() == null) { // The directory where the results
											// will be saved
			throw new InvalidConfigurationException("The path to the test directory must be specified");
		}

		if (getExecutionOrder() == null) {
			throw new InvalidConfigurationException("The execution order must be specified");
		}
		
		/*if (getOutputPath() == null) {
			throw new InvalidConfigurationException("The output events file must be specified");
		}*/
		
		if (getTestSuiteFile() == null) {
			throw new InvalidConfigurationException("The test suite file must be specified");
		}
		
		if (getCoverage() == null) {
			throw new InvalidConfigurationException("The coverage option must be specified (true or false).");
		}

		File testDirectoryFile = new File(getTestDirectory());
		if (!testDirectoryFile.exists())
			throw new InvalidConfigurationException("The test directory " + getTestDirectory() + " does not exist");
		else
			setTestDirectory(normalizeDirectory(getTestDirectory()));

		
		File origP = new File (getOriginalProgramPath());
		if (!origP.canRead())
			throw new InvalidConfigurationException(
					"The file with the original program at " + getOriginalProgramPath() + " is not readable");
	}

	@Override
	public AnalysisResults analyze(GAProgressMonitor monitor) throws AnalysisException {
		try {

			// Create the process to execute "analyze"
			System.out.println("The command analyze starts...");
			
			List<String> args = new ArrayList<String>();
			args.clear();
			
			String pathToJar = System.getenv("MUEPL_JAR");
			
			if (pathToJar == null) {
				pathToJar = "muepl.jar";
				System.err.println("MUEPL_JAR not set:using default location" + pathToJar);
			}
			
			args.add("java");
			args.add("-jar");
			args.add(pathToJar);
			args.add("analyze");
			args.add(getOriginalProgramPath() + "/queries.epl");
			args.add(String.valueOf(getNumberOfQueries()));
			launchProcess(args);

			// Read the results
			String AnalysisFile = originalProgram + "/Analyze.txt";
			FileReader fr = new FileReader(AnalysisFile);
			BufferedReader br = new BufferedReader(fr);
			ArrayList<String> lines = new ArrayList<String>();
			String delimiter = " ";

			String[] opNames = null;
			BigInteger[] opCounts = null;
			BigInteger[] attrRanges = null;
			String line = null;
			String[] subLines = null;

			lines.clear();
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
			br.close();
			fr.close();

			// Assign the number of operators to initialize arrays
			int nOperators = lines.size();

			opNames = new String[nOperators];
			opCounts = new BigInteger[nOperators];
			attrRanges = new BigInteger[nOperators];

			for (int iOperator = 0; iOperator < lines.size(); iOperator++) {
				// Take each line and split with " "
				subLines = lines.get(iOperator).split(delimiter);
				opNames[iOperator] = subLines[0];
				opCounts[iOperator] = new BigInteger(subLines[1]);
				attrRanges[iOperator] = new BigInteger(subLines[2]);

				// Create the list of code-operator
				mapOpCode.put(new BigInteger(Integer.toString(iOperator + 1)), subLines[0]);
			}

			return new AnalysisResults(opNames, opCounts, attrRanges);
		} catch (Exception e) {
			throw new AnalysisException(e);
		}
	}

	private File generate(GAIndividual individual) throws GenerationException {
		try {
			// Check if the mutant was previously generated
			if (!mapIndividualToFile.containsKey(individual)) { 

		    	System.out.println("Generating the mutant: " + getMapOpCode().get(individual.getThOperator(1)).toString() + "_"
						+ individual.getThLocation(1).toString() + "_" + individual.getThAttribute(1).toString());
		    	
				// Create the process to execute "apply"
				List<String> args = new ArrayList<String>();
				args.clear();
				
				String pathToJar = System.getenv("MUEPL_JAR");
				
				if (pathToJar == null) {
					pathToJar = "muepl.jar";
					System.err.println("MUEPL_JAR not set:using default location" + pathToJar);
				}
				
				args.add("java");
				args.add("-jar");
				args.add(pathToJar);
				args.add("apply");
				args.add(originalDirectory);
				args.add(getOriginalProgramPath() + "/queries.epl");
				args.add(nameEPLDirectory);
				args.add(getMapOpCode().get(individual.getThOperator(1)).toString());
				args.add(individual.getThLocation(1).toString());
				args.add(individual.getThAttribute(1).toString());
				args.add(coverage);

				launchProcess(args);

				String pathToMutantProgram = originalDirectory + getMapOpCode().get(individual.getThOperator(1)).toString() + "_"
						+ individual.getThLocation(1).toString() + "_" + individual.getThAttribute(1).toString();

				String programName = pathToMutantProgram.substring(pathToMutantProgram.lastIndexOf("/") + 1, pathToMutantProgram.length());
				File MutantProgram = new File(pathToMutantProgram);

				// Save the mutant
				mapIndividualToFile.put(individual, MutantProgram);
				mapBasenameToIndividual.put(programName, individual);

				return MutantProgram;
			} else {
				final File cachedFile = mapIndividualToFile.get(individual);
				return cachedFile;
			}			

		} catch (Exception e) {
			throw new GenerationException(e);
		}
	}

	@Override
	public ComparisonResults[] compare(GAProgressMonitor monitor,
			GAIndividual... individuals) throws ComparisonException {
		try {

			final List<ComparisonResults> lComparisons = new ArrayList<ComparisonResults>();
			List<String> args = new ArrayList<String>();
			String nameBranch = null;

			RemovingFiles();

			System.out.println("The command apply starts...");
			for (GAIndividual individual : individuals) { // To generate the mutants
				nameBranch = getMutantFile(individual).toString();
				args.add(nameBranch);
			}

			// To read each path in the testsuite file (a test case)
			String testcaseline = "";
			FileReader tsfile = new FileReader(testSuiteFile);
			BufferedReader tsb = new BufferedReader(tsfile);

			String CompareFile = testDirectory + "Compare.txt"; 
			// The file where the compare results will be saved. A line for each test case.
			
			while ((testcaseline = tsb.readLine()) != null) { 
				// Each test case must be run against all the mutants

				System.out.println("Running the test case " + testcaseline);
				// Run all the AG generated mutants against the original
				args.clear();

				String pathToJar = System.getenv("MUEPL_JAR");
				
				if (pathToJar == null) {
					pathToJar = "muepl.jar";
					System.err.println("MUEPL_JAR not set:using default location" + pathToJar);
				}
				
				args.add("java");
				args.add("-jar");
				args.add(pathToJar);
				args.add("runallsequence");
				args.add(originalDirectory);
				args.add(testDirectory + "Outputs");
				args.add(executionOrder);
				args.add(testcaseline);
				args.add(getMutantPathsFile());
				launchProcess(args);

				System.out.println("Comparing the results of the test case " + testcaseline);
				// Create the process to execute "compare"
				// For each test case must be a result file
				args.clear();
				args.add("java");
				args.add("-jar");
				args.add(pathToJar);
				args.add("compare");
				args.add(originalDirectory);
				args.add(getMutantPathsFile());
				args.add(testDirectory + "Outputs");
				launchProcess(args);
				
				CleaningTemporalFiles();
			}

			tsb.close();
			tsfile.close();

			// Read the results
			ArrayList<String> lines = new ArrayList<String>();
			String delimiter = " ";

			List<Pair<String, Pair<int[], Long>>> bkgResults = new ArrayList<Pair<String, Pair<int[], Long>>>();

			String[] subLines = null;
			String line = null;
			int nColumns = 0;
			
			// The file InternalCompare.txt will contains all the outputs in a single line. 
			// The Compare.txt lines (one per test case) are dump in a single line in InternalCompare.txt
			
			String InternalCompareFile = testDirectory + "InternalCompare.txt"; 
			DumpContent(CompareFile, InternalCompareFile);
			
			// Now the nColumns will be set
			try (BufferedReader br = new BufferedReader(new FileReader(InternalCompareFile))) {
				// It is necessary to know the size of the test suite
				line = br.readLine(); // One line is enough

				String[] parts = line.split(" ");
				int part = 1;
				while (!parts[part].equals("T")) {
					nColumns++;
					part++;
				}
				br.close();
				
			} catch (Exception e) {
				throw new ComparisonException("Not possible to read the file " + CompareFile, e);
			}

			try (BufferedReader br = new BufferedReader(new FileReader(InternalCompareFile))) {
				line = null;
				subLines = null;

				while ((line = br.readLine()) != null) {
					lines.add(line);
				}
				
				br.close();
			} catch (Exception e) {
				throw new ComparisonException("Not possible to read the file " + CompareFile, e);
			}

			// The number of lines should match the number of individuals (one line per mutant)
			if (lines.size() == individuals.length) {

				for (int iResult = 0; iResult < lines.size(); iResult++) {
					String path = null;
					int[] comparisons = new int[nColumns];
					long nanos = 0;

					// Take each line and split with " "
					subLines = lines.get(iResult).split(delimiter);
					path = subLines[0];
					int iTest = 1;

					while (iTest < subLines.length
							&& !subLines[iTest].equals("T")) {
						comparisons[iTest - 1] = Integer
								.parseInt(subLines[iTest]);
						iTest++;
					}

					int iTestAux = iTest;
					if (iTestAux < nColumns) {
						comparisons[iTestAux] = 0;
						iTestAux++;
					}

					iTest++; // Saltar la T
					nanos = Long.parseLong(subLines[iTest]);

					Pair<int[], Long> resultsTimes = new Pair<int[], Long>(comparisons, nanos);
					bkgResults.add(new Pair<String, Pair<int[], Long>>(path, resultsTimes));
				}
				convertMatrixIntoResults(bkgResults, lComparisons);
				
			} else {
				throw new ComparisonException("Incomplete results");
			}

			lines.clear();

			return lComparisons.toArray(new ComparisonResults[lComparisons.size()]);

		} catch (Exception ex) {
			throw new ComparisonException(ex);
		}
	}
	
	private void DumpContent(String CompareFile, String internalCompareFile) throws IOException {
		
		BufferedReader br = new BufferedReader(new FileReader(CompareFile));
		BufferedWriter bw = new BufferedWriter(new FileWriter(internalCompareFile));
		
		String line = null;
		long time = 0;
		String comparisionResults = "";
		String mutantName = null;

		while ((line = br.readLine()) != null) {
			if (mutantName == null) {
				mutantName = line.substring(0, line.indexOf(" ") + 1);
			}
			time += Long.parseLong(line.substring(line.indexOf("T ") + 2));					
			comparisionResults += line.substring(line.indexOf(" ") + 1, line.indexOf("T "));
		}
			
		bw.write(mutantName + comparisionResults + "T " + time);
		br.close();
		bw.close();
	}

	private void CleaningTemporalFiles() {
		File f = new File(testDirectory + "Outputs");
		f.delete();
		f = new File(testDirectory + "ExecutionTimes.txt");
		f.delete();	
		f = new File(testDirectory + "ProgramPaths.txt");
		f.delete();	
		f = new File("/tmp/queries.epl");
		f.delete();
	}

	private void RemovingFiles() {
		System.out.println("Preparing the next generation...");

		// The directory with the mutant outputs will be deleted
		File outputsDir = new File("/tmp/outputs");
		if (outputsDir.exists()) {
			DeleteFolder(outputsDir);
			outputsDir.delete();
		}
				
		//Cleaning the results
		File Directory = new File(testDirectory);
		CleanResults(Directory);
		
		//Cleaning the generated mutants
		Directory = new File(originalDirectory).getAbsoluteFile().getParentFile();
		DeleteMutants(Directory);
	}
	
	private void DeleteMutants(File directory) {
		
		File[] paths;
		final String directoryname = originalDirectory.substring(originalDirectory.lastIndexOf("/") + 1);
		
		// create new filename filter
        FilenameFilter fileNameFilter = new FilenameFilter() {
  
           @Override
           public boolean accept(File directory, String name) {
        	   return name.startsWith(directoryname);
           }
        };
           
        // returns pathnames for files and directory
     // returns pathnames for files and directory
        paths = directory.listFiles(fileNameFilter);
		Arrays.sort(paths);
        
        // for each pathname in pathname array
        for(File path:paths) {
			if (!path.getAbsolutePath().equals(originalDirectory)) {
				DeleteFolder(path);
			}
			path.delete();
		}		
	}

	private void CleanResults(File Directory) {
		
		// To remove the results files
		String[]entries = Directory.list();
		
		for(String s: entries){
		    File currentFile = new File(Directory.getPath(),s);
		    currentFile.delete();
		}
	}
	
	private void DeleteFolder(File dir) {

		String[]entries = dir.list();
		
		for (String s : entries) {
			File currentFile = new File(dir.getPath(),s);
			if (currentFile.isDirectory()) {
				DeleteFolder(currentFile);
			}
			currentFile.delete();
		}
	}
	
	private int launchProcess(List<String> args) throws Exception {
		try {
			ProcessBuilder pb = new ProcessBuilder();
			pb.command(args);
			Process p = pb.start();
			
			dumpStream(p.getInputStream(), System.out);
			dumpStream(p.getErrorStream(), System.err);

			p.waitFor();
			return p.exitValue();
		} catch (Exception e) {
			throw new AnalysisException(e);
		}
	}

	private void dumpStream(InputStream is, PrintStream out) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
			String line;
			while ((line = reader.readLine()) != null) {
				out.println(line);
			}
		}
	}

	public String getOriginalProgramPath() {
		return originalProgram;
	}

	public void setOriginalProgram(String o) {
		this.originalProgram = o;
	}

	private String getOriginalProgramDirectory() {
		return originalDirectory;
	}
	
	public void setOriginalProgramDirectory(String path) {
		this.originalDirectory = path;
	}

	private String getDirectoryName() {
		return nameEPLDirectory;
	}
	
	public void setDirectoryName(String path) {
		this.nameEPLDirectory = path;
	}

	private String getExecutionOrder() {
		return executionOrder;
	}
	
	public void setExecutionOrder(String order) {
		this.executionOrder = order;
	}

	public String getTestSuiteFile() {
		return testSuiteFile;
	}

	public void setTestSuiteFile(String path) {
		this.testSuiteFile = path;
	}
	
	private String getMutantPathsFile() throws Exception {

		// Creating the file with the mutant directory paths
		File OriginalDirectoryFile = new File (originalDirectory);
		File original = OriginalDirectoryFile.getParentFile();
		
		File[] paths;
		
		final String directoryname = OriginalDirectoryFile.getName();
		
		// create new filename filter
        FilenameFilter fileNameFilter = new FilenameFilter() {
  
           @Override
           public boolean accept(File dir, String name) {
        	   return name.startsWith(directoryname);
           }
        };
           
        // returns pathnames for files and directory
        paths = original.listFiles(fileNameFilter);
        File FilePaths = new File(new File(testDirectory), "ProgramPaths.txt");
        		
        FileWriter MutFilePaths = new FileWriter(FilePaths, true);
		BufferedWriter bw = new BufferedWriter(MutFilePaths);
		Arrays.sort(paths);
		
		for(File path:paths) {
			bw.write(path.toPath().resolve(nameEPLDirectory).toRealPath().toString() + "\n");
		}

		bw.close();

		return FilePaths.getCanonicalPath();
	}

	private int getNumberOfQueries() {
		return numberOfQueries;
	}
	
	public void setNumberOfQueries(int nqueries) {
		this.numberOfQueries = nqueries;
	}

	/*public void setDelimiter(String word) {
		this.delimiter = word;
	}

	public String getDelimiter() {
		return delimiter;
	}*/
	
	private Object getCoverage() {
		return coverage;
	}

	public void setCoverage(String covopt) {
		this.coverage = covopt;
	}
	public Map<BigInteger, String> getMapOpCode() {
		return mapOpCode;
	}

	private String normalizeDirectory(String directory) {
		return directory.charAt(directory.length() - 1) == '/' ? directory : directory.concat("/");
	}

	@Override
	public void loadDataFile(File dataFile) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public File getMutantFile(GAIndividual individual) throws UnsupportedOperationException, GenerationException {
		return generate(individual);
	}

	public String getTestDirectory() {
		return testDirectory;
	}

	public void setTestDirectory(String testDirectory) {
		this.testDirectory = testDirectory;
	}
	
	@Override
	public List<String> getTestCaseNames() throws AnalysisException {
		return null;
	}

	private void convertMatrixIntoResults(final List<Pair<String, Pair<int[], Long>>> cmpMatrix,
			final List<ComparisonResults> cmpResults) throws ComparisonException {
		for (final Pair<String, Pair<int[], Long>> result : cmpMatrix) {
			final String mutantPath = result.getLeft();
			final int[] bkgComparisons = result.getRight().getLeft();
			final long bkgNanos = result.getRight().getRight();

			final String basename = new File(mutantPath).getName();
			final ComparisonResults comparisonResults = convertLineIntoResults(basename, bkgComparisons, bkgNanos);

			cmpResults.add(comparisonResults);
		}
	}

	private ComparisonResults convertLineIntoResults(final String basename, final int[] cmps, final long nanos)
			throws ComparisonException {

		final GAIndividual individual = mapBasenameToIndividual.get(basename);

		if (individual == null) {
			throw new ComparisonException("Could not find individual with basename " + basename);
		}

		final ComparisonResult[] results = new ComparisonResult[cmps.length];
		for (int i = 0; i < cmps.length; ++i) {
			results[i] = ComparisonResult.getObject(cmps[i]);
		}
		final ComparisonResults comparisonResults = new ComparisonResults(individual, results);

		final BigInteger[] bigNanos = new BigInteger[cmps.length];
		for (int i = 0; i < bigNanos.length; ++i) {
			bigNanos[i] = BigInteger.valueOf(nanos);
		}
		comparisonResults.setTestWallNanos(bigNanos);

		return comparisonResults;
	}

	@Override
	public List<String> getMutationOperatorNames() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public File getOriginalProgramFile() throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		// NO SE USA
		return null;
	}

}
